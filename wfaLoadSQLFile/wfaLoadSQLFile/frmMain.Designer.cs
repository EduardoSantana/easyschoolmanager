﻿namespace wfaLoadSQLFile
{
	partial class frmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblTotalLineasLabel = new System.Windows.Forms.Label();
            this.lblTotalInsertLabel = new System.Windows.Forms.Label();
            this.lblTotalCommitLabel = new System.Windows.Forms.Label();
            this.lblTiempoLabel = new System.Windows.Forms.Label();
            this.lblTiempoActual = new System.Windows.Forms.Label();
            this.lblTotalCommit = new System.Windows.Forms.Label();
            this.lblTotalInsert = new System.Windows.Forms.Label();
            this.lblSelectedFileLabel = new System.Windows.Forms.Label();
            this.txtSelectedFile = new System.Windows.Forms.TextBox();
            this.cmdFindFile = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnStartWork = new System.Windows.Forms.Button();
            this.cmdCancelWork = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lblOthersTotal = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCurrentTotalLine = new System.Windows.Forms.Label();
            this.chkIsTest = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLoops = new System.Windows.Forms.TextBox();
            this.lblLoopsLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Multiselect = true;
            // 
            // lblTotalLineasLabel
            // 
            this.lblTotalLineasLabel.AutoSize = true;
            this.lblTotalLineasLabel.Location = new System.Drawing.Point(100, 162);
            this.lblTotalLineasLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalLineasLabel.Name = "lblTotalLineasLabel";
            this.lblTotalLineasLabel.Size = new System.Drawing.Size(62, 13);
            this.lblTotalLineasLabel.TabIndex = 0;
            this.lblTotalLineasLabel.Text = "Total Linas:";
            // 
            // lblTotalInsertLabel
            // 
            this.lblTotalInsertLabel.AutoSize = true;
            this.lblTotalInsertLabel.Location = new System.Drawing.Point(71, 89);
            this.lblTotalInsertLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalInsertLabel.Name = "lblTotalInsertLabel";
            this.lblTotalInsertLabel.Size = new System.Drawing.Size(89, 13);
            this.lblTotalInsertLabel.TabIndex = 1;
            this.lblTotalInsertLabel.Text = "Insert Processed:";
            // 
            // lblTotalCommitLabel
            // 
            this.lblTotalCommitLabel.AutoSize = true;
            this.lblTotalCommitLabel.Location = new System.Drawing.Point(64, 112);
            this.lblTotalCommitLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalCommitLabel.Name = "lblTotalCommitLabel";
            this.lblTotalCommitLabel.Size = new System.Drawing.Size(97, 13);
            this.lblTotalCommitLabel.TabIndex = 2;
            this.lblTotalCommitLabel.Text = "Commit Processed:";
            // 
            // lblTiempoLabel
            // 
            this.lblTiempoLabel.AutoSize = true;
            this.lblTiempoLabel.Location = new System.Drawing.Point(117, 182);
            this.lblTiempoLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTiempoLabel.Name = "lblTiempoLabel";
            this.lblTiempoLabel.Size = new System.Drawing.Size(45, 13);
            this.lblTiempoLabel.TabIndex = 3;
            this.lblTiempoLabel.Text = "Tiempo:";
            // 
            // lblTiempoActual
            // 
            this.lblTiempoActual.AutoSize = true;
            this.lblTiempoActual.Location = new System.Drawing.Point(164, 182);
            this.lblTiempoActual.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTiempoActual.Name = "lblTiempoActual";
            this.lblTiempoActual.Size = new System.Drawing.Size(49, 13);
            this.lblTiempoActual.TabIndex = 7;
            this.lblTiempoActual.Text = "00:00:00";
            // 
            // lblTotalCommit
            // 
            this.lblTotalCommit.AutoSize = true;
            this.lblTotalCommit.Location = new System.Drawing.Point(164, 112);
            this.lblTotalCommit.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalCommit.Name = "lblTotalCommit";
            this.lblTotalCommit.Size = new System.Drawing.Size(19, 13);
            this.lblTotalCommit.TabIndex = 6;
            this.lblTotalCommit.Text = "00";
            // 
            // lblTotalInsert
            // 
            this.lblTotalInsert.AutoSize = true;
            this.lblTotalInsert.Location = new System.Drawing.Point(164, 89);
            this.lblTotalInsert.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalInsert.Name = "lblTotalInsert";
            this.lblTotalInsert.Size = new System.Drawing.Size(19, 13);
            this.lblTotalInsert.TabIndex = 5;
            this.lblTotalInsert.Text = "00";
            // 
            // lblSelectedFileLabel
            // 
            this.lblSelectedFileLabel.AutoSize = true;
            this.lblSelectedFileLabel.Location = new System.Drawing.Point(27, 24);
            this.lblSelectedFileLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSelectedFileLabel.Name = "lblSelectedFileLabel";
            this.lblSelectedFileLabel.Size = new System.Drawing.Size(71, 13);
            this.lblSelectedFileLabel.TabIndex = 8;
            this.lblSelectedFileLabel.Text = "Selected File:";
            // 
            // txtSelectedFile
            // 
            this.txtSelectedFile.Location = new System.Drawing.Point(104, 22);
            this.txtSelectedFile.Margin = new System.Windows.Forms.Padding(2);
            this.txtSelectedFile.Name = "txtSelectedFile";
            this.txtSelectedFile.Size = new System.Drawing.Size(185, 20);
            this.txtSelectedFile.TabIndex = 9;
            // 
            // cmdFindFile
            // 
            this.cmdFindFile.Location = new System.Drawing.Point(292, 20);
            this.cmdFindFile.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFindFile.Name = "cmdFindFile";
            this.cmdFindFile.Size = new System.Drawing.Size(56, 22);
            this.cmdFindFile.TabIndex = 10;
            this.cmdFindFile.Text = "Find File";
            this.cmdFindFile.UseVisualStyleBackColor = true;
            this.cmdFindFile.Click += new System.EventHandler(this.cmdFindFile_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(19, 213);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(329, 19);
            this.progressBar1.TabIndex = 11;
            // 
            // btnStartWork
            // 
            this.btnStartWork.Location = new System.Drawing.Point(18, 255);
            this.btnStartWork.Margin = new System.Windows.Forms.Padding(2);
            this.btnStartWork.Name = "btnStartWork";
            this.btnStartWork.Size = new System.Drawing.Size(152, 22);
            this.btnStartWork.TabIndex = 14;
            this.btnStartWork.Text = "Start Working";
            this.btnStartWork.UseVisualStyleBackColor = true;
            this.btnStartWork.Click += new System.EventHandler(this.btnStartWork_Click);
            // 
            // cmdCancelWork
            // 
            this.cmdCancelWork.Location = new System.Drawing.Point(196, 255);
            this.cmdCancelWork.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCancelWork.Name = "cmdCancelWork";
            this.cmdCancelWork.Size = new System.Drawing.Size(152, 22);
            this.cmdCancelWork.TabIndex = 15;
            this.cmdCancelWork.Text = "Cancel Work";
            this.cmdCancelWork.UseVisualStyleBackColor = true;
            this.cmdCancelWork.Click += new System.EventHandler(this.cmdCancelWork_Click);
            // 
            // lblOthersTotal
            // 
            this.lblOthersTotal.AutoSize = true;
            this.lblOthersTotal.Location = new System.Drawing.Point(164, 136);
            this.lblOthersTotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOthersTotal.Name = "lblOthersTotal";
            this.lblOthersTotal.Size = new System.Drawing.Size(19, 13);
            this.lblOthersTotal.TabIndex = 17;
            this.lblOthersTotal.Text = "00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 136);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Others Processed:";
            // 
            // lblCurrentTotalLine
            // 
            this.lblCurrentTotalLine.AutoSize = true;
            this.lblCurrentTotalLine.Location = new System.Drawing.Point(164, 162);
            this.lblCurrentTotalLine.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCurrentTotalLine.Name = "lblCurrentTotalLine";
            this.lblCurrentTotalLine.Size = new System.Drawing.Size(19, 13);
            this.lblCurrentTotalLine.TabIndex = 18;
            this.lblCurrentTotalLine.Text = "00";
            // 
            // chkIsTest
            // 
            this.chkIsTest.AutoSize = true;
            this.chkIsTest.Location = new System.Drawing.Point(102, 58);
            this.chkIsTest.Margin = new System.Windows.Forms.Padding(2);
            this.chkIsTest.Name = "chkIsTest";
            this.chkIsTest.Size = new System.Drawing.Size(15, 14);
            this.chkIsTest.TabIndex = 19;
            this.chkIsTest.UseVisualStyleBackColor = true;
            this.chkIsTest.CheckedChanged += new System.EventHandler(this.chkIsTest_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 58);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Is a Test:";
            // 
            // txtLoops
            // 
            this.txtLoops.Location = new System.Drawing.Point(166, 55);
            this.txtLoops.Margin = new System.Windows.Forms.Padding(2);
            this.txtLoops.Name = "txtLoops";
            this.txtLoops.Size = new System.Drawing.Size(122, 20);
            this.txtLoops.TabIndex = 21;
            // 
            // lblLoopsLabel
            // 
            this.lblLoopsLabel.AutoSize = true;
            this.lblLoopsLabel.Location = new System.Drawing.Point(124, 57);
            this.lblLoopsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLoopsLabel.Name = "lblLoopsLabel";
            this.lblLoopsLabel.Size = new System.Drawing.Size(39, 13);
            this.lblLoopsLabel.TabIndex = 22;
            this.lblLoopsLabel.Text = "Loops:";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 297);
            this.Controls.Add(this.lblLoopsLabel);
            this.Controls.Add(this.txtLoops);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkIsTest);
            this.Controls.Add(this.lblCurrentTotalLine);
            this.Controls.Add(this.lblOthersTotal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmdCancelWork);
            this.Controls.Add(this.btnStartWork);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.cmdFindFile);
            this.Controls.Add(this.txtSelectedFile);
            this.Controls.Add(this.lblSelectedFileLabel);
            this.Controls.Add(this.lblTiempoActual);
            this.Controls.Add(this.lblTotalCommit);
            this.Controls.Add(this.lblTotalInsert);
            this.Controls.Add(this.lblTiempoLabel);
            this.Controls.Add(this.lblTotalCommitLabel);
            this.Controls.Add(this.lblTotalInsertLabel);
            this.Controls.Add(this.lblTotalLineasLabel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmMain";
            this.Text = "Load Large SQL Files To DB";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Label lblTotalLineasLabel;
		private System.Windows.Forms.Label lblTotalInsertLabel;
		private System.Windows.Forms.Label lblTotalCommitLabel;
		private System.Windows.Forms.Label lblTiempoLabel;
		private System.Windows.Forms.Label lblTiempoActual;
		private System.Windows.Forms.Label lblTotalCommit;
		private System.Windows.Forms.Label lblTotalInsert;
		private System.Windows.Forms.Label lblSelectedFileLabel;
		private System.Windows.Forms.TextBox txtSelectedFile;
		private System.Windows.Forms.Button cmdFindFile;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnStartWork;
		private System.Windows.Forms.Button cmdCancelWork;
		private System.ComponentModel.BackgroundWorker backgroundWorker1;
		private System.Windows.Forms.Label lblOthersTotal;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lblCurrentTotalLine;
		private System.Windows.Forms.CheckBox chkIsTest;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtLoops;
		private System.Windows.Forms.Label lblLoopsLabel;
	}
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace wfaLoadSQLFile
{
	public partial class frmMain : Form
	{

		public frmMain()
		{
			InitializeComponent();
			// To report progress from the background worker we need to set this property
			backgroundWorker1.WorkerReportsProgress = true;
			// This event will be raised on the worker thread when the worker starts
			backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
			// This event will be raised when we call ReportProgress
			backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
			// This event will support cancellation
			backgroundWorker1.WorkerSupportsCancellation = true;

			chkIsTest.Checked = false;
			lblLoopsLabel.Visible = chkIsTest.Checked;
			txtLoops.Visible = chkIsTest.Checked;
		}

		private void cmdFindFile_Click(object sender, EventArgs e)
		{
			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				int i = 0;
				foreach (string item in openFileDialog1.FileNames)
				{ 
					if(i>0)
						txtSelectedFile.Text += ",";
					txtSelectedFile.Text += item;
					i++;
				}
			}
		}
		
		// On worker thread so do our thing!
		private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
		{
			var send = (IclLoadFile)e.Argument;
			send.LoadFiles(e);
		}

		// Back on the 'UI' thread so we can update the progress bar
		private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			// The progress percentage is a property of e
			var obj = (uiNotifiedBack)e.UserState;
			if (obj.totalLines > int.MaxValue)
				obj.totalLines = int.MaxValue;
			progressBar1.Maximum = (int)obj.totalLines;
			progressBar1.Value = e.ProgressPercentage;
			lblTotalInsert.Text = obj.totalOfInsert.ToString("N");
			lblTotalCommit.Text = obj.totalOfCommit.ToString("N");
			lblOthersTotal.Text = obj.totalOfOthers.ToString("N");
			lblCurrentTotalLine.Text = string.Format("{0} of {1}", e.ProgressPercentage.ToString("N"), obj.totalLines.ToString("N"));
			lblTiempoActual.Text = string.Format("{0} of {1}", obj.actualTimeLabel, obj.totalTimeLabel);
		}

		private void cmdCancelWork_Click(object sender, EventArgs e)
		{
			backgroundWorker1.CancelAsync();
		}

		private void btnStartWork_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtSelectedFile.Text)) 
			{
				MessageBox.Show("Please select a file!", "Select a File", MessageBoxButtons.OK);
				return;
			}
			int totalLines = 0;
			if (!int.TryParse(txtLoops.Text, out totalLines) && chkIsTest.Checked)
			{
				MessageBox.Show("Please select a number!", "Select a number", MessageBoxButtons.OK);
				return;
			}
			// Start the background worker
			var send = new uiNotifiedSend()
			{
				filePath = txtSelectedFile.Text,
				isTest = chkIsTest.Checked,
				totalLines = totalLines,
				backgroundWorker1 = backgroundWorker1,
				frm = this
			};
		
			IclLoadFile lf = new clLoadFileRNC(send);

			if (send.isTest)
				lf = new clLoadFileTest(send);

			backgroundWorker1.RunWorkerAsync(lf);
						
		}

		private void chkIsTest_CheckedChanged(object sender, EventArgs e)
		{
			lblLoopsLabel.Visible = chkIsTest.Checked;
			txtLoops.Visible = chkIsTest.Checked;
		}

		private void frmMain_Load(object sender, EventArgs e)
		{
			txtSelectedFile.Text = System.Configuration.ConfigurationManager.AppSettings["FileToDownloadURL"];
		}
	}
}

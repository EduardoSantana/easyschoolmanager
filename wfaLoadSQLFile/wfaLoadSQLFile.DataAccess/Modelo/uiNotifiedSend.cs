﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wfaLoadSQLFile
{
	public class uiNotifiedSend
	{
		public string filePath { get; set; }
		public int totalLines { get; set; }
		public bool isTest { get; set; }
		public DoWorkEventArgs e { get; set; }
		public BackgroundWorker backgroundWorker1 { get; set; }
		public Form frm { get; set; }
	}

}

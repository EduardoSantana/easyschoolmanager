﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wfaLoadSQLFile
{
	public struct uiNotifiedBack
	{
		public double totalLines { get; set; }
		public double totalOfInsert { get; set; }
		public double totalOfCommit { get; set; }
		public double totalOfOthers { get; set; }
		public double actualTime { get; set; }
		public string actualTimeLabel { get; set; }
		public double totalTime { get; set; }
		public string totalTimeLabel { get; set; }
	}
}

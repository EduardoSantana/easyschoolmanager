﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wfaLoadSQLFile
{
	public class FilesLoadedLog
	{
		public int RecordsTotal { get; set; }
		public string RecordsTotalToString { get { return string.Format("Total Records Readed: {0}", RecordsTotal); } }

		public int RecordsInserted { get; set; }
		public string RecordsInsertedToString { get { return string.Format("Total Records Inserted: {0}", RecordsInserted); } }

		public int RecordsUpdated { get; set; }
		public string RecordsUpdatedToString { get { return string.Format("Total Records Updated: {0}", RecordsUpdated); } }

		public int RecordsNotAffected { get { return RecordsTotal - (RecordsUpdated + RecordsInserted); } }
		public string RecordsNotAffectedToString { get { return string.Format("Total Not Affected: {0}", RecordsNotAffected); } }

		public override string ToString()
		{
			return string.Format("Log of Import is:\n{0}\n{1}\n{2}\n{3}", RecordsInsertedToString, RecordsUpdatedToString, RecordsNotAffectedToString, RecordsTotalToString);
		}
	}

}

namespace wfaLoadSQLFile
{
	using System.Data.Entity;
	public partial class dbDataAccessContext : DbContext
	{
		public dbDataAccessContext()
			: base("name=dbDataAccessContext")
		{
            Database.SetInitializer<dbDataAccessContext>(null);
        }

		public virtual DbSet<ErrorImportings> ErrrorImporting { get; set; }

	}
}

namespace wfaLoadSQLFile
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ErrorImportings")]
    public partial class ErrorImportings
    {
        public ErrorImportings()
        {
            IsActive = true;
            IsDeleted = false;
            CreationTime = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        public string Details { get; set; }

        public string InnserException { get; set; }

        public string CurrentItem { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }

        public long? CreatorUserId { get; set; }

        public DateTime CreationTime { get; set; }

    }
}

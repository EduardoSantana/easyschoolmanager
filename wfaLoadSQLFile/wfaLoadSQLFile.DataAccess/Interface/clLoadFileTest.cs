﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
namespace wfaLoadSQLFile
{
	public class clLoadFileTest : clLoadFileAdstract
	{

		public clLoadFileTest(uiNotifiedSend _paramSend) 
		{
			this.paramSend = _paramSend;
		}

		public override FilesLoadedLog LoadFile(string filePath, int totalLines)
		{
			var sw = new Stopwatch();
			sw.Start();
			double currentLoop = 0;
			double totalOfInsert = 0;
			double totalOfCommit = 0;
			double totalOfOthers = 0;
			string lineExecuted = "";
			var db = new dbDataAccessContext();
			var file = new System.IO.StreamReader(filePath);
			for (int i = 1; i <= totalLines; i++)
			{
				currentLoop++;
				var firstWord = "Insert";
				
				try
				{
					if (paramSend.backgroundWorker1.CancellationPending) //checks for cancel request
					{
						paramSend.e.Cancel = true;
						break;
					}
					lineExecuted += "abx" + i.ToString();
					var asdfasdf = lineExecuted.Substring(lineExecuted.Length - 1, 1);
					//bool execInsert = lineExecuted.Substring(lineExecuted.Length - 2, lineExecuted.Length - 1) == ";";

				}
				catch (Exception ex)
				{
					logError(firstWord, db, ex);
				}

				if (firstWord.Contains("Insert"))
					totalOfInsert++;

				if (firstWord.Contains("Commit"))
					totalOfCommit++;

				if (!firstWord.Contains("Insert") && !firstWord.Contains("Commit"))
					totalOfOthers++;

				var send = notifyWork(paramSend.backgroundWorker1, totalLines, currentLoop, totalOfInsert, totalOfCommit, totalOfOthers, sw);
				paramSend.backgroundWorker1.ReportProgress((int)currentLoop, send);
				
				System.Threading.Thread.Sleep(10);

			}
			sw.Stop();
			file.Close();
            return new FilesLoadedLog();

        }

		public override int getTotalLines(string filePath, int totalLines)
		{
			var sw = new Stopwatch();
			sw.Start();
			int tiempoEstimado = 1 * 60 * 500; // 1 Minutos en milesimas.
			int retVal = 0;

			for (int i = 0; i <= totalLines; i++)
			{
				if (paramSend.backgroundWorker1.CancellationPending) //checks for cancel request
				{
					paramSend.e.Cancel = true;
					break;
				}

				if (tiempoEstimado <= sw.Elapsed.TotalMilliseconds)
					tiempoEstimado = (int)sw.Elapsed.TotalMilliseconds + 1000;

				var send = notifyWork(paramSend.backgroundWorker1, 0, i, 0, 0, 0, sw, tiempoEstimado);

				if (send.totalLines <= i)
					send.totalLines = i + 1;

				paramSend.backgroundWorker1.ReportProgress((int)i, send);

				retVal = i;

				System.Threading.Thread.Sleep(1);
			
			}

			var send2 = notifyWork(paramSend.backgroundWorker1, retVal, retVal, 0, 0, 0, sw);
			paramSend.backgroundWorker1.ReportProgress(retVal, send2);

			sw.Stop();
			return retVal;
		}

	}

}

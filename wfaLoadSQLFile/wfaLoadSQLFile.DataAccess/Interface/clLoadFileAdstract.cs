﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
// poner total lines
namespace wfaLoadSQLFile
{
	public abstract class clLoadFileAdstract : IclLoadFile
	{
		public uiNotifiedSend paramSend { get; set; }

		public virtual void LoadFiles(DoWorkEventArgs e) 
		{
			paramSend.e = e;
			foreach (string item in paramSend.filePath.Split(','))
			{
				int totalLine = getTotalLines(item, paramSend.totalLines);
				LoadFile(item, totalLine);
				logError(item, new dbDataAccessContext(), new Exception("Archivo Procesado!"));
			}
		}

        public virtual FilesLoadedLog LoadFile(string filePath, int totalLines)
		{
			var sw = new Stopwatch();
			sw.Start();
			double currentLoop = 0;
			double totalOfInsert = 0;
			double totalOfCommit = 0;
			double totalOfOthers = 0;
			string line;
			string lineExecuted = "";
			var db = new dbDataAccessContext();
			var file = new StreamReader(filePath);
			while ((line = file.ReadLine()) != null)
			{
				currentLoop++;
				bool isExec = false;
				bool isCommit = line.Contains("commit;");
				try
				{
					if (paramSend.backgroundWorker1.CancellationPending) //checks for cancel request
					{
						paramSend.e.Cancel = true;
						break;
					}
					
					line = line.Replace(",TO_CLOB(", ",CONVERT(VARCHAR(MAX),");
					line = line.Replace("|| TO_CLOB(", "+ CONVERT(VARCHAR(MAX),");
					line = line.Replace(",to_date", ",dbo.to_date");
					line = line.Trim().Replace(Environment.NewLine, "");
					lineExecuted += line;

					isExec = lineExecuted.Substring(lineExecuted.Length - 1, 1) == ";";

					if (isExec && !isCommit)
					{
						db.Database.ExecuteSqlCommand(lineExecuted);
					}

					if (isExec || isCommit)
						lineExecuted = "";

				}
				catch (Exception ex)
				{
					logError(line, db, ex);
				}

				if (isExec)
					totalOfInsert++;

				if (isCommit)
					totalOfCommit++;

				if (!isExec && !isCommit)
					totalOfOthers++;


				var send = notifyWork(paramSend.backgroundWorker1, totalLines, currentLoop, totalOfInsert, totalOfCommit, totalOfOthers, sw);

				paramSend.frm.Invoke((System.Windows.Forms.MethodInvoker)delegate { paramSend.backgroundWorker1.ReportProgress((int)currentLoop, send); });

			}
			sw.Stop();
			file.Close();
            return new FilesLoadedLog();

        }

		protected void logError(string line, dbDataAccessContext db, Exception ex)
		{
			var _error = new ErrorImportings();
			_error.CurrentItem = line;
			_error.Details = ex.Message;
			_error.InnserException = (ex.InnerException == null ? "" : ex.InnerException.ToString());
			db.ErrrorImporting.Add(_error);
			db.SaveChanges();
		}

		protected uiNotifiedBack notifyWork(BackgroundWorker backgroundWorker1, double totalLines, double currentLoop, double totalOfInsert, double totalOfCommit, double totalOfOthers, Stopwatch sw, int tiempoEstimado = 0)
		{
			var retVal = new uiNotifiedBack();
			retVal.totalOfInsert = totalOfInsert;
			retVal.totalOfOthers = totalOfOthers;
			retVal.totalOfCommit = totalOfCommit;
			retVal.actualTime = sw.Elapsed.TotalMilliseconds;
			retVal.actualTimeLabel = string.Format("{0}:{1}:{2}", sw.Elapsed.Hours.ToString("00"), sw.Elapsed.Minutes.ToString("00"), sw.Elapsed.Seconds.ToString("00"));

			if (tiempoEstimado > 0 || totalLines == 0)
			{
				retVal.totalLines = getRegla3(retVal.actualTime, currentLoop, tiempoEstimado);
				retVal.totalTime = tiempoEstimado;
			}
			else
			{
				retVal.totalLines = totalLines;
				retVal.totalTime = getRegla3(currentLoop, retVal.actualTime, totalLines);
			}

			var ts = new TimeSpan(0, 0, 0, 0, (int)retVal.totalTime);
			retVal.totalTimeLabel = string.Format("{0}:{1}:{2}", ts.Hours.ToString("00"), ts.Minutes.ToString("00"), ts.Seconds.ToString("00"));
			return retVal;
		}

		protected double getRegla3(double a, double b, double c)
		{
			// a => b
			// c => x
			if (a == 0)
				a = 1;

			if (b == 0)
				b = 1;

			if (c == 0)
				c = 1;

			return (b * c) / a;
		}

		public virtual int getTotalLines(string filePath, int totalLines)
		{
			var sw = new Stopwatch();
			sw.Start();
			using (System.IO.StreamReader r = new System.IO.StreamReader(filePath))
			{
				int i = 0;
				while (r.ReadLine() != null)
				{

					i++;
					if (paramSend.backgroundWorker1.CancellationPending) //checks for cancel request
					{
						paramSend.e.Cancel = true;
						break;
					}

				}

				totalLines = i;
			}

			var send = notifyWork(paramSend.backgroundWorker1, totalLines, totalLines, 0, 0, 0, sw);
			paramSend.backgroundWorker1.ReportProgress(totalLines, send);

			sw.Stop();
			return totalLines;
		}

	}

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
namespace wfaLoadSQLFile
{
	public class clLoadFileRNC : clLoadFileAdstract
	{

		public string CommandToUnzip { get { return ConfigurationManager.AppSettings["CommandToUnzip"]; } }
		public string FolderToImport { get { return ConfigurationManager.AppSettings["FolderToImport"]; } }
		public string FileToImport { get { return ConfigurationManager.AppSettings["FileToImport"]; } }
		public string FileToDownload { get { return ConfigurationManager.AppSettings["FileToDownload"]; } }

		public clLoadFileRNC(uiNotifiedSend _paramSend) 
		{
			this.paramSend = _paramSend;
		}

		public override void LoadFiles(DoWorkEventArgs e)
		{
			paramSend.e = e;
            WebClient client = new WebClient();
			DownloadFile(paramSend.filePath);
		}

        public string GetRemoteFilenameTimestamp(Uri address)
        {

            HttpWebRequest request = null;
            HttpWebResponse response = null;
            string dat = "";

            try
            {
                //' Create the web request   
                request = (System.Net.HttpWebRequest)WebRequest.Create(address);

                //' Get response   
                response = (System.Net.HttpWebResponse)request.GetResponse();
                dat = response.Headers.Get("Last-Modified");
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
                //If Not response Is Nothing Then response.Close()
            }

            return dat;

        }

        public void DownloadFile(string fromUrl)
		{
			WebClient client = new WebClient();
			client.DownloadFile(new Uri(fromUrl), FileToDownload);
			ExecuteCommandSync(CommandToUnzip + " " + FileToDownload);
			ReadFiles();
		}

		public void ReadFiles()
		{
			var files = this.GetType().Assembly.Location.Replace(this.GetType().Assembly.ManifestModule.Name, "") + FileToImport;

			foreach (string item in files.Split(','))
			{
				int totalLine = getTotalLines(item, paramSend.totalLines);
				var retVal = LoadFile(item, totalLine);
				logError(item, new dbDataAccessContext(), new Exception("Archivo Procesado!\n" + retVal.ToString() ));
				Directory.Delete(FolderToImport, true);
				File.Delete(FileToDownload);
			}
		}

		/// <span class="code-SummaryComment"><summary></span>
		/// Executes a shell command synchronously.
		/// <span class="code-SummaryComment"></summary></span>
		/// <span class="code-SummaryComment"><param name="command">string command</param></span>
		/// <span class="code-SummaryComment"><returns>string, as output of the command.</returns></span>
		public string ExecuteCommandSync(object command)
		{
			try
			{
				// create the ProcessStartInfo using "cmd" as the program to be run,
				// and "/c " as the parameters.
				// Incidentally, /c tells cmd that we want it to execute the command that follows,
				// and then exit.
				System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);

				// The following commands are needed to redirect the standard output.
				// This means that it will be redirected to the Process.StandardOutput StreamReader.
				procStartInfo.RedirectStandardOutput = true;
				procStartInfo.UseShellExecute = false;
				// Do not create the black window.
				procStartInfo.CreateNoWindow = true;
				// Now we create a process, assign its ProcessStartInfo and start it
				System.Diagnostics.Process proc = new System.Diagnostics.Process();
				proc.StartInfo = procStartInfo;
				proc.Start();

				// Get the output into a string
				return proc.StandardOutput.ReadToEnd();
				// Display the command output.

			}
			catch (Exception objException)
			{
				return objException.Message;
			}
		}
		
		public override FilesLoadedLog LoadFile(string filePath, int totalLines)
		{
			var retVal = new FilesLoadedLog() { RecordsTotal = totalLines };
			var sw = new Stopwatch();
			sw.Start();
			char columnSeparator = '|';
			double currentLoop = 0;
			double totalOfInsert = 0;
			double totalOfCommit = 0;
			double totalOfOthers = 0;
			string line;
			var db = new dbDataAccessContext();
			var file = new StreamReader(filePath);
			while ((line = file.ReadLine()) != null)
			{
				currentLoop++;
				bool isExec = false;
				
				try
				{
					if (paramSend.backgroundWorker1.CancellationPending) //checks for cancel request
					{
						paramSend.e.Cancel = true;
						break;
					}
					var columns = line.Split(columnSeparator);
					var sqlStatement = @"
					IF NOT EXISTS (SELECT 1 FROM [dbo].[Companies] WHERE RNC = {0})
					BEGIN
						INSERT INTO [dbo].[Companies]
								   ([RNC]
								   ,[Name]
								   ,[CommercialName]
								   ,[CommercialActivity]
								   ,[CompanyDate]
								   ,[Status]
								   ,[PaymentSystem]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime])
							 VALUES
								   (@p0 --<RNC, nvarchar(15),>
								   ,@p1 --<Name, nvarchar(200),>
								   ,@p2 --<CommercialName, nvarchar(200),>
								   ,@p3 --<CommercialActivity, nvarchar(200),>
								   ,@p4 --<CompanyDate, nvarchar(10),>
								   ,@p5 --<Status, nvarchar(15),>
								   ,@p6 --<PaymentSystem, nvarchar(100),>
								   ,0 --<IsDeleted, bit,>
								   ,1 --<IsActive, bit,>
								   ,NULL --<CreatorUserId, bigint,>
								   ,GETDATE() --<CreationTime, datetime,>
								   ,NULL --<LastModifierUserId, bigint,>
								   ,NULL --<LastModificationTime, datetime,>
								   ,NULL --<DeleterUserId, bigint,>
								   ,NULL) --<DeletionTime, datetime,>
						SELECT CAST(1 AS BIT) AS IS_INSERT, @@ROWCOUNT AS QTY_ROWS;
					END
					ELSE
					BEGIN
						UPDATE [dbo].[Companies] SET [Status] = @p5, [LastModificationTime] = GETDATE() WHERE RNC = @p0 AND [Status] != @p5;
						SELECT CAST(0 AS BIT) AS IS_INSERT, @@ROWCOUNT AS QTY_ROWS;
					END";

					isExec = true;

					if (isExec)
					{
						
						var rowsAffected = db.Database.SqlQuery<SqlQueryRetVal>(sqlStatement,
						(string.IsNullOrEmpty(columns[0]) ? DBNull.Value : (object)columns[0]), // RNC
						(string.IsNullOrEmpty(columns[1]) ? DBNull.Value : (object)columns[1]), // Name
						(string.IsNullOrEmpty(columns[2]) ? DBNull.Value : (object)columns[2]), // CommercialName
						(string.IsNullOrEmpty(columns[3]) ? DBNull.Value : (object)columns[3]), // CommercialActivity
						(string.IsNullOrEmpty(columns[8]) ? DBNull.Value : (object)columns[8]), // CompanyDate
						(string.IsNullOrEmpty(columns[9]) ? DBNull.Value : (object)columns[9]), // Status
						(string.IsNullOrEmpty(columns[10]) ? DBNull.Value : (object)columns[10]) // PaymentSystem
						).FirstOrDefault();

						if (rowsAffected.IS_INSERT)
							totalOfInsert++;
						else if (rowsAffected.QTY_ROWS > 0)
							totalOfOthers++;

					}

				}
				catch (Exception ex)
				{
					logError(line, db, ex);
				}

				retVal.RecordsInserted = (int)totalOfInsert;
				retVal.RecordsUpdated = (int)totalOfOthers;
			   
				var send = notifyWork(paramSend.backgroundWorker1, totalLines, currentLoop, totalOfInsert, totalOfCommit, totalOfOthers, sw);

				paramSend.frm.Invoke((System.Windows.Forms.MethodInvoker)delegate { paramSend.backgroundWorker1.ReportProgress((int)currentLoop, send); });

			}
			sw.Stop();
			file.Close();
			return retVal;
		}

	}

}

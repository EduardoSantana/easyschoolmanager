﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MG
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string ViewsDirectory = @"EasySchoolManager.Web\App\Main\views\";
        private string ViewDirectory = "";
        private string fileNameIndexJs = "";
        private string fileNameEditJs = "";
        private string fileNameCreateJs = "";
        public static string ClassesPath = @"EasySchoolManager.Core\Models\";
        public static string DtosPath = @"EasySchoolManager.Application\";
        public static string ClassPath = "";
        public string pathDirectoryApp = @"EasySchoolManager.Application\";
        public static string DtosPathTemp;
        private string fileNameIndexCsHtml = "";
        private string fileNameEditCsHtml = "";
        private string fileNameCreateCsHtml = "";

        private void txtEntityNameSingular_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string camell = "";
                var lower = txtCamelSingular.Text;
                camell = lower + "s";
                var capital = camell.Substring(0, 1).ToUpper() + camell.Substring(1);
                var capitalSingular = lower.Substring(0, 1).ToUpper() + lower.Substring(1);

                txtCamelPlural.Text = camell;
                txtCapitalPlural.Text = capital;
                txtCapitalSingular.Text = capitalSingular;

                btnGenerate_Click(null, null);
            }
            catch (Exception err)
            { }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            cleanRiches();

            var stringDataIAppService = IAppServiceCreator.TemplateIAppService.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            stringDataIAppService = stringDataIAppService.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            rtbIAppSericeGenerator.Clear();
            rtbIAppSericeGenerator.AppendText(stringDataIAppService);

            var stringDataAppService = AppServiceCreator.TemplateAppService.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            stringDataAppService = stringDataAppService.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            stringDataAppService = stringDataAppService.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            rtbAppSericeGenerator.Clear();
            rtbAppSericeGenerator.AppendText(stringDataAppService);

            ClassPath = ClassesPath + txtCapitalPlural.Text + ".cs";

            var LoadedClass = System.IO.File.ReadAllLines(ClassPath);


            var FielList = GenerateDtos(LoadedClass);
            List<string> FielListForIndexJs = GenerateFielListForIndexJs(FielList);
            List<string> FielListForCreateCsHtml = GenerateFielListForCreateCsHtml(FielList);

            GenerateJsFiles(FielList, FielListForIndexJs);

            GenerateCsHtmlFiles(FielListForCreateCsHtml);

        }

        private void cleanRiches()
        {
            rtbIAppSericeGenerator.Clear();
            rtbAppSericeGenerator.Clear();
            rtbEntityCreateDtoGenerator.Clear();
            rtbEntityDtoGenerator.Clear();
            rtbEntityUpdateDtoGenerator.Clear();
            rtbIndexJsGenerator.Clear();
            rtbIndexCshtmlGenerator.Clear();
            rtbEntityDtoGenerator.Clear();
            rtbCreateJsGenerator.Clear();
            rtbCreateCsHtmlGenerator.Clear();
            rtbUpdateJsGenerator.Clear();
        }

        private void GenerateCsHtmlFiles(List<String> fielListForCreateCsHtml)
        {
            fileNameIndexCsHtml = ViewDirectory + @"\index.cshtml";
            fileNameEditCsHtml = ViewDirectory + @"\editModal.cshtml";
            fileNameCreateCsHtml = ViewDirectory + @"\createModal.cshtml";

            string IndexCsHtmlTemplate = UsingTemplates.CsHtmlCreator.IndexCsHtml;
            IndexCsHtmlTemplate = IndexCsHtmlTemplate.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            IndexCsHtmlTemplate = IndexCsHtmlTemplate.Replace("XXXEntityLowerPluralXXX", txtCamelPlural.Text);
            IndexCsHtmlTemplate = IndexCsHtmlTemplate.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            IndexCsHtmlTemplate = IndexCsHtmlTemplate.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            IndexCsHtmlTemplate += "\n";


            string CreateCsHtmlTemplate = UsingTemplates.CsHtmlCreator.CreateModalCsHtml;
            var strinWithFields = string.Join("", fielListForCreateCsHtml.ToArray());
            CreateCsHtmlTemplate = CreateCsHtmlTemplate.Replace(@"//XXXFieldsHtmlXXX".ToString(), strinWithFields);
            CreateCsHtmlTemplate = CreateCsHtmlTemplate.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            CreateCsHtmlTemplate = CreateCsHtmlTemplate.Replace("XXXEntityLowerPluralXXX", txtCamelPlural.Text);
            CreateCsHtmlTemplate = CreateCsHtmlTemplate.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            CreateCsHtmlTemplate = CreateCsHtmlTemplate.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            CreateCsHtmlTemplate += "\n";


            string UpdateCsHtmlTemplate = UsingTemplates.CsHtmlCreator.EditModalCsHtml;
            UpdateCsHtmlTemplate = UpdateCsHtmlTemplate.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            UpdateCsHtmlTemplate = UpdateCsHtmlTemplate.Replace("XXXEntityLowerPluralXXX", txtCamelPlural.Text);
            UpdateCsHtmlTemplate = UpdateCsHtmlTemplate.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            UpdateCsHtmlTemplate = UpdateCsHtmlTemplate.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            UpdateCsHtmlTemplate += "\n";

            rtbIndexCshtmlGenerator.Clear();
            rtbIndexCshtmlGenerator.AppendText(IndexCsHtmlTemplate);
            rtbUpdateCsHtmlGenerator.Clear();
            rtbUpdateCsHtmlGenerator.AppendText(UpdateCsHtmlTemplate);
            rtbCreateCsHtmlGenerator.Clear();
            rtbCreateCsHtmlGenerator.AppendText(CreateCsHtmlTemplate);
        }

        private void GenerateJsFiles(List<KeyValuePair<string, string>> fielList, List<string> fielListForIndexJs)
        {
            ViewDirectory = ViewsDirectory + txtCamelPlural.Text;
            fileNameIndexJs = ViewDirectory + @"\index.js";
            fileNameEditJs = ViewDirectory + @"\editModal.js";
            fileNameCreateJs = ViewDirectory + @"\createModal.js";

            string IndexjsTemplate = UsingTemplates.JSCreator.IndexJS;
            var strinWithFields = string.Join("", fielListForIndexJs.ToArray());
            IndexjsTemplate = IndexjsTemplate.Replace(@"//XXXEntityFieldsJSXXX".ToString(), strinWithFields);
            IndexjsTemplate = IndexjsTemplate.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            IndexjsTemplate = IndexjsTemplate.Replace("XXXEntityLowerPluralXXX", txtCamelPlural.Text);
            IndexjsTemplate = IndexjsTemplate.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            IndexjsTemplate = IndexjsTemplate.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            IndexjsTemplate += "\n";



            string CreateJsTemplate = UsingTemplates.JSCreator.CreateJS;
            CreateJsTemplate = CreateJsTemplate.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            CreateJsTemplate = CreateJsTemplate.Replace("XXXEntityLowerPluralXXX", txtCamelPlural.Text);
            CreateJsTemplate = CreateJsTemplate.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            CreateJsTemplate = CreateJsTemplate.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            CreateJsTemplate += "\n";


            string UpdateJsTemplate = UsingTemplates.JSCreator.UpdateJS;
            UpdateJsTemplate = UpdateJsTemplate.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            UpdateJsTemplate = UpdateJsTemplate.Replace("XXXEntityLowerPluralXXX", txtCamelPlural.Text);
            UpdateJsTemplate = UpdateJsTemplate.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            UpdateJsTemplate = UpdateJsTemplate.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            UpdateJsTemplate += "\n";


            rtbIndexJsGenerator.Clear();
            rtbIndexJsGenerator.AppendText(IndexjsTemplate);
            rtbCreateJsGenerator.Clear();
            rtbCreateJsGenerator.AppendText(IndexjsTemplate);
            rtbUpdateJsGenerator.Clear();
            rtbUpdateJsGenerator.AppendText(IndexjsTemplate);
        }

        private List<string> GenerateFielListForIndexJs(List<KeyValuePair<string, string>> fielList)
        {
            List<string> fieldListForIndexJs = new List<string>();
            foreach (var item in fielList)
            {
                String newField = "";
                newField += "                    {\n";
                newField += "                    name: app.localize('" + item.Value + "'),\n";
                newField += "                    field: '" + item.Value.Substring(0, 1).ToLower() + item.Value.Substring(1) + "',\n";
                newField += "                    minWidth: 125'\n";
                newField += "                    },\n";
                fieldListForIndexJs.Add(newField);
            }
            return fieldListForIndexJs;
        }

        private List<string> GenerateFielListForCreateCsHtml(List<KeyValuePair<string, string>> fielList)
        {
            List<string> fieldListForIndexCsHtml = new List<string>();
            var FieldStringTemplate = UsingTemplates.CsHtmlCreator.FieldString;
            FieldStringTemplate = FieldStringTemplate.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            FieldStringTemplate = FieldStringTemplate.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            FieldStringTemplate = FieldStringTemplate.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            FieldStringTemplate = FieldStringTemplate.Replace("XXXEntityLowerPluralXXX", txtCamelPlural.Text);
            FieldStringTemplate += "\n";

            var FieldNumberTemplate = UsingTemplates.CsHtmlCreator.FieldNumber;
            FieldNumberTemplate = FieldNumberTemplate.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            FieldNumberTemplate = FieldNumberTemplate.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            FieldNumberTemplate = FieldNumberTemplate.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            FieldNumberTemplate = FieldNumberTemplate.Replace("XXXEntityLowerPluralXXX", txtCamelPlural.Text);
            FieldNumberTemplate += "\n";


            foreach (var item in fielList)
            {
                string isRequired = "required";
                string nameCamel = item.Value.Substring(0, 1).ToLower() + item.Value.Substring(1);
                String newField = "";
                if (item.Key == "String" || item.Key == "string")
                    newField = FieldStringTemplate.Replace("XXXFieldNameCamelXXX", nameCamel);

                if (item.Key == "int" || item.Key == "long" || item.Key == "decimal" || item.Key == "Decimal" || item.Key == "double"
                    || item.Key == "Double" || item.Key == "Integer" || item.Key == "Int64" || item.Key == "Int32" || item.Key == "Int16")
                    newField = FieldNumberTemplate.Replace("XXXFieldNameCamelXXX", nameCamel);

                newField = newField.Replace("XXXFieldNameCapitalXXX", item.Value);
                newField = newField.Replace("XXXrequiredXXX", isRequired);
                fieldListForIndexCsHtml.Add(newField);
            }
            return fieldListForIndexCsHtml;
        }

        public static string fileNameEntityDto = "ListDto.cs";
        public static string fileNameCreateDto = "CreateDto.cs";
        public static string fileNameUpdateDto = "UpdateDto.cs";

        private List<KeyValuePair<String, String>> GenerateDtos(string[] loadedClass)
        {
            List<KeyValuePair<String, String>> fieldList = new List<KeyValuePair<string, string>>();
            DtosPathTemp = DtosPath + txtCapitalPlural.Text + @"\Dto\";

            fileNameEntityDto = DtosPathTemp + txtCapitalPlural.Text + "Dto.cs";
            fileNameCreateDto = DtosPathTemp + txtCapitalPlural.Text + "CreateDto.cs";
            fileNameUpdateDto = DtosPathTemp + txtCapitalPlural.Text + "UpdateDto.cs";

            List<string> allowedtypes = new List<string>() { "int", "string", "String","Integer","DateTime","Date","Decimal","float","double","Double","long","int32",
                "int16","int64","bool","Boolean" };

            string classListDto = DtosCreator.EntityDto_JustStartLines;
            classListDto = classListDto.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            classListDto = classListDto.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            classListDto = classListDto.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            classListDto += "\n";


            string classUpdateDto = DtosCreator.UpdateDto_JustStartLines;
            classUpdateDto = classUpdateDto.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            classUpdateDto = classUpdateDto.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            classUpdateDto = classUpdateDto.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            classUpdateDto = classUpdateDto.Replace("XXXEntityLowerPluralXXX", txtCamelPlural.Text);
            classUpdateDto += "\n";


            string classCreateDto = DtosCreator.CreateDto_JustStartLines;
            classCreateDto = classCreateDto.Replace("XXXEntitySingularXXX", txtCapitalSingular.Text);
            classCreateDto = classCreateDto.Replace("XXXEntityPluralXXX", txtCapitalPlural.Text);
            classCreateDto = classCreateDto.Replace("XXXEntityLowerSingularXXX", txtCamelSingular.Text);
            classCreateDto = classCreateDto.Replace("XXXEntityLowerPluralXXX", txtCamelPlural.Text);
            classCreateDto += "\n";


            classListDto += "              public int Id {get;set;} \n";
            classUpdateDto += "              public int Id {get;set;} \n";

            for (int i = 0; i < loadedClass.Length; i++)
            {
                var lineX = loadedClass[i];
                if (!lineX.Contains("public"))
                    continue;

                if (lineX.Contains("class"))
                    continue;

                if (lineX.Contains("("))
                    continue;



                var type = GetTypeString(lineX);
                var property = GetPropertyString(lineX);


                if (!allowedtypes.Contains(type))
                    continue;

                var field = new KeyValuePair<string, string>(type, property);
                fieldList.Add(field);
                classListDto += "              public " + type + " " + property + " {get;set;} \n";
                classUpdateDto += "              public " + type + " " + property + " {get;set;} \n";
                classCreateDto += "              public " + type + " " + property + " {get;set;} \n";
            }

            classListDto += "              public bool IsActive {get;set;} \n";
            classListDto += "              public DateTime CreationTime {get;set;} \n";
            classListDto += "              public long? CreatorUserId {get;set;} \n";


            classCreateDto += "              public bool IsActive {get;set;} \n";
            classCreateDto += "              public DateTime CreationTime {get;set;} \n";
            classCreateDto += "              public long? CreatorUserId {get;set;} \n";


            classUpdateDto += "              public bool IsActive {get;set;} \n";
            classUpdateDto += "              public DateTime CreationTime {get;set;} \n";
            classUpdateDto += "              public long? CreatorUserId {get;set;} \n";

            classListDto += "\n         }\n}";
            classUpdateDto += "\n         }\n}";
            classCreateDto += "\n         }\n}";

            rtbEntityDtoGenerator.Clear();
            rtbEntityDtoGenerator.AppendText(classListDto);
            rtbEntityUpdateDtoGenerator.Clear();
            rtbEntityUpdateDtoGenerator.AppendText(classUpdateDto);
            rtbEntityCreateDtoGenerator.Clear();
            rtbEntityCreateDtoGenerator.AppendText(classCreateDto);

            return fieldList;
        }

        public string GetTypeString(String Line)
        {
            string currentType = "";

            var index = Line.IndexOf("public");
            var indexType = Line.IndexOf(" ", index + 1);
            var indexProperty = Line.IndexOf(" ", indexType + 1);
            currentType = Line.Substring(indexType + 1, indexProperty - indexType - 1);
            return currentType;
        }

        public string GetPropertyString(String Line)
        {
            string property = "";

            var index = Line.IndexOf("public");
            var indexType = Line.IndexOf(" ", index + 1);
            var indexProperty = Line.IndexOf(" ", indexType + 1);
            var indexPropertyEnd = Line.IndexOf(" ", indexProperty + 1);
            property = Line.Substring(indexProperty + 1, indexPropertyEnd - indexProperty - 1);
            return property;
        }


        private void btnSaveOnDisk_Click(object sender, EventArgs e)
        {
            string PathDirectoryAppService = pathDirectoryApp + txtCapitalPlural.Text;
            if (!System.IO.Directory.Exists(PathDirectoryAppService))
                System.IO.Directory.CreateDirectory(PathDirectoryAppService);

            if (!System.IO.Directory.Exists(DtosPathTemp))
                System.IO.Directory.CreateDirectory(DtosPathTemp);

            if (!System.IO.Directory.Exists(ViewDirectory))
                System.IO.Directory.CreateDirectory(ViewDirectory);

            string fileNameIAppService = PathDirectoryAppService + @"\I" + txtCapitalSingular.Text + "AppService.cs";
            string fileNameAppService = PathDirectoryAppService + @"\" + txtCapitalSingular.Text + "AppService.cs";

            //Se genera el archivo de la Interface
            createSpecificFileOnDisk(fileNameIAppService, rtbIAppSericeGenerator);

            //Se general el archivo del servicio
            createSpecificFileOnDisk(fileNameAppService, rtbAppSericeGenerator);

            createSpecificFileOnDisk(fileNameEntityDto, rtbEntityDtoGenerator);
            createSpecificFileOnDisk(fileNameCreateDto, rtbEntityCreateDtoGenerator);
            createSpecificFileOnDisk(fileNameUpdateDto, rtbEntityUpdateDtoGenerator);

            createSpecificFileOnDisk(fileNameIndexJs, rtbIndexJsGenerator);
            createSpecificFileOnDisk(fileNameCreateJs, rtbCreateJsGenerator);
            createSpecificFileOnDisk(fileNameEditJs, rtbUpdateJsGenerator);

            createSpecificFileOnDisk(fileNameIndexCsHtml, rtbIndexCshtmlGenerator);
            createSpecificFileOnDisk(fileNameEditCsHtml, rtbUpdateCsHtmlGenerator);
            createSpecificFileOnDisk(fileNameCreateCsHtml, rtbCreateCsHtmlGenerator);

        }

        private void createSpecificFileOnDisk(string fileName, RichTextBox textContainer)
        {
            if (System.IO.File.Exists(fileName))
                System.IO.File.Delete(fileName);
            System.IO.File.AppendAllText(fileName, textContainer.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Utils.ReadAllTemplastes();
        }

        private void RichTextBoxGeneral_MouseClick(object sender, MouseEventArgs e)
        {
            frmVisor vs = new frmVisor((RichTextBox)sender);
            vs.ShowDialog();
        }
    }
}

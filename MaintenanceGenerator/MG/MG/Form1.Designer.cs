﻿namespace MG
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenerate = new System.Windows.Forms.Button();
            this.txtCamelSingular = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCamelPlural = new System.Windows.Forms.TextBox();
            this.txtCapitalPlural = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rtbIAppSericeGenerator = new System.Windows.Forms.RichTextBox();
            this.txtCapitalSingular = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSaveOnDisk = new System.Windows.Forms.Button();
            this.rtbAppSericeGenerator = new System.Windows.Forms.RichTextBox();
            this.rtbEntityDtoGenerator = new System.Windows.Forms.RichTextBox();
            this.rtbEntityUpdateDtoGenerator = new System.Windows.Forms.RichTextBox();
            this.rtbEntityCreateDtoGenerator = new System.Windows.Forms.RichTextBox();
            this.rtbIndexJsGenerator = new System.Windows.Forms.RichTextBox();
            this.rtbIndexCshtmlGenerator = new System.Windows.Forms.RichTextBox();
            this.rtbCreateJsGenerator = new System.Windows.Forms.RichTextBox();
            this.rtbCreateCsHtmlGenerator = new System.Windows.Forms.RichTextBox();
            this.rtbUpdateJsGenerator = new System.Windows.Forms.RichTextBox();
            this.rtbUpdateCsHtmlGenerator = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(182, 121);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(158, 23);
            this.btnGenerate.TabIndex = 0;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // txtCamelSingular
            // 
            this.txtCamelSingular.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txtCamelSingular.Location = new System.Drawing.Point(182, 17);
            this.txtCamelSingular.Name = "txtCamelSingular";
            this.txtCamelSingular.Size = new System.Drawing.Size(158, 20);
            this.txtCamelSingular.TabIndex = 1;
            this.txtCamelSingular.Text = "coursex";
            this.txtCamelSingular.TextChanged += new System.EventHandler(this.txtEntityNameSingular_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Entity name Singular";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Entity name plural camell case";
            // 
            // txtCamelPlural
            // 
            this.txtCamelPlural.Location = new System.Drawing.Point(182, 69);
            this.txtCamelPlural.Name = "txtCamelPlural";
            this.txtCamelPlural.Size = new System.Drawing.Size(158, 20);
            this.txtCamelPlural.TabIndex = 1;
            // 
            // txtCapitalPlural
            // 
            this.txtCapitalPlural.Location = new System.Drawing.Point(182, 95);
            this.txtCapitalPlural.Name = "txtCapitalPlural";
            this.txtCapitalPlural.Size = new System.Drawing.Size(158, 20);
            this.txtCapitalPlural.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Entity name plural capital case";
            // 
            // rtbIAppSericeGenerator
            // 
            this.rtbIAppSericeGenerator.Location = new System.Drawing.Point(12, 150);
            this.rtbIAppSericeGenerator.Name = "rtbIAppSericeGenerator";
            this.rtbIAppSericeGenerator.Size = new System.Drawing.Size(588, 68);
            this.rtbIAppSericeGenerator.TabIndex = 3;
            this.rtbIAppSericeGenerator.Text = "";
            this.rtbIAppSericeGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // txtCapitalSingular
            // 
            this.txtCapitalSingular.Location = new System.Drawing.Point(182, 43);
            this.txtCapitalSingular.Name = "txtCapitalSingular";
            this.txtCapitalSingular.Size = new System.Drawing.Size(158, 20);
            this.txtCapitalSingular.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Entity name singular capital case";
            // 
            // btnSaveOnDisk
            // 
            this.btnSaveOnDisk.Location = new System.Drawing.Point(346, 17);
            this.btnSaveOnDisk.Name = "btnSaveOnDisk";
            this.btnSaveOnDisk.Size = new System.Drawing.Size(75, 127);
            this.btnSaveOnDisk.TabIndex = 4;
            this.btnSaveOnDisk.Text = "Save";
            this.btnSaveOnDisk.UseVisualStyleBackColor = true;
            this.btnSaveOnDisk.Click += new System.EventHandler(this.btnSaveOnDisk_Click);
            // 
            // rtbAppSericeGenerator
            // 
            this.rtbAppSericeGenerator.Location = new System.Drawing.Point(12, 224);
            this.rtbAppSericeGenerator.Name = "rtbAppSericeGenerator";
            this.rtbAppSericeGenerator.Size = new System.Drawing.Size(588, 62);
            this.rtbAppSericeGenerator.TabIndex = 3;
            this.rtbAppSericeGenerator.Text = "";
            this.rtbAppSericeGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // rtbEntityDtoGenerator
            // 
            this.rtbEntityDtoGenerator.Location = new System.Drawing.Point(12, 292);
            this.rtbEntityDtoGenerator.Name = "rtbEntityDtoGenerator";
            this.rtbEntityDtoGenerator.Size = new System.Drawing.Size(588, 70);
            this.rtbEntityDtoGenerator.TabIndex = 3;
            this.rtbEntityDtoGenerator.Text = "";
            this.rtbEntityDtoGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // rtbEntityUpdateDtoGenerator
            // 
            this.rtbEntityUpdateDtoGenerator.Location = new System.Drawing.Point(12, 368);
            this.rtbEntityUpdateDtoGenerator.Name = "rtbEntityUpdateDtoGenerator";
            this.rtbEntityUpdateDtoGenerator.Size = new System.Drawing.Size(588, 75);
            this.rtbEntityUpdateDtoGenerator.TabIndex = 3;
            this.rtbEntityUpdateDtoGenerator.Text = "";
            this.rtbEntityUpdateDtoGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // rtbEntityCreateDtoGenerator
            // 
            this.rtbEntityCreateDtoGenerator.Location = new System.Drawing.Point(12, 449);
            this.rtbEntityCreateDtoGenerator.Name = "rtbEntityCreateDtoGenerator";
            this.rtbEntityCreateDtoGenerator.Size = new System.Drawing.Size(588, 72);
            this.rtbEntityCreateDtoGenerator.TabIndex = 3;
            this.rtbEntityCreateDtoGenerator.Text = "";
            this.rtbEntityCreateDtoGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // rtbIndexJsGenerator
            // 
            this.rtbIndexJsGenerator.Location = new System.Drawing.Point(12, 527);
            this.rtbIndexJsGenerator.Name = "rtbIndexJsGenerator";
            this.rtbIndexJsGenerator.Size = new System.Drawing.Size(588, 68);
            this.rtbIndexJsGenerator.TabIndex = 3;
            this.rtbIndexJsGenerator.Text = "";
            this.rtbIndexJsGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // rtbIndexCshtmlGenerator
            // 
            this.rtbIndexCshtmlGenerator.Location = new System.Drawing.Point(606, 150);
            this.rtbIndexCshtmlGenerator.Name = "rtbIndexCshtmlGenerator";
            this.rtbIndexCshtmlGenerator.Size = new System.Drawing.Size(588, 68);
            this.rtbIndexCshtmlGenerator.TabIndex = 3;
            this.rtbIndexCshtmlGenerator.Text = "";
            this.rtbIndexCshtmlGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // rtbCreateJsGenerator
            // 
            this.rtbCreateJsGenerator.Location = new System.Drawing.Point(606, 224);
            this.rtbCreateJsGenerator.Name = "rtbCreateJsGenerator";
            this.rtbCreateJsGenerator.Size = new System.Drawing.Size(588, 62);
            this.rtbCreateJsGenerator.TabIndex = 3;
            this.rtbCreateJsGenerator.Text = "";
            this.rtbCreateJsGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // rtbCreateCsHtmlGenerator
            // 
            this.rtbCreateCsHtmlGenerator.Location = new System.Drawing.Point(606, 292);
            this.rtbCreateCsHtmlGenerator.Name = "rtbCreateCsHtmlGenerator";
            this.rtbCreateCsHtmlGenerator.Size = new System.Drawing.Size(588, 68);
            this.rtbCreateCsHtmlGenerator.TabIndex = 3;
            this.rtbCreateCsHtmlGenerator.Text = "";
            this.rtbCreateCsHtmlGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // rtbUpdateJsGenerator
            // 
            this.rtbUpdateJsGenerator.Location = new System.Drawing.Point(606, 366);
            this.rtbUpdateJsGenerator.Name = "rtbUpdateJsGenerator";
            this.rtbUpdateJsGenerator.Size = new System.Drawing.Size(588, 77);
            this.rtbUpdateJsGenerator.TabIndex = 3;
            this.rtbUpdateJsGenerator.Text = "";
            this.rtbUpdateJsGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // rtbUpdateCsHtmlGenerator
            // 
            this.rtbUpdateCsHtmlGenerator.Location = new System.Drawing.Point(606, 449);
            this.rtbUpdateCsHtmlGenerator.Name = "rtbUpdateCsHtmlGenerator";
            this.rtbUpdateCsHtmlGenerator.Size = new System.Drawing.Size(588, 72);
            this.rtbUpdateCsHtmlGenerator.TabIndex = 3;
            this.rtbUpdateCsHtmlGenerator.Text = "";
            this.rtbUpdateCsHtmlGenerator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBoxGeneral_MouseClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1233, 601);
            this.Controls.Add(this.btnSaveOnDisk);
            this.Controls.Add(this.rtbUpdateCsHtmlGenerator);
            this.Controls.Add(this.rtbUpdateJsGenerator);
            this.Controls.Add(this.rtbEntityCreateDtoGenerator);
            this.Controls.Add(this.rtbCreateCsHtmlGenerator);
            this.Controls.Add(this.rtbEntityUpdateDtoGenerator);
            this.Controls.Add(this.rtbCreateJsGenerator);
            this.Controls.Add(this.rtbEntityDtoGenerator);
            this.Controls.Add(this.rtbIndexCshtmlGenerator);
            this.Controls.Add(this.rtbIndexJsGenerator);
            this.Controls.Add(this.rtbAppSericeGenerator);
            this.Controls.Add(this.rtbIAppSericeGenerator);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCapitalSingular);
            this.Controls.Add(this.txtCapitalPlural);
            this.Controls.Add(this.txtCamelPlural);
            this.Controls.Add(this.txtCamelSingular);
            this.Controls.Add(this.btnGenerate);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.TextBox txtCamelSingular;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCamelPlural;
        private System.Windows.Forms.TextBox txtCapitalPlural;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rtbIAppSericeGenerator;
        private System.Windows.Forms.TextBox txtCapitalSingular;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSaveOnDisk;
        private System.Windows.Forms.RichTextBox rtbAppSericeGenerator;
        private System.Windows.Forms.RichTextBox rtbEntityDtoGenerator;
        private System.Windows.Forms.RichTextBox rtbEntityUpdateDtoGenerator;
        private System.Windows.Forms.RichTextBox rtbEntityCreateDtoGenerator;
        private System.Windows.Forms.RichTextBox rtbIndexJsGenerator;
        private System.Windows.Forms.RichTextBox rtbIndexCshtmlGenerator;
        private System.Windows.Forms.RichTextBox rtbCreateJsGenerator;
        private System.Windows.Forms.RichTextBox rtbCreateCsHtmlGenerator;
        private System.Windows.Forms.RichTextBox rtbUpdateJsGenerator;
        private System.Windows.Forms.RichTextBox rtbUpdateCsHtmlGenerator;
    }
}


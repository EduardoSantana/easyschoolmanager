﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace MG
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        public static string ProjectName = "EasySchoolManager";

        public static string AuthorizationProviderFile = "";
        public static string AppJsFile = "";
        public static string NavigationProviderFile = "";
        public static string PermissionNamesFile = "";


        public static string AuthorizationProviderFileMenuTemplate = "";
        public static string AppJsFileMenuTemplate = "";
        public static string NavigationProviderFilaMenuTemplate = "";
        private string PermissionNamesFileMenuTemplate = "";

        public static string AuthorizationDirectory { get { return ProjectName + @".Core\Authorization"; } }
        public static string ViewsDirectory { get { return ProjectName + @".Web\App\Main\views\"; } }
        public static string WebMainDirectory { get { return ProjectName + @".Web\App\Main\"; } }
        public static string NavBarJSFile { get; set; }
        public static string WebDirectory { get { return ProjectName + @".Web\"; } }
        private string ViewDirectory = "";
        private string fileNameIndexJs = "";
        private string fileNameEditJs = "";
        private string fileNameCreateJs = "";

        public static string ClassesPath { get { return ProjectName + @".Core\Models\"; } }
        public static string DtosPath { get { return ProjectName + @".Application\"; } }
        public static string pathDirectoryApp { get { return ProjectName + @".Application\"; } }

        public string XXXServiceCallsXXX = "XXXServiceCallsXXX";

        public static string XXXRepositoryConstructorListXXX = "XXXRepositoryConstructorListXXX";
        public static string XXXRepositorySettingListXXX = "XXXRepositorySettingListXXX";
        public static string XXXRepositoryDeclarationListXXX = "XXXRepositoryDeclarationListXXX";

        public string XXXFirstFieldNameXXX = "XXXFirstFieldNameXXX";

        public string XXXFieldNameCapitalSingularXXX = "XXXFieldNameCapitalSingularXXX";
        public string XXXFieldNameCamelPluralXXX = "XXXFieldNameCamelPluralXXX";

        public static string ClassPath = "";
        public static string DtosPathTemp;
        private string fileNameIndexCsHtml = "";
        private string fileNameEditCsHtml = "";
        private string fileNameCreateCsHtml = "";

        private string XXXInsertPermissionNamesXXX = "XXXInsertPermissionNamesXXX";
        private string XXXInsertNavitationProviderXXX = "XXXInsertNavitationProviderXXX";
        private string XXXInsertAuthorizationProviderXXX = "XXXInsertAuthorizationProviderXXX";
        private string XXXInsertAppJsMenuXXX = "XXXInsertAppJsMenuXXX";
        private string XXXFieldKeyXXX = "XXXFieldKeyXXX";

        private string XXXEntityPluralXXX = "XXXEntityPluralXXX";
        private string XXXEntitySingularXXX = "XXXEntitySingularXXX";
        private string XXXEntityLowerSingularXXX = "XXXEntityLowerSingularXXX";
        private string XXXEntityLowerPluralXXX = "XXXEntityLowerPluralXXX";
        private string XXXInsertMenuItemXXX = "XXXInsertMenuItemXXX";

        private string XXXServicesUsedDeclarationXXX = "XXXServicesUsedDeclarationXXX";
        private string XXXServicesUsedSettingXXX = "XXXServicesUsedSettingXXX";

        private string XXXFieldNameCamelXXX = "XXXFieldNameCamelXXX";
        private string XXXFieldNameCapitalXXX = "XXXFieldNameCapitalXXX";
        private string XXXFieldNameCapitalPluralXXX = "XXXFieldNameCapitalPluralXXX";
        private string XXXFieldNameCamelSingularXXX = "XXXFieldNameCamelSingularXXX";

        private string XXXrequiredXXX = "XXXrequiredXXX";

        private void txtEntityNameSingular_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string camell = "";
                var lower = txtCamelSingular.Text;
                if (lower.ToLower().Substring(lower.Length - 1, 1) == "s")
                    camell = lower + "es";
                else if (lower.ToLower().Substring(lower.Length - 1, 1) == "y" &&
                    (
                        (lower.ToLower().Substring(lower.Length - 2, 1) != "a") &&
                    (lower.ToLower().Substring(lower.Length - 2, 1) != "e") &&
                    (lower.ToLower().Substring(lower.Length - 2, 1) != "i") &&
                    (lower.ToLower().Substring(lower.Length - 2, 1) != "o") &&
                    (lower.ToLower().Substring(lower.Length - 2, 1) != "u")

                    )
                    )
                {
                    camell = lower.Substring(0, lower.Length - 1) + "ies";
                }
                else
                    camell = lower + "s";


                var capital = camell.Substring(0, 1).ToUpper() + camell.Substring(1);
                var capitalSingular = lower.Substring(0, 1).ToUpper() + lower.Substring(1);

                txtCamelPlural.Text = camell;
                txtCapitalPlural.Text = capital;
                txtCapitalSingular.Text = capitalSingular;

                btnGenerate_Click(null, null);
            }
            catch (Exception err)
            { }
        }

        List<KeyValuePair<String, String>> FielList = new List<KeyValuePair<string, string>>();

        public bool ExistInFielList(String field)
        {
            return FielList.Any(x => x.Value == field);
        }

        public bool ExistInRelatedFielList(String field)
        {
            return fieldsRelatedEntity.Any(x => x.Value == field);
        }


        private void btnGenerate_Click(object sender, EventArgs e)
        {
            cleanRiches();

            var stringDataIAppService = IAppServiceCreator.TemplateIAppService.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            stringDataIAppService = stringDataIAppService.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            rtbIAppSericeGenerator.Clear();
            rtbIAppSericeGenerator.AppendText(stringDataIAppService);


            List<KeyValuePair<string, string>> fielListUsed = Utils.getFieldListFromEntity(txtCapitalSingular.Text);
            String StringWitRepositoryDeclarationList = generateListOfRepositoryDeclaration(fielListUsed);
            String StringWitRepositoryConstructorList = generateListOfRepositoryContructor(fielListUsed);
            String StringWitRepositorySettingList = generateListOfRepositorySetting(fielListUsed);

            var stringDataAppService = AppServiceCreator.TemplateAppService.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            stringDataAppService = stringDataAppService.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            stringDataAppService = stringDataAppService.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            stringDataAppService = stringDataAppService.Replace(XXXRepositoryDeclarationListXXX, StringWitRepositoryDeclarationList);
            stringDataAppService = stringDataAppService.Replace(XXXRepositoryConstructorListXXX, StringWitRepositoryConstructorList);
            stringDataAppService = stringDataAppService.Replace(XXXRepositorySettingListXXX, StringWitRepositorySettingList);
            rtbAppSericeGenerator.Clear();
            rtbAppSericeGenerator.AppendText(stringDataAppService);

            ClassPath = ClassesPath + txtCapitalPlural.Text + ".cs";

            var LoadedClass = System.IO.File.ReadAllLines(ClassPath);

            String CallRelatedEntities = "";
            FielList = GenerateDtos(LoadedClass);
            List<string> FielListForIndexJs = GenerateFielListForIndexJs(FielList, out CallRelatedEntities);
            List<string> FielListForCreateCsHtml = GenerateFielListForCreateCsHtml(FielList);

            GenerateJsFiles(FielList, FielListForIndexJs, CallRelatedEntities);

            GenerateCsHtmlFiles(FielListForCreateCsHtml);

            GenerateNavigationYAuthorizationEditions();

        }

        private void GenerateNavigationYAuthorizationEditions()
        {


            var MenuSideBarNavTemplate = ReplaceNameEntitiesAllFormat(UsingTemplates.NavigationAutorizationCreator.MenuSideBarNavTemplate);
            var PermisionNameTemplate = ReplaceNameEntitiesAllFormat(UsingTemplates.NavigationAutorizationCreator.PermissionNamesTemplate);
            var NavigationProviderTemplate = ReplaceNameEntitiesAllFormat(UsingTemplates.NavigationAutorizationCreator.NavigationProviderTemplate);
            var AuthorizationProviderTemplate = ReplaceNameEntitiesAllFormat(UsingTemplates.NavigationAutorizationCreator.AuthorizationProviderTemplate);
            var AppJsTemplate = ReplaceNameEntitiesAllFormat(UsingTemplates.NavigationAutorizationCreator.AppJsTemplate);

            var permissionNameOriginal = UsingTemplates.NavigationAutorizationCreator.PermissionNamesOriginal
                .Replace(XXXInsertPermissionNamesXXX, XXXInsertPermissionNamesXXX + "\n\n" + PermisionNameTemplate);

            var authorizationProviderOriginal = UsingTemplates.NavigationAutorizationCreator.AuthorizationProviderOriginal
                .Replace(XXXInsertAuthorizationProviderXXX, XXXInsertAuthorizationProviderXXX + "\n\n" + AuthorizationProviderTemplate);

            var appJsOriginal = UsingTemplates.NavigationAutorizationCreator.AppJsOriginal
                .Replace(XXXInsertAppJsMenuXXX, XXXInsertAppJsMenuXXX + "\n\n" + AppJsTemplate).Replace("XXXFaIconXXX",txtFaIcon.Text);

            var navigationProviderOriginal = UsingTemplates.NavigationAutorizationCreator.NavigationProviderOriginal
                .Replace(XXXInsertNavitationProviderXXX, XXXInsertNavitationProviderXXX + "\n\n" + NavigationProviderTemplate);

            var MenuSideBarNavOriginal = UsingTemplates.NavigationAutorizationCreator.MenuSideBarNavOriginal
                .Replace(XXXInsertMenuItemXXX, XXXInsertMenuItemXXX + "\n\n" + MenuSideBarNavTemplate);

            rtbAppJsGenerator.Clear();
            rtbAutorizationProviderGenerator.Clear();
            rtbPermissionNameGenerator.Clear();
            rtbNavigationProviderGenerator.Clear();
            rtbMenuNavBarGenerator.Clear();

            rtbAppJsGenerator.Text = appJsOriginal;
            rtbAutorizationProviderGenerator.Text = authorizationProviderOriginal;
            rtbPermissionNameGenerator.Text = permissionNameOriginal;
            rtbNavigationProviderGenerator.Text = navigationProviderOriginal;
            rtbMenuNavBarGenerator.Text = MenuSideBarNavOriginal;

        }

        private void cleanRiches()
        {
            rtbIAppSericeGenerator.Clear();
            rtbAppSericeGenerator.Clear();
            rtbEntityCreateDtoGenerator.Clear();
            rtbEntityDtoGenerator.Clear();
            rtbEntityUpdateDtoGenerator.Clear();
            rtbIndexJsGenerator.Clear();
            rtbIndexCshtmlGenerator.Clear();
            rtbEntityDtoGenerator.Clear();
            rtbCreateJsGenerator.Clear();
            rtbCreateCsHtmlGenerator.Clear();
            rtbUpdateJsGenerator.Clear();
            rtbUpdateCsHtmlGenerator.Clear();

            rtbAppJsGenerator.Clear();
            rtbAutorizationProviderGenerator.Clear();
            rtbPermissionNameGenerator.Clear();
            rtbNavigationProviderGenerator.Clear();
            rtbMenuNavBarGenerator.Clear();
        }

        private void GenerateCsHtmlFiles(List<String> fielListForCreateCsHtml)
        {
            fileNameIndexCsHtml = ViewDirectory + @"\index.cshtml";
            fileNameEditCsHtml = ViewDirectory + @"\editModal.cshtml";
            fileNameCreateCsHtml = ViewDirectory + @"\createModal.cshtml";

            string IndexCsHtmlTemplate = UsingTemplates.CsHtmlCreator.IndexCsHtml;
            IndexCsHtmlTemplate = IndexCsHtmlTemplate.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            IndexCsHtmlTemplate = IndexCsHtmlTemplate.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            IndexCsHtmlTemplate = IndexCsHtmlTemplate.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            IndexCsHtmlTemplate = IndexCsHtmlTemplate.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            IndexCsHtmlTemplate += "\n";

            string CreateCsHtmlTemplate = UsingTemplates.CsHtmlCreator.CreateModalCsHtml;
            var strinWithFields = string.Join("", fielListForCreateCsHtml.ToArray());
            CreateCsHtmlTemplate = CreateCsHtmlTemplate.Replace(@"//XXXFieldsHtmlXXX".ToString(), strinWithFields);
            CreateCsHtmlTemplate = CreateCsHtmlTemplate.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            CreateCsHtmlTemplate = CreateCsHtmlTemplate.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            CreateCsHtmlTemplate = CreateCsHtmlTemplate.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            CreateCsHtmlTemplate = CreateCsHtmlTemplate.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            CreateCsHtmlTemplate += "\n";


            string UpdateCsHtmlTemplate = UsingTemplates.CsHtmlCreator.EditModalCsHtml;
            UpdateCsHtmlTemplate = UpdateCsHtmlTemplate.Replace(@"//XXXFieldsHtmlXXX".ToString(), strinWithFields);
            UpdateCsHtmlTemplate = UpdateCsHtmlTemplate.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            UpdateCsHtmlTemplate = UpdateCsHtmlTemplate.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            UpdateCsHtmlTemplate = UpdateCsHtmlTemplate.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            UpdateCsHtmlTemplate = UpdateCsHtmlTemplate.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            UpdateCsHtmlTemplate += "\n";

            rtbIndexCshtmlGenerator.Clear();
            rtbIndexCshtmlGenerator.AppendText(IndexCsHtmlTemplate);
            rtbUpdateCsHtmlGenerator.Clear();
            rtbUpdateCsHtmlGenerator.AppendText(UpdateCsHtmlTemplate);
            rtbCreateCsHtmlGenerator.Clear();
            rtbCreateCsHtmlGenerator.AppendText(CreateCsHtmlTemplate);
        }

        public String ReplaceNameEntitiesAllFormat(String StringTemplate)
        {
            string stringRetorno = StringTemplate;
            stringRetorno = stringRetorno.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            stringRetorno = stringRetorno.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            stringRetorno = stringRetorno.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            stringRetorno = stringRetorno.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            return stringRetorno;
        }

        private void GenerateJsFiles(List<KeyValuePair<string, string>> fielList, List<string> fielListForIndexJs, string CallRelatedEntities)
        {
            ViewDirectory = ViewsDirectory + txtCamelPlural.Text;
            fileNameIndexJs = ViewDirectory + @"\index.js";
            fileNameEditJs = ViewDirectory + @"\editModal.js";
            fileNameCreateJs = ViewDirectory + @"\createModal.js";

            string IndexjsTemplate = UsingTemplates.JSCreator.IndexJS;
            var strinWithFields = string.Join("", fielListForIndexJs.ToArray());
            IndexjsTemplate = IndexjsTemplate.Replace(@"//XXXEntityFieldsJSXXX".ToString(), strinWithFields);
            IndexjsTemplate = IndexjsTemplate.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            IndexjsTemplate = IndexjsTemplate.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            IndexjsTemplate = IndexjsTemplate.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            IndexjsTemplate = IndexjsTemplate.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            IndexjsTemplate = IndexjsTemplate.Replace(XXXFieldNameCapitalXXX, FielList.First().Value.ToString());
            IndexjsTemplate += "\n";

            
            String ServiceDeclaration = generateListOfServiceDeclaration(fielList);
            String ServiceSetting = generateListOfServiceSetting(fielList);
            String ServiceCalls = generateListOfServiceCall(fielList);


            string CreateJsTemplate = UsingTemplates.JSCreator.CreateJS;
            CreateJsTemplate = CreateJsTemplate.Replace(@"//XXXInsertCallRelatedEntitiesXXX".ToString(), CallRelatedEntities);
            CreateJsTemplate = CreateJsTemplate.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            CreateJsTemplate = CreateJsTemplate.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            CreateJsTemplate = CreateJsTemplate.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            CreateJsTemplate = CreateJsTemplate.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            CreateJsTemplate = CreateJsTemplate.Replace(XXXFieldNameCapitalXXX, FielList.First().Value.ToString());
            CreateJsTemplate = CreateJsTemplate.Replace(XXXServicesUsedDeclarationXXX, ServiceDeclaration);
            CreateJsTemplate = CreateJsTemplate.Replace(XXXServicesUsedSettingXXX, ServiceSetting);
            CreateJsTemplate = CreateJsTemplate.Replace(XXXServiceCallsXXX, ServiceCalls);
            CreateJsTemplate += "\n";


            string EditJsTemplate = UsingTemplates.JSCreator.UpdateJS;
            EditJsTemplate = EditJsTemplate.Replace(@"//XXXInsertCallRelatedEntitiesXXX".ToString(), CallRelatedEntities);
            EditJsTemplate = EditJsTemplate.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            EditJsTemplate = EditJsTemplate.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            EditJsTemplate = EditJsTemplate.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            EditJsTemplate = EditJsTemplate.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            EditJsTemplate = EditJsTemplate.Replace(XXXFieldNameCapitalXXX, FielList.First().Value.ToString());
            EditJsTemplate = EditJsTemplate.Replace(XXXServicesUsedDeclarationXXX, ServiceDeclaration);
            EditJsTemplate = EditJsTemplate.Replace(XXXServicesUsedSettingXXX, ServiceSetting);
            EditJsTemplate = EditJsTemplate.Replace(XXXServiceCallsXXX, ServiceCalls);
            EditJsTemplate += "\n";


            rtbIndexJsGenerator.Clear();
            rtbIndexJsGenerator.AppendText(IndexjsTemplate);
            rtbCreateJsGenerator.Clear();
            rtbCreateJsGenerator.AppendText(CreateJsTemplate);
            rtbUpdateJsGenerator.Clear();
            rtbUpdateJsGenerator.AppendText(EditJsTemplate);
        }

        private string generateListOfRepositoryDeclaration(List<KeyValuePair<string, string>> fielList)
        {
            var RelatedEntities = "\n";
            foreach (var item in fielList)
            {
                var comboParameter = GetComboParameter(item.Value);
                if (comboParameter != null)
                {
                    var templateSeletedted = "		    private readonly IRepository<Models.XXXEntityPluralXXX, int> _XXXEntityLowerSingularXXXRepository;";
                    templateSeletedted = templateSeletedted.Replace(XXXEntityPluralXXX, comboParameter.EntityPlural);
                    templateSeletedted = templateSeletedted.Replace(XXXEntityLowerSingularXXX, comboParameter.EntityCamelSingular);

                    RelatedEntities += templateSeletedted + "\n";
                }
            }
            return RelatedEntities;
        }

        private string generateListOfRepositoryContructor(List<KeyValuePair<string, string>> fielList)
        {
            var RelatedEntities = "";
            foreach (var item in fielList)
            {
                var comboParameter = GetComboParameter(item.Value);
                if (comboParameter != null)
                {
                    var templateSeletedted = ",\n            IRepository<Models.XXXEntityPluralXXX, int> XXXEntityLowerSingularXXXRepository";
                    templateSeletedted = templateSeletedted.Replace(XXXEntityPluralXXX, comboParameter.EntityPlural);
                    templateSeletedted = templateSeletedted.Replace(XXXEntityLowerSingularXXX, comboParameter.EntityCamelSingular);

                    RelatedEntities += templateSeletedted + "\n";
                }
            }
            return RelatedEntities;
        }

        private string generateListOfRepositorySetting(List<KeyValuePair<string, string>> fielList)
        {
            var RelatedEntities = "\n";
            foreach (var item in fielList)
            {


                var comboParameter = GetComboParameter(item.Value);
                if (comboParameter != null)
                {
                    var templateSeletedted = "            _XXXEntityLowerSingularXXXRepository = XXXEntityLowerSingularXXXRepository;\n";
                    templateSeletedted = templateSeletedted.Replace(XXXEntityPluralXXX, comboParameter.EntityPlural);
                    templateSeletedted = templateSeletedted.Replace(XXXEntityLowerSingularXXX, comboParameter.EntityCamelSingular);

                    RelatedEntities += templateSeletedted + "\n";
                }
            }
            return RelatedEntities;
        }

        private string generateListOfServiceSetting(List<KeyValuePair<string, string>> fielList)
        {
            var RelatedEntities = "";
            foreach (var item in fielList)
            {
                var comboParameter = GetComboParameter(item.Value);
                if (comboParameter != null)
                {
                    var templateSeletedted = ", XXXEntityLowerSingularXXXService";
                    templateSeletedted = templateSeletedted.Replace(XXXEntityLowerSingularXXX, comboParameter.EntityCamelSingular);
                    RelatedEntities += templateSeletedted + "";
                }
            }
            return RelatedEntities;
        }

        private string generateListOfServiceCall(List<KeyValuePair<string, string>> fielList)
        {
            var RelatedEntities = "";
            foreach (var item in fielList)
            {
                var comboParameter = GetComboParameter(item.Value);
                if (comboParameter != null)
                {
                    var templateSeletedted = "                vm.getXXXEntityPluralXXX();\n";
                    templateSeletedted = templateSeletedted.Replace(XXXEntityPluralXXX, comboParameter.EntityPlural);
                    RelatedEntities += templateSeletedted + "";
                }
            }
            return RelatedEntities;
        }

        private string generateListOfServiceDeclaration(List<KeyValuePair<string, string>> fielList)
        {
            var RelatedEntities = "";
            foreach (var item in fielList)
            {


                var comboParameter = GetComboParameter(item.Value);
                if (comboParameter != null)
                {
                    var templateSeletedted = "'abp.services.app.XXXEntityLowerSingularXXX',";
                    templateSeletedted = templateSeletedted.Replace(XXXEntityLowerSingularXXX, comboParameter.EntityCamelSingular);

                    RelatedEntities += templateSeletedted + "";
                }
            }
            return RelatedEntities;
        }

        public ComboParameter GetComboParameter(String FieldNameValue)
        {
            ComboParameter cp = null;
            foreach (var item in lbxCombos.Items)
            {
                var cpTemp = (ComboParameter)item;

                if (cpTemp.FieldNameValue == FieldNameValue)
                {
                    cp = cpTemp;
                    break;
                }
            }
            return cp;
        }

        private List<string> GenerateFielListForIndexJs(List<KeyValuePair<string, string>> fielList, out String RelatedEntities)
        {
            RelatedEntities = "//XXXInsertCallRelatedEntitiesXXX\n\n";
            List<string> fieldListForIndexJs = new List<string>();
            foreach (var item in fielList)
            {


                String newField = "";
                newField += "                    {\n";
                newField += "                    name: App.localize('" + item.Value + "'),\n";
                newField += "                    field: '" + item.Value.Substring(0, 1).ToLower() + item.Value.Substring(1) + "',\n";
                newField += "                    minWidth: 125\n";
                newField += "                    },\n";
                fieldListForIndexJs.Add(newField);

                var comboParameter = GetComboParameter(item.Value);
                if (comboParameter != null)
                {
                    var templateSeletedted = UsingTemplates.JSCreator.FieldRelatedJS;
                    templateSeletedted = templateSeletedted.Replace(XXXFieldNameCamelPluralXXX, comboParameter.EntityCamelPlural);
                    templateSeletedted = templateSeletedted.Replace(XXXFieldNameCapitalPluralXXX, comboParameter.EntityPlural);
                    templateSeletedted = templateSeletedted.Replace(XXXEntityLowerSingularXXX, comboParameter.EntityCamelSingular);

                    RelatedEntities += templateSeletedted + "\n\n";
                }

            }




            return fieldListForIndexJs;
        }

        private List<string> GenerateFielListForCreateCsHtml(List<KeyValuePair<string, string>> fielList)
        {
            List<string> fieldListForIndexCsHtml = new List<string>();
            var FieldStringTemplate = UsingTemplates.CsHtmlCreator.FieldString;
            FieldStringTemplate = FieldStringTemplate.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            FieldStringTemplate = FieldStringTemplate.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            FieldStringTemplate = FieldStringTemplate.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            FieldStringTemplate = FieldStringTemplate.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            FieldStringTemplate += "\n";

            var FieldNumberTemplate = UsingTemplates.CsHtmlCreator.FieldNumber;
            FieldNumberTemplate = FieldNumberTemplate.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            FieldNumberTemplate = FieldNumberTemplate.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            FieldNumberTemplate = FieldNumberTemplate.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            FieldNumberTemplate = FieldNumberTemplate.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            FieldNumberTemplate += "\n";

            var FieldRelatedTemplate = UsingTemplates.CsHtmlCreator.FieldRelated;
            FieldRelatedTemplate = ReplaceNameEntitiesAllFormat(FieldRelatedTemplate);
            FieldNumberTemplate += "\n";


            foreach (var item in fielList)
            {
                string isRequired = "required";
                string nameCamel = item.Value.Substring(0, 1).ToLower() + item.Value.Substring(1);
                String newField = "";

                var comboParameter = GetComboParameter(item.Value);
                if (comboParameter != null)
                {
                    string fieldForAngular = item.Value.Substring(0, 1).ToLower() + item.Value.Substring(1);
                    newField = FieldRelatedTemplate.Replace(XXXFieldKeyXXX, fieldForAngular);
                    newField = newField.Replace(XXXFieldNameCamelXXX, comboParameter.FieldNameValue);
                    newField = newField.Replace(XXXEntityLowerSingularXXX, comboParameter.EntityCamelSingular);
                    newField = newField.Replace(XXXFieldNameCamelPluralXXX, comboParameter.EntityCamelPlural);
                    newField = newField.Replace(XXXFieldNameCapitalXXX, comboParameter.EntitySingular);
                    newField = newField.Replace(XXXFieldNameCamelSingularXXX, comboParameter.EntitySingular);
                    newField = newField.Replace(XXXFieldNameCapitalSingularXXX, comboParameter.EntitySingular);
                }
                else
                {
                    if (item.Key == "String" || item.Key == "string")
                        newField = FieldStringTemplate.Replace(XXXFieldNameCamelXXX, nameCamel);

                    if (item.Key == "int" || item.Key == "long" || item.Key == "decimal" || item.Key == "Decimal" || item.Key == "double"
                        || item.Key == "Double" || item.Key == "Integer" || item.Key == "Int64" || item.Key == "Int32" || item.Key == "Int16")
                        newField = FieldNumberTemplate.Replace(XXXFieldNameCamelXXX, nameCamel);

                    newField = newField.Replace(XXXFieldNameCapitalXXX, item.Value);
                    newField = newField.Replace(XXXrequiredXXX, isRequired);
                }
                fieldListForIndexCsHtml.Add(newField);
            }
            return fieldListForIndexCsHtml;
        }

        public static string fileNameEntityDto = "ListDto.cs";
        public static string fileNameCreateDto = "CreateDto.cs";
        public static string fileNameUpdateDto = "UpdateDto.cs";

        private List<KeyValuePair<String, String>> GenerateDtos(string[] loadedClass)
        {
            List<KeyValuePair<String, String>> fieldList = new List<KeyValuePair<string, string>>();
            DtosPathTemp = DtosPath + txtCapitalPlural.Text + @"\Dto\";

            fileNameEntityDto = DtosPathTemp + txtCapitalPlural.Text + "Dto.cs";
            fileNameCreateDto = DtosPathTemp + txtCapitalPlural.Text + "CreateDto.cs";
            fileNameUpdateDto = DtosPathTemp + txtCapitalPlural.Text + "UpdateDto.cs";

            List<string> allowedtypes = new List<string>() { "int", "string", "String","Integer","DateTime","Date","Decimal","float","double","Double","long","int32",
                "int16","int64","bool","Boolean" };

            string classListDto = DtosCreator.EntityDto_JustStartLines;
            classListDto = classListDto.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            classListDto = classListDto.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            classListDto = classListDto.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            classListDto += "\n";


            string classUpdateDto = DtosCreator.UpdateDto_JustStartLines;
            classUpdateDto = classUpdateDto.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            classUpdateDto = classUpdateDto.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            classUpdateDto = classUpdateDto.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            classUpdateDto = classUpdateDto.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            classUpdateDto += "\n";


            string classCreateDto = DtosCreator.CreateDto_JustStartLines;
            classCreateDto = classCreateDto.Replace(XXXEntitySingularXXX, txtCapitalSingular.Text);
            classCreateDto = classCreateDto.Replace(XXXEntityPluralXXX, txtCapitalPlural.Text);
            classCreateDto = classCreateDto.Replace(XXXEntityLowerSingularXXX, txtCamelSingular.Text);
            classCreateDto = classCreateDto.Replace(XXXEntityLowerPluralXXX, txtCamelPlural.Text);
            classCreateDto += "\n";


            classListDto += "              public int Id {get;set;} \n";
            classUpdateDto += "              public int Id {get;set;} \n";

            for (int i = 0; i < loadedClass.Length; i++)
            {
                var lineX = loadedClass[i];
                if (!lineX.Contains("public"))
                    continue;

                if (lineX.Contains("class"))
                    continue;

                if (lineX.Contains("("))
                    continue;



                var type = GetTypeString(lineX);
                var property = GetPropertyString(lineX);


                if (!allowedtypes.Contains(type))
                    continue;

                var field = new KeyValuePair<string, string>(type, property);
                fieldList.Add(field);
                classListDto += "              public " + type + " " + property + " {get;set;} \n";
                classUpdateDto += "              public " + type + " " + property + " {get;set;} \n";
                classCreateDto += "              public " + type + " " + property + " {get;set;} \n";
            }

            classListDto += "              public bool IsActive {get;set;} \n";
            classListDto += "              public DateTime CreationTime {get;set;} \n";
            classListDto += "              public long? CreatorUserId {get;set;} \n";


            classCreateDto += "              public bool IsActive {get;set;} \n";
            classCreateDto += "              public DateTime CreationTime {get;set;} \n";
            classCreateDto += "              public long? CreatorUserId {get;set;} \n";


            classUpdateDto += "              public bool IsActive {get;set;} \n";
            classUpdateDto += "              public DateTime CreationTime {get;set;} \n";
            classUpdateDto += "              public long? CreatorUserId {get;set;} \n";

            classListDto += "\n         }\n}";
            classUpdateDto += "\n         }\n}";
            classCreateDto += "\n         }\n}";

            rtbEntityDtoGenerator.Clear();
            rtbEntityDtoGenerator.AppendText(classListDto);
            rtbEntityUpdateDtoGenerator.Clear();
            rtbEntityUpdateDtoGenerator.AppendText(classUpdateDto);
            rtbEntityCreateDtoGenerator.Clear();
            rtbEntityCreateDtoGenerator.AppendText(classCreateDto);

            return fieldList;
        }

        public static string GetTypeString(String Line)
        {
            string currentType = "";

            var index = Line.IndexOf("public");
            var indexType = Line.IndexOf(" ", index + 1);
            var indexProperty = Line.IndexOf(" ", indexType + 1);
            currentType = Line.Substring(indexType + 1, indexProperty - indexType - 1);
            return currentType;
        }

        public static string GetPropertyString(String Line)
        {
            string property = "";

            var index = Line.IndexOf("public");
            var indexType = Line.IndexOf(" ", index + 1);
            var indexProperty = Line.IndexOf(" ", indexType + 1);
            var indexPropertyEnd = Line.IndexOf(" ", indexProperty + 1);
            property = Line.Substring(indexProperty + 1, indexPropertyEnd - indexProperty - 1);
            return property;
        }


        private void btnSaveOnDisk_Click(object sender, EventArgs e)
        {
            btnSaveOnDisk.Visible = false;
            try
            {
                string PathDirectoryAppService = pathDirectoryApp + txtCapitalPlural.Text;
                if (!System.IO.Directory.Exists(PathDirectoryAppService))
                    System.IO.Directory.CreateDirectory(PathDirectoryAppService);

                if (!System.IO.Directory.Exists(DtosPathTemp))
                    System.IO.Directory.CreateDirectory(DtosPathTemp);

                if (!System.IO.Directory.Exists(ViewDirectory))
                    System.IO.Directory.CreateDirectory(ViewDirectory);

                string fileNameIAppService = PathDirectoryAppService + @"\I" + txtCapitalSingular.Text + "AppService.cs";
                string fileNameAppService = PathDirectoryAppService + @"\" + txtCapitalSingular.Text + "AppService.cs";

                //Se genera el archivo de la Interface
                createSpecificFileOnDisk(fileNameIAppService, rtbIAppSericeGenerator);

                //Se general el archivo del servicio
                createSpecificFileOnDisk(fileNameAppService, rtbAppSericeGenerator);

                createSpecificFileOnDisk(fileNameEntityDto, rtbEntityDtoGenerator);
                createSpecificFileOnDisk(fileNameCreateDto, rtbEntityCreateDtoGenerator);
                createSpecificFileOnDisk(fileNameUpdateDto, rtbEntityUpdateDtoGenerator);

                createSpecificFileOnDisk(fileNameIndexJs, rtbIndexJsGenerator);
                createSpecificFileOnDisk(fileNameCreateJs, rtbCreateJsGenerator);
                createSpecificFileOnDisk(fileNameEditJs, rtbUpdateJsGenerator);

                createSpecificFileOnDisk(fileNameIndexCsHtml, rtbIndexCshtmlGenerator);
                createSpecificFileOnDisk(fileNameEditCsHtml, rtbUpdateCsHtmlGenerator);
                createSpecificFileOnDisk(fileNameCreateCsHtml, rtbCreateCsHtmlGenerator);


                createSpecificFileOnDisk(frmPrincipal.AuthorizationProviderFile, rtbAutorizationProviderGenerator);
                createSpecificFileOnDisk(frmPrincipal.NavigationProviderFile, rtbNavigationProviderGenerator);
                createSpecificFileOnDisk(frmPrincipal.PermissionNamesFile, rtbPermissionNameGenerator);
                createSpecificFileOnDisk(frmPrincipal.AppJsFile, rtbAppJsGenerator);
                createSpecificFileOnDisk(frmPrincipal.NavBarJSFile, rtbMenuNavBarGenerator);

                System.Threading.Thread.Sleep(1000);
                MessageBox.Show("Mantenimiento generado exitosamente");
            }
            catch (Exception err)
            {
                MessageBox.Show("Surgió el siguiente error : " + err.Message);
            }
            Application.Exit();

        }

        private void createSpecificFileOnDisk(string fileName, RichTextBox textContainer)
        {
            if (System.IO.File.Exists(fileName))
                System.IO.File.Delete(fileName);
            System.IO.File.AppendAllText(fileName, textContainer.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Utils.ReadAllTemplastes();
            Utils.ReadAllOriginal();
            lbxCombos.Items.Clear();
            this.Text = Utils.Version;
            txtCamelSingular.Focus();
        }

        private void RichTextBoxGeneral_MouseClick(object sender, MouseEventArgs e)
        {
            frmVisor vs = new frmVisor((RichTextBox)sender);
            vs.ShowDialog();
        }

        private void btnAgregarCombo_Click(object sender, EventArgs e)
        {
            if (txtRelateFieldDisplay.BackColor == Color.Red)
                return;
            if (txtRelateFieldValue.BackColor == Color.Red)
                return;
            if (txtRelateEntity.BackColor == Color.Red)
                return;

            if (string.IsNullOrEmpty(txtRelateEntity.Text) ||
                string.IsNullOrEmpty(txtRelateFieldDisplay.Text) ||
                string.IsNullOrEmpty(txtRelateFieldValue.Text))
            {
                MessageBox.Show("Debe completar todos los campos de la entidad relacionda");
                return;
            }

            ComboParameter cbp = new ComboParameter(txtRelateEntity.Text, txtRelateFieldValue.Text, txtRelateFieldDisplay.Text);
            lbxCombos.Items.Add(cbp);

            txtRelateEntity.Clear();
            txtRelateFieldDisplay.Clear();
            txtRelateFieldValue.Clear();
            btnGenerate_Click(null, null);
        }

        private void txtRelateFieldValue_TextChanged(object sender, EventArgs e)
        {
            txtRelateFieldValue.BackColor = Color.White;
            if (txtRelateFieldValue.Text.Length == 0)
                return;


            if (ExistInFielList(txtRelateFieldValue.Text))
            {
                txtRelateFieldValue.BackColor = Color.Green;
                btnAgregarCombo.Enabled = true;
            }
            else
                if (!ExistInFielList(txtRelateFieldValue.Text))
            {
                txtRelateFieldValue.BackColor = Color.Red;
                btnAgregarCombo.Enabled = false;
            }
        }

        public List<KeyValuePair<string, string>> fieldsRelatedEntity = new List<KeyValuePair<string, string>>();


        private void txtRelateEntity_TextChanged(object sender, EventArgs e)
        {
            txtRelateEntity.BackColor = Color.White;
            if (txtRelateEntity.Text.Length == 0)
                return;

            fieldsRelatedEntity = Utils.getFieldListFromEntity(txtRelateEntity.Text);
            if (fieldsRelatedEntity.Count == 0)
                txtRelateEntity.BackColor = Color.Red;
            else
                txtRelateEntity.BackColor = Color.Green;
        }

        private void txtRelateFieldDisplay_TextChanged(object sender, EventArgs e)
        {
            txtRelateFieldDisplay.BackColor = Color.White;
            if (txtRelateFieldDisplay.Text.Length == 0)
                return;


            if (ExistInRelatedFielList(txtRelateFieldDisplay.Text))
            {
                txtRelateFieldDisplay.BackColor = Color.Green;
                btnAgregarCombo.Enabled = true;
            }
            else
                    if (!ExistInRelatedFielList(txtRelateFieldDisplay.Text))
            {
                txtRelateFieldDisplay.BackColor = Color.Red;
                btnAgregarCombo.Enabled = false;
            }
        }

        private void frmPrincipal_Shown(object sender, EventArgs e)
        {
            txtCamelSingular.Focus();
        }
    }
}

//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.ScoreTypes.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ScoreTypes 
{ 
    [AbpAuthorize(PermissionNames.Pages_ScoreTypes)] 
    public class ScoreTypeAppService : AsyncCrudAppService<Models.ScoreTypes, ScoreTypeDto, int, PagedResultRequestDto, CreateScoreTypeDto, UpdateScoreTypeDto>, IScoreTypeAppService 
    { 
        private readonly IRepository<Models.ScoreTypes, int> _scoreTypeRepository;
		


        public ScoreTypeAppService( 
            IRepository<Models.ScoreTypes, int> repository, 
            IRepository<Models.ScoreTypes, int> scoreTypeRepository 
            ) 
            : base(repository) 
        { 
            _scoreTypeRepository = scoreTypeRepository; 
			

			
        } 
        public override async Task<ScoreTypeDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ScoreTypeDto> Create(CreateScoreTypeDto input) 
        { 
            CheckCreatePermission(); 
            var scoreType = ObjectMapper.Map<Models.ScoreTypes>(input); 
			try{
              await _scoreTypeRepository.InsertAsync(scoreType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(scoreType); 
        } 
        public override async Task<ScoreTypeDto> Update(UpdateScoreTypeDto input) 
        { 
            CheckUpdatePermission(); 
            var scoreType = await _scoreTypeRepository.GetAsync(input.Id);
            MapToEntity(input, scoreType); 
		    try{
               await _scoreTypeRepository.UpdateAsync(scoreType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var scoreType = await _scoreTypeRepository.GetAsync(input.Id); 
               await _scoreTypeRepository.DeleteAsync(scoreType);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.ScoreTypes MapToEntity(CreateScoreTypeDto createInput) 
        { 
            var scoreType = ObjectMapper.Map<Models.ScoreTypes>(createInput); 
            return scoreType; 
        } 
        protected override void MapToEntity(UpdateScoreTypeDto input, Models.ScoreTypes scoreType) 
        { 
            ObjectMapper.Map(input, scoreType); 
        } 
        protected override IQueryable<Models.ScoreTypes> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ScoreTypes> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ScoreTypes> GetEntityByIdAsync(int id) 
        { 
            var scoreType = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(scoreType); 
        } 
        protected override IQueryable<Models.ScoreTypes> ApplySorting(IQueryable<Models.ScoreTypes> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ScoreTypeDto>> GetAllScoreTypes(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ScoreTypeDto> ouput = new PagedResultDto<ScoreTypeDto>(); 
            IQueryable<Models.ScoreTypes> query = query = from x in _scoreTypeRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _scoreTypeRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ScoreTypes.Dto.ScoreTypeDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<ScoreTypeDto>> GetAllScoreTypesForCombo()
        {
            var scoreTypeList = await _scoreTypeRepository.GetAllListAsync(x => x.IsActive == true);

            var scoreType = ObjectMapper.Map<List<ScoreTypeDto>>(scoreTypeList.ToList());

            return scoreType;
        }
		
    } 
} ;
//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.ScoreTypes.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.ScoreTypes
{
      public interface IScoreTypeAppService : IAsyncCrudAppService<ScoreTypeDto, int, PagedResultRequestDto, CreateScoreTypeDto, UpdateScoreTypeDto>
      {
            Task<PagedResultDto<ScoreTypeDto>> GetAllScoreTypes(GdPagedResultRequestDto input);
			Task<List<Dto.ScoreTypeDto>> GetAllScoreTypesForCombo();
      }
}
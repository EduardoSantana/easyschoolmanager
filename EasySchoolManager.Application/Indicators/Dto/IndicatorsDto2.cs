//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.IndicatorGroups.Dto;
using System.Collections.Generic;
using EasySchoolManager.Evaluations.Dto;

namespace EasySchoolManager.Indicators.Dto
{
    [AutoMap(typeof(Models.Indicators))]
    public class IndicatorDto2 : EntityDto<int>
    {
        public string Name { get; set; }
        public int IndicatorGroupId { get; set; }
        public bool IsActive { get; set; }
        public int Sequence { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        //public IndicatorGroupDto IndicatorGroups { get; set; }
    }
}
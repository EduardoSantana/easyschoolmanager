//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Indicators.Dto 
{
        [AutoMap(typeof(Models.Indicators))] 
        public class CreateIndicatorDto : EntityDto<int> 
        {

              [StringLength(1000)] 
              public string Name {get;set;} 
              public int Sequence { get; set; }
              public int IndicatorGroupId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
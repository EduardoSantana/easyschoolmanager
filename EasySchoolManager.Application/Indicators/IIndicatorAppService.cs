//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Indicators.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Indicators
{
      public interface IIndicatorAppService : IAsyncCrudAppService<IndicatorDto, int, PagedResultRequestDto, CreateIndicatorDto, UpdateIndicatorDto>
      {
            Task<PagedResultDto<IndicatorDto>> GetAllIndicators(GdPagedResultRequestDto input);
			Task<List<Dto.IndicatorDto>> GetAllIndicatorsForCombo();
      }
}
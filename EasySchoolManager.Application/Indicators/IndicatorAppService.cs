//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Indicators.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Indicators 
{ 
    [AbpAuthorize(PermissionNames.Pages_Indicators)] 
    public class IndicatorAppService : AsyncCrudAppService<Models.Indicators, IndicatorDto, int, PagedResultRequestDto, CreateIndicatorDto, UpdateIndicatorDto>, IIndicatorAppService 
    { 
        private readonly IRepository<Models.Indicators, int> _indicatorRepository;
		
		    private readonly IRepository<Models.IndicatorGroups, int> _indicatorGroupRepository;


        public IndicatorAppService( 
            IRepository<Models.Indicators, int> repository, 
            IRepository<Models.Indicators, int> indicatorRepository ,
            IRepository<Models.IndicatorGroups, int> indicatorGroupRepository

            ) 
            : base(repository) 
        { 
            _indicatorRepository = indicatorRepository; 
			
            _indicatorGroupRepository = indicatorGroupRepository;


			
        } 
        public override async Task<IndicatorDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<IndicatorDto> Create(CreateIndicatorDto input) 
        { 
            CheckCreatePermission(); 
            var indicator = ObjectMapper.Map<Models.Indicators>(input); 
			try{
              await _indicatorRepository.InsertAsync(indicator); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(indicator); 
        } 
        public override async Task<IndicatorDto> Update(UpdateIndicatorDto input) 
        { 
            CheckUpdatePermission(); 
            var indicator = await _indicatorRepository.GetAsync(input.Id);
            MapToEntity(input, indicator); 
		    try{
               await _indicatorRepository.UpdateAsync(indicator); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var indicator = await _indicatorRepository.GetAsync(input.Id); 
               await _indicatorRepository.DeleteAsync(indicator);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Indicators MapToEntity(CreateIndicatorDto createInput) 
        { 
            var indicator = ObjectMapper.Map<Models.Indicators>(createInput); 
            return indicator; 
        } 
        protected override void MapToEntity(UpdateIndicatorDto input, Models.Indicators indicator) 
        { 
            ObjectMapper.Map(input, indicator); 
        } 
        protected override IQueryable<Models.Indicators> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Indicators> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Indicators> GetEntityByIdAsync(int id) 
        { 
            var indicator = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(indicator); 
        } 
        protected override IQueryable<Models.Indicators> ApplySorting(IQueryable<Models.Indicators> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<IndicatorDto>> GetAllIndicators(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<IndicatorDto> ouput = new PagedResultDto<IndicatorDto>(); 
            IQueryable<Models.Indicators> query = query = from x in _indicatorRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _indicatorRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Indicators.Dto.IndicatorDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<IndicatorDto>> GetAllIndicatorsForCombo()
        {
            var indicatorList = await _indicatorRepository.GetAllListAsync(x => x.IsActive == true);

            var indicator = ObjectMapper.Map<List<IndicatorDto>>(indicatorList.ToList());

            return indicator;
        }
		
    } 
} ;
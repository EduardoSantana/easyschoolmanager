//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.CourseSessions.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.CourseSessions
{
	  public interface ICourseSessionAppService : IAsyncCrudAppService<CourseSessionDto, int, PagedResultRequestDto, CreateCourseSessionDto, UpdateCourseSessionDto>
	  {
			Task<PagedResultDto<CourseSessionDto>> GetAllCourseSessions(GdPagedResultRequestDto input);
			List<CourseSessionDto> GetAllCourseSessionsForCombo();
			List<CourseSessionDto> GetAllCourseSessionsPrimaryForCombo();
			List<CourseSessionDto> GetAllCourseSessionsHighsForCombo();
	}
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.CourseSessions.Dto 
{
        [AutoMap(typeof(Models.CourseSessions))] 
        public class CreateCourseSessionDto : EntityDto<int> 
        {
              public int CourseId {get;set;} 
              public string Name {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper;
using EasySchoolManager.Courses.Dto;

namespace EasySchoolManager.CourseSessions.Dto 
{
        [AutoMap(typeof(Models.CourseSessions))] 
        public class CourseSessionDto : EntityDto<int> 
        { 
              public int CourseId {get;set;} 
              public string Name {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 
              public string Courses_Name { get; set; }
              public string Courses_Abbreviation { get; set; }
              public CourseDto Courses { get; set; }

              public string CoursesNameName { get { return this.Courses_Name + " - " + this.Name; } }



         }
}
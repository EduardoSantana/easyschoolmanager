//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.CourseSessions.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
namespace EasySchoolManager.CourseSessions 
{ 
    [AbpAuthorize(PermissionNames.Pages_CourseSessions)] 
    public class CourseSessionAppService : BaseAppService<Models.CourseSessions, CourseSessionDto, int, PagedResultRequestDto, CreateCourseSessionDto, UpdateCourseSessionDto>, ICourseSessionAppService 
    { 
        private readonly IRepository<Models.CourseSessions, int> _courseSessionRepository;
		
		    private readonly IRepository<Models.Courses, int> _courseRepository;


        public CourseSessionAppService( 
            IRepository<Models.CourseSessions, int> repository, 
            IRepository<Models.CourseSessions, int> courseSessionRepository ,
            IRepository<Models.Courses, int> courseRepository

            ) 
            : base(repository) 
        { 
            _courseSessionRepository = courseSessionRepository; 
			
            _courseRepository = courseRepository;


			
        } 
        public override async Task<CourseSessionDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<CourseSessionDto> Create(CreateCourseSessionDto input) 
        { 
            CheckCreatePermission();
            input.Name = input.Name.ToUpper();
            var courseSession = ObjectMapper.Map<Models.CourseSessions>(input); 
            await _courseSessionRepository.InsertAsync(courseSession); 
            return MapToEntityDto(courseSession); 
        } 
        public override async Task<CourseSessionDto> Update(UpdateCourseSessionDto input) 
        { 
            CheckUpdatePermission();
            input.Name = input.Name.ToUpper();
           var courseSession = await _courseSessionRepository.GetAsync(input.Id); 
           MapToEntity(input, courseSession); 
            await _courseSessionRepository.UpdateAsync(courseSession); 
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        { 
            var courseSession = await _courseSessionRepository.GetAsync(input.Id); 
            await _courseSessionRepository.DeleteAsync(courseSession); 
        } 
        protected override Models.CourseSessions MapToEntity(CreateCourseSessionDto createInput) 
        { 
            var courseSession = ObjectMapper.Map<Models.CourseSessions>(createInput); 
            return courseSession; 
        } 
        protected override void MapToEntity(UpdateCourseSessionDto input, Models.CourseSessions courseSession) 
        { 
            ObjectMapper.Map(input, courseSession); 
        } 
        protected override IQueryable<Models.CourseSessions> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.CourseSessions> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.CourseSessions> GetEntityByIdAsync(int id) 
        { 
            var courseSession = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(courseSession); 
        } 
        protected override IQueryable<Models.CourseSessions> ApplySorting(IQueryable<Models.CourseSessions> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<CourseSessionDto>> GetAllCourseSessions(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<CourseSessionDto> ouput = new PagedResultDto<CourseSessionDto>(); 
            IQueryable<Models.CourseSessions> query = query = from x in _courseSessionRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _courseSessionRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<CourseSessions.Dto.CourseSessionDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public List<CourseSessionDto> GetAllCourseSessionsForCombo()
        {
            var courseSessionQuery=  _courseSessionRepository.GetAll().Where(x => x.IsActive == true);
            courseSessionQuery = this.FilterWhere(courseSessionQuery);

            var courseSession = ObjectMapper.Map<List<CourseSessionDto>>(courseSessionQuery.ToList());

            return courseSession;
        }

        [AbpAllowAnonymous]
        public List<CourseSessionDto> GetAllCourseSessionsPrimaryForCombo()
        {
            var courseSessionQuery = _courseSessionRepository.GetAll().Where(x => x.IsActive == true && x.Courses.LevelId < 3);
            courseSessionQuery = this.FilterWhere(courseSessionQuery);

            var courseSession = ObjectMapper.Map<List<CourseSessionDto>>(courseSessionQuery.ToList());

            return courseSession;
        }

        [AbpAllowAnonymous]
        public List<CourseSessionDto> GetAllCourseSessionsHighsForCombo()
        {
            var courseSessionQuery = _courseSessionRepository.GetAll().Where(x => x.IsActive == true && x.Courses.LevelId == 3);
            courseSessionQuery = this.FilterWhere(courseSessionQuery);

            var courseSession = ObjectMapper.Map<List<CourseSessionDto>>(courseSessionQuery.ToList());

            return courseSession;
        }

    } 
} 
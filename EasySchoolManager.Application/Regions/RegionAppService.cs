//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Regions.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
namespace EasySchoolManager.Regions 
{ 
    [AbpAuthorize(PermissionNames.Pages_Regions)] 
    public class RegionAppService : AsyncCrudAppService<Models.Regions, RegionDto, int, PagedResultRequestDto, CreateRegionDto, UpdateRegionDto>, IRegionAppService 
    { 
        private readonly IRepository<Models.Regions, int> _regionRepository;
		


        public RegionAppService( 
            IRepository<Models.Regions, int> repository, 
            IRepository<Models.Regions, int> regionRepository 
            ) 
            : base(repository) 
        { 
            _regionRepository = regionRepository; 
			

			
        } 
        public override async Task<RegionDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<RegionDto> Create(CreateRegionDto input) 
        { 
            CheckCreatePermission(); 
            var region = ObjectMapper.Map<Models.Regions>(input); 
            await _regionRepository.InsertAsync(region); 
            return MapToEntityDto(region); 
        } 
        public override async Task<RegionDto> Update(UpdateRegionDto input) 
        { 
            CheckUpdatePermission(); 
           var region = await _regionRepository.GetAsync(input.Id); 
           MapToEntity(input, region); 
            await _regionRepository.UpdateAsync(region); 
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        { 
            var region = await _regionRepository.GetAsync(input.Id); 
            await _regionRepository.DeleteAsync(region); 
        } 
        protected override Models.Regions MapToEntity(CreateRegionDto createInput) 
        { 
            var region = ObjectMapper.Map<Models.Regions>(createInput); 
            return region; 
        } 
        protected override void MapToEntity(UpdateRegionDto input, Models.Regions region) 
        { 
            ObjectMapper.Map(input, region); 
        } 
        protected override IQueryable<Models.Regions> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Regions> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Regions> GetEntityByIdAsync(int id) 
        { 
            var region = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(region); 
        } 
        protected override IQueryable<Models.Regions> ApplySorting(IQueryable<Models.Regions> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<RegionDto>> GetAllRegions(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<RegionDto> ouput = new PagedResultDto<RegionDto>(); 
            IQueryable<Models.Regions> query = query = from x in _regionRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _regionRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Regions.Dto.RegionDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<RegionDto>> GetAllRegionsForCombo()
        {
            var regionList = await _regionRepository.GetAllListAsync(x => x.IsActive == true);

            var region = ObjectMapper.Map<List<RegionDto>>(regionList.ToList());

            return region;
        }
		
    } 
} ;
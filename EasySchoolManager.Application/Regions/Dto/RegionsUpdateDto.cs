//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Regions.Dto 
{
        [AutoMap(typeof(Models.Regions))] 
        public class UpdateRegionDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public string Name {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;}
              public string Code { get; set; }
              public string Rnc { get; set; }
              public string LegalName { get; set; }

    }
}
//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Regions.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Regions
{
      public interface IRegionAppService : IAsyncCrudAppService<RegionDto, int, PagedResultRequestDto, CreateRegionDto, UpdateRegionDto>
      {
            Task<PagedResultDto<RegionDto>> GetAllRegions(GdPagedResultRequestDto input);
			Task<List<Dto.RegionDto>> GetAllRegionsForCombo();
      }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.HistoryCancelReceipts.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.HistoryCancelReceipts 
{ 
   // [AbpAuthorize(PermissionNames.Pages_HistoryCancelReceipts)] 
    public class HistoryCancelReceiptAppService : AsyncCrudAppService<Models.HistoryCancelReceipts, HistoryCancelReceiptDto, int, PagedResultRequestDto, CreateHistoryCancelReceiptDto, UpdateHistoryCancelReceiptDto>, IHistoryCancelReceiptAppService 
    { 
        private readonly IRepository<Models.HistoryCancelReceipts, int> _historyCancelReceiptRepository;
		


        public HistoryCancelReceiptAppService( 
            IRepository<Models.HistoryCancelReceipts, int> repository, 
            IRepository<Models.HistoryCancelReceipts, int> historyCancelReceiptRepository 
            ) 
            : base(repository) 
        { 
            _historyCancelReceiptRepository = historyCancelReceiptRepository; 
			

			
        } 
        public override async Task<HistoryCancelReceiptDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<HistoryCancelReceiptDto> Create(CreateHistoryCancelReceiptDto input) 
        { 
            CheckCreatePermission(); 
            var historyCancelReceipt = ObjectMapper.Map<Models.HistoryCancelReceipts>(input); 
			try{
              await _historyCancelReceiptRepository.InsertAsync(historyCancelReceipt); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(historyCancelReceipt); 
        } 
        public override async Task<HistoryCancelReceiptDto> Update(UpdateHistoryCancelReceiptDto input) 
        { 
            CheckUpdatePermission(); 
            var historyCancelReceipt = await _historyCancelReceiptRepository.GetAsync(input.Id);
            MapToEntity(input, historyCancelReceipt); 
		    try{
               await _historyCancelReceiptRepository.UpdateAsync(historyCancelReceipt); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var historyCancelReceipt = await _historyCancelReceiptRepository.GetAsync(input.Id); 
               await _historyCancelReceiptRepository.DeleteAsync(historyCancelReceipt);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.HistoryCancelReceipts MapToEntity(CreateHistoryCancelReceiptDto createInput) 
        { 
            var historyCancelReceipt = ObjectMapper.Map<Models.HistoryCancelReceipts>(createInput); 
            return historyCancelReceipt; 
        } 
        protected override void MapToEntity(UpdateHistoryCancelReceiptDto input, Models.HistoryCancelReceipts historyCancelReceipt) 
        { 
            ObjectMapper.Map(input, historyCancelReceipt); 
        } 
        protected override IQueryable<Models.HistoryCancelReceipts> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.HistoryCancelReceipts> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.HistoryCancelReceipts> GetEntityByIdAsync(int id) 
        { 
            var historyCancelReceipt = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(historyCancelReceipt); 
        } 
        protected override IQueryable<Models.HistoryCancelReceipts> ApplySorting(IQueryable<Models.HistoryCancelReceipts> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Description); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<HistoryCancelReceiptDto>> GetAllHistoryCancelReceipts(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<HistoryCancelReceiptDto> ouput = new PagedResultDto<HistoryCancelReceiptDto>(); 
            IQueryable<Models.HistoryCancelReceipts> query = query = from x in _historyCancelReceiptRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _historyCancelReceiptRepository.GetAll() 
                        where x.Description.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<HistoryCancelReceipts.Dto.HistoryCancelReceiptDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<HistoryCancelReceiptDto>> GetAllHistoryCancelReceiptsForCombo()
        {
            var historyCancelReceiptList = await _historyCancelReceiptRepository.GetAllListAsync(x => x.IsActive == true);

            var historyCancelReceipt = ObjectMapper.Map<List<HistoryCancelReceiptDto>>(historyCancelReceiptList.ToList());

            return historyCancelReceipt;
        }
		
    } 
} ;
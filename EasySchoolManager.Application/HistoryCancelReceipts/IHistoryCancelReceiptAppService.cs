//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.HistoryCancelReceipts.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.HistoryCancelReceipts
{
      public interface IHistoryCancelReceiptAppService : IAsyncCrudAppService<HistoryCancelReceiptDto, int, PagedResultRequestDto, CreateHistoryCancelReceiptDto, UpdateHistoryCancelReceiptDto>
      {
            Task<PagedResultDto<HistoryCancelReceiptDto>> GetAllHistoryCancelReceipts(GdPagedResultRequestDto input);
			Task<List<Dto.HistoryCancelReceiptDto>> GetAllHistoryCancelReceiptsForCombo();
      }
}
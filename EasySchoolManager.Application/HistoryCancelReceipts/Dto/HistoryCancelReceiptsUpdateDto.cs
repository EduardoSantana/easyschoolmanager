//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.HistoryCancelReceipts.Dto 
{
        [AutoMap(typeof(Models.HistoryCancelReceipts))] 
        public class UpdateHistoryCancelReceiptDto : EntityDto<int> 
        {

              public long TransactionId {get;set;} 

              [StringLength(100)]  
              public string Description {get;set;} 

              public DateTime Date {get;set;} 

              public int CA {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
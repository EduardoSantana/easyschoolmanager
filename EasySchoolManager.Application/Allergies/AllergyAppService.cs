//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Allergies.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
namespace EasySchoolManager.Allergies 
{ 
    [AbpAuthorize(PermissionNames.Pages_Allergies)] 
    public class AllergyAppService : AsyncCrudAppService<Models.Allergies, AllergyDto, int, PagedResultRequestDto, CreateAllergyDto, UpdateAllergyDto>, IAllergyAppService 
    { 
        private readonly IRepository<Models.Allergies, int> _allergyRepository;
		


        public AllergyAppService( 
            IRepository<Models.Allergies, int> repository, 
            IRepository<Models.Allergies, int> allergyRepository 
            ) 
            : base(repository) 
        { 
            _allergyRepository = allergyRepository; 
			

			
        } 
        public override async Task<AllergyDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<AllergyDto> Create(CreateAllergyDto input) 
        { 
            CheckCreatePermission(); 
            var allergy = ObjectMapper.Map<Models.Allergies>(input); 
            await _allergyRepository.InsertAsync(allergy); 
            return MapToEntityDto(allergy); 
        } 
        public override async Task<AllergyDto> Update(UpdateAllergyDto input) 
        { 
            CheckUpdatePermission(); 
           var allergy = await _allergyRepository.GetAsync(input.Id); 
           MapToEntity(input, allergy); 
            await _allergyRepository.UpdateAsync(allergy); 
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        { 
            var allergy = await _allergyRepository.GetAsync(input.Id); 
            await _allergyRepository.DeleteAsync(allergy); 
        } 
        protected override Models.Allergies MapToEntity(CreateAllergyDto createInput) 
        { 
            var allergy = ObjectMapper.Map<Models.Allergies>(createInput); 
            return allergy; 
        } 
        protected override void MapToEntity(UpdateAllergyDto input, Models.Allergies allergy) 
        { 
            ObjectMapper.Map(input, allergy); 
        } 
        protected override IQueryable<Models.Allergies> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Allergies> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Allergies> GetEntityByIdAsync(int id) 
        { 
            var allergy = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(allergy); 
        } 
        protected override IQueryable<Models.Allergies> ApplySorting(IQueryable<Models.Allergies> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<AllergyDto>> GetAllAllergies(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<AllergyDto> ouput = new PagedResultDto<AllergyDto>(); 
            IQueryable<Models.Allergies> query = query = from x in _allergyRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _allergyRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Allergies.Dto.AllergyDto>>(query.ToList()); 
            return ouput; 
        } 
		
        [AbpAllowAnonymous]
		public async Task<List<AllergyDto>> GetAllAllergiesForCombo()
        {
            var allergyList = await _allergyRepository.GetAllListAsync(x => x.IsActive == true);

            var allergy = ObjectMapper.Map<List<AllergyDto>>(allergyList.ToList());

            return allergy;
        }
		
    } 
} ;
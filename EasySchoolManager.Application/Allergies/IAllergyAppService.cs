//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Allergies.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Allergies
{
      public interface IAllergyAppService : IAsyncCrudAppService<AllergyDto, int, PagedResultRequestDto, CreateAllergyDto, UpdateAllergyDto>
      {
            Task<PagedResultDto<AllergyDto>> GetAllAllergies(GdPagedResultRequestDto input);
			Task<List<Dto.AllergyDto>> GetAllAllergiesForCombo();
      }
}
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Courses.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;

namespace EasySchoolManager.Courses
{
    [AbpAuthorize(PermissionNames.Pages_Courses)]
    public class CourseAppService : BaseAppService<Models.Courses, CourseDto, int, PagedResultRequestDto, CreateCourseDto, UpdateCourseDto>, ICourseAppService
    {
        private readonly IRepository<Models.Courses, int> _courseRepository;
        private readonly IRepository<Models.Levels, int> _levelRepository;

        public CourseAppService(
            IRepository<Models.Courses, int> repository,
            IRepository<Models.Courses, int> courseRepository,
             IRepository<Models.Levels, int> levelRepository
            )
            : base(repository)
        {
            _courseRepository = courseRepository;
            _levelRepository = levelRepository;
        }

        public override async Task<CourseDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);

            return user;
        }

        public override async Task<CourseDto> Create(CreateCourseDto input)
        {
            CheckCreatePermission();

            var course = ObjectMapper.Map<Models.Courses>(input);

            await _courseRepository.InsertAsync(course);

            return MapToEntityDto(course);
        }

        public override async Task<CourseDto> Update(UpdateCourseDto input)
        {
            CheckUpdatePermission();

           // var ass = get

           // var ass = AbpSession.TenantId;

            var course = await _courseRepository.GetAsync(input.Id);

            MapToEntity(input, course);

            await _courseRepository.UpdateAsync(course);

            return await Get(input);
        }

        public override async Task Delete(EntityDto<int> input)
        {
            var course = await _courseRepository.GetAsync(input.Id);
            await _courseRepository.DeleteAsync(course);
        }


        protected override Models.Courses MapToEntity(CreateCourseDto createInput)
        {
            var course = ObjectMapper.Map<Models.Courses>(createInput);
            return course;
        }

        protected override void MapToEntity(UpdateCourseDto input, Models.Courses course)
        {
            ObjectMapper.Map(input, course);
        }

        protected override IQueryable<Models.Courses> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }

        protected  IQueryable<Models.Courses> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado =  Repository.GetAll();

            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter));

            return resultado;
        }

        protected override async Task<Models.Courses> GetEntityByIdAsync(int id)
        {
            var course = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(course);
        }

        protected override IQueryable<Models.Courses> ApplySorting(IQueryable<Models.Courses> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Name);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
        

        public async Task<PagedResultDto<CourseDto>> GetAllCourses(GdPagedResultRequestDto input)
        {
            PagedResultDto<CourseDto> ouput = new PagedResultDto<CourseDto>();

            IQueryable<Models.Courses> query = query = from x in _courseRepository.GetAll()
                                                       select x;

            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _courseRepository.GetAll()
                        where x.Name.Contains(input.TextFilter)
                        select x;
            }

            ouput.TotalCount = await Task.Run(() => { return query.Count(); });


            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }

            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }

            ouput.Items = ObjectMapper.Map<List<Courses.Dto.CourseDto>>(query.ToList());

            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<CourseDto>> GetAllCoursesForCombo()
        {
            var courseList = await this.GetAllForCombo();

            var course = ObjectMapper.Map<List<CourseDto>>(courseList.ToList());
            foreach (var item in course)
            {
                item.Name += " - " + item.Levels_Name; 
            }

            return course;
        }

        [AbpAllowAnonymous]
        public async Task<List<Dto.CourseDto>> GetAllCoursesByLevelsForCombo(int LevelId)
        {
            var courseList = await _courseRepository.GetAllListAsync(x => x.IsActive == true && x.LevelId == LevelId);

            var course = ObjectMapper.Map<List<CourseDto>>(courseList.ToList());
            foreach (var item in course)
            {
                item.Name += " - " + item.Levels_Name; 
            }

            return course;
        }
    }
}
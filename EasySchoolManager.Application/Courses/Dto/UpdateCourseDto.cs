using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.Authorization.Users;

namespace EasySchoolManager.Courses.Dto
{
    [AutoMap(typeof(Models.Courses))]
    public class UpdateCourseDto: EntityDto<int>
    {
        [Required]
        [MaxLength(50, ErrorMessage = "Usted no puede agregar m�s de {1} caracteres para el campo {0}")]
        public string Name { get; set; }

        [Required]
        [MaxLength(5, ErrorMessage = "Usted no puede agregar m�s de {1} caracteres a al campo {1}")]
        public string Abbreviation { get; set; }
   
        public int LevelId { get; set; }

        public bool IsActive { get; set; }
    }
}
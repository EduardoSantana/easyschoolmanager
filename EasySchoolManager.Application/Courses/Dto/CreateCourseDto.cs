﻿using System.ComponentModel.DataAnnotations;
using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.Authorization.Users;
using Abp.Application.Services.Dto;

namespace EasySchoolManager.Courses.Dto
{
    [AutoMap(typeof(Models.Courses))]
    public class CreateCourseDto: EntityDto<int>
    {
        [Required]
        [MaxLength(50, ErrorMessage = "Usted no puede agregar más de {1} caracteres para el campo {0}")]
        public string Name { get; set; }

        [Required]
        [StringLength(5)]
        public string Abbreviation { get; set; }

        public int LevelId { get; set; }

        public bool IsActive { get; set; }

    }
}
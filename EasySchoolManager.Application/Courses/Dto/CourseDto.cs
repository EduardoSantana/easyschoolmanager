﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.Courses.Dto
{
    [AutoMap(typeof(Models.Courses))]
    public class CourseDto : EntityDto<int>
    {
        [Required]
        [MaxLength(50, ErrorMessage = "Usted no puede agregar más de {1} caracteres para el campo {0}")]
        public string Name { get; set; }

        [Required]
        [MaxLength(5, ErrorMessage = "Usted no puede agregar más de {1} caracteres a al campo {1}")]
        public String Abbreviation { get; set; }

        public bool IsActive { get; set; }

        public int LevelId { get; set; }

        public DateTime CreationTime { get; set; }

        public string Levels_Name { get; set; }
    }
}

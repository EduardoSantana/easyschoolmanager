using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.Courses.Dto;
using EasySchoolManager.GD;
using System.Collections.Generic;

namespace EasySchoolManager.Courses
{
    public interface ICourseAppService : IAsyncCrudAppService<CourseDto, int, PagedResultRequestDto, CreateCourseDto, UpdateCourseDto>
    {
        Task<PagedResultDto<CourseDto>> GetAllCourses(GdPagedResultRequestDto input);
        Task<List<Dto.CourseDto>> GetAllCoursesForCombo();
        Task<List<Dto.CourseDto>> GetAllCoursesByLevelsForCombo(int LevelId);
    }
}
//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.TransactionQueues.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using Abp.Domain.Uow;
using System.Configuration;
using System.IO;
using Abp.Runtime.Caching;

namespace EasySchoolManager.TransactionQueues
{
    [AbpAuthorize(PermissionNames.Pages_TransactionQueues)]
    public class TransactionQueueAppService : AsyncCrudAppService<Models.TransactionQueues, TransactionQueueDto, long, PagedResultRequestDto, CreateTransactionQueueDto, UpdateTransactionQueueDto>, ITransactionQueueAppService
    {
        private const int TransactionTypeReceipt = 1;
        private const int ReceiptTypeStudentReceipt = 1;
        private const int ReceiptTypeGeneralReceipt = 2;
        private readonly IRepository<Models.TransactionQueues, long> _TransactionQueueRepository;
        private readonly IRepository<Models.Transactions, long> _transactionRepository;
        private readonly ICacheManager cacheManager;

        public TransactionQueueAppService(
            IRepository<Models.TransactionQueues, long> repository,
            IRepository<Models.TransactionQueues, long> TransactionQueueRepository,
            IRepository<Models.Transactions, long> transactionRepository,
            ICacheManager cacheManager
            )
            : base(repository)
        {
            _TransactionQueueRepository = TransactionQueueRepository;
            _transactionRepository = transactionRepository;
            this.cacheManager = cacheManager;
        }

        private static bool TryingToSendTransactionQueues = false;

        [AbpAllowAnonymous]
        [UnitOfWork(isTransactional: false)]
        public async Task TryToSend()
        {

            if (TryingToSendTransactionQueues == true)
                return;

            TryingToSendTransactionQueues = true;

            try
            {

                //Desabilitamos el filtro por tenant, para que se puedan buscar todos los tenants
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                {
                    var pendingsTransactions = _TransactionQueueRepository
                        .GetAllList(x => x.IsActive == true && x.Send == false)
                        .OrderBy(x => x.Id).ToList();

                    foreach (var item in pendingsTransactions)
                    {

                        item.Send = false;

                        /// /////////////////////////// 
                        /// /////////////////////////// 
                        /// TODO CONNECT OF WEB SERVICE
                        /// /////////////////////////// 
                        /// /////////////////////////// 

                        if (item.Send)
                        {
                            item.IsActive = false;
                            item.SendDate = DateTime.Now;
                        }
                        else
                        {
                            item.Retry = (item.Retry.HasValue ? item.Retry++ : 1);
                            item.LastTryDate = DateTime.Now;
                            int reTry = 25;
                            int.TryParse(ConfigurationManager.AppSettings["WSendTransactionQueuesRetry"], out reTry);
                            if (item.Retry >= reTry)
                            {
                                item.IsActive = false;
                            }
                            break;
                        }

                    }

                    CurrentUnitOfWork.SaveChanges();
                }
            }
            catch (Exception err)
            {
                Logger.Error("Error while was running the process TryingToSendTransactionQueues");
                Logger.Error(err.Message);
            }
            finally
            {
                TryingToSendTransactionQueues = false;
            }

        }

        private List<LedgerLineDto> GetLines(Models.Transactions t)
        {
            var retVal = new List<LedgerLineDto>();
            if (t.TransactionTypeId == TransactionTypeReceipt && t.ReceiptTypeId == ReceiptTypeStudentReceipt)
            {
                foreach (var ts in t.TransactionStudents)
                {
                    retVal.Add(GetLine(ts, null, null));
                }

            }
            else if (t.TransactionTypeId == TransactionTypeReceipt && t.ReceiptTypeId == ReceiptTypeGeneralReceipt && !t.ConceptId.HasValue)
            {
                foreach (var tc in t.TransactionConcepts)
                {
                    retVal.Add(GetLine(null, tc, null));
                }
            }
            else
            {
                retVal.Add(GetLine(null, null, t));
            }
            return retVal;
        }

        private string GetXMLFileContents(DateTime fromDate, DateTime toDate, int? tenantId)
        {
            var getTransactions = _transactionRepository
                    .GetAll()
                    .Where(f => f.Date > fromDate && f.Date < toDate);

            if (tenantId.HasValue && tenantId > 0)
            {
                getTransactions = getTransactions.Where(f => f.TenantId == tenantId);
            }

            var l = new List<LedgerLineDto>();
            foreach (var t in getTransactions)
            {
                l.AddRange(GetLines(t));
            }
            return ConvertToString(l);
        }

        private string ConvertToString(List<LedgerLineDto> l)
        {
            var lines = "";
            foreach (var item in l)
            {
                lines += GetLineString(item);
            }
            var retVal = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
            retVal += string.Format(@"
            <SSC>
              <User></User>
              <SunSystemsContext>
                <BusinessUnit>UDO</BusinessUnit>
              </SunSystemsContext>
              <MethodContext>
                <LedgerPostingParameters>
                  <AllowOverBudget>Y</AllowOverBudget>
                  <AllowPostToSuspended>N</AllowPostToSuspended>
                  <AllowBalTran>N</AllowBalTran>
                  <Description>Payroll</Description>
                  <JournalType>PAY</JournalType>
                  <PostProvisional>N</PostProvisional>
                  <PostToHold>N</PostToHold>
                  <PostingType>0</PostingType>
                  <Print>N</Print>
                  <ReportErrorsOnly>N</ReportErrorsOnly>
                  <ReportingAccount>338999</ReportingAccount>
                  <SuspenseAccount>338999</SuspenseAccount>
                  <TransactionAmountAccount>338999</TransactionAmountAccount>
                </LedgerPostingParameters>
              </MethodContext>
              <Payload>
                <Ledger>
                  {0}
                </Ledger>
              </Payload>
            </SSC>
            ", lines);

           
            return retVal;
        }

        public FileOutput DownloadFileXml(RequestDownloadDto input)
        {
            // <span style="white-space: normal;">Generate unique token based in a timestamp</span>
            var token = DateTime.Now.Ticks + ".xml";

            var memoryStream = new MemoryStream();

            // Generate the TXT file and write the content in the
            // in the "ms" stream
            TextWriter tw = new StreamWriter(memoryStream);
            tw.Write(GetXMLFileContents(input.FromDate, input.ToDate, input.TenantId));
            tw.Flush();

            // Storing the stream in the cache.
            // The third param sets to delete the object after 1 minute
            cacheManager.GetCache("xml").Set(
                    token,
                    memoryStream,
                    TimeSpan.FromMinutes(1));

            return new FileOutput
            {
                File = new FileDto()
                {
                    Token = token,
                    Filename = string.Format("File_From_{0}_To_{1}{2}.xml", input.FromDate.ToString("yyyyMMdd"), input.ToDate.ToString("yyyyMMdd"), (input.TenantId.HasValue ? "_T" + input.TenantId : ""))
                }
            };
        }

        private string GetLineString(LedgerLineDto l)
        {
            return string.Format(@"
                <Line>
                    <AccountCode>{0}</AccountCode>
                    <AccountingPeriod>{1}</AccountingPeriod>
                    <AnalysisCode1>{2}</AnalysisCode1>
                    <AnalysisCode3>{3}</AnalysisCode3>
                    <AnalysisCode4>{4}</AnalysisCode4>
                    <AnalysisCode5>{5}</AnalysisCode5>
                    <AnalysisCode6>{6}</AnalysisCode6>
                    <AnalysisCode7>{7}</AnalysisCode7>
                    <AnalysisCode8>{8}</AnalysisCode8>
                    <AnalysisCode9>{9}</AnalysisCode9>
                    <Value4CurrencyCode>{10}</Value4CurrencyCode>
                    <DebitCredit>{11}</DebitCredit>
                    <Description>{12}</Description>
                    <Value4Amount>{13}</Value4Amount>
                    <TransactionDate>{14}</TransactionDate>
                    <TransactionReference>{15}</TransactionReference>
                  </Line>
            ",
            l.AccountCode,
            l.AccountingPeriod,
            l.AnalysisCode1,
            l.AnalysisCode3,
            l.AnalysisCode4,
            l.AnalysisCode5,
            l.AnalysisCode6,
            l.AnalysisCode7,
            l.AnalysisCode8,
            l.AnalysisCode9,
            l.Value4CurrencyCode,
            l.DebitCredit,
            l.Description,
            l.Value4Amount,
            l.TransactionDate,
            l.TransactionReference);
        }

        private LedgerLineDto GetLine(Models.TransactionStudents ts, Models.TransactionConcepts tc, Models.Transactions t)
        {
            var l = new LedgerLineDto();
            if (ts != null)
            {
                l.AccountCode = ts.Transactions.Concepts.Catalogs.AccountNumber;
                //<AccountCode>102120</AccountCode> poner aqui: accountNumber que esta en catalogo, que viene del concepto, que esta en transaction.
                l.AccountingPeriod = ts.Transactions.CreationTime.ToString("0MMYYYY");
                //<AccountingPeriod>0122016</AccountingPeriod> poner aqui: mes y ano de la transaccion. 
                l.AnalysisCode1 = "UFNT";
                //<AnalysisCode1>PAY20161231</AnalysisCode1> Poner Aqui: siempre UFNT fijo.
                l.AnalysisCode3 = ts.Transactions.Tenant.Cities.Provinces.Regions.Code;
                //<AnalysisCode3>302</AnalysisCode3>poner aqui el codigo de la region, que viene de la provincia, que viene de la ciudad seleccionada en el tenant.
                l.AnalysisCode4 = ts.EnrollmentStudents.CourseEnrollmentStudents.FirstOrDefault().Courses.Levels.Code;
                //<AnalysisCode4></AnalysisCode4> poner aqui el codigo del nivel que esta el curso del estudiante.
                l.AnalysisCode5 = "0";
                //<AnalysisCode5></AnalysisCode5> poner aqui siempre un 0 (preguntar si siempre es cero o basio???preguntar)
                l.AnalysisCode5 = ts.Transactions.Tenant.Abbreviation;
                //<AnalysisCode6>UAMERI01</AnalysisCode6> poner aqui la abreviatura del tenant
                //l.AnalysisCode7 = ts.Transactions.Tenant.Abbreviation;
                //<AnalysisCode7></AnalysisCode7> preguntar que poner aqui el jueves
                //<AnalysisCode8></AnalysisCode8> preguntar que poner aqui el jueves
                l.AnalysisCode9 = ts.Transactions.Tenant.Districts.Abbreviation;
                //<AnalysisCode9>DTDISTR01</AnalysisCode9> poner aqui el campo Abbreviation de tabla distrito, que viene del tenant
                l.Value4CurrencyCode = "DOP1";
                //<Value4CurrencyCode>DOP1</Value4CurrencyCode> poner aqui siempre la moneda fija
                l.DebitCredit = ts.Transactions.Origins.Letter;
                //<DebitCredit>C</DebitCredit> poner aqui campo letter de tabla origin, que viene de transapcion OriginId
                l.Description = ts.Transactions.Concepts.Description;
                //<Description>12/31/2016 Payroll</Description> poner aqui campo descripcion de tabla concepto, que viene de transacion
                l.Value4Amount = ts.Amount.ToString();
                //<Value4Amount>809287.91</Value4Amount> poner aqui el monto de la transaccion sin separacion de miles. con punto decimal.
                l.TransactionDate = ts.Transactions.Date.ToString("ddMMyyyy");
                //<TransactionDate>31122016</TransactionDate> poner aqui la fecha en el formato seleccionado. 
                l.TransactionReference = ts.Transactions.Date.ToString("DOP1yyyyMMdd");
                //<TransactionReference>DOP120161231</TransactionReference> poner aqui la moneda con la fecha en el formato seleccionado. 

            }
            else if (tc != null)
            {
                l.AccountCode = tc.Concepts.Catalogs.AccountNumber;
                //<AccountCode>102120</AccountCode> poner aqui: accountNumber que esta en catalogo, que viene del concepto, que esta en transaction.
                l.AccountingPeriod = tc.Transactions.CreationTime.ToString("0MMYYYY");
                //<AccountingPeriod>0122016</AccountingPeriod> poner aqui: mes y ano de la transaccion. 
                l.AnalysisCode1 = "UFNT";
                //<AnalysisCode1>PAY20161231</AnalysisCode1> Poner Aqui: siempre UFNT fijo.
                l.AnalysisCode3 = tc.Transactions.Tenant.Cities.Provinces.Regions.Code;
                //<AnalysisCode3>302</AnalysisCode3>poner aqui el codigo de la region, que viene de la provincia, que viene de la ciudad seleccionada en el tenant.
                l.AnalysisCode4 = "";
                //<AnalysisCode4></AnalysisCode4> poner aqui el codigo del nivel que esta el curso del estudiante.
                l.AnalysisCode5 = "0";
                //<AnalysisCode5></AnalysisCode5> poner aqui siempre un 0 (preguntar si siempre es cero o basio???preguntar)
                l.AnalysisCode5 = tc.Transactions.Tenant.Abbreviation;
                //<AnalysisCode6>UAMERI01</AnalysisCode6> poner aqui la abreviatura del tenant
                //l.AnalysisCode7 = ts.Transactions.Tenant.Abbreviation;
                //<AnalysisCode7></AnalysisCode7> preguntar que poner aqui el jueves
                //<AnalysisCode8></AnalysisCode8> preguntar que poner aqui el jueves
                l.AnalysisCode9 = tc.Transactions.Tenant.Districts.Abbreviation;
                //<AnalysisCode9>DTDISTR01</AnalysisCode9> poner aqui el campo Abbreviation de tabla distrito, que viene del tenant
                l.Value4CurrencyCode = "DOP1";
                //<Value4CurrencyCode>DOP1</Value4CurrencyCode> poner aqui siempre la moneda fija
                l.DebitCredit = tc.Transactions.Origins.Letter;
                //<DebitCredit>C</DebitCredit> poner aqui campo letter de tabla origin, que viene de transapcion OriginId
                l.Description = tc.Transactions.Concepts.Description;
                //<Description>12/31/2016 Payroll</Description> poner aqui campo descripcion de tabla concepto, que viene de transacion
                l.Value4Amount = tc.Amount.ToString();
                //<Value4Amount>809287.91</Value4Amount> poner aqui el monto de la transaccion sin separacion de miles. con punto decimal.
                l.TransactionDate = tc.Transactions.Date.ToString("ddMMyyyy");
                //<TransactionDate>31122016</TransactionDate> poner aqui la fecha en el formato seleccionado. 
                l.TransactionReference = tc.Transactions.Date.ToString("DOP1yyyyMMdd");
                //<TransactionReference>DOP120161231</TransactionReference> poner aqui la moneda con la fecha en el formato seleccionado. 
            }
            else
            {
                l.AccountCode = t.Concepts.Catalogs.AccountNumber;
                //<AccountCode>102120</AccountCode> poner aqui: accountNumber que esta en catalogo, que viene del concepto, que esta en transaction.
                l.AccountingPeriod = t.CreationTime.ToString("0MMYYYY");
                //<AccountingPeriod>0122016</AccountingPeriod> poner aqui: mes y ano de la transaccion. 
                l.AnalysisCode1 = "UFNT";
                //<AnalysisCode1>PAY20161231</AnalysisCode1> Poner Aqui: siempre UFNT fijo.
                l.AnalysisCode3 = t.Tenant.Cities.Provinces.Regions.Code;
                //<AnalysisCode3>302</AnalysisCode3>poner aqui el codigo de la region, que viene de la provincia, que viene de la ciudad seleccionada en el tenant.
                l.AnalysisCode4 = "";
                //<AnalysisCode4></AnalysisCode4> poner aqui el codigo del nivel que esta el curso del estudiante.
                l.AnalysisCode5 = "0";
                //<AnalysisCode5></AnalysisCode5> poner aqui siempre un 0 (preguntar si siempre es cero o basio???preguntar)
                l.AnalysisCode5 = t.Tenant.Abbreviation;
                //<AnalysisCode6>UAMERI01</AnalysisCode6> poner aqui la abreviatura del tenant
                //l.AnalysisCode7 = ts.Transactions.Tenant.Abbreviation;
                //<AnalysisCode7></AnalysisCode7> preguntar que poner aqui el jueves
                //<AnalysisCode8></AnalysisCode8> preguntar que poner aqui el jueves
                l.AnalysisCode9 = t.Tenant.Districts.Abbreviation;
                //<AnalysisCode9>DTDISTR01</AnalysisCode9> poner aqui el campo Abbreviation de tabla distrito, que viene del tenant
                l.Value4CurrencyCode = "DOP1";
                //<Value4CurrencyCode>DOP1</Value4CurrencyCode> poner aqui siempre la moneda fija
                l.DebitCredit = t.Origins.Letter;
                //<DebitCredit>C</DebitCredit> poner aqui campo letter de tabla origin, que viene de transapcion OriginId
                l.Description = t.Concepts.Description;
                //<Description>12/31/2016 Payroll</Description> poner aqui campo descripcion de tabla concepto, que viene de transacion
                l.Value4Amount = t.Amount.ToString();
                //<Value4Amount>809287.91</Value4Amount> poner aqui el monto de la transaccion sin separacion de miles. con punto decimal.
                l.TransactionDate = t.Date.ToString("ddMMyyyy");
                //<TransactionDate>31122016</TransactionDate> poner aqui la fecha en el formato seleccionado. 
                l.TransactionReference = t.Date.ToString("DOP1yyyyMMdd");
                //<TransactionReference>DOP120161231</TransactionReference> poner aqui la moneda con la fecha en el formato seleccionado. 
            }
            return l;
        }

        public override async Task<TransactionQueueDto> Get(EntityDto<long> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<TransactionQueueDto> Create(CreateTransactionQueueDto input)
        {
            CheckCreatePermission();
            var TransactionQueue = ObjectMapper.Map<Models.TransactionQueues>(input);
            try
            {
                await _TransactionQueueRepository.InsertAsync(TransactionQueue);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(TransactionQueue);
        }
        public override async Task<TransactionQueueDto> Update(UpdateTransactionQueueDto input)
        {
            CheckUpdatePermission();
            var TransactionQueue = await _TransactionQueueRepository.GetAsync(input.Id);
            MapToEntity(input, TransactionQueue);
            try
            {
                await _TransactionQueueRepository.UpdateAsync(TransactionQueue);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<long> input)
        {
            try
            {
                var TransactionQueue = await _TransactionQueueRepository.GetAsync(input.Id);
                await _TransactionQueueRepository.DeleteAsync(TransactionQueue);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.TransactionQueues MapToEntity(CreateTransactionQueueDto createInput)
        {
            var TransactionQueue = ObjectMapper.Map<Models.TransactionQueues>(createInput);
            return TransactionQueue;
        }
        protected override void MapToEntity(UpdateTransactionQueueDto input, Models.TransactionQueues TransactionQueue)
        {
            ObjectMapper.Map(input, TransactionQueue);
        }
        protected override IQueryable<Models.TransactionQueues> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.TransactionQueues> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            //if (!string.IsNullOrEmpty(input.TextFilter)) 
            //    resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado;
        }
        protected override async Task<Models.TransactionQueues> GetEntityByIdAsync(long id)
        {
            var TransactionQueue = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(TransactionQueue);
        }
        protected override IQueryable<Models.TransactionQueues> ApplySorting(IQueryable<Models.TransactionQueues> query, PagedResultRequestDto input)
        {
            return query;
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<TransactionQueueDto>> GetAllTransactionQueues(GdPagedResultRequestDto input)
        {
            PagedResultDto<TransactionQueueDto> ouput = new PagedResultDto<TransactionQueueDto>();
            IQueryable<Models.TransactionQueues> query = query = from x in _TransactionQueueRepository.GetAll()
                                                                 select x;
            //if (!string.IsNullOrEmpty(input.TextFilter)) 
            //{ 
            //    query = from x in _TransactionQueueRepository.GetAll() 
            //            where x.Name.Contains(input.TextFilter) 
            //            select x; 
            //} 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<TransactionQueues.Dto.TransactionQueueDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<TransactionQueueDto>> GetAllTransactionQueuesForCombo()
        {
            var TransactionQueueList = await _TransactionQueueRepository.GetAllListAsync(x => x.IsActive == true);

            var TransactionQueue = ObjectMapper.Map<List<TransactionQueueDto>>(TransactionQueueList.ToList());

            return TransactionQueue;
        }

    }
}
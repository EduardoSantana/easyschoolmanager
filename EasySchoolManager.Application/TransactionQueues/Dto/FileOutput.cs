﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.TransactionQueues.Dto
{
    public class FileOutput
    {
        public FileDto File { get; set; }
    }
}

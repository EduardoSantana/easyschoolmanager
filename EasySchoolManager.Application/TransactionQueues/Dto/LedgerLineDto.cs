﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.TransactionQueues.Dto
{
    public class LedgerLineDto
    {
        public string AccountCode { get; set; }
        public string AccountingPeriod { get; set; }
        public string AnalysisCode1 { get; set; }
        public string AnalysisCode3 { get; set; }
        public string AnalysisCode4 { get; set; }
        public string AnalysisCode5 { get; set; }
        public string AnalysisCode6 { get; set; }
        public string AnalysisCode7 { get; set; }
        public string AnalysisCode8 { get; set; }
        public string AnalysisCode9 { get; set; }
        public string Value4CurrencyCode { get; set; }
        public string DebitCredit { get; set; }
        public string Description { get; set; }
        public string Value4Amount { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionReference { get; set; }
    }
}

﻿namespace EasySchoolManager.TransactionQueues.Dto
{
    public class FileDto
    {
        public string Token { get; set; }
        public string Filename { get; set; }
    }
}
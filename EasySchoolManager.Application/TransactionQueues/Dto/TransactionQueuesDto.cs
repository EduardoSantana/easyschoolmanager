//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.TransactionQueues.Dto 
{
        [AutoMap(typeof(Models.TransactionQueues))] 
        public class TransactionQueueDto : EntityDto<long> 
        {
              public int Id {get;set;} 
              public long TransactionId {get;set;} 
              public bool Send {get;set;} 
              public DateTime? SendDate {get;set;} 
              public DateTime? LastTryDate {get;set;} 
              public long? Retry {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
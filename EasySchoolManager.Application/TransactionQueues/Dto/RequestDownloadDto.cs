﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.TransactionQueues.Dto
{
    public class RequestDownloadDto
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int? TenantId { get; set; }
    }
}

//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.TransactionQueues.Dto;
using System.Collections.Generic;
using System;

namespace EasySchoolManager.TransactionQueues
{
	  public interface ITransactionQueueAppService : IAsyncCrudAppService<TransactionQueueDto, long, PagedResultRequestDto, CreateTransactionQueueDto, UpdateTransactionQueueDto>
	  {
			Task<PagedResultDto<TransactionQueueDto>> GetAllTransactionQueues(GdPagedResultRequestDto input);
			Task<List<Dto.TransactionQueueDto>> GetAllTransactionQueuesForCombo();
			FileOutput DownloadFileXml(RequestDownloadDto input);
			Task TryToSend();
	  }
}
﻿//Created from Templaste MG

using System;

namespace EasySchoolManager.TaxReceipts
{
    public class TaxReceiptResultDto
    {
        public string Sequence { get; set; }
        public DateTime ExpirationDate{ get; set; }
    }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper;
using EasySchoolManager.TaxReceiptTypes.Dto;
using EasySchoolManager.Regions.Dto;

namespace EasySchoolManager.TaxReceipts.Dto 
{
        [AutoMap(typeof(Models.TaxReceipts))] 
        public class TaxReceiptDto : EntityDto<int> 
        {
              public int TaxReceiptTypeId {get;set;} 
              public string Name {get;set;} 
              public long InitialNumber {get;set;} 
              public long FinalNumber {get;set;} 
              public long CurrentSequence {get;set;} 
              public int RegionId {get;set;} 
              public string Serie {get;set;} 
              public String Base {get;set;} 
              public DateTime ExpirationDate {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 
              public String DisplayName {
              get { return string.Join(" - " , TaxReceiptTypes?.Name, this.Serie) ; } }
              public TaxReceiptTypeDto TaxReceiptTypes { get; set; }
              public RegionDto Regions { get; set; }

         }
}
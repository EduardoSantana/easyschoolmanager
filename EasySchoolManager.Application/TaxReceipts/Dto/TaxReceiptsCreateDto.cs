//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.TaxReceipts.Dto 
{
        [AutoMap(typeof(Models.TaxReceipts))] 
        public class CreateTaxReceiptDto : EntityDto<int> 
        {

              public int TaxReceiptTypeId {get;set;} 

              [StringLength(50)] 
              public string Name {get;set;} 

              public long InitialNumber {get;set;} 

              public long FinalNumber {get;set;} 

              public long CurrentSequence {get;set;} 

              public int RegionId {get;set;} 

              [StringLength(1)] 
              public string Serie {get;set;} 

              [StringLength(50)] 
              public String Base {get;set;} 

              public DateTime ExpirationDate {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
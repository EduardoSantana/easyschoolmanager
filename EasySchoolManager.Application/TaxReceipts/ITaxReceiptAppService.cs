//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.TaxReceipts.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.TaxReceipts
{
    public interface ITaxReceiptAppService : IAsyncCrudAppService<TaxReceiptDto, int, PagedResultRequestDto, CreateTaxReceiptDto, UpdateTaxReceiptDto>
    {
        Task<PagedResultDto<TaxReceiptDto>> GetAllTaxReceipts(GdPagedResultRequestDto input);
        Task<List<Dto.TaxReceiptDto>> GetAllTaxReceiptsForCombo();
        Task<TaxReceiptResultDto> GetCurrentSecuenceAndIncrement(int TaxReceiptId);
        Task<bool> IncrementSequence(int TaxReceiptId, int RegionId);
        Task<List<TaxReceiptDto>> GetAllTaxReceiptsByRegion();
    }
}
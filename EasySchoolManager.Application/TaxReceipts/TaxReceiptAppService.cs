//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.TaxReceipts.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.TaxReceipts 
{ 
    [AbpAuthorize(PermissionNames.Pages_TaxReceipts)] 
    public class TaxReceiptAppService : AsyncCrudAppService<Models.TaxReceipts, TaxReceiptDto, int, PagedResultRequestDto, CreateTaxReceiptDto, UpdateTaxReceiptDto>, ITaxReceiptAppService 
    { 
        private readonly IRepository<Models.TaxReceipts, int> _taxReceiptRepository;
		
		    private readonly IRepository<Models.TaxReceiptTypes, int> _taxReceiptTypeRepository;
		    private readonly IRepository<Models.Regions, int> _regionRepository;

        private readonly MultiTenancy.TenantManager _tenantManager;


        public TaxReceiptAppService( 
            IRepository<Models.TaxReceipts, int> repository, 
            IRepository<Models.TaxReceipts, int> taxReceiptRepository ,
            IRepository<Models.TaxReceiptTypes, int> taxReceiptTypeRepository,
            MultiTenancy.TenantManager tenantManager,
            IRepository<Models.Regions, int> regionRepository

            ) 
            : base(repository) 
        { 
            _taxReceiptRepository = taxReceiptRepository; 
			
            _taxReceiptTypeRepository = taxReceiptTypeRepository;

            _regionRepository = regionRepository;

            _tenantManager = tenantManager;

            LocalizationSourceName = EasySchoolManagerConsts.LocalizationSourceName;
        } 
        public override async Task<TaxReceiptDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 

        public override async Task<TaxReceiptDto> Create(CreateTaxReceiptDto input) 
        { 
            CheckCreatePermission(); 
            var taxReceipt = ObjectMapper.Map<Models.TaxReceipts>(input); 


			try{


                if (input.FinalNumber < input.InitialNumber)
                    throw new Exception(L("YouCanNotCreateATaxReceiptWithAFinalNumberLessThanTheInitialNumber"));

                if (input.CurrentSequence < input.InitialNumber)
                    throw new Exception(L("YouCanNotCreateATaxReceiptWithACurrentSequenceLessThanTheInitialNumber"));

                if (input.CurrentSequence  >  input.FinalNumber)
                    throw new Exception(L("YouCanNotCreateATaxReceiptWithAFinalNumberLessThanTheCurrentSequence"));




                var lasts = _taxReceiptRepository.GetAllList().
                    Where(x => x.TaxReceiptTypeId == input.TaxReceiptTypeId && x.RegionId == input.RegionId).
                    OrderByDescending(x => x.ExpirationDate).ToList();

                foreach (var current in lasts)
                {
                    //No puede poner un comprobante con una fecha menor o igual a otro comprobante en la misma region.
                    if (input.ExpirationDate <= current.ExpirationDate)
                    {
                        throw new Exception(L("YouCanNotAppendATaxReceiptThatContainAnExpirationDateLessThanAnotherExistingInTheSameRegion"));
                    }
                    else
                    {
                        //Si el comprobante aun no ha expirado, no se puede asignar una secuencia que exista en otro comprobante.
                        if (current.FinalNumber >= input.InitialNumber && current.ExpirationDate > DateTime.Now)
                            throw new Exception(L("YouCanNoAppendATaxRecepitWithAnInitialNumberLessOrEqualTheFinalNumberOfAnExistingTaxReceipt"));

                        //Si el comprobante aun no ha expirado, no se puede asignar una secuencia que exista en otro comprobante.
                        if (current.CurrentSequence >= input.InitialNumber && current.ExpirationDate < DateTime.Now)
                            throw new Exception(L("YouCanNoAppendATaxRecepitWithAnInitialNumberLessOrEqualTheCurrentSequenceOfAnExistingTaxReceipt"));
                    }
                }
              await _taxReceiptRepository.InsertAsync(taxReceipt); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(taxReceipt); 
        } 


        public override async Task<TaxReceiptDto> Update(UpdateTaxReceiptDto input) 
        { 
            CheckUpdatePermission(); 
            var taxReceipt = await _taxReceiptRepository.GetAsync(input.Id);
            MapToEntity(input, taxReceipt); 
		    try{

                if (input.FinalNumber <= input.CurrentSequence)
                    throw new Exception("YouCanNotAssignAFinalNumberLessOrEqualTotheCurrentSequence");

               await _taxReceiptRepository.UpdateAsync(taxReceipt); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var taxReceipt = await _taxReceiptRepository.GetAsync(input.Id); 
               await _taxReceiptRepository.DeleteAsync(taxReceipt);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.TaxReceipts MapToEntity(CreateTaxReceiptDto createInput) 
        { 
            var taxReceipt = ObjectMapper.Map<Models.TaxReceipts>(createInput); 
            return taxReceipt; 
        } 
        protected override void MapToEntity(UpdateTaxReceiptDto input, Models.TaxReceipts taxReceipt) 
        { 
            ObjectMapper.Map(input, taxReceipt); 
        } 
        protected override IQueryable<Models.TaxReceipts> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.TaxReceipts> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.TaxReceipts> GetEntityByIdAsync(int id) 
        { 
            var taxReceipt = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(taxReceipt); 
        } 
        protected override IQueryable<Models.TaxReceipts> ApplySorting(IQueryable<Models.TaxReceipts> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        }

        /// <summary>
        /// Metodo usado para obtener la secuencia del comprobante fiscal, para una serie, tipo y region dada.
        /// </summary>
        /// <param name="TaxReceiptTypeId">Tipo de comprobante fiscal</param>
        /// <param name="RegionId">Regi�n a la cual pertenece el colegio</param>
        /// <param name="Serie">Serie del comprobante fiscal ( al momento 12 09 2018 la serie era "B")</param>
        /// <returns>Devuelve el Numero de Comprobante Fiscal Ejemplo "B0100000025" </returns>
        public async Task<TaxReceiptResultDto> GetCurrentSecuenceAndIncrement(int TaxReceiptId)
        {

            TaxReceiptResultDto tc = new TaxReceiptResultDto();
            var TaxReceipts = await _taxReceiptRepository.GetAllListAsync(x => x.Id == TaxReceiptId);

            var TaxReceipt = TaxReceipts.FirstOrDefault();
            
            //Verificamos si el tipo de comprobante fiscal existe
            if (TaxReceipt == null)
                throw new Exception(L("TheTaxReceiptSelectedDoestNotExistsInTheCurrentRegion"));

            if (TaxReceipt.ExpirationDate < DateTime.Now)
                throw new Exception(L("TheTaxReceiptSelectedHasAnExpiredDateAndYouCanNotUseItToGenerateReceipts"));

            //Verificamos si hay sequencia disponible
            if (TaxReceipt.FinalNumber < TaxReceipt.CurrentSequence)
                throw new Exception(L("ThereIsNotSecuenceAvailableToGenerateSecuenceForTheSelectedTaxReceiptType"));

            string number = string.Format("{0:00000000}", TaxReceipt.CurrentSequence);
            var taxReceiptType = _taxReceiptTypeRepository.Get(TaxReceipt.TaxReceiptTypeId);

            tc.Sequence = string.Join("", TaxReceipt.Serie, taxReceiptType.Code, number);
            TaxReceipt.CurrentSequence++;
            tc.ExpirationDate = TaxReceipt.ExpirationDate;

            return tc;
        }


        public async Task<bool> IncrementSequence(int TaxReceiptId, int RegionId)
        {
            bool incremented = false;
            var TaxReceipts = await _taxReceiptRepository.GetAllListAsync(x => x.RegionId == RegionId && x.Id == TaxReceiptId);

            var TaxReceipt = TaxReceipts.FirstOrDefault();

            //Verificamos si el tipo de comprobante fiscal existe
            if (TaxReceipt == null)
                throw new Exception(L("TheTaxReceiptSelectedDoestNotExistsInTheCurrentRegion"));

            //Verificamos si hay sequencia disponible
            if (TaxReceipt.FinalNumber < TaxReceipt.CurrentSequence)
                throw new Exception(L("ThereIsNotSecuenceAvailableToGenerateSecuenceForTheSelectedTaxReceiptType"));

            TaxReceipt.CurrentSequence++;
            CurrentUnitOfWork.SaveChanges();
            return incremented;
        }



        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<TaxReceiptDto>> GetAllTaxReceipts(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<TaxReceiptDto> ouput = new PagedResultDto<TaxReceiptDto>(); 
            IQueryable<Models.TaxReceipts> query = query = from x in _taxReceiptRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _taxReceiptRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<TaxReceipts.Dto.TaxReceiptDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<TaxReceiptDto>> GetAllTaxReceiptsForCombo()
        {
            var taxReceiptList = await _taxReceiptRepository.GetAllListAsync(x => x.IsActive == true);

            var taxReceipt = ObjectMapper.Map<List<TaxReceiptDto>>(taxReceiptList.ToList());

            return taxReceipt;
        }

        [AbpAllowAnonymous]
        public async Task<List<TaxReceiptDto>> GetAllTaxReceiptsByRegion()
        {
            int RegionId = 0;

            var tm = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
            
            RegionId = tm.Cities.Provinces.RegionId;

            var taxReceiptList = await _taxReceiptRepository.GetAllListAsync(x => 
                    x.IsActive == true &&
                    x.RegionId == RegionId && 
                    x.ExpirationDate >= DateTime.Now &&
                    x.CurrentSequence <= x.FinalNumber);
            List<TaxReceiptDto> taxReceipt = new List<TaxReceiptDto>();
            foreach (var item in taxReceiptList)
            {
                if (!taxReceipt.Any(x => x.TaxReceiptTypeId == item.TaxReceiptTypeId))
                {
                    taxReceipt.Add(ObjectMapper.Map<TaxReceiptDto>(item));
                }
            }
            return taxReceipt;
        }

    } 
} ;
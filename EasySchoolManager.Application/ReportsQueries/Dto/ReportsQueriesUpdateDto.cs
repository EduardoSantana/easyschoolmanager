//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.ReportsQueries.Dto 
{
        [AutoMap(typeof(Models.ReportsQueries))] 
        public class UpdateReportsQueryDto : EntityDto<int> 
        {
              public int Id { get; set; }
              [StringLength(20)]  
              public string ValueId {get;set;} 

              [StringLength(40)]  
              public string ValueDisplay {get;set;} 

              public int ReportsFiltersId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
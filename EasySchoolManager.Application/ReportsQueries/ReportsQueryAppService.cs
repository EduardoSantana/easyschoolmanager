//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.ReportsQueries.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ReportsQueries 
{ 
    [AbpAuthorize(PermissionNames.Pages_ReportsQueries)] 
    public class ReportsQueryAppService : AsyncCrudAppService<Models.ReportsQueries, ReportsQueryDto, int, PagedResultRequestDto, CreateReportsQueryDto, UpdateReportsQueryDto>, IReportsQueryAppService 
    { 
        private readonly IRepository<Models.ReportsQueries, int> _reportsQueryRepository;
		


        public ReportsQueryAppService( 
            IRepository<Models.ReportsQueries, int> repository, 
            IRepository<Models.ReportsQueries, int> reportsQueryRepository 
            ) 
            : base(repository) 
        { 
            _reportsQueryRepository = reportsQueryRepository; 
			

			
        } 
        public override async Task<ReportsQueryDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ReportsQueryDto> Create(CreateReportsQueryDto input) 
        { 
            CheckCreatePermission(); 
            var reportsQuery = ObjectMapper.Map<Models.ReportsQueries>(input); 
			try{
              await _reportsQueryRepository.InsertAsync(reportsQuery); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(reportsQuery); 
        } 
        public override async Task<ReportsQueryDto> Update(UpdateReportsQueryDto input) 
        { 
            CheckUpdatePermission(); 
            var reportsQuery = await _reportsQueryRepository.GetAsync(input.Id);
            MapToEntity(input, reportsQuery); 
		    try{
               await _reportsQueryRepository.UpdateAsync(reportsQuery); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var reportsQuery = await _reportsQueryRepository.GetAsync(input.Id); 
               await _reportsQueryRepository.DeleteAsync(reportsQuery);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.ReportsQueries MapToEntity(CreateReportsQueryDto createInput) 
        { 
            var reportsQuery = ObjectMapper.Map<Models.ReportsQueries>(createInput); 
            return reportsQuery; 
        } 
        protected override void MapToEntity(UpdateReportsQueryDto input, Models.ReportsQueries reportsQuery) 
        { 
            ObjectMapper.Map(input, reportsQuery); 
        } 
        protected override IQueryable<Models.ReportsQueries> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ReportsQueries> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.ValueDisplay.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ReportsQueries> GetEntityByIdAsync(int id) 
        { 
            var reportsQuery = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(reportsQuery); 
        } 
        protected override IQueryable<Models.ReportsQueries> ApplySorting(IQueryable<Models.ReportsQueries> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.ValueDisplay); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ReportsQueryDto>> GetAllReportsQueries(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ReportsQueryDto> ouput = new PagedResultDto<ReportsQueryDto>(); 
            IQueryable<Models.ReportsQueries> query = query = from x in _reportsQueryRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _reportsQueryRepository.GetAll() 
                        where x.ValueDisplay.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ReportsQueries.Dto.ReportsQueryDto>>(query.ToList()); 
            return ouput; 
        }
        [AbpAllowAnonymous]
        public async Task<List<ReportsQueryDto>> GetAllReportsQueriesForCombo()
        {
            var reportsQueryList = await _reportsQueryRepository.GetAllListAsync(x => x.IsActive == true);

            var reportsQuery = ObjectMapper.Map<List<ReportsQueryDto>>(reportsQueryList.ToList());

            return reportsQuery;
        }
		
    } 
} ;
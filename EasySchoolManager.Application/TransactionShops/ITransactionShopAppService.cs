//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.TransactionShops.Dto;
using System.Collections.Generic;
using EasySchoolManager.Transactions.Dto;

namespace EasySchoolManager.TransactionShops
{
    public interface ITransactionShopAppService : IAsyncCrudAppService<TransactionShopDto, long, PagedResultRequestDto, CreateTransactionShopDto, UpdateTransactionShopDto>
    {
        Task<PagedResultDto<TransactionShopDto>> GetAllTransactionShops(GdPagedResultRequestDto input);
        Task<List<Dto.TransactionShopDto>> GetAllTransactionShopsForCombo();
        Task<List<List<ArticleShops.Dto.ArticleShopDto>>> GetAllArticlesShops();
        Task<TransactionReportDto> CreateAndPrint(CreateTransactionShopDto input);
    }
}
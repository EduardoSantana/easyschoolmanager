//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.TransactionShops.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.ArticleShops.Dto;
using Abp.Domain.Uow;
using EasySchoolManager.Reports.Dto;
using EasySchoolManager.ReportsFilters.Dto;
using EasySchoolManager.Transactions.Dto;
using EasySchoolManager.Reports;

namespace EasySchoolManager.TransactionShops
{
    [AbpAuthorize(PermissionNames.Pages_TransactionShops)]
    public class TransactionShopAppService : AsyncCrudAppService<Models.TransactionShops, TransactionShopDto, long, PagedResultRequestDto, CreateTransactionShopDto, UpdateTransactionShopDto>, ITransactionShopAppService
    {
        private readonly IRepository<Models.TransactionShops, long> _transactionShopRepository;
        private readonly IRepository<Models.ClientShops, int> _clientShopRepository;
        private readonly IRepository<Models.TransactionArticleShops, int> _transactionArticleShopRepository;
        private readonly IRepository<Models.InventoryShops, int> _inventoryShopRepository;
        private readonly IRepository<Models.ArticleShops, int> _articleShopRepository;
        private readonly IReportAppService _reportService;

        public TransactionShopAppService(
            IRepository<Models.TransactionShops, long> repository,
            IRepository<Models.TransactionShops, long> transactionShopRepository,
            IRepository<Models.ClientShops, int> clientShopRepository,
            IRepository<Models.TransactionArticleShops, int> transactionArticleShopRepository,
            IRepository<Models.InventoryShops, int> inventoryShopRepository,
             IRepository<Models.ArticleShops, int> articleShopRepository,
             IReportAppService reportService
            )
            : base(repository)
        {
            _transactionShopRepository = transactionShopRepository;

            _clientShopRepository = clientShopRepository;

            _transactionArticleShopRepository = transactionArticleShopRepository;

            _inventoryShopRepository = inventoryShopRepository;

            _articleShopRepository = articleShopRepository;

            _reportService = reportService;
        }
        public override async Task<TransactionShopDto> Get(EntityDto<long> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<TransactionShopDto> Create(CreateTransactionShopDto input)
        {
            CheckCreatePermission();
            var transactionShop = ObjectMapper.Map<Models.TransactionShops>(input);
            transactionShop.Sequence = getTransactionSequence(transactionShop.TransactionTypeId);
            try
            {
                await _transactionShopRepository.InsertAsync(transactionShop);
                if (input.ArticleShops != null)
                    foreach (var art in input.ArticleShops)
                    {
                        Models.TransactionArticleShops ta = new Models.TransactionArticleShops();
                        ta.ArticleShopId = art.Id;
                        ta.Quantity = art.Quantity.Value;
                        ta.Amount = art.Price;
                        ta.Total = art.Total.Value;
                        ta.TransactionShopId = transactionShop.Id;
                        ta.IsActive = true;
                        _transactionArticleShopRepository.Insert(ta);

                        DecreaseInventory(art);
                    }

            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(transactionShop);
        }

        private void DecreaseInventory(ArticleShopDto art)
        {

            var inventory = _inventoryShopRepository.
                 GetAllList(x => x.ArticleShopId == art.Id && x.TenantId == AbpSession.TenantId).FirstOrDefault();
            if (inventory == null)
            {
                inventory = new Models.InventoryShops();
                inventory.ArticleShopId = art.Id;
                inventory.Existence = 0;
                inventory.IsActive = true;
                _inventoryShopRepository.Insert(inventory);
                CurrentUnitOfWork.SaveChanges();
            }

            inventory.Existence -= art.Quantity.Value;
            CurrentUnitOfWork.SaveChanges();

        }

        public int getTransactionSequence(int TransactionTypeId)
        {
            int sequence = 1;
            try
            {
                sequence = _transactionShopRepository.GetAllList(x => x.TransactionTypeId == TransactionTypeId).Max(x => x.Sequence) + 1;
            }
            catch
            {
            }

            return sequence;
        }

        public override async Task<TransactionShopDto> Update(UpdateTransactionShopDto input)
        {
            CheckUpdatePermission();
            var transactionShop = await _transactionShopRepository.GetAsync(input.Id);
            MapToEntity(input, transactionShop);
            try
            {
                await _transactionShopRepository.UpdateAsync(transactionShop);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<long> input)
        {
            try
            {
                var transactionShop = await _transactionShopRepository.GetAsync(input.Id);
                await _transactionShopRepository.DeleteAsync(transactionShop);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.TransactionShops MapToEntity(CreateTransactionShopDto createInput)
        {
            var transactionShop = ObjectMapper.Map<Models.TransactionShops>(createInput);
            return transactionShop;
        }
        protected override void MapToEntity(UpdateTransactionShopDto input, Models.TransactionShops transactionShop)
        {
            ObjectMapper.Map(input, transactionShop);
        }
        protected override IQueryable<Models.TransactionShops> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.TransactionShops> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.TransactionShops> GetEntityByIdAsync(long id)
        {
            var transactionShop = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(transactionShop);
        }
        protected override IQueryable<Models.TransactionShops> ApplySorting(IQueryable<Models.TransactionShops> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Name);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<TransactionShopDto>> GetAllTransactionShops(GdPagedResultRequestDto input)
        {
            PagedResultDto<TransactionShopDto> ouput = new PagedResultDto<TransactionShopDto>();
            IQueryable<Models.TransactionShops> query = query = from x in _transactionShopRepository.GetAll()
                                                                select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _transactionShopRepository.GetAll()
                        where x.Name.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<TransactionShops.Dto.TransactionShopDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<TransactionShopDto>> GetAllTransactionShopsForCombo()
        {
            var transactionShopList = await _transactionShopRepository.GetAllListAsync(x => x.IsActive == true);

            var transactionShop = ObjectMapper.Map<List<TransactionShopDto>>(transactionShopList.ToList());

            return transactionShop;
        }

        public async Task<List<List<ArticleShopDto>>> GetAllArticlesShops()
        {
            int NumberOfLines = 3;
            int ItemsPerLines = 15;

            List<List<ArticleShopDto>> ListOfList = new List<List<ArticleShopDto>>();

            var query = await _articleShopRepository.GetAllListAsync(x => x.IsActive == true
            && x.ButtonPositionId.HasValue == true);

            var articlesShops = query.Take(NumberOfLines * ItemsPerLines).ToList();

            for (int i = 0; i < NumberOfLines; i++)
            {
                var listax = new List<ArticleShopDto>();
                for (int j = 0; j < ItemsPerLines; j++)
                {
                    var elementToAdd = ((i * ItemsPerLines) + j);
                    if (elementToAdd >= articlesShops.Count)
                        break;
                    listax.Add(ObjectMapper.Map<ArticleShopDto>( articlesShops[elementToAdd]));
                }

                ListOfList.Add(listax);
            }
            return ListOfList;
        }

        [UnitOfWork(IsDisabled = true)]
        public async Task<TransactionReportDto> CreateAndPrint(CreateTransactionShopDto input)
        {
            var retVal = await Create(input);

            var _reportCode = "62"; //This is the code by default of the tenant
            TransactionReportDto reportString = null;
            try
            {
                var report = new _ReportDto
                {
                    ReportCode = _reportCode,
                    ReportExport = "Pdf",
                    ReportsFilters = new List<_ReportsFiltersDto>(new _ReportsFiltersDto[] {
                         new _ReportsFiltersDto{ Name= "TransactionShopId", Value = retVal.Id.ToString() }
                     })
                };

                var reportResult = (byte[])_reportService.GetDynamicReport(report, out string mimeType);
                reportString = new TransactionReportDto
                {
                    Id = retVal.Id,
                    reportString = Convert.ToBase64String(reportResult)
                };
            }
            catch (Exception err)
            {
                Logger.Error(err.Message);
                throw new UserFriendlyException(err.Message);
            }
            return reportString;
        }

    }
};
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.TransactionShops.Dto 
{
        [AutoMap(typeof(Models.TransactionShops))] 
        public class UpdateTransactionShopDto : EntityDto<long> 
        {

              public int Sequence {get;set;} 

              [StringLength(20)]  
              public string Number {get;set;} 

              public DateTime Date {get;set;} 

              [StringLength(100)]  
              public string Name {get;set;} 

              public int ClientShopId {get;set;} 

              public int? PaymentMethodId {get;set;} 

              public int OriginId {get;set;} 

              public int TransactionTypeId {get;set;}
              public decimal Amount { get; set; }
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
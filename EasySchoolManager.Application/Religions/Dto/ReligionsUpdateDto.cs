//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Religions.Dto 
{
        [AutoMap(typeof(Models.Religions))] 
        public class UpdateReligionDto : EntityDto<int> 
        {

              [StringLength(50)]  
              public String ShortName {get;set;} 

              [StringLength(120)]  
              public String LongName {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
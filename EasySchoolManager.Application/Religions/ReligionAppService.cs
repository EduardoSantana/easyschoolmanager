//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Religions.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Religions 
{ 
    [AbpAuthorize(PermissionNames.Pages_Religions)] 
    public class ReligionAppService : AsyncCrudAppService<Models.Religions, ReligionDto, int, PagedResultRequestDto, CreateReligionDto, UpdateReligionDto>, IReligionAppService 
    { 
        private readonly IRepository<Models.Religions, int> _religionRepository;
		


        public ReligionAppService( 
            IRepository<Models.Religions, int> repository, 
            IRepository<Models.Religions, int> religionRepository 
            ) 
            : base(repository) 
        { 
            _religionRepository = religionRepository; 
			

			
        } 
        public override async Task<ReligionDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ReligionDto> Create(CreateReligionDto input) 
        { 
            CheckCreatePermission(); 
            var religion = ObjectMapper.Map<Models.Religions>(input); 
			try{
              await _religionRepository.InsertAsync(religion); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(religion); 
        } 
        public override async Task<ReligionDto> Update(UpdateReligionDto input) 
        { 
            CheckUpdatePermission(); 
            var religion = await _religionRepository.GetAsync(input.Id);
            MapToEntity(input, religion); 
		    try{
               await _religionRepository.UpdateAsync(religion); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var religion = await _religionRepository.GetAsync(input.Id); 
               await _religionRepository.DeleteAsync(religion);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Religions MapToEntity(CreateReligionDto createInput) 
        { 
            var religion = ObjectMapper.Map<Models.Religions>(createInput); 
            return religion; 
        } 
        protected override void MapToEntity(UpdateReligionDto input, Models.Religions religion) 
        { 
            ObjectMapper.Map(input, religion); 
        } 
        protected override IQueryable<Models.Religions> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Religions> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.ShortName.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Religions> GetEntityByIdAsync(int id) 
        { 
            var religion = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(religion); 
        } 
        protected override IQueryable<Models.Religions> ApplySorting(IQueryable<Models.Religions> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.ShortName); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ReligionDto>> GetAllReligions(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ReligionDto> ouput = new PagedResultDto<ReligionDto>(); 
            IQueryable<Models.Religions> query = query = from x in _religionRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _religionRepository.GetAll() 
                        where x.ShortName.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Religions.Dto.ReligionDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<ReligionDto>> GetAllReligionsForCombo()
        {
            var religionList = await _religionRepository.GetAllListAsync(x => x.IsActive == true);

            var religion = ObjectMapper.Map<List<ReligionDto>>(religionList.ToList());

            return religion;
        }
		
    } 
} ;
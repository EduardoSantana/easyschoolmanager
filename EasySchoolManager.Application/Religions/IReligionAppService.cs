//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Religions.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Religions
{
      public interface IReligionAppService : IAsyncCrudAppService<ReligionDto, int, PagedResultRequestDto, CreateReligionDto, UpdateReligionDto>
      {
            Task<PagedResultDto<ReligionDto>> GetAllReligions(GdPagedResultRequestDto input);
			Task<List<Dto.ReligionDto>> GetAllReligionsForCombo();
      }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.CancelStatus.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.CancelStatus 
{ 
    [AbpAuthorize(PermissionNames.Pages_CancelStatus)] 
    public class CancelStatuAppService : AsyncCrudAppService<Models.CancelStatus, CancelStatuDto, int, PagedResultRequestDto, CreateCancelStatuDto, UpdateCancelStatuDto>, ICancelStatuAppService 
    { 
        private readonly IRepository<Models.CancelStatus, int> _cancelStatuRepository;
		


        public CancelStatuAppService( 
            IRepository<Models.CancelStatus, int> repository, 
            IRepository<Models.CancelStatus, int> cancelStatuRepository 
            ) 
            : base(repository) 
        { 
            _cancelStatuRepository = cancelStatuRepository; 
			

			
        } 
        public override async Task<CancelStatuDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<CancelStatuDto> Create(CreateCancelStatuDto input) 
        { 
            CheckCreatePermission(); 
            var cancelStatu = ObjectMapper.Map<Models.CancelStatus>(input); 
			try{
              await _cancelStatuRepository.InsertAsync(cancelStatu); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(cancelStatu); 
        } 
        public override async Task<CancelStatuDto> Update(UpdateCancelStatuDto input) 
        { 
            CheckUpdatePermission(); 
            var cancelStatu = await _cancelStatuRepository.GetAsync(input.Id);
            MapToEntity(input, cancelStatu); 
		    try{
               await _cancelStatuRepository.UpdateAsync(cancelStatu); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var cancelStatu = await _cancelStatuRepository.GetAsync(input.Id); 
               await _cancelStatuRepository.DeleteAsync(cancelStatu);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.CancelStatus MapToEntity(CreateCancelStatuDto createInput) 
        { 
            var cancelStatu = ObjectMapper.Map<Models.CancelStatus>(createInput); 
            return cancelStatu; 
        } 
        protected override void MapToEntity(UpdateCancelStatuDto input, Models.CancelStatus cancelStatu) 
        { 
            ObjectMapper.Map(input, cancelStatu); 
        } 
        protected override IQueryable<Models.CancelStatus> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.CancelStatus> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.CancelStatus> GetEntityByIdAsync(int id) 
        { 
            var cancelStatu = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(cancelStatu); 
        } 
        protected override IQueryable<Models.CancelStatus> ApplySorting(IQueryable<Models.CancelStatus> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Description); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<CancelStatuDto>> GetAllCancelStatus(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<CancelStatuDto> ouput = new PagedResultDto<CancelStatuDto>(); 
            IQueryable<Models.CancelStatus> query = query = from x in _cancelStatuRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _cancelStatuRepository.GetAll() 
                        where x.Description.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<CancelStatus.Dto.CancelStatuDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<CancelStatuDto>> GetAllCancelStatusForCombo()
        {
            var cancelStatuList = await _cancelStatuRepository.GetAllListAsync(x => x.IsActive == true);

            var cancelStatu = ObjectMapper.Map<List<CancelStatuDto>>(cancelStatuList.ToList());

            return cancelStatu;
        }
		
    } 
} ;
//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.CancelStatus.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.CancelStatus
{
      public interface ICancelStatuAppService : IAsyncCrudAppService<CancelStatuDto, int, PagedResultRequestDto, CreateCancelStatuDto, UpdateCancelStatuDto>
      {
            Task<PagedResultDto<CancelStatuDto>> GetAllCancelStatus(GdPagedResultRequestDto input);
			Task<List<Dto.CancelStatuDto>> GetAllCancelStatusForCombo();
      }
}
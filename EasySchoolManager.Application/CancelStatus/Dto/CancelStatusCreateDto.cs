//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.CancelStatus.Dto 
{
        [AutoMap(typeof(Models.CancelStatus))] 
        public class CreateCancelStatuDto : EntityDto<int> 
        {

              [StringLength(200)] 
              public string Description {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
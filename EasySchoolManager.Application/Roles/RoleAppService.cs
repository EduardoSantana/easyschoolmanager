﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.UI;
using EasySchoolManager.Authorization;
using EasySchoolManager.Authorization.Roles;
using EasySchoolManager.Authorization.Users;
using EasySchoolManager.Roles.Dto;
using Microsoft.AspNet.Identity;
using EasySchoolManager.MultiTenancy;
using EasySchoolManager.Helpers;
using Abp.Domain.Uow;
using System;
using EasySchoolManager.Authorization.Permissions;

namespace EasySchoolManager.Roles
{
    [AbpAuthorize(PermissionNames.Pages_Roles)]
    public class RoleAppService : AsyncCrudAppService<Role, RoleDto, int, PagedResultRequestDto, CreateRoleDto, RoleDto>, IRoleAppService
    {
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly TenantManager _tenantManager;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IPermissionAppService _permisionService;

        public RoleAppService(
            IRepository<Role> repository,
            RoleManager roleManager,
            UserManager userManager,
            IRepository<User, long> userRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<Role> roleRepository,
            TenantManager tenantManager,
            IPermissionAppService permisionService)
            : base(repository)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _tenantManager = tenantManager;
            _permisionService = permisionService;
        }

        [AbpAllowAnonymous]
        public override async Task<RoleDto> Create(CreateRoleDto input)
        {
            CheckCreatePermission();

            var role = ObjectMapper.Map<Role>(input);
            role.AffectTenants = true;

            CheckErrors(await _roleManager.CreateAsync(role));
            CurrentUnitOfWork.SaveChanges();

            //Busco todos los permisos que tiene asignnado el rol que se esta creando.
            var grantedPermissions = PermissionManager
                .GetAllPermissions()
                .Where(p => input.Permissions.Contains(p.Name))
                .ToList();

            await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);

            CurrentUnitOfWork.SaveChanges();

            List<Role> AllRoles = null;
            List<RolesPermsCopy.Item> AllPermisions = null;
            AllRoles = _roleManager.Roles.Where(f => f.AffectTenants == true).ToList();
            AllPermisions = new List<RolesPermsCopy.Item>();

            foreach (Role item in AllRoles)
            {
                var perms = await _roleManager.GetGrantedPermissionsAsync(item.Id);
                if (item.Id == role.Id)
                    AllPermisions.Add(new RolesPermsCopy.Item { RolId = item.Id, Perms = grantedPermissions.ToList() });
                else
                    AllPermisions.Add(new RolesPermsCopy.Item { RolId = item.Id, Perms = perms.ToList() });
            }

            await CurrentUnitOfWork.SaveChangesAsync();

            foreach (var tenant in _tenantManager.Tenants)
            {
                if (tenant.Id > 0)
                {
                    using (CurrentUnitOfWork.SetTenantId(tenant.Id))
                    {
                        //Recorriendo los roles
                        foreach (Role item in AllRoles)
                        {
                            var rol = _roleManager.Roles.FirstOrDefault(r => r.ParentRolId == item.Id && r.TenantId == tenant.Id);

                            if (rol == null)
                            {
                                rol = new Role()
                                {
                                    Name = item.Name,
                                    Description = item.Description,
                                    DisplayName = item.DisplayName,
                                    IsStatic = item.IsStatic,
                                    IsDefault = item.IsDefault,
                                    IsDeleted = item.IsDeleted,
                                    CreationTime = DateTime.Now,
                                    TenantId = tenant.Id,
                                    ParentRolId = item.Id
                                };
                                await _roleManager.CreateAsync(rol);
                            }
                        }

                        await CurrentUnitOfWork.SaveChangesAsync(); //To get static role ids

                        var rolesx = _roleManager.Roles.Where(r => r.TenantId == tenant.Id).ToList();

                        foreach (Role item in rolesx)
                        {
                            //if (item.Id > 1)
                            //    await _roleManager.ResetAllPermissionsAsync(item);
                            CurrentUnitOfWork.SaveChanges();
                            var RoleX = AllPermisions.FirstOrDefault(f => f.RolId == item.ParentRolId);
                            if (RoleX != null)
                            {
                                var perms = RoleX.Perms;
                                await _roleManager.SetGrantedPermissionsAsync(item, perms);
                            }
                        }

                        await CurrentUnitOfWork.SaveChangesAsync(); //To get admin user's id

                    }

                }

            }

            return MapToEntityDto(role);
        }

        public override async Task<RoleDto> Update(RoleDto input)
        {
            CheckUpdatePermission();

            var role = await _roleManager.GetRoleByIdAsync(input.Id);

            ObjectMapper.Map(input, role);

            CheckErrors(await _roleManager.UpdateAsync(role));

            var grantedPermissions = PermissionManager
                .GetAllPermissions()
                .Where(p => input.Permissions.Contains(p.Name))
                .ToList();

            await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);

            foreach (var tenant in _tenantManager.Tenants)
            {
                if (tenant.Id > 0)
                {
                    using (CurrentUnitOfWork.SetTenantId(tenant.Id))
                    {
                        var item1 = _roleManager.Roles.FirstOrDefault(r => r.TenantId == tenant.Id && r.ParentRolId == role.Id);
                        if (item1 != null)
                        {
                            await _roleManager.SetGrantedPermissionsAsync(item1, grantedPermissions);
                            await CurrentUnitOfWork.SaveChangesAsync(); //To get admin user's id
                        }
                    }
                }
            }

            return MapToEntityDto(role);
        }

        public override async Task Delete(EntityDto<int> input)
        {
            CheckDeletePermission();

            var role = await _roleManager.FindByIdAsync(input.Id);
            if (role.IsStatic)
            {
                throw new UserFriendlyException("CannotDeleteAStaticRole");
            }

            var users = await GetUsersInRoleAsync(role.Name);

            foreach (var user in users)
            {
                CheckErrors(await _userManager.RemoveFromRoleAsync(user, role.Name));
            }

            CheckErrors(await _roleManager.DeleteAsync(role));
        }

        [AbpAllowAnonymous]
        private Task<List<long>> GetUsersInRoleAsync(string roleName)
        {
            var users = (from user in _userRepository.GetAll()
                         join userRole in _userRoleRepository.GetAll() on user.Id equals userRole.UserId
                         join role in _roleRepository.GetAll() on userRole.RoleId equals role.Id
                         where role.Name == roleName
                         select user.Id).Distinct().ToList();

            return Task.FromResult(users);
        }

        [AbpAllowAnonymous]
        public Task<ListResultDto<PermissionDto>> GetAllPermissions()
        {
            var permissions = PermissionManager.GetAllPermissions();

            var permissions2 = _permisionService.GetAllPermissions();


            return Task.FromResult(new ListResultDto<PermissionDto>(
                ObjectMapper.Map<List<PermissionDto>>(permissions).OrderBy(x => x.Name).ToList()
            ));
        }
        

        

        protected override IQueryable<Role> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Permissions);
        }

        [AbpAllowAnonymous]
        protected override Task<Role> GetEntityByIdAsync(int id)
        {
            var role = Repository.GetAllIncluding(x => x.Permissions).FirstOrDefault(x => x.Id == id);
            if (role.Id == 1)
                role.AffectTenants = true;
            return Task.FromResult(role);
        }

        protected override IQueryable<Role> ApplySorting(IQueryable<Role> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.DisplayName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
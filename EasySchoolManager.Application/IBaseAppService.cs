﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager
{
    public interface IBaseAppService<TEntityDto, TPrimaryKey,in TRequestDto,in TCreateDto,in TUpdateDto> :
        IAsyncCrudAppService<TEntityDto, TPrimaryKey,TRequestDto,TCreateDto,TUpdateDto>
     where TEntityDto : IEntityDto<TPrimaryKey>
     where TUpdateDto : IEntityDto<TPrimaryKey>
     where TCreateDto : IEntityDto<TPrimaryKey>
    {
        Task<List<TEntityDto>> GetAllForCombo();
    }
}

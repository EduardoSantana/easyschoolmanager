//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.CommentTypes.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.CommentTypes 
{ 
    [AbpAuthorize(PermissionNames.Pages_CommentTypes)] 
    public class CommentTypeAppService : AsyncCrudAppService<Models.CommentTypes, CommentTypeDto, int, PagedResultRequestDto, CreateCommentTypeDto, UpdateCommentTypeDto>, ICommentTypeAppService 
    { 
        private readonly IRepository<Models.CommentTypes, int> _commentTypeRepository;
		


        public CommentTypeAppService( 
            IRepository<Models.CommentTypes, int> repository, 
            IRepository<Models.CommentTypes, int> commentTypeRepository 
            ) 
            : base(repository) 
        { 
            _commentTypeRepository = commentTypeRepository; 
			

			
        } 
        public override async Task<CommentTypeDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<CommentTypeDto> Create(CreateCommentTypeDto input) 
        { 
            CheckCreatePermission(); 
            var commentType = ObjectMapper.Map<Models.CommentTypes>(input); 
			try{
              await _commentTypeRepository.InsertAsync(commentType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(commentType); 
        } 
        public override async Task<CommentTypeDto> Update(UpdateCommentTypeDto input) 
        { 
            CheckUpdatePermission(); 
            var commentType = await _commentTypeRepository.GetAsync(input.Id);
            MapToEntity(input, commentType); 
		    try{
               await _commentTypeRepository.UpdateAsync(commentType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var commentType = await _commentTypeRepository.GetAsync(input.Id); 
               await _commentTypeRepository.DeleteAsync(commentType);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.CommentTypes MapToEntity(CreateCommentTypeDto createInput) 
        { 
            var commentType = ObjectMapper.Map<Models.CommentTypes>(createInput); 
            return commentType; 
        } 
        protected override void MapToEntity(UpdateCommentTypeDto input, Models.CommentTypes commentType) 
        { 
            ObjectMapper.Map(input, commentType); 
        } 
        protected override IQueryable<Models.CommentTypes> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.CommentTypes> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.CommentTypes> GetEntityByIdAsync(int id) 
        { 
            var commentType = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(commentType); 
        } 
        protected override IQueryable<Models.CommentTypes> ApplySorting(IQueryable<Models.CommentTypes> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<CommentTypeDto>> GetAllCommentTypes(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<CommentTypeDto> ouput = new PagedResultDto<CommentTypeDto>(); 
            IQueryable<Models.CommentTypes> query = query = from x in _commentTypeRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _commentTypeRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<CommentTypes.Dto.CommentTypeDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<CommentTypeDto>> GetAllCommentTypesForCombo()
        {
            var commentTypeList = await _commentTypeRepository.GetAllListAsync(x => x.IsActive == true);

            var commentType = ObjectMapper.Map<List<CommentTypeDto>>(commentTypeList.ToList());

            return commentType;
        }
		
    } 
} ;
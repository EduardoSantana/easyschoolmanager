//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.CommentTypes.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.CommentTypes
{
      public interface ICommentTypeAppService : IAsyncCrudAppService<CommentTypeDto, int, PagedResultRequestDto, CreateCommentTypeDto, UpdateCommentTypeDto>
      {
            Task<PagedResultDto<CommentTypeDto>> GetAllCommentTypes(GdPagedResultRequestDto input);
			Task<List<Dto.CommentTypeDto>> GetAllCommentTypesForCombo();
      }
}
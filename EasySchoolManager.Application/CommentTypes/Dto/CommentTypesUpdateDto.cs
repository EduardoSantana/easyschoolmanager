//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.CommentTypes.Dto 
{
        [AutoMap(typeof(Models.CommentTypes))] 
        public class UpdateCommentTypeDto : EntityDto<int> 
        {

              [StringLength(50)]  
              public string Name {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Evaluations.Dto 
{
        [AutoMap(typeof(Models.Evaluations))] 
        public class UpdateEvaluationDto : EntityDto<long> 
        {

              public int IndicatorId {get;set;} 

              public int? EvaluationLegendId {get;set;} 

              public int EnrollmentStudentId {get;set;} 

              public int EvaluationPeriodId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
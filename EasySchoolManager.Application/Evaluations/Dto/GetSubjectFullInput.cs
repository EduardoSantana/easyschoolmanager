﻿//Created from Templaste MG

using Abp.Application.Services.Dto;

namespace EasySchoolManager.Evaluations
{
    public class GetSubjectFullInput
    {
        public int SubjectId { get; set; }
        public int EnrollmentStudentId { get; set; }
    }
}
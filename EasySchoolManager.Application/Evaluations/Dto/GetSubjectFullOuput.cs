﻿//Created from Templaste MG

using System.Collections.Generic;
using EasySchoolManager.EvaluationLegends.Dto;
using EasySchoolManager.EvaluationPeriods.Dto;
using EasySchoolManager.Subjects.Dto;

namespace EasySchoolManager.Evaluations
{
    public class GetSubjectFullOuput
    {
        public SubjectDto Subjects { get; set; }
        public List<EvaluationPeriodDto> EvaluationPeriods { get; set; }
        public List<EvaluationLegendDto> EvaluationLegends { get; set; }
    }
}
//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.EvaluationPeriods.Dto;
using EasySchoolManager.EnrollmentStudents.Dto;
using EasySchoolManager.EvaluationLegends.Dto;
using EasySchoolManager.Indicators.Dto;

namespace EasySchoolManager.Evaluations.Dto
{
    [AutoMap(typeof(Models.Evaluations))]
    public class EvaluationDto : EntityDto<long>
    {
        public int IndicatorId { get; set; }
        public int? EvaluationLegendId { get; set; }
        public int EnrollmentStudentId { get; set; }
        public int EvaluationPeriodId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        public IndicatorDto2 Indicators { get; set; }

        public EvaluationLegendDto EvaluationLegends { get; set; }

        public EnrollmentStudentDto EnrollmentStudents { get; set; }

        public EvaluationPeriodDto EvaluationPerios { get; set; }

    }
}
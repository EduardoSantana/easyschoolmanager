//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Evaluations.Dto;
using System.Collections.Generic;
using EasySchoolManager.SessionStudentSelections;

namespace EasySchoolManager.Evaluations
{
    public interface IEvaluationAppService : IAsyncCrudAppService<EvaluationDto, long, PagedResultRequestDto, CreateEvaluationDto, UpdateEvaluationDto>
    {
        Task<PagedResultDto<EvaluationDto>> GetAllEvaluations(GdPagedResultRequestDto input);
        Task<List<Dto.EvaluationDto>> GetAllEvaluationsForCombo();

        Task<GetAllStudentByCourseOuput> GetAllStudentByCourse(GetAllStudentByCourseInput input);

        Task<GetInitialValuesOuput> GetInitialValues();

        Task<GetInitialValuesOuput> GetSessionsByCourse(GetSessionsByCourseInput input);

        Task<GetSubjectFullOuput> GetSubjectFull(GetSubjectFullInput input);
    }
}
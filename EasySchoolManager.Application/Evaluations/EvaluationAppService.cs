//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Evaluations.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.SessionStudentSelections;
using EasySchoolManager.Helpers;
using EasySchoolManager.Subjects;
using EasySchoolManager.Subjects.Dto;
using EasySchoolManager.EvaluationPeriods.Dto;
using EasySchoolManager.EvaluationLegends.Dto;

namespace EasySchoolManager.Evaluations
{
    [AbpAuthorize(PermissionNames.Pages_Evaluations)]
    public class EvaluationAppService : AsyncCrudAppService<Models.Evaluations, EvaluationDto, long, PagedResultRequestDto, CreateEvaluationDto, UpdateEvaluationDto>, IEvaluationAppService
    {
        private readonly IRepository<Models.Evaluations, long> _evaluationRepository;
        private readonly IRepository<Models.EvaluationPeriods, int> _evaluationPeriodsRepository;
        private readonly IRepository<Models.EvaluationLegends, int> _evaluationLegendsRepository;
        private readonly IRepository<Models.CourseSessions, int> _courseSessionRepository;
        private readonly IRepository<Models.EnrollmentStudents, int> _enrollmentStudentRepository;
        private readonly IRepository<Models.CourseEnrollmentStudents, int> _courseEnrollmentStudentsRepository;
        private readonly IRepository<Models.Courses, int> _courseRepository;
        private readonly IRepository<Models.Subjects, int> _subjectRepository;
        private readonly ISubjectAppService _subjectService;

        private readonly MultiTenancy.TenantManager _tenantManager;


        public EvaluationAppService(
            IRepository<Models.Evaluations, long> repository,
            IRepository<Models.EnrollmentStudents, int> enrollmentStudentRepository,
            IRepository<Models.CourseSessions, int> courseSessionRepository,
            IRepository<Models.CourseEnrollmentStudents, int> courseEnrollmentStudentsRepository,
            IRepository<Models.Courses, int> courseRepository,
            MultiTenancy.TenantManager tenantManager,
            ISubjectAppService subjectService,
            IRepository<Models.EvaluationPeriods, int> evaluationPeriodsRepository,
            IRepository<Models.EvaluationLegends, int> evaluationLegendsRepository,
            IRepository<Models.Subjects, int> subjectRepository
            )
            : base(repository)
        {
            _courseSessionRepository = courseSessionRepository;
            _enrollmentStudentRepository = enrollmentStudentRepository;
            _courseEnrollmentStudentsRepository = courseEnrollmentStudentsRepository;
            _courseRepository = courseRepository;
            _tenantManager = tenantManager;
            _subjectService = subjectService;
            _evaluationPeriodsRepository = evaluationPeriodsRepository;
            _evaluationLegendsRepository = evaluationLegendsRepository;
            _evaluationRepository = repository;
            _subjectRepository = subjectRepository;
            LocalizationSourceName = EasySchoolManagerConsts.LocalizationSourceName;
        }
        public override async Task<EvaluationDto> Get(EntityDto<long> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<EvaluationDto> Create(CreateEvaluationDto input)
        {
            CheckCreatePermission();
            Models.Evaluations evaluation = null;
            try
            {

                var evaluationLegends = _evaluationLegendsRepository.GetAllList(x => x.IsActive == true);
                var evaluationPeriods = _evaluationPeriodsRepository.GetAllList(x => x.IsActive == true);
                var allStudentEvaluation = _evaluationRepository.GetAllList(x => x.IsActive == true && x.EnrollmentStudentId == input.EnrollmentStudentId
                  && x.IndicatorId == input.IndicatorId);


                var newEvaluationLegend = evaluationLegends.Where(x => x.Id == input.EvaluationLegendId.Value).FirstOrDefault();
                var maxEvaluationLegend = evaluationLegends.OrderByDescending(x => x.Sequence).FirstOrDefault();


                //Buscamos el evaluation periodo que estamos intetando insertar
                var currentEvaluationPeriod =  evaluationPeriods.Where(x =>
                                        x.Id == input.EvaluationPeriodId && x.IsActive == true).FirstOrDefault();


                //Si el valor del evaluation legend es igual a -1 significa que fue quitado uno, si el valor quitado pertenece aun periodo
                //que tiene mas periodos posteriores configurados, se indica que la accion es invalida,, boche.
                if (input.EvaluationLegendId < 0)
                {
                    var evaluationPeriodSuperiors = evaluationPeriods.Where(x => x.Position > currentEvaluationPeriod.Position).OrderBy(x => x.Position);
                    if (evaluationPeriodSuperiors.Count() > 0)
                    {
                        var nextEvaluation = _evaluationRepository.GetAll().Where(x => x.EvaluationPeriodId == evaluationPeriodSuperiors.First().Id
                          && x.EnrollmentStudentId == input.EnrollmentStudentId);

                        if (nextEvaluation != null && 
                            allStudentEvaluation.OrderByDescending(x=> x.EvaluationPerios.Position).FirstOrDefault().EvaluationPerios != currentEvaluationPeriod)
                            throw new UserFriendlyException(L("YouCanNotEraseAnEvaluationLegendsThatIsNotTheLast"));

                      // p  xxxx   Me quede aqui buscando la parte de asignar el maximo periodo con evaluacion asignada//
                    }

                    var deleteThisEvaluation = _evaluationRepository.GetAll().Where(x => x.EvaluationPeriodId == input.EvaluationPeriodId
                    && x.EnrollmentStudentId == input.EnrollmentStudentId
                    && x.IndicatorId == input.IndicatorId).FirstOrDefault();
                    if (deleteThisEvaluation != null)
                    {
                        deleteThisEvaluation.ComplementForDeletion = deleteThisEvaluation.Id;
                        CurrentUnitOfWork.SaveChanges();
                        _evaluationRepository.Delete(deleteThisEvaluation);
                        CurrentUnitOfWork.SaveChanges();
                    }

                    return null;
                }






                if (currentEvaluationPeriod != null)
                {
                    if (currentEvaluationPeriod.Position > 1)
                    {
                        //Buscamos el evaluation periodo anterior
                        var priorEvaluationPeriod = _evaluationPeriodsRepository.GetAll().Where(x =>
                                                x.Position < currentEvaluationPeriod.Position && x.IsActive == true).OrderByDescending(x => x.Position).FirstOrDefault();

                        ///Si la evaluacion es de la segunda en adelante buscamos que el anterior este registrado. si no esta registrado devolvemos un boche.
                        var priorEvaluation = _evaluationRepository.GetAll().Where(x => x.EvaluationPerios.Position == priorEvaluationPeriod.Position
                        && x.EnrollmentStudentId == input.EnrollmentStudentId
                        && x.IndicatorId == input.IndicatorId).FirstOrDefault();
                        if (priorEvaluation == null)
                            throw new UserFriendlyException(L("YouCanNotSaveAnEvaluationBeforeSaveThePriorEvaluations"));
                        else
                        {
                            //Si en el periodo anterior fue puesta la legenda de evaluacion maxima se tira un error para que no se pongan mas
                            if (priorEvaluation.EvaluationLegends.Sequence == maxEvaluationLegend.Sequence)
                            {
                                throw new UserFriendlyException(L("TheMaxValueForEvaluationLegendWasSettedInThePriorEvaluationPeriodYouCanNotSetMoreLegends"));
                            }

                        }


                    }

                }
                else
                    throw new Exception(L("ThisEvaluationPeriodDoesNotExists"));


                evaluation = await _evaluationRepository.FirstOrDefaultAsync(x => x.EvaluationPeriodId == input.EvaluationPeriodId &&
                         x.EnrollmentStudentId == input.EnrollmentStudentId &&
                         x.IndicatorId == input.IndicatorId &&
                         x.IsActive == true);

                if (evaluation == null)
                {


                    //Si el evalualtion period tiene una posicion > 1, cuando el evaluation es null quiere decir que se intento modificar un evaluation period mayor al primero
                    //tenemos que ver si el anterior ha sido llenado
                    if (currentEvaluationPeriod.Position > 1)
                    {
                        //Buscamos el evaluation periodo que estamos intetando insertar
                        var priorEvaluationPeriods = await _evaluationPeriodsRepository.GetAllListAsync(x =>
                                            x.Position < currentEvaluationPeriod.Position && x.IsActive == true);
                        if (priorEvaluationPeriods.Count > 0)
                        {
                            var lastEvaluationPeriod = priorEvaluationPeriods.OrderByDescending(x => x.Position).FirstOrDefault();

                            //Si la diferencia entre el perido actual y el ultimo peridodo modificado es mayor a 1, quiere decir
                            //que hubieron saltos, entonces se debe enviar un boche e impedir la acci�n.
                            if ((currentEvaluationPeriod.Position - lastEvaluationPeriod.Position) > 1)
                                throw new UserFriendlyException(L("YouCanNotSaveAnEvaluationBeforeSaveThePriorEvaluations"));

                            ///Si la evaluacion es de la segunda en adelante buscamos que el anterior este registrado. si no esta registrado devolvemos un boche.
                            var priorEvaluation = _evaluationRepository.GetAll().Where(x => x.EvaluationPerios.Position == lastEvaluationPeriod.Position
                            && x.EnrollmentStudentId == input.EnrollmentStudentId
                            && x.IndicatorId == input.IndicatorId).FirstOrDefault();
                            if (priorEvaluation == null)
                                throw new UserFriendlyException(L("YouCanNotSaveAnEvaluationBeforeSaveThePriorEvaluations"));
                            else
                            {
                                //Si en el periodo anterior fue puesta la legenda de evaluacion maxima se tira un error para que no se pongan mas
                                if (priorEvaluation.EvaluationLegends.Sequence == maxEvaluationLegend.Sequence)
                                {
                                    throw new UserFriendlyException(L("TheMaxValueForEvaluationLegendWasSettedInThePriorEvaluationPeriodYouCanNotSetMoreLegends"));
                                }

                                if (priorEvaluation.EvaluationLegends.Sequence > newEvaluationLegend.Sequence)
                                {
                                    throw new UserFriendlyException(L("YouSelTheLegendXXXIntheLasPerAndSelectTheLegXXXInTheCurrenPeriod_IsNotAllowed",
                                        priorEvaluation.EvaluationLegends.Name, newEvaluationLegend.Name));

                                }
                            }

                        }
                    }
                    evaluation = ObjectMapper.Map<Models.Evaluations>(input);
                    evaluation.IsActive = true;
                    await _evaluationRepository.InsertAndGetIdAsync(evaluation);
                }
                else
                {

                    //Si el evaluation period es mayor al periodo 1, quiere decir que tiene uno anterior, por tanto buscamos
                    //el anterior, para ver si el evaluation legen indicado puede ser marcado, debido a la secuencia de 
                    //ejecucion de las leyendas de calificaciones.
                    if (evaluation.EvaluationPerios.Position > 1)
                    {
                        var priorEvaluations = await _evaluationRepository.GetAllListAsync(x => x.EvaluationPeriodId < evaluation.EvaluationPeriodId
                         && x.EnrollmentStudentId == input.EnrollmentStudentId &&
                         x.IndicatorId == input.IndicatorId &&
                         x.IsActive == true);

                        var priorEvaluation = priorEvaluations.OrderByDescending(x => x.EvaluationPerios.Position).FirstOrDefault();

                        //Si el evalualtion period es null quiere decir que se intento modificar un evaluation period mayor al primero
                        //aunque el primero aun no ha sido llenado, y estos registros deben ser llenados en secuencia, por indicador.
                        if (priorEvaluation == null)
                            throw new UserFriendlyException(L("YouCanNotSaveAnEvaluationBeforeSaveThePriorEvaluations"));

                        //Si la diferencia entre el perido actual y el ultimo peridodo modificado es mayor a 1, quiere decir
                        //que hubieron saltos, entonces se debe enviar un boche e impedir la acci�n.
                        if ((evaluation.EvaluationPerios.Position - priorEvaluation.EvaluationPerios.Position) > 1)
                            throw new UserFriendlyException(L("YouCanNotSaveAnEvaluationBeforeSaveThePriorEvaluations"));


                        if (newEvaluationLegend.Sequence < priorEvaluation.EvaluationLegends.Sequence)
                            throw new UserFriendlyException(L("YouSelTheLegendXXXIntheLasPerAndSelectTheLegXXXInTheCurrenPeriod_IsNotAllowed",
                                priorEvaluation.EvaluationLegends.Description, evaluation.EvaluationLegends.Description));


                    }


                    evaluation.EvaluationLegendId = input.EvaluationLegendId;
                    await _evaluationRepository.UpdateAsync(evaluation);
                    CurrentUnitOfWork.SaveChanges();
                }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(evaluation);
        }
        public override async Task<EvaluationDto> Update(UpdateEvaluationDto input)
        {
            CheckUpdatePermission();
            var evaluation = await _evaluationRepository.GetAsync(input.Id);
            MapToEntity(input, evaluation);
            try
            {
                await _evaluationRepository.UpdateAsync(evaluation);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<long> input)
        {
            try
            {
                var evaluation = await _evaluationRepository.GetAsync(input.Id);
                await _evaluationRepository.DeleteAsync(evaluation);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.Evaluations MapToEntity(CreateEvaluationDto createInput)
        {
            var evaluation = ObjectMapper.Map<Models.Evaluations>(createInput);
            return evaluation;
        }
        protected override void MapToEntity(UpdateEvaluationDto input, Models.Evaluations evaluation)
        {
            ObjectMapper.Map(input, evaluation);
        }
        protected override IQueryable<Models.Evaluations> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.Evaluations> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.IndicatorId.ToString().Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.Evaluations> GetEntityByIdAsync(long id)
        {
            var evaluation = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(evaluation);
        }
        protected override IQueryable<Models.Evaluations> ApplySorting(IQueryable<Models.Evaluations> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.IndicatorId.ToString());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<EvaluationDto>> GetAllEvaluations(GdPagedResultRequestDto input)
        {
            PagedResultDto<EvaluationDto> ouput = new PagedResultDto<EvaluationDto>();
            IQueryable<Models.Evaluations> query = query = from x in _evaluationRepository.GetAll()
                                                           select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _evaluationRepository.GetAll()
                        where x.IndicatorId.ToString().Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Evaluations.Dto.EvaluationDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<EvaluationDto>> GetAllEvaluationsForCombo()
        {
            var evaluationList = await _evaluationRepository.GetAllListAsync(x => x.IsActive == true);

            var evaluation = ObjectMapper.Map<List<EvaluationDto>>(evaluationList.ToList());

            return evaluation;
        }

        public async Task<GetAllStudentByCourseOuput> GetAllStudentByCourse(GetAllStudentByCourseInput input)
        {
            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();
            List<StudentSessionDto> studentListOrdened = new List<StudentSessionDto>();

            GeneralHelper.ValidateUserBellongToASchool(AbpSession);

            var tenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
            if (tenant == null)
            {
                Logger.Error(L("TheUserMustBellongToATenantToUseThisOption"));
                return null;
            }

            if (!tenant.PeriodId.HasValue)
            {
                throw new UserFriendlyException(L("ToUseThisOptionTheSchoolMustHasSelectedTheCurrentPeriod"));
            }

            try
            {

                if (input.SessionId.HasValue)
                {
                    var query2 = await _courseEnrollmentStudentsRepository.GetAllListAsync(x => x.CourseId == input.CourseId
                                && x.SessionId == input.SessionId
                                && x.EnrollmentStudents.Enrollments.EnrollmentSequences.FirstOrDefault().IsDeleted == false
                                && x.EnrollmentStudents.PeriodId == tenant.PeriodId);

                    var studentOfSession = query2
                            .OrderBy(x => x?.Sequence)
                            .ThenBy(x => x.EnrollmentStudents?.Students?.LastName)
                            .ToList();

                    foreach (var item in studentOfSession)
                    {
                        StudentSessionDto st = new StudentSessionDto();

                        st.Students = new Students.Dto.StudentDto();
                        st.Students.FirstName = item.EnrollmentStudents.Students.FirstName;
                        st.Students.LastName = item.EnrollmentStudents.Students.LastName;
                        st.Students.Id = item.EnrollmentStudents.Students.Id;
                        if (item.Sequence.HasValue)
                            st.Sequence = item.Sequence.Value;
                        st.Id = item.EnrollmentStudents.Id;
                        studentListOrdened.Add(st);
                    }
                }

                ouput.studentCount = studentListOrdened.Count;
                ouput.StudentSession = studentListOrdened.OrderBy(x => x.LastName).ToList();
            }
            catch (Exception err)
            {
                Logger.Error(err.Message);
                throw new UserFriendlyException(err.Message);
            }

            return ouput;
        }

        public async Task<GetInitialValuesOuput> GetInitialValues()
        {
            GetInitialValuesOuput output = new GetInitialValuesOuput();

            var subjectData = await _subjectService.GetAllSubjectsForCombo();

            var coursesQuery = await _courseRepository.GetAllListAsync(x => x.IsActive == true);
            var coursesData = coursesQuery.ToList().Select(x => new ComboBoxObject(x.Id, x.Name)).ToList();

            output.Courses = coursesData;


            var sessionQuery = await _courseSessionRepository.GetAllListAsync(x => x.IsActive == true);
            var sessionData = sessionQuery.ToList().Select(x =>
                new ComboBoxObject(x.Id, x.Name)).ToList();

            output.Sessions = sessionData;

            output.Subjects = subjectData.Select(x =>
                    new ComboBoxObject(x.Id, x.Name + " (" + x.LongName + ")")).ToList();

            return output;
        }

        public async Task<GetInitialValuesOuput> GetSessionsByCourse(GetSessionsByCourseInput input)
        {
            GetInitialValuesOuput output = new GetInitialValuesOuput();

            var sessionQuery = await _courseSessionRepository.GetAllListAsync(x => x.IsActive == true && x.CourseId == input.CourseId);
            var sessionData = sessionQuery.ToList().Select(x => new ComboBoxObject(x.Id, x.Name)).ToList();
            output.Sessions = sessionData;

            return output;
        }


        public async Task<GetSubjectFullOuput> GetSubjectFull(GetSubjectFullInput input)
        {
            GetSubjectFullOuput output = new GetSubjectFullOuput();


            var data = _subjectService.Get(new EntityDto<int>(input.SubjectId));

            output.Subjects = await Task.Run(() => { return data; });

            var EvaluationPeriodQuery = _evaluationPeriodsRepository.GetAllList(x => x.IsActive == true);

            var EvaluationLegendQuery = _evaluationLegendsRepository.GetAllList(x => x.IsActive == true);

            output.EvaluationPeriods = AutoMapper.Mapper.Map<List<EvaluationPeriodDto>>(EvaluationPeriodQuery.ToList());
            output.EvaluationLegends = AutoMapper.Mapper.Map<List<EvaluationLegendDto>>(EvaluationLegendQuery.ToList());



            //Recorremos todos los indicator groups
            foreach (var indG in output.Subjects.IndicatorGroups)
            {
                //Recorremos todos los indicadores para asignarle los evaluations
                foreach (var Ind in indG.Indicators)
                {
                    var dataEvaluations = await _evaluationRepository.GetAllListAsync(x => x.EnrollmentStudentId == input.EnrollmentStudentId
                                && x.IndicatorId == Ind.Id);
                    Ind.Evaluations = AutoMapper.Mapper.Map<List<EvaluationDto>>(dataEvaluations);

                    //Recorremos los evaluation periods, para con cada uno crear un evaluation y asignarlo al indicator
                    foreach (var evp in output.EvaluationPeriods)
                    {
                        //Si un evaluation period, ya existe dentro de los evaluation, entonces no lo registre.
                        if (Ind.Evaluations.Any(x => x.EvaluationPeriodId == evp.Id))
                            continue;

                        EvaluationDto eva = new EvaluationDto();
                        eva.EvaluationPerios = evp;
                        eva.EvaluationPeriodId = evp.Id;
                        eva.IndicatorId = Ind.Id;
                        eva.EnrollmentStudentId = input.EnrollmentStudentId;
                        eva.Indicators = new Indicators.Dto.IndicatorDto2()
                        {
                            Id = Ind.Id,
                            Name = Ind.Name,
                            IsActive = Ind.IsActive,
                            Sequence = Ind.Sequence,
                            IndicatorGroupId = Ind.IndicatorGroupId
                        };
                        Ind.Evaluations.Add(eva);
                    }
                    Ind.Evaluations = Ind.Evaluations.OrderBy(x => x.EvaluationPerios.Position).ToList();

                }

            }


            return output;

        }



    }
};
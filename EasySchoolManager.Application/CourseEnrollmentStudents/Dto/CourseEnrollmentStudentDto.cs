﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.CourseEnrollmentStudents.Dto
{
    [AutoMap(typeof(Models.CourseEnrollmentStudents))]
    public class CourseEnrollmentStudentDto
    {
        public int CourseId { get; set; }

        public int EnrollmentStudentId { get; set; }

        public int? SessionId { get; set; }

        public int? Sequence { get; set; }
        
        public virtual Courses.Dto.CourseDto Courses { get; set; }

        public virtual CourseSessions.Dto.CourseSessionDto Sessions { get; set; }
    }
}

//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Providers.Dto 
{
        [AutoMap(typeof(Models.Providers))] 
        public class ProviderDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public string Name {get;set;} 
              public string Reference {get;set;} 
              public string Rnc {get;set;} 
              public string Phone1 {get;set;} 
              public string Phone2 {get;set;} 
              public string Address {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Providers.Dto 
{
        [AutoMap(typeof(Models.Providers))] 
        public class CreateProviderDto : EntityDto<int> 
        {

              [StringLength(200)] 
              public string Name {get;set;} 

              [StringLength(20)] 
              public string Reference {get;set;} 

              [StringLength(15)] 
              public string Rnc {get;set;} 

              [StringLength(15)] 
              public string Phone1 {get;set;} 

              [StringLength(15)] 
              public string Phone2 {get;set;} 

              [StringLength(100)] 
              public string Address {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
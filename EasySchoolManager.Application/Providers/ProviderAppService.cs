//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Providers.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Providers 
{ 
    [AbpAuthorize(PermissionNames.Pages_Providers)] 
    public class ProviderAppService : AsyncCrudAppService<Models.Providers, ProviderDto, int, PagedResultRequestDto, CreateProviderDto, UpdateProviderDto>, IProviderAppService 
    { 
        private readonly IRepository<Models.Providers, int> _providerRepository;
		


        public ProviderAppService( 
            IRepository<Models.Providers, int> repository, 
            IRepository<Models.Providers, int> providerRepository 
            ) 
            : base(repository) 
        { 
            _providerRepository = providerRepository; 
			

			
        } 
        public override async Task<ProviderDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ProviderDto> Create(CreateProviderDto input) 
        { 
            CheckCreatePermission(); 
            var provider = ObjectMapper.Map<Models.Providers>(input); 
			try{
              await _providerRepository.InsertAsync(provider); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(provider); 
        } 
        public override async Task<ProviderDto> Update(UpdateProviderDto input) 
        { 
            CheckUpdatePermission(); 
            var provider = await _providerRepository.GetAsync(input.Id);
            MapToEntity(input, provider); 
		    try{
               await _providerRepository.UpdateAsync(provider); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var provider = await _providerRepository.GetAsync(input.Id); 
               await _providerRepository.DeleteAsync(provider);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Providers MapToEntity(CreateProviderDto createInput) 
        { 
            var provider = ObjectMapper.Map<Models.Providers>(createInput); 
            return provider; 
        } 
        protected override void MapToEntity(UpdateProviderDto input, Models.Providers provider) 
        { 
            ObjectMapper.Map(input, provider); 
        } 
        protected override IQueryable<Models.Providers> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Providers> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Providers> GetEntityByIdAsync(int id) 
        { 
            var provider = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(provider); 
        } 
        protected override IQueryable<Models.Providers> ApplySorting(IQueryable<Models.Providers> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ProviderDto>> GetAllProviders(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ProviderDto> ouput = new PagedResultDto<ProviderDto>(); 
            IQueryable<Models.Providers> query = query = from x in _providerRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _providerRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Providers.Dto.ProviderDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<ProviderDto>> GetAllProvidersForCombo()
        {
            var providerList = await _providerRepository.GetAllListAsync(x => x.IsActive == true);

            var provider = ObjectMapper.Map<List<ProviderDto>>(providerList.ToList());

            return provider;
        }
		
    } 
} ;
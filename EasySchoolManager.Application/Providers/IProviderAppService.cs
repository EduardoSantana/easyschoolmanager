//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Providers.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Providers
{
      public interface IProviderAppService : IAsyncCrudAppService<ProviderDto, int, PagedResultRequestDto, CreateProviderDto, UpdateProviderDto>
      {
            Task<PagedResultDto<ProviderDto>> GetAllProviders(GdPagedResultRequestDto input);
			Task<List<Dto.ProviderDto>> GetAllProvidersForCombo();
      }
}
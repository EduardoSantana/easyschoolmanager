//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Messages.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Messages
{
    public interface IMessageAppService : IAsyncCrudAppService<MessageDto, long, PagedResultRequestDto, CreateMessageDto, UpdateMessageDto>
    {
        Task<PagedResultDto<MessageDto>> GetAllMessages(GdPagedResultRequestDto input);
        Task<List<Dto.MessageDto>> GetAllMessagesForCombo();
        Task<PagedResultDto<MessageSubjectDto>> GetAllMessagesSubject(ResultRequestDto input);
    }
}
//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Messages.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.Authorization.Users;
using Abp.Domain.Uow;

namespace EasySchoolManager.Messages
{

    public class MessageAppService : AsyncCrudAppService<Models.Messages, MessageDto, long, PagedResultRequestDto, CreateMessageDto, UpdateMessageDto>, IMessageAppService
    {
        private readonly IRepository<Models.Messages, long> _messageRepository;
        private readonly IRepository<Models.MessageRead, long> _messageReadRepository;
        private readonly IRepository<User, long> _userRepository;



        public MessageAppService(
            IRepository<Models.Messages, long> messageRepository,
            IRepository<Models.MessageRead, long> messageReadRepository,
            IRepository<User, long> userRepository
            )
            : base(messageRepository)
        {
            _messageRepository = messageRepository;
            _messageReadRepository = messageReadRepository;
            _userRepository = userRepository;
            LocalizationSourceName = EasySchoolManager.EasySchoolManagerConsts.LocalizationSourceName;

        }
        public override async Task<MessageDto> Get(EntityDto<long> input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                long userId = AbpSession.UserId.Value;
                var messageRead = _messageReadRepository.GetAll().FirstOrDefault(t => t.MessageId == input.Id && t.UserId == userId);
                if (messageRead == null)
                {
                    messageRead = new Models.MessageRead
                    {
                        MessageId = input.Id,
                        UserId = userId,
                        IsActive = true
                    };

                    _messageReadRepository.Insert(messageRead);
                }
                var message = await base.Get(input);
                var user = _userRepository.Load(message.CreatorUserId.Value);
                message.FromUser_FullName = user.FullName;
                return message;
            }
        }

        [AbpAuthorize(PermissionNames.Pages_Messages_Create)]
        public override async Task<MessageDto> Create(CreateMessageDto input)
        {
            CheckCreatePermission();
            var message = ObjectMapper.Map<Models.Messages>(input);
            try
            {
                await _messageRepository.InsertAsync(message);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(message);
        }

        [AbpAuthorize(PermissionNames.Pages_Messages_Update)]
        public override async Task<MessageDto> Update(UpdateMessageDto input)
        {
            CheckUpdatePermission();
            var message = await _messageRepository.GetAsync(input.Id);
            MapToEntity(input, message);
            try
            {
                await _messageRepository.UpdateAsync(message);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }

        [AbpAuthorize(PermissionNames.Pages_Messages_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            try
            {
                var message = await _messageRepository.GetAsync(input.Id);
                await _messageRepository.DeleteAsync(message);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }

        protected override Models.Messages MapToEntity(CreateMessageDto createInput)
        {
            var message = ObjectMapper.Map<Models.Messages>(createInput);
            return message;
        }
        protected override void MapToEntity(UpdateMessageDto input, Models.Messages message)
        {
            ObjectMapper.Map(input, message);
        }
        protected override IQueryable<Models.Messages> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.Messages> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Subject.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.Messages> GetEntityByIdAsync(long id)
        {
            var message = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(message);
        }
        protected override IQueryable<Models.Messages> ApplySorting(IQueryable<Models.Messages> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Subject);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        [AbpAllowAnonymous]
        public async Task<PagedResultDto<MessageDto>> GetAllMessages(GdPagedResultRequestDto input)
        {

            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var currentUser = _userRepository.Load(AbpSession.UserId.Value);
                PagedResultDto<MessageDto> ouput = new PagedResultDto<MessageDto>();
                var query = from t in _messageRepository.GetAll()
                            join fromUser in _userRepository.GetAll() on t.CreatorUserId equals fromUser.Id
                            where t.AllUser == true || t.DestinationUserId == currentUser.Id || t.CreatorUserId == currentUser.Id
                            where t.CreationTime >= currentUser.CreationTime
                            select new { message = t, fromUser };

                if (AbpSession.TenantId.HasValue)
                    query = query.Where(t => t.message.AllTenant == true || t.message.TenantId == AbpSession.TenantId.Value);

                if (!string.IsNullOrEmpty(input.TextFilter))
                {
                    query = query.Where(t => t.message.Subject.Contains(input.TextFilter));
                }
                ouput.TotalCount = await Task.Run(() => { return query.Count(); });
                if (input.SkipCount > 0)
                {
                    query = query.Skip(input.SkipCount);
                }
                if (input.MaxResultCount > 0)
                {
                    query = query.Take(input.MaxResultCount);
                }

                query = query.OrderByDescending(x => x.message.Id);

                var _query = (from q in query
                              join _toUser in _userRepository.GetAll() on q.message.DestinationUserId equals _toUser.Id into lf
                              from toUser in lf.DefaultIfEmpty()
                              select new { message = q.message, q.fromUser, toUser });

                ouput.Items = _query.Select(t => new MessageDto()
                {
                    Id = t.message.Id,
                    AllTenant = t.message.AllTenant,
                    IsActive = t.message.IsActive,
                    AllUser = t.message.AllUser,
                    Body = t.message.Body,
                    CreationTime = t.message.CreationTime,
                    CreatorUserId = t.message.CreatorUserId,
                    DestinationUserId = t.message.DestinationUserId,
                    DestinationUser_FullName = t.toUser.Name + " " + t.toUser.Surname,
                    FromUser_FullName = t.fromUser.Name + " " + t.fromUser.Surname,
                    Subject = t.message.Subject,
                    Tenant_Name = t.message.Tenant.Name

                }).ToList();
                return ouput;
            }
        }

        [AbpAllowAnonymous]
        public async Task<PagedResultDto<MessageSubjectDto>> GetAllMessagesSubject(ResultRequestDto input)
        {

            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var currentUser = _userRepository.Load(AbpSession.UserId.Value);
                PagedResultDto<MessageSubjectDto> ouput = new PagedResultDto<MessageSubjectDto>();
                IQueryable<Models.Messages> query = _messageRepository.GetAll()
                    .Where(t => t.CreatorUserId != currentUser.Id)
                    .Where(t => t.AllUser == true || t.DestinationUserId == currentUser.Id)
                    .Where(t => t.CreationTime >= currentUser.CreationTime);

                if (AbpSession.TenantId.HasValue)
                    query = query.Where(t => t.AllTenant == true || t.TenantId == AbpSession.TenantId.Value);

                if (!string.IsNullOrEmpty(input.TextFilter))
                {
                    query = from x in _messageRepository.GetAll()
                            where x.Subject.Contains(input.TextFilter)
                            select x;
                }

                if (input.ReadedMessages.HasValue)
                {
                    if (input.ReadedMessages.Value)
                        query = query.Where(t => t.MessageRead.FirstOrDefault() != null);
                    else
                        query = query.Where(t => t.MessageRead.FirstOrDefault() == null);
                }

                ouput.TotalCount = await Task.Run(() => { return query.Count(); });

                if (input.SkipCount > 0)
                {
                    query = query.Skip(input.SkipCount);
                }
                if (input.MaxResultCount > 0)
                {
                    query = query.Take(input.MaxResultCount);
                }

                query = query.OrderByDescending(x => x.Id);

                ouput.Items = query.ToList().Select(t => new MessageSubjectDto
                {
                    Id = t.Id,
                    Subject = t.Subject,
                    CreationTime = t.CreationTime,
                    CreatorUserId = t.CreatorUserId,
                    DestinationUserId = t.DestinationUserId,
                    L = L
                }).ToList();
                return ouput;
            }
        }

        [AbpAllowAnonymous]
        public async Task<List<MessageDto>> GetAllMessagesForCombo()
        {
            var messageList = await _messageRepository.GetAllListAsync(x => x.IsActive == true);

            var message = ObjectMapper.Map<List<MessageDto>>(messageList.ToList());

            return message;
        }

    }
};
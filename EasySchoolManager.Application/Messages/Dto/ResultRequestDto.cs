﻿using EasySchoolManager.GD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Messages.Dto
{
    public class ResultRequestDto : GdPagedResultRequestDto
    {
        public bool? ReadedMessages { get; set; }
    }
}

//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Messages.Dto 
{
        [AutoMap(typeof(Models.Messages))] 
        public class UpdateMessageDto : EntityDto<long> 
        {

              public long? DestinationUserId {get;set;} 

              [StringLength(200)]  
              public string Subject {get;set;} 

              public string Body {get;set;} 

              public bool AllTenant {get;set;} 

              public bool AllUser {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
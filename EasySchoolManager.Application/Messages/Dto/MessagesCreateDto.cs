//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.Messages.Dto
{
    [AutoMap(typeof(Models.Messages))]
    public class CreateMessageDto : EntityDto<long>
    {

        public long? DestinationUserId { get; set; }
        public int? TenantId { get; set; }

        [StringLength(200)]
        public string Subject { get; set; }

        public string Body { get; set; }

        public bool AllTenant { get; set; }

        public bool AllUser { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        public string DestinationUser_FullName { get; set; }
        public string Tenant_FullName { get; set; }

    }
}
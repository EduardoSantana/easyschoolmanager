//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Localization;
using AutoMapper;

namespace EasySchoolManager.Messages.Dto
{
    [AutoMap(typeof(Models.Messages))]
    public class MessageSubjectDto : EntityDto<long>
    {

        public long? DestinationUserId { get; set; }
        public string Subject { get; set; }
        public bool AllTenant { get; set; }
        public bool AllUser { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        public string Tenant_Name { get; set; }
        public string DestinationUser_FullName { get; set; }
        public string FromUser_FullName { get; set; }

        public Func<string, string> L { get; set; }

        [IgnoreMap]
        public string Time
        {
            get
            {

                string _time = "";
                var timeDue =(DateTime.Now - CreationTime);
                var _timeDue = new {
                    Year = Convert.ToInt32(timeDue.TotalDays / 365),
                    Month = Convert.ToInt32(timeDue.TotalDays / 30),
                    Day = Convert.ToInt32(timeDue.TotalDays),
                    Hour = Convert.ToUInt32(timeDue.TotalHours) ,
                    Minute = Convert.ToUInt32(timeDue.TotalMinutes)
                };

                if (_timeDue.Year > 1)
                    _time = $"{_timeDue.Year} {L("Years")}";
                else if (_timeDue.Year == 1)
                    _time = $"{_timeDue.Year} {L("Year")}";

                else if (_timeDue.Month > 1)
                    _time = $"{_timeDue.Month} {L("Months")}";
                else if (_timeDue.Month == 1)
                    _time = $"{_timeDue.Month} {L("Month")}";

                else if (_timeDue.Day > 1)
                    _time = $"{_timeDue.Day} {L("Days")}";
                else if (_timeDue.Month == 1)
                    _time = $"{_timeDue.Day} {L("Day")}";

                else if (_timeDue.Hour > 1)
                    _time = $"{_timeDue.Hour} {L("Hours")}";
                else if (_timeDue.Hour == 1)
                    _time = $"{_timeDue.Hour} {L("Hour")}";

                else if (_timeDue.Minute > 1)
                    _time = $"{_timeDue.Minute} {L("Minutes")}";
                else if (_timeDue.Minute == 1)
                    _time = $"{_timeDue.Minute} {L("Minute")}";


                else 
                    _time = L("JustNow");
                return _time;
            }
        }

    }
}
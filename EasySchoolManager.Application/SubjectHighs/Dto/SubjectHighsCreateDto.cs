//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.SubjectHighs.Dto 
{
        [AutoMap(typeof(Models.SubjectHighs))] 
        public class CreateSubjectHighDto : EntityDto<int> 
        {

              [StringLength(80)] 
              public string Name {get;set;} 

              [StringLength(350)] 
              public string LongName {get;set;} 

              [StringLength(20)] 
              public string Code {get;set;} 

              public int CourseId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
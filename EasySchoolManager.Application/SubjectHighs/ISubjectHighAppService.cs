//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.SubjectHighs.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.SubjectHighs
{
      public interface ISubjectHighAppService : IAsyncCrudAppService<SubjectHighDto, int, PagedResultRequestDto, CreateSubjectHighDto, UpdateSubjectHighDto>
      {
            Task<PagedResultDto<SubjectHighDto>> GetAllSubjectHighs(GdPagedResultRequestDto input);
			Task<List<Dto.SubjectHighDto>> GetAllSubjectHighsForCombo();
            Task<List<Dto.SubjectHighDto>> GetAllSubjectHighsByCourseForCombo(int courseId);
    }
}
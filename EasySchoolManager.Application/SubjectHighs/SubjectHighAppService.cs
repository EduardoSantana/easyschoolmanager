//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.SubjectHighs.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.SubjectHighs 
{ 
	[AbpAuthorize(PermissionNames.Pages_SubjectHighs)] 
	public class SubjectHighAppService : AsyncCrudAppService<Models.SubjectHighs, SubjectHighDto, int, PagedResultRequestDto, CreateSubjectHighDto, UpdateSubjectHighDto>, ISubjectHighAppService 
	{ 
		private readonly IRepository<Models.SubjectHighs, int> _subjectHighRepository;
		
			private readonly IRepository<Models.Courses, int> _courseRepository;

		   
		public SubjectHighAppService( 
			IRepository<Models.SubjectHighs, int> repository, 
			IRepository<Models.SubjectHighs, int> subjectHighRepository ,
			IRepository<Models.Courses, int> courseRepository
			
			) 
			: base(repository) 
		{ 
			_subjectHighRepository = subjectHighRepository; 
			
			_courseRepository = courseRepository;


		} 
		public override async Task<SubjectHighDto> Get(EntityDto<int> input) 
		{ 
			var user = await base.Get(input); 
			return user; 
		} 
		public override async Task<SubjectHighDto> Create(CreateSubjectHighDto input) 
		{ 
			CheckCreatePermission(); 
			var subjectHigh = ObjectMapper.Map<Models.SubjectHighs>(input); 
			try{
			  await _subjectHighRepository.InsertAsync(subjectHigh); 
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}
			return MapToEntityDto(subjectHigh); 
		} 
		public override async Task<SubjectHighDto> Update(UpdateSubjectHighDto input) 
		{ 
			CheckUpdatePermission(); 
			var subjectHigh = await _subjectHighRepository.GetAsync(input.Id);
			MapToEntity(input, subjectHigh); 
			try{
			   await _subjectHighRepository.UpdateAsync(subjectHigh); 
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}
			return await Get(input); 
		} 
		public override async Task Delete(EntityDto<int> input) 
		{
			try{
			   var subjectHigh = await _subjectHighRepository.GetAsync(input.Id); 
			   await _subjectHighRepository.DeleteAsync(subjectHigh);
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}			
		} 
		protected override Models.SubjectHighs MapToEntity(CreateSubjectHighDto createInput) 
		{ 
			var subjectHigh = ObjectMapper.Map<Models.SubjectHighs>(createInput); 
			return subjectHigh; 
		} 
		protected override void MapToEntity(UpdateSubjectHighDto input, Models.SubjectHighs subjectHigh) 
		{ 
			ObjectMapper.Map(input, subjectHigh); 
		} 
		protected override IQueryable<Models.SubjectHighs> CreateFilteredQuery(PagedResultRequestDto input) 
		{ 
			return Repository.GetAll(); 
		} 
		protected  IQueryable<Models.SubjectHighs> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
		{ 
			var resultado =  Repository.GetAll(); 
			if (!string.IsNullOrEmpty(input.TextFilter)) 
				resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
			return resultado; 
		} 
		protected override async Task<Models.SubjectHighs> GetEntityByIdAsync(int id) 
		{ 
			var subjectHigh = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
			return await Task.FromResult(subjectHigh); 
		} 
		protected override IQueryable<Models.SubjectHighs> ApplySorting(IQueryable<Models.SubjectHighs> query, PagedResultRequestDto input) 
		{ 
			return query.OrderBy(r => r.Name); 
		} 
 
		protected virtual void CheckErrors(IdentityResult identityResult) 
		{ 
			identityResult.CheckErrors(LocalizationManager); 
		} 
		
		public async Task<PagedResultDto<SubjectHighDto>> GetAllSubjectHighs(GdPagedResultRequestDto input) 
		{ 
			PagedResultDto<SubjectHighDto> ouput = new PagedResultDto<SubjectHighDto>(); 
			IQueryable<Models.SubjectHighs> query = query = from x in _subjectHighRepository.GetAll() 
													   select x; 
			if (!string.IsNullOrEmpty(input.TextFilter)) 
			{ 
				query = from x in _subjectHighRepository.GetAll() 
						where x.Name.Contains(input.TextFilter) 
						select x; 
			} 
			ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
			if (input.SkipCount > 0) 
			{ 
				query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
			}  
			if (input.MaxResultCount > 0) 
			{ 
				query = query.Take(input.MaxResultCount); 
			} 
			ouput.Items = ObjectMapper.Map<List<SubjectHighs.Dto.SubjectHighDto>>(query.ToList()); 
			return ouput; 
		} 
		
		[AbpAllowAnonymous]
		public async Task<List<SubjectHighDto>> GetAllSubjectHighsForCombo()
		{
			var subjectHighList = await _subjectHighRepository.GetAllListAsync(x => x.IsActive == true);

			var subjectHigh = ObjectMapper.Map<List<SubjectHighDto>>(subjectHighList.ToList());

			return subjectHigh;
		}

		[AbpAllowAnonymous]
		public async Task<List<SubjectHighDto>> GetAllSubjectHighsByCourseForCombo(int courseId)
		{
			var subjectHighList = await _subjectHighRepository.GetAllListAsync(x => x.IsActive == true && x.CourseId==courseId);

			var subjectHigh = ObjectMapper.Map<List<SubjectHighDto>>(subjectHighList.ToList());

			return subjectHigh;
		}

	} 
} ;
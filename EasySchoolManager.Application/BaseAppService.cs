﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager
{
    public class BaseAppService<TEntity, TEntityDto, TPrimaryKey, TRequestDto, TCreateDto,TUpdateDto>
        : AsyncCrudAppService<TEntity, TEntityDto, TPrimaryKey, TRequestDto, TCreateDto, TUpdateDto>,
          
        IBaseAppService<TEntityDto, TPrimaryKey, TRequestDto, TCreateDto, TUpdateDto>
         where TEntity : class, IEntity<TPrimaryKey>
        where TEntityDto : IEntityDto<TPrimaryKey>
        where TCreateDto : IEntityDto<TPrimaryKey>
        where TUpdateDto : IEntityDto<TPrimaryKey>
      
    {
        protected BaseAppService(IRepository<TEntity, TPrimaryKey> repository) : base(repository)
        {
        }

        [AbpAllowAnonymous]
        public async Task<List<TEntityDto>> GetAllForCombo()
        {
            var parameter = Expression.Parameter(typeof(TEntity), "t");
            var member = Expression.PropertyOrField(parameter, "IsActive");
            var _value = Expression.Constant(true, typeof(bool));
            var filter = Expression.Lambda<Func<TEntity, bool>>(Expression.Equal(member, _value), new ParameterExpression[] { parameter });

            var listResult = Repository.GetAll().Where(filter);
            listResult = FilterWhere(listResult);

            var result = await Task.Run<List<TEntityDto>>(() =>
                ObjectMapper.Map<List<TEntityDto>>(listResult.ToList())
            );

            return result;
        }
        [AbpAllowAnonymous]
        protected virtual IQueryable<TEntity> FilterWhere(IQueryable<TEntity> queryable)
        {
            var filters = FilterWhere<TEntity>();
            foreach (var filter in filters)
                queryable = queryable.Where(filter);

            return queryable;
        }

        [AbpAllowAnonymous]
        protected virtual List<Expression<Func<T, bool>>> FilterWhere<T>()
        {

            List<Expression<Func<T, bool>>> filters = new List<Expression<Func<T, bool>>>(); ;
            var collection = System.Web.HttpContext.Current.Request.QueryString;
            foreach (var item in collection.AllKeys)
            {
                try
                {
                    object value = null;
                    var property = ObjectType(typeof(T), item, collection[item], out value);

                    if (property == null) continue;

                    var parameter = Expression.Parameter(typeof(T), "t");
                    var member = Expression.PropertyOrField(parameter, property.Name);
                    var _value = Expression.Constant(value, property.PropertyType);
                    var filter = Expression.Lambda<Func<T, bool>>(Expression.Equal(member, _value), new ParameterExpression[] { parameter });
                    filters.Add(filter);

                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return filters;
        }
        [AbpAllowAnonymous]
        PropertyInfo ObjectType(Type _type, string name, object _value, out object value)
        {
            value = _value;
            var property = _type.GetProperties().FirstOrDefault(t => string.Compare(t.Name, name, true) == 0);

            if (property == null) return null;

            Type propertyType = property.PropertyType;

            if (propertyType.IsGenericType && propertyType.Name == typeof(Nullable<>).Name)
                propertyType = propertyType.GetGenericArguments()[0];

            if (propertyType == typeof(DateTime))
            {
                string[] formats = { "yyyy-MM-ddTHH:mm:ss.000Z", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", "yyyy-MM-ddTHH:mm:ss" };
                value = DateTime.ParseExact(_value.ToString(), formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
            else if (propertyType == typeof(int))
                value = Convert.ToInt32(_value);
            else if (propertyType == typeof(long))
                value = Convert.ToInt64(_value);
            else if (propertyType == typeof(decimal))
                value = Convert.ToDecimal(_value);
            else if (propertyType == typeof(float))
                value = Convert.ToDouble(_value);
            else if (propertyType == typeof(bool))
                value = Convert.ToBoolean(_value);

            return property;
        }


    }
}

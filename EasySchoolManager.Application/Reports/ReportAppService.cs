//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Reports.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using System.Data.SqlClient;
using EasySchoolManager.ADO;
using Microsoft.Reporting.WebForms;
using System.Data;
using Abp.UI;
using System;
using EasySchoolManager.ReportsFilters.Dto;
using System.Globalization;
using Abp.AutoMapper;
using EasySchoolManager.Users;
using EasySchoolManager.Authorization.Users;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing;

namespace EasySchoolManager.Reports
{
    [AbpAuthorize()]
    public class ReportAppService : AsyncCrudAppService<Models.Reports, _ReportDto, int, PagedResultRequestDto, CreateReportDto, UpdateReportDto>,
        IReportAppService
    {
        private readonly IRepository<Models.Reports, int> _ReportRepository;
        private readonly IRepository<Models.ReportsTypes, int> _ReportTypesRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<MultiTenancy.Tenant> _tenatRepository;
        private readonly IRepository<EasySchoolManager.Models.MasterTenants> _masterRepository;
        private readonly IRepository<EasySchoolManager.Models.ReportDataSources> _ReportDataSourcesRepository;
        private readonly IRepository<EasySchoolManager.Models.DataType> _dataTypeRepository;
        private readonly IDBService _IDBService;

        public ReportAppService(
            IRepository<Models.Reports, int> ReportRepository,
            IRepository<Models.ReportsTypes, int> ReportTypesRepository,
            IRepository<User, long> userRepository,
            IRepository<MultiTenancy.Tenant> tenatRepository,
            IRepository<EasySchoolManager.Models.MasterTenants> masterRepository,
            IRepository<EasySchoolManager.Models.ReportDataSources> ReportDataSourcesRepository,
            IRepository<EasySchoolManager.Models.DataType> dataTypeRepository,
            IDBService IDBService
            )
            : base(ReportRepository)
        {
            base.LocalizationSourceName = EasySchoolManagerConsts.LocalizationSourceName;
            _ReportRepository = ReportRepository;
            _ReportTypesRepository = ReportTypesRepository;
            _userRepository = userRepository;
            _tenatRepository = tenatRepository;
            _masterRepository = masterRepository;
            _ReportDataSourcesRepository = ReportDataSourcesRepository;
            _IDBService = IDBService;
            _dataTypeRepository = dataTypeRepository;
        }

        public override async Task<_ReportDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<_ReportDto> Create(CreateReportDto input)
        {
            CheckCreatePermission();
            var Report = ObjectMapper.Map<Models.Reports>(input);
            Report.IsActive = true;
            await _ReportRepository.InsertAndGetIdAsync(Report);
            return MapToEntityDto(Report);
        }
        public override async Task<_ReportDto> Update(UpdateReportDto input)
        {
            CheckUpdatePermission();
            var Report = await _ReportRepository.GetAsync(input.Id);
            MapToEntity(input, Report);
            _ReportRepository.Update(Report);
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            var Report = await _ReportRepository.GetAsync(input.Id);
            await _ReportRepository.DeleteAsync(Report);
        }
        protected override Models.Reports MapToEntity(CreateReportDto createInput)
        {
            var Report = ObjectMapper.Map<Models.Reports>(createInput);
            return Report;
        }
        protected override void MapToEntity(UpdateReportDto input, Models.Reports Report)
        {
            ObjectMapper.Map(input, Report);
        }
        protected override async Task<Models.Reports> GetEntityByIdAsync(int id)
        {
            var Report = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(Report);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<_ReportDto>> GetAllReports(GdPagedResultRequestDto input)
        {
            PagedResultDto<_ReportDto> ouput = new PagedResultDto<_ReportDto>();
            IQueryable<Models.Reports> query = query = from x in _ReportRepository.GetAll()
                                                       where x.IsActive == true
                                                       select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _ReportRepository.GetAll()
                        where x.ReportName.Contains(input.TextFilter) && x.IsActive == true
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Reports.Dto._ReportDto>>(query.OrderBy(f => f.ReportsTypesId).ToList());
            return ouput;
        }

        public async Task<List<ReportsTypesDto>> GetAllReportsForMenu()
        {

            var _reports = await _ReportRepository.GetAllListAsync(t => t.IsActive == true);

            var result = (from r in _reports
                          group r by new { r.ReportsTypesId, r.ReportsTypes.Descripcion, r.ReportsTypes.Nombre } into g
                          select new ReportsTypesDto
                          {
                              Id = g.Key.ReportsTypesId,
                              Descripcion = g.Key.Descripcion,
                              Nombre = g.Key.Nombre,
                              Reports = ObjectMapper.Map<List<_ReportDto>>(g)
                          }).ToList();

            return result;

            //var ReportType = _ReportTypesRepository.GetAll();


            //var result = ObjectMapper.Map<List<ReportsTypesDto>>(ReportType);



            //return result;
        }

        public async Task<List<_ReportDto>> GetAllReportsForCombo()
        {
            var ReportList = await _ReportRepository.GetAllListAsync(x => x.IsActive == true);

            var Report = ObjectMapper.Map<List<_ReportDto>>(ReportList.ToList());

            return Report;
        }

        public async Task<List<_ReportDto>> GetAllReportsForCombo2()
        {
            var ReportList = await _ReportRepository.GetAllListAsync();

            var Report = ObjectMapper.Map<List<_ReportDto>>(ReportList.ToList());

            return Report;
        }

        [AbpAllowAnonymous]
        public async Task<List<ReportsTypesDto>> GetAllReportsTypesForCombo()
        {
            var reportsTypesList = await _ReportTypesRepository.GetAllListAsync(x => x.IsActive == true);

            var reporTypes = ObjectMapper.Map<List<ReportsTypesDto>>(reportsTypesList.ToList());

            return reporTypes;
        }


        /////////Seccion nueva --Elias

        public byte[] GetDynamicReport(_ReportDto _report, out string mimeType)
        {
            Warning[] warnings;
            string[] streamids;
            string encoding;
            string filenameExtension;

            var _reportViewer = GetDynamicLocalReport(_report);
            var result = _reportViewer.LocalReport.Render(_report.ReportExport, null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

            return result;
        }

        public ReportViewer GetDynamicLocalReport(_ReportDto _report)
        {

            string reportExport = _report.ReportExport;
            var report = _ReportRepository.GetAll().FirstOrDefault(t => t.Id == _report.Id || t.ReportCode == _report.ReportCode).MapTo<_ReportDto>();
            ReportViewer result = null;

            Func<_ReportsFiltersDto, _ReportsFiltersDto> Eval = (_ReportsFiltersDto item) =>
            {
                var _item = _report.ReportsFilters.Find(t => t.Id == item.Id || t.Name?.ToUpper() == item.Name.ToUpper());
                item.Value = _item?.Value;
                item.RangeValue = _item?.RangeValue;
                item.ValueText = _item?.ValueText;
                return item;
            };
            try
            {

                if (!string.IsNullOrEmpty(report.PermissionName) && !PermissionChecker.IsGranted(report.PermissionName))
                {
                    throw new AbpAuthorizationException("You are not authorized to view this report!");
                }

                report.ReportExport = reportExport;
                report.ReportsFilters = report.ReportsFilters.Select(Eval).ToList();

                DataSet source = null;

                var parameters = new List<SqlParameter>();
                var rptViewer = new Microsoft.Reporting.WebForms.ReportViewer();
                var path = $"{report.ReportRoot}{report.ReportPath}";

                if (report.FilterByTenant && AbpSession.TenantId.HasValue)
                {
                    var tenantFilter = report.ReportsFilters.Where(f => f.DataField == "TenantId").FirstOrDefault();
                    if (tenantFilter == null || string.IsNullOrEmpty(tenantFilter.Value))
                    {
                        var _filter = new _ReportsFiltersDto { Name = "TenantId", DataField = "TenantId", Value = $"{AbpSession.TenantId}" };
                        _filter.DataType = new DataTypeDto() { Code = EasySchoolManagerConsts.DataType.Number };
                        report.ReportsFilters.Add(_filter);
                    }

                }

                //if(!System.IO.File.Exists(path))
                //    throw new UserFriendlyException(L("ReportFileNotFound"));

                if (!File.Exists(path))
                {
                    var i = 0;
                    //throw new UserFriendlyException(L("TheReportDoesNotExistInTheSpecifiedPath"));
                }


                rptViewer.LocalReport.ReportPath = path;
                rptViewer.LocalReport.EnableExternalImages = true;



                //const string dataSourceView = "01";
                //const string dataSourceProcedure = "02";
                //const string dataSourceQuery = "03";
                //const string dataSourceReporte = "04";

                string query = "";

                if (report.ReportDataSource_Code == EasySchoolManagerConsts.DataSource.Report)
                {
                    foreach (var parameter in report.ReportsFilters)
                    {
                        rptViewer.LocalReport.SetParameters(new ReportParameter(parameter.Name, parameter.Value));
                        if (parameter.Range)
                            rptViewer.LocalReport.SetParameters(new ReportParameter(parameter.Name + "Range", parameter.RangeValue));
                    }

                }
                else if (report.ReportDataSource_Code == EasySchoolManagerConsts.DataSource.ViewOrTable)
                {
                    string _where = GetWhere(report.ReportsFilters, parameters);
                    query = $"SELECT * FROM {report.View} {_where}";
                    source = _IDBService.ExecuteQuerySet(query, parameters.ToArray());

                    for (int i = 0; i < source.Tables.Count; i++)
                        rptViewer.LocalReport.DataSources.Add(new ReportDataSource($"DataSet{i + 1}", source.Tables[i]));

                }
                else if (report.ReportDataSource_Code == EasySchoolManagerConsts.DataSource.Query)
                {
                    string _where = GetWhere(report.ReportsFilters, parameters);
                    query = $"{report.View} {_where}";
                    source = _IDBService.ExecuteQuerySet(query, parameters.ToArray());

                    for (int i = 0; i < source.Tables.Count; i++ )
                        rptViewer.LocalReport.DataSources.Add(new ReportDataSource($"DataSet{i+1}", source.Tables[i]));

                    
                }
                else if (report.ReportDataSource_Code == EasySchoolManagerConsts.DataSource.Procedure)
                {
                    GetWhere(report.ReportsFilters, parameters);
                    query = $"{report.View}";
                    source = _IDBService.ExecuteQuerySet(query, CommandType.StoredProcedure, parameters.ToArray());
                    for (int i = 0; i < source.Tables.Count; i++)
                        rptViewer.LocalReport.DataSources.Add(new ReportDataSource($"DataSet{i + 1}", source.Tables[i]));
                }

                LoadDefaultParamereters(rptViewer.LocalReport, source.Tables[0]);
                LoadParameter(rptViewer.LocalReport, rptViewer.LocalReport.GetParameters(), report.ReportsFilters);
                result = rptViewer;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }

            return result;
        }

        public void LoadDefaultParamereters(LocalReport localReport, DataTable source)
        {
            try
            {
                var master = _masterRepository.GetAll().FirstOrDefault();
                var masterType = master.GetType();

                var tenant = AbpSession.TenantId.HasValue ? _tenatRepository.Get(AbpSession.TenantId.Value) : null;
                var tenantType = typeof(MultiTenancy.Tenant);

                var user = _userRepository.Get(AbpSession.UserId.Value);
                var userType = user.GetType();

                foreach (var parameter in localReport.GetParameters())
                {
                    if (parameter.Name.EndsWith("Master"))
                    {
                        var _property = masterType.GetProperty(parameter.Name.Replace("Master", ""));
                        if (_property != null)
                            localReport.SetParameters(new ReportParameter(parameter.Name, _property.GetValue(master, null)?.ToString()));
                    }
                    else if (parameter.Name.EndsWith("Tenant"))
                    {
                        var _property = tenantType.GetProperty(parameter.Name.Replace("Tenant", ""));
                        if (_property != null && tenant != null)
                            localReport.SetParameters(new ReportParameter(parameter.Name, _property.GetValue(tenant, null)?.ToString()));
                    }
                    else if (parameter.Name.EndsWith("User"))
                    {
                        var _property = userType.GetProperty(parameter.Name.Replace("User", ""));
                        if (_property != null)
                            localReport.SetParameters(new ReportParameter(parameter.Name, _property.GetValue(user, null)?.ToString()));
                    }
                    else if (System.Text.RegularExpressions.Regex.Match(parameter.Name, @"^(Column(\d+))$").Success)
                    {
                        int index = int.Parse(parameter.Name.Replace("Column", ""));
                        if (source != null && source.Columns.Count > index)
                            localReport.SetParameters(new ReportParameter(parameter.Name, L(source?.Columns[index].ColumnName)));
                    }
                    if (parameter.Name.EndsWith("ServerHost"))
                    {
                        var host = $"{System.Web.HttpContext.Current.Request.Url.Scheme}://{System.Web.HttpContext.Current.Request.Url.Host}:{System.Web.HttpContext.Current.Request.Url.Port}{System.Web.HttpContext.Current.Request.ApplicationPath}";
                        localReport.SetParameters(new ReportParameter(parameter.Name, host));
                    }
                }
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        public void LoadParameter(LocalReport report, ReportParameterInfoCollection parameters, IEnumerable<_ReportsFiltersDto> filters)
        {
            foreach (var parameter in parameters)
            {

                if (parameter.Name.EndsWith("Label"))
                    report.SetParameters(new ReportParameter(parameter.Name, L(parameter.Name.Replace("Label", ""))));
                else if (parameter.Name.EndsWith("Text"))
                    report.SetParameters(new ReportParameter(parameter.Name, filters.FirstOrDefault(t => t.Name == parameter.Name.Replace("Text", ""))?.ValueText));
                else if (parameter.Name.EndsWith("RangeValue"))
                    report.SetParameters(new ReportParameter(parameter.Name, filters.FirstOrDefault(t => t.Name == parameter.Name.Replace("RangeValue", ""))?.RangeValue));
                else if (parameter.Name.EndsWith("Value"))
                    report.SetParameters(new ReportParameter(parameter.Name, filters.FirstOrDefault(t => t.Name == parameter.Name.Replace("Value", ""))?.Value));
            }
        }

        public string GetWhere(IEnumerable<_ReportsFiltersDto> filters, List<SqlParameter> parameters)
        {
            bool hasWhere = false;
            string result = "";
            string oper = " WHERE ";

            foreach (var filter in filters)
            {
                if (string.IsNullOrEmpty(filter.Value)) continue;
                if (filter.Range && string.IsNullOrEmpty(filter.RangeValue)) continue;
                if (filter.OnlyParameter) continue;

                SqlParameter parameter = null;
                SqlParameter parameterRange = null;
                if (filter.Range)
                {
                    parameter = new SqlParameter("@" + filter.Name, GetDato(filter, filter.Value, false, false));
                    parameterRange = new SqlParameter($"@{filter.Name}Range", GetDato(filter, filter.RangeValue, true, false));
                    parameters.Add(parameter);
                    parameters.Add(parameterRange);
                    result += $"{oper} {filter.DataField} BETWEEN @{filter.Name} AND @{filter.Name}Range";
                }
                else
                {
                    parameter = new SqlParameter("@" + filter.Name, GetDato(filter, filter.Value, false, false));
                    parameters.Add(parameter);
                    if (filter.Operator?.ToLower() == "like")
                        result += $"{oper} {filter.DataField} LIKE '%' + @{filter.Name} + '%'";
                    else
                        result += $"{oper} {filter.DataField} {(string.IsNullOrEmpty(filter.Operator) ? "=" : filter.Operator)} @{filter.Name} ";
                }

                if (!hasWhere)
                {
                    oper = " AND ";
                    hasWhere = true;
                }


            }
            return result;
        }

        public string GetDato(_ReportsFiltersDto filter, string value, bool range, bool quote = true)
        {
            string q = quote ? "'" : "";

            if (filter.DataType.Code == EasySchoolManagerConsts.DataType.Number)
                return value;
            if (filter.DataType.Code == EasySchoolManagerConsts.DataType.Text)
                return $"{q}{value}{q}";
            if (filter.DataType.Code == EasySchoolManagerConsts.DataType.Date)
            {
                DateTime _value = DateTime.ParseExact(value, new string[] {
                    "yyyy-MM-ddTHH:mm:ss.000Z",
                    "yyyy-MM-ddTHH:mm:ss",
                    "yyyy-MM-dd HH:mm:ss",
                    "yyyy-MM-dd"}, CultureInfo.InvariantCulture, DateTimeStyles.None);
                if (range)
                    return $"{q}{_value:yyyy-MM-dd}T23:59:59{q}";

                return $"{q}{_value:yyyy-MM-dd}T00:00:00{q}";
            }
            if (filter.DataType.Code == EasySchoolManagerConsts.DataType.Bit)
                return $"{q}{value}{q}";

            return "";
        }



        public ReportDirectOutput GetReportDirect(ReportDirectInput input)
        {
            var output = new ReportDirectOutput();
            var data = (List<TableDataOutput>)GetDynamicData(input.Report);
            var rptViewer = new Microsoft.Reporting.WebForms.ReportViewer();
            string path = $"{input.Report.ReportRoot}{input.Report.ReportPath}";
            rptViewer.LocalReport.ReportPath = path;
            var reportPath = System.Web.HttpContext.Current.Server.MapPath(input.Report.ReportPath.StartsWith("~") ? path : "~/" + path);

            LoadDefaultParamereters(rptViewer.LocalReport, (DataTable)data[0].Table);
            LoadParameter(rptViewer.LocalReport, rptViewer.LocalReport.GetParameters(), input.Report.ReportsFilters);

            output.Data = data;
            output.Parameters = new List<ReportParamterDto>();
            foreach (var item in rptViewer.LocalReport.GetParameters())
            {
                output.Parameters.Add(new ReportParamterDto { Name = item.Name, Value = item.Values[0] });
            }

            var fileInfo = new System.IO.FileInfo(reportPath);
            if (!input.RdlcTime.HasValue || input.RdlcTime.Value.ToUniversalTime() != fileInfo.LastWriteTimeUtc)
            {
                var _file = File.ReadAllBytes(reportPath);
                output.RdlcFile = Convert.ToBase64String(_file);
                output.RdlcModifiedTime = fileInfo.LastWriteTimeUtc;
            }

            return output;
        }

        public object GetDynamicData(_ReportDto _report)
        {

            string reportExport = _report.ReportExport;
            var report = _ReportRepository.GetAll().FirstOrDefault(t => t.Id == _report.Id || t.ReportCode == _report.ReportCode).MapTo<_ReportDto>();
            List<TableDataOutput> result = new List<TableDataOutput>();

            Func<_ReportsFiltersDto, _ReportsFiltersDto> Eval = (_ReportsFiltersDto item) =>
            {
                var _item = _report.ReportsFilters.Find(t => t.Id == item.Id || t.Name?.ToUpper() == item.Name.ToUpper());
                item.Value = _item?.Value;
                item.RangeValue = _item?.RangeValue;
                item.ValueText = _item?.ValueText;
                return item;
            };
            try
            {

                if (!string.IsNullOrEmpty(report.PermissionName) && !PermissionChecker.IsGranted(report.PermissionName))
                {
                    throw new AbpAuthorizationException("You are not authorized to view this report!");
                }

                report.ReportExport = reportExport;
                report.ReportsFilters = report.ReportsFilters.Select(Eval).ToList();

                DataSet source = null;

                var parameters = new List<SqlParameter>();
                var rptViewer = new Microsoft.Reporting.WebForms.ReportViewer();
                var path = $"{report.ReportRoot}{report.ReportPath}";

                if (report.FilterByTenant && AbpSession.TenantId.HasValue)
                {
                    var tenantFilter = report.ReportsFilters.Where(f => f.DataField == "TenantId").FirstOrDefault();
                    if (tenantFilter == null || string.IsNullOrEmpty(tenantFilter.Value))
                    {
                        var _filter = new _ReportsFiltersDto { Name = "TenantId", DataField = "TenantId", Value = $"{AbpSession.TenantId}" };
                        _filter.DataType = new DataTypeDto() { Code = EasySchoolManagerConsts.DataType.Number };
                        report.ReportsFilters.Add(_filter);
                    }

                }

                rptViewer.LocalReport.ReportPath = path;
                ObjectMapper.Map(report, _report);
                string query = "";

                if (report.ReportDataSource_Code == EasySchoolManagerConsts.DataSource.ViewOrTable)
                {
                    string _where = GetWhere(report.ReportsFilters, parameters);
                    query = $"SELECT * FROM {report.View} {_where}";
                    source = _IDBService.ExecuteQuerySet(query, parameters.ToArray());
                }
                else if (report.ReportDataSource_Code == EasySchoolManagerConsts.DataSource.Query)
                {
                    string _where = GetWhere(report.ReportsFilters, parameters);
                    query = $"{report.View} {_where}";
                    source = _IDBService.ExecuteQuerySet(query, parameters.ToArray());
                }
                else if (report.ReportDataSource_Code == EasySchoolManagerConsts.DataSource.Procedure)
                {
                    GetWhere(report.ReportsFilters, parameters);
                    query = $"{report.View}";
                    source = _IDBService.ExecuteQuerySet(query, CommandType.StoredProcedure, parameters.ToArray());
                }


                if (source != null)
                {
                    foreach (DataTable table in source.Tables)
                    {
                        var _table = new TableDataOutput();
                        _table.Columns = new List<TableColumnDto>();
                        _table.Table = table;
                        for (int i = table.Columns.Count - 1; i >= 0; i--)
                        {
                            string columnName = table.Columns[i].ColumnName;
                            if (columnName.EndsWith("Id") || columnName.StartsWith("Column"))
                                table.Columns.RemoveAt(i);
                            else
                                _table.Columns.Add(new TableColumnDto(columnName, table.Columns[i].DataType));
                        }

                        result.Add(_table);
                    }

                }

            }
            catch (Exception ex)
            {

                throw new UserFriendlyException($"{ex.Message}.{ex.InnerException?.Message}.");
            }

            return result;
        }

        /************************/

        public async Task<List<ReportDataSourceDto>> GetReportDataSources()
        {

            var result = await _ReportDataSourcesRepository.GetAllListAsync();

            return result.MapTo<List<ReportDataSourceDto>>();
        }

        public async Task<List<SourceDto>> GetReportSources(string dataSourceCode)
        {

            string query = "";

            if (dataSourceCode == EasySchoolManagerConsts.DataSource.ViewOrTable)
                query = "SELECT name FROM sys.tables UNION ALL SELECT name FROM sys.views ORDER BY name";

            else if (dataSourceCode == EasySchoolManagerConsts.DataSource.Procedure)
                query = "SELECT name FROM sys.procedures ORDER BY name ";

            var data = await Task.Run(() => _IDBService.ExecuteQuery<SourceDto>(query));

            return data.ToList();
        }

        public async Task<List<DataTypeDto>> GetDataTypes()
        {
            var result = await _dataTypeRepository.GetAllListAsync();

            return result.MapTo<List<DataTypeDto>>();
        }
    }
};
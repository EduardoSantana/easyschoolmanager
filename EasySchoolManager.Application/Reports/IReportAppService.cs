//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Reports.Dto;
using System.Collections.Generic;
using Microsoft.Reporting.WebForms;
using EasySchoolManager.ReportsFilters.Dto;
using System.Data.SqlClient;
using System.Data;
using Abp.Web.Models;

namespace EasySchoolManager.Reports
{
    public interface IReportAppService : IAsyncCrudAppService<_ReportDto, int, PagedResultRequestDto, CreateReportDto, UpdateReportDto>
    {
        Task<PagedResultDto<_ReportDto>> GetAllReports(GdPagedResultRequestDto input);
        Task<List<Dto._ReportDto>> GetAllReportsForCombo();
        Task<List<Dto._ReportDto>> GetAllReportsForCombo2();
        Task<List<Dto.ReportsTypesDto>> GetAllReportsTypesForCombo();

        [RemoteService(IsEnabled = false,IsMetadataEnabled =false)]
        byte[] GetDynamicReport(_ReportDto _report, out string mimeType);

        Task<List<ReportsTypesDto>> GetAllReportsForMenu();

        Task<List<ReportDataSourceDto>> GetReportDataSources();

        Task<List<SourceDto>> GetReportSources(string dataSourceCode);
        Task<List<DataTypeDto>> GetDataTypes();

        ReportDirectOutput GetReportDirect(ReportDirectInput input);
    }
}
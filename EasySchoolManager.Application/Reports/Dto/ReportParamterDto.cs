﻿namespace EasySchoolManager.Reports.Dto
{
    public class ReportParamterDto
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
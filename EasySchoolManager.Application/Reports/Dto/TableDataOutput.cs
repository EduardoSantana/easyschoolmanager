﻿using System.Collections.Generic;

namespace EasySchoolManager.Reports.Dto
{
    public class TableDataOutput
    {
        public List<TableColumnDto> Columns { get; set; }
        public object Table { get; set; }
    }
}

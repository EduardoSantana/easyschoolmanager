﻿
using System;
using System.Collections.Generic;

namespace EasySchoolManager.Reports.Dto
{
    public class ReportDirectOutput
    {
        public string RdlcFile { get; set; }
        public DateTime RdlcModifiedTime { get; set; }
        public object  Data { get; set; }
        public List<ReportParamterDto> Parameters { get; set; }
    }
}
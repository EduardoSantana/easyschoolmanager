﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using EasySchoolManager.ReportsFilters.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Reports.Dto
{
    [AutoMap(typeof(Models.Reports), typeof(_ReportDto))]
    public class _ReportDto : EntityDto<int>
    {       

        public string ReportCode { get; set; }
        public string ReportName { get; set; }
        public string View { get; set; }
        public string ReportRoot { get; set; }
        public string ReportPath { get; set; }
        public string ReportDataSource_Code { get; set; }
        public bool FilterByTenant { get; set; }
        public int IsForTenant { get; set; }

        [AutoMapper.IgnoreMap]
        public string ReportExport { get; set; }

        public int ReportsTypesId { get; set; }

        public string ReportsTypes_Descripcion { get; set; }

        public List<_ReportsFiltersDto> ReportsFilters { get; set; }

        public int ReportDataSourceId { get; set; }
        public bool IsActive { get; set; }
        public string PermissionName { get; set; }
    }
}

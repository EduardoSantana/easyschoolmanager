﻿
using System;

namespace EasySchoolManager.Reports.Dto
{
    public class ReportDirectInput
    {
        public _ReportDto Report { get; set; }
        public DateTime? RdlcTime { get; set; }
    }
}
﻿using Abp.AutoMapper;

namespace EasySchoolManager.Reports.Dto
{
    [AutoMap(typeof(Models.ReportDataSources))]
    public class ReportDataSourceDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}

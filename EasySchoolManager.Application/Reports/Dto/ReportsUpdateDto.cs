//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;
using EasySchoolManager.ReportsFilters.Dto;

namespace EasySchoolManager.Reports.Dto
{
    [AutoMap(typeof(Models.Reports))]
    public class UpdateReportDto : EntityDto<int>
    {
        public int ReportsTypesId { get; set; }
        public bool IsActive { get; set; }

        public string ReportCode { get; set; }
        public string ReportName { get; set; }
        public string View { get; set; }
        public string ReportRoot { get; set; }
        public string ReportPath { get; set; }
        public string ReportDataSource_Code { get; set; }
        public bool FilterByTenant { get; set; }
        public int IsForTenant { get; set; }
        public string PermissionName { get; set; }
        public string ReportsTypes_Descripcion { get; set; }

    }
}
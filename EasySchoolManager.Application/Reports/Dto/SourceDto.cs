﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Reports.Dto
{
    public class SourceDto
    {
        public string Name { get; set; }
    }
}

﻿using Abp.Dependency;
using Abp.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Reports.Dto
{
    public class TableColumnDto
    {
        public TableColumnDto(string _name, Type _type)
        {
            type = _type;
            Name = _name;
            PropertyName = $"{_name[0].ToString().ToLower()}{_name.Substring(1)}";
        }

        Type type = null;
        public string Name { get; set; }
        public string PropertyName { get; set; }
        public string CellFilter { get {

                if (type == typeof(string))
                    return "";
                else if (type == typeof(int) || type == typeof(long))
                    return "number";
                else if (type == typeof(decimal) || type == typeof(float) || type == typeof(double))
                    return "number:2";
                else if (type == typeof(DateTime))
                    return "date:'MM/dd/yyyy'";


                return null;
            } }

        public string CellTemplate
        {
            get
            {
                var L = IocManager.Instance.Resolve<ILocalizationManager>();
                string _yes = L.GetString(EasySchoolManagerConsts.LocalizationSourceName, "Yes");
                string _no = L.GetString(EasySchoolManagerConsts.LocalizationSourceName, "No");


                if (type == typeof(bool))
                    return $@"<div class=""ui-grid-cell-contents"">
                            <span ng-show=""row.entity.isActive"" class=""label label-success"">{_yes}</span>
                           <span ng-show=""!row.entity.isActive"" class=""label label-default"">{_no}</span>
                           </div>";


                return null;
            }
        }

        public string PropertyType { get {
                return this.type?.Name;
            } }
    }
}

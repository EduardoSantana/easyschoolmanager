﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.StudenIllnesses.Dto
{
    [AutoMap(typeof(Models.StudentIllnesses))]
    public class StudentIllnessCreateDto : EntityDto
    {
        public int StudentId { get; set; }
        public int IllnessId { get; set; }
    }
}

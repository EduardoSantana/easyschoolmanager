﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace EasySchoolManager.StudentIllnesses.Dto
{
    [AutoMap(typeof(Models.StudentIllnesses))]
    public class StudentIllnessesDto : EntityDto
    {
        public int StudentId { get; set; }
        public int IllnessId { get; set; }

        public Illnesses.Dto.IllnessDto Illnesses { get; set; }

        


    }
}

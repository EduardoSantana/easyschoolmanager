//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.CourseValues.Dto 
{
        [AutoMap(typeof(Models.CourseValues))] 
        public class CourseValueDto : EntityDto<int> 
        {
              public int PeriodId {get;set;} 
              public int CourseId {get;set;} 
              public Decimal InscriptionAmount {get;set;} 
              public Decimal TotalAmount {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 
              public string Periods_Description { get; set; }
              public string Courses_Name { get; set; }
              public string Courses_Levels_Name { get; set; }
              public Periods.Dto.PeriodDto Periods { get; set; }

         }
}
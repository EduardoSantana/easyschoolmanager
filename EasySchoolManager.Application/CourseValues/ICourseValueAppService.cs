//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.CourseValues.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.CourseValues
{
      public interface ICourseValueAppService : IAsyncCrudAppService<CourseValueDto, int, PagedResultRequestDto, CreateCourseValueDto, UpdateCourseValueDto>
      {
            Task<PagedResultDto<CourseValueDto>> GetAllCourseValues(GdPagedResultRequestDto input);
            Task<CourseValueDto> GetCourseValue(int CourseId, int PeriodId);
            CourseValueDto GetCourseValues(int CourseId, int PeriodId);
			Task<List<Dto.CourseValueDto>> GetAllCourseValuesForCombo();
      }
}
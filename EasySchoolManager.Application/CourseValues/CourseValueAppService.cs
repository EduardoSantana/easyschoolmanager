//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.CourseValues.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.CourseValues
{
    [AbpAuthorize(PermissionNames.Pages_CourseValues)]
    public class CourseValueAppService : AsyncCrudAppService<Models.CourseValues, CourseValueDto, int, PagedResultRequestDto, CreateCourseValueDto, UpdateCourseValueDto>, ICourseValueAppService
    {
        private readonly IRepository<Models.CourseValues, int> _courseValueRepository;

        private readonly IRepository<Models.Courses, int> _courseRepository;

        public CourseValueAppService(
            IRepository<Models.CourseValues, int> repository,
            IRepository<Models.CourseValues, int> courseValueRepository,
            IRepository<Models.Courses, int> courseRepository

            )
            : base(repository)
        {
            _courseValueRepository = courseValueRepository;

            _courseRepository = courseRepository;
        }
        public override async Task<CourseValueDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<CourseValueDto> Create(CreateCourseValueDto input)
        {
            CheckCreatePermission();
            var courseValue = ObjectMapper.Map<Models.CourseValues>(input);
            try
            {
                await _courseValueRepository.InsertAsync(courseValue);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(courseValue);
        }
        public override async Task<CourseValueDto> Update(UpdateCourseValueDto input)
        {
            CheckUpdatePermission();
            var courseValue = await _courseValueRepository.GetAsync(input.Id);
            MapToEntity(input, courseValue);
            try
            {
                await _courseValueRepository.UpdateAsync(courseValue);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var courseValue = await _courseValueRepository.GetAsync(input.Id);
                await _courseValueRepository.DeleteAsync(courseValue);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.CourseValues MapToEntity(CreateCourseValueDto createInput)
        {
            var courseValue = ObjectMapper.Map<Models.CourseValues>(createInput);
            return courseValue;
        }
        protected override void MapToEntity(UpdateCourseValueDto input, Models.CourseValues courseValue)
        {
            ObjectMapper.Map(input, courseValue);
        }
        protected override IQueryable<Models.CourseValues> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.CourseValues> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Courses.Name.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.CourseValues> GetEntityByIdAsync(int id)
        {
            var courseValue = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(courseValue);
        }
        protected override IQueryable<Models.CourseValues> ApplySorting(IQueryable<Models.CourseValues> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Courses.Name);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<CourseValueDto> GetCourseValue(int CourseId, int PeriodId)
        {
            CourseValueDto course = null;
            var courseQuery = await _courseValueRepository.GetAllListAsync(x => x.CourseId == CourseId && x.PeriodId == PeriodId);
            var courseX = courseQuery.FirstOrDefault();

            if (courseX != null)
                course = ObjectMapper.Map<CourseValueDto>(courseX);
            return course;
        }

        public CourseValueDto GetCourseValues(int CourseId, int PeriodId)
        {


            CourseValueDto course = null;
            var courseQuery =  _courseValueRepository.GetAllList(x => x.CourseId == CourseId && x.PeriodId == PeriodId  );
            var courseX = courseQuery.FirstOrDefault();

            if (courseX != null)
                course = ObjectMapper.Map<CourseValueDto>(courseX);
            return course;
        }

        public async Task<PagedResultDto<CourseValueDto>> GetAllCourseValues(GdPagedResultRequestDto input)
        {
            PagedResultDto<CourseValueDto> ouput = new PagedResultDto<CourseValueDto>();
            IQueryable<Models.CourseValues> query = query = from x in _courseValueRepository.GetAll()
                                                            select x;

            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _courseValueRepository.GetAll()
                        where x.Courses.Name.Contains(input.TextFilter)
                        select x;

            }

            if (input.PeriodId.HasValue && input.PeriodId.Value > 0)
                query = query.Where(x => x.PeriodId == input.PeriodId);

            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<CourseValues.Dto.CourseValueDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<CourseValueDto>> GetAllCourseValuesForCombo()
        {
            var courseValueList = await _courseValueRepository.GetAllListAsync(x => x.IsActive == true);

            var courseValue = ObjectMapper.Map<List<CourseValueDto>>(courseValueList.ToList());

            return courseValue;
        }

    }
};
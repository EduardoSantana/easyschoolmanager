//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper;
using EasySchoolManager.DiscountNames.Dto;
using EasySchoolManager.MultiTenancy.Dto;

namespace EasySchoolManager.DiscountNameTenants.Dto 
{
        [AutoMap(typeof(Models.DiscountNameTenants))] 
        public class DiscountNameTenantDto : EntityDto<int> 
        {
              public int PeriodDiscountId { get;set;} 
              public int TenantId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 
            
              public DiscountNameDto DiscountNames { get; set; }
              public TenantDto Tenant { get; set; }

         }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.DiscountNameTenants.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.DiscountNameTenants 
{
    [AbpAllowAnonymous]
    public class DiscountNameTenantAppService : AsyncCrudAppService<Models.DiscountNameTenants, DiscountNameTenantDto, int, PagedResultRequestDto, CreateDiscountNameTenantDto, UpdateDiscountNameTenantDto>, IDiscountNameTenantAppService 
    { 
        private readonly IRepository<Models.DiscountNameTenants, int> _discountNameTenantRepository;
		
		    private readonly IRepository<Models.DiscountNames, int> _discountNameRepository;


        public DiscountNameTenantAppService( 
            IRepository<Models.DiscountNameTenants, int> repository, 
            IRepository<Models.DiscountNameTenants, int> discountNameTenantRepository ,
            IRepository<Models.DiscountNames, int> discountNameRepository

            ) 
            : base(repository) 
        { 
            _discountNameTenantRepository = discountNameTenantRepository; 
			
            _discountNameRepository = discountNameRepository;


			
        } 
        public override async Task<DiscountNameTenantDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<DiscountNameTenantDto> Create(CreateDiscountNameTenantDto input) 
        { 
            CheckCreatePermission(); 
            var discountNameTenant = ObjectMapper.Map<Models.DiscountNameTenants>(input);
            discountNameTenant.IsActive = true;
			try{
              await _discountNameTenantRepository.InsertAsync(discountNameTenant); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(discountNameTenant); 
        } 
        public override async Task<DiscountNameTenantDto> Update(UpdateDiscountNameTenantDto input) 
        { 
            CheckUpdatePermission(); 
            var discountNameTenant = await _discountNameTenantRepository.GetAsync(input.Id);
            MapToEntity(input, discountNameTenant); 
		    try{
               await _discountNameTenantRepository.UpdateAsync(discountNameTenant); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var discountNameTenant = await _discountNameTenantRepository.GetAsync(input.Id); 
               await _discountNameTenantRepository.DeleteAsync(discountNameTenant);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.DiscountNameTenants MapToEntity(CreateDiscountNameTenantDto createInput) 
        { 
            var discountNameTenant = ObjectMapper.Map<Models.DiscountNameTenants>(createInput); 
            return discountNameTenant; 
        } 
        protected override void MapToEntity(UpdateDiscountNameTenantDto input, Models.DiscountNameTenants discountNameTenant) 
        { 
            ObjectMapper.Map(input, discountNameTenant); 
        } 
        protected override IQueryable<Models.DiscountNameTenants> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.DiscountNameTenants> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.PeriodDiscounts.DiscountNames.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.DiscountNameTenants> GetEntityByIdAsync(int id) 
        { 
            var discountNameTenant = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(discountNameTenant); 
        } 
        protected override IQueryable<Models.DiscountNameTenants> ApplySorting(IQueryable<Models.DiscountNameTenants> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.PeriodDiscounts.DiscountNames.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<DiscountNameTenantDto>> GetAllDiscountNameTenants(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<DiscountNameTenantDto> ouput = new PagedResultDto<DiscountNameTenantDto>(); 
            IQueryable<Models.DiscountNameTenants> query = query = from x in _discountNameTenantRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _discountNameTenantRepository.GetAll() 
                        where x.PeriodDiscounts.DiscountNames.Name.Contains(input.TextFilter) 
                        select x; 
            }

            //Fue usado el parametro periodId, para no tener que crear otro objeto input, pero realmente se refiere a discountName.
            if (input.PeriodId.HasValue)
            {
                query = query.Where(x => x.PeriodDiscountId == input.PeriodId.Value);
            }

            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<DiscountNameTenants.Dto.DiscountNameTenantDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<DiscountNameTenantDto>> GetAllDiscountNameTenantsForCombo()
        {
            var discountNameTenantList = await _discountNameTenantRepository.GetAllListAsync(x => x.IsActive == true);

            var discountNameTenant = ObjectMapper.Map<List<DiscountNameTenantDto>>(discountNameTenantList.ToList());

            return discountNameTenant;
        }
		
    } 
} ;
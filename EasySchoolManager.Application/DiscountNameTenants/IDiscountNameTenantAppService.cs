//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.DiscountNameTenants.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.DiscountNameTenants
{
      public interface IDiscountNameTenantAppService : IAsyncCrudAppService<DiscountNameTenantDto, int, PagedResultRequestDto, CreateDiscountNameTenantDto, UpdateDiscountNameTenantDto>
      {
            Task<PagedResultDto<DiscountNameTenantDto>> GetAllDiscountNameTenants(GdPagedResultRequestDto input);
			Task<List<Dto.DiscountNameTenantDto>> GetAllDiscountNameTenantsForCombo();
      }
}
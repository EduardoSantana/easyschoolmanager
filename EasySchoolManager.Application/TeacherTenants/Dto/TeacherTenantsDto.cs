//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper;
using EasySchoolManager.Authorization.Users;

namespace EasySchoolManager.TeacherTenants.Dto 
{
    [AutoMap(typeof(Models.TeacherTenants))] 
    public class TeacherTenantsDto : EntityDto<int> 
    {
        public int TeacherId { get; set; }

        public int TenantId { get; set; }

        public string Teachers_Name { get; set; }
        
    }
}
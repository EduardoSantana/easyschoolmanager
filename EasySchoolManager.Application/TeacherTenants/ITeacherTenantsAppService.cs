using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.TeacherTenants.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.TeacherTenants
{
      public interface ITeacherTenantAppService : IAsyncCrudAppService<TeacherTenantsDto, int, PagedResultRequestDto, TeacherTenantsDto, TeacherTenantsDto>
      {
            Task<PagedResultDto<TeacherTenantsDto>> GetAllTeacherTenants(GdPagedResultRequestDto input);
			Task<List<Dto.TeacherTenantsDto>> GetAllTeacherTenantsForCombo();
      }
}
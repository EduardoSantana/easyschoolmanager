//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.TeacherTenants.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.TeacherTenants 
{ 
	[AbpAuthorize(PermissionNames.Pages_Teachers)] 
	public class TeacherTenantAppService : AsyncCrudAppService<Models.TeacherTenants, TeacherTenantsDto, int, PagedResultRequestDto, TeacherTenantsDto, TeacherTenantsDto>, ITeacherTenantAppService
	{ 
		private readonly IRepository<Models.TeacherTenants, int> _teacherRepository;
		
		public TeacherTenantAppService( 
			IRepository<Models.TeacherTenants, int> repository, 
			IRepository<Models.TeacherTenants, int> teacherRepository 
			) 
			: base(repository) 
		{ 
			_teacherRepository = teacherRepository; 
			
		} 
	   
		protected virtual void CheckErrors(IdentityResult identityResult) 
		{ 
			identityResult.CheckErrors(LocalizationManager); 
		} 
		
		public async Task<PagedResultDto<TeacherTenantsDto>> GetAllTeacherTenants(GdPagedResultRequestDto input) 
		{ 
			PagedResultDto<TeacherTenantsDto> ouput = new PagedResultDto<TeacherTenantsDto>(); 
			IQueryable<Models.TeacherTenants> query = query = from x in _teacherRepository.GetAll() 
													   select x; 
			if (!string.IsNullOrEmpty(input.TextFilter)) 
			{ 
			  
			} 
			ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
			if (input.SkipCount > 0) 
			{ 
				query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
			}  
			if (input.MaxResultCount > 0) 
			{ 
				query = query.Take(input.MaxResultCount); 
			} 
			ouput.Items = ObjectMapper.Map<List<TeacherTenants.Dto.TeacherTenantsDto>>(query.ToList()); 
			return ouput; 
		} 
		
		[AbpAllowAnonymous]
		public async Task<List<TeacherTenantsDto>> GetAllTeacherTenantsForCombo()
		{
			var teacherList = await _teacherRepository.GetAllListAsync(x => x.IsActive == true);

			var teacher = ObjectMapper.Map<List<TeacherTenantsDto>>(teacherList.ToList());

			return teacher;
		}
		
	} 
} ;
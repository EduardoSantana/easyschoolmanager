//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Genders.Dto 
{
        [AutoMap(typeof(Models.Genders))] 
        public class GenderDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public string Name {get;set;} 
              public string Code {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Genders.Dto 
{
        [AutoMap(typeof(Models.Genders))] 
        public class CreateGenderDto : EntityDto<int> 
        {

              [StringLength(10)] 
              public string Name {get;set;} 

              [StringLength(1)] 
              public string Code {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
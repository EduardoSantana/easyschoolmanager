//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Genders.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Genders
{
      public interface IGenderAppService : IAsyncCrudAppService<GenderDto, int, PagedResultRequestDto, CreateGenderDto, UpdateGenderDto>
      {
            Task<PagedResultDto<GenderDto>> GetAllGenders(GdPagedResultRequestDto input);
			Task<List<Dto.GenderDto>> GetAllGendersForCombo();
      }
}
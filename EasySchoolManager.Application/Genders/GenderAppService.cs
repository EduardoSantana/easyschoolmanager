//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Genders.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Genders 
{ 
    [AbpAuthorize(PermissionNames.Pages_Genders)] 
    public class GenderAppService : AsyncCrudAppService<Models.Genders, GenderDto, int, PagedResultRequestDto, CreateGenderDto, UpdateGenderDto>, IGenderAppService 
    { 
        private readonly IRepository<Models.Genders, int> _genderRepository;
		


        public GenderAppService( 
            IRepository<Models.Genders, int> repository, 
            IRepository<Models.Genders, int> genderRepository 
            ) 
            : base(repository) 
        { 
            _genderRepository = genderRepository; 
			

			
        } 
        public override async Task<GenderDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<GenderDto> Create(CreateGenderDto input) 
        { 
            CheckCreatePermission(); 
            var gender = ObjectMapper.Map<Models.Genders>(input); 
			try{
              await _genderRepository.InsertAsync(gender); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(gender); 
        } 
        public override async Task<GenderDto> Update(UpdateGenderDto input) 
        { 
            CheckUpdatePermission(); 
            var gender = await _genderRepository.GetAsync(input.Id);
            MapToEntity(input, gender); 
		    try{
               await _genderRepository.UpdateAsync(gender); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var gender = await _genderRepository.GetAsync(input.Id); 
               await _genderRepository.DeleteAsync(gender);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Genders MapToEntity(CreateGenderDto createInput) 
        { 
            var gender = ObjectMapper.Map<Models.Genders>(createInput); 
            return gender; 
        } 
        protected override void MapToEntity(UpdateGenderDto input, Models.Genders gender) 
        { 
            ObjectMapper.Map(input, gender); 
        } 
        protected override IQueryable<Models.Genders> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Genders> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Genders> GetEntityByIdAsync(int id) 
        { 
            var gender = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(gender); 
        } 
        protected override IQueryable<Models.Genders> ApplySorting(IQueryable<Models.Genders> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<GenderDto>> GetAllGenders(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<GenderDto> ouput = new PagedResultDto<GenderDto>(); 
            IQueryable<Models.Genders> query = query = from x in _genderRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _genderRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Genders.Dto.GenderDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<GenderDto>> GetAllGendersForCombo()
        {
            var genderList = await _genderRepository.GetAllListAsync(x => x.IsActive == true);

            var gender = ObjectMapper.Map<List<GenderDto>>(genderList.ToList());

            return gender;
        }
		
    } 
} ;
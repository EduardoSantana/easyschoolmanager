﻿using Abp.AutoMapper;
using EasySchoolManager.Concepts.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.TransactionConcepts.Dto
{
    [AutoMap(typeof(Models.TransactionConcepts))]
    public class TransactionConceptsDto
    {
        public long? TransactionId { get; set; }

        public int ConceptId { get; set; }

        public decimal Amount { get; set; }

        public virtual ConceptDto Concepts { get; set; }
    }
}

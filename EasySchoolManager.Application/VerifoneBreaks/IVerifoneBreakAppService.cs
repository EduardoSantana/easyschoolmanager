//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.VerifoneBreaks.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.VerifoneBreaks
{
      public interface IVerifoneBreakAppService : IAsyncCrudAppService<VerifoneBreakDto, int, PagedResultRequestDto, CreateVerifoneBreakDto, UpdateVerifoneBreakDto>
      {
            Task<PagedResultDto<VerifoneBreakDto>> GetAllVerifoneBreaks(GdPagedResultRequestDto input);
			Task<List<Dto.VerifoneBreakDto>> GetAllVerifoneBreaksForCombo();
      }
}
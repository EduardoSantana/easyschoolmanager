//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.VerifoneBreaks.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.VerifoneBreaks 
{ 
    [AbpAuthorize(PermissionNames.Pages_VerifoneBreaks)] 
    public class VerifoneBreakAppService : AsyncCrudAppService<Models.VerifoneBreaks, VerifoneBreakDto, int, PagedResultRequestDto, CreateVerifoneBreakDto, UpdateVerifoneBreakDto>, IVerifoneBreakAppService 
    { 
        private readonly IRepository<Models.VerifoneBreaks, int> _verifoneBreakRepository;
		
		    private readonly IRepository<Models.Banks, int> _bankRepository;
		    private readonly IRepository<Models.BankAccounts, int> _BankAccountRepository;
            private readonly MultiTenancy.TenantManager _tenantManager;


        public VerifoneBreakAppService( 
            IRepository<Models.VerifoneBreaks, int> repository, 
            IRepository<Models.VerifoneBreaks, int> verifoneBreakRepository ,
            IRepository<Models.Banks, int> bankRepository,
            IRepository<Models.BankAccounts, int> BankAccountRepository,
            MultiTenancy.TenantManager tenantManager

            ) 
            : base(repository) 
        { 
            _verifoneBreakRepository = verifoneBreakRepository; 
			
            _bankRepository = bankRepository;

            _BankAccountRepository = BankAccountRepository;

            _tenantManager = tenantManager;



        } 
        public override async Task<VerifoneBreakDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<VerifoneBreakDto> Create(CreateVerifoneBreakDto input) 
        { 
            CheckCreatePermission(); 
          
            var verifoneBreak = ObjectMapper.Map<Models.VerifoneBreaks>(input);
            verifoneBreak.Sequence = getVerifoneBreakSequence();

            var tm = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();

            if (tm == null)
                throw new Exception("MasterTenantMustBeConfigured");

            decimal CardPercent = tm.CreditCardPercent;

            verifoneBreak.CreditCardPercent = CardPercent;


            try {
              await _verifoneBreakRepository.InsertAsync(verifoneBreak); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(verifoneBreak); 
        }


        public int getVerifoneBreakSequence()
        {
            int sequence = 1;
            try
            {
                sequence = _verifoneBreakRepository.GetAllList().Max(x => x.Sequence) + 1;
            }
            catch
            {
            }

            return sequence;
        }

        public override async Task<VerifoneBreakDto> Update(UpdateVerifoneBreakDto input) 
        { 
            CheckUpdatePermission(); 
            var verifoneBreak = await _verifoneBreakRepository.GetAsync(input.Id);
            MapToEntity(input, verifoneBreak); 
		    try{
               await _verifoneBreakRepository.UpdateAsync(verifoneBreak); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var verifoneBreak = await _verifoneBreakRepository.GetAsync(input.Id); 
               await _verifoneBreakRepository.DeleteAsync(verifoneBreak);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.VerifoneBreaks MapToEntity(CreateVerifoneBreakDto createInput) 
        { 
            var verifoneBreak = ObjectMapper.Map<Models.VerifoneBreaks>(createInput); 
            return verifoneBreak; 
        } 
        protected override void MapToEntity(UpdateVerifoneBreakDto input, Models.VerifoneBreaks verifoneBreak) 
        { 
            ObjectMapper.Map(input, verifoneBreak); 
        } 
        protected override IQueryable<Models.VerifoneBreaks> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.VerifoneBreaks> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Comments.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.VerifoneBreaks> GetEntityByIdAsync(int id) 
        { 
            var verifoneBreak = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(verifoneBreak); 
        } 
        protected override IQueryable<Models.VerifoneBreaks> ApplySorting(IQueryable<Models.VerifoneBreaks> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Comments); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<VerifoneBreakDto>> GetAllVerifoneBreaks(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<VerifoneBreakDto> ouput = new PagedResultDto<VerifoneBreakDto>(); 
            IQueryable<Models.VerifoneBreaks> query = query = from x in _verifoneBreakRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _verifoneBreakRepository.GetAll() 
                        where x.Comments.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<VerifoneBreaks.Dto.VerifoneBreakDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<VerifoneBreakDto>> GetAllVerifoneBreaksForCombo()
        {
            var verifoneBreakList = await _verifoneBreakRepository.GetAllListAsync(x => x.IsActive == true);

            var verifoneBreak = ObjectMapper.Map<List<VerifoneBreakDto>>(verifoneBreakList.ToList());

            return verifoneBreak;
        }
		
    } 
} ;
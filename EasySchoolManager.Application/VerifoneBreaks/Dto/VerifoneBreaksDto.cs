//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.VerifoneBreaks.Dto 
{
        [AutoMap(typeof(Models.VerifoneBreaks))] 
        public class VerifoneBreakDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public int Sequence {get;set;} 
              public int? BankId {get;set;} 
              public int? BankAccountId {get;set;} 
              public DateTime Date {get;set;} 
              public decimal Amount {get;set;} 
              public decimal CreditCardPercent {get;set;} 
              public string Comments {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.TransactionArticles.Dto
{
    [AutoMap(typeof(Models.TransactionArticles))]
    public class TransactionArticleDto
    {
        public long TransactionId { get; set; }

        public int ArticleId { get; set; }

        public decimal Amount { get; set; }

        public decimal Price { get; set; }

        public decimal Total { get; set; }
        
        public Articles.Dto.ArticleDto Articles { get; set; }
        
        public long Quantity { get; set; }
    }
}

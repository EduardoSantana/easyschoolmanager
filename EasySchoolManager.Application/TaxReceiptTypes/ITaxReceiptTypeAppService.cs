//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.TaxReceiptTypes.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.TaxReceiptTypes
{
      public interface ITaxReceiptTypeAppService : IAsyncCrudAppService<TaxReceiptTypeDto, int, PagedResultRequestDto, CreateTaxReceiptTypeDto, UpdateTaxReceiptTypeDto>
      {
            Task<PagedResultDto<TaxReceiptTypeDto>> GetAllTaxReceiptTypes(GdPagedResultRequestDto input);
			Task<List<Dto.TaxReceiptTypeDto>> GetAllTaxReceiptTypesForCombo();
      }
}
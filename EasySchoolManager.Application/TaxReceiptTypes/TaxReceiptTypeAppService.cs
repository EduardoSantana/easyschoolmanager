//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.TaxReceiptTypes.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.TaxReceiptTypes 
{ 
    [AbpAuthorize(PermissionNames.Pages_TaxReceiptTypes)] 
    public class TaxReceiptTypeAppService : AsyncCrudAppService<Models.TaxReceiptTypes, TaxReceiptTypeDto, int, PagedResultRequestDto, CreateTaxReceiptTypeDto, UpdateTaxReceiptTypeDto>, ITaxReceiptTypeAppService 
    { 
        private readonly IRepository<Models.TaxReceiptTypes, int> _taxReceiptTypeRepository;
		


        public TaxReceiptTypeAppService( 
            IRepository<Models.TaxReceiptTypes, int> repository, 
            IRepository<Models.TaxReceiptTypes, int> taxReceiptTypeRepository 
            ) 
            : base(repository) 
        { 
            _taxReceiptTypeRepository = taxReceiptTypeRepository; 
			

			
        } 
        public override async Task<TaxReceiptTypeDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<TaxReceiptTypeDto> Create(CreateTaxReceiptTypeDto input) 
        { 
            CheckCreatePermission(); 
            var taxReceiptType = ObjectMapper.Map<Models.TaxReceiptTypes>(input); 
			try{
              await _taxReceiptTypeRepository.InsertAsync(taxReceiptType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(taxReceiptType); 
        } 
        public override async Task<TaxReceiptTypeDto> Update(UpdateTaxReceiptTypeDto input) 
        { 
            CheckUpdatePermission(); 
            var taxReceiptType = await _taxReceiptTypeRepository.GetAsync(input.Id);
            MapToEntity(input, taxReceiptType); 
		    try{
               await _taxReceiptTypeRepository.UpdateAsync(taxReceiptType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var taxReceiptType = await _taxReceiptTypeRepository.GetAsync(input.Id); 
               await _taxReceiptTypeRepository.DeleteAsync(taxReceiptType);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.TaxReceiptTypes MapToEntity(CreateTaxReceiptTypeDto createInput) 
        { 
            var taxReceiptType = ObjectMapper.Map<Models.TaxReceiptTypes>(createInput); 
            return taxReceiptType; 
        } 
        protected override void MapToEntity(UpdateTaxReceiptTypeDto input, Models.TaxReceiptTypes taxReceiptType) 
        { 
            ObjectMapper.Map(input, taxReceiptType); 
        } 
        protected override IQueryable<Models.TaxReceiptTypes> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.TaxReceiptTypes> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.TaxReceiptTypes> GetEntityByIdAsync(int id) 
        { 
            var taxReceiptType = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(taxReceiptType); 
        } 
        protected override IQueryable<Models.TaxReceiptTypes> ApplySorting(IQueryable<Models.TaxReceiptTypes> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<TaxReceiptTypeDto>> GetAllTaxReceiptTypes(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<TaxReceiptTypeDto> ouput = new PagedResultDto<TaxReceiptTypeDto>(); 
            IQueryable<Models.TaxReceiptTypes> query = query = from x in _taxReceiptTypeRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _taxReceiptTypeRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<TaxReceiptTypes.Dto.TaxReceiptTypeDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<TaxReceiptTypeDto>> GetAllTaxReceiptTypesForCombo()
        {
            var taxReceiptTypeList = await _taxReceiptTypeRepository.GetAllListAsync(x => x.IsActive == true);

            var taxReceiptType = ObjectMapper.Map<List<TaxReceiptTypeDto>>(taxReceiptTypeList.ToList());

            return taxReceiptType;
        }
		
    } 
} ;
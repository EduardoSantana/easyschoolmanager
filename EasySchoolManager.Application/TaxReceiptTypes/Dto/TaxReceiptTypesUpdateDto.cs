//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.TaxReceiptTypes.Dto 
{
        [AutoMap(typeof(Models.TaxReceiptTypes))] 
        public class UpdateTaxReceiptTypeDto : EntityDto<int> 
        {

              [StringLength(50)]  
              public string Name {get;set;} 

              [StringLength(2)]  
              public string Code {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using EasySchoolManager.MultiTenancy;

namespace EasySchoolManager.Sessions.Dto
{
    [AutoMapFrom(typeof(Tenant))]
    public class TenantLoginInfoDto : EntityDto
    {
        public string TenancyName { get; set; }

        public string Name { get; set; }

        public string Abbreviation { get; set; }
    }
}
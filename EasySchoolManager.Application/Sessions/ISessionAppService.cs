﻿using System.Threading.Tasks;
using Abp.Application.Services;
using EasySchoolManager.Sessions.Dto;

namespace EasySchoolManager.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}

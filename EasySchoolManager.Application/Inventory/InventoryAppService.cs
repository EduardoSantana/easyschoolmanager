//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Inventory.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using Abp.Domain.Uow;

namespace EasySchoolManager.Inventory
{
    [AbpAuthorize(PermissionNames.Pages_Inventory)]
    public class InventoryAppService : AsyncCrudAppService<Models.Inventory, InventoryDto, int, PagedResultRequestDto, CreateInventoryDto, UpdateInventoryDto>, IInventoryAppService
    {
        private readonly IRepository<Models.Inventory, int> _inventoryRepository;



        public InventoryAppService(
            IRepository<Models.Inventory, int> repository,
            IRepository<Models.Inventory, int> inventoryRepository
            )
            : base(repository)
        {
            _inventoryRepository = inventoryRepository;



        }
        public override async Task<InventoryDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<InventoryDto> Create(CreateInventoryDto input)
        {
            CheckCreatePermission();
            var inventory = ObjectMapper.Map<Models.Inventory>(input);
            try
            {
                await _inventoryRepository.InsertAsync(inventory);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(inventory);
        }
        public override async Task<InventoryDto> Update(UpdateInventoryDto input)
        {
            CheckUpdatePermission();
            var inventory = await _inventoryRepository.GetAsync(input.Id);
            MapToEntity(input, inventory);
            try
            {
                await _inventoryRepository.UpdateAsync(inventory);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var inventory = await _inventoryRepository.GetAsync(input.Id);
                await _inventoryRepository.DeleteAsync(inventory);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.Inventory MapToEntity(CreateInventoryDto createInput)
        {
            var inventory = ObjectMapper.Map<Models.Inventory>(createInput);
            return inventory;
        }
        protected override void MapToEntity(UpdateInventoryDto input, Models.Inventory inventory)
        {
            ObjectMapper.Map(input, inventory);
        }
        protected override IQueryable<Models.Inventory> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.Inventory> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Articles.Description.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.Inventory> GetEntityByIdAsync(int id)
        {
            var inventory = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(inventory);
        }
        protected override IQueryable<Models.Inventory> ApplySorting(IQueryable<Models.Inventory> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Articles.Description);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<InventoryDto>> GetAllInventory(GdPagedResultRequestDto input)
        {
            PagedResultDto<InventoryDto> ouput = new PagedResultDto<InventoryDto>();
            IQueryable<Models.Inventory> query = query = from x in _inventoryRepository.GetAll()
                                                         select x;

            if (AbpSession.TenantId.HasValue)
            {

                if (!string.IsNullOrEmpty(input.TextFilter))
                {
                    query = from x in _inventoryRepository.GetAll()
                            where x.Articles.Description.Contains(input.TextFilter)
                            select x;
                }
                ouput.TotalCount = await Task.Run(() => { return query.Count(); });
                if (input.SkipCount > 0)
                {
                    query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
                }
                if (input.MaxResultCount > 0)
                {
                    query = query.Take(input.MaxResultCount);
                }
                ouput.Items = ObjectMapper.Map<List<Inventory.Dto.InventoryDto>>(query.ToList());
                return ouput;
            }
            else
            {
                //Disabling MayHaveTenant filter, so we can reach to all users
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                {
                    if (!string.IsNullOrEmpty(input.TextFilter))
                    {
                        if (input.TenantId.HasValue)
                            query = from x in _inventoryRepository.GetAll()
                                    where x.Articles.Description.Contains(input.TextFilter) && x.TenantInventoryId == input.TenantId.Value
                                    select x;
                        else
                            query = from x in _inventoryRepository.GetAll()
                                    where x.Articles.Description.Contains(input.TextFilter) 
                                    select x;

                    }
                    else
                    {
                        if (input.TenantId.HasValue)
                            query = from x in _inventoryRepository.GetAll()
                                    where x.TenantInventoryId == input.TenantId.Value
                                    select x;
                        else
                            query = from x in _inventoryRepository.GetAll()
                                    select x;
                    }
                    ouput.TotalCount = await Task.Run(() => { return query.Count(); });
                    if (input.SkipCount > 0)
                    {
                        query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
                    }
                    if (input.MaxResultCount > 0)
                    {
                        query = query.Take(input.MaxResultCount);
                    }
                    ouput.Items = ObjectMapper.Map<List<Inventory.Dto.InventoryDto>>(query.ToList());
                    return ouput;
                }

            }
        }


        [AbpAllowAnonymous]
        public async Task<List<InventoryDto>> GetAllInventoryForCombo()
        {
            var inventoryList = await _inventoryRepository.GetAllListAsync(x => x.IsActive == true);

            var inventory = ObjectMapper.Map<List<InventoryDto>>(inventoryList.ToList());

            return inventory;
        }

    }
};
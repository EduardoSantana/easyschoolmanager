//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Inventory.Dto 
{
        [AutoMap(typeof(Models.Inventory))] 
        public class InventoryDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public int ArticleId {get;set;} 
              public long Existence {get;set;} 
              public int? TenantInventoryId {get;set;} 
              public Decimal Price {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 
              public string Articles_Description { get; set; }
              public string TenantInventories_Name { get; set; }

         }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Inventory.Dto 
{
        [AutoMap(typeof(Models.Inventory))] 
        public class UpdateInventoryDto : EntityDto<int> 
        {

              public int ArticleId {get;set;} 

              public long Existence {get;set;} 

              public int? TenantInventoryId {get;set;} 

              public Decimal Price {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
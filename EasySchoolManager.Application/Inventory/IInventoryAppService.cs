//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Inventory.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Inventory
{
      public interface IInventoryAppService : IAsyncCrudAppService<InventoryDto, int, PagedResultRequestDto, CreateInventoryDto, UpdateInventoryDto>
      {
            Task<PagedResultDto<InventoryDto>> GetAllInventory(GdPagedResultRequestDto input);
			Task<List<Dto.InventoryDto>> GetAllInventoryForCombo();
      }
}
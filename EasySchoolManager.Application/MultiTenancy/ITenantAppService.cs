﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.MultiTenancy.Dto;
using EasySchoolManager.Students.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EasySchoolManager.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
        List<TenantDto> GetAllTenantsForCombo();
        Task<Periods.Dto.PeriodDto> GetCurrentPeriod();
        Task<PagedResultDto<TenantDto>> GetAllTenants(GdPagedResultRequestDto input);
        Task ClosePeriod(ClosePeriodInput input);
    }
}

﻿namespace EasySchoolManager.MultiTenancy
{
    public class ClosePeriodInput
    {
        public int tenantId { get; set; }
        public int? CurrentPeriod { get; set; }
        public int? NextPeriodId { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.MultiTenancy;
using System;
using EasySchoolManager.PrinterTypes.Dto;
using System.Collections.Generic;
using EasySchoolManager.TeacherTenants.Dto;
using EasySchoolManager.ConceptTenantRegistrations.Dto;

namespace EasySchoolManager.MultiTenancy.Dto
{
    [AutoMapTo(typeof(Tenant)), AutoMapFrom(typeof(Tenant))]
    public class TenantDto : EntityDto
    {
        public TenantDto()
        {
            TeacherTenants = new HashSet<TeacherTenantsDto>();
        }
        [Required]
        [StringLength(AbpTenantBase.MaxTenancyNameLength)]
        [RegularExpression(Tenant.TenancyNameRegex)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(Tenant.MaxNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        [StringLength(15)]
        public string Phone1 { get; set; }

        [StringLength(15)]
        public string Phone2 { get; set; }

        [StringLength(15)]
        public string Phone3 { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        public int? CityId { get; set; }

        [StringLength(100)]
        public string DirectorName { get; set; }

        [StringLength(100)]
        public string RegisterName { get; set; }

        public int? PeriodId { get; set; }

        public int? PreviousPeriodId { get; set; }

        public int? NextPeriodId { get; set; }

        public int? DistrictId { get; set; }

        [StringLength(15)]
        public string AccreditationNumber { get; set; }

        [StringLength(15)]
        public string EducationalDistrict { get; set; }

        [StringLength(60)]
        public string Regional { get; set; }

        [StringLength(60)]
        public string RegionalDirector { get; set; }

        [StringLength(60)]
        public string DistritctDirector { get; set; }

        [StringLength(15)]
        public string Abbreviation { get; set; }

        public int Cities_ProvinceId { get; set; }

        public string Districts_Name { get; set; }

        public string Periods_Description { get; set; }

        public string NextPeriods_Description { get; set; }

        public int? PrinterTypeId { get; set; }

        //Es para ver el tipo de impresora que se estara utilizando en un colegio determinado.
        public virtual PrinterTypeDto PrinterTypes { get; set; }

        public bool ApplyAutomaticCharges { get; set; }

        public decimal CreditCardPercent { get; set; }
        public DateTime? FreeDateReceipt { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }


        public virtual ICollection<TeacherTenantsDto> TeacherTenants { get; set; }
        public virtual ICollection<ConceptTenantRegistrationDto> ConceptTenantRegistrations { get; set; }
    }
}

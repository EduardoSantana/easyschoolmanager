using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using System;
using Abp.MultiTenancy;
using System.Collections.Generic;
using EasySchoolManager.TeacherTenants.Dto;

namespace EasySchoolManager.MultiTenancy.Dto
{
    [AutoMapTo(typeof(Tenant))]
    public class EditTenantDto
    {
        public EditTenantDto()
        {
            TeacherTenants = new HashSet<TeacherTenantsDto>();
        }
        [Required]
        [StringLength(AbpTenantBase.MaxTenancyNameLength)]
        [RegularExpression(Tenant.TenancyNameRegex)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(Tenant.MaxNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        [StringLength(15)]
        public string Phone1 { get; set; }

        [StringLength(15)]
        public string Phone2 { get; set; }

        [StringLength(15)]
        public string Phone3 { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        public int? CityId { get; set; }

        [StringLength(100)]
        public string DirectorName { get; set; }

        [StringLength(100)]
        public string RegisterName { get; set; }

        public int? PeriodId { get; set; }

        public int? PreviousPeriodId { get; set; }

        public int? NextPeriodId { get; set; }

        public int? DistrictId { get; set; }

        [StringLength(15)]
        public string AccreditationNumber { get; set; }

        [StringLength(15)]
        public string EducationalDistrict { get; set; }

        [StringLength(60)]
        public string Regional { get; set; }

        [StringLength(60)]
        public string RegionalDirector { get; set; }

        [StringLength(60)]
        public string DistritctDirector { get; set; }

        [StringLength(15)]
        public string Abbreviation { get; set; }

        public bool ApplyAutomaticCharges { get; set; }

        public int? PrinterTypeId { get; set; }

        public string Comment { get; set; }

        public decimal CreditCardPercent { get; set; }
        public DateTime? FreeDateReceipt { get; set; }

        public virtual ICollection<TeacherTenantsDto> TeacherTenants { get; set; }

    }
}
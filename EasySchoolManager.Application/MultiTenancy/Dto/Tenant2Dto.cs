﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.MultiTenancy;
using EasySchoolManager.PrinterTypes.Dto;

namespace EasySchoolManager.MultiTenancy.Dto
{
    [AutoMapTo(typeof(Tenant)), AutoMapFrom(typeof(Tenant))]
    public class Tenant2Dto : EntityDto
    {
        [Required]
        [StringLength(Tenant.MaxNameLength)]
        public string Name { get; set; }

        public string TenancyName { get; set; }


        public int? PrinterTypeId { get; set; }

        //Es para ver el tipo de impresora que se estara utilizando en un colegio determinado.
        public virtual PrinterTypeDto PrinterTypes { get; set; }


        public bool IsActive { get; set; }

    }
}

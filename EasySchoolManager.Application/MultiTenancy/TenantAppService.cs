using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.MultiTenancy;
using EasySchoolManager.Authorization;
using EasySchoolManager.Authorization.Roles;
using EasySchoolManager.Authorization.Users;
using EasySchoolManager.Editions;
using EasySchoolManager.MultiTenancy.Dto;
using EasySchoolManager.Students.Dto;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using EasySchoolManager.GD;
using System;
using Abp.UI;
using EasySchoolManager.Helpers;

namespace EasySchoolManager.MultiTenancy
{
    [AbpAuthorize(PermissionNames.Pages_Tenants)]
    public class TenantAppService : AsyncCrudAppService<Tenant, TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>, ITenantAppService
    {
        private readonly TenantManager _tenantManager;
        private readonly EditionManager _editionManager;
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly IAbpZeroDbMigrator _abpZeroDbMigrator;
        private readonly IRepository<Models.EnrollmentStudents, int> _enrollmentStudentRepository;
        private readonly IRepository<Models.CourseEnrollmentStudents, int> _courseEnrollmentStudentRepository;
        private readonly IRepository<Models.TransactionStudents, int> _transactionStudentRepository;
        private readonly IRepository<Models.Courses, int> _coursesRepository;
        private readonly IRepository<Models.Transactions, long> _transactionRepository;
        private readonly IRepository<Models.Students, int> _studentRepository;

        public TenantAppService(
            IRepository<Tenant, int> repository,
            IRepository<Models.EnrollmentStudents, int> enrollmentStudentRepository,
            IRepository<Models.CourseEnrollmentStudents, int> courseEnrollmentStudentRepository,
            IRepository<Models.TransactionStudents, int> transactionStudentRepository,
            IRepository<Models.Students, int> studentRepository,
            IRepository<Models.Transactions, long> transactionRepository,
            IRepository<Models.Courses, int> coursesRepository,
            TenantManager tenantManager,
            EditionManager editionManager,
            UserManager userManager,
            RoleManager roleManager,
            IAbpZeroDbMigrator abpZeroDbMigrator
        ) : base(repository)
        {
            _tenantManager = tenantManager;
            _enrollmentStudentRepository = enrollmentStudentRepository;
            _courseEnrollmentStudentRepository = courseEnrollmentStudentRepository;
            _transactionStudentRepository = transactionStudentRepository;
            _studentRepository = studentRepository;
            _transactionRepository = transactionRepository;
            _editionManager = editionManager;
            _roleManager = roleManager;
            _abpZeroDbMigrator = abpZeroDbMigrator;
            _userManager = userManager;
            _coursesRepository = coursesRepository;
        }

        public async Task<Periods.Dto.PeriodDto> GetCurrentPeriod()
        {
            Periods.Dto.PeriodDto per = new Periods.Dto.PeriodDto();
            var currentTenant = await Task.Run(() => { return _tenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId); });
            if (currentTenant != null)
                per = ObjectMapper.Map<Periods.Dto.PeriodDto>(currentTenant.Periods);
            return per;
        }

        public async Task ClosePeriod(ClosePeriodInput input)
        {

            try
            {
                var error = "Tenant Id is not correct!";
                var selectedTenant = await _tenantManager.GetByIdAsync(input.tenantId);

                if (selectedTenant == null)
                {
                    Logger.Error(error);
                    throw new UserFriendlyException(error);
                }

                input.NextPeriodId = selectedTenant.NextPeriodId;
                input.CurrentPeriod = selectedTenant.PeriodId;

                if (!(input.NextPeriodId > 0 && input.CurrentPeriod > 0) || !selectedTenant.ConceptTenantRegistrations.Any())
                {
                    error = "Next Period, Current Period or Registration Concept are null. Please Correct it, before continue.";
                    Logger.Error(error);
                    throw new UserFriendlyException(error);
                }

                var tempLista = selectedTenant.ConceptTenantRegistrations.Select(f => f.ConceptId).ToList();

                var query = _transactionRepository.GetAll()
                    .Where(f => tempLista.Contains(f.ConceptId.Value) && f.TenantId == input.tenantId);

                foreach (var transaction in query)
                {
                    foreach (var students in transaction.TransactionStudents)
                    {
                        if (students.Amount > 0)
                        {
                            if (students.EnrollmentStudents == null)
                            {
                                continue;
                            }
                            var courseObj = students.EnrollmentStudents.CourseEnrollmentStudents.FirstOrDefault().Courses;
                            int courseid = 0;
                            try
                            {
                                courseid = _coursesRepository
                                .GetAllList(f => f.Sequence > courseObj.Sequence)
                                .OrderBy(a => a.Sequence)
                                .FirstOrDefault()
                                .Id;
                            }
                            catch (Exception ex)
                            {
                                courseid = courseObj.Id;
                            }

                            var exist = _enrollmentStudentRepository.GetAllList(f =>
                                f.EnrollmentId == students.EnrollmentStudents.EnrollmentId &&
                                f.StudentId == students.EnrollmentStudents.StudentId &&
                                f.PeriodId == input.NextPeriodId.Value &&
                                f.TenantId == input.tenantId).Any();

                            if (!exist)
                            {
                                students.EnrollmentStudents.IsActive = false;
                                students.EnrollmentStudents.LastModifierUserId = AbpSession.UserId;
                                RegisterEnrollmentStudent(students.EnrollmentStudents, input.NextPeriodId.Value, courseid, input.tenantId);
                            }
                        }
                    }
                }

                //selectedTenant.RegistrationConceptId = null; TODO: poner aqui eliminar todos los que se elijieron...
                selectedTenant.NextPeriodId = null;
                selectedTenant.PreviousPeriodId = input.CurrentPeriod;
                selectedTenant.PeriodId = input.NextPeriodId;
                selectedTenant.ApplyAutomaticCharges = false;
                await CurrentUnitOfWork.SaveChangesAsync();

            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }

        }

        private void RegisterEnrollmentStudent(Models.EnrollmentStudents student, int periodId, int courseId, int tenantId)
        {
            var enrollmentStuden = new Models.EnrollmentStudents();
            enrollmentStuden.EnrollmentId = student.EnrollmentId;
            enrollmentStuden.StudentId = student.StudentId;
            enrollmentStuden.RelationshipId = student.RelationshipId;
            enrollmentStuden.PeriodId = periodId;
            enrollmentStuden.IsActive = true;
            enrollmentStuden.TenantId = tenantId;
            enrollmentStuden.CreatorUserId = AbpSession.UserId;
            var enrollmentStudenId = _enrollmentStudentRepository.InsertAndGetId(enrollmentStuden);

            var courseEnrollmentStudent = new Models.CourseEnrollmentStudents();
            courseEnrollmentStudent.CourseId = courseId;
            courseEnrollmentStudent.EnrollmentStudentId = enrollmentStudenId;
            courseEnrollmentStudent.IsActive = true;
            courseEnrollmentStudent.TenantId = tenantId;
            courseEnrollmentStudent.CreatorUserId = AbpSession.UserId;
            _courseEnrollmentStudentRepository.Insert(courseEnrollmentStudent);

        }

        public override async Task<TenantDto> Create(CreateTenantDto input)
        {
            CheckCreatePermission();

            //Create tenant

            Tenant tenant = new Tenant();

            tenant = ObjectMapper.Map<Tenant>(input);

            tenant.TenancyName = tenant.Name.Replace(" ", "").Replace(".", "").Replace("/", "").Replace(@"\", "").Replace("@", "");
           
            try
            {
                var defaultEdition = await _editionManager.FindByNameAsync(EditionManager.DefaultEditionName);
                if (defaultEdition != null)
                {
                    tenant.EditionId = defaultEdition.Id;
                }

                await _tenantManager.CreateAsync(tenant);
                CurrentUnitOfWork.SaveChanges(); //To get new tenant's id.

                //Create tenant database
                _abpZeroDbMigrator.CreateOrMigrateForTenant(tenant);

                var newRolId = 0;

                //////////////////////////////////
                //We are working entities of new tenant, so changing tenant filter
                using (CurrentUnitOfWork.SetTenantId(tenant.Id))
                {
                    //Create static roles for new tenant
                    CheckErrors(await _roleManager.CreateStaticRoles(tenant.Id));

                    await CurrentUnitOfWork.SaveChangesAsync(); //To get static role ids

                    //grant all permissions to admin role
                    var adminRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Admin);
                    CurrentUnitOfWork.SaveChanges();
                    await _roleManager.GrantAllPermissionsAsync(adminRole);

                    CurrentUnitOfWork.SaveChanges();
                    newRolId = adminRole.Id;

                    adminRole.ParentRolId = 1;
                    CurrentUnitOfWork.SaveChanges();

                    //Create admin user for the tenant
                    var adminUser = User.CreateTenantAdminUser(tenant.Id, tenant.TenancyName+ "@gmail.com",
                        "123qwe","admin"+tenant.Id.ToString(), "of system" + tenant.Id.ToString(), "admin" + tenant.Id.ToString());
                    CheckErrors(await _userManager.CreateAsync(adminUser));
                    await CurrentUnitOfWork.SaveChangesAsync(); //To get admin user's id

                    //Assign admin user to role!
                    CheckErrors(await _userManager.AddToRoleAsync(adminUser.Id, adminRole.Name));
                    await CurrentUnitOfWork.SaveChangesAsync();
                    
                    CurrentUnitOfWork.SaveChanges();
                }
                //////////////////////////////////
                


                List<Role> AllRoles = null;
                List<RolesPermsCopy.Item> AllPermisions = null;
                AllRoles = _roleManager.Roles.Where(f => f.AffectTenants == true).ToList();
                AllPermisions = new List<RolesPermsCopy.Item>();

                foreach (Role item in AllRoles)
                {
                    var perms = await _roleManager.GetGrantedPermissionsAsync(item.Id);
                    AllPermisions.Add(new RolesPermsCopy.Item { RolId = item.Id, Perms = perms.ToList() });
                }

                //We are working entities of new tenant, so changing tenant filter
                using (CurrentUnitOfWork.SetTenantId(tenant.Id))
                {
                    foreach (Role item in AllRoles)
                    {
                        var rol = _roleManager.Roles.FirstOrDefault(r => r.ParentRolId == item.Id && r.TenantId == tenant.Id);

                        if (rol == null)
                        {
                            rol = new Role()
                            {
                                Name = item.Name,
                                Description = item.Description,
                                DisplayName = item.DisplayName,
                                IsStatic = item.IsStatic,
                                IsDefault = item.IsDefault,
                                IsDeleted = item.IsDeleted,
                                CreationTime = DateTime.Now,
                                TenantId = tenant.Id,
                                ParentRolId = item.Id
                            };
                            _roleManager.Create(rol);

                            CurrentUnitOfWork.SaveChanges();
                        }

                    }
                    //To get static role ids

                    var rolesex = _roleManager.Roles.Where(r => r.TenantId == tenant.Id).ToList();
                    foreach (Role itemX in rolesex)
                    {
                        await _roleManager.ResetAllPermissionsAsync(itemX);
                        var ROLX = AllPermisions.FirstOrDefault(f => f.RolId == itemX.ParentRolId);
                        if (ROLX != null)
                        {
                            var perms = ROLX.Perms;
                            await _roleManager.SetGrantedPermissionsAsync(itemX, perms);
                        }
                    }

                    var roleAdminis = _roleManager.Roles.Where(x => x.Id == newRolId).FirstOrDefault();
                    var rolex = AllPermisions.FirstOrDefault(x => x.RolId == 1);
                    if(rolex != null)
                    await _roleManager.SetGrantedPermissionsAsync(newRolId, rolex.Perms);

                    await CurrentUnitOfWork.SaveChangesAsync(); //To get admin user's id

                }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }

            return MapToEntityDto(tenant);
        }

        protected override void MapToEntity(TenantDto updateInput, Tenant entity)
        {
            //Manually mapped since TenantDto contains non-editable properties too.
            entity.Name = updateInput.Name;
            entity.TenancyName = updateInput.TenancyName;
            entity.IsActive = updateInput.IsActive;
            entity.CityId = updateInput.CityId;
            entity.Abbreviation = updateInput.Abbreviation;
            entity.Phone1 = updateInput.Phone1;
            entity.Phone2 = updateInput.Phone2;
            entity.Phone3 = updateInput.Phone3;
            entity.Address = updateInput.Address;
            entity.PeriodId = updateInput.PeriodId;
            entity.DirectorName = updateInput.DirectorName;
            entity.RegisterName = updateInput.RegisterName;
            entity.AccreditationNumber = updateInput.AccreditationNumber;
            entity.EducationalDistrict = updateInput.EducationalDistrict;
            entity.Regional = updateInput.Regional;
            entity.RegionalDirector = updateInput.RegionalDirector;
            entity.DistritctDirector = updateInput.DistritctDirector;
            entity.DistrictId = updateInput.DistrictId;
            entity.PreviousPeriodId = updateInput.PreviousPeriodId;
            entity.NextPeriodId = updateInput.NextPeriodId;
            entity.ApplyAutomaticCharges = updateInput.ApplyAutomaticCharges;
            entity.PrinterTypeId = updateInput.PrinterTypeId;
            entity.Comment = updateInput.Comment;
            entity.CreditCardPercent = updateInput.CreditCardPercent;
            entity.Email = updateInput.Email;
            entity.FreeDateReceipt = updateInput.FreeDateReceipt;
            using (CurrentUnitOfWork.DisableFilter(Abp.Domain.Uow.AbpDataFilters.SoftDelete))
            {
                foreach (var item in updateInput.TeacherTenants)
                {
                    var entityItem = entity.TeacherTenants.FirstOrDefault(f => f.TeacherId == item.TeacherId && f.TenantId == entity.Id);
                    if (entityItem != null)
                    {
                        if (!entityItem.IsActive || entityItem.IsDeleted)
                        {
                            entityItem.IsActive = true;
                            entityItem.IsDeleted = false;
                            entityItem.LastModifierUserId = AbpSession.UserId;
                            entityItem.LastModificationTime = DateTime.Now;
                        }
                    }
                    else
                    {
                        entity.TeacherTenants.Add(new Models.TeacherTenants
                        {
                            CreatorUserId = AbpSession.UserId,
                            TeacherId = item.TeacherId,
                            TenantId = entity.Id,
                            IsActive = true
                        });
                    }
                }
                foreach (var item in entity.TeacherTenants)
                {
                    if (!updateInput.TeacherTenants.Any(f => f.TeacherId == item.TeacherId && f.TenantId == entity.Id))
                    {
                        item.IsDeleted = true;
                        item.DeletionTime = DateTime.Now;
                        item.DeleterUserId = AbpSession.UserId;
                    }
                }
            }


            using (CurrentUnitOfWork.DisableFilter(Abp.Domain.Uow.AbpDataFilters.SoftDelete))
            {
                foreach (var item in updateInput.ConceptTenantRegistrations)
                {
                    var entityItem = entity.ConceptTenantRegistrations.FirstOrDefault(f => f.ConceptId == item.ConceptId && f.TenantId == entity.Id);
                    if (entityItem != null)
                    {
                        if (!entityItem.IsActive || entityItem.IsDeleted)
                        {
                            entityItem.IsActive = true;
                            entityItem.IsDeleted = false;
                            entityItem.LastModifierUserId = AbpSession.UserId;
                            entityItem.LastModificationTime = DateTime.Now;
                        }
                    }
                    else
                    {
                        entity.ConceptTenantRegistrations.Add(new Models.ConceptTenantRegistrations
                        {
                            CreatorUserId = AbpSession.UserId,
                            ConceptId = item.ConceptId,
                            TenantId = entity.Id,
                            IsActive = true
                        });
                    }
                }
                foreach (var item in entity.ConceptTenantRegistrations)
                {
                    if (!updateInput.ConceptTenantRegistrations.Any(f => f.ConceptId == item.ConceptId && f.TenantId == entity.Id))
                    {
                        item.IsDeleted = true;
                        item.DeletionTime = DateTime.Now;
                        item.DeleterUserId = AbpSession.UserId;
                    }
                }
            }

        }

        public override async Task Delete(EntityDto<int> input)
        {
            CheckDeletePermission();

            var tenant = await _tenantManager.GetByIdAsync(input.Id);
            await _tenantManager.DeleteAsync(tenant);
        }

        [AbpAllowAnonymous]
        public List<TenantDto> GetAllTenantsForCombo()
        {
            var list = _tenantManager.Tenants.Where(f => f.IsActive);
            if (AbpSession.TenantId.HasValue)
                list = list.Where(f => f.Id == AbpSession.TenantId.Value);

            var course = ObjectMapper.Map<List<TenantDto>>(list.ToList());
            return course;
        }

        private void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<TenantDto>> GetAllTenants(GdPagedResultRequestDto input)
        {
            PagedResultDto<TenantDto> ouput = new PagedResultDto<TenantDto>();
            IQueryable<MultiTenancy.Tenant> query = query = from x in _tenantManager.Tenants
                                                            select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _tenantManager.Tenants
                        where x.Name.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<TenantDto>>(query.ToList());
            return ouput;
        }
    }

}
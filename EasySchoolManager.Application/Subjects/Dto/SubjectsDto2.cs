//Created from Templaste MG

using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using EasySchoolManager.Teachers.Dto;
using EasySchoolManager.Courses.Dto;
using EasySchoolManager.IndicatorGroups.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Subjects.Dto
{
    [AutoMap(typeof(Models.Subjects))]
    public class SubjectDto2 : EntityDto<int>
    {
        public string Name { get; set; }
        public string LongName { get; set; }
        public string Code { get; set; }
        public int CourseId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
    }
}
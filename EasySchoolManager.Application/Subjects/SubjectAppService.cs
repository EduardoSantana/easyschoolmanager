//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Subjects.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Subjects 
{ 
	[AbpAuthorize(PermissionNames.Pages_Subjects)] 
	public class SubjectAppService : AsyncCrudAppService<Models.Subjects, SubjectDto, int, PagedResultRequestDto, CreateSubjectDto, UpdateSubjectDto>, ISubjectAppService 
	{ 
		private readonly IRepository<Models.Subjects, int> _subjectRepository;
		
			private readonly IRepository<Models.Courses, int> _courseRepository;
			private readonly IRepository<Models.ScoreTypes, int> _scoreTypeRepository;
			private readonly IRepository<Models.Teachers, int> _teacherRepository;


		public SubjectAppService( 
			IRepository<Models.Subjects, int> repository, 
			IRepository<Models.Subjects, int> subjectRepository ,
			IRepository<Models.Courses, int> courseRepository
,
			IRepository<Models.ScoreTypes, int> scoreTypeRepository
,
			IRepository<Models.Teachers, int> teacherRepository

			) 
			: base(repository) 
		{ 
			_subjectRepository = subjectRepository; 
			
			_courseRepository = courseRepository;

			_scoreTypeRepository = scoreTypeRepository;

			_teacherRepository = teacherRepository;


			
		} 
		public override async Task<SubjectDto> Get(EntityDto<int> input) 
		{ 
			var user = await base.Get(input); 
			return user; 
		} 
		public override async Task<SubjectDto> Create(CreateSubjectDto input) 
		{ 
			CheckCreatePermission(); 
			var subject = ObjectMapper.Map<Models.Subjects>(input); 
			try{
			  await _subjectRepository.InsertAsync(subject); 
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}
			return MapToEntityDto(subject); 
		} 
		public override async Task<SubjectDto> Update(UpdateSubjectDto input) 
		{ 
			CheckUpdatePermission(); 
			var subject = await _subjectRepository.GetAsync(input.Id);
			MapToEntity(input, subject); 
			try{
			   await _subjectRepository.UpdateAsync(subject); 
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}
			return await Get(input); 
		} 
		public override async Task Delete(EntityDto<int> input) 
		{
			try{
			   var subject = await _subjectRepository.GetAsync(input.Id); 
			   await _subjectRepository.DeleteAsync(subject);
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}			
		} 
		protected override Models.Subjects MapToEntity(CreateSubjectDto createInput) 
		{ 
			var subject = ObjectMapper.Map<Models.Subjects>(createInput); 
			return subject; 
		} 
		protected override void MapToEntity(UpdateSubjectDto input, Models.Subjects subject) 
		{ 
			ObjectMapper.Map(input, subject); 
		} 
		protected override IQueryable<Models.Subjects> CreateFilteredQuery(PagedResultRequestDto input) 
		{ 
			return Repository.GetAll(); 
		} 
		protected  IQueryable<Models.Subjects> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
		{ 
			var resultado =  Repository.GetAll(); 
			if (!string.IsNullOrEmpty(input.TextFilter)) 
				resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
			return resultado; 
		} 
		protected override async Task<Models.Subjects> GetEntityByIdAsync(int id) 
		{ 
			var subject = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
			return await Task.FromResult(subject); 
		} 
		protected override IQueryable<Models.Subjects> ApplySorting(IQueryable<Models.Subjects> query, PagedResultRequestDto input) 
		{ 
			return query.OrderBy(r => r.Name); 
		} 
 
		protected virtual void CheckErrors(IdentityResult identityResult) 
		{ 
			identityResult.CheckErrors(LocalizationManager); 
		} 
		
		public async Task<PagedResultDto<SubjectDto>> GetAllSubjects(GdPagedResultRequestDto input) 
		{ 
			PagedResultDto<SubjectDto> ouput = new PagedResultDto<SubjectDto>(); 
			IQueryable<Models.Subjects> query = query = from x in _subjectRepository.GetAll() 
													   select x; 
			if (!string.IsNullOrEmpty(input.TextFilter)) 
			{ 
				query = from x in _subjectRepository.GetAll() 
						where x.Name.Contains(input.TextFilter) 
						select x; 
			} 
			ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
			if (input.SkipCount > 0) 
			{ 
				query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
			}  
			if (input.MaxResultCount > 0) 
			{ 
				query = query.Take(input.MaxResultCount); 
			} 
			ouput.Items = ObjectMapper.Map<List<Subjects.Dto.SubjectDto>>(query.ToList()); 
			return ouput; 
		} 
		
		[AbpAllowAnonymous]
		public async Task<List<SubjectDto2>> GetAllSubjectsForCombo()
		{
			var subjectList = await _subjectRepository.GetAllListAsync(x => x.IsActive == true);

			var subject = ObjectMapper.Map<List<SubjectDto2>>(subjectList.ToList());

			return subject;
		}

		[AbpAllowAnonymous]
		public async Task<List<SubjectDto2>> GetAllSubjectsByCourseForCombo(int courseId)
		{
			var subjectList = await _subjectRepository.GetAllListAsync(x => x.IsActive == true && x.CourseId == courseId);

			var subject = ObjectMapper.Map<List<SubjectDto2>>(subjectList.ToList());

			return subject;
		}


	} 
} ;
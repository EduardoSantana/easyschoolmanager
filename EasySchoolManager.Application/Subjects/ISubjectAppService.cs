//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Subjects.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Subjects
{
    public interface ISubjectAppService : IAsyncCrudAppService<SubjectDto, int, PagedResultRequestDto, CreateSubjectDto, UpdateSubjectDto>
    {
        Task<PagedResultDto<SubjectDto>> GetAllSubjects(GdPagedResultRequestDto input);
        Task<List<Dto.SubjectDto2>> GetAllSubjectsForCombo();
        Task<List<SubjectDto2>> GetAllSubjectsByCourseForCombo(int courseId);
    }
}
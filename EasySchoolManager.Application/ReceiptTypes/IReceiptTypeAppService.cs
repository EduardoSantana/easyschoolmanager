//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.ReceiptTypes.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.ReceiptTypes
{
      public interface IReceiptTypeAppService : IAsyncCrudAppService<ReceiptTypeDto, int, PagedResultRequestDto, CreateReceiptTypeDto, UpdateReceiptTypeDto>
      {
            Task<PagedResultDto<ReceiptTypeDto>> GetAllReceiptTypes(GdPagedResultRequestDto input);
			Task<List<Dto.ReceiptTypeDto>> GetAllReceiptTypesForCombo();
      }
}
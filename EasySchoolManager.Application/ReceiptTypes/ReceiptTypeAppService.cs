//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.ReceiptTypes.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ReceiptTypes 
{ 
    [AbpAuthorize(PermissionNames.Pages_ReceiptTypes)] 
    public class ReceiptTypeAppService : AsyncCrudAppService<Models.ReceiptTypes, ReceiptTypeDto, int, PagedResultRequestDto, CreateReceiptTypeDto, UpdateReceiptTypeDto>, IReceiptTypeAppService 
    { 
        private readonly IRepository<Models.ReceiptTypes, int> _receiptTypeRepository;
		


        public ReceiptTypeAppService( 
            IRepository<Models.ReceiptTypes, int> repository, 
            IRepository<Models.ReceiptTypes, int> receiptTypeRepository 
            ) 
            : base(repository) 
        { 
            _receiptTypeRepository = receiptTypeRepository; 
			

			
        } 
        public override async Task<ReceiptTypeDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ReceiptTypeDto> Create(CreateReceiptTypeDto input) 
        { 
            CheckCreatePermission(); 
            var receiptType = ObjectMapper.Map<Models.ReceiptTypes>(input); 
			try{
              await _receiptTypeRepository.InsertAsync(receiptType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(receiptType); 
        } 
        public override async Task<ReceiptTypeDto> Update(UpdateReceiptTypeDto input) 
        { 
            CheckUpdatePermission(); 
            var receiptType = await _receiptTypeRepository.GetAsync(input.Id);
            MapToEntity(input, receiptType); 
		    try{
               await _receiptTypeRepository.UpdateAsync(receiptType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var receiptType = await _receiptTypeRepository.GetAsync(input.Id); 
               await _receiptTypeRepository.DeleteAsync(receiptType);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.ReceiptTypes MapToEntity(CreateReceiptTypeDto createInput) 
        { 
            var receiptType = ObjectMapper.Map<Models.ReceiptTypes>(createInput); 
            return receiptType; 
        } 
        protected override void MapToEntity(UpdateReceiptTypeDto input, Models.ReceiptTypes receiptType) 
        { 
            ObjectMapper.Map(input, receiptType); 
        } 
        protected override IQueryable<Models.ReceiptTypes> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ReceiptTypes> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ReceiptTypes> GetEntityByIdAsync(int id) 
        { 
            var receiptType = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(receiptType); 
        } 
        protected override IQueryable<Models.ReceiptTypes> ApplySorting(IQueryable<Models.ReceiptTypes> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ReceiptTypeDto>> GetAllReceiptTypes(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ReceiptTypeDto> ouput = new PagedResultDto<ReceiptTypeDto>(); 
            IQueryable<Models.ReceiptTypes> query = query = from x in _receiptTypeRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _receiptTypeRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ReceiptTypes.Dto.ReceiptTypeDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<ReceiptTypeDto>> GetAllReceiptTypesForCombo()
        {
            var receiptTypeList = await _receiptTypeRepository.GetAllListAsync(x => x.IsActive == true);

            var receiptType = ObjectMapper.Map<List<ReceiptTypeDto>>(receiptTypeList.ToList());

            return receiptType;
        }
		
    } 
} ;
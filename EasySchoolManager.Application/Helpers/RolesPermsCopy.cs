﻿using Abp.Authorization;
using EasySchoolManager.Authorization.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using EasySchoolManager.MultiTenancy;

namespace EasySchoolManager.Helpers
{
    public class RolesPermsCopy
    {
        public class Item
        {
            public int RolId { get; set; }
            public List<Permission> Perms { get; set; }
        }
        public RoleManager _roleManager { get; set; }
        public TenantManager _tenantManager { get; set; }
        public List<Role> AllRoles { get; set; }
        public List<RolesPermsCopy.Item> AllPermisions { get; set; }

        public void Set(RoleManager roleManager)
        {
            _roleManager = roleManager;
            AllRoles = _roleManager.Roles.Where(f => f.AffectTenants == true).ToList();
            AllPermisions = new List<RolesPermsCopy.Item>();

            foreach (Role item in AllRoles)
            {
                var perms = AsyncHelpers.RunSync<IReadOnlyList<Permission>>(() => _roleManager.GetGrantedPermissionsAsync(item.Id));
                AllPermisions.Add(new RolesPermsCopy.Item { RolId = item.Id, Perms = perms.ToList() });
            }
        }

        public void CreateRoles(RoleManager roleManager = null, int tenantId = 0)
        {
            if (roleManager != null)
            {
                _roleManager = roleManager;
            }
            foreach (Role item in AllRoles)
            {
                if (tenantId > 1)
                {
                    var rol = _roleManager.Roles.FirstOrDefault(r => r.Name == item.Name);
                    if (tenantId > 0)
                    {
                        rol = _roleManager.Roles.FirstOrDefault(r => r.Name == item.Name && r.TenantId == tenantId);
                    }
                    if (rol == null)
                    {
                        rol = new Role()
                        {
                            Name = item.Name,
                            Description = item.Description,
                            DisplayName = item.DisplayName,
                            IsStatic = item.IsStatic,
                            IsDefault = item.IsDefault,
                            IsDeleted = item.IsDeleted,
                            CreationTime = DateTime.Now,
                            TenantId = tenantId
                        };
                        _roleManager.Create(rol);
                        
                    }
                }
                
            }
        }

        public void CreateRoles(TenantManager tenantManager)
        {
            _tenantManager = tenantManager;
            foreach (Tenant item in _tenantManager.Tenants)
            {
                CreateRoles(null,item.Id);
            }
        }

        public void AssignPerms(int tenantId)
        {
            foreach (Role item in _roleManager.Roles.Where(f => f.TenantId == tenantId))
            {
                AsyncHelpers.RunSync(() => _roleManager.ResetAllPermissionsAsync(item));
                AsyncHelpers.RunSync(() => _roleManager.SetGrantedPermissionsAsync(item, AllPermisions.FirstOrDefault(f => f.RolId == item.Id).Perms));
            }
        }

        public void AssignPerms(TenantManager tenantManager)
        {
            _tenantManager = tenantManager;
            foreach (Tenant tenant in _tenantManager.Tenants)
            {
                AssignPerms(tenant.Id);
            }
        }
    }
}

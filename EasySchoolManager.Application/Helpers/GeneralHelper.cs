﻿using Abp.Runtime.Session;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Helpers
{
    public class GeneralHelper
    {
        public static void ValidateUserBellongToASchool(IAbpSession abpSession)
        {
            if (abpSession.TenantId == null)
                throw new UserFriendlyException(("ToDoThisProcessTheUserMustBellongToASchool"));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Helpers
{
    public class EnrollmentAcumulator
    {
        public EnrollmentAcumulator()
        {
            PaymentProjectionIdList = new List<int>();
            Students = new List<TransactionStudentTmp>();
        }

        public long EnrollmentId { get; set; }
        public Decimal Amount { get; set; }
        public List<int> PaymentProjectionIdList { get; set; }
        public List<TransactionStudentTmp> Students { get; set; }
        public int Sequence { get;  set; }
        public DateTime PaymentDate { get;  set; }
        public string FullName { get;  set; }
        public int TenantId { get;  set; }
    }
}

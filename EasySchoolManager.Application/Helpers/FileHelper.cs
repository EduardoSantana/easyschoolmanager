﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Helpers
{
    public static class FileHelper
    {

        public static Image ConvertToImage(string Imagen)
        {
            string mimeType;
            byte[] _image = ConvertToBytes(Imagen, out mimeType);

            return new Bitmap(new MemoryStream(_image));
        }
        public static byte[] ConvertToBytes(string file, out string mimeType)
        {
            mimeType = null;
            if (string.IsNullOrEmpty(file)) return null;
            string[] imSplit = file.Split(',');
            mimeType = imSplit[0].Replace("data:", "").Replace(";base64", "");
            byte[] _image = Convert.FromBase64String(imSplit[1]);

            return _image;
        }

        public static string Redimensionar(string Imagen, int Ancho, int Alto)
        {
            if (string.IsNullOrEmpty(Imagen)) return null;
            string[] imSplit = Imagen.Split(',');
            byte[] _image = Convert.FromBase64String(imSplit[1]);
            _image = Redimensionar(_image, Ancho, Alto);

            return $"{imSplit[0]},{Convert.ToBase64String(_image)}";
        }

        public static System.Drawing.Image Redimensionar(System.Drawing.Image Imagen, int Ancho, int Alto, int resolucion)
        {
            //Bitmap sera donde trabajaremos los cambios
            var imagenBitmap = new Bitmap(Ancho, Alto, PixelFormat.Format32bppRgb);

            imagenBitmap.SetResolution(resolucion, resolucion);
            //Hacemos los cambios a ImagenBitmap usando a ImagenGraphics y la Imagen Original(Imagen)
            //ImagenBitmap se comporta como un objeto de referenciado
            var imagenGraphics = Graphics.FromImage(imagenBitmap);

            imagenGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            imagenGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            imagenGraphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            imagenGraphics.DrawImage(Imagen, new Rectangle(0, 0, Ancho, Alto), new Rectangle(0, 0, Imagen.Width, Imagen.Height), GraphicsUnit.Pixel);
            //todos los cambios hechos en imagenBitmap lo llevaremos un Image(Imagen) con nuevos datos a travez de un MemoryStream
            var imagenMemoryStream = new MemoryStream();
            imagenBitmap.Save(imagenMemoryStream, ImageFormat.Jpeg);
            Imagen = System.Drawing.Image.FromStream(imagenMemoryStream);

            return Imagen;
        }

        public static byte[] Redimensionar(byte[] image, int SizeHorizontalPercent = 50, int SizeVerticalPercent = 50)
        {
            //Obntenemos el ancho y el alto a partir del porcentaje de tamaño solicitado
            var ms = new MemoryStream(image);
            var img = new Bitmap(ms);

            var anchoDestino = (img.Width * (((decimal)SizeVerticalPercent / img.Height) * 100)) / 100;
            var altoDestino = (img.Height * (((decimal)SizeVerticalPercent / img.Height) * 100)) / 100;
            //Obtenemos la resolucion original 
            var resolucion = Convert.ToInt32(img.HorizontalResolution);

            var nuevaImagen = Redimensionar(img, (int)anchoDestino, (int)altoDestino, resolucion);
            byte[] imgbuffer;
            var fs = new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate, FileAccess.ReadWrite);
            nuevaImagen.Save(fs, ImageFormat.Jpeg);
            fs.Position = 0;

            int length = (int)fs.Length;
            imgbuffer = new byte[length];
            fs.Read(imgbuffer, 0, length);
            fs.Flush();
            fs.Close();

            return imgbuffer;
        }
    }
}


﻿using Abp.Dependency;
using Abp.Events.Bus.Exceptions;
using Abp.Events.Bus.Handlers;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasySchoolManager.Web.Helpers
{
    public class MyExceptionHandler : IEventHandler<AbpHandledExceptionData>, ITransientDependency
    {
        public void HandleEvent(AbpHandledExceptionData eventData)
        {


            string mens = eventData.Exception.Message;

            if (eventData.Exception.InnerException != null)
            {
                mens = ((System.Data.Entity.Validation.DbEntityValidationException)eventData.Exception.InnerException).EntityValidationErrors.First().ValidationErrors.First().ErrorMessage;
            }
            else
            {
                try
                {

                    mens = ((System.Data.Entity.Validation.DbEntityValidationException)eventData.Exception).EntityValidationErrors.First().ValidationErrors.First().ErrorMessage;
                }
                catch { }
            }

             //   if (!eventData.Exception.Message.ToUpper().Contains("NO HA INICIADO SESI"))
                //    throw new UserFriendlyException(mens);


        }
    }
}
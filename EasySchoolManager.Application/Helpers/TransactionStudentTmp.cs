﻿using System;

namespace EasySchoolManager.Helpers
{
    public class TransactionStudentTmp
    {
        public decimal Amount { get; set; }
        public int EnrollmentStudentId { get; set; }
        public DateTime PaymentDate { get; set; }
        public int Sequence { get; set; }
        public int TenantId { get; set; }
        public int PaymentProjectionId { get;  set; }
    }
}
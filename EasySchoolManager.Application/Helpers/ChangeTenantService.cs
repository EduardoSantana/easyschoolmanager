﻿using Abp.Runtime.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Helpers
{
    public class ChangeTenantService
    {
        private readonly IAbpSession _session;

        public ChangeTenantService(IAbpSession session)
        {
            _session = session;
        }

        public void Test(int TenantId)
        {
            var seesion = _session.Use(TenantId, null);
        }
    }
}

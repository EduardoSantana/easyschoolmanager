//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Brands.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Brands
{
      public interface IBrandAppService : IAsyncCrudAppService<BrandDto, int, PagedResultRequestDto, CreateBrandDto, UpdateBrandDto>
      {
            Task<PagedResultDto<BrandDto>> GetAllBrands(GdPagedResultRequestDto input);
			Task<List<Dto.BrandDto>> GetAllBrandsForCombo();
      }
}
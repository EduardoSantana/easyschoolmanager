//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Brands.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Brands 
{ 
    [AbpAuthorize(PermissionNames.Pages_Brands)] 
    public class BrandAppService : AsyncCrudAppService<Models.Brands, BrandDto, int, PagedResultRequestDto, CreateBrandDto, UpdateBrandDto>, IBrandAppService 
    { 
        private readonly IRepository<Models.Brands, int> _brandRepository;
		


        public BrandAppService( 
            IRepository<Models.Brands, int> repository, 
            IRepository<Models.Brands, int> brandRepository 
            ) 
            : base(repository) 
        { 
            _brandRepository = brandRepository; 
			

			
        } 
        public override async Task<BrandDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<BrandDto> Create(CreateBrandDto input) 
        { 
            CheckCreatePermission(); 
            var brand = ObjectMapper.Map<Models.Brands>(input); 
			try{
              await _brandRepository.InsertAsync(brand); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(brand); 
        } 
        public override async Task<BrandDto> Update(UpdateBrandDto input) 
        { 
            CheckUpdatePermission(); 
            var brand = await _brandRepository.GetAsync(input.Id);
            MapToEntity(input, brand); 
		    try{
               await _brandRepository.UpdateAsync(brand); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var brand = await _brandRepository.GetAsync(input.Id); 
               await _brandRepository.DeleteAsync(brand);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Brands MapToEntity(CreateBrandDto createInput) 
        { 
            var brand = ObjectMapper.Map<Models.Brands>(createInput); 
            return brand; 
        } 
        protected override void MapToEntity(UpdateBrandDto input, Models.Brands brand) 
        { 
            ObjectMapper.Map(input, brand); 
        } 
        protected override IQueryable<Models.Brands> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Brands> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Brands> GetEntityByIdAsync(int id) 
        { 
            var brand = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(brand); 
        } 
        protected override IQueryable<Models.Brands> ApplySorting(IQueryable<Models.Brands> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<BrandDto>> GetAllBrands(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<BrandDto> ouput = new PagedResultDto<BrandDto>(); 
            IQueryable<Models.Brands> query = query = from x in _brandRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _brandRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Brands.Dto.BrandDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<BrandDto>> GetAllBrandsForCombo()
        {
            var brandList = await _brandRepository.GetAllListAsync(x => x.IsActive == true);

            var brand = ObjectMapper.Map<List<BrandDto>>(brandList.ToList());

            return brand;
        }
		
    } 
} ;
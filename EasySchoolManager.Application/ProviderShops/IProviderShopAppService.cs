//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.ProviderShops.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.ProviderShops
{
      public interface IProviderShopAppService : IAsyncCrudAppService<ProviderShopDto, int, PagedResultRequestDto, CreateProviderShopDto, UpdateProviderShopDto>
      {
            Task<PagedResultDto<ProviderShopDto>> GetAllProviderShops(GdPagedResultRequestDto input);
			Task<List<Dto.ProviderShopDto>> GetAllProviderShopsForCombo();
      }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.ProviderShops.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ProviderShops 
{ 
    [AbpAuthorize(PermissionNames.Pages_ProviderShops)] 
    public class ProviderShopAppService : AsyncCrudAppService<Models.ProviderShops, ProviderShopDto, int, PagedResultRequestDto, CreateProviderShopDto, UpdateProviderShopDto>, IProviderShopAppService 
    { 
        private readonly IRepository<Models.ProviderShops, int> _providerShopRepository;
		


        public ProviderShopAppService( 
            IRepository<Models.ProviderShops, int> repository, 
            IRepository<Models.ProviderShops, int> providerShopRepository 
            ) 
            : base(repository) 
        { 
            _providerShopRepository = providerShopRepository; 
			

			
        } 
        public override async Task<ProviderShopDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ProviderShopDto> Create(CreateProviderShopDto input) 
        { 
            CheckCreatePermission(); 
            var providerShop = ObjectMapper.Map<Models.ProviderShops>(input);
            providerShop.Sequence = getProviderShopSequence();

            try
            {
              await _providerShopRepository.InsertAsync(providerShop); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(providerShop); 
        }

        public int getProviderShopSequence()
        {
            int sequence = 1;
            try
            {
                sequence = _providerShopRepository.GetAllList().Max(x => x.Sequence) + 1;
            }
            catch
            {
            }

            return sequence;
        }

        public override async Task<ProviderShopDto> Update(UpdateProviderShopDto input) 
        { 
            CheckUpdatePermission(); 
            var providerShop = await _providerShopRepository.GetAsync(input.Id);
            MapToEntity(input, providerShop); 
		    try{
               await _providerShopRepository.UpdateAsync(providerShop); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var providerShop = await _providerShopRepository.GetAsync(input.Id); 
               await _providerShopRepository.DeleteAsync(providerShop);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.ProviderShops MapToEntity(CreateProviderShopDto createInput) 
        { 
            var providerShop = ObjectMapper.Map<Models.ProviderShops>(createInput); 
            return providerShop; 
        } 
        protected override void MapToEntity(UpdateProviderShopDto input, Models.ProviderShops providerShop) 
        { 
            ObjectMapper.Map(input, providerShop); 
        } 
        protected override IQueryable<Models.ProviderShops> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ProviderShops> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ProviderShops> GetEntityByIdAsync(int id) 
        { 
            var providerShop = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(providerShop); 
        } 
        protected override IQueryable<Models.ProviderShops> ApplySorting(IQueryable<Models.ProviderShops> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ProviderShopDto>> GetAllProviderShops(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ProviderShopDto> ouput = new PagedResultDto<ProviderShopDto>(); 
            IQueryable<Models.ProviderShops> query = query = from x in _providerShopRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _providerShopRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ProviderShops.Dto.ProviderShopDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<ProviderShopDto>> GetAllProviderShopsForCombo()
        {
            var providerShopList = await _providerShopRepository.GetAllListAsync(x => x.IsActive == true);

            var providerShop = ObjectMapper.Map<List<ProviderShopDto>>(providerShopList.ToList());

            return providerShop;
        }
		
    } 
} ;
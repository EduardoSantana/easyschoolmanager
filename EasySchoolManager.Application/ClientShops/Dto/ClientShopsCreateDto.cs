//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.ClientShops.Dto 
{


           [AutoMap(typeof(Models.ClientShops))] 
           public class CreateClientShopDto : EntityDto<int> 
           {

              public int Sequence {get;set;} 

              [StringLength(200)] 
              public string Name {get;set;} 

              [StringLength(20)] 
              public string Reference {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
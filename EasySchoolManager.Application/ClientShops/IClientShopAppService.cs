//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.ClientShops.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.ClientShops
{
      public interface IClientShopAppService : IAsyncCrudAppService<ClientShopDto, int, PagedResultRequestDto, CreateClientShopDto, UpdateClientShopDto>
      {
            Task<PagedResultDto<ClientShopDto>> GetAllClientShops(GdPagedResultRequestDto input);
			Task<List<Dto.ClientShopDto>> GetAllClientShopsForCombo();
      }
}
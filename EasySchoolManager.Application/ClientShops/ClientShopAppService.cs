//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.ClientShops.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ClientShops 
{ 
    [AbpAuthorize(PermissionNames.Pages_ClientShops)] 
    public class ClientShopAppService : AsyncCrudAppService<Models.ClientShops, ClientShopDto, int, PagedResultRequestDto, CreateClientShopDto, UpdateClientShopDto>, IClientShopAppService 
    { 
        private readonly IRepository<Models.ClientShops, int> _clientShopRepository;
		


        public ClientShopAppService( 
            IRepository<Models.ClientShops, int> repository, 
            IRepository<Models.ClientShops, int> clientShopRepository 
            ) 
            : base(repository) 
        { 
            _clientShopRepository = clientShopRepository; 
			

			
        } 
        public override async Task<ClientShopDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ClientShopDto> Create(CreateClientShopDto input) 
        { 
            CheckCreatePermission(); 
            var clientShop = ObjectMapper.Map<Models.ClientShops>(input);
            clientShop.Sequence = getProviderShopSequence();
			try{
              await _clientShopRepository.InsertAsync(clientShop); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(clientShop); 
        }

        public int getProviderShopSequence()
        {
            int sequence = 1;
            try
            {
                sequence = _clientShopRepository.GetAllList().Max(x => x.Sequence) + 1;
            }
            catch
            {
            }

            return sequence;
        }

        public override async Task<ClientShopDto> Update(UpdateClientShopDto input) 
        { 
            CheckUpdatePermission(); 
            var clientShop = await _clientShopRepository.GetAsync(input.Id);
            MapToEntity(input, clientShop); 
		    try{
               await _clientShopRepository.UpdateAsync(clientShop); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var clientShop = await _clientShopRepository.GetAsync(input.Id); 
               await _clientShopRepository.DeleteAsync(clientShop);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.ClientShops MapToEntity(CreateClientShopDto createInput) 
        { 
            var clientShop = ObjectMapper.Map<Models.ClientShops>(createInput); 
            return clientShop; 
        } 
        protected override void MapToEntity(UpdateClientShopDto input, Models.ClientShops clientShop) 
        { 
            ObjectMapper.Map(input, clientShop); 
        } 
        protected override IQueryable<Models.ClientShops> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ClientShops> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ClientShops> GetEntityByIdAsync(int id) 
        { 
            var clientShop = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(clientShop); 
        } 
        protected override IQueryable<Models.ClientShops> ApplySorting(IQueryable<Models.ClientShops> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ClientShopDto>> GetAllClientShops(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ClientShopDto> ouput = new PagedResultDto<ClientShopDto>(); 
            IQueryable<Models.ClientShops> query = query = from x in _clientShopRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _clientShopRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ClientShops.Dto.ClientShopDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<ClientShopDto>> GetAllClientShopsForCombo()
        {
            var clientShopList = await _clientShopRepository.GetAllListAsync(x => x.IsActive == true);

            var clientShop = ObjectMapper.Map<List<ClientShopDto>>(clientShopList.ToList());

            return clientShop;
        }
		
    } 
} ;
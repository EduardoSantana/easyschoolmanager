//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.IndicatorGroups.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.IndicatorGroups
{
      public interface IIndicatorGroupAppService : IAsyncCrudAppService<IndicatorGroupDto, int, PagedResultRequestDto, CreateIndicatorGroupDto, UpdateIndicatorGroupDto>
      {
            Task<PagedResultDto<IndicatorGroupDto>> GetAllIndicatorGroups(GdPagedResultRequestDto input);
			Task<List<Dto.IndicatorGroupDto>> GetAllIndicatorGroupsForCombo();
			Task<List<Dto.IndicatorGroupDto>> GetAllIndicatorGroupsForComboWitoutChildrens();
      }
}
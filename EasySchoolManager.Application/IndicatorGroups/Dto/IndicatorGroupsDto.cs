//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.Subjects.Dto;
using System.Collections.Generic;
using EasySchoolManager.Indicators.Dto;

namespace EasySchoolManager.IndicatorGroups.Dto
{
    [AutoMap(typeof(Models.IndicatorGroups))]
    public class IndicatorGroupDto : EntityDto<int>
    {
        public IndicatorGroupDto()
        {
           // ChildIndicatorGrupos = new List<IndicatorGroupDto>();
            Indicators = new List<IndicatorDto>();
        }
        public string Name { get; set; }
        public int SubjectId { get; set; }
        public int? ParentIndicatorGroupId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string Subjects_Name { get; set; }

        //public SubjectDto Subjects { get; set; }
        public IndicatorGroupDto ParentIndicatorGroups { get; set; }
        
        public List<IndicatorDto> Indicators { get; set; }

        //public List<IndicatorGroupDto> ChildIndicatorGrupos { get; set; }

    }
}
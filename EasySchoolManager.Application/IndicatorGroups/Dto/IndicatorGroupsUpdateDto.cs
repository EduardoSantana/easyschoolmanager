//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.IndicatorGroups.Dto 
{
        [AutoMap(typeof(Models.IndicatorGroups))] 
        public class UpdateIndicatorGroupDto : EntityDto<int> 
        {

              [StringLength(200)]  
              public string Name {get;set;} 

              public int SubjectId {get;set;} 

              public int? ParentIndicatorGroupId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
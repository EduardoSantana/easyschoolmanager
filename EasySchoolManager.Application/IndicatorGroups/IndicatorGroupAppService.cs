//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.IndicatorGroups.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.IndicatorGroups 
{ 
    [AbpAuthorize(PermissionNames.Pages_IndicatorGroups)] 
    public class IndicatorGroupAppService : AsyncCrudAppService<Models.IndicatorGroups, IndicatorGroupDto, int, PagedResultRequestDto, CreateIndicatorGroupDto, UpdateIndicatorGroupDto>, IIndicatorGroupAppService 
    { 
        private readonly IRepository<Models.IndicatorGroups, int> _indicatorGroupRepository;
		
		    private readonly IRepository<Models.Subjects, int> _subjectRepository;


        public IndicatorGroupAppService( 
            IRepository<Models.IndicatorGroups, int> repository, 
            IRepository<Models.IndicatorGroups, int> indicatorGroupRepository ,
            IRepository<Models.Subjects, int> subjectRepository
            ) 
            : base(repository) 
        { 
            _indicatorGroupRepository = indicatorGroupRepository; 
			
            _subjectRepository = subjectRepository;
            


			
        } 
        public override async Task<IndicatorGroupDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<IndicatorGroupDto> Create(CreateIndicatorGroupDto input) 
        { 
            CheckCreatePermission(); 
            var indicatorGroup = ObjectMapper.Map<Models.IndicatorGroups>(input); 
			try{
              await _indicatorGroupRepository.InsertAsync(indicatorGroup); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(indicatorGroup); 
        } 
        public override async Task<IndicatorGroupDto> Update(UpdateIndicatorGroupDto input) 
        { 
            CheckUpdatePermission(); 
            var indicatorGroup = await _indicatorGroupRepository.GetAsync(input.Id);
            MapToEntity(input, indicatorGroup);

            if (indicatorGroup.Id == indicatorGroup.ParentIndicatorGroupId)
                throw new UserFriendlyException("TheIndicatorGrupAndHisParentIndicatorGroupMustBeDifferent");

		    try{
               await _indicatorGroupRepository.UpdateAsync(indicatorGroup); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var indicatorGroup = await _indicatorGroupRepository.GetAsync(input.Id); 
               await _indicatorGroupRepository.DeleteAsync(indicatorGroup);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.IndicatorGroups MapToEntity(CreateIndicatorGroupDto createInput) 
        { 
            var indicatorGroup = ObjectMapper.Map<Models.IndicatorGroups>(createInput); 
            return indicatorGroup; 
        } 
        protected override void MapToEntity(UpdateIndicatorGroupDto input, Models.IndicatorGroups indicatorGroup) 
        { 
            ObjectMapper.Map(input, indicatorGroup); 
        } 
        protected override IQueryable<Models.IndicatorGroups> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.IndicatorGroups> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.IndicatorGroups> GetEntityByIdAsync(int id) 
        { 
            var indicatorGroup = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(indicatorGroup); 
        } 
        protected override IQueryable<Models.IndicatorGroups> ApplySorting(IQueryable<Models.IndicatorGroups> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<IndicatorGroupDto>> GetAllIndicatorGroups(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<IndicatorGroupDto> ouput = new PagedResultDto<IndicatorGroupDto>(); 
            IQueryable<Models.IndicatorGroups> query = query = from x in _indicatorGroupRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _indicatorGroupRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<IndicatorGroups.Dto.IndicatorGroupDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<IndicatorGroupDto>> GetAllIndicatorGroupsForCombo()
        {
            var indicatorGroupList = await _indicatorGroupRepository.GetAllListAsync(x => x.IsActive == true);

            var indicatorGroup = ObjectMapper.Map<List<IndicatorGroupDto>>(indicatorGroupList.ToList());

            return indicatorGroup;
        }


        [AbpAllowAnonymous]
        public async Task<List<IndicatorGroupDto>> GetAllIndicatorGroupsForComboWitoutChildrens()
        {
            var indicatorGroupList = await _indicatorGroupRepository.GetAllListAsync(x => x.IsActive == true && x.ChildIndicatorGrupos.Count == 0);

            var indicatorGroup = ObjectMapper.Map<List<IndicatorGroupDto>>(indicatorGroupList.ToList());

            return indicatorGroup;
        }

    } 
} ;
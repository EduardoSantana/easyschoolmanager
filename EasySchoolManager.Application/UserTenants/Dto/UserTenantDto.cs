﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.UserTenants.Dto
{
    [AutoMap(typeof(Models.UserTenants))]
    public class UserTenantDto : EntityDto
    {
        public long UserId { get; set; }
        public int TenantId { get; set; }
        public string Tenant_Name { get; set; }
        public string Tenant_TenancyName { get; set; }
        public MultiTenancy.Dto.Tenant2Dto Tenant { get; set; }
    }
}

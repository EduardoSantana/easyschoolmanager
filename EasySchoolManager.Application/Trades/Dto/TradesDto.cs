//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Trades.Dto 
{
        [AutoMap(typeof(Models.Trades))] 
        public class TradeDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public string Comment {get;set;} 
              public string CommentSend {get;set;} 
              public string CommentReceive {get;set;} 
              public int CourrierPersonId {get;set;} 
              public DateTime Date {get;set;} 
              public DateTime DateSend {get;set;} 
              public DateTime? DateReceive {get;set;} 
              public int TenantReceiveId {get;set;} 
              public long? UserReceiveId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long CreatorUserId {get;set;} 
              public string CourrierPersons_Name {get;set;}
              public string TenantReceive_Name { get; set; }
               
    }
}
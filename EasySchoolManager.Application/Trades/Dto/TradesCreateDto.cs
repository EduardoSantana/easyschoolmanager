//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.Trades.Dto 
{
        [AutoMap(typeof(Models.Trades))] 
        public class CreateTradeDto : EntityDto<int> 
        {

              [StringLength(250)] 
              public string Comment {get;set;} 

              [StringLength(250)] 
              public string CommentSend {get;set;} 

              [StringLength(250)] 
              public string CommentReceive {get;set;} 

              public int CourrierPersonId {get;set;} 

              public DateTime Date {get;set;} 

              public DateTime DateSend {get;set;} 

              public DateTime? DateReceive {get;set;} 

              public int? TenantReceiveId {get;set;} 

              public long? UserReceiveId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long CreatorUserId {get;set;}
              public List<Articles.Dto.ArticleDto> Articles { get; set; }

    }
}
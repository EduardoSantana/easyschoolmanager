//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Trades.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.Articles.Dto;

namespace EasySchoolManager.Trades 
{ 
    [AbpAuthorize(PermissionNames.Pages_Trades)] 
    public class TradeAppService : AsyncCrudAppService<Models.Trades, TradeDto, int, PagedResultRequestDto, CreateTradeDto, UpdateTradeDto>, ITradeAppService 
    { 
        private readonly IRepository<Models.Trades, int> _tradeRepository;
		
		    private readonly IRepository<Models.CourrierPersons, int> _courrierPersonRepository;
            private readonly IRepository<Models.TradeArticles, int> _tradeArticles;
            private readonly IRepository<Models.Inventory, int> _InventoryRepository;



        public TradeAppService(
            IRepository<Models.Trades, int> repository,
            IRepository<Models.Trades, int> tradeRepository,
            IRepository<Models.CourrierPersons, int> courrierPersonRepository,
            IRepository<Models.TradeArticles, int> tradeArticles,
            IRepository<Models.Inventory, int> InventoryRepository


            ) 
            : base(repository) 
        { 
            _tradeRepository = tradeRepository; 
            _courrierPersonRepository = courrierPersonRepository;
            _tradeArticles = tradeArticles;
            _InventoryRepository = InventoryRepository;

        } 
        public override async Task<TradeDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<TradeDto> Create(CreateTradeDto input) 
        { 
            CheckCreatePermission(); 
            var trade = ObjectMapper.Map<Models.Trades>(input); 
			try{
              await _tradeRepository.InsertAsync(trade);
                if (input.Articles != null)
                    foreach (var art in input.Articles)
                    {
                        Models.TradeArticles ta = new Models.TradeArticles();
                        ta.ArticleId = art.Id;
                        ta.Quantity = art.Quantity.Value;
                        ta.Amount = art.Price;
                        ta.Total = art.Total.Value;
                        ta.TradeId = trade.Id;
                        ta.IsActive = true;
                        _tradeArticles.Insert(ta);

                        IncreaseInventory(art,input.TenantReceiveId.Value);
                        DecreaseInventory(art);

                    }

            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(trade); 
        } 
        public override async Task<TradeDto> Update(UpdateTradeDto input) 
        { 
            CheckUpdatePermission(); 
            var trade = await _tradeRepository.GetAsync(input.Id);
            MapToEntity(input, trade); 
		    try{
               await _tradeRepository.UpdateAsync(trade);
                if (input.Articles != null)
                    foreach (var art in input.Articles)
                    {
                        Models.TradeArticles ta = new Models.TradeArticles();
                        ta.ArticleId = art.Id;
                        ta.Quantity = art.Quantity.Value;
                        ta.Amount = art.Price;
                        ta.Total = art.Total.Value;
                        ta.TradeId = trade.Id;
                        ta.IsActive = true;
                        _tradeArticles.Insert(ta);

                        
                      //  IncreaseInventory(art,input.TenantReceiveId);
                      //  DecreaseInventory(art);

                    }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var trade = await _tradeRepository.GetAsync(input.Id); 
               await _tradeRepository.DeleteAsync(trade);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        }


        public void IncreaseInventory(ArticleDto art, int TenId)
        {

            var inventory = _InventoryRepository.
                 GetAllList(x => x.ArticleId == art.Id && x.TenantInventoryId==TenId).FirstOrDefault();
            if (inventory == null) {
                inventory = new Models.Inventory();
                inventory.ArticleId = art.Id;
                inventory.Existence = 0;
                inventory.IsActive = true;
                inventory.TenantInventoryId = TenId;
                _InventoryRepository.Insert(inventory);
                CurrentUnitOfWork.SaveChanges();
                //throw new UserFriendlyException("TheActualProductIsOutOfStock");
            }
            inventory.Existence += art.Quantity.Value;
            CurrentUnitOfWork.SaveChanges();

        }


        private void DecreaseInventory(ArticleDto art)
        {

            var inventory = _InventoryRepository.
                 GetAllList(x => x.ArticleId == art.Id && x.TenantInventoryId==null).FirstOrDefault();
            if (inventory == null)
                throw new UserFriendlyException("TheActualProductIsOutOfStock");

            if (inventory.Existence < art.Quantity.Value)
                throw new UserFriendlyException(L("TheExistenceIsLessThanTheRequiredQuantity"
                    , inventory.Existence, art.Quantity, inventory.Articles.Description));

            inventory.Existence -= art.Quantity.Value;
            CurrentUnitOfWork.SaveChanges();

        }
        protected override Models.Trades MapToEntity(CreateTradeDto createInput) 
        { 
            var trade = ObjectMapper.Map<Models.Trades>(createInput); 
            return trade; 
        } 
        protected override void MapToEntity(UpdateTradeDto input, Models.Trades trade) 
        { 
            ObjectMapper.Map(input, trade); 
        } 
        protected override IQueryable<Models.Trades> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Trades> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Comment.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Trades> GetEntityByIdAsync(int id) 
        { 
            var trade = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(trade); 
        } 
        protected override IQueryable<Models.Trades> ApplySorting(IQueryable<Models.Trades> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Comment); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<TradeDto>> GetAllTrades(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<TradeDto> ouput = new PagedResultDto<TradeDto>(); 
            IQueryable<Models.Trades> query = query = from x in _tradeRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _tradeRepository.GetAll() 
                        where x.Comment.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Trades.Dto.TradeDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<TradeDto>> GetAllTradesForCombo()
        {
            var tradeList = await _tradeRepository.GetAllListAsync(x => x.IsActive == true);

            var trade = ObjectMapper.Map<List<TradeDto>>(tradeList.ToList());

            return trade;
        }
		
    } 
} ;
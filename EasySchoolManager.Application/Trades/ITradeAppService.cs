//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Trades.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Trades
{
      public interface ITradeAppService : IAsyncCrudAppService<TradeDto, int, PagedResultRequestDto, CreateTradeDto, UpdateTradeDto>
      {
            Task<PagedResultDto<TradeDto>> GetAllTrades(GdPagedResultRequestDto input);
			Task<List<Dto.TradeDto>> GetAllTradesForCombo();
      }
}
//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Purchases.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.Models;
using EasySchoolManager.Articles.Dto;


namespace EasySchoolManager.Purchases 
{ 
    [AbpAuthorize(PermissionNames.Pages_Purchases)] 
    public class PurchaseAppService : AsyncCrudAppService<Models.Purchases, PurchaseDto, int, PagedResultRequestDto, CreatePurchaseDto, UpdatePurchaseDto>, IPurchaseAppService 
    { 
        private readonly IRepository<Models.Purchases, int> _purchaseRepository;
		
		    private readonly IRepository<Models.Providers, int> _providerRepository;
            private readonly IRepository<Models.PurchaseArticles, int> _purchaseArticles;
            private readonly IRepository<Models.Inventory, int> _InventoryRepository;

        public PurchaseAppService( 
            IRepository<Models.Purchases, int> repository, 
            IRepository<Models.Purchases, int> purchaseRepository ,
            IRepository<Models.Providers, int> providerRepository,
            IRepository<Models.PurchaseArticles, int> purchaseArticles,
            IRepository<Models.Inventory, int> InventoryRepository


            ) 
            : base(repository) 
        { 
            _purchaseRepository = purchaseRepository; 
            _providerRepository = providerRepository;
            _purchaseArticles = purchaseArticles;
            _InventoryRepository = InventoryRepository;


        } 
        public override async Task<PurchaseDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<PurchaseDto> Create(CreatePurchaseDto input) 
        { 
            CheckCreatePermission(); 
            var purchase = ObjectMapper.Map<Models.Purchases>(input); 
			try{
              await _purchaseRepository.InsertAsync(purchase);
                if (input.Articles != null)
                    foreach (var art in input.Articles)
                    {
                        Models.PurchaseArticles ta = new Models.PurchaseArticles();
                        ta.ArticleId = art.Id;
                        ta.Quantity = art.Quantity.Value;
                        ta.Amount = art.Price;
                        ta.Total = art.Total.Value;
                        ta.PurchaseId = purchase.Id;
                        ta.IsActive = true;
                        _purchaseArticles.Insert(ta);

                        IncreaseInventory(art);
                    }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(purchase); 
        } 
        public override async Task<PurchaseDto> Update(UpdatePurchaseDto input) 
        { 
            CheckUpdatePermission(); 
            var purchase = await _purchaseRepository.GetAsync(input.Id);
            MapToEntity(input, purchase); 
		    try{
               await _purchaseRepository.UpdateAsync(purchase);
 
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var purchase = await _purchaseRepository.GetAsync(input.Id); 
               await _purchaseRepository.DeleteAsync(purchase);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        }

        private void IncreaseInventory(ArticleDto art)
        {

            var inventory = _InventoryRepository.
                 GetAllList(x => x.ArticleId == art.Id && x.TenantInventoryId ==null).FirstOrDefault();
            if (inventory == null)
            {
                inventory = new Models.Inventory();
                inventory.ArticleId = art.Id;
                inventory.Existence = 0;
                inventory.IsActive = true;
                _InventoryRepository.Insert(inventory);
                CurrentUnitOfWork.SaveChanges();
            }

            inventory.Existence += art.Quantity.Value;
            CurrentUnitOfWork.SaveChanges();

        }

        protected override Models.Purchases MapToEntity(CreatePurchaseDto createInput) 
        { 
            var purchase = ObjectMapper.Map<Models.Purchases>(createInput); 
            return purchase; 
        } 
        protected override void MapToEntity(UpdatePurchaseDto input, Models.Purchases purchase) 
        { 
            ObjectMapper.Map(input, purchase); 
        } 
        protected override IQueryable<Models.Purchases> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Purchases> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Document.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Purchases> GetEntityByIdAsync(int id) 
        { 
            var purchase = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(purchase); 
        } 
        protected override IQueryable<Models.Purchases> ApplySorting(IQueryable<Models.Purchases> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Document); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<PurchaseDto>> GetAllPurchases(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<PurchaseDto> ouput = new PagedResultDto<PurchaseDto>(); 
            IQueryable<Models.Purchases> query = query = from x in _purchaseRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _purchaseRepository.GetAll() 
                        where x.Document.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Purchases.Dto.PurchaseDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<PurchaseDto>> GetAllPurchasesForCombo()
        {
            var purchaseList = await _purchaseRepository.GetAllListAsync(x => x.IsActive == true);

            var purchase = ObjectMapper.Map<List<PurchaseDto>>(purchaseList.ToList());

            return purchase;
        }
		
    } 
} ;
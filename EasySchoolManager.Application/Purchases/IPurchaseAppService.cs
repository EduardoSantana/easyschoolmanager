//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Purchases.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Purchases
{
      public interface IPurchaseAppService : IAsyncCrudAppService<PurchaseDto, int, PagedResultRequestDto, CreatePurchaseDto, UpdatePurchaseDto>
      {
            Task<PagedResultDto<PurchaseDto>> GetAllPurchases(GdPagedResultRequestDto input);
			Task<List<Dto.PurchaseDto>> GetAllPurchasesForCombo();
      }
}
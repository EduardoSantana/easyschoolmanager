//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Purchases.Dto 
{
        [AutoMap(typeof(Models.Purchases))] 
        public class PurchaseDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public string Document {get;set;}
              public decimal Amount { get; set; }
              public int ProviderId {get;set;} 
              public DateTime Date {get;set;} 
              public string Comnent1 {get;set;} 
              public string Comment2 {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 
              public string Providers_Name {get;set;}

    }
}
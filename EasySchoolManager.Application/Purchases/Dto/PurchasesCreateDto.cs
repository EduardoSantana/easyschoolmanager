//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.Purchases.Dto 
{
        [AutoMap(typeof(Models.Purchases))] 
        public class CreatePurchaseDto : EntityDto<int> 
        {

              [StringLength(25)] 
              public string Document {get;set;} 

              public int ProviderId {get;set;}
              public decimal Amount { get; set; }
              public DateTime Date {get;set;} 

              [StringLength(250)] 
              public string Comnent1 {get;set;} 

              [StringLength(250)] 
              public string Comment2 {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;}
              public List<Articles.Dto.ArticleDto> Articles { get; set; }

    }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.GrantEnterprises.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.GrantEnterprises 
{ 
    [AbpAuthorize(PermissionNames.Pages_GrantEnterprises)] 
    public class GrantEnterpriseAppService : AsyncCrudAppService<Models.GrantEnterprises, GrantEnterpriseDto, int, PagedResultRequestDto, CreateGrantEnterpriseDto, UpdateGrantEnterpriseDto>, IGrantEnterpriseAppService 
    { 
        private readonly IRepository<Models.GrantEnterprises, int> _grantEnterpriseRepository;
		


        public GrantEnterpriseAppService( 
            IRepository<Models.GrantEnterprises, int> repository, 
            IRepository<Models.GrantEnterprises, int> grantEnterpriseRepository 
            ) 
            : base(repository) 
        { 
            _grantEnterpriseRepository = grantEnterpriseRepository; 
			

			
        } 
        public override async Task<GrantEnterpriseDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<GrantEnterpriseDto> Create(CreateGrantEnterpriseDto input) 
        { 
            CheckCreatePermission(); 
            var grantEnterprise = ObjectMapper.Map<Models.GrantEnterprises>(input);
            grantEnterprise.Sequence = getGrantEnterpriseSequence();
			try{
              await _grantEnterpriseRepository.InsertAsync(grantEnterprise); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(grantEnterprise); 
        }

        public int getGrantEnterpriseSequence()
        {
            int sequence = 1;
            try
            {
                sequence = _grantEnterpriseRepository.GetAllList().Max(x => x.Sequence) + 1;
            }
            catch
            {
            }

            return sequence;
        }

        public override async Task<GrantEnterpriseDto> Update(UpdateGrantEnterpriseDto input) 
        { 
            CheckUpdatePermission(); 
            var grantEnterprise = await _grantEnterpriseRepository.GetAsync(input.Id);
            MapToEntity(input, grantEnterprise); 
		    try{
               await _grantEnterpriseRepository.UpdateAsync(grantEnterprise); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var grantEnterprise = await _grantEnterpriseRepository.GetAsync(input.Id); 
               await _grantEnterpriseRepository.DeleteAsync(grantEnterprise);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.GrantEnterprises MapToEntity(CreateGrantEnterpriseDto createInput) 
        { 
            var grantEnterprise = ObjectMapper.Map<Models.GrantEnterprises>(createInput); 
            return grantEnterprise; 
        } 
        protected override void MapToEntity(UpdateGrantEnterpriseDto input, Models.GrantEnterprises grantEnterprise) 
        { 
            ObjectMapper.Map(input, grantEnterprise); 
        } 
        protected override IQueryable<Models.GrantEnterprises> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.GrantEnterprises> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.GrantEnterprises> GetEntityByIdAsync(int id) 
        { 
            var grantEnterprise = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(grantEnterprise); 
        } 
        protected override IQueryable<Models.GrantEnterprises> ApplySorting(IQueryable<Models.GrantEnterprises> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<GrantEnterpriseDto>> GetAllGrantEnterprises(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<GrantEnterpriseDto> ouput = new PagedResultDto<GrantEnterpriseDto>(); 
            IQueryable<Models.GrantEnterprises> query = query = from x in _grantEnterpriseRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _grantEnterpriseRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<GrantEnterprises.Dto.GrantEnterpriseDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<GrantEnterpriseDto>> GetAllGrantEnterprisesForCombo()
        {
            var grantEnterpriseList = await _grantEnterpriseRepository.GetAllListAsync(x => x.IsActive == true);

            var grantEnterprise = ObjectMapper.Map<List<GrantEnterpriseDto>>(grantEnterpriseList.ToList());

            return grantEnterprise;
        }
		
    } 
} ;
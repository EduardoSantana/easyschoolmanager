//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.GrantEnterprises.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.GrantEnterprises
{
      public interface IGrantEnterpriseAppService : IAsyncCrudAppService<GrantEnterpriseDto, int, PagedResultRequestDto, CreateGrantEnterpriseDto, UpdateGrantEnterpriseDto>
      {
            Task<PagedResultDto<GrantEnterpriseDto>> GetAllGrantEnterprises(GdPagedResultRequestDto input);
			Task<List<Dto.GrantEnterpriseDto>> GetAllGrantEnterprisesForCombo();
      }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.GrantEnterprises.Dto 
{
        [AutoMap(typeof(Models.GrantEnterprises))] 
        public class GrantEnterpriseDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public int Sequence {get;set;} 
              public string Name {get;set;} 
              public string Phone {get;set;} 
              public string Address {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
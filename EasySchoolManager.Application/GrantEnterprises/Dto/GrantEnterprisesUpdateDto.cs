//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.GrantEnterprises.Dto 
{
        [AutoMap(typeof(Models.GrantEnterprises))] 
        public class UpdateGrantEnterpriseDto : EntityDto<int> 
        {

              public int Sequence {get;set;} 

              [StringLength(200)]  
              public string Name {get;set;} 

              [StringLength(15)]  
              public string Phone {get;set;} 

              [StringLength(100)]  
              public string Address {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
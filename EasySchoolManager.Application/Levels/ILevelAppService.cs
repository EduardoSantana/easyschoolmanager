//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Levels.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Levels
{
      public interface ILevelAppService : IAsyncCrudAppService<LevelDto, int, PagedResultRequestDto, CreateLevelDto, UpdateLevelDto>
      {
            Task<PagedResultDto<LevelDto>> GetAllLevels(GdPagedResultRequestDto input);
			Task<List<Dto.LevelDto>> GetAllLevelsForCombo();
      }
}
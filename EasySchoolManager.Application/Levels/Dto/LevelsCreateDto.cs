//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Levels.Dto 
{
        [AutoMap(typeof(Models.Levels))] 
        public class CreateLevelDto : EntityDto<int> 
        {
              public string Name {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;}
              public string Code { get; set; }

    }
}
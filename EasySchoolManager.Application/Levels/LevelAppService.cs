//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Levels.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
namespace EasySchoolManager.Levels
{
    [AbpAuthorize(PermissionNames.Pages_Levels)]
    public class LevelAppService : BaseAppService<Models.Levels, LevelDto, int, PagedResultRequestDto, CreateLevelDto, UpdateLevelDto>,  ILevelAppService
    {
        private readonly IRepository<Models.Levels, int> _levelRepository;

        public LevelAppService(
            IRepository<Models.Levels, int> repository,
            IRepository<Models.Levels, int> levelRepository
            )
            : base(repository)
        {
            _levelRepository = levelRepository;



        }
        public override async Task<LevelDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<LevelDto> Create(CreateLevelDto input)
        {
            CheckCreatePermission();
            var level = ObjectMapper.Map<Models.Levels>(input);
            await _levelRepository.InsertAsync(level);
            return MapToEntityDto(level);
        }
        public override async Task<LevelDto> Update(UpdateLevelDto input)
        {
            CheckUpdatePermission();
            var level = await _levelRepository.GetAsync(input.Id);
            MapToEntity(input, level);
            await _levelRepository.UpdateAsync(level);
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            var level = await _levelRepository.GetAsync(input.Id);
            await _levelRepository.DeleteAsync(level);
        }
        protected override Models.Levels MapToEntity(CreateLevelDto createInput)
        {
            var level = ObjectMapper.Map<Models.Levels>(createInput);
            return level;
        }
        protected override void MapToEntity(UpdateLevelDto input, Models.Levels level)
        {
            ObjectMapper.Map(input, level);
        }
        protected override IQueryable<Models.Levels> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.Levels> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.Levels> GetEntityByIdAsync(int id)
        {
            var level = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(level);
        }
        protected override IQueryable<Models.Levels> ApplySorting(IQueryable<Models.Levels> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Name);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<LevelDto>> GetAllLevels(GdPagedResultRequestDto input)
        {
            PagedResultDto<LevelDto> ouput = new PagedResultDto<LevelDto>();
            IQueryable<Models.Levels> query = query = from x in _levelRepository.GetAll()
                                                      select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _levelRepository.GetAll()
                        where x.Name.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Levels.Dto.LevelDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<LevelDto>> GetAllLevelsForCombo()
        {
            var levelList = await _levelRepository.GetAllListAsync(x => x.IsActive == true);

            var level = ObjectMapper.Map<List<LevelDto>>(levelList.ToList());

            return level;
        }

    }

}

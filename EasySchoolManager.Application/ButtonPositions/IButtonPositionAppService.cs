//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.ButtonPositions.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.ButtonPositions
{
      public interface IButtonPositionAppService : IAsyncCrudAppService<ButtonPositionDto, int, PagedResultRequestDto, CreateButtonPositionDto, UpdateButtonPositionDto>
      {
            Task<PagedResultDto<ButtonPositionDto>> GetAllButtonPositions(GdPagedResultRequestDto input);
			Task<List<Dto.ButtonPositionDto>> GetAllButtonPositionsForCombo();
      }
}
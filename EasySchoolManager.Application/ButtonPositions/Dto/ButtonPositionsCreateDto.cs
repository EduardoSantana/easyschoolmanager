//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.ButtonPositions.Dto 
{
        [AutoMap(typeof(Models.ButtonPositions))] 
        public class CreateButtonPositionDto : EntityDto<int> 
        {

              [StringLength(2)] 
              public string Number {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.ButtonPositions.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ButtonPositions 
{ 
    [AbpAuthorize(PermissionNames.Pages_ButtonPositions)] 
    public class ButtonPositionAppService : AsyncCrudAppService<Models.ButtonPositions, ButtonPositionDto, int, PagedResultRequestDto, CreateButtonPositionDto, UpdateButtonPositionDto>, IButtonPositionAppService 
    { 
        private readonly IRepository<Models.ButtonPositions, int> _buttonPositionRepository;
		


        public ButtonPositionAppService( 
            IRepository<Models.ButtonPositions, int> repository, 
            IRepository<Models.ButtonPositions, int> buttonPositionRepository 
            ) 
            : base(repository) 
        { 
            _buttonPositionRepository = buttonPositionRepository; 
			

			
        } 
        public override async Task<ButtonPositionDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ButtonPositionDto> Create(CreateButtonPositionDto input) 
        { 
            CheckCreatePermission(); 
            var buttonPosition = ObjectMapper.Map<Models.ButtonPositions>(input); 
			try{
              await _buttonPositionRepository.InsertAsync(buttonPosition); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(buttonPosition); 
        } 
        public override async Task<ButtonPositionDto> Update(UpdateButtonPositionDto input) 
        { 
            CheckUpdatePermission(); 
            var buttonPosition = await _buttonPositionRepository.GetAsync(input.Id);
            MapToEntity(input, buttonPosition); 
		    try{
               await _buttonPositionRepository.UpdateAsync(buttonPosition); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var buttonPosition = await _buttonPositionRepository.GetAsync(input.Id); 
               await _buttonPositionRepository.DeleteAsync(buttonPosition);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.ButtonPositions MapToEntity(CreateButtonPositionDto createInput) 
        { 
            var buttonPosition = ObjectMapper.Map<Models.ButtonPositions>(createInput); 
            return buttonPosition; 
        } 
        protected override void MapToEntity(UpdateButtonPositionDto input, Models.ButtonPositions buttonPosition) 
        { 
            ObjectMapper.Map(input, buttonPosition); 
        } 
        protected override IQueryable<Models.ButtonPositions> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ButtonPositions> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Number.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ButtonPositions> GetEntityByIdAsync(int id) 
        { 
            var buttonPosition = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(buttonPosition); 
        } 
        protected override IQueryable<Models.ButtonPositions> ApplySorting(IQueryable<Models.ButtonPositions> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Number); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ButtonPositionDto>> GetAllButtonPositions(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ButtonPositionDto> ouput = new PagedResultDto<ButtonPositionDto>(); 
            IQueryable<Models.ButtonPositions> query = query = from x in _buttonPositionRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _buttonPositionRepository.GetAll() 
                        where x.Number.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ButtonPositions.Dto.ButtonPositionDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<ButtonPositionDto>> GetAllButtonPositionsForCombo()
        {
            var buttonPositionList = await _buttonPositionRepository.GetAllListAsync(x => x.IsActive == true);

            var buttonPosition = ObjectMapper.Map<List<ButtonPositionDto>>(buttonPositionList.ToList());

            return buttonPosition;
        }
		
    } 
} ;
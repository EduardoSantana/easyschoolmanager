//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.TransactionTypes.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.TransactionTypes
{
      public interface ITransactionTypeAppService : IAsyncCrudAppService<TransactionTypeDto, int, PagedResultRequestDto, CreateTransactionTypeDto, UpdateTransactionTypeDto>
      {
            Task<PagedResultDto<TransactionTypeDto>> GetAllTransactionTypes(GdPagedResultRequestDto input);
			Task<List<Dto.TransactionTypeDto>> GetAllTransactionTypesForCombo();
      }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.TransactionTypes.Dto 
{
        [AutoMap(typeof(Models.TransactionTypes))] 
        public class CreateTransactionTypeDto : EntityDto<int> 
        {

              [StringLength(25)] 
              public string Name {get;set;} 

              [StringLength(3)] 
              public string Abbreviation {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
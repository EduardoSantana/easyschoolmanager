//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.TransactionTypes.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.TransactionTypes 
{ 
    [AbpAuthorize(PermissionNames.Pages_TransactionTypes)] 
    public class TransactionTypeAppService : AsyncCrudAppService<Models.TransactionTypes, TransactionTypeDto, int, PagedResultRequestDto, CreateTransactionTypeDto, UpdateTransactionTypeDto>, ITransactionTypeAppService 
    { 
        private readonly IRepository<Models.TransactionTypes, int> _transactionTypeRepository;
		


        public TransactionTypeAppService( 
            IRepository<Models.TransactionTypes, int> repository, 
            IRepository<Models.TransactionTypes, int> transactionTypeRepository 
            ) 
            : base(repository) 
        { 
            _transactionTypeRepository = transactionTypeRepository; 
			

			
        } 
        public override async Task<TransactionTypeDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<TransactionTypeDto> Create(CreateTransactionTypeDto input) 
        { 
            CheckCreatePermission(); 
            var transactionType = ObjectMapper.Map<Models.TransactionTypes>(input); 
			try{
              await _transactionTypeRepository.InsertAsync(transactionType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(transactionType); 
        } 
        public override async Task<TransactionTypeDto> Update(UpdateTransactionTypeDto input) 
        { 
            CheckUpdatePermission(); 
            var transactionType = await _transactionTypeRepository.GetAsync(input.Id);
            MapToEntity(input, transactionType); 
		    try{
               await _transactionTypeRepository.UpdateAsync(transactionType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var transactionType = await _transactionTypeRepository.GetAsync(input.Id); 
               await _transactionTypeRepository.DeleteAsync(transactionType);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.TransactionTypes MapToEntity(CreateTransactionTypeDto createInput) 
        { 
            var transactionType = ObjectMapper.Map<Models.TransactionTypes>(createInput); 
            return transactionType; 
        } 
        protected override void MapToEntity(UpdateTransactionTypeDto input, Models.TransactionTypes transactionType) 
        { 
            ObjectMapper.Map(input, transactionType); 
        } 
        protected override IQueryable<Models.TransactionTypes> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.TransactionTypes> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.TransactionTypes> GetEntityByIdAsync(int id) 
        { 
            var transactionType = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(transactionType); 
        } 
        protected override IQueryable<Models.TransactionTypes> ApplySorting(IQueryable<Models.TransactionTypes> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<TransactionTypeDto>> GetAllTransactionTypes(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<TransactionTypeDto> ouput = new PagedResultDto<TransactionTypeDto>(); 
            IQueryable<Models.TransactionTypes> query = query = from x in _transactionTypeRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _transactionTypeRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<TransactionTypes.Dto.TransactionTypeDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<TransactionTypeDto>> GetAllTransactionTypesForCombo()
        {
            var transactionTypeList = await _transactionTypeRepository.GetAllListAsync(x => x.IsActive == true);

            var transactionType = ObjectMapper.Map<List<TransactionTypeDto>>(transactionTypeList.ToList());

            return transactionType;
        }
		
    } 
} ;
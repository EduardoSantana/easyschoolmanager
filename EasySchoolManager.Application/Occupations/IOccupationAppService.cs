//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Occupations.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Occupations
{
      public interface IOccupationAppService : IAsyncCrudAppService<OccupationDto, int, PagedResultRequestDto, CreateOccupationDto, UpdateOccupationDto>
      {
            Task<PagedResultDto<OccupationDto>> GetAllOccupations(GdPagedResultRequestDto input);
			Task<List<Dto.OccupationDto>> GetAllOccupationsForCombo();
      }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Occupations.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Occupations 
{ 
    [AbpAuthorize(PermissionNames.Pages_Occupations)] 
    public class OccupationAppService : AsyncCrudAppService<Models.Occupations, OccupationDto, int, PagedResultRequestDto, CreateOccupationDto, UpdateOccupationDto>, IOccupationAppService 
    { 
        private readonly IRepository<Models.Occupations, int> _occupationRepository;
		


        public OccupationAppService( 
            IRepository<Models.Occupations, int> repository, 
            IRepository<Models.Occupations, int> occupationRepository 
            ) 
            : base(repository) 
        { 
            _occupationRepository = occupationRepository; 
			

			
        } 
        public override async Task<OccupationDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<OccupationDto> Create(CreateOccupationDto input) 
        { 
            CheckCreatePermission(); 
            var occupation = ObjectMapper.Map<Models.Occupations>(input); 
			try{
              await _occupationRepository.InsertAsync(occupation); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(occupation); 
        } 
        public override async Task<OccupationDto> Update(UpdateOccupationDto input) 
        { 
            CheckUpdatePermission(); 
            var occupation = await _occupationRepository.GetAsync(input.Id);
            MapToEntity(input, occupation); 
		    try{
               await _occupationRepository.UpdateAsync(occupation); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var occupation = await _occupationRepository.GetAsync(input.Id); 
               await _occupationRepository.DeleteAsync(occupation);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Occupations MapToEntity(CreateOccupationDto createInput) 
        { 
            var occupation = ObjectMapper.Map<Models.Occupations>(createInput); 
            return occupation; 
        } 
        protected override void MapToEntity(UpdateOccupationDto input, Models.Occupations occupation) 
        { 
            ObjectMapper.Map(input, occupation); 
        } 
        protected override IQueryable<Models.Occupations> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Occupations> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Occupations> GetEntityByIdAsync(int id) 
        { 
            var occupation = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(occupation); 
        } 
        protected override IQueryable<Models.Occupations> ApplySorting(IQueryable<Models.Occupations> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<OccupationDto>> GetAllOccupations(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<OccupationDto> ouput = new PagedResultDto<OccupationDto>(); 
            IQueryable<Models.Occupations> query = query = from x in _occupationRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _occupationRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Occupations.Dto.OccupationDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<OccupationDto>> GetAllOccupationsForCombo()
        {
            var occupationList = await _occupationRepository.GetAllListAsync(x => x.IsActive == true);

            var occupation = ObjectMapper.Map<List<OccupationDto>>(occupationList.ToList());

            return occupation;
        }
		
    } 
} ;
﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.EnrollmentStudents.Dto
{
    [AutoMap(typeof(Models.EnrollmentStudents))]
    public class EnrollmentStudentDto : EntityDto<int>
    {
        public long EnrollmentId { get; set; }

        public int StudentId { get; set; }

        public int PeriodId { get; set; }

        public int RelationshipId { get; set; }

        //Fue comentada la propiedad enrollment debido a que provocaba redundancia ciclica y lanzaba un error
        //public Enrollments.Dto.EnrollmentDto Enrollments { set; get; }

        public Relationships.Dto.RelationshipDto Relationships { set; get; }

        public MultiTenancy.Dto.TenantDto Tenants { set; get; }

        public List<CourseEnrollmentStudents.Dto.CourseEnrollmentStudentDto> CourseEnrollmentStudents { get; set; }

        public Periods.Dto.PeriodDto Periods { get; set; }

        public Students.Dto.StudentDto3 Students { get; set; }

        public decimal StudentBalance { get; set; }

        public List<TransactionStudents.Dto.TransactionStudentDto> TransactionStudents { get; set; }

        public bool IsActive { get; set; }

    }
}

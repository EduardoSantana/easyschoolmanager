//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.Articles.Dto
{
    [AutoMap(typeof(Models.Articles))]
    public class ArticleDto : EntityDto<int>
    {
        public ArticleDto()
        {
            Inventories = new  HashSet<Inventory.Dto.InventoryDto>();
        }

        public string Description { get; set; }
        public string Reference { get; set; }
        public Decimal Price { get; set; }
        public int? ArticleTypeId { get; set; }
        public int? UnitId { get; set; }
        public int? BrandId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string ArticlesTypes_Description { get; set; }
        public string Units_Description { get; set; }
        public string Brands_Name { get; set; }

        //ThoseProperty just will be used to sell products in the receipt option
        public long? Quantity { get; set; }
        public Decimal? Total { get; set; }

        public virtual ICollection<Inventory.Dto.InventoryDto> Inventories { get; set; }
    }
}
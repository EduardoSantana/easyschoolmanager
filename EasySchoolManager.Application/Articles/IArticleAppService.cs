//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Articles.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Articles
{
      public interface IArticleAppService : IAsyncCrudAppService<ArticleDto, int, PagedResultRequestDto, CreateArticleDto, UpdateArticleDto>
      {
            Task<PagedResultDto<ArticleDto>> GetAllArticles(GdPagedResultRequestDto input);
			Task<List<Dto.ArticleDto>> GetAllArticlesForCombo();
      }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Articles.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Articles 
{ 
    [AbpAuthorize(PermissionNames.Pages_Articles)] 
    public class ArticleAppService : AsyncCrudAppService<Models.Articles, ArticleDto, int, PagedResultRequestDto, CreateArticleDto, UpdateArticleDto>, IArticleAppService 
    { 
        private readonly IRepository<Models.Articles, int> _articleRepository;
		
		    private readonly IRepository<Models.ArticlesTypes, int> _articlesTypeRepository;
		    private readonly IRepository<Models.Units, int> _unitRepository;
		    private readonly IRepository<Models.Brands, int> _brandRepository;


        public ArticleAppService( 
            IRepository<Models.Articles, int> repository, 
            IRepository<Models.Articles, int> articleRepository ,
            IRepository<Models.ArticlesTypes, int> articlesTypeRepository
,
            IRepository<Models.Units, int> unitRepository
,
            IRepository<Models.Brands, int> brandRepository

            ) 
            : base(repository) 
        { 
            _articleRepository = articleRepository; 
			
            _articlesTypeRepository = articlesTypeRepository;

            _unitRepository = unitRepository;

            _brandRepository = brandRepository;


			
        } 
        public override async Task<ArticleDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ArticleDto> Create(CreateArticleDto input) 
        { 
            CheckCreatePermission(); 
            var article = ObjectMapper.Map<Models.Articles>(input); 
			try{
              await _articleRepository.InsertAsync(article); 



              
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(article); 
        } 

        public override async Task<ArticleDto> Update(UpdateArticleDto input) 
        { 
            CheckUpdatePermission(); 
            var article = await _articleRepository.GetAsync(input.Id);
            MapToEntity(input, article); 
		    try{
               await _articleRepository.UpdateAsync(article); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var article = await _articleRepository.GetAsync(input.Id); 
               await _articleRepository.DeleteAsync(article);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Articles MapToEntity(CreateArticleDto createInput) 
        { 
            var article = ObjectMapper.Map<Models.Articles>(createInput); 
            return article; 
        } 
        protected override void MapToEntity(UpdateArticleDto input, Models.Articles article) 
        { 
            ObjectMapper.Map(input, article); 
        } 
        protected override IQueryable<Models.Articles> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Articles> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Articles> GetEntityByIdAsync(int id) 
        { 
            var article = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(article); 
        } 
        protected override IQueryable<Models.Articles> ApplySorting(IQueryable<Models.Articles> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Description); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ArticleDto>> GetAllArticles(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ArticleDto> ouput = new PagedResultDto<ArticleDto>(); 
            IQueryable<Models.Articles> query = query = from x in _articleRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _articleRepository.GetAll() 
                        where x.Description.Contains(input.TextFilter) || x.Reference.Contains(input.TextFilter)
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Articles.Dto.ArticleDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<ArticleDto>> GetAllArticlesForCombo()
        {
            var articleList = await _articleRepository.GetAllListAsync(x => x.IsActive == true);

            var article = ObjectMapper.Map<List<ArticleDto>>(articleList.ToList());

            return article;
        }
		
    } 
} ;
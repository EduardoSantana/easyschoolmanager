//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.Students.Dto
{
    [AutoMap(typeof(Models.Students))]
    public class StudentsForReceiptsDto : EntityDto<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string EmergencyNote { get; set; }
        public int GenderId { get; set; }
        public string Derivation { get; set; }
        public string Medication { get; set; }
        public string Nutrition { get; set; }
        public int BloodId { get; set; }
        public string EmailAddress { get; set; }
        public string FatherPersonalID { get; set; }
        public string FatherName { get; set; }
        public string MotherPersonalId { get; set; }
        public string MotherName { get; set; }
        public bool? FatherAttendedHere { get; set; }
        public bool? MotherAttendedHere { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string Genders_Name { get; set; }
        public string Bloods_Name { get; set; }
        public Decimal Amount { get; set; }

        public int EnrollmentStudentId { get; set; }

        public long EnrollmentId { get; set; }

        public int RelationshipId { get; set; }

        public int CourseId { get; set; }

        public int LevelId { get; set; }
    }
}
//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.Students.Dto
{
    [AutoMap(typeof(Models.Students))]
    public class StudentDto : EntityDto<int>
    {
        public StudentDto()
        {
            StudentIllnesses = new List<EasySchoolManager.StudentIllnesses.Dto.StudentIllnessesDto>();
            StudentAllergies = new List<EasySchoolManager.StudentAllergies.Dto.StudentAllergiesDto>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EnrollmentName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string EmergencyNote { get; set; }
        public int GenderId { get; set; }
        public string Derivation { get; set; }
        public string Medication { get; set; }
        public string Nutrition { get; set; }
        public int BloodId { get; set; }
        public string EmailAddress { get; set; }
        public string StudentPersonalId { get; set; }
        public string FatherPersonalID { get; set; }
        public string FatherName { get; set; }
        public string MotherPersonalId { get; set; }
        public string MotherName { get; set; }
        public bool? FatherAttendedHere { get; set; }
        public bool? MotherAttendedHere { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string Genders_Name { get; set; }
        public string Bloods_Name { get; set; }
        public int EnrollmentSequence { get; set; }
        public bool? EmployeeSon { get; set; }

        public long EnrollmentId { get; set; }

        public int RelationshipId { get; set; }

        public int CourseId { get; set; }

        public int LevelId { get; set; }
        public string FullName { get; set; }
        public string FullNameInverso { get; set; }
        public List<StudentIllnesses.Dto.StudentIllnessesDto> StudentIllnesses { get; set; }
        public List<StudentAllergies.Dto.StudentAllergiesDto> StudentAllergies { get; set; }
        public List<EnrollmentStudents.Dto.EnrollmentStudentDto> EnrollmentStudents { get; set; }

    }
}
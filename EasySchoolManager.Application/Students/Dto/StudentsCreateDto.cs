//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.Students.Dto
{
    [AutoMap(typeof(Models.Students))]
    public class CreateStudentDto : EntityDto<int>
    {

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [Required(ErrorMessage ="Debe indicar la Fecha de Nacimiento Valida en formato dd/MM/yyy. Ej. 05/03/1982")]
        public DateTime DateOfBirth { get; set; }

        [StringLength(250)]
        public string EmergencyNote { get; set; }

        public int GenderId { get; set; }

        [StringLength(100)]
        public string Derivation { get; set; }

        [StringLength(200)]
        public string Medication { get; set; }

        [StringLength(200)]
        public string Nutrition { get; set; }

        public int BloodId { get; set; }

        public bool? EmployeeSon { get; set; }

        [StringLength(255)]
        public string EmailAddress { get; set; }

        [StringLength(11)]
        public string StudentPersonalId { get; set; }

        [StringLength(11)]
        public string FatherPersonalId { get; set; }

        [StringLength(100)]
        public string FatherName { get; set; }

        [StringLength(11)]
        public string MotherPersonalId { get; set; }

        [StringLength(100)]
        public string MotherName { get; set; }

        public bool? FatherAttendedHere { get; set; }

        public bool? MotherAttendedHere { get; set; }

        public bool IsActive { get; set; }

        public long EnrollmentId { get; set; }

        public int RelationshipId { get; set; }

        public int CourseId { get; set; }

        public short Position { get; set; }

    
        public List<StudenIllnesses.Dto.StudentIllnessCreateDto> StudentIllnesses { get; set; }

        public List<StudentAllergies.Dto.StudentAlergyCreateDto> StudentAllergies { get; set; }

        public bool IsPreviousPeriod { get; set; }

        public bool IsNextPeriod { get; set; }

    }
}
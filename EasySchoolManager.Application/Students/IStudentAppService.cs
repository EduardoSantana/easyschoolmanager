//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Students.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Students
{
    public interface IStudentAppService : IAsyncCrudAppService<StudentDto, int, PagedResultRequestDto, CreateStudentDto, UpdateStudentDto>
    {
        Task<PagedResultDto<StudentDto>> GetAllStudents(GdPagedResultRequestDto input);
        Task<PagedResultDto<StudentDto>> GetAllStudentsForNameOrFatherName(GdPagedResultRequestDto input);
        Task<List<Dto.StudentDto>> GetAllStudentsForCombo();
        Task<PagedResultDto<StudentDto>> GetAllStudentForEnrollment(long enrollmentId, int period123);
        Task<StudentDto> GetStudentByPersonalId(string studentId);
        Task<StudentDto> GetFromCurrentPeriod(EntityDto<int> input);
        Task PassStudentToCurrentPeriod(int id);
        Task PassStudentToBackPeriod(int id);
        Task PassStudentToNextPeriod(int id);
        Task PutStudentInactive(int id);
        void RegisterEnrollmentStudent(Models.EnrollmentStudents student, int periodId, int courseId, int tenantId, bool IsActive = true);
    }
}
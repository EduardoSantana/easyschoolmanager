//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Students.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.MultiTenancy;

namespace EasySchoolManager.Students
{
    [AbpAuthorize(PermissionNames.Pages_Enrollments)]
    public class StudentAppService : AsyncCrudAppService<Models.Students, StudentDto, int, PagedResultRequestDto, CreateStudentDto, UpdateStudentDto>, IStudentAppService
    {
        private readonly IRepository<Models.Students, int> _studentRepository;
        private readonly IRepository<Models.Genders, int> _genderRepository;
        private readonly IRepository<Models.Bloods, int> _bloodRepository;
        private readonly IRepository<Models.EnrollmentStudents, int> _enrollmentStudentRepository;
        private readonly IRepository<Models.Relationships, int> _relationshipsRepository;
        private readonly IRepository<Models.CourseEnrollmentStudents, int> _courseEnrollmentStudentRepository;
        private readonly IRepository<Models.Courses, int> _coursesRepository;
        private readonly IRepository<Models.StudentAllergies, int> _studentAllergyRepository;
        private readonly IRepository<Models.StudentIllnesses, int> _studentIllnessRepository;
        private readonly IRepository<Tenant> _tenatRepository;
        public readonly TenantManager _tenantManager;

        private readonly IRepository<Models.Illnesses, int> _illnessRepository;
        private readonly IRepository<Models.Allergies, int> _allergyRepository;
        private readonly IRepository<Models.Levels, int> _levelRepository;

        public StudentAppService(
            IRepository<Models.Students, int> studentRepository,
            IRepository<Models.Genders, int> genderRepository,
            IRepository<Models.Bloods, int> bloodRepository,
            IRepository<Models.EnrollmentStudents, int> enrollmentStudentRepository,
            IRepository<Models.Relationships, int> relationshipsRepository,
            IRepository<Models.Illnesses, int> illnessesRepository,
            IRepository<Models.Allergies, int> allergiesRepository,
            IRepository<Models.Courses, int> coursesRepository,
            IRepository<Models.CourseEnrollmentStudents, int> courseEnrollmentStudentRepository,
            IRepository<Models.StudentAllergies, int> studentAllergiesRepository,
            IRepository<Models.StudentIllnesses, int> studentIllnessesRepository,
            IRepository<Models.Levels, int> levelRepository,
            TenantManager tenantManager,
            IRepository<Tenant> tenatRepository

            )
            : base(studentRepository)
        {
            _studentRepository = studentRepository;

            _genderRepository = genderRepository;

            _bloodRepository = bloodRepository;

            _enrollmentStudentRepository = enrollmentStudentRepository;

            _relationshipsRepository = relationshipsRepository;

            _illnessRepository = illnessesRepository;

            _allergyRepository = allergiesRepository;

            _courseEnrollmentStudentRepository = courseEnrollmentStudentRepository;

            _studentAllergyRepository = studentAllergiesRepository;

            _studentIllnessRepository = studentIllnessesRepository;

            _levelRepository = levelRepository;

            _tenantManager = tenantManager;

            _tenatRepository = tenatRepository;

            _coursesRepository = coursesRepository;

            this.LocalizationSourceName = EasySchoolManagerConsts.LocalizationSourceName;
        }

        public async Task PassStudentToBackPeriod(int id)
        {
            try
            {
                var userObj = _enrollmentStudentRepository.Get(id);
                var selectedTenant = await _tenantManager.GetByIdAsync(userObj.TenantId);
                var courseObj = userObj.CourseEnrollmentStudents.FirstOrDefault().Courses;
                var coursePlano = userObj.CourseEnrollmentStudents.FirstOrDefault();
                var exist = _enrollmentStudentRepository.GetAllList(f =>
                               f.EnrollmentId == userObj.EnrollmentId &&
                               f.StudentId == userObj.StudentId &&
                               f.PeriodId == selectedTenant.PreviousPeriodId.Value &&
                               f.TenantId == selectedTenant.Id).FirstOrDefault();
                userObj.IsActive = false;
                userObj.LastModifierUserId = AbpSession.UserId;
                coursePlano.SessionId = null;
                coursePlano.Sequence = null;

                if (exist != null)
                {
                    exist.IsActive = true;
                    exist.LastModifierUserId = AbpSession.UserId;
                }
                else
                {
                    RegisterEnrollmentStudent(userObj, selectedTenant.PreviousPeriodId.Value, courseObj.Id, selectedTenant.Id);
                }

                await CurrentUnitOfWork.SaveChangesAsync();
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }

        }

        public async Task PutStudentInactive(int id)
        {
            try
            {
                var userObj = _enrollmentStudentRepository.Get(id);
                userObj.IsActive = false;
                userObj.LastModifierUserId = AbpSession.UserId;
                await CurrentUnitOfWork.SaveChangesAsync();
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }

        }

        public async Task PassStudentToCurrentPeriod(int id)
        {
            try
            {
                var userObj = _enrollmentStudentRepository.Get(id);
                var selectedTenant = await _tenantManager.GetByIdAsync(userObj.TenantId);
                var courseObj = userObj.CourseEnrollmentStudents.FirstOrDefault().Courses;
                int courseid = 0;
                try
                {
                    courseid = _coursesRepository
                    .GetAllList(f => f.Sequence > courseObj.Sequence)
                    .OrderBy(a => a.Sequence)
                    .FirstOrDefault()
                    .Id;
                }
                catch (Exception ex)
                {
                    courseid = courseObj.Id;
                }
                var userObjCurrentPeriod = _enrollmentStudentRepository.GetAllList(f =>
                               f.EnrollmentId == userObj.EnrollmentId &&
                               f.StudentId == userObj.StudentId &&
                               f.PeriodId == selectedTenant.PeriodId.Value &&
                               f.TenantId == selectedTenant.Id).FirstOrDefault();
                userObj.IsActive = false;
                userObj.LastModifierUserId = AbpSession.UserId;
                if (userObjCurrentPeriod == null)
                {
                    RegisterEnrollmentStudent(userObj, selectedTenant.PeriodId.Value, courseid, selectedTenant.Id);
                }
                else
                {
                    userObjCurrentPeriod.IsActive = true;
                    userObjCurrentPeriod.LastModifierUserId = AbpSession.UserId;
                }

                await CurrentUnitOfWork.SaveChangesAsync();
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }

        }



        public async Task PassStudentToNextPeriod(int id)
        {
            try
            {
                var userObj = _enrollmentStudentRepository.Get(id);
                var selectedTenant = await _tenantManager.GetByIdAsync(userObj.TenantId);
                var courseObj = userObj.CourseEnrollmentStudents.FirstOrDefault().Courses;
                int courseid = 0;
                try
                {
                    courseid = _coursesRepository
                    .GetAllList(f => f.Sequence > courseObj.Sequence)
                    .OrderBy(a => a.Sequence)
                    .FirstOrDefault()
                    .Id;
                }
                catch (Exception ex)
                {
                    courseid = courseObj.Id;
                }
                var userObjCurrentPeriod = _enrollmentStudentRepository.GetAllList(f =>
                               f.EnrollmentId == userObj.EnrollmentId &&
                               f.StudentId == userObj.StudentId &&
                               f.PeriodId == selectedTenant.NextPeriodId.Value &&
                               f.TenantId == selectedTenant.Id).FirstOrDefault();
                userObj.LastModifierUserId = AbpSession.UserId;

                if (!selectedTenant.NextPeriodId.HasValue)
                    throw new Exception(L("YouMustSelectTheNextPeriodoInTheTenantBeforePassOneStudent"));

                if (userObjCurrentPeriod == null)
                {
                    RegisterEnrollmentStudent(userObj, selectedTenant.NextPeriodId.Value, courseid, selectedTenant.Id);
                }
                else
                {
                    userObjCurrentPeriod.IsActive = true;
                    userObjCurrentPeriod.LastModifierUserId = AbpSession.UserId;
                }

                await CurrentUnitOfWork.SaveChangesAsync();
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }

        }

        public void RegisterEnrollmentStudent(Models.EnrollmentStudents student, int periodId, int courseId, int tenantId, bool IsActive = true)
        {

            var existingStudent = _enrollmentStudentRepository.GetAll().Where(x => x.StudentId == student.StudentId && x.PeriodId == periodId).FirstOrDefault();
            if (existingStudent != null)
                throw new Exception(L("ThisStudentAlredyExistInTheSpecifiedPeriod"));

            var enrollmentStuden = new Models.EnrollmentStudents();
            enrollmentStuden.EnrollmentId = student.EnrollmentId;
            enrollmentStuden.StudentId = student.StudentId;
            enrollmentStuden.RelationshipId = student.RelationshipId;
            enrollmentStuden.PeriodId = periodId;
            enrollmentStuden.IsActive = IsActive;
            enrollmentStuden.TenantId = tenantId;
            enrollmentStuden.CreatorUserId = AbpSession.UserId;
            var enrollmentStudenId = _enrollmentStudentRepository.InsertAndGetId(enrollmentStuden);

            var courseEnrollmentStudent = new Models.CourseEnrollmentStudents();
            courseEnrollmentStudent.CourseId = courseId;
            courseEnrollmentStudent.EnrollmentStudentId = enrollmentStudenId;
            courseEnrollmentStudent.IsActive = IsActive;
            courseEnrollmentStudent.TenantId = tenantId;
            courseEnrollmentStudent.CreatorUserId = AbpSession.UserId;
            _courseEnrollmentStudentRepository.Insert(courseEnrollmentStudent);

        }

        public override async Task<StudentDto> Get(EntityDto<int> input)
        {
            var student = await base.Get(input);
            try
            {
                student.LevelId = student.EnrollmentStudents.FirstOrDefault().CourseEnrollmentStudents.FirstOrDefault().Courses.LevelId;
            }
            catch (Exception err)
            {

            }

            return student;
        }

        public async Task<StudentDto> GetFromCurrentPeriod(EntityDto<int> input)
        {
            var student = await Get(input);
            var currentPeriod = _tenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId);
            if (currentPeriod != null)
            {
                student.EnrollmentStudents = student.EnrollmentStudents.Where(f => f.Periods.Id == currentPeriod.PeriodId).ToList();
            }
            else
            {
                var asdfasdf = 0;
            }
            return student;
        }

        public async Task<Periods.Dto.PeriodDto> GetCurrentPeriod()
        {
            Periods.Dto.PeriodDto per = null;
            try
            {
                var currentTenant = await Task.Run(() => { return _tenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId); });
                if (currentTenant != null)
                    per = ObjectMapper.Map<Periods.Dto.PeriodDto>(currentTenant.Periods);
            }
            catch { }
            return per;
        }

        public async Task<Periods.Dto.PeriodDto> GetPreviousPeriod()
        {
            Periods.Dto.PeriodDto per = null;
            try
            {
                var currentTenant = await Task.Run(() => { return _tenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId); });
                if (currentTenant != null)
                    per = ObjectMapper.Map<Periods.Dto.PeriodDto>(currentTenant.PreviousPeriods);
            }
            catch { }
            return per;
        }

        public async Task<Periods.Dto.PeriodDto> GetNextPeriod()
        {
            Periods.Dto.PeriodDto per = null;
            try
            {
                var currentTenant = await Task.Run(() => { return _tenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId); });
                if (currentTenant != null)
                    per = ObjectMapper.Map<Periods.Dto.PeriodDto>(currentTenant.NextPeriods);
            }
            catch { }
            return per;
        }


        [AbpAllowAnonymous]
        public async Task<StudentDto> GetStudentByPersonalId(string studentId)
        {
            var dato = await _studentRepository.GetAllListAsync(x => x.StudentPersonalId == studentId);
            var first = dato.FirstOrDefault();
            var mappedObject = ObjectMapper.Map<StudentDto>(first);
            if (mappedObject != null)
            {
                mappedObject.EnrollmentStudents = null;
            }
            return mappedObject;
        }

        public override async Task<StudentDto> Create(CreateStudentDto input)
        {
            CheckCreatePermission();
            var student = ObjectMapper.Map<Models.Students>(input);

            if (input.DateOfBirth < new DateTime(1900, 1, 1))
            {
                throw new UserFriendlyException(L("TheDateOfBirthIsInvalid"));
            }

            try
            {
                var usedPeriod = await GetCurrentPeriod();

                if (usedPeriod == null)
                    throw new Exception("ThereIsNoDefinedTheCurrentPeriodInTheSchoolPleaseContactTheAdministration");

                if (input.IsPreviousPeriod == true)
                {
                    usedPeriod = await GetPreviousPeriod();
                    if (usedPeriod == null)
                        throw new Exception("YouMustSetThePreviousPeriodForTheCurrentSchool");
                }

                if (input.IsNextPeriod == true)
                {
                    usedPeriod = await GetNextPeriod();
                    if (usedPeriod == null)
                        throw new Exception("YouMustSetThePreviousPeriodForTheCurrentSchool");
                }

                if (input.StudentPersonalId == null)
                {
                    student.StudentPersonalId = "-1";

                }

                student.FirstName = student.FirstName.ToUpper();
                student.LastName = student.LastName.ToUpper();
                student.FatherName = student.FatherName?.ToUpper();
                student.MotherName = student.MotherName?.ToUpper();

                student.StudentIllnesses = null;
                student.StudentAllergies = null;
                _studentRepository.InsertAndGetId(student);
                if (student.StudentPersonalId == "-1")
                {
                    student.StudentPersonalId = student.Id.ToString();
                }

                CurrentUnitOfWork.SaveChanges();
                Models.EnrollmentStudents enrollmentStuden = new Models.EnrollmentStudents();
                enrollmentStuden.StudentId = student.Id;
                enrollmentStuden.EnrollmentId = input.EnrollmentId;
                enrollmentStuden.RelationshipId = input.RelationshipId;
                enrollmentStuden.PeriodId = usedPeriod.Id;
                enrollmentStuden.IsActive = true;


                if (enrollmentStuden.RelationshipId == 0)
                    enrollmentStuden.RelationshipId = 1;

                _enrollmentStudentRepository.InsertAndGetId(enrollmentStuden);

                Models.CourseEnrollmentStudents courseEnrollmentStudent = new Models.CourseEnrollmentStudents();

                courseEnrollmentStudent.CourseId = input.CourseId;
                courseEnrollmentStudent.IsActive = true;
                if (courseEnrollmentStudent.CourseId == 0)
                    courseEnrollmentStudent.CourseId = 1;
                courseEnrollmentStudent.EnrollmentStudentId = enrollmentStuden.Id;
                _courseEnrollmentStudentRepository.Insert(courseEnrollmentStudent);


                foreach (var item in input.StudentAllergies)
                {
                    item.AllergyId = item.Id;
                    item.StudentId = student.Id;
                    var currentAllergy = ObjectMapper.Map<Models.StudentAllergies>(item);
                    _studentAllergyRepository.Insert(currentAllergy);

                }

                foreach (var item in input.StudentIllnesses)
                {
                    item.IllnessId = item.Id;
                    item.StudentId = student.Id;
                    var currentIllness = ObjectMapper.Map<Models.StudentIllnesses>(item);
                    _studentIllnessRepository.Insert(currentIllness);
                }

            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(student);
        }
        public override async Task<StudentDto> Update(UpdateStudentDto input)
        {
            CheckUpdatePermission();
            var student = _studentRepository.Get(input.Id);
            MapToEntity(input, student);


            try
            {
                student.FirstName = student.FirstName.ToUpper();
                student.LastName = student.LastName.ToUpper();
                student.FatherName = student.FatherName?.ToUpper();
                student.MotherName = student.MotherName?.ToUpper();
                student.StudentIllnesses = null;
                student.StudentAllergies = null;
                var enrollmentStudent = student.EnrollmentStudents.FirstOrDefault(x => x.IsActive == true);
                //Si no existe un enrollment sttudent, que representa la relcion entre el studiante el el tutor, se crear� uno
                if (enrollmentStudent == null)
                {
                    enrollmentStudent = new Models.EnrollmentStudents();
                    enrollmentStudent.StudentId = student.Id;
                    enrollmentStudent.EnrollmentId = student.EnrollmentStudents.FirstOrDefault().EnrollmentId;
                    enrollmentStudent.RelationshipId = input.RelationshipId;
                    _enrollmentStudentRepository.InsertAndGetId(enrollmentStudent);
                }
                enrollmentStudent.Relationships = null;
                enrollmentStudent.RelationshipId = input.RelationshipId;

                Models.CourseEnrollmentStudents courseEnrollmentStudent = enrollmentStudent.CourseEnrollmentStudents.FirstOrDefault(x => x.IsActive == true);
                if (courseEnrollmentStudent == null)
                {
                    courseEnrollmentStudent = new Models.CourseEnrollmentStudents();

                    courseEnrollmentStudent.CourseId = input.CourseId;
                    courseEnrollmentStudent.IsActive = true;
                    if (courseEnrollmentStudent.CourseId == 0)
                        courseEnrollmentStudent.CourseId = 1;
                    courseEnrollmentStudent.EnrollmentStudentId = enrollmentStudent.Id;
                    _courseEnrollmentStudentRepository.Insert(courseEnrollmentStudent);
                }
                else
                {
                    if (courseEnrollmentStudent.CourseId != input.CourseId)
                    {
                        if (courseEnrollmentStudent.Sequence.HasValue)
                        {
                            throw new UserFriendlyException(L("TheStudentHasBeenAssignedToASessionYouMustRemoveItIfWantToDoAnyChange"));
                        }
                        courseEnrollmentStudent.SessionId = null;
                    }
                    courseEnrollmentStudent.CourseId = input.CourseId;
                }



                var currentIllnesses = (from x in _studentIllnessRepository.GetAllList(x => x.StudentId == student.Id)
                                        select x.IllnessId).ToList();

                var insertingIllnesses = (from x in input.StudentIllnesses
                                          select x.Id).ToList();
                var newIllnesses = insertingIllnesses.Except(currentIllnesses).ToList();
                var erasedIllnesses = currentIllnesses.Except(insertingIllnesses).ToList();

                var currentAllergies = (from x in _studentAllergyRepository.GetAllList(x => x.StudentId == student.Id)
                                        select x.AllergyId).ToList();

                var insertingAllergies = (from x in input.StudentAllergies
                                          select x.Id).ToList();
                var newAllergies = insertingAllergies.Except(currentAllergies).ToList();
                var erasedAllergies = currentAllergies.Except(insertingAllergies).ToList();


                //Borrando de la base de datos las alergias que fueron desceleccionadas.
                foreach (var item in erasedAllergies)
                {
                    var toErraseAllergy = _studentAllergyRepository.GetAllList(x => x.StudentId == student.Id && x.AllergyId == item).FirstOrDefault();
                    if (toErraseAllergy != null)
                    {
                        _studentAllergyRepository.Delete(toErraseAllergy.Id);
                    }
                }

                //Borrando de la base de datos las enfermedades que fueron desceleccionadas.
                foreach (var item in erasedIllnesses)
                {
                    var toErraseIllnesses = _studentIllnessRepository.GetAllList(x => x.StudentId == student.Id && x.IllnessId == item).FirstOrDefault();
                    if (toErraseIllnesses != null)
                    {
                        _studentIllnessRepository.Delete(toErraseIllnesses.Id);

                    }
                }

                //Creando las nuevas alergias
                foreach (var item in newAllergies)
                {
                    var currentAllergy = new Models.StudentAllergies();
                    currentAllergy.AllergyId = item;
                    currentAllergy.StudentId = student.Id;
                    _studentAllergyRepository.Insert(currentAllergy);
                }

                //crando las nuevas enfermedades
                foreach (var item in newIllnesses)
                {
                    var currentIllness = new Models.StudentIllnesses();
                    currentIllness.IllnessId = item;
                    currentIllness.StudentId = student.Id;
                    _studentIllnessRepository.Insert(currentIllness);
                }

            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }

            try
            {
                _studentRepository.Update(student);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            var returnedValue = await Get(input);
            return returnedValue;
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var student = await _studentRepository.GetAsync(input.Id);
                await _studentRepository.DeleteAsync(student);
                var enrollstudent = await _enrollmentStudentRepository.GetAllListAsync(x => x.StudentId == input.Id);
                var enrollstudentfinal = enrollstudent.FirstOrDefault();
                await _enrollmentStudentRepository.DeleteAsync(enrollstudentfinal);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.Students MapToEntity(CreateStudentDto createInput)
        {
            var student = ObjectMapper.Map<Models.Students>(createInput);
            return student;
        }
        protected override void MapToEntity(UpdateStudentDto input, Models.Students student)
        {
            //ObjectMapper.Map(input, student);

            student.BloodId = input.BloodId;
            student.Derivation = input.Derivation;
            student.EmailAddress = input.EmailAddress;
            student.EmergencyNote = input.EmergencyNote;
            student.FatherName = input.FatherName;
            student.StudentPersonalId = input.StudentPersonalId;
            student.FatherPersonalId = input.FatherPersonalID;
            student.FirstName = input.FirstName;
            student.GenderId = input.GenderId;
            student.IsActive = input.IsActive;
            student.LastName = input.LastName;
            student.Medication = input.Medication;
            student.MotherAttendedHere = input.MotherAttendedHere;
            student.MotherName = input.MotherName;
            student.MotherPersonalId = input.MotherPersonalId;
            student.Nutrition = input.Nutrition;
            student.Position = input.Position;
            student.DateOfBirth = input.DateOfBirth;
            student.EmployeeSon = input.EmployeeSon;

        }
        protected override IQueryable<Models.Students> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.Students> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => (x.FirstName + " " + x.LastName).Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.Students> GetEntityByIdAsync(int id)
        {
            var student = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(student);
        }
        protected override IQueryable<Models.Students> ApplySorting(IQueryable<Models.Students> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.FirstName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        [AbpAllowAnonymous]
        public async Task<PagedResultDto<StudentDto>> GetAllStudents(GdPagedResultRequestDto input)
        {
            PagedResultDto<StudentDto> ouput = new PagedResultDto<StudentDto>();
            IQueryable<Models.Students> query = query = from x in _studentRepository.GetAll()
                                                        select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _studentRepository.GetAll()
                        where (x.FirstName + " " + x.LastName).Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Students.Dto.StudentDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<PagedResultDto<StudentDto>> GetAllStudentsForNameOrFatherName(GdPagedResultRequestDto input)
        {
            PagedResultDto<StudentDto> ouput = new PagedResultDto<StudentDto>();
            IQueryable<Models.Students> query = query = from x in _studentRepository.GetAll()
                                                        select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                int enrollmentx = 0;
                try
                { enrollmentx = int.Parse(input.TextFilter); } catch (Exception) {}

                query = from x in _studentRepository.GetAll()
                        where ((x.FirstName + " " + x.LastName).Contains(input.TextFilter)
                          || (x.EnrollmentStudents.FirstOrDefault().Enrollments.FirstName + " " +
                          x.EnrollmentStudents.FirstOrDefault().Enrollments.LastName).Contains(input.TextFilter)
                          || x.EnrollmentStudents.FirstOrDefault().Enrollments.EnrollmentSequences.FirstOrDefault().Sequence == enrollmentx)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Students.Dto.StudentDto>>(query.ToList());
            Models.Students studentx = new Models.Students();
            foreach (var item in ouput.Items)
            {
                try
                {
                    studentx = query.ToList().Where(x => x.Id == item.Id).FirstOrDefault();
                    if (studentx != null)
                    {
                        var tutor = studentx.EnrollmentStudents?.FirstOrDefault()?.Enrollments;
                        if (tutor != null)
                        {
                            item.EnrollmentName = tutor.FirstName + " " + tutor.LastName;
                            item.EnrollmentId = tutor.Id;
                            var seqquence = tutor.EnrollmentSequences?.FirstOrDefault();
                            if (seqquence != null)
                                item.EnrollmentSequence = seqquence.Sequence;
                        }
                    }
                }
                catch (Exception err)
                {
                    string error = "Error en " + studentx.FirstName;
                }
            }

            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<PagedResultDto<StudentDto>> GetAllStudentForEnrollment(long enrollmentId, int period012)
        {
            var currentPeriod = await GetCurrentPeriod();

            if (period012 == 1)
            {
                currentPeriod = await GetPreviousPeriod();
            }

            if (period012 == 2)
            {
                currentPeriod = await GetNextPeriod();
            }


            if (currentPeriod == null)
                throw new Exception("ThereIsNoDefinedTheCurrentPeriodInTheSchoolPleaseContactTheAdministration");


            PagedResultDto<StudentDto> ouput = new PagedResultDto<StudentDto>();
            IQueryable<Models.Students> query = query = from x in _studentRepository.GetAll()
                                                        join x2 in _enrollmentStudentRepository.GetAll()
                                                        on x.Id equals x2.StudentId
                                                        where x2.EnrollmentId == enrollmentId && x2.PeriodId == currentPeriod.Id && x2.IsActive == true
                                                        select x;
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });

            ouput.Items = ObjectMapper.Map<List<Students.Dto.StudentDto>>(query.ToList());
            foreach (var item in ouput.Items)
            {
                item.EnrollmentStudents = item.EnrollmentStudents.Where(x => x.Periods.Id == currentPeriod.Id).ToList();
            }
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<StudentDto>> GetAllStudentsForCombo()
        {
            var studentList = new List<Models.Students>();

            if (AbpSession.TenantId.HasValue)
            {
                studentList = (from p in _studentRepository.GetAll() from x in p.EnrollmentStudents where x.TenantId == AbpSession.TenantId.Value && p.IsActive == true orderby p.LastName select p).ToList();
            }
            else
            {
                studentList = await _studentRepository.GetAllListAsync(x => x.IsActive == true);
            }
            var student = ObjectMapper.Map<List<StudentDto>>(studentList);

            return student;
        }
    }
};
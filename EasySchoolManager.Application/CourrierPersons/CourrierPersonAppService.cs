//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.CourrierPersons.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.CourrierPersons 
{ 
    [AbpAuthorize(PermissionNames.Pages_CourrierPersons)] 
    public class CourrierPersonAppService : AsyncCrudAppService<Models.CourrierPersons, CourrierPersonDto, int, PagedResultRequestDto, CreateCourrierPersonDto, UpdateCourrierPersonDto>, ICourrierPersonAppService 
    { 
        private readonly IRepository<Models.CourrierPersons, int> _courrierPersonRepository;
		


        public CourrierPersonAppService( 
            IRepository<Models.CourrierPersons, int> repository, 
            IRepository<Models.CourrierPersons, int> courrierPersonRepository 
            ) 
            : base(repository) 
        { 
            _courrierPersonRepository = courrierPersonRepository; 
			

			
        } 
        public override async Task<CourrierPersonDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<CourrierPersonDto> Create(CreateCourrierPersonDto input) 
        { 
            CheckCreatePermission(); 
            var courrierPerson = ObjectMapper.Map<Models.CourrierPersons>(input); 
			try{
              await _courrierPersonRepository.InsertAsync(courrierPerson); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(courrierPerson); 
        } 
        public override async Task<CourrierPersonDto> Update(UpdateCourrierPersonDto input) 
        { 
            CheckUpdatePermission(); 
            var courrierPerson = await _courrierPersonRepository.GetAsync(input.Id);
            MapToEntity(input, courrierPerson); 
		    try{
               await _courrierPersonRepository.UpdateAsync(courrierPerson); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var courrierPerson = await _courrierPersonRepository.GetAsync(input.Id); 
               await _courrierPersonRepository.DeleteAsync(courrierPerson);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.CourrierPersons MapToEntity(CreateCourrierPersonDto createInput) 
        { 
            var courrierPerson = ObjectMapper.Map<Models.CourrierPersons>(createInput); 
            return courrierPerson; 
        } 
        protected override void MapToEntity(UpdateCourrierPersonDto input, Models.CourrierPersons courrierPerson) 
        { 
            ObjectMapper.Map(input, courrierPerson); 
        } 
        protected override IQueryable<Models.CourrierPersons> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.CourrierPersons> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.CourrierPersons> GetEntityByIdAsync(int id) 
        { 
            var courrierPerson = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(courrierPerson); 
        } 
        protected override IQueryable<Models.CourrierPersons> ApplySorting(IQueryable<Models.CourrierPersons> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<CourrierPersonDto>> GetAllCourrierPersons(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<CourrierPersonDto> ouput = new PagedResultDto<CourrierPersonDto>(); 
            IQueryable<Models.CourrierPersons> query = query = from x in _courrierPersonRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _courrierPersonRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<CourrierPersons.Dto.CourrierPersonDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<CourrierPersonDto>> GetAllCourrierPersonsForCombo()
        {
            var courrierPersonList = await _courrierPersonRepository.GetAllListAsync(x => x.IsActive == true);

            var courrierPerson = ObjectMapper.Map<List<CourrierPersonDto>>(courrierPersonList.ToList());

            return courrierPerson;
        }
		
    } 
} ;
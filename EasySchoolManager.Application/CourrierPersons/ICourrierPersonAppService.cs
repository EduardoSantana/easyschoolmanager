//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.CourrierPersons.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.CourrierPersons
{
      public interface ICourrierPersonAppService : IAsyncCrudAppService<CourrierPersonDto, int, PagedResultRequestDto, CreateCourrierPersonDto, UpdateCourrierPersonDto>
      {
            Task<PagedResultDto<CourrierPersonDto>> GetAllCourrierPersons(GdPagedResultRequestDto input);
			Task<List<Dto.CourrierPersonDto>> GetAllCourrierPersonsForCombo();
      }
}
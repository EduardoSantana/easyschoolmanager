//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.CourrierPersons.Dto 
{
        [AutoMap(typeof(Models.CourrierPersons))] 
        public class CourrierPersonDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public String Name {get;set;} 
              public String FuntionDescription {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
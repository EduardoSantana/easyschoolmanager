//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Bloods.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Bloods
{
      public interface IBloodAppService : IAsyncCrudAppService<BloodDto, int, PagedResultRequestDto, CreateBloodDto, UpdateBloodDto>
      {
            Task<PagedResultDto<BloodDto>> GetAllBloods(GdPagedResultRequestDto input);
			Task<List<Dto.BloodDto>> GetAllBloodsForCombo();
      }
}
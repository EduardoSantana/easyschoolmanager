//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Bloods.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Bloods 
{ 
    [AbpAuthorize(PermissionNames.Pages_Bloods)] 
    public class BloodAppService : AsyncCrudAppService<Models.Bloods, BloodDto, int, PagedResultRequestDto, CreateBloodDto, UpdateBloodDto>, IBloodAppService 
    { 
        private readonly IRepository<Models.Bloods, int> _bloodRepository;
		


        public BloodAppService( 
            IRepository<Models.Bloods, int> repository, 
            IRepository<Models.Bloods, int> bloodRepository 
            ) 
            : base(repository) 
        { 
            _bloodRepository = bloodRepository; 
			

			
        } 
        public override async Task<BloodDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<BloodDto> Create(CreateBloodDto input) 
        { 
            CheckCreatePermission(); 
            var blood = ObjectMapper.Map<Models.Bloods>(input); 
			try{
              await _bloodRepository.InsertAsync(blood); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(blood); 
        } 
        public override async Task<BloodDto> Update(UpdateBloodDto input) 
        { 
            CheckUpdatePermission(); 
            var blood = await _bloodRepository.GetAsync(input.Id);
            MapToEntity(input, blood); 
		    try{
               await _bloodRepository.UpdateAsync(blood); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var blood = await _bloodRepository.GetAsync(input.Id); 
               await _bloodRepository.DeleteAsync(blood);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Bloods MapToEntity(CreateBloodDto createInput) 
        { 
            var blood = ObjectMapper.Map<Models.Bloods>(createInput); 
            return blood; 
        } 
        protected override void MapToEntity(UpdateBloodDto input, Models.Bloods blood) 
        { 
            ObjectMapper.Map(input, blood); 
        } 
        protected override IQueryable<Models.Bloods> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Bloods> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Bloods> GetEntityByIdAsync(int id) 
        { 
            var blood = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(blood); 
        } 
        protected override IQueryable<Models.Bloods> ApplySorting(IQueryable<Models.Bloods> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<BloodDto>> GetAllBloods(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<BloodDto> ouput = new PagedResultDto<BloodDto>(); 
            IQueryable<Models.Bloods> query = query = from x in _bloodRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _bloodRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Bloods.Dto.BloodDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<BloodDto>> GetAllBloodsForCombo()
        {
            var bloodList = await _bloodRepository.GetAllListAsync(x => x.IsActive == true);

            var blood = ObjectMapper.Map<List<BloodDto>>(bloodList.ToList());

            return blood;
        }
		
    } 
} ;
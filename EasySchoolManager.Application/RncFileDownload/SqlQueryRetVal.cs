﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.RncFileDownload
{
    public class SqlQueryRetVal
    {
        public bool IS_INSERT { get; set; }
        public int QTY_ROWS { get; set; }
    }
}

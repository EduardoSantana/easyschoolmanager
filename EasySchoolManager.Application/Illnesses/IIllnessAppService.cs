//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Illnesses.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Illnesses
{
      public interface IIllnessAppService : IAsyncCrudAppService<IllnessDto, int, PagedResultRequestDto, CreateIllnessDto, UpdateIllnessDto>
      {
            Task<PagedResultDto<IllnessDto>> GetAllIllnesses(GdPagedResultRequestDto input);
			Task<List<Dto.IllnessDto>> GetAllIllnessesForCombo();
      }
}
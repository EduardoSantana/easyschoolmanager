//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Illnesses.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Illnesses 
{ 
    [AbpAuthorize(PermissionNames.Pages_Illnesses)] 
    public class IllnessAppService : AsyncCrudAppService<Models.Illnesses, IllnessDto, int, PagedResultRequestDto, CreateIllnessDto, UpdateIllnessDto>, IIllnessAppService 
    { 
        private readonly IRepository<Models.Illnesses, int> _illnessRepository;
		


        public IllnessAppService( 
            IRepository<Models.Illnesses, int> repository, 
            IRepository<Models.Illnesses, int> illnessRepository 
            ) 
            : base(repository) 
        { 
            _illnessRepository = illnessRepository; 
			

			
        } 
        public override async Task<IllnessDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<IllnessDto> Create(CreateIllnessDto input) 
        { 
            CheckCreatePermission(); 
            var illness = ObjectMapper.Map<Models.Illnesses>(input); 
			try{
              await _illnessRepository.InsertAsync(illness); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(illness); 
        } 
        public override async Task<IllnessDto> Update(UpdateIllnessDto input) 
        { 
            CheckUpdatePermission(); 
            var illness = await _illnessRepository.GetAsync(input.Id);
            MapToEntity(input, illness); 
		    try{
               await _illnessRepository.UpdateAsync(illness); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var illness = await _illnessRepository.GetAsync(input.Id); 
               await _illnessRepository.DeleteAsync(illness);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Illnesses MapToEntity(CreateIllnessDto createInput) 
        { 
            var illness = ObjectMapper.Map<Models.Illnesses>(createInput); 
            return illness; 
        } 
        protected override void MapToEntity(UpdateIllnessDto input, Models.Illnesses illness) 
        { 
            ObjectMapper.Map(input, illness); 
        } 
        protected override IQueryable<Models.Illnesses> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Illnesses> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Illnesses> GetEntityByIdAsync(int id) 
        { 
            var illness = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(illness); 
        } 
        protected override IQueryable<Models.Illnesses> ApplySorting(IQueryable<Models.Illnesses> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<IllnessDto>> GetAllIllnesses(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<IllnessDto> ouput = new PagedResultDto<IllnessDto>(); 
            IQueryable<Models.Illnesses> query = query = from x in _illnessRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _illnessRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Illnesses.Dto.IllnessDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<IllnessDto>> GetAllIllnessesForCombo()
        {
            var illnessList = await _illnessRepository.GetAllListAsync(x => x.IsActive == true);

            var illness = ObjectMapper.Map<List<IllnessDto>>(illnessList.ToList());

            return illness;
        }
		
    } 
} ;
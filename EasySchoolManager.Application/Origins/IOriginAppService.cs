//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Origins.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Origins
{
      public interface IOriginAppService : IAsyncCrudAppService<OriginDto, int, PagedResultRequestDto, CreateOriginDto, UpdateOriginDto>
      {
            Task<PagedResultDto<OriginDto>> GetAllOrigins(GdPagedResultRequestDto input);
			Task<List<Dto.OriginDto>> GetAllOriginsForCombo();
      }
}
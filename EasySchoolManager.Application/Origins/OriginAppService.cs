//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Origins.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Origins 
{ 
    [AbpAuthorize(PermissionNames.Pages_Origins)] 
    public class OriginAppService : AsyncCrudAppService<Models.Origins, OriginDto, int, PagedResultRequestDto, CreateOriginDto, UpdateOriginDto>, IOriginAppService 
    { 
        private readonly IRepository<Models.Origins, int> _originRepository;
		


        public OriginAppService( 
            IRepository<Models.Origins, int> repository, 
            IRepository<Models.Origins, int> originRepository 
            ) 
            : base(repository) 
        { 
            _originRepository = originRepository; 
			

			
        } 
        public override async Task<OriginDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<OriginDto> Create(CreateOriginDto input) 
        { 
            CheckCreatePermission(); 
            var origin = ObjectMapper.Map<Models.Origins>(input); 
			try{
              await _originRepository.InsertAsync(origin); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(origin); 
        } 
        public override async Task<OriginDto> Update(UpdateOriginDto input) 
        { 
            CheckUpdatePermission(); 
            var origin = await _originRepository.GetAsync(input.Id);
            MapToEntity(input, origin); 
		    try{
               await _originRepository.UpdateAsync(origin); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var origin = await _originRepository.GetAsync(input.Id); 
               await _originRepository.DeleteAsync(origin);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Origins MapToEntity(CreateOriginDto createInput) 
        { 
            var origin = ObjectMapper.Map<Models.Origins>(createInput); 
            return origin; 
        } 
        protected override void MapToEntity(UpdateOriginDto input, Models.Origins origin) 
        { 
            ObjectMapper.Map(input, origin); 
        } 
        protected override IQueryable<Models.Origins> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Origins> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Origins> GetEntityByIdAsync(int id) 
        { 
            var origin = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(origin); 
        } 
        protected override IQueryable<Models.Origins> ApplySorting(IQueryable<Models.Origins> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<OriginDto>> GetAllOrigins(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<OriginDto> ouput = new PagedResultDto<OriginDto>(); 
            IQueryable<Models.Origins> query = query = from x in _originRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _originRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Origins.Dto.OriginDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<OriginDto>> GetAllOriginsForCombo()
        {
            var originList = await _originRepository.GetAllListAsync(x => x.IsActive == true);

            var origin = ObjectMapper.Map<List<OriginDto>>(originList.ToList());

            return origin;
        }
		
    } 
} ;
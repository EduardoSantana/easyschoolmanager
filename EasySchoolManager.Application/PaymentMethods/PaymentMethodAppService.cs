//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.PaymentMethods.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.PaymentMethods 
{ 
    [AbpAuthorize(PermissionNames.Pages_PaymentMethods)] 
    public class PaymentMethodAppService : AsyncCrudAppService<Models.PaymentMethods, PaymentMethodDto, int, PagedResultRequestDto, CreatePaymentMethodDto, UpdatePaymentMethodDto>, IPaymentMethodAppService 
    { 
        private readonly IRepository<Models.PaymentMethods, int> _paymentMethodRepository;
		


        public PaymentMethodAppService( 
            IRepository<Models.PaymentMethods, int> repository, 
            IRepository<Models.PaymentMethods, int> paymentMethodRepository 
            ) 
            : base(repository) 
        { 
            _paymentMethodRepository = paymentMethodRepository; 
			

			
        } 
        public override async Task<PaymentMethodDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<PaymentMethodDto> Create(CreatePaymentMethodDto input) 
        { 
            CheckCreatePermission(); 
            var paymentMethod = ObjectMapper.Map<Models.PaymentMethods>(input); 
			try{
              await _paymentMethodRepository.InsertAsync(paymentMethod); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(paymentMethod); 
        } 
        public override async Task<PaymentMethodDto> Update(UpdatePaymentMethodDto input) 
        { 
            CheckUpdatePermission(); 
            var paymentMethod = await _paymentMethodRepository.GetAsync(input.Id);
            MapToEntity(input, paymentMethod); 
		    try{
               await _paymentMethodRepository.UpdateAsync(paymentMethod); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var paymentMethod = await _paymentMethodRepository.GetAsync(input.Id); 
               await _paymentMethodRepository.DeleteAsync(paymentMethod);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.PaymentMethods MapToEntity(CreatePaymentMethodDto createInput) 
        { 
            var paymentMethod = ObjectMapper.Map<Models.PaymentMethods>(createInput); 
            return paymentMethod; 
        } 
        protected override void MapToEntity(UpdatePaymentMethodDto input, Models.PaymentMethods paymentMethod) 
        { 
            ObjectMapper.Map(input, paymentMethod); 
        } 
        protected override IQueryable<Models.PaymentMethods> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.PaymentMethods> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.PaymentMethods> GetEntityByIdAsync(int id) 
        { 
            var paymentMethod = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(paymentMethod); 
        } 
        protected override IQueryable<Models.PaymentMethods> ApplySorting(IQueryable<Models.PaymentMethods> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<PaymentMethodDto>> GetAllPaymentMethods(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<PaymentMethodDto> ouput = new PagedResultDto<PaymentMethodDto>(); 
            IQueryable<Models.PaymentMethods> query = query = from x in _paymentMethodRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _paymentMethodRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<PaymentMethods.Dto.PaymentMethodDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<PaymentMethodDto>> GetAllPaymentMethodsForCombo()
        {
            var paymentMethodList = await _paymentMethodRepository.GetAllListAsync(x => x.IsActive == true);

            var paymentMethod = ObjectMapper.Map<List<PaymentMethodDto>>(paymentMethodList.ToList());

            return paymentMethod;
        }
		
    } 
} ;
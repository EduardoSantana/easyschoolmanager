//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.PaymentMethods.Dto 
{
        [AutoMap(typeof(Models.PaymentMethods))] 
        public class PaymentMethodDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public string Name {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
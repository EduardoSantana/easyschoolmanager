//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.PaymentMethods.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.PaymentMethods
{
      public interface IPaymentMethodAppService : IAsyncCrudAppService<PaymentMethodDto, int, PagedResultRequestDto, CreatePaymentMethodDto, UpdatePaymentMethodDto>
      {
            Task<PagedResultDto<PaymentMethodDto>> GetAllPaymentMethods(GdPagedResultRequestDto input);
			Task<List<Dto.PaymentMethodDto>> GetAllPaymentMethodsForCombo();
      }
}
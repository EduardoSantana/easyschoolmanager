//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.DiscountNames.Dto 
{
        [AutoMap(typeof(Models.DiscountNames))] 
        public class CreateDiscountNameDto : EntityDto<int> 
        {

              public string Name {get;set;} 

              public bool HasGrantEnterprise {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
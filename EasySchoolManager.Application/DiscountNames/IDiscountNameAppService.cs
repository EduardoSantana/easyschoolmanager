//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.DiscountNames.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.DiscountNames
{
      public interface IDiscountNameAppService : IAsyncCrudAppService<DiscountNameDto, int, PagedResultRequestDto, CreateDiscountNameDto, UpdateDiscountNameDto>
      {
            Task<PagedResultDto<DiscountNameDto>> GetAllDiscountNames(GdPagedResultRequestDto input);
			Task<List<Dto.DiscountNameDto>> GetAllDiscountNamesForCombo();
      }
}
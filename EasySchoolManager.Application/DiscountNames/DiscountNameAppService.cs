//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.DiscountNames.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.DiscountNames
{
    [AbpAuthorize(PermissionNames.Pages_DiscountNames)]
    public class DiscountNameAppService : AsyncCrudAppService<Models.DiscountNames, DiscountNameDto, int, PagedResultRequestDto, CreateDiscountNameDto, UpdateDiscountNameDto>, IDiscountNameAppService
    {
        private readonly IRepository<Models.DiscountNames, int> _discountNameRepository;

        private readonly IRepository<Models.DiscountNameTenants, int> _discountNameTenantRepository;



        public DiscountNameAppService(
            IRepository<Models.DiscountNames, int> repository,
            IRepository<Models.DiscountNames, int> discountNameRepository,
            IRepository<Models.DiscountNameTenants, int> discountNameTenantRepository
            )
            : base(repository)
        {
            _discountNameRepository = discountNameRepository;
            _discountNameTenantRepository = discountNameTenantRepository;

        }
        public override async Task<DiscountNameDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<DiscountNameDto> Create(CreateDiscountNameDto input)
        {
            CheckCreatePermission();
            var discountName = ObjectMapper.Map<Models.DiscountNames>(input);
            try
            {
                await _discountNameRepository.InsertAsync(discountName);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(discountName);
        }
        public override async Task<DiscountNameDto> Update(UpdateDiscountNameDto input)
        {
            CheckUpdatePermission();
            var discountName = await _discountNameRepository.GetAsync(input.Id);
            MapToEntity(input, discountName);
            try
            {
                await _discountNameRepository.UpdateAsync(discountName);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var discountName = await _discountNameRepository.GetAsync(input.Id);
                await _discountNameRepository.DeleteAsync(discountName);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.DiscountNames MapToEntity(CreateDiscountNameDto createInput)
        {
            var discountName = ObjectMapper.Map<Models.DiscountNames>(createInput);
            return discountName;
        }
        protected override void MapToEntity(UpdateDiscountNameDto input, Models.DiscountNames discountName)
        {
            ObjectMapper.Map(input, discountName);
        }
        protected override IQueryable<Models.DiscountNames> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.DiscountNames> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.DiscountNames> GetEntityByIdAsync(int id)
        {
            var discountName = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(discountName);
        }
        protected override IQueryable<Models.DiscountNames> ApplySorting(IQueryable<Models.DiscountNames> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Name);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<DiscountNameDto>> GetAllDiscountNames(GdPagedResultRequestDto input)
        {
            PagedResultDto<DiscountNameDto> ouput = new PagedResultDto<DiscountNameDto>();
            IQueryable<Models.DiscountNames> query = query = from x in _discountNameRepository.GetAll()
                                                             select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _discountNameRepository.GetAll()
                        where x.Name.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<DiscountNames.Dto.DiscountNameDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<DiscountNameDto>> GetAllDiscountNamesForCombo()
        {
            var discountNameList = await _discountNameRepository.GetAllListAsync(x => x.IsActive == true
              && (
                        (x.PeriodDiscounts.FirstOrDefault().DiscountNameTenants.Count(y=> x.IsActive == true) == 0 ) || 
                        x.PeriodDiscounts.FirstOrDefault().DiscountNameTenants.Any(z=> z.TenantId == AbpSession.TenantId.Value && x.IsActive == true) 
                  )
              );
            
            var discountName = ObjectMapper.Map<List<DiscountNameDto>>(discountNameList.ToList());

            return discountName;
        }

    }
};
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.InventoryShops.Dto 
{
        [AutoMap(typeof(Models.InventoryShops))] 
        public class InventoryShopDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public int ArticleShopId {get;set;} 
              public long Existence {get;set;} 
              public Decimal Price {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 
              public string ArticleShops_Description {get;set;}
              public string ArticleShops_Sequence {get; set;}

    }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.InventoryShops.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.ArticleShops.Dto;

namespace EasySchoolManager.InventoryShops 
{ 
    [AbpAuthorize(PermissionNames.Pages_InventoryShops)] 
    public class InventoryShopAppService : AsyncCrudAppService<Models.InventoryShops, InventoryShopDto, int, PagedResultRequestDto, CreateInventoryShopDto, UpdateInventoryShopDto>, IInventoryShopAppService 
    { 
        private readonly IRepository<Models.InventoryShops, int> _inventoryShopRepository;
		
		    private readonly IRepository<Models.ArticleShops, int> _articleShopRepository;


        public InventoryShopAppService( 
            IRepository<Models.InventoryShops, int> repository, 
            IRepository<Models.InventoryShops, int> inventoryShopRepository ,
            IRepository<Models.ArticleShops, int> articleShopRepository

            ) 
            : base(repository) 
        { 
            _inventoryShopRepository = inventoryShopRepository; 
			
            _articleShopRepository = articleShopRepository;


			
        }

        //este
        public async Task<InventoryShopDto> GetPrice(int art)
        {
            var query = await _inventoryShopRepository.GetAllListAsync(x => x.ArticleShopId == art);

            var res = query.FirstOrDefault();
            var re = AutoMapper.Mapper.Map<InventoryShopDto>(res);
            return re;
        }

        public async Task<InventoryShopDto> GetPriceSeq(int seq)
        {
            var query = await _inventoryShopRepository.GetAllListAsync(x => x.ArticleShops.Sequence == seq);

            var res = query.FirstOrDefault();
            var re = AutoMapper.Mapper.Map<InventoryShopDto>(res);
            return re;
        }


        public override async Task<InventoryShopDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<InventoryShopDto> Create(CreateInventoryShopDto input) 
        { 
            CheckCreatePermission(); 
            var inventoryShop = ObjectMapper.Map<Models.InventoryShops>(input); 
			try{
              await _inventoryShopRepository.InsertAsync(inventoryShop); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(inventoryShop); 
        } 
        public override async Task<InventoryShopDto> Update(UpdateInventoryShopDto input) 
        { 
            CheckUpdatePermission(); 
            var inventoryShop = await _inventoryShopRepository.GetAsync(input.Id);
            MapToEntity(input, inventoryShop); 
		    try{
               await _inventoryShopRepository.UpdateAsync(inventoryShop); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var inventoryShop = await _inventoryShopRepository.GetAsync(input.Id); 
               await _inventoryShopRepository.DeleteAsync(inventoryShop);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.InventoryShops MapToEntity(CreateInventoryShopDto createInput) 
        { 
            var inventoryShop = ObjectMapper.Map<Models.InventoryShops>(createInput); 
            return inventoryShop; 
        } 
        protected override void MapToEntity(UpdateInventoryShopDto input, Models.InventoryShops inventoryShop) 
        { 
            ObjectMapper.Map(input, inventoryShop); 
        } 
        protected override IQueryable<Models.InventoryShops> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.InventoryShops> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.ArticleShops.Description.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.InventoryShops> GetEntityByIdAsync(int id) 
        { 
            var inventoryShop = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(inventoryShop); 
        } 
        protected override IQueryable<Models.InventoryShops> ApplySorting(IQueryable<Models.InventoryShops> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.ArticleShops.Description); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<InventoryShopDto>> GetAllInventoryShops(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<InventoryShopDto> ouput = new PagedResultDto<InventoryShopDto>(); 
            IQueryable<Models.InventoryShops> query = query = from x in _inventoryShopRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _inventoryShopRepository.GetAll() 
                        where x.ArticleShops.Description.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<InventoryShops.Dto.InventoryShopDto>>(query.ToList()); 
            return ouput; 
        } 


		
		[AbpAllowAnonymous]
		public async Task<List<InventoryShopDto>> GetAllInventoryShopsForCombo()
        {
            var inventoryShopList = await _inventoryShopRepository.GetAllListAsync(x => x.IsActive == true);

            var inventoryShop = ObjectMapper.Map<List<InventoryShopDto>>(inventoryShopList.ToList());

            return inventoryShop;
        }
		
    } 
} ;
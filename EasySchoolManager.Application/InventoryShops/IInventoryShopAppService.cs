//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.InventoryShops.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.InventoryShops
{
      public interface IInventoryShopAppService : IAsyncCrudAppService<InventoryShopDto, int, PagedResultRequestDto, CreateInventoryShopDto, UpdateInventoryShopDto>
      {
            Task<PagedResultDto<InventoryShopDto>> GetAllInventoryShops(GdPagedResultRequestDto input);
			Task<List<Dto.InventoryShopDto>> GetAllInventoryShopsForCombo();
            Task<InventoryShopDto> GetPrice(int art);
            Task<InventoryShopDto> GetPriceSeq(int seque);
    }
}
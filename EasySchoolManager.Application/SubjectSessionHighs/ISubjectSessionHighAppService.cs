//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.SubjectSessionHighs.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.SubjectSessionHighs
{
      public interface ISubjectSessionHighAppService : IAsyncCrudAppService<SubjectSessionHighDto, int, PagedResultRequestDto, CreateSubjectSessionHighDto, UpdateSubjectSessionHighDto>
      {
            Task<PagedResultDto<SubjectSessionHighDto>> GetAllSubjectSessionHighs(GdPagedResultRequestDto input);
			Task<List<Dto.SubjectSessionHighDto>> GetAllSubjectSessionHighsForCombo();
      }
}
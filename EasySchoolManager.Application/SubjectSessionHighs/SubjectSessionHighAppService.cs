//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.SubjectSessionHighs.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.SubjectSessionHighs 
{ 
	[AbpAuthorize(PermissionNames.Pages_SubjectSessionHighs)] 
	public class SubjectSessionHighAppService : AsyncCrudAppService<Models.SubjectSessionHighs, SubjectSessionHighDto, int, PagedResultRequestDto, CreateSubjectSessionHighDto, UpdateSubjectSessionHighDto>, ISubjectSessionHighAppService 
	{ 
		private readonly IRepository<Models.SubjectSessionHighs, int> _subjectSessionHighRepository;
		
			private readonly IRepository<Models.SubjectHighs, int> _subjectHighRepository;
			private readonly IRepository<Models.TeacherTenants, int> _teacherTenantRepository;
			private readonly IRepository<Models.CourseSessions, int> _courseSessionRepository;
			private readonly IRepository<Models.Periods, int> _periodRepository;


		public SubjectSessionHighAppService( 
			IRepository<Models.SubjectSessionHighs, int> repository, 
			IRepository<Models.SubjectSessionHighs, int> subjectSessionHighRepository ,
			IRepository<Models.SubjectHighs, int> subjectHighRepository
,
			IRepository<Models.TeacherTenants, int> teacherTenantRepository
,
			IRepository<Models.CourseSessions, int> courseSessionRepository
,
			IRepository<Models.Periods, int> periodRepository

			) 
			: base(repository) 
		{ 
			_subjectSessionHighRepository = subjectSessionHighRepository; 
			
			_subjectHighRepository = subjectHighRepository;

			_teacherTenantRepository = teacherTenantRepository;

			_courseSessionRepository = courseSessionRepository;

			_periodRepository = periodRepository;


			
		} 
		public override async Task<SubjectSessionHighDto> Get(EntityDto<int> input) 
		{ 
			var user = await base.Get(input); 
			return user; 
		} 
		public override async Task<SubjectSessionHighDto> Create(CreateSubjectSessionHighDto input) 
		{ 
			CheckCreatePermission(); 
			var subjectSessionHigh = ObjectMapper.Map<Models.SubjectSessionHighs>(input); 
			try{
			  await _subjectSessionHighRepository.InsertAsync(subjectSessionHigh); 
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}
			return MapToEntityDto(subjectSessionHigh); 
		} 
		public override async Task<SubjectSessionHighDto> Update(UpdateSubjectSessionHighDto input) 
		{ 
			CheckUpdatePermission(); 
			var subjectSessionHigh = await _subjectSessionHighRepository.GetAsync(input.Id);
			MapToEntity(input, subjectSessionHigh); 
			try{
			   await _subjectSessionHighRepository.UpdateAsync(subjectSessionHigh); 
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}
			return await Get(input); 
		} 
		public override async Task Delete(EntityDto<int> input) 
		{
			try{
			   var subjectSessionHigh = await _subjectSessionHighRepository.GetAsync(input.Id); 
			   await _subjectSessionHighRepository.DeleteAsync(subjectSessionHigh);
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}			
		} 
		protected override Models.SubjectSessionHighs MapToEntity(CreateSubjectSessionHighDto createInput) 
		{ 
			var subjectSessionHigh = ObjectMapper.Map<Models.SubjectSessionHighs>(createInput); 
			return subjectSessionHigh; 
		} 
		protected override void MapToEntity(UpdateSubjectSessionHighDto input, Models.SubjectSessionHighs subjectSessionHigh) 
		{ 
			ObjectMapper.Map(input, subjectSessionHigh); 
		} 
		protected override IQueryable<Models.SubjectSessionHighs> CreateFilteredQuery(PagedResultRequestDto input) 
		{ 
			return Repository.GetAll(); 
		} 
		protected  IQueryable<Models.SubjectSessionHighs> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
		{ 
			var resultado =  Repository.GetAll(); 
			//if (!string.IsNullOrEmpty(input.TextFilter)) 
			//    resultado = resultado.Where(x => x.TeacherTenants.Name.Contains(input.TextFilter)); 
			return resultado; 
		} 
		protected override async Task<Models.SubjectSessionHighs> GetEntityByIdAsync(int id) 
		{ 
			var subjectSessionHigh = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
			return await Task.FromResult(subjectSessionHigh); 
		} 
		protected override IQueryable<Models.SubjectSessionHighs> ApplySorting(IQueryable<Models.SubjectSessionHighs> query, PagedResultRequestDto input) 
		{ 
			return query; 
		} 
 
		protected virtual void CheckErrors(IdentityResult identityResult) 
		{ 
			identityResult.CheckErrors(LocalizationManager); 
		} 
		
		public async Task<PagedResultDto<SubjectSessionHighDto>> GetAllSubjectSessionHighs(GdPagedResultRequestDto input) 
		{ 
			PagedResultDto<SubjectSessionHighDto> ouput = new PagedResultDto<SubjectSessionHighDto>(); 
			IQueryable<Models.SubjectSessionHighs> query = query = from x in _subjectSessionHighRepository.GetAll() 
													   select x; 
			if (!string.IsNullOrEmpty(input.TextFilter)) 
			{ 
				//query = from x in _subjectSessionHighRepository.GetAll() 
				//        where x.Name.Contains(input.TextFilter) 
				//        select x; 
			} 
			ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
			if (input.SkipCount > 0) 
			{ 
				query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
			}  
			if (input.MaxResultCount > 0) 
			{ 
				query = query.Take(input.MaxResultCount); 
			} 
			ouput.Items = ObjectMapper.Map<List<SubjectSessionHighs.Dto.SubjectSessionHighDto>>(query.ToList()); 
			return ouput; 
		} 
		
		[AbpAllowAnonymous]
		public async Task<List<SubjectSessionHighDto>> GetAllSubjectSessionHighsForCombo()
		{
			var subjectSessionHighList = await _subjectSessionHighRepository.GetAllListAsync(x => x.IsActive == true);

			var subjectSessionHigh = ObjectMapper.Map<List<SubjectSessionHighDto>>(subjectSessionHighList.ToList());

			return subjectSessionHigh;
		}
		
	} 
} ;
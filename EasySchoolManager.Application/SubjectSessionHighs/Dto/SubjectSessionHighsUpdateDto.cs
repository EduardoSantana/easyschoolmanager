//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.SubjectSessionHighs.Dto 
{
        [AutoMap(typeof(Models.SubjectSessionHighs))] 
        public class UpdateSubjectSessionHighDto : EntityDto<int> 
        {

              public int SubjectHighId {get;set;} 

              public int TeacherTenantId {get;set;} 

              public int CourseSessionId {get;set;} 

              public int PeriodId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
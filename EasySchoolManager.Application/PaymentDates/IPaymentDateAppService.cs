//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.PaymentDates.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.PaymentDates
{
    public interface IPaymentDateAppService : IAsyncCrudAppService<PaymentDateDto, int, PagedResultRequestDto, CreatePaymentDateDto, UpdatePaymentDateDto>
    {
        Task<PagedResultDto<PaymentDateDto>> GetAllPaymentDates(GdPagedResultRequestDto input);
        PagedResultDto<PaymentDateDto> GetAllPaymentsDates(GdPagedResultRequestDto input);
        Task<List<Dto.PaymentDateDto>> GetAllPaymentDatesForCombo();
        Task GeneratePaymentDateList(GeneratePaymentDateListInput input);
        //Task GetAllPaymentDates(object pagere);
    }
}
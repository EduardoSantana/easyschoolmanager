//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.PaymentDates.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.MultiTenancy;
using Abp.Domain.Uow;

namespace EasySchoolManager.PaymentDates
{
    [AbpAuthorize(PermissionNames.Pages_PaymentDates)]
    public class PaymentDateAppService : AsyncCrudAppService<Models.PaymentDates, PaymentDateDto, int, PagedResultRequestDto, CreatePaymentDateDto, UpdatePaymentDateDto>, IPaymentDateAppService
    {
        private readonly IRepository<Models.PaymentDates, int> _paymentDateRepository;
        private readonly IRepository<Models.Periods, int> _periodRepository;
        private readonly TenantManager _tenantManager;



        public PaymentDateAppService(
            IRepository<Models.PaymentDates, int> repository,
            IRepository<Models.PaymentDates, int> paymentDateRepository,
            TenantManager tenantManager,
            IRepository<Models.Periods, int> periodRepository
            )
            : base(repository)
        {
            _paymentDateRepository = paymentDateRepository;
            _tenantManager = tenantManager;
            _periodRepository = periodRepository;
        }
        public override async Task<PaymentDateDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<PaymentDateDto> Create(CreatePaymentDateDto input)
        {
            CheckCreatePermission();
            var paymentDate = ObjectMapper.Map<Models.PaymentDates>(input);
            try
            {
                await _paymentDateRepository.InsertAsync(paymentDate);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(paymentDate);
        }
        public override async Task<PaymentDateDto> Update(UpdatePaymentDateDto input)
        {
            CheckUpdatePermission();
            var paymentDate = await _paymentDateRepository.GetAsync(input.Id);
            MapToEntity(input, paymentDate);
            try
            {
                await _paymentDateRepository.UpdateAsync(paymentDate);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var paymentDate = await _paymentDateRepository.GetAsync(input.Id);
                await _paymentDateRepository.DeleteAsync(paymentDate);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.PaymentDates MapToEntity(CreatePaymentDateDto createInput)
        {
            var paymentDate = ObjectMapper.Map<Models.PaymentDates>(createInput);
            return paymentDate;
        }
        protected override void MapToEntity(UpdatePaymentDateDto input, Models.PaymentDates paymentDate)
        {
            ObjectMapper.Map(input, paymentDate);
        }
        protected override IQueryable<Models.PaymentDates> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.PaymentDates> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.PaymentDay.ToString().Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.PaymentDates> GetEntityByIdAsync(int id)
        {
            var paymentDate = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(paymentDate);
        }
        protected override IQueryable<Models.PaymentDates> ApplySorting(IQueryable<Models.PaymentDates> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.PaymentDay.ToString());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
        
        public async Task<PagedResultDto<PaymentDateDto>> GetAllPaymentDates(GdPagedResultRequestDto input)
        {
            PagedResultDto<PaymentDateDto> ouput = new PagedResultDto<PaymentDateDto>();
            IQueryable<Models.PaymentDates> query = query = from x in _paymentDateRepository.GetAll()
                                                            select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _paymentDateRepository.GetAll()
                        where x.PaymentDay.ToString().Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<PaymentDates.Dto.PaymentDateDto>>(query.ToList());
            return ouput;
        }

        public PagedResultDto<PaymentDateDto> GetAllPaymentsDates(GdPagedResultRequestDto input)
        {
            PagedResultDto<PaymentDateDto> ouput = new PagedResultDto<PaymentDateDto>();
            IQueryable<Models.PaymentDates> query = query = from x in _paymentDateRepository.GetAll()
                                                            select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _paymentDateRepository.GetAll()
                        where x.PaymentDay.ToString().Contains(input.TextFilter)
                        select x;
            }
            
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<PaymentDates.Dto.PaymentDateDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<PaymentDateDto>> GetAllPaymentDatesForCombo()
        {
            var paymentDateList = await _paymentDateRepository.GetAllListAsync(x => x.IsActive == true);

            var paymentDate = ObjectMapper.Map<List<PaymentDateDto>>(paymentDateList.ToList());

            return paymentDate;
        }

        public async Task GeneratePaymentDateList(GeneratePaymentDateListInput input)
        {

            var currentTenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);
            var currentPeriodId = currentTenant.PeriodId;
            Models.Periods currentPeriod = null;
            if (!currentPeriodId.HasValue || currentPeriodId.Value == 0)
            {
                throw new UserFriendlyException(L("YouMustSetThePeriodInTheSchoolConfigurationToBeAbleToDoThisProcess"));
            }
            try
            {
                currentPeriod = _periodRepository.Get(currentPeriodId.Value);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(L("YouMustSetThePeriodInTheSchoolConfigurationToBeAbleToDoThisProcess"));
            }

            var initialDate = currentPeriod.StartDate;
            var initialMonth = input.InitialMonth;
            var AllPriorPaymentDates = await _paymentDateRepository.GetAllListAsync();
            if (AllPriorPaymentDates.Count > 0)
            {
                foreach (var item in AllPriorPaymentDates)
                {
                    _paymentDateRepository.Delete(item);
                    CurrentUnitOfWork.SaveChanges();
                }
            }
            var yearUsed = input.InitialYear;
            Models.PaymentDates pd = new Models.PaymentDates();
            pd.Sequence = 0;
            pd.PaymentMonth = initialMonth;
            pd.PaymentDay = 1;
            pd.PaymentYear = yearUsed;
            pd.IsActive = true;

            _paymentDateRepository.Insert(pd);

            for (int i = 0; i < input.NumberOfPayments; i++)
            {
                Models.PaymentDates cpd = new Models.PaymentDates();
                if (initialMonth == 12)
                    cpd.PaymentYear = ++yearUsed;
                else
                    cpd.PaymentYear = yearUsed;
                cpd.Sequence = i + 1;
                cpd.PaymentMonth = (initialMonth++)%12 + 1;
                cpd.PaymentDay = input.PaymentDay;
                cpd.IsActive = true;

                _paymentDateRepository.Insert(cpd);
                CurrentUnitOfWork.SaveChanges();
            }
        }

    }
};
﻿//Created from Templaste MG

namespace EasySchoolManager.PaymentDates.Dto
{
    public class GeneratePaymentDateListInput
    {
        public int NumberOfPayments { get; set; }
        public int PaymentDay { get;set; }
        public int InitialMonth{ get;set; }
        public int InitialYear { get;set; }
    }
}
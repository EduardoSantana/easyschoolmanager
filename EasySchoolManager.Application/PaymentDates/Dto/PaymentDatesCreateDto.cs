//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.PaymentDates.Dto
{
    [AutoMap(typeof(Models.PaymentDates))]
    public class CreatePaymentDateDto : EntityDto<int>
    {

        public int Sequence { get; set; }

        public int PaymentDay { get; set; }

        public int PaymentMonth { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        public int InitialMonth { get; set; }
        public int InitialYear { get; set; }

    }
}
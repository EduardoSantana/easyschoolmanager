//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Cities.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Cities
{
      public interface ICityAppService : IAsyncCrudAppService<CityDto, int, PagedResultRequestDto, CreateCityDto, UpdateCityDto>
      {
            Task<PagedResultDto<CityDto>> GetAllCities(GdPagedResultRequestDto input);
			Task<List<Dto.CityDto>> GetAllCitiesForCombo();
			Task<List<Dto.CityDto>> GetAllCitiesForComboByProvinceId(getAllCitiesForComboByProvinceIdinputDto input);
      }
}
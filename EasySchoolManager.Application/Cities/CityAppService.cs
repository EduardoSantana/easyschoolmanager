//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Cities.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Cities 
{ 
    [AbpAuthorize(PermissionNames.Pages_Cities)] 
    public class CityAppService : AsyncCrudAppService<Models.Cities, CityDto, int, PagedResultRequestDto, CreateCityDto, UpdateCityDto>, ICityAppService 
    { 
        private readonly IRepository<Models.Cities, int> _cityRepository;
		


        public CityAppService( 
            IRepository<Models.Cities, int> repository, 
            IRepository<Models.Cities, int> cityRepository 
            ) 
            : base(repository) 
        { 
            _cityRepository = cityRepository; 
			

			
        } 
        public override async Task<CityDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<CityDto> Create(CreateCityDto input) 
        { 
            CheckCreatePermission(); 
            var city = ObjectMapper.Map<Models.Cities>(input); 
			try{
              await _cityRepository.InsertAsync(city); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(city); 
        } 
        public override async Task<CityDto> Update(UpdateCityDto input) 
        { 
            CheckUpdatePermission(); 
            var city = await _cityRepository.GetAsync(input.Id);
            MapToEntity(input, city); 
		    try{
               await _cityRepository.UpdateAsync(city); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var city = await _cityRepository.GetAsync(input.Id); 
               await _cityRepository.DeleteAsync(city);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Cities MapToEntity(CreateCityDto createInput) 
        { 
            var city = ObjectMapper.Map<Models.Cities>(createInput); 
            return city; 
        } 
        protected override void MapToEntity(UpdateCityDto input, Models.Cities city) 
        { 
            ObjectMapper.Map(input, city); 
        } 
        protected override IQueryable<Models.Cities> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Cities> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Cities> GetEntityByIdAsync(int id) 
        { 
            var city = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(city); 
        } 
        protected override IQueryable<Models.Cities> ApplySorting(IQueryable<Models.Cities> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<CityDto>> GetAllCities(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<CityDto> ouput = new PagedResultDto<CityDto>(); 
            IQueryable<Models.Cities> query = query = from x in _cityRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _cityRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Cities.Dto.CityDto>>(query.ToList()); 
            return ouput; 
        }
        [AbpAllowAnonymous]
        public async Task<List<CityDto>> GetAllCitiesForCombo()
        {
            var cityList = await _cityRepository.GetAllListAsync(x => x.IsActive == true);

            var city = ObjectMapper.Map<List<CityDto>>(cityList.ToList());

            return city;
        }
        [AbpAllowAnonymous]
        public async Task<List<Dto.CityDto>> GetAllCitiesForComboByProvinceId(getAllCitiesForComboByProvinceIdinputDto input)
        {
            var cityList = await _cityRepository.GetAllListAsync(x => x.IsActive == true && x.ProvinceId == input.ProvinceId);

            var city = ObjectMapper.Map<List<CityDto>>(cityList.ToList());

            return city;
        }
		
    } 
} ;
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Cities.Dto 
{
        [AutoMap(typeof(Models.Cities))] 
        public class CityDto : EntityDto<int> 
        {
              public int ProvinceId {get;set;} 
              public string Name {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 
              public String Provinces_Name { get; set; }

         }
}
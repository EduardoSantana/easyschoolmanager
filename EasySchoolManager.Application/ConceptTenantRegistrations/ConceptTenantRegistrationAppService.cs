//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.ConceptTenantRegistrations.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ConceptTenantRegistrations 
{ 
    [AbpAuthorize(PermissionNames.Pages_ConceptTenantRegistrations)] 
    public class ConceptTenantRegistrationAppService : AsyncCrudAppService<Models.ConceptTenantRegistrations, ConceptTenantRegistrationDto, int, PagedResultRequestDto, CreateConceptTenantRegistrationDto, UpdateConceptTenantRegistrationDto>, IConceptTenantRegistrationAppService 
    { 
        private readonly IRepository<Models.ConceptTenantRegistrations, int> _conceptTenantRegistrationRepository;
		


        public ConceptTenantRegistrationAppService( 
            IRepository<Models.ConceptTenantRegistrations, int> repository, 
            IRepository<Models.ConceptTenantRegistrations, int> conceptTenantRegistrationRepository 
            ) 
            : base(repository) 
        { 
            _conceptTenantRegistrationRepository = conceptTenantRegistrationRepository; 
			

			
        } 
        public override async Task<ConceptTenantRegistrationDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ConceptTenantRegistrationDto> Create(CreateConceptTenantRegistrationDto input) 
        { 
            CheckCreatePermission(); 
            var conceptTenantRegistration = ObjectMapper.Map<Models.ConceptTenantRegistrations>(input); 
			try{
              await _conceptTenantRegistrationRepository.InsertAsync(conceptTenantRegistration); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(conceptTenantRegistration); 
        } 
        public override async Task<ConceptTenantRegistrationDto> Update(UpdateConceptTenantRegistrationDto input) 
        { 
            CheckUpdatePermission(); 
            var conceptTenantRegistration = await _conceptTenantRegistrationRepository.GetAsync(input.Id);
            MapToEntity(input, conceptTenantRegistration); 
		    try{
               await _conceptTenantRegistrationRepository.UpdateAsync(conceptTenantRegistration); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var conceptTenantRegistration = await _conceptTenantRegistrationRepository.GetAsync(input.Id); 
               await _conceptTenantRegistrationRepository.DeleteAsync(conceptTenantRegistration);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.ConceptTenantRegistrations MapToEntity(CreateConceptTenantRegistrationDto createInput) 
        { 
            var conceptTenantRegistration = ObjectMapper.Map<Models.ConceptTenantRegistrations>(createInput); 
            return conceptTenantRegistration; 
        } 
        protected override void MapToEntity(UpdateConceptTenantRegistrationDto input, Models.ConceptTenantRegistrations conceptTenantRegistration) 
        { 
            ObjectMapper.Map(input, conceptTenantRegistration); 
        } 
        protected override IQueryable<Models.ConceptTenantRegistrations> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ConceptTenantRegistrations> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Id.ToString().Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ConceptTenantRegistrations> GetEntityByIdAsync(int id) 
        { 
            var conceptTenantRegistration = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(conceptTenantRegistration); 
        } 
        protected override IQueryable<Models.ConceptTenantRegistrations> ApplySorting(IQueryable<Models.ConceptTenantRegistrations> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Id.ToString()); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ConceptTenantRegistrationDto>> GetAllConceptTenantRegistrations(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ConceptTenantRegistrationDto> ouput = new PagedResultDto<ConceptTenantRegistrationDto>(); 
            IQueryable<Models.ConceptTenantRegistrations> query = query = from x in _conceptTenantRegistrationRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _conceptTenantRegistrationRepository.GetAll() 
                        where x.Id.ToString().Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ConceptTenantRegistrations.Dto.ConceptTenantRegistrationDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<ConceptTenantRegistrationDto>> GetAllConceptTenantRegistrationsForCombo()
        {
            var conceptTenantRegistrationList = await _conceptTenantRegistrationRepository.GetAllListAsync(x => x.IsActive == true);

            var conceptTenantRegistration = ObjectMapper.Map<List<ConceptTenantRegistrationDto>>(conceptTenantRegistrationList.ToList());

            return conceptTenantRegistration;
        }
		
    } 
} ;
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.ConceptTenantRegistrations.Dto 
{
        [AutoMap(typeof(Models.ConceptTenantRegistrations))] 
        public class UpdateConceptTenantRegistrationDto : EntityDto<int> 
        {

              public int ConceptId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.ConceptTenantRegistrations.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.ConceptTenantRegistrations
{
      public interface IConceptTenantRegistrationAppService : IAsyncCrudAppService<ConceptTenantRegistrationDto, int, PagedResultRequestDto, CreateConceptTenantRegistrationDto, UpdateConceptTenantRegistrationDto>
      {
            Task<PagedResultDto<ConceptTenantRegistrationDto>> GetAllConceptTenantRegistrations(GdPagedResultRequestDto input);
			Task<List<Dto.ConceptTenantRegistrationDto>> GetAllConceptTenantRegistrationsForCombo();
      }
}
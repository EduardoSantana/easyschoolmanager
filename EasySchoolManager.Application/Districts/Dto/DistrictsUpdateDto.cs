//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Districts.Dto 
{
        [AutoMap(typeof(Models.Districts))] 
        public class UpdateDistrictDto : EntityDto<int> 
        {

              [StringLength(50)]  
              public string Name {get;set;} 

              [StringLength(9)]  
              public string Abbreviation {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
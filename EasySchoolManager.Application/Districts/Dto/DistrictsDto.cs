//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Districts.Dto 
{
        [AutoMap(typeof(Models.Districts))] 
        public class DistrictDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public string Name {get;set;} 
              public string Abbreviation {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
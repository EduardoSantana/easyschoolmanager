//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Districts.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Districts 
{ 
    [AbpAuthorize(PermissionNames.Pages_Districts)] 
    public class DistrictAppService : AsyncCrudAppService<Models.Districts, DistrictDto, int, PagedResultRequestDto, CreateDistrictDto, UpdateDistrictDto>, IDistrictAppService 
    { 
        private readonly IRepository<Models.Districts, int> _districtRepository;
		


        public DistrictAppService( 
            IRepository<Models.Districts, int> repository, 
            IRepository<Models.Districts, int> districtRepository 
            ) 
            : base(repository) 
        { 
            _districtRepository = districtRepository; 
			

			
        } 
        public override async Task<DistrictDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<DistrictDto> Create(CreateDistrictDto input) 
        { 
            CheckCreatePermission(); 
            var district = ObjectMapper.Map<Models.Districts>(input); 
			try{
              await _districtRepository.InsertAsync(district); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(district); 
        } 
        public override async Task<DistrictDto> Update(UpdateDistrictDto input) 
        { 
            CheckUpdatePermission(); 
            var district = await _districtRepository.GetAsync(input.Id);
            MapToEntity(input, district); 
		    try{
               await _districtRepository.UpdateAsync(district); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var district = await _districtRepository.GetAsync(input.Id); 
               await _districtRepository.DeleteAsync(district);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Districts MapToEntity(CreateDistrictDto createInput) 
        { 
            var district = ObjectMapper.Map<Models.Districts>(createInput); 
            return district; 
        } 
        protected override void MapToEntity(UpdateDistrictDto input, Models.Districts district) 
        { 
            ObjectMapper.Map(input, district); 
        } 
        protected override IQueryable<Models.Districts> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Districts> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Districts> GetEntityByIdAsync(int id) 
        { 
            var district = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(district); 
        } 
        protected override IQueryable<Models.Districts> ApplySorting(IQueryable<Models.Districts> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<DistrictDto>> GetAllDistricts(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<DistrictDto> ouput = new PagedResultDto<DistrictDto>(); 
            IQueryable<Models.Districts> query = query = from x in _districtRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _districtRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Districts.Dto.DistrictDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<DistrictDto>> GetAllDistrictsForCombo()
        {
            var districtList = await _districtRepository.GetAllListAsync(x => x.IsActive == true);

            var district = ObjectMapper.Map<List<DistrictDto>>(districtList.ToList());

            return district;
        }
		
    } 
} ;
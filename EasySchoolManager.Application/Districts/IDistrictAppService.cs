//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Districts.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Districts
{
      public interface IDistrictAppService : IAsyncCrudAppService<DistrictDto, int, PagedResultRequestDto, CreateDistrictDto, UpdateDistrictDto>
      {
            Task<PagedResultDto<DistrictDto>> GetAllDistricts(GdPagedResultRequestDto input);
			Task<List<Dto.DistrictDto>> GetAllDistrictsForCombo();
      }
}
﻿using Abp.Application.Services;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace EasySchoolManager.ADO
{
    public interface IDBService : IApplicationService
    {
        [RemoteService(IsEnabled = false, IsMetadataEnabled = false)]
        int ExecuteNonQuery(string query, params SqlParameter[] parameters);
        [RemoteService(IsEnabled = false, IsMetadataEnabled = false)]
        DataTable ExecuteQuery(string query, params SqlParameter[] parameters);
        [RemoteService(IsEnabled = false, IsMetadataEnabled = false)]
        DataTable ExecuteQuery(string query, CommandType coomandType, params SqlParameter[] parameters);
        [RemoteService(IsEnabled = false, IsMetadataEnabled = false)]
        IEnumerable<T> ExecuteQuery<T>(string query, params SqlParameter[] parameters);
        [RemoteService(IsEnabled = false, IsMetadataEnabled = false)]
        IEnumerable<T> ExecuteQuery<T>(string query, CommandType coomandType, params SqlParameter[] parameters);

        [RemoteService(IsEnabled = false, IsMetadataEnabled = false)]
        DataSet ExecuteQuerySet(string query, params SqlParameter[] parameters);

        [RemoteService(IsEnabled = false, IsMetadataEnabled = false)]
        DataSet ExecuteQuerySet(string query, CommandType comandType, params SqlParameter[] parameters);

    }
}

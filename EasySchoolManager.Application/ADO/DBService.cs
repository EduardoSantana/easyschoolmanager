﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.ADO
{
    public class DBService : IDBService
    {

        SqlConnection GetConnection()
        {
            string conString = System.Configuration.ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

            return new SqlConnection(conString);
        }

        SqlCommand GetCommand()
        {
            var command = new SqlCommand();
            command.Connection = GetConnection();
            return command;
        }

        SqlDataAdapter GetAdapter()
        {
            var adapter = new SqlDataAdapter();
            adapter.SelectCommand = GetCommand();
            return adapter;
        }




        public int ExecuteNonQuery(string query, params SqlParameter[] parameters)
        {
            var command = GetCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = query;

            foreach (var p in parameters)
                command.Parameters.Add(p);

            return command.ExecuteNonQuery();
        }

        public DataTable ExecuteQuery(string query, params SqlParameter[] parameters)
        {
            return ExecuteQuery(query, CommandType.Text, parameters);
        }

        public IEnumerable<T> ExecuteQuery<T>(string query, params SqlParameter[] parameters)
        {
            return ExecuteQuery<T>(query, CommandType.Text, parameters);
        }

        public DataSet ExecuteQuerySet(string query, params SqlParameter[] parameters)
        {
            return ExecuteQuerySet(query, CommandType.Text, parameters);
        }


        public DataSet ExecuteQuerySet(string query, CommandType comandType, params SqlParameter[] parameters)
        {
            var adapter = GetAdapter();
            var result = new DataSet();

            adapter.SelectCommand.CommandType = comandType;
            adapter.SelectCommand.CommandText = query;

            foreach (var p in parameters)
                adapter.SelectCommand.Parameters.Add(p);

            adapter.Fill(result);

            return result;
        }



        public DataTable ExecuteQuery(string query, CommandType comandType, params SqlParameter[] parameters)
        {
            var adapter = GetAdapter();
            var result = new DataTable();

            adapter.SelectCommand.CommandType = comandType;
            adapter.SelectCommand.CommandText = query;

            foreach (var p in parameters)
                adapter.SelectCommand.Parameters.Add(p);

            adapter.Fill(result);

            return result;
        }


        public IEnumerable<T> ExecuteQuery<T>(string query, CommandType coomandType, params SqlParameter[] parameters)
        {
            EasySchoolManager.EntityFramework.EasySchoolManagerDbContext db = new EntityFramework.EasySchoolManagerDbContext();

           return db.Database.SqlQuery<T>(query);
        }
    }
}

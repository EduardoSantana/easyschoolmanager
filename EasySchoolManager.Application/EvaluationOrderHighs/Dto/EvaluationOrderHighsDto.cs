//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.EvaluationOrderHighs.Dto 
{
        [AutoMap(typeof(Models.EvaluationOrderHighs))] 
        public class EvaluationOrderHighDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public int EvaluationPositionHighId {get;set;} 
              public int Month {get;set;} 
              public string Description {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.EvaluationOrderHighs.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.EvaluationOrderHighs
{
      public interface IEvaluationOrderHighAppService : IAsyncCrudAppService<EvaluationOrderHighDto, int, PagedResultRequestDto, CreateEvaluationOrderHighDto, UpdateEvaluationOrderHighDto>
      {
            Task<PagedResultDto<EvaluationOrderHighDto>> GetAllEvaluationOrderHighs(GdPagedResultRequestDto input);
			Task<List<Dto.EvaluationOrderHighDto>> GetAllEvaluationOrderHighsForCombo();
      }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.EvaluationOrderHighs.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.EvaluationOrderHighs 
{ 
    [AbpAuthorize(PermissionNames.Pages_EvaluationOrderHighs)] 
    public class EvaluationOrderHighAppService : AsyncCrudAppService<Models.EvaluationOrderHighs, EvaluationOrderHighDto, int, PagedResultRequestDto, CreateEvaluationOrderHighDto, UpdateEvaluationOrderHighDto>, IEvaluationOrderHighAppService 
    { 
        private readonly IRepository<Models.EvaluationOrderHighs, int> _evaluationOrderHighRepository;
		
		    private readonly IRepository<Models.EvaluationPositionHighs, int> _evaluationPositionHighRepository;


        public EvaluationOrderHighAppService( 
            IRepository<Models.EvaluationOrderHighs, int> repository, 
            IRepository<Models.EvaluationOrderHighs, int> evaluationOrderHighRepository ,
            IRepository<Models.EvaluationPositionHighs, int> evaluationPositionHighRepository

            ) 
            : base(repository) 
        { 
            _evaluationOrderHighRepository = evaluationOrderHighRepository; 
			
            _evaluationPositionHighRepository = evaluationPositionHighRepository;


			
        } 
        public override async Task<EvaluationOrderHighDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<EvaluationOrderHighDto> Create(CreateEvaluationOrderHighDto input) 
        { 
            CheckCreatePermission(); 
            var evaluationOrderHigh = ObjectMapper.Map<Models.EvaluationOrderHighs>(input); 
			try{
              await _evaluationOrderHighRepository.InsertAsync(evaluationOrderHigh); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(evaluationOrderHigh); 
        } 
        public override async Task<EvaluationOrderHighDto> Update(UpdateEvaluationOrderHighDto input) 
        { 
            CheckUpdatePermission(); 
            var evaluationOrderHigh = await _evaluationOrderHighRepository.GetAsync(input.Id);
            MapToEntity(input, evaluationOrderHigh); 
		    try{
               await _evaluationOrderHighRepository.UpdateAsync(evaluationOrderHigh); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var evaluationOrderHigh = await _evaluationOrderHighRepository.GetAsync(input.Id); 
               await _evaluationOrderHighRepository.DeleteAsync(evaluationOrderHigh);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.EvaluationOrderHighs MapToEntity(CreateEvaluationOrderHighDto createInput) 
        { 
            var evaluationOrderHigh = ObjectMapper.Map<Models.EvaluationOrderHighs>(createInput); 
            return evaluationOrderHigh; 
        } 
        protected override void MapToEntity(UpdateEvaluationOrderHighDto input, Models.EvaluationOrderHighs evaluationOrderHigh) 
        { 
            ObjectMapper.Map(input, evaluationOrderHigh); 
        } 
        protected override IQueryable<Models.EvaluationOrderHighs> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.EvaluationOrderHighs> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.EvaluationOrderHighs> GetEntityByIdAsync(int id) 
        { 
            var evaluationOrderHigh = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(evaluationOrderHigh); 
        } 
        protected override IQueryable<Models.EvaluationOrderHighs> ApplySorting(IQueryable<Models.EvaluationOrderHighs> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Description); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<EvaluationOrderHighDto>> GetAllEvaluationOrderHighs(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<EvaluationOrderHighDto> ouput = new PagedResultDto<EvaluationOrderHighDto>(); 
            IQueryable<Models.EvaluationOrderHighs> query = query = from x in _evaluationOrderHighRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _evaluationOrderHighRepository.GetAll() 
                        where x.Description.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<EvaluationOrderHighs.Dto.EvaluationOrderHighDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<EvaluationOrderHighDto>> GetAllEvaluationOrderHighsForCombo()
        {
            var evaluationOrderHighList = await _evaluationOrderHighRepository.GetAllListAsync(x => x.IsActive == true);

            var evaluationOrderHigh = ObjectMapper.Map<List<EvaluationOrderHighDto>>(evaluationOrderHighList.ToList());

            return evaluationOrderHigh;
        }
		
    } 
} ;
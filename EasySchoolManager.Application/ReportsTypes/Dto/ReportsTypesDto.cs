//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;
using EasySchoolManager.ReportsFilters.Dto;

namespace EasySchoolManager.Reports.Dto
{
    [AutoMap(typeof(Models.ReportsTypes))]
    public class ReportsTypesDto : EntityDto<int>
    {
        [Required]
        [MaxLength(50)]
        public string Nombre { get; set; }
        [MaxLength(120)]
        public string Descripcion { get; set; }

        public virtual List<_ReportDto> Reports { get; set; }

    }
}
﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.SessionStudentSelections
{
    public class GetAllStudentByCourseByPosInput
    {
        public int CourseId { get; set; }
        public int? SessionId { get; set; }
        public int? EnrollmentStudentId { get; set; }
        public int? EvaluationPositionHighId { get; set; }
        public int? SubjectHighId { get; set; }
    }

}
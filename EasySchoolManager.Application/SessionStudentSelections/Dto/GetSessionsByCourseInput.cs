﻿namespace EasySchoolManager.SessionStudentSelections
{
    public class GetSessionsByCourseInput
    {
        public int CourseId { get; set; }
        public long EnrollmentStudentId { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.SessionStudentSelections
{
    public class GetAllStudentByCourseInput
    {
        public int CourseId { get; set; }
        public int? SessionId { get; set; }
        public int? EnrollmentStudentId { get; set; }
        public int? EvaluationOrderHighId { get; set; }
        public int? SubjectHighId { get; set; }
    }

    public class GetAllStudentByCourseInput2
    {
        public int CourseId { get; set; }
        public int? SessionId { get; set; }
        public List<int> EnrollmentStudentIds { get; set; }
        public int? EvaluationOrderHighId { get; set; }
        public int? SubjectHighId { get; set; }
    }
}
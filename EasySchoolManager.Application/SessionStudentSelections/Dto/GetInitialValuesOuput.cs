﻿using System.Collections.Generic;

namespace EasySchoolManager.SessionStudentSelections
{
    public class GetInitialValuesOuput
    {
        public List<ComboBoxObject> Courses { get; set; }
        public List<ComboBoxObject> Sessions { get; set; }
        public List<ComboBoxObject> Evaluation { get; set; }
        public List<ComboBoxObject> Subjects { get; internal set; }
    }
}
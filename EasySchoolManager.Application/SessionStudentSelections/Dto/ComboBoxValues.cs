﻿namespace EasySchoolManager.SessionStudentSelections
{
    public class ComboBoxObject
    {
        public int Id;
        public string Name;
        public string Alternative;
        public ComboBoxObject(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
        public ComboBoxObject(int id, string name, string alternative)
            : this(id, name)
        {
            this.Alternative = alternative;
        }
    }
}
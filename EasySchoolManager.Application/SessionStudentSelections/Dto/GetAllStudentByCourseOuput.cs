﻿using System.Collections.Generic;

namespace EasySchoolManager.SessionStudentSelections
{
    public class GetAllStudentByCourseOuput
    {
        public int studentCount { get; set; }

        public GetAllStudentByCourseOuput()
        {
            StudentCourse = new List<StudentSessionDto>();
            StudentSession = new List<StudentSessionDto>();
            StudentEvaluationHigh = new List<StudentSessionEvaluationHighDto>();
        }
        public List<StudentSessionDto> StudentCourse { get; set; }
        public List<StudentSessionDto> StudentSession { get; set; }
        public List<StudentSessionEvaluationHighDto> StudentEvaluationHigh { get; set; }

    }
}
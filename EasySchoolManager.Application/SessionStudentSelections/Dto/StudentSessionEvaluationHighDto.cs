﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace EasySchoolManager.SessionStudentSelections
{
    [AutoMap(typeof(Models.EnrollmentStudents))]
    public class StudentSessionEvaluationHighDto: EntityDto<int>
    {
        public int PeriodId { get; set; }

        public long EnrollmentId { get; set; }

        public int StudentId { get; set; }

        public int RelationshipId { get; set; }

        public int Sequence { get; set; }

        public int? Expression { get; set; }

        public int? Idd { get; set; }

        public Students.Dto.StudentDto Students { get; set; }

        public Periods.Dto.PeriodDto Periods { get; set; }
        public string LastName { get; internal set; }
    }
}
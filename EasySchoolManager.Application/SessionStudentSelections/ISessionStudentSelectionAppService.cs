﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.SessionStudentSelections
{
    public interface ISessionStudentSelectionAppService : IApplicationService
    {
        Task<GetAllStudentByCourseOuput> GetAllStudentByCourse(GetAllStudentByCourseInput input);
        Task<GetAllStudentByCourseOuput> GetAllStudentByCoursePrevious(GetAllStudentByCourseInput input);


        Task<GetAllStudentByCourseOuput> InsertStudentInSessionAndGetAllStudentByCourse(GetAllStudentByCourseInput input);
        Task<GetAllStudentByCourseOuput> InsertAllStudentInSessionAndGetAllStudentByCourse(GetAllStudentByCourseInput2 input);

        Task<GetAllStudentByCourseOuput> RemoveStudentFromSessionAndGetAllStudentByCourse(GetAllStudentByCourseInput input);
        Task<GetAllStudentByCourseOuput> RemoveAllStudentFromSessionAndGetAllStudentByCourse(GetAllStudentByCourseInput2 input);

        Task<GetInitialValuesOuput> GetInitialValues();

        Task<GetInitialValuesOuput> GetSessionsByCourse(GetSessionsByCourseInput input);

        Task<GetAllStudentByCourseOuput> SetOrderNumberForSessionInCourse(GetAllStudentByCourseInput input);

        Task<GetAllStudentByCourseOuput> GetAllStudentScoreByCourse(GetAllStudentByCourseInput input);
        Task<GetAllStudentByCourseOuput> GetAllStudentScoreByCourseByPos(GetAllStudentByCourseByPosInput input);
        Task<GetAllStudentByCourseOuput> GetAllStudentScoreByCourseByPosPrevious(GetAllStudentByCourseByPosInput input);
    }
}

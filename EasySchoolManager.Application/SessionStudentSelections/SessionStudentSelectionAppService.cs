﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using EasySchoolManager.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.SessionStudentSelections
{
    public class SessionStudentSelectionAppService : ApplicationService, ISessionStudentSelectionAppService
    {
        private readonly IRepository<Models.CourseSessions, int> _courseSessionRepository;
        private readonly IRepository<Models.EnrollmentStudents, int> _enrollmentStudentRepository;
        private readonly IRepository<Models.CourseEnrollmentStudents, int> _courseEnrollmentStudentsRepository;
        private readonly IRepository<Models.Courses, int> _courseRepository;
        private readonly IRepository<Models.EvaluationPositionHighs, int> _evaluationPositionHighRepository;
        private readonly IRepository<Models.Students, int> _studentRepository;
        private readonly IRepository<Models.EvaluationHighs, int> _evaluationHighRepository;

        private readonly MultiTenancy.TenantManager _tenantManager;

        public SessionStudentSelectionAppService(
            IRepository<Models.EnrollmentStudents, int> enrollmentStudentRepository,
            IRepository<Models.CourseSessions, int> courseSessionRepository,
            IRepository<Models.CourseEnrollmentStudents, int> courseEnrollmentStudentsRepository,
            IRepository<Models.Courses, int> courseRepository,
            IRepository<Models.Students, int> studentRepository,
            IRepository<Models.EvaluationPositionHighs, int> evaluationPositionHighRepository,
            IRepository<Models.EvaluationHighs, int> evaluationHighRepository,
            MultiTenancy.TenantManager tenantManager
            )
        {
            _courseSessionRepository = courseSessionRepository;
            _enrollmentStudentRepository = enrollmentStudentRepository;
            _courseEnrollmentStudentsRepository = courseEnrollmentStudentsRepository;
            _courseRepository = courseRepository;
            _studentRepository = studentRepository;
            _tenantManager = tenantManager;
            _evaluationHighRepository = evaluationHighRepository;
            _evaluationPositionHighRepository = evaluationPositionHighRepository;
            LocalizationSourceName = EasySchoolManagerConsts.LocalizationSourceName;
        }


        public async Task<GetAllStudentByCourseOuput> SetOrderNumberForSessionInCourse(GetAllStudentByCourseInput input)
        {
            GeneralHelper.ValidateUserBellongToASchool(AbpSession);
            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();

            var tenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
            if (tenant == null)
            {
                Logger.Error(L("TheUserMustBellongToATenantToUseThisOption"));
                return null;
            }

            if (!tenant.PeriodId.HasValue)
            {
                throw new UserFriendlyException(L("ToUseThisOptionTheSchoolMustHasSelectedTheCurrentPeriod"));
            }


            if (input.SessionId.HasValue)
            {
                var query2 = await _courseEnrollmentStudentsRepository.GetAllListAsync(x => x.CourseId == input.CourseId
                            && x.SessionId == input.SessionId
                            && x.EnrollmentStudents.PeriodId == tenant.PeriodId);
                var studentOfSession = query2.OrderBy(x => x.EnrollmentStudents?.Students?.LastName).ToList();

                if (studentOfSession.Any(x => x.Sequence.HasValue))
                {
                    if (studentOfSession.Any(x => x.Sequence.HasValue == false) == false)
                        throw new UserFriendlyException(L("ThisSessionHasAlreadyStudentOrdenedThereForYouCanNotDoDisProcessAgain"));

                    var studentOfSessionPending = studentOfSession.Where(x => x.Sequence.HasValue == false).ToList();
                    var studentSequence = studentOfSession.Max(x => x.Sequence);

                    foreach (var item in studentOfSessionPending)
                    {
                        item.Sequence = ++studentSequence;
                    }
                }
                else
                {
                    var studentSequence = 0;

                    foreach (var item in studentOfSession)
                    {
                        item.Sequence = ++studentSequence;
                    }
                }
            }
            ouput = await GetAllStudentByCourse(input);
            return ouput;
        }

        public async Task<GetAllStudentByCourseOuput> GetAllStudentByCourse(GetAllStudentByCourseInput input)
        {
            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();
            List<StudentSessionDto> studentListOrdened = new List<StudentSessionDto>();
            List<StudentSessionDto> studentListUnordened = new List<StudentSessionDto>();

            GeneralHelper.ValidateUserBellongToASchool(AbpSession);

            var tenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
            if (tenant == null)
            {
                Logger.Error(L("TheUserMustBellongToATenantToUseThisOption"));
                return null;
            }

            if (!tenant.PeriodId.HasValue)
            {
                throw new UserFriendlyException(L("ToUseThisOptionTheSchoolMustHasSelectedTheCurrentPeriod"));
            }

            try
            {

                var queryCount = _courseEnrollmentStudentsRepository.GetAllList(x => x.CourseId == input.CourseId &&
                    x.EnrollmentStudents.PeriodId == tenant.PeriodId &&
                    x.EnrollmentStudents.Enrollments.EnrollmentSequences.FirstOrDefault().IsDeleted == false
                    && x.EnrollmentStudents.Enrollments.EnrollmentSequences.FirstOrDefault().IsActive == true
                    && x.EnrollmentStudents.IsActive == true && x.EnrollmentStudents.IsDeleted == false).Count();

                ouput.studentCount = queryCount;

                //var query = await _courseEnrollmentStudentsRepository.GetAllListAsync(x => x.CourseId == input.CourseId && x.SessionId.HasValue == true);
                //var studentOrdened = query.ToList();
                if (input.SessionId.HasValue)
                {
                    var query2 = await _courseEnrollmentStudentsRepository.GetAllListAsync(x => x.CourseId == input.CourseId
                                && x.SessionId == input.SessionId
                                && x.EnrollmentStudents.Enrollments.EnrollmentSequences.FirstOrDefault().IsDeleted == false
                                && x.EnrollmentStudents.PeriodId == tenant.PeriodId
                                && x.EnrollmentStudents.IsActive == true
                                && x.EnrollmentStudents.IsDeleted == false);
                    var studentOfSession = query2.OrderBy(x => x?.Sequence).ThenBy(x => x.EnrollmentStudents?.Students?.LastName).ToList();

                    foreach (var item in studentOfSession)
                    {
                        StudentSessionDto st = new StudentSessionDto();

                        st.Students = new Students.Dto.StudentDto();
                        st.Students.FirstName = item.EnrollmentStudents.Students.FirstName;
                        st.Students.LastName = item.EnrollmentStudents.Students.LastName;
                        st.Students.Id = item.EnrollmentStudents.Students.Id;
                        if (item.Sequence.HasValue)
                            st.Sequence = item.Sequence.Value;
                        st.Id = item.EnrollmentStudents.Id;
                        studentListOrdened.Add(st);
                    }
                }

                var query3 = await _courseEnrollmentStudentsRepository.GetAllListAsync(x => x.CourseId == input.CourseId
                                            && x.EnrollmentStudents != null
                                            && x.SessionId.HasValue == false
                                            && x.EnrollmentStudents.PeriodId == tenant.PeriodId &&
                    x.EnrollmentStudents.Enrollments.EnrollmentSequences.FirstOrDefault().IsDeleted == false
                    && x.EnrollmentStudents.IsActive == true
                    && x.EnrollmentStudents.IsDeleted == false);
                var studentUnordened = query3.OrderBy(x => x.EnrollmentStudents?.Students?.LastName).ToList();

                foreach (var item in studentUnordened)
                {
                    StudentSessionDto st = new StudentSessionDto();

                    st.Students = new Students.Dto.StudentDto();
                    st.Students.FirstName = item.EnrollmentStudents.Students.FirstName;
                    st.Students.LastName = item.EnrollmentStudents.Students.LastName;
                    st.Students.Id = item.EnrollmentStudents.Students.Id;
                    st.LastName = st.Students.LastName;
                    if (item.Sequence.HasValue)
                        st.Sequence = item.Sequence.Value;
                    st.Id = item.EnrollmentStudents.Id;
                    studentListUnordened.Add(st);
                }

                ouput.StudentCourse = studentListUnordened.OrderBy(x => x.LastName).ToList();
                ouput.StudentSession = studentListOrdened.OrderBy(x => x.LastName).ToList();

            }
            catch (Exception err)
            {
                Logger.Error(err.Message);
                throw new UserFriendlyException(err.Message);
            }

            return ouput;
        }

        public async Task<GetAllStudentByCourseOuput> GetAllStudentByCoursePrevious(GetAllStudentByCourseInput input)
        {
            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();
            List<StudentSessionDto> studentListOrdened = new List<StudentSessionDto>();
            List<StudentSessionDto> studentListUnordened = new List<StudentSessionDto>();

            GeneralHelper.ValidateUserBellongToASchool(AbpSession);

            var tenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
            if (tenant == null)
            {
                Logger.Error(L("TheUserMustBellongToATenantToUseThisOption"));
                return null;
            }

            if (!tenant.PeriodId.HasValue)
            {
                throw new UserFriendlyException(L("ToUseThisOptionTheSchoolMustHasSelectedTheCurrentPeriod"));
            }

            try
            {

                var queryCount = _courseEnrollmentStudentsRepository.GetAllList(x => x.CourseId == input.CourseId &&
                    x.EnrollmentStudents.PeriodId == tenant.PreviousPeriodId &&
                    x.EnrollmentStudents.Enrollments.EnrollmentSequences.FirstOrDefault().IsDeleted == false
                    && x.EnrollmentStudents.Enrollments.EnrollmentSequences.FirstOrDefault().IsActive == true
                    && x.EnrollmentStudents.IsActive == true && x.EnrollmentStudents.IsDeleted == false).Count();

                ouput.studentCount = queryCount;

                //var query = await _courseEnrollmentStudentsRepository.GetAllListAsync(x => x.CourseId == input.CourseId && x.SessionId.HasValue == true);
                //var studentOrdened = query.ToList();
                if (input.SessionId.HasValue)
                {
                    var query2 = await _courseEnrollmentStudentsRepository.GetAllListAsync(x => x.CourseId == input.CourseId
                                && x.SessionId == input.SessionId
                                && x.EnrollmentStudents.Enrollments.EnrollmentSequences.FirstOrDefault().IsDeleted == false
                                && x.EnrollmentStudents.PeriodId == tenant.PreviousPeriodId
                                && x.EnrollmentStudents.IsActive == true
                                && x.EnrollmentStudents.IsDeleted == false);
                    var studentOfSession = query2.OrderBy(x => x?.Sequence).ThenBy(x => x.EnrollmentStudents?.Students?.LastName).ToList();

                    foreach (var item in studentOfSession)
                    {
                        StudentSessionDto st = new StudentSessionDto();

                        st.Students = new Students.Dto.StudentDto();
                        st.Students.FirstName = item.EnrollmentStudents.Students.FirstName;
                        st.Students.LastName = item.EnrollmentStudents.Students.LastName;
                        st.Students.Id = item.EnrollmentStudents.Students.Id;
                        if (item.Sequence.HasValue)
                            st.Sequence = item.Sequence.Value;
                        st.Id = item.EnrollmentStudents.Id;
                        studentListOrdened.Add(st);
                    }
                }

                var query3 = await _courseEnrollmentStudentsRepository.GetAllListAsync(x => x.CourseId == input.CourseId
                                            && x.EnrollmentStudents != null
                                            && x.SessionId.HasValue == false
                                            && x.EnrollmentStudents.PeriodId == tenant.PreviousPeriodId &&
                    x.EnrollmentStudents.Enrollments.EnrollmentSequences.FirstOrDefault().IsDeleted == false
                    && x.EnrollmentStudents.IsActive == true
                    && x.EnrollmentStudents.IsDeleted == false);
                var studentUnordened = query3.OrderBy(x => x.EnrollmentStudents?.Students?.LastName).ToList();

                foreach (var item in studentUnordened)
                {
                    StudentSessionDto st = new StudentSessionDto();

                    st.Students = new Students.Dto.StudentDto();
                    st.Students.FirstName = item.EnrollmentStudents.Students.FirstName;
                    st.Students.LastName = item.EnrollmentStudents.Students.LastName;
                    st.Students.Id = item.EnrollmentStudents.Students.Id;
                    st.LastName = st.Students.LastName;
                    if (item.Sequence.HasValue)
                        st.Sequence = item.Sequence.Value;
                    st.Id = item.EnrollmentStudents.Id;
                    studentListUnordened.Add(st);
                }

                ouput.StudentCourse = studentListUnordened.OrderBy(x => x.LastName).ToList();
                ouput.StudentSession = studentListOrdened.OrderBy(x => x.LastName).ToList();

            }
            catch (Exception err)
            {
                Logger.Error(err.Message);
                throw new UserFriendlyException(err.Message);
            }

            return ouput;
        }





        public async Task<GetAllStudentByCourseOuput> GetAllStudentScoreByCourse(GetAllStudentByCourseInput input)
        {
            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();
            List<StudentSessionEvaluationHighDto> studentListOrdened = new List<StudentSessionEvaluationHighDto>();

            GeneralHelper.ValidateUserBellongToASchool(AbpSession);

            var tenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
            if (tenant == null)
            {
                Logger.Error(L("TheUserMustBellongToATenantToUseThisOption"));
                return null;
            }

            if (!tenant.PeriodId.HasValue)
                throw new UserFriendlyException(L("ToUseThisOptionTheSchoolMustHasSelectedTheCurrentPeriod"));

            if (!input.SubjectHighId.HasValue)
                return ouput;

            if (!input.EvaluationOrderHighId.HasValue)
                return ouput;

            try
            {

                ouput.studentCount = _courseEnrollmentStudentsRepository
                    .GetAllList(x => x.CourseId == input.CourseId
                             && x.EnrollmentStudents.PeriodId == tenant.PeriodId)
                    .Count();

                if (input.SessionId.HasValue)
                {
                    var studentOfSession = (await _courseEnrollmentStudentsRepository
                        .GetAllListAsync(x => x.CourseId == input.CourseId
                                           && x.SessionId == input.SessionId
                                           && x.EnrollmentStudents.PeriodId == tenant.PeriodId))
                        .OrderBy(x => x?.Sequence)
                        .ThenBy(x => x.EnrollmentStudents?.Students?.LastName)
                        .ToList();

                    var Califications = await _evaluationHighRepository
                        .GetAllListAsync(x => x.EvaluationOrderHighId == input.EvaluationOrderHighId
                                           && x.SubjectHighId == input.SubjectHighId);

                    foreach (var item in studentOfSession)
                    {
                        StudentSessionEvaluationHighDto st = new StudentSessionEvaluationHighDto();

                        st.Students = new Students.Dto.StudentDto();
                        st.Students.FirstName = item.EnrollmentStudents.Students.FirstName;
                        st.Students.LastName = item.EnrollmentStudents.Students.LastName;
                        st.Students.Id = item.EnrollmentStudents.Students.Id;
                        var StudentScore = Califications.FirstOrDefault(x => x.EnrollmentStudentId == item.EnrollmentStudentId);
                        if (StudentScore != null)
                        {
                            st.Expression = StudentScore.Expression;
                            st.Idd = StudentScore.Id;
                        }

                        if (item.Sequence.HasValue)
                            st.Sequence = item.Sequence.Value;

                        st.Id = item.EnrollmentStudents.Id;
                        studentListOrdened.Add(st);
                    }
                }

                ouput.StudentEvaluationHigh = studentListOrdened.OrderBy(x => x.LastName).ToList();
            }
            catch (Exception err)
            {
                Logger.Error(err.Message);
                throw new UserFriendlyException(err.Message);
            }

            return ouput;
        }

        public async Task<GetAllStudentByCourseOuput> GetAllStudentScoreByCourseByPos(GetAllStudentByCourseByPosInput input)
        {
            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();
            List<StudentSessionEvaluationHighDto> studentListOrdened = new List<StudentSessionEvaluationHighDto>();

            GeneralHelper.ValidateUserBellongToASchool(AbpSession);

            var tenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
            if (tenant == null)
            {
                Logger.Error(L("TheUserMustBellongToATenantToUseThisOption"));
                return null;
            }

            if (!tenant.PeriodId.HasValue)
                throw new UserFriendlyException(L("ToUseThisOptionTheSchoolMustHasSelectedTheCurrentPeriod"));

            if (!input.SubjectHighId.HasValue)
                return ouput;

            if (!input.EvaluationPositionHighId.HasValue)
                return ouput;

            try
            {

                var EvaluationOrderHighId = _evaluationPositionHighRepository.Get(input.EvaluationPositionHighId.Value).EvaluationOrderHighs.FirstOrDefault().Id;

                ouput.studentCount = _courseEnrollmentStudentsRepository
                    .GetAllList(x => x.CourseId == input.CourseId
                             && x.EnrollmentStudents.PeriodId == tenant.PeriodId)
                    .Count();

                if (input.SessionId.HasValue)
                {
                    var studentOfSession = (await _courseEnrollmentStudentsRepository
                        .GetAllListAsync(x => x.CourseId == input.CourseId
                                           && x.SessionId == input.SessionId
                                           && x.EnrollmentStudents.PeriodId == tenant.PeriodId))
                        .OrderBy(x => x?.Sequence)
                        .ThenBy(x => x.EnrollmentStudents?.Students?.LastName)
                        .ToList();

                    var Califications = await _evaluationHighRepository
                        .GetAllListAsync(x => x.EvaluationOrderHighId == EvaluationOrderHighId
                                           && x.SubjectHighId == input.SubjectHighId);

                    foreach (var item in studentOfSession)
                    {
                        StudentSessionEvaluationHighDto st = new StudentSessionEvaluationHighDto();

                        st.Students = new Students.Dto.StudentDto();
                        st.Students.FirstName = item.EnrollmentStudents.Students.FirstName;
                        st.Students.LastName = item.EnrollmentStudents.Students.LastName;
                        st.Students.Id = item.EnrollmentStudents.Students.Id;
                        var StudentScore = Califications.FirstOrDefault(x => x.EnrollmentStudentId == item.EnrollmentStudentId);
                        if (StudentScore != null)
                        {
                            st.Expression = StudentScore.Expression;
                            st.Idd = StudentScore.Id;
                        }

                        if (item.Sequence.HasValue)
                            st.Sequence = item.Sequence.Value;

                        st.Id = item.EnrollmentStudents.Id;
                        studentListOrdened.Add(st);
                    }
                }

                ouput.StudentEvaluationHigh = studentListOrdened.OrderBy(x => x.LastName).ToList();
            }
            catch (Exception err)
            {
                Logger.Error(err.Message);
                throw new UserFriendlyException(err.Message);
            }

            return ouput;
        }


        public async Task<GetAllStudentByCourseOuput> GetAllStudentScoreByCourseByPosPrevious(GetAllStudentByCourseByPosInput input)
        {
            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();
            List<StudentSessionEvaluationHighDto> studentListOrdened = new List<StudentSessionEvaluationHighDto>();

            GeneralHelper.ValidateUserBellongToASchool(AbpSession);

            var tenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
            if (tenant == null)
            {
                Logger.Error(L("TheUserMustBellongToATenantToUseThisOption"));
                return null;
            }

            if (!tenant.PeriodId.HasValue)
                throw new UserFriendlyException(L("ToUseThisOptionTheSchoolMustHasSelectedTheCurrentPeriod"));

            if (!input.SubjectHighId.HasValue)
                return ouput;

            if (!input.EvaluationPositionHighId.HasValue)
                return ouput;

            try
            {

                var EvaluationOrderHighId = _evaluationPositionHighRepository.Get(input.EvaluationPositionHighId.Value).EvaluationOrderHighs.FirstOrDefault().Id;

                ouput.studentCount = _courseEnrollmentStudentsRepository
                    .GetAllList(x => x.CourseId == input.CourseId
                             && x.EnrollmentStudents.PeriodId == tenant.PreviousPeriodId)
                    .Count();

                if (input.SessionId.HasValue)
                {
                    var studentOfSession = (await _courseEnrollmentStudentsRepository
                        .GetAllListAsync(x => x.CourseId == input.CourseId
                                           && x.SessionId == input.SessionId
                                           && x.EnrollmentStudents.PeriodId == tenant.PreviousPeriodId))
                        .OrderBy(x => x?.Sequence)
                        .ThenBy(x => x.EnrollmentStudents?.Students?.LastName)
                        .ToList();

                    var Califications = await _evaluationHighRepository
                        .GetAllListAsync(x => x.EvaluationOrderHighId == EvaluationOrderHighId
                                           && x.SubjectHighId == input.SubjectHighId);

                    foreach (var item in studentOfSession)
                    {
                        StudentSessionEvaluationHighDto st = new StudentSessionEvaluationHighDto();

                        st.Students = new Students.Dto.StudentDto();
                        st.Students.FirstName = item.EnrollmentStudents.Students.FirstName;
                        st.Students.LastName = item.EnrollmentStudents.Students.LastName;
                        st.Students.Id = item.EnrollmentStudents.Students.Id;
                        var StudentScore = Califications.FirstOrDefault(x => x.EnrollmentStudentId == item.EnrollmentStudentId);
                        if (StudentScore != null)
                        {
                            st.Expression = StudentScore.Expression;
                            st.Idd = StudentScore.Id;
                        }

                        if (item.Sequence.HasValue)
                            st.Sequence = item.Sequence.Value;

                        st.Id = item.EnrollmentStudents.Id;
                        studentListOrdened.Add(st);
                    }
                }

                ouput.StudentEvaluationHigh = studentListOrdened.OrderBy(x => x.LastName).ToList();
            }
            catch (Exception err)
            {
                Logger.Error(err.Message);
                throw new UserFriendlyException(err.Message);
            }

            return ouput;
        }


        public async Task<GetInitialValuesOuput> GetInitialValues()
        {
            GetInitialValuesOuput output = new GetInitialValuesOuput();

            var coursesQuery = await _courseRepository.GetAllListAsync(x => x.IsActive == true);
            var coursesData = coursesQuery.ToList().Select(x => new ComboBoxObject(x.Id, x.Name)).ToList();
            output.Courses = coursesData;


            var sessionQuery = await _courseSessionRepository.GetAllListAsync(x => x.IsActive == true);
            var sessionData = sessionQuery.ToList().Select(x => new ComboBoxObject(x.Id, x.Name)).ToList();
            output.Sessions = sessionData;

            return output;
        }

        public async Task<GetInitialValuesOuput> GetSessionsByCourse(GetSessionsByCourseInput input)
        {
            GetInitialValuesOuput output = new GetInitialValuesOuput();

            var sessionQuery = await _courseSessionRepository.GetAllListAsync(x => x.IsActive == true && x.CourseId == input.CourseId);
            var sessionData = sessionQuery.ToList().Select(x => new ComboBoxObject(x.Id, x.Name)).ToList();
            output.Sessions = sessionData;

            return output;
        }

        public async Task<GetAllStudentByCourseOuput> InsertStudentInSessionAndGetAllStudentByCourse(GetAllStudentByCourseInput input)
        {
            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();

            var enrollmentStudent = _courseEnrollmentStudentsRepository.GetAllList(x => x.EnrollmentStudentId == input.EnrollmentStudentId).FirstOrDefault();
            if (enrollmentStudent != null)
                enrollmentStudent.SessionId = input.SessionId;
            CurrentUnitOfWork.SaveChanges();

            ouput = await GetAllStudentByCourse(input);
            return ouput;
        }

        public async Task<GetAllStudentByCourseOuput> InsertAllStudentInSessionAndGetAllStudentByCourse(GetAllStudentByCourseInput2 input)
        {
            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();

            GetAllStudentByCourseInput inputAlternativo = new GetAllStudentByCourseInput()
            {
                CourseId = input.CourseId,
                SessionId = input.SessionId
            };

            var enrollmentStudents = _courseEnrollmentStudentsRepository.GetAllList(x => input.EnrollmentStudentIds.Contains(x.EnrollmentStudentId));
            if (input.SessionId.HasValue)
                foreach (var enrollmentStudent in enrollmentStudents)
                {
                    if (enrollmentStudent != null)
                        enrollmentStudent.SessionId = input.SessionId;
                }
            CurrentUnitOfWork.SaveChanges();

            ouput = await GetAllStudentByCourse(inputAlternativo);
            return ouput;
        }


        public async Task<GetAllStudentByCourseOuput> RemoveStudentFromSessionAndGetAllStudentByCourse(GetAllStudentByCourseInput input)
        {

            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();

            var enrollmentStudent = _courseEnrollmentStudentsRepository.GetAllList(x => x.EnrollmentStudentId == input.EnrollmentStudentId).FirstOrDefault();
            if (enrollmentStudent != null)
            {
                enrollmentStudent.SessionId = null;
                enrollmentStudent.Sequence = null;
                CurrentUnitOfWork.SaveChanges();
            }

            ouput = await GetAllStudentByCourse(input);
            return ouput;
        }

        public async Task<GetAllStudentByCourseOuput> RemoveAllStudentFromSessionAndGetAllStudentByCourse(GetAllStudentByCourseInput2 input)
        {
            GetAllStudentByCourseOuput ouput = new GetAllStudentByCourseOuput();

            GetAllStudentByCourseInput inputAlternativo = new GetAllStudentByCourseInput()
            {
                CourseId = input.CourseId,
                SessionId = input.SessionId
            };

            var enrollmentStudents = _courseEnrollmentStudentsRepository.GetAllList(x => input.EnrollmentStudentIds.Contains(x.EnrollmentStudentId));
            if (input.SessionId.HasValue)
                foreach (var enrollmentStudent in enrollmentStudents)
                {
                    if (enrollmentStudent != null)
                    {
                        enrollmentStudent.SessionId = null;
                        enrollmentStudent.Sequence = null;
                    }
                }
            CurrentUnitOfWork.SaveChanges();

            ouput = await GetAllStudentByCourse(inputAlternativo);
            return ouput;
        }
    }
}

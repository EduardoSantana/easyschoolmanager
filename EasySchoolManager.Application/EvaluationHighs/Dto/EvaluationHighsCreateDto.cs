//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.EvaluationHighs.Dto 
{
        [AutoMap(typeof(Models.EvaluationHighs))] 
        public class CreateEvaluationHighDto : EntityDto<int> 
        {

              public int SubjectHighId {get;set;} 

              public int EnrollmentStudentId {get;set;}

              public int StudentId { get; set; }
        
              public int EvaluationOrderHighId {get;set;} 

              public int Expression {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.EvaluationHighs.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.EvaluationHighs 
{ 
	[AbpAuthorize(PermissionNames.Pages_EvaluationHighs)] 
	public class EvaluationHighAppService : AsyncCrudAppService<Models.EvaluationHighs, EvaluationHighDto, int, PagedResultRequestDto, CreateEvaluationHighDto, UpdateEvaluationHighDto>, IEvaluationHighAppService 
	{ 
		private readonly IRepository<Models.EvaluationHighs, int> _evaluationHighRepository;
		
			private readonly IRepository<Models.SubjectHighs, int> _subjectHighRepository;
			private readonly IRepository<Models.EvaluationOrderHighs, int> _evaluationOrderHighRepository;
			private readonly IRepository<Models.EvaluationPositionHighs, int> _evaluationPositionHighRepository;
			private readonly IRepository<Models.EnrollmentStudents, int> _enrollmentStudentRepository;
			private readonly MultiTenancy.TenantManager _tenantManager;



		public EvaluationHighAppService( 
			IRepository<Models.EvaluationHighs, int> repository, 
			IRepository<Models.EvaluationHighs, int> evaluationHighRepository ,
			IRepository<Models.SubjectHighs, int> subjectHighRepository
,
			IRepository<Models.EvaluationOrderHighs, int> evaluationOrderHighRepository,
			IRepository<Models.EvaluationPositionHighs, int> evaluationPositionHighRepository,
			IRepository<Models.EnrollmentStudents, int> enrollmentStudentRepository,
			MultiTenancy.TenantManager tenantManager

			) 
			: base(repository) 
		{ 
			_evaluationHighRepository = evaluationHighRepository; 
			
			_subjectHighRepository = subjectHighRepository;

			_evaluationPositionHighRepository = evaluationPositionHighRepository;
			_evaluationOrderHighRepository = evaluationOrderHighRepository;

			_enrollmentStudentRepository = enrollmentStudentRepository;

			_tenantManager = tenantManager;

		} 
		public override async Task<EvaluationHighDto> Get(EntityDto<int> input) 
		{ 
			var user = await base.Get(input); 
			return user; 
		} 
		public override async Task<EvaluationHighDto> Create(CreateEvaluationHighDto input) 
		{ 
			CheckCreatePermission();

			//buscando el enrollmentStudentId del estudiante

			//Necesario buscar el perido pues varios a�os de un estudiante tiene varios enrollmentStudent
			var peract = _tenantManager.Tenants.FirstOrDefault();
			var studenroll = _enrollmentStudentRepository.GetAllList(x => x.StudentId == input.StudentId && x.PeriodId==peract.PeriodId);
			var studenrol = studenroll.FirstOrDefault();
			input.EnrollmentStudentId = studenrol.Id;

			var evaluationHigh = ObjectMapper.Map<Models.EvaluationHighs>(input); 

			//llamar metodo geExiste
		  if (  getExiste(input.SubjectHighId, input.EnrollmentStudentId, input.EvaluationOrderHighId, input.Expression))
			{
				return MapToEntityDto(evaluationHigh);
			}
			try{
			  await _evaluationHighRepository.InsertAsync(evaluationHigh); 
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}
			return MapToEntityDto(evaluationHigh); 
		}


		public async Task<EvaluationHighDto> CreateByPos(EvaluationHighsCreateByPosDto input)
		{
			CheckCreatePermission();

			//buscando el enrollmentStudentId del estudiante

			//Necesario buscar el perido pues varios a�os de un estudiante tiene varios enrollmentStudent
			var peract = _tenantManager.Tenants.FirstOrDefault();
			var studenroll = _enrollmentStudentRepository.GetAllList(x => x.StudentId == input.StudentId && x.PeriodId == peract.PeriodId);
			var studenrol = studenroll.FirstOrDefault();
			input.EnrollmentStudentId = studenrol.Id;

			var evaluationPosition = _evaluationPositionHighRepository.Get(input.EvaluationPositionHighId);

			var evaluationHigh = new Models.EvaluationHighs();

			foreach (var item in evaluationPosition.EvaluationOrderHighs)
			{
				evaluationHigh = ObjectMapper.Map<Models.EvaluationHighs>(input);
				evaluationHigh.EvaluationOrderHighId = item.Id;
				try
				{
					if (!getExiste(evaluationHigh.SubjectHighId, evaluationHigh.EnrollmentStudentId, evaluationHigh.EvaluationOrderHighId, evaluationHigh.Expression))
						await _evaluationHighRepository.InsertAsync(evaluationHigh);
				}
				catch (Exception err)
				{
					throw new UserFriendlyException(err.Message);
				}
			}

			
			return MapToEntityDto(evaluationHigh);
		}


		public bool getExiste( int Subj, int EnrEst, int Ord, int exp  )
		{
		
			var exisss = _evaluationHighRepository.GetAllList(x => x.EnrollmentStudentId == EnrEst && x.SubjectHighId == Subj && x.EvaluationOrderHighId == Ord);
			var exiss = exisss.FirstOrDefault();

			if (exiss == null)
			{
				return false;
			}
			else
			{
				exiss.Expression = exp;
				return true;
			}
		}


		public override async Task<EvaluationHighDto> Update(UpdateEvaluationHighDto input) 
		{ 
			CheckUpdatePermission();


			//buscando el enrollmentStudentId del estudiante

			//Necesario buscar el perido pues varios a�os de un estudiante tiene varios enrollmentStudent
			var peract = _tenantManager.Tenants.FirstOrDefault();
			var studenroll = _enrollmentStudentRepository.GetAllList(x => x.StudentId == input.StudentId && x.PeriodId == peract.PeriodId);
			var studenrol = studenroll.FirstOrDefault();
			input.EnrollmentStudentId = studenrol.Id;

			var evaluationHigh = await _evaluationHighRepository.GetAsync(input.Id);
			MapToEntity(input, evaluationHigh); 
			try{
			   await _evaluationHighRepository.UpdateAsync(evaluationHigh); 
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}
			return await Get(input); 
		} 
		public override async Task Delete(EntityDto<int> input) 
		{
			try{
			   var evaluationHigh = await _evaluationHighRepository.GetAsync(input.Id); 
			   await _evaluationHighRepository.DeleteAsync(evaluationHigh);
			}
			catch (Exception err)
			{
				throw new UserFriendlyException(err.Message);
			}			
		} 
		protected override Models.EvaluationHighs MapToEntity(CreateEvaluationHighDto createInput) 
		{ 
			var evaluationHigh = ObjectMapper.Map<Models.EvaluationHighs>(createInput); 
			return evaluationHigh; 
		} 
		protected override void MapToEntity(UpdateEvaluationHighDto input, Models.EvaluationHighs evaluationHigh) 
		{ 
			ObjectMapper.Map(input, evaluationHigh); 
		} 
		protected override IQueryable<Models.EvaluationHighs> CreateFilteredQuery(PagedResultRequestDto input) 
		{ 
			return Repository.GetAll(); 
		} 
		protected  IQueryable<Models.EvaluationHighs> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
		{ 
			var resultado =  Repository.GetAll(); 
			if (!string.IsNullOrEmpty(input.TextFilter)) 
				resultado = resultado.Where(x => x.Expression.ToString().Contains(input.TextFilter)); 
			return resultado; 
		} 
		protected override async Task<Models.EvaluationHighs> GetEntityByIdAsync(int id) 
		{ 
			var evaluationHigh = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
			return await Task.FromResult(evaluationHigh); 
		} 
		protected override IQueryable<Models.EvaluationHighs> ApplySorting(IQueryable<Models.EvaluationHighs> query, PagedResultRequestDto input) 
		{ 
			return query.OrderBy(r => r.Expression.ToString()); 
		} 
 
		protected virtual void CheckErrors(IdentityResult identityResult) 
		{ 
			identityResult.CheckErrors(LocalizationManager); 
		} 
		
		public async Task<PagedResultDto<EvaluationHighDto>> GetAllEvaluationHighs(GdPagedResultRequestDto input) 
		{ 
			PagedResultDto<EvaluationHighDto> ouput = new PagedResultDto<EvaluationHighDto>(); 
			IQueryable<Models.EvaluationHighs> query = query = from x in _evaluationHighRepository.GetAll() 
													   select x; 
			if (!string.IsNullOrEmpty(input.TextFilter)) 
			{ 
				query = from x in _evaluationHighRepository.GetAll() 
						where x.Expression.ToString().Contains(input.TextFilter) 
						select x; 
			} 
			ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
			if (input.SkipCount > 0) 
			{ 
				query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
			}  
			if (input.MaxResultCount > 0) 
			{ 
				query = query.Take(input.MaxResultCount); 
			} 
			ouput.Items = ObjectMapper.Map<List<EvaluationHighs.Dto.EvaluationHighDto>>(query.ToList()); 
			return ouput; 
		} 
		
		[AbpAllowAnonymous]
		public async Task<List<EvaluationHighDto>> GetAllEvaluationHighsForCombo()
		{
			var evaluationHighList = await _evaluationHighRepository.GetAllListAsync(x => x.IsActive == true);

			var evaluationHigh = ObjectMapper.Map<List<EvaluationHighDto>>(evaluationHighList.ToList());

			return evaluationHigh;
		}
		
	} 
} ;
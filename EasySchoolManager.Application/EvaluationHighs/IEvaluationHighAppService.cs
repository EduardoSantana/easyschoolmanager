//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.EvaluationHighs.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.EvaluationHighs
{
      public interface IEvaluationHighAppService : IAsyncCrudAppService<EvaluationHighDto, int, PagedResultRequestDto, CreateEvaluationHighDto, UpdateEvaluationHighDto>
      {
            Task<PagedResultDto<EvaluationHighDto>> GetAllEvaluationHighs(GdPagedResultRequestDto input);
			Task<List<Dto.EvaluationHighDto>> GetAllEvaluationHighsForCombo();
            Task<EvaluationHighDto> CreateByPos(EvaluationHighsCreateByPosDto input);
      }
}
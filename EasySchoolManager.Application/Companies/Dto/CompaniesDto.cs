//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Companies.Dto 
{
        [AutoMap(typeof(Models.Companies))] 
        public class CompanyDto : EntityDto<long> 
        {
              public string RNC {get;set;} 
              public string Name {get;set;} 
              public string CommercialName {get;set;} 
              public string CommercialActivity {get;set;} 
              public string CompanyDate {get;set;} 
              public string Status {get;set;} 
              public string PaymentSystem {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
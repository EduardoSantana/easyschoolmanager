//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Companies.Dto 
{
        [AutoMap(typeof(Models.Companies))] 
        public class UpdateCompanyDto : EntityDto<long> 
        {

              [StringLength(15)]  
              public string RNC {get;set;} 

              [StringLength(200)]  
              public string Name {get;set;} 

              [StringLength(200)]  
              public string CommercialName {get;set;} 

              [StringLength(200)]  
              public string CommercialActivity {get;set;} 

              [StringLength(10)]  
              public string CompanyDate {get;set;} 

              [StringLength(15)]  
              public string Status {get;set;} 

              [StringLength(100)]  
              public string PaymentSystem {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
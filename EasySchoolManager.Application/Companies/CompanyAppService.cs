//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Companies.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Companies
{
    [AbpAuthorize(PermissionNames.Pages_Companies)]
    public class CompanyAppService : AsyncCrudAppService<Models.Companies, CompanyDto, long, PagedResultRequestDto, CreateCompanyDto, UpdateCompanyDto>, ICompanyAppService
    {
        private readonly IRepository<Models.Companies, long> _companyRepository;



        public CompanyAppService(
            IRepository<Models.Companies, long> repository,
            IRepository<Models.Companies, long> companyRepository
            )
            : base(repository)
        {
            _companyRepository = companyRepository;



        }
        public override async Task<CompanyDto> Get(EntityDto<long> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<CompanyDto> Create(CreateCompanyDto input)
        {
            CheckCreatePermission();
            var company = ObjectMapper.Map<Models.Companies>(input);
            try
            {
                await _companyRepository.InsertAsync(company);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(company);
        }
        public override async Task<CompanyDto> Update(UpdateCompanyDto input)
        {
            CheckUpdatePermission();
            var company = await _companyRepository.GetAsync(input.Id);
            MapToEntity(input, company);
            try
            {
                await _companyRepository.UpdateAsync(company);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<long> input)
        {
            try
            {
                var company = await _companyRepository.GetAsync(input.Id);
                await _companyRepository.DeleteAsync(company);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.Companies MapToEntity(CreateCompanyDto createInput)
        {
            var company = ObjectMapper.Map<Models.Companies>(createInput);
            return company;
        }
        protected override void MapToEntity(UpdateCompanyDto input, Models.Companies company)
        {
            ObjectMapper.Map(input, company);
        }
        protected override IQueryable<Models.Companies> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.Companies> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.Companies> GetEntityByIdAsync(long id)
        {
            var company = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(company);
        }
        protected override IQueryable<Models.Companies> ApplySorting(IQueryable<Models.Companies> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Name);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<CompanyDto>> GetAllCompanies(GdPagedResultRequestDto input)
        {
            PagedResultDto<CompanyDto> ouput = new PagedResultDto<CompanyDto>();
            IQueryable<Models.Companies> query = query = from x in _companyRepository.GetAll()
                                                         select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _companyRepository.GetAll()
                        where x.Name.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Companies.Dto.CompanyDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<CompanyDto>> GetAllCompaniesForCombo()
        {
            var companyList = await _companyRepository.GetAllListAsync(x => x.IsActive == true);

            var company = ObjectMapper.Map<List<CompanyDto>>(companyList.ToList());

            return company;
        }

        public async Task<CompanyDto> GetCompanyByRNC(getCompanyByRNCInput input)
        {
            CompanyDto output = new CompanyDto() {CommercialName = "No Encontrado", Name = "No Encontrado"};

            var rnc = await _companyRepository.FirstOrDefaultAsync(x => x.RNC == input.Rnc);
            if (rnc != null)
                output = ObjectMapper.Map<CompanyDto>(rnc);

            return output;
        }
    }
};
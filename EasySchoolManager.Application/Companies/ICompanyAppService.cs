//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Companies.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Companies
{
    public interface ICompanyAppService : IAsyncCrudAppService<CompanyDto, long, PagedResultRequestDto, CreateCompanyDto, UpdateCompanyDto>
    {
        Task<PagedResultDto<CompanyDto>> GetAllCompanies(GdPagedResultRequestDto input);
        Task<List<Dto.CompanyDto>> GetAllCompaniesForCombo();

        Task<CompanyDto> GetCompanyByRNC(getCompanyByRNCInput input);
    }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.ArticlesTypes.Dto 
{
        [AutoMap(typeof(Models.ArticlesTypes))] 
        public class UpdateArticlesTypeDto : EntityDto<int> 
        {

              [StringLength(50)]  
              public string Description {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
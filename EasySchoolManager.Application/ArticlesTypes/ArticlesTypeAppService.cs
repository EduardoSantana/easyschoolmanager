//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.ArticlesTypes.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ArticlesTypes 
{ 
    [AbpAuthorize(PermissionNames.Pages_ArticlesTypes)] 
    public class ArticlesTypeAppService : AsyncCrudAppService<Models.ArticlesTypes, ArticlesTypeDto, int, PagedResultRequestDto, CreateArticlesTypeDto, UpdateArticlesTypeDto>, IArticlesTypeAppService 
    { 
        private readonly IRepository<Models.ArticlesTypes, int> _articlesTypeRepository;
		


        public ArticlesTypeAppService( 
            IRepository<Models.ArticlesTypes, int> repository, 
            IRepository<Models.ArticlesTypes, int> articlesTypeRepository 
            ) 
            : base(repository) 
        { 
            _articlesTypeRepository = articlesTypeRepository; 
			

			
        } 
        public override async Task<ArticlesTypeDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ArticlesTypeDto> Create(CreateArticlesTypeDto input) 
        { 
            CheckCreatePermission(); 
            var articlesType = ObjectMapper.Map<Models.ArticlesTypes>(input); 
			try{
              await _articlesTypeRepository.InsertAsync(articlesType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(articlesType); 
        } 
        public override async Task<ArticlesTypeDto> Update(UpdateArticlesTypeDto input) 
        { 
            CheckUpdatePermission(); 
            var articlesType = await _articlesTypeRepository.GetAsync(input.Id);
            MapToEntity(input, articlesType); 
		    try{
               await _articlesTypeRepository.UpdateAsync(articlesType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var articlesType = await _articlesTypeRepository.GetAsync(input.Id); 
               await _articlesTypeRepository.DeleteAsync(articlesType);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.ArticlesTypes MapToEntity(CreateArticlesTypeDto createInput) 
        { 
            var articlesType = ObjectMapper.Map<Models.ArticlesTypes>(createInput); 
            return articlesType; 
        } 
        protected override void MapToEntity(UpdateArticlesTypeDto input, Models.ArticlesTypes articlesType) 
        { 
            ObjectMapper.Map(input, articlesType); 
        } 
        protected override IQueryable<Models.ArticlesTypes> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ArticlesTypes> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ArticlesTypes> GetEntityByIdAsync(int id) 
        { 
            var articlesType = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(articlesType); 
        } 
        protected override IQueryable<Models.ArticlesTypes> ApplySorting(IQueryable<Models.ArticlesTypes> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Description); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ArticlesTypeDto>> GetAllArticlesTypes(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ArticlesTypeDto> ouput = new PagedResultDto<ArticlesTypeDto>(); 
            IQueryable<Models.ArticlesTypes> query = query = from x in _articlesTypeRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _articlesTypeRepository.GetAll() 
                        where x.Description.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ArticlesTypes.Dto.ArticlesTypeDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<ArticlesTypeDto>> GetAllArticlesTypesForCombo()
        {
            var articlesTypeList = await _articlesTypeRepository.GetAllListAsync(x => x.IsActive == true);

            var articlesType = ObjectMapper.Map<List<ArticlesTypeDto>>(articlesTypeList.ToList());

            return articlesType;
        }
		
    } 
} ;
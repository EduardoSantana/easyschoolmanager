//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.ArticlesTypes.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.ArticlesTypes
{
      public interface IArticlesTypeAppService : IAsyncCrudAppService<ArticlesTypeDto, int, PagedResultRequestDto, CreateArticlesTypeDto, UpdateArticlesTypeDto>
      {
            Task<PagedResultDto<ArticlesTypeDto>> GetAllArticlesTypes(GdPagedResultRequestDto input);
			Task<List<Dto.ArticlesTypeDto>> GetAllArticlesTypesForCombo();
      }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Banks.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Banks 
{ 
    [AbpAuthorize(PermissionNames.Pages_Banks)] 
    public class BankAppService : AsyncCrudAppService<Models.Banks, BankDto, int, PagedResultRequestDto, CreateBankDto, UpdateBankDto>, IBankAppService 
    { 
        private readonly IRepository<Models.Banks, int> _bankRepository;

        public BankAppService( 
            IRepository<Models.Banks, int> repository, 
            IRepository<Models.Banks, int> bankRepository 
            ) 
            : base(repository) 
        { 
            _bankRepository = bankRepository; 
        } 
        public override async Task<BankDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<BankDto> Create(CreateBankDto input) 
        { 
            CheckCreatePermission(); 
            var bank = ObjectMapper.Map<Models.Banks>(input); 
			try{
              await _bankRepository.InsertAsync(bank); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(bank); 
        } 
        public override async Task<BankDto> Update(UpdateBankDto input) 
        { 
            CheckUpdatePermission(); 
            var bank = await _bankRepository.GetAsync(input.Id);
            MapToEntity(input, bank); 
		    try{
               await _bankRepository.UpdateAsync(bank); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var bank = await _bankRepository.GetAsync(input.Id); 
               await _bankRepository.DeleteAsync(bank);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Banks MapToEntity(CreateBankDto createInput) 
        { 
            var bank = ObjectMapper.Map<Models.Banks>(createInput); 
            return bank; 
        } 
        protected override void MapToEntity(UpdateBankDto input, Models.Banks bank) 
        { 
            ObjectMapper.Map(input, bank); 
        } 
        protected override IQueryable<Models.Banks> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Banks> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Banks> GetEntityByIdAsync(int id) 
        { 
            var bank = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(bank); 
        } 
        protected override IQueryable<Models.Banks> ApplySorting(IQueryable<Models.Banks> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<BankDto>> GetAllBanks(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<BankDto> ouput = new PagedResultDto<BankDto>(); 
            IQueryable<Models.Banks> query = query = from x in _bankRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _bankRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Banks.Dto.BankDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<BankDto>> GetAllBanksForCombo()
        {
            var bankList = await _bankRepository.GetAllListAsync(x => x.IsActive == true);

            var bank = ObjectMapper.Map<List<BankDto>>(bankList.ToList());

            return bank;
        }
		
    } 
} ;
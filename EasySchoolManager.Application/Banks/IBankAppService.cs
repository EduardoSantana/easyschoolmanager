//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Banks.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Banks
{
      public interface IBankAppService : IAsyncCrudAppService<BankDto, int, PagedResultRequestDto, CreateBankDto, UpdateBankDto>
      {
            Task<PagedResultDto<BankDto>> GetAllBanks(GdPagedResultRequestDto input);
			Task<List<Dto.BankDto>> GetAllBanksForCombo();
      }
}
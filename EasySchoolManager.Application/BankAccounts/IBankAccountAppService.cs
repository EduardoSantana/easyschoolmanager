//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.BankAccounts.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.BankAccounts
{
    public interface IBankAccountAppService : IAsyncCrudAppService<BankAccountDto, int, PagedResultRequestDto, CreateBankAccountDto, UpdateBankAccountDto>
    {
        Task<PagedResultDto<BankAccountDto>> GetAllBankAccounts(GdPagedResultRequestDto input);
        Task<List<Dto.BankAccountDto>> GetAllBankAccountsForCombo();
        Task<List<Dto.BankAccountDto>> GetAllBankAccountsForComboByBankId(getAllBankAccountsForComboByBankIdinputDto input);
        Task<List<BankAccountDto>> GetAllBankAccountsForComboByRegionId();
        Task<List<Dto.BankAccountDto>> GetAllBankAccountsForComboByBankIdAndRegionId(getAllBankAccountsForComboByBankIdinputDto input);
    }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.BankAccounts.Dto 
{
        [AutoMap(typeof(Models.BankAccounts))] 
        public class CreateBankAccountDto : EntityDto<int> 
        {

              [StringLength(25)] 
              public string Number {get;set;} 

              public int BanktId {get;set;} 

              [StringLength(100)] 
              public string Description {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;}
              public int? RegionId { get; set; }

    }
}
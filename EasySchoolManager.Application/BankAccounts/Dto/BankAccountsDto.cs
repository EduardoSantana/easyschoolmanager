//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.BankAccounts.Dto 
{
        [AutoMap(typeof(Models.BankAccounts))] 
        public class BankAccountDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public string Number {get;set;} 
              public int BanktId {get;set;} 
              public string Description {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;}
              public string Banks_Name { get; set; }
              public int? RegionId { get; set; }
              public string Regions_Name { get; set; }

    }
}
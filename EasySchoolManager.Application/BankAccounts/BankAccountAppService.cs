//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.BankAccounts.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.MultiTenancy;

namespace EasySchoolManager.BankAccounts
{
    [AbpAuthorize(PermissionNames.Pages_BankAccounts)]
    public class BankAccountAppService : AsyncCrudAppService<Models.BankAccounts, BankAccountDto, int, PagedResultRequestDto, CreateBankAccountDto, UpdateBankAccountDto>, IBankAccountAppService
    {
        private readonly IRepository<Models.BankAccounts, int> _bankAccountRepository;

        private readonly IRepository<Models.Banks, int> _bankRepository;
        private readonly TenantManager _tenantManager;


        public BankAccountAppService(
            IRepository<Models.BankAccounts, int> repository,
            IRepository<Models.BankAccounts, int> bankAccountRepository,
            IRepository<Models.Banks, int> bankRepository,
             TenantManager tenantManager

            )
            : base(repository)
        {
            base.LocalizationSourceName = EasySchoolManagerConsts.LocalizationSourceName;
         
            _bankAccountRepository = bankAccountRepository;

            _bankRepository = bankRepository;
            _tenantManager = tenantManager;

        }
        public override async Task<BankAccountDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<BankAccountDto> Create(CreateBankAccountDto input)
        {
            CheckCreatePermission();
            var bankAccount = ObjectMapper.Map<Models.BankAccounts>(input);
            try
            {
                await _bankAccountRepository.InsertAsync(bankAccount);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(bankAccount);
        }
        public override async Task<BankAccountDto> Update(UpdateBankAccountDto input)
        {
            CheckUpdatePermission();
            var bankAccount = await _bankAccountRepository.GetAsync(input.Id);
            MapToEntity(input, bankAccount);
            try
            {
                await _bankAccountRepository.UpdateAsync(bankAccount);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var bankAccount = await _bankAccountRepository.GetAsync(input.Id);
                await _bankAccountRepository.DeleteAsync(bankAccount);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.BankAccounts MapToEntity(CreateBankAccountDto createInput)
        {
            var bankAccount = ObjectMapper.Map<Models.BankAccounts>(createInput);
            return bankAccount;
        }
        protected override void MapToEntity(UpdateBankAccountDto input, Models.BankAccounts bankAccount)
        {
            ObjectMapper.Map(input, bankAccount);
        }
        protected override IQueryable<Models.BankAccounts> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.BankAccounts> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.BankAccounts> GetEntityByIdAsync(int id)
        {
            var bankAccount = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(bankAccount);
        }
        protected override IQueryable<Models.BankAccounts> ApplySorting(IQueryable<Models.BankAccounts> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Description);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<BankAccountDto>> GetAllBankAccounts(GdPagedResultRequestDto input)
        {
            PagedResultDto<BankAccountDto> ouput = new PagedResultDto<BankAccountDto>();
            IQueryable<Models.BankAccounts> query = query = from x in _bankAccountRepository.GetAll()
                                                            select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _bankAccountRepository.GetAll()
                        where x.Description.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<BankAccounts.Dto.BankAccountDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<BankAccountDto>> GetAllBankAccountsForCombo()
        {
            var bankAccountList = await _bankAccountRepository.GetAllListAsync(x => x.IsActive == true);

            var bankAccount = ObjectMapper.Map<List<BankAccountDto>>(bankAccountList.ToList());

            return bankAccount;
        }

        [AbpAllowAnonymous]
        public async Task<List<BankAccountDto>> GetAllBankAccountsForComboByRegionId()
        {
            List<Dto.BankAccountDto> bankAccount = new List<BankAccountDto>();
            try
            {
                Helpers.GeneralHelper.ValidateUserBellongToASchool(AbpSession);

                var currentTenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
                if (currentTenant.Cities?.Provinces?.RegionId == null)
                    throw new UserFriendlyException(L("ThisSchoolDoesNotHaveRegionAssigned"));

                var regionId = currentTenant.Cities?.Provinces?.RegionId;
                var bankAccountList = await _bankAccountRepository.GetAllListAsync(x => x.IsActive == true
                && x.RegionId == regionId.Value);

                bankAccount = ObjectMapper.Map<List<BankAccountDto>>(bankAccountList.ToList());
            }
            catch (Exception)
            {
                throw new UserFriendlyException(L("ThisSchoolDoesNotHaveRegionAssigned"));
            }
            return bankAccount;
        }

        [AbpAllowAnonymous]
        public async Task<List<Dto.BankAccountDto>> GetAllBankAccountsForComboByBankId(getAllBankAccountsForComboByBankIdinputDto input)
        {
            var cityList = await _bankAccountRepository.GetAllListAsync(x => x.IsActive == true && x.BanktId == input.BankId);

            var city = ObjectMapper.Map<List<BankAccountDto>>(cityList.ToList());

            return city;
        }


        [AbpAllowAnonymous]
        public async Task<List<Dto.BankAccountDto>> GetAllBankAccountsForComboByBankIdAndRegionId(getAllBankAccountsForComboByBankIdinputDto input)
        {
            List<Dto.BankAccountDto> bankAccounts = new List<BankAccountDto>();
            try
            {
                Helpers.GeneralHelper.ValidateUserBellongToASchool(AbpSession);

                var currentTenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
                if (currentTenant.Cities?.Provinces?.RegionId == null)
                    throw new UserFriendlyException(L("ThisSchoolDoesNotHaveRegionAssigned"));

                var regionId = currentTenant.Cities?.Provinces?.RegionId;

                var cityList = await _bankAccountRepository.GetAllListAsync(x => x.IsActive == true && x.BanktId == input.BankId
                && x.RegionId == regionId.Value
                );

                bankAccounts = ObjectMapper.Map<List<BankAccountDto>>(cityList.ToList());
            }
            catch (Exception)
            {
                throw new UserFriendlyException(L("ThisSchoolDoesNotHaveRegionAssigned"));
            }
            return bankAccounts;
        }
    }
};
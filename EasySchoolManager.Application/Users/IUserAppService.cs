using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.Roles.Dto;
using EasySchoolManager.Users.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Authorization.Users;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace EasySchoolManager.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();
        Task<PagedResultDto<UserDto>> GetAllUserByTenant(GdPagedResultUserRequestDto input);
        Task<GetTenantListForUserDto> GetTenantListForUser(GetTenantListForUserInput input);
        //IdentityResult SetRoles(User user, string[] roleNames);
        Task<List<UserDto>> GetAllUserForMessage(GetUserForMessageInput input);

    }
}
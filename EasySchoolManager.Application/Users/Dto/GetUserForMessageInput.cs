﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Users.Dto
{
    public class GetUserForMessageInput
    {
        public int? TenantId { get; set; }
        public string TextFilter { get; set; }
    }
}

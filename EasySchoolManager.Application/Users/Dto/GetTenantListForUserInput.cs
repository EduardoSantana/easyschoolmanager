﻿using System.Collections.Generic;

namespace EasySchoolManager.Users.Dto
{
    public class GetTenantListForUserInput
    {
        public string UserName { get; set; }
    }
}
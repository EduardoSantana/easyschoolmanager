﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.Authorization.Users;
using EasySchoolManager.Models;
using EasySchoolManager.UserTenants.Dto;
using System.Collections.Generic;
using AutoMapper;

namespace EasySchoolManager.Users.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserDto : EntityDto<long>
    {
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }


        public bool IsActive { get; set; }

        public string PhoneNumber { get; set; }

        public string FullName { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public DateTime CreationTime { get; set; }

        public string[] Roles { get; set; }
        public int? TenantId { get; set; }

        public string Tenant_Name { get; set; }

        public int? PositionId { get; set; }

        public string Password { get; set; }

        public long? ParentUserId { get; set; }

        public List<UserTenantDto> UserTenants { get; set; }

        [IgnoreMap]
        public string Picture { get; set; }

    }
}

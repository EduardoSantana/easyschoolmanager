﻿using System.Collections.Generic;

namespace EasySchoolManager.Users.Dto
{
    public class GetTenantListForUserDto
    {
        public List<string> TenantNames { get; set; }
    }
}
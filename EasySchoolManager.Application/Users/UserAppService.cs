using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using EasySchoolManager.Authorization.Roles;
using EasySchoolManager.Authorization.Users;
using EasySchoolManager.Roles.Dto;
using EasySchoolManager.Users.Dto;
using Microsoft.AspNet.Identity;
using Abp.Domain.Uow;
using EasySchoolManager.GD;
using Abp.UI;
using EasySchoolManager.MultiTenancy;
using System;
using EasySchoolManager.Helpers;
using EasySchoolManager.Models;

namespace EasySchoolManager.Users
{
    [AbpAuthorize]
    public class UserAppService : AsyncCrudAppService<User, UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>, IUserAppService
    {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly TenantManager _tenantManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Models.UserTenants, long> _userTenantRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<UserPictures, long> _UserPicturesRepository;

        public UserAppService(
            IRepository<User, long> repository,
            UserManager userManager,
            IRepository<Role> roleRepository,
            RoleManager roleManager,
            IRepository<Models.UserTenants, long> userTenantRepository,
            IRepository<UserRole, long> userRoleRepository,
             IRepository<UserPictures, long> UserPicturesRepository
            )
            : base(repository)
        {
            _userManager = userManager;
            _roleRepository = roleRepository;
            _roleManager = roleManager;
            _userTenantRepository = userTenantRepository;
            _userRoleRepository = userRoleRepository;
            _UserPicturesRepository = UserPicturesRepository;
        }

        public override async Task<UserDto> Get(EntityDto<long> input)
        {
            UserDto user = null;
            IList<string> userRoles = new List<string>();

            if (AbpSession.TenantId.HasValue)
            {
                user = await base.Get(input);
                //user.Picture = _UserPicturesRepository.FirstOrDefault(t => t.UserId == user.Id)?.Picture;
            }

            //Disabling MayHaveTenant filter, so we can reach to all users
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var usertenants = new List<Models.UserTenants>();
                if (!AbpSession.TenantId.HasValue)
                {
                    user = await base.Get(input);
                    //user.Picture = _UserPicturesRepository.FirstOrDefault(t => t.UserId == user.Id)?.Picture;
                    userRoles = await _userManager.GetRolesAsync(user.Id);
                    usertenants = _userTenantRepository.GetAllList(x => x.UserId == input.Id).ToList();
                }
                else if (user.ParentUserId.HasValue)
                {
                    usertenants = _userTenantRepository.GetAllList(x => x.UserId == user.ParentUserId).ToList();
                }
                user.UserTenants = ObjectMapper.Map<List<UserTenants.Dto.UserTenantDto>>(usertenants);
                user.UserTenants.Add(new UserTenants.Dto.UserTenantDto {
                    Id = -1,
                    Tenant_Name = "PRINCIPAL",
                    Tenant_TenancyName = "PRINCIPAL",
                    TenantId = -1});
            }
            if (AbpSession.TenantId.HasValue)
                userRoles = await _userManager.GetRolesAsync(user.Id);
            user.Roles = userRoles.Select(ur => ur).ToArray();
            return user;
        }

        public override async Task<UserDto> Create(CreateUserDto input)
        {
            CheckCreatePermission();

            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var test1 = await _userManager.FindByEmailAsync(input.EmailAddress);
                if (test1 != null)
                {
                    throw new UserFriendlyException("Ooppps! Email Address Duplicated!", "You are trying to save a duplicated email address...");
                }

                test1 = await _userManager.FindByNameAsync(input.UserName);
                if (test1 != null)
                {
                    throw new UserFriendlyException("Ooppps! User Name Duplicated!", "You are trying to save a duplicated User Name...");
                }
            }
            var user = ObjectMapper.Map<User>(input);
            if (input.Picture != null)
            {
                user.UserPictures = new List<Models.UserPictures>();
                user.UserPictures.Add(new Models.UserPictures { Picture = input.Picture });
            }
            if (AbpSession.TenantId.HasValue)
            {
                user.TenantId = AbpSession.TenantId;
            }
            else
            {
                user.TenantId = (user.TenantId.HasValue ? user.TenantId.Value : AbpSession.TenantId);
            }

            user.Password = new PasswordHasher().HashPassword(input.UserName);
            user.IsEmailConfirmed = true;
            user.MustChangePassword = true;
            user.CreatorUserId = AbpSession.UserId;


            CheckErrors(await _userManager.CreateAsync(user));

            //Assign roles
            user.Roles = new Collection<UserRole>();

            //foreach (var roleName in input.RoleNames)
            //{
            //    var role = await _roleManager.GetRoleByNameAsync(roleName);
            //    user.Roles.Add(new UserRole(AbpSession.TenantId, user.Id, role.Id));
            //}

            if (input.RoleNames != null)
            {
                SetRoles(user, input.RoleNames);
                if (input.TenantId.HasValue)
                {
                    SetRolesEspecial(user, input.RoleNames, input.TenantId.Value);
                }
            }



            return MapToEntityDto(user);
        }

        public override async Task<UserDto> Update(UpdateUserDto input)
        {
            CheckUpdatePermission();
            var createdRoles = false;
            User user;
            List<String> rolesDelUsuario;
            if (!AbpSession.TenantId.HasValue)
            {
                //Disabling MayHaveTenant filter, so we can reach to all users
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                {
                    user = await _userManager.GetUserByIdAsync(input.Id);

                    MapToEntity(input, user);
                    if (AbpSession.TenantId.HasValue)
                    {
                        input.TenantId = AbpSession.TenantId;
                        user.TenantId = AbpSession.TenantId;
                    }
                    else
                    {
                        input.TenantId = (input.TenantId.HasValue ? input.TenantId.Value : AbpSession.TenantId);
                        user.TenantId = (user.TenantId.HasValue ? user.TenantId.Value : AbpSession.TenantId);
                    }

                    CheckErrors(await _userManager.UpdateAsync(user));

                    rolesDelUsuario = _userManager.GetRoles(user.Id).ToList();

                    var rolesInsertados = input.RoleNames.ToList();

                    //foreach (var item in rolesDelUsuario)
                    //{
                    //    _userManager.RemoveFromRole(user.Id, item);
                    //}
                    RemoveAllRolesFromUser(user);

                    //CurrentUnitOfWork.SaveChanges();
                    if (input.TenantId.HasValue)
                    {
                        //ChangeTenantService cts = new ChangeTenantService(AbpSession);
                        //cts.Test(input.TenantId.Value);

                        if (input.RoleNames != null)
                        {
                            SetRolesEspecial(user, input.RoleNames, input.TenantId.Value);
                            createdRoles = true;
                        }
                    }
                    CurrentUnitOfWork.SaveChanges();

                    if (input.Tenants.Count > 0)
                    {
                        var listTenant = input.Tenants.Select(x => x.Id).ToList();
                        await CreateOrUpdateUsersInTenants(user, listTenant, input.RoleNames);
                    }

                }


                //Bloque de codigo para inserta los tenants a los cuales el usuario puede entrar y trabajar, siendo este un
                //usuario general

                var currentTenants = (from x in _userTenantRepository.GetAllList(x => x.UserId == user.Id)
                                      select x.TenantId).ToList();

                var insertingTenants = (from x in input.Tenants
                                        select x.Id).ToList();
                var newTenants = insertingTenants.Except(currentTenants).ToList();
                var erasedTenants = currentTenants.Except(insertingTenants).ToList();

                //Borrando de la base de datos los tenans que fueron desceleccionados.
                foreach (var item in erasedTenants)
                {
                    var toErraseTenant = _userTenantRepository.GetAllList(x => x.UserId == user.Id && x.TenantId == item).FirstOrDefault();
                    if (toErraseTenant != null)
                    {
                        _userTenantRepository.Delete(toErraseTenant.Id);
                        CurrentUnitOfWork.SaveChanges();
                    }
                }

                //crando las nuevos Tenans
                foreach (var item in newTenants)
                {
                    var currentTenant = new Models.UserTenants();
                    currentTenant.TenantId = item;
                    currentTenant.UserId = user.Id;
                    _userTenantRepository.Insert(currentTenant);
                    CurrentUnitOfWork.SaveChanges();
                }

                if (input.RoleNames != null)
                {
                    //var rolesDelUsuario = _userManager.GetRoles(input.Id).ToList();

                    if (user.Id > 1)
                        RemoveAllRolesFromUser(user);
                    CurrentUnitOfWork.SaveChanges();
                    SetRoles(user, input.RoleNames);
                }

            }
            else
            {
                user = await _userManager.GetUserByIdAsync(input.Id);

                MapToEntity(input, user);
                if (AbpSession.TenantId.HasValue)
                {
                    input.TenantId = AbpSession.TenantId;
                    user.TenantId = AbpSession.TenantId;
                }
                else
                {
                    input.TenantId = (input.TenantId.HasValue ? input.TenantId.Value : AbpSession.TenantId);
                    user.TenantId = (user.TenantId.HasValue ? user.TenantId.Value : AbpSession.TenantId);
                }

                CheckErrors(await _userManager.UpdateAsync(user));



                if (input.RoleNames != null)
                {
                    rolesDelUsuario = _userManager.GetRoles(user.Id).ToList();
                    foreach (var item in rolesDelUsuario)
                    {
                        if (user.Id > 1)
                            _userManager.RemoveFromRole(user.Id, item);
                        CurrentUnitOfWork.SaveChanges();
                    }
                    SetRoles(user, input.RoleNames);
                }

            }

            if (input.PictureChanged)
            {
                var picture = user.UserPictures.FirstOrDefault();
                if (picture == null)
                {
                    picture = new Models.UserPictures();
                    user.UserPictures.Add(picture);
                }

                picture.Picture = input.Picture;

            }

            return await Get(input);
        }

        public override async Task Delete(EntityDto<long> input)
        {
            User user;
            if (!AbpSession.TenantId.HasValue)
            {
                //Disabling MayHaveTenant filter, so we can reach to all users
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                {
                    user = await _userManager.GetUserByIdAsync(input.Id);
                    await _userManager.DeleteAsync(user);
                }
            }
            else
            {
                user = await _userManager.GetUserByIdAsync(input.Id);
                await _userManager.DeleteAsync(user);
            }

        }

        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            var roles = await _roleRepository.GetAllListAsync();
            return new ListResultDto<RoleDto>(ObjectMapper.Map<List<RoleDto>>(roles));
        }

        public async Task<PagedResultDto<UserDto>> GetAllUserByTenant(GdPagedResultUserRequestDto input)
        {
            PagedResultDto<UserDto> ouput = new PagedResultDto<UserDto>();

            IQueryable<User> query = _userManager.Users.Where(x => x.ParentUserId == null); //_userManager.Users;

            if (AbpSession.TenantId.HasValue)
            {
                if (!string.IsNullOrEmpty(input.TextFilter))
                {
                    query = from x in query
                            where x.Name.Contains(input.TextFilter)
                            select x;
                }
                ouput.TotalCount = await Task.Run(() => { return query.Count(); });
                if (input.SkipCount > 0)
                {
                    query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
                }
                if (input.MaxResultCount > 0)
                {
                    query = query.Take(input.MaxResultCount);
                }
                ouput.Items = ObjectMapper.Map<List<UserDto>>(query.ToList());
                return ouput;

            }
            else
            {
                //Disabling MayHaveTenant filter, so we can reach to all users
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                {
                    if (input.id.HasValue)
                    {
                        query = _userManager.Users.Where(f => f.TenantId == input.id);
                    }

                    if (!string.IsNullOrEmpty(input.TextFilter))
                    {
                        query = from x in query
                                where x.Name.Contains(input.TextFilter)
                                select x;
                    }
                    ouput.TotalCount = await Task.Run(() => { return query.Count(); });
                    if (input.SkipCount > 0)
                    {
                        query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
                    }
                    if (input.MaxResultCount > 0)
                    {
                        query = query.Take(input.MaxResultCount);
                    }
                    ouput.Items = ObjectMapper.Map<List<UserDto>>(query.ToList());
                    return ouput;

                }

            }
        }

        public async Task<List<UserDto>> GetAllUserForMessage(GetUserForMessageInput input)
        {

            return await Task.Run(() =>
            {

                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                {
                    IQueryable<User> result = _userManager.Users;

                    if (input.TenantId.HasValue)
                        result = result.Where(t => t.TenantId == input.TenantId.Value);

                    if (!string.IsNullOrEmpty(input.TextFilter))
                        result = result.Where(t => t.UserName.Contains(input.TextFilter) || t.Name.Contains(input.TextFilter) || t.Surname.Contains(input.TextFilter));

                    return ObjectMapper.Map<List<UserDto>>(result);
                }
            });
        }

        protected override User MapToEntity(CreateUserDto createInput)
        {
            var user = ObjectMapper.Map<User>(createInput);
            return user;
        }

        protected override void MapToEntity(UpdateUserDto input, User user)
        {
            ObjectMapper.Map(input, user);
        }

        protected override IQueryable<User> CreateFilteredQuery(PagedResultRequestDto input)
        {
            var userList = Repository.GetAllIncluding(x => x.Roles);

            return userList;
        }

        protected override async Task<User> GetEntityByIdAsync(long id)
        {
            var user = Repository.GetAllIncluding(x => x.Roles).FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(user);
        }

        protected override IQueryable<User> ApplySorting(IQueryable<User> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.UserName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }


        public async Task<GetTenantListForUserDto> GetTenantListForUser(GetTenantListForUserInput input)
        {
            GetTenantListForUserDto output = new GetTenantListForUserDto();
            output.TenantNames = new List<string>();

            List<string> listadoTenant = new List<string>() { "Default", "COLEGIOALMENGUAL" };

            if (input.UserName == "Admin")
                output.TenantNames = await Task.Run(() => { return listadoTenant.ToList(); });

            return output;
        }

        public IdentityResult SetRoles(User user, string[] roleNames)
        {
            IdentityResult ir = null;
            try
            {
                var roleList = roleNames.ToList();

                foreach (var item in roleList)
                {
                    var especificRole = _roleManager.Roles.Where(x => x.Name == item).FirstOrDefault();
                    if (especificRole != null)
                    {
                        var userrole = _userRoleRepository.GetAllList(x => x.RoleId == especificRole.Id && x.UserId == user.Id).FirstOrDefault();
                        if (userrole == null)
                        {
                            var rolex = new UserRole();
                            rolex.UserId = user.Id;
                            rolex.RoleId = especificRole.Id;
                            rolex.TenantId = AbpSession.TenantId;
                            _userRoleRepository.Insert(rolex);
                            CurrentUnitOfWork.SaveChanges();
                        }
                    }
                }
                // ir = new IdentityResult();
            }
            catch (Exception err)
            {
                ir = new IdentityResult(new string[] { err.Message });
            }

            //return base.SetRoles(user, roleNames);
            return ir;
        }

        public void RemoveAllRolesFromUser(User user)
        {
            var roles = _userRoleRepository.GetAllList(x => x.UserId == user.Id);
            foreach (var item in roles)
            {
                if (user.Id > 1)
                {
                    _userRoleRepository.Delete(item.Id);
                    CurrentUnitOfWork.SaveChanges();
                }
            }
        }


        public IdentityResult SetRolesEspecial(User user, string[] roleNames, int TenantId)
        {
            IdentityResult ir = null;

            try
            {
                var roleList = roleNames.ToList();
                //Desabilitando el tenant para buscar en el tenant del usuario en cuestion
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                {

                    foreach (var item in roleList)
                    {
                        var especificRole = _roleManager.Roles.Where(x => x.Name == item && x.TenantId == TenantId).FirstOrDefault();
                        if (especificRole != null)
                        {
                            var userrole = _userRoleRepository.GetAllList(x => x.RoleId == especificRole.Id && x.UserId == user.Id).FirstOrDefault();
                            if (userrole == null)
                            {
                                var rolex = new UserRole();
                                rolex.UserId = user.Id;
                                rolex.RoleId = especificRole.Id;
                                rolex.TenantId = TenantId;
                                _userRoleRepository.Insert(rolex);
                                CurrentUnitOfWork.SaveChanges();
                            }
                        }
                    }
                }
                // ir = new IdentityResult();
            }
            catch (Exception err)
            {
                ir = new IdentityResult(new string[] { err.Message });
            }

            //return base.SetRoles(user, roleNames);
            return ir;
        }


        public async Task CreateOrUpdateUsersInTenants(User user, List<int> TenantCodeList, string[] RolesNames)
        {
            var userGeneral = _userManager.Users.Where(x => x.UserName == user.UserName && x.TenantId == null).FirstOrDefault();
            if (userGeneral != null)
            {
                //Desabilitando el tenant para buscar en el tenant del usuario en cuestion
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                {
                    foreach (var item in TenantCodeList)
                    {
                        var userInTenant = _userManager.Users.Where(x => x.ParentUserId == user.Id && x.TenantId == item).FirstOrDefault();
                        //Si el usuario exsiste en el tenant en cuestion lo modificamos
                        if (userInTenant != null)
                        {
                            userInTenant.Password = userGeneral.Password;
                            userInTenant.Name = userGeneral.Name;
                            userInTenant.PhoneNumber = userGeneral.PhoneNumber;
                            userInTenant.IsActive = user.IsActive;
                            CurrentUnitOfWork.SaveChanges();
                        }
                        //por el contrario, si no existe, lo creamos
                        else
                        {
                            User userNew = new User();
                            userNew.UserName = user.UserName + "_" + item.ToString();
                            userNew.Name = userGeneral.Name;
                            userNew.Surname = userGeneral.Surname;
                            userNew.Password = userGeneral.Password;
                            userNew.PhoneNumber = userGeneral.PhoneNumber;
                            userNew.MustChangePassword = false;
                            userNew.EmailAddress = "generated_email__" + item + "_" + userGeneral.Id + DateTime.Now.Millisecond.ToString() + "_@EasySchoolManager.edu.do";
                            userNew.ParentUserId = userGeneral.Id;
                            userNew.TenantId = item;
                            userNew.IsActive = userGeneral.IsActive;

                            await _userManager.CreateAsync(userNew);

                            CurrentUnitOfWork.SaveChanges();

                            SetRolesEspecial(userNew, RolesNames, item);
                        }

                    }
                }
            }
        }

        

    }
}
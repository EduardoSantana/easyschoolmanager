//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.MasterTenants.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.MasterTenants
{
      public interface IMasterTenantAppService : IAsyncCrudAppService<MasterTenantDto, int, PagedResultRequestDto, CreateMasterTenantDto, UpdateMasterTenantDto>
      {
            Task<PagedResultDto<MasterTenantDto>> GetAllMasterTenants(GdPagedResultRequestDto input);
			Task<List<Dto.MasterTenantDto>> GetAllMasterTenantsForCombo();
            List<Models.MasterTenants> MasterTenants();
            Models.MasterTenants GetEntityById(int id);
      }
}
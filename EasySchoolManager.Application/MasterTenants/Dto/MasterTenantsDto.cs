//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.Catalogs.Dto;

namespace EasySchoolManager.MasterTenants.Dto
{
    [AutoMap(typeof(Models.MasterTenants))]
    public class MasterTenantDto : EntityDto<int>
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string DirectorName { get; set; }
        public string AccountantName { get; set; }
        public string TreasurerName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string Cities_Name { get; set; }
        public string CurrencyCode { get; set; }


        /// <summary>
        /// Porcentaje que sera retenido de los pagos por tarjeta de credito
        /// </summary>
        public decimal CreditCardPercent { get; set; }

        /// <summary>
        /// Cuenta contable para registrar los intereses o cargos de tarjetas de 
        /// credito en los pagos que sean realizados con forma de pago tarjeta de credito.
        /// </summary>
        public int? CatalogForCreditCardId { get; set; }

        /// <summary>
        /// Referencia a la tabla del catalogo de cuentas.
        /// </summary>
        public virtual CatalogDto CatalogForCreditCards { get; set; }

        public int? ConceptForInscriptionTransactionsId { get; set; }

        public int? ConceptForMonthlyPaymentTransactionsId { get; set; }

        public bool ApplyAutomaticCharges { get; set; }

    }
}
//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.MasterTenants.Dto
{
    [AutoMap(typeof(Models.MasterTenants))]
    public class UpdateMasterTenantDto : EntityDto<int>
    {

        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(100)]
        public string ShortName { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        public int CityId { get; set; }

        [StringLength(15)]
        public string Phone1 { get; set; }

        [StringLength(15)]
        public string Phone2 { get; set; }

        [StringLength(15)]
        public string Phone3 { get; set; }

        [StringLength(100)]
        public string DirectorName { get; set; }

        [StringLength(100)]
        public string AccountantName { get; set; }

        [StringLength(100)]
        public string TreasurerName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Porcentaje que sera retenido de los pagos por tarjeta de credito
        /// </summary>
        public decimal CreditCardPercent { get; set; }
        /// <summary>
        /// Cuenta contable para registrar los intereses o cargos de tarjetas de 
        /// credito en los pagos que sean realizados con forma de pago tarjeta de credito.
        /// </summary>
        public int? CatalogForCreditCardId { get; set; }

        public int? ConceptForInscriptionTransactionsId { get; set; }

        public int? ConceptForMonthlyPaymentTransactionsId { get; set; }

        public bool ApplyAutomaticCharges { get; set; }
    }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.MasterTenants.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.MasterTenants 
{ 
    [AbpAuthorize(PermissionNames.Pages_MasterTenants)] 
    public class MasterTenantAppService : AsyncCrudAppService<Models.MasterTenants, MasterTenantDto, int, PagedResultRequestDto, CreateMasterTenantDto, UpdateMasterTenantDto>, IMasterTenantAppService 
    { 
        private readonly IRepository<Models.MasterTenants, int> _masterTenantRepository;
		
		    private readonly IRepository<Models.Cities, int> _citieRepository;


        public MasterTenantAppService( 
            IRepository<Models.MasterTenants, int> repository, 
            IRepository<Models.MasterTenants, int> masterTenantRepository ,
            IRepository<Models.Cities, int> citieRepository

            ) 
            : base(repository) 
        { 
            _masterTenantRepository = masterTenantRepository; 
			
            _citieRepository = citieRepository;


			
        } 
        public override async Task<MasterTenantDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<MasterTenantDto> Create(CreateMasterTenantDto input) 
        { 
            CheckCreatePermission(); 
            var masterTenant = ObjectMapper.Map<Models.MasterTenants>(input); 
			try{
              await _masterTenantRepository.InsertAsync(masterTenant); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(masterTenant); 
        } 
        public override async Task<MasterTenantDto> Update(UpdateMasterTenantDto input) 
        { 
            CheckUpdatePermission(); 
            var masterTenant = await _masterTenantRepository.GetAsync(input.Id);
            MapToEntity(input, masterTenant); 
		    try{
               await _masterTenantRepository.UpdateAsync(masterTenant); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var masterTenant = await _masterTenantRepository.GetAsync(input.Id); 
               await _masterTenantRepository.DeleteAsync(masterTenant);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.MasterTenants MapToEntity(CreateMasterTenantDto createInput) 
        { 
            var masterTenant = ObjectMapper.Map<Models.MasterTenants>(createInput); 
            return masterTenant; 
        } 
        protected override void MapToEntity(UpdateMasterTenantDto input, Models.MasterTenants masterTenant) 
        { 
            ObjectMapper.Map(input, masterTenant); 
        } 
        protected override IQueryable<Models.MasterTenants> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.MasterTenants> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.MasterTenants> GetEntityByIdAsync(int id) 
        { 
            var masterTenant = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(masterTenant); 
        }

        public List<Models.MasterTenants> MasterTenants() { return Repository.GetAllList(); }

        public Models.MasterTenants GetEntityById(int id)
        {
            var masterTenant = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return masterTenant;
        }

        protected override IQueryable<Models.MasterTenants> ApplySorting(IQueryable<Models.MasterTenants> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<MasterTenantDto>> GetAllMasterTenants(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<MasterTenantDto> ouput = new PagedResultDto<MasterTenantDto>(); 
            IQueryable<Models.MasterTenants> query = query = from x in _masterTenantRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _masterTenantRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<MasterTenants.Dto.MasterTenantDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<MasterTenantDto>> GetAllMasterTenantsForCombo()
        {
            var masterTenantList = await _masterTenantRepository.GetAllListAsync(x => x.IsActive == true);

            var masterTenant = ObjectMapper.Map<List<MasterTenantDto>>(masterTenantList.ToList());

            return masterTenant;
        }
		
    } 
} ;
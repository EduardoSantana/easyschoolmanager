﻿//Created from Templaste MG

using System;

namespace EasySchoolManager.Enrollments.Dto
{
    public class TutorPaymentProyectionDto
    {
        public TutorPaymentProyectionDto(int Sequence, DateTime Date, Decimal Amount)
        {
            this.Sequence = Sequence;
            this.Date = Date;
            this.Amount = Amount;
        }

        public int Sequence { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
    }
}
//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.Enrollments.Dto
{
    [AutoMap(typeof(Models.Enrollments))]
    public class UpdateEnrollmentDto : EntityDto<long>
    {
        public int Enrollment { get; set; }

        [Required(ErrorMessage = "Debe indicar el nombre")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Debe indicar los apellidos")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Debe indicar la direcci�n")]
        public string Address { get; set; }

        [Required(ErrorMessage ="Debe indicar la direcci�n completa")]
        public string FullAddress { get; set; }


        [Required(ErrorMessage = "Debe indicar el municipio")]
        public int CityId { get; set; }
        
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }


        [Required(ErrorMessage = "Debe indicar la ocupaci�n")]
        public int OccupationId { get; set; }

        [Required(ErrorMessage = "Debe indicar la religi�n")]
        public int ReligionId { get; set; }

        public string Comment { get; set; }

        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        [Required(ErrorMessage = "Debe indicar el genero")]
        public int GenderId { get; set; }

        public string OtherDocument { get; set; }
    }
}
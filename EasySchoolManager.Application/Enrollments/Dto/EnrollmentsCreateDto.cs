//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.Enrollments.Dto
{
    [AutoMap(typeof(Models.Enrollments))]
    public class CreateEnrollmentDto : EntityDto<long>
    {


        [Required(ErrorMessage = "Se debe indicar el nombre")]
        public string FirstName { get; set; }


        [Required(ErrorMessage = "Se deben indicar los apellidos")]
        public string LastName { get; set; }

        public string Address { get; set; }

        public string FullAddress { get; set; }


        [Required(ErrorMessage = "Usted debe indicar la ciudad de domicilio")]
        public int CityId { get; set; }

        [Required(ErrorMessage ="Usted debe indicar el genero del tutor que se est� matriculando")]
        public int GenderId { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        [Required]
        public int OccupationId { get; set; }

        [Required]
        public int ReligionId { get; set; }
        public string Comment { get; set; }
        public string EmailAddress { get; set; }
        public bool IsActive { get; set; }
        public string OtherDocument { get; set; }

    }
}
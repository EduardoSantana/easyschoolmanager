﻿//Created from Templaste MG

using System.Collections.Generic;

namespace EasySchoolManager.Enrollments.Dto
{
    public class SetPaymentProjectionToNumberInputDto
    {
        public SetPaymentProjectionToNumberInputDto()
        {
            PaymentProjections = new List<TutorPaymentProyectionDto>();
            EnrollmentStudents = new List<EasySchoolManager.EnrollmentStudents.Dto.EnrollmentStudentDto2>();
        }
        public int NumberOfPayments { get; set; }
        public List<TutorPaymentProyectionDto> PaymentProjections { get; set; }
        public List<EnrollmentStudents.Dto.EnrollmentStudentDto2> EnrollmentStudents { get; set; }
    }
}
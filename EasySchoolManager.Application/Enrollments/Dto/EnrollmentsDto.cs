//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.EnrollmentSequence.Dto;
using System.Collections.Generic;
using EasySchoolManager.EnrollmentStudents.Dto;

namespace EasySchoolManager.Enrollments.Dto
{
    [AutoMap(typeof(Models.Enrollments))]
    public class EnrollmentDto : EntityDto<long>
    {
        public EnrollmentDto()
        {
            EnrollmentSequences = new List<EnrollmentSequenceDto>();
            EnrollmentStudents = new List<EasySchoolManager.EnrollmentStudents.Dto.EnrollmentStudentDto>();
        }

        public int Enrollment { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string FullAddress { get; set; }
        public int CityId { get; set; }
        public int Cities_ProvinceId { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public int OccupationId { get; set; }
        public int ReligionId { get; set; }
        public string Comment { get; set; }
        public int GenderId { get; set; }
        public string EmailAddress { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string Occupations_Name { get; set; }
        public string Religions_ShortName { get; set; }
        public List<EnrollmentSequenceDto> EnrollmentSequences { get; set; }
        public List<EnrollmentStudentDto> EnrollmentStudents { get; set; }
        public int NumberOfStudents { get; set; }
        public string OtherDocument { get; set; }

        public string StudentsNames { get; set; }

        public string FullName
        {
            get
            {
                var retVal = "";
                if (!string.IsNullOrEmpty(this.FirstName))
                {
                    retVal += this.FirstName.TrimEnd() + " ";
                }
                if (!string.IsNullOrEmpty(this.LastName))
                {
                    retVal += this.LastName.TrimEnd() + " ";
                }
                return retVal;
            }
        }
    }
}
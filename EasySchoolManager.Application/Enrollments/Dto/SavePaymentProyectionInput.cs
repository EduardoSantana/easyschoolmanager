﻿//Created from Templaste MG

using System.Collections.Generic;

namespace EasySchoolManager.Enrollments.Dto
{
    public class SavePaymentProyectionInput
    {
        public SavePaymentProyectionInput()
        {
            PaymentProjections = new List<PaymentProjecctions.Dto.PaymentProjectionDto>();
        }

      public long EnrollmentId { get; set; }
      public List<PaymentProjecctions.Dto.PaymentProjectionDto> PaymentProjections { get; set; }
    }
}
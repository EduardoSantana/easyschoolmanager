﻿//Created from Templaste MG

namespace EasySchoolManager.Enrollments.Dto
{
    public class GetEnrollmentPaymentProyectionInput
    {
        public long EnrollmentId { get; set; }
        public int TenantId { get; set; }
        public int? PeriodId { get; set; }
    }
}
﻿//Created from Templaste MG

namespace EasySchoolManager.Enrollments.Dto
{
    public class ChangeEnrollmentIdDto
    {
        public long CurrentEnrollmentId { get; set; }
        public long NewEnrollmentId { get; set; }
    }
}
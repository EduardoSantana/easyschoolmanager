﻿//Created from Templaste MG

namespace EasySchoolManager.Enrollments.Dto
{
    public class GetEnrollmentPaymentProyectionOutPut
    {
        public EnrollmentDto2 Enrollment { get; set; }
        public int NumberOfPayments {get;set;}
        public bool HasAlreadyPaymentProjections { get; set; }
    }
}
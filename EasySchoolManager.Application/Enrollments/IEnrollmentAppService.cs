//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Enrollments.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Enrollments
{
    public interface IEnrollmentAppService : IAsyncCrudAppService<EnrollmentDto, long, PagedResultRequestDto, CreateEnrollmentDto, UpdateEnrollmentDto>
    {
        Task<PagedResultDto<EnrollmentDto>> GetAllEnrollments(GdPagedResultRequestDto input);
        Task<List<Dto.EnrollmentDto>> GetAllEnrollmentsForCombo();
        Task<EnrollmentDto>  GetEnrollmentByPersonalId(long enrollmentId);
        EnrollmentSequence.Dto.EnrollmentSequenceDto CreateOrGetEnrollmentSequenceByPersonalId(long enrollmentId);
        EnrollmentSequence.Dto.EnrollmentSequence2Dto GetEnrollmentSequenceByEnrollment(long Enrollment);
        EnrollmentDto CreateEnrollment(CreateEnrollmentDto input);
        EnrollmentDto UpdateEnrollment(UpdateEnrollmentDto input);
        void ChangeEnrollmentId(ChangeEnrollmentIdDto input);
        Task<GetEnrollmentPaymentProyectionOutPut> GetEnrollmentPaymentProyection(GetEnrollmentPaymentProyectionInput input);
        void SavePaymentProyection(SavePaymentProyectionInput input);
        Task<SetPaymentProjectionToNumberOuputDto> SetPaymentProjectionToNumber(SetPaymentProjectionToNumberInputDto input);
        void GenerateAllPaymentProyectionForASchool();
        void GenerateAllPaymentProyectionForASchoolPriorPeriod();
        Task<GetEnrollmentsByStudentNameOuput> GetEnrollmentsByStudentName(GdPagedResultRequestDto input);
        Task<PagedResultDto<EnrollmentDto>> GetAllEnrollmentsPreviousEnrollments(GdPagedResultRequestDto input);
        Task<Periods.Dto.PeriodDto> GetNextPeriod();
        bool GetCharged();
        Task<PagedResultDto<EnrollmentDto>> GetAllEnrollmentsForQueries(GdPagedResultRequestDto input);
    }
}
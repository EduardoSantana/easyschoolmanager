//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Enrollments.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.Models;
using Abp.Runtime.Session;
using EasySchoolManager.MultiTenancy;
using EasySchoolManager.Editions;
using EasySchoolManager.Authorization.Roles;
using EasySchoolManager.Authorization.Users;
using EasySchoolManager.Helpers;
using EasySchoolManager.EnrollmentStudents.Dto;
using EasySchoolManager.PaymentDates.Dto;
using EasySchoolManager.CourseValues.Dto;
using EasySchoolManager.Students;
using EasySchoolManager.Transactions;

namespace EasySchoolManager.Enrollments
{
    [AbpAuthorize(PermissionNames.Pages_Enrollments)]
    public class EnrollmentAppService : AsyncCrudAppService<Models.Enrollments, EnrollmentDto, long,
        PagedResultRequestDto, CreateEnrollmentDto, UpdateEnrollmentDto>, IEnrollmentAppService
    {
        private readonly IRepository<Models.Enrollments, long> _enrollmentRepository;

        private readonly IRepository<Models.Cities, int> _cityRepository;
        private readonly IRepository<Models.Occupations, int> _occupationRepository;
        private readonly IRepository<Models.Religions, int> _religionRepository;
        private readonly IRepository<Models.EnrollmentSequences> _enrollmentSequenceRepository;
        private readonly IRepository<Models.Sequences> _sequencesRepository;
        private readonly IRepository<Models.EnrollmentStudents> _enrollmentStudentsRepository;
        private readonly IRepository<Models.Periods> _periodRepository;
        private readonly IRepository<Models.Students> _studentRepository;
        private readonly ITransactionAppService _transctionService;

        private readonly PaymentDates.IPaymentDateAppService _paymentDateRepository;
        private readonly CourseValues.ICourseValueAppService _courseValueRepository;

        private readonly TenantManager _tenantManager;
        private readonly EditionManager _editionManager;
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<PaymentProjections> _paymentProjectionsRepository;
        private readonly IRepository<Models.Genders, int> _genderRepository;
        private readonly IRepository<Models.Bloods, int> _bloodRepository;
        private readonly IRepository<Models.EnrollmentStudents, int> _enrollmentStudentRepository;
        private readonly IRepository<Models.Relationships, int> _relationshipsRepository;
        private readonly IRepository<Models.Illnesses, int> _illnessesRepository;
        private readonly IRepository<Models.Allergies, int> _allergiesRepository;
        private readonly IRepository<Models.Courses, int> _coursesRepository;
        private readonly IRepository<Models.CourseEnrollmentStudents, int> _courseEnrollmentStudentRepository;
        private readonly IRepository<Models.StudentAllergies, int> _studentAllergiesRepository;
        private readonly IRepository<Models.StudentIllnesses, int> _studentIllnessesRepository;
        private readonly IRepository<Models.Levels, int> _levelRepository;
        private readonly IRepository<Tenant> _tenantRepository;

        public EnrollmentAppService(
            IRepository<Models.Enrollments, long> enrollmentRepository,
            IRepository<Models.Cities, int> cityRepository,
            IRepository<Models.Occupations, int> occupationRepository,
            IRepository<Models.Religions, int> religionRepository,
            IRepository<Models.EnrollmentSequences> enrollmentSequenceRepository,
                        TenantManager tenantManager,
            EditionManager editionManager,
            UserManager userManager,
            RoleManager roleManager,
            IRepository<Role> roleRepository,
            PaymentDates.IPaymentDateAppService paymentDateRepository,
            CourseValues.ICourseValueAppService courseValueRepository,
            IRepository<PaymentProjections> paymentProjectionsRepository,
            IRepository<Models.Sequences> sequencesRepository,
            IRepository<Models.EnrollmentStudents> enrollmentStudentsRepository,
             IRepository<Models.Periods> periodRepository,
             IRepository<Models.Students> studentRepository
        , IRepository<Models.Genders, int> genderRepository
        , IRepository<Models.Bloods, int> bloodRepository
        , IRepository<Models.EnrollmentStudents, int> enrollmentStudentRepository
        , IRepository<Models.Relationships, int> relationshipsRepository
        , IRepository<Models.Illnesses, int> illnessesRepository
        , IRepository<Models.Allergies, int> allergiesRepository
        , IRepository<Models.Courses, int> coursesRepository
        , IRepository<Models.CourseEnrollmentStudents, int> courseEnrollmentStudentRepository
        , IRepository<Models.StudentAllergies, int> studentAllergiesRepository
        , IRepository<Models.StudentIllnesses, int> studentIllnessesRepository
        , IRepository<Models.Levels, int> levelRepository
        , IRepository<Tenant> tenantRepository
        , ITransactionAppService transactionService
            )
            : base(enrollmentRepository)
        {
            _enrollmentRepository = enrollmentRepository;

            _cityRepository = cityRepository;

            _occupationRepository = occupationRepository;

            _religionRepository = religionRepository;

            _enrollmentSequenceRepository = enrollmentSequenceRepository;

            _tenantManager = tenantManager;
            _editionManager = editionManager;
            _roleManager = roleManager;
            _userManager = userManager;
            _roleRepository = roleRepository;
            _paymentDateRepository = paymentDateRepository;
            _courseValueRepository = courseValueRepository;
            _paymentProjectionsRepository = paymentProjectionsRepository;
            _sequencesRepository = sequencesRepository;
            _enrollmentStudentsRepository = enrollmentStudentsRepository;

            _periodRepository = periodRepository;

            _studentRepository = studentRepository;

            _genderRepository = genderRepository;

            _bloodRepository = bloodRepository;

            _enrollmentStudentRepository = enrollmentStudentRepository;

            _relationshipsRepository = relationshipsRepository;

            _illnessesRepository = illnessesRepository;

            _allergiesRepository = allergiesRepository;

            _coursesRepository = coursesRepository;

            _courseEnrollmentStudentRepository = courseEnrollmentStudentRepository;

            _studentAllergiesRepository = studentAllergiesRepository;

            _studentIllnessesRepository = studentIllnessesRepository;

            _levelRepository = levelRepository;

            _tenantRepository = tenantRepository;

            _transctionService = transactionService;

            this.LocalizationSourceName = EasySchoolManagerConsts.LocalizationSourceName;

        }
        public override async Task<EnrollmentDto> Get(EntityDto<long> input)
        {
            var user = await _enrollmentRepository.GetAsync(input.Id);

            var returnedValue = ObjectMapper.Map<EnrollmentDto>(user);
            returnedValue.Enrollment = returnedValue.EnrollmentSequences.First(x => x.TenantId == AbpSession.TenantId.Value).Sequence;
            returnedValue.EnrollmentStudents = null;
            returnedValue.EnrollmentSequences = null;
            return returnedValue;
        }


        public EnrollmentDto CreateEnrollment(CreateEnrollmentDto input)
        {
            CheckCreatePermission();

            Helpers.GeneralHelper.ValidateUserBellongToASchool(AbpSession);

            Models.Enrollments enrollment = ObjectMapper.Map<Models.Enrollments>(input);
            enrollment.FirstName = enrollment.FirstName.ToUpper();
            enrollment.LastName = enrollment.LastName.ToUpper();
            var enrollmentInBD = _enrollmentRepository.GetAllList(x => x.Id == input.Id).FirstOrDefault();
            if (enrollmentInBD == null)
            {
                if (input.Id == 999)
                {
                    long idGenerado = 1;
                    var Seq = _sequencesRepository.GetAllList(x => x.Code == "001").FirstOrDefault();
                    if (Seq == null)
                    {
                        Sequences seqNew = new Sequences();
                        seqNew.Code = "001";
                        seqNew.Name = "Alternative Personal Id";
                        seqNew.Sequence = 2;

                        _sequencesRepository.Insert(seqNew);
                        CurrentUnitOfWork.SaveChanges();
                    }
                    else
                    {
                        idGenerado = Seq.Sequence++;
                    }

                    String SEQUENCE = String.Format("999{0:00000000}", idGenerado);
                    input.Id = Convert.ToInt64(SEQUENCE);
                    enrollment = ObjectMapper.Map<Models.Enrollments>(input);
                    enrollment.Id = input.Id;
                }

                _enrollmentRepository.InsertAndGetId(enrollment);
            }
            else
            {
                MapToEntity(input, enrollmentInBD);
            }

            var enrolmentSequence = GenerateOrGetEnrolmentSequence(enrollment.Id, AbpSession.TenantId.Value);


            //Si puesto el email adress del enrollment, entonces le ser� creado un usuario
            if (enrollment.EmailAddress != null && enrollment.EmailAddress.Trim().Length > 0)
                createUserWithEnrollment(enrollment);

            enrollment.EnrollmentSequences = null;
            enrollment.EnrollmentStudents = null;

            return MapToEntityDto(enrollment);
        }



        public void CreateEnrollmentRole()
        {
            var enrollmentRole = _roleRepository.FirstOrDefault(r => r.TenantId == AbpSession.TenantId && r.Name == StaticRoleNames.Tenants.Enrollment);
            if (enrollmentRole == null)
            {
                enrollmentRole = _roleRepository.Insert(new Role(AbpSession.TenantId, StaticRoleNames.Tenants.Enrollment, StaticRoleNames.Tenants.Enrollment) { IsStatic = true });
            }
        }

        public bool GetCharged()
        {
            var saber = _tenantRepository.FirstOrDefault(x => x.Id == AbpSession.TenantId).ApplyAutomaticCharges;

            return saber;
        }


        public async Task createUserWithEnrollment(Models.Enrollments enrollment)
        {
            try
            {
                //We are working entities of new tenant, so changing tenant filter
                using (CurrentUnitOfWork.SetTenantId(AbpSession.TenantId))
                {
                    CreateEnrollmentRole();
                    //CurrentUnitOfWork.SaveChanges();

                    var enrollmentRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Enrollment);


                    //Create admin user for the tenant
                    var cedula = string.Format("{0:00000000000}", enrollment.Id);

                    var EnrollmentUser = User.CreateTenantUser(tenantId: AbpSession.TenantId.Value,
                        emailAddress: enrollment.EmailAddress,
                        password: cedula /**Aqui se debe poner la ceudla ocmo contrasena; */,
                        name: enrollment.FirstName,
                        surName: enrollment.LastName,
                        userName: cedula);

                    CheckErrors(_userManager.Create(EnrollmentUser));

                    var enrollmentUserconsultado = _userManager.Users.Where(x => x.UserName == EnrollmentUser.UserName).FirstOrDefault();

                    //CurrentUnitOfWork.SaveChanges(); //To get admin user's id
                    if (enrollmentUserconsultado != null)
                        CheckErrors(_userManager.AddToRole(enrollmentUserconsultado.Id, enrollmentRole.Name));

                }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }

        public override async Task<EnrollmentDto> Create(CreateEnrollmentDto input)
        {
            CheckCreatePermission();
            var enrollment = ObjectMapper.Map<Models.Enrollments>(input);
            await _enrollmentRepository.InsertAsync(enrollment);

            return MapToEntityDto(enrollment);
        }



        private EnrollmentSequences GenerateOrGetEnrolmentSequence(long enrolmentId, int TenantId)
        {
            Models.EnrollmentSequences es = new Models.EnrollmentSequences();
            var esX = _enrollmentSequenceRepository.GetAllList(x => x.EnrollmentId == enrolmentId && x.TenantId == TenantId).ToList();
            if (esX.Count > 0)
            {
                //Como es unique field solo habra uno
                es = esX.First();
            }
            else
            {
                var sequence = 1;
                try
                {
                    sequence = _enrollmentSequenceRepository.GetAllList(x => x.TenantId == TenantId).Max(x => x.Sequence) + 1;
                }
                catch { } // EL CATCH NO TIENE NADA, POR QUE CUANDO DE ERROR ES QUE NO SE ENCUENTRANN REGISTRO EN DICHO COLEGIO, POR TANTO LA SECUENCIA INICIAL CERA 1
                es.TenantId = TenantId;
                es.Sequence = sequence;
                es.EnrollmentId = enrolmentId;
                _enrollmentSequenceRepository.Insert(es);
            }

            return es;
        }

        public override async Task<EnrollmentDto> Update(UpdateEnrollmentDto input)
        {
            CheckUpdatePermission();
            var enrollment = await _enrollmentRepository.GetAsync(input.Id);
            MapToEntity(input, enrollment);
            try
            {
                await _enrollmentRepository.UpdateAsync(enrollment);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            enrollment.FirstName = enrollment.FirstName.ToUpper();
            enrollment.LastName = enrollment.LastName.ToUpper();
            return await Get(input);
        }

        public EnrollmentDto UpdateEnrollment(UpdateEnrollmentDto input)
        {
            CheckUpdatePermission();
            var enrollment = _enrollmentRepository.Get(input.Id);
            MapToEntity(input, enrollment);

            try
            {
                _enrollmentRepository.Update(enrollment);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            enrollment.FirstName = enrollment.FirstName.ToUpper();
            enrollment.LastName = enrollment.LastName.ToUpper();
            //var enrolmentX = ObjectMapper.Map<EnrollmentDto>(enrollment);
            //enrolmentX.EnrollmentSequences = null;
            //enrolmentX.EnrollmentStudents = null;
            return new EnrollmentDto();
        }

        public override async Task Delete(EntityDto<long> input)
        {
            try
            {
                //TODO: VERIFICAR si todo se ejecuta de acuerdo a lo pensado, una vez verificado se
                //debe borrar este comentario inmediatamente.
                var enrollment = _enrollmentSequenceRepository.
                    GetAllList(x => x.EnrollmentId == input.Id).FirstOrDefault();
                await _enrollmentSequenceRepository.DeleteAsync(enrollment);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.Enrollments MapToEntity(CreateEnrollmentDto createInput)
        {
            var enrollment = ObjectMapper.Map<Models.Enrollments>(createInput);
            return enrollment;
        }
        protected override void MapToEntity(UpdateEnrollmentDto input, Models.Enrollments enrollment)
        {
            enrollment.Address = input.Address;
            enrollment.FirstName = input.FirstName;
            enrollment.LastName = input.LastName;
            enrollment.CityId = input.CityId;
            enrollment.Comment = input.Comment;
            enrollment.EmailAddress = input.EmailAddress;
            enrollment.FullAddress = input.FullAddress;
            enrollment.GenderId = input.GenderId;
            enrollment.IsActive = input.IsActive;
            enrollment.OccupationId = input.OccupationId;
            enrollment.Phone1 = input.Phone1;
            enrollment.Phone2 = input.Phone2;
            enrollment.ReligionId = input.ReligionId;
            enrollment.OtherDocument = input.OtherDocument;
        }

        protected void MapToEntity(CreateEnrollmentDto input, Models.Enrollments enrollment)
        {
            enrollment.Address = input.Address;
            enrollment.FirstName = input.FirstName;
            enrollment.LastName = input.LastName;
            enrollment.CityId = input.CityId;
            enrollment.Comment = input.Comment;
            enrollment.EmailAddress = input.EmailAddress;
            enrollment.FullAddress = input.FullAddress;
            enrollment.GenderId = input.GenderId;
            enrollment.IsActive = input.IsActive;
            enrollment.OccupationId = input.OccupationId;
            enrollment.Phone1 = input.Phone1;
            enrollment.Phone2 = input.Phone2;
            enrollment.ReligionId = input.ReligionId;
        }

        protected override IQueryable<Models.Enrollments> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }

        protected IQueryable<Models.Enrollments> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => (x.FirstName + " " + x.LastName).Contains(input.TextFilter));
            return resultado;
        }

        protected override async Task<Models.Enrollments> GetEntityByIdAsync(long id)
        {
            var enrollment = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(enrollment);
        }
        protected override IQueryable<Models.Enrollments> ApplySorting(IQueryable<Models.Enrollments> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.FirstName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<Periods.Dto.PeriodDto> GetCurrentPeriod()
        {
            Periods.Dto.PeriodDto per = null;
            try
            {
                var currentTenant = await Task.Run(() => { return _tenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId); });
                if (currentTenant != null)
                    per = ObjectMapper.Map<Periods.Dto.PeriodDto>(currentTenant.Periods);
            }
            catch { }
            return per;
        }

        public async Task<Periods.Dto.PeriodDto> GetPreviousPeriod()
        {
            Periods.Dto.PeriodDto per = null;
            try
            {
                var currentTenant = await Task.Run(() => { return _tenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId); });
                if (currentTenant != null)
                    per = ObjectMapper.Map<Periods.Dto.PeriodDto>(currentTenant.PreviousPeriods);
            }
            catch { }
            return per;
        }


        public async Task<Periods.Dto.PeriodDto> GetNextPeriod()
        {
            Periods.Dto.PeriodDto per = null;
            try
            {
                var currentTenant = await Task.Run(() => { return _tenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId); });
                if (currentTenant != null)
                    per = ObjectMapper.Map<Periods.Dto.PeriodDto>(currentTenant.NextPeriods);
            }
            catch { }
            return per;
        }



        public async Task<PagedResultDto<EnrollmentDto>> GetAllEnrollmentsPreviousEnrollments(GdPagedResultRequestDto input)
        {
            var retVal = await GetAllEnrollments(input);
            return retVal;
        }


        public async Task<PagedResultDto<EnrollmentDto>> GetAllEnrollments(GdPagedResultRequestDto input)
        {
            PagedResultDto<EnrollmentDto> ouput = new PagedResultDto<EnrollmentDto>();
            IQueryable<Models.Enrollments> query = query = from x in _enrollmentRepository.GetAll() select x;

            if (AbpSession.TenantId.HasValue)
            {
                query = from x in query
                        from x2 in x.EnrollmentSequences
                        where x2.TenantId == AbpSession.TenantId.Value
                        orderby x2.Sequence
                        select x;
            }

            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                if (long.TryParse(input.TextFilter, out long searchId))
                {
                    query = from x in query
                            from x2 in x.EnrollmentSequences
                            where x.Id == searchId ||
                                    x2.Sequence == searchId
                            orderby x2.Sequence
                            select x;
                }
                else
                {
                    query = (from x in query
                             from x2 in x.EnrollmentSequences
                             from x3 in x.EnrollmentStudents
                             where (x.FirstName + " " + x.LastName).Contains(input.TextFilter) ||
                                     (x3.Students.FirstName + " " + x3.Students.LastName).Contains(input.TextFilter)
                             orderby x2.Sequence
                             select x).Distinct();
                }

            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Enrollments.Dto.EnrollmentDto>>(query.ToList());

            var usedPeriod = await GetCurrentPeriod();

            if (input.PreviousPeriod.HasValue)
                if (input.PreviousPeriod.Value == true)
                {
                    usedPeriod = await GetPreviousPeriod();

                    if (usedPeriod == null)
                        throw new UserFriendlyException(L("YouMustSetThePreviousPeriodForTheCurrentSchool"));
                }


            if (input.NextPeriod.HasValue)
                if (input.NextPeriod.Value == true)
                {
                    usedPeriod = await GetNextPeriod();

                    if (usedPeriod == null)
                        throw new UserFriendlyException(L("YouMustSetThePreviousPeriodForTheCurrentSchool"));
                }


            foreach (var item in ouput.Items)
            {

                if (usedPeriod == null)
                    throw new UserFriendlyException(L("YouMustSetThePreviousPeriodForTheCurrentSchool"));

                item.Enrollment = item.EnrollmentSequences.First().Sequence;
                if (usedPeriod == null)
                    item.NumberOfStudents = item.EnrollmentStudents.Count();
                else
                    item.NumberOfStudents = item.EnrollmentStudents.Where(x => x.Periods.Id == usedPeriod.Id && x.IsActive == true).Count();

                var enrollmentStudent = item.EnrollmentStudents.Where(x => x.Periods.Id == usedPeriod.Id && x.IsActive == true);

                item.StudentsNames = "";
                foreach (var student2 in enrollmentStudent)
                {
                    item.StudentsNames += student2.Students.FullName + ", ";
                    GetStudentBalanceInput inputStudent = new GetStudentBalanceInput();
                    inputStudent.EnrollmentStudentId = student2.Id;
                    //var studentBalanceX =  _transctionService.GetStudentBalance(inputStudent);

                    //student2.StudentBalance = studentBalanceX.TotalBalance;
                }

                item.EnrollmentSequences = null;
                item.EnrollmentStudents = null;
            }

            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<EnrollmentDto>> GetAllEnrollmentsForCombo()
        {
            var enrollmentList = await _enrollmentRepository.GetAllListAsync(x => x.IsActive == true);

            var enrollment = ObjectMapper.Map<List<EnrollmentDto>>(enrollmentList.ToList());

            return enrollment;
        }

        [AbpAllowAnonymous]
        public async Task<EnrollmentDto> GetEnrollmentByPersonalId(long enrollmentId)
        {
            var dato = await _enrollmentRepository.GetAllListAsync(x => x.Id == enrollmentId);
            var first = dato.FirstOrDefault();
            var mappedObject = ObjectMapper.Map<EnrollmentDto>(first);
            if (mappedObject != null)
            {
                mappedObject.EnrollmentSequences = null;
                mappedObject.EnrollmentStudents = null;
            }
            return mappedObject;
        }


        public EasySchoolManager.EnrollmentSequence.Dto.EnrollmentSequence2Dto GetEnrollmentSequenceByEnrollment(long Enrollment)
        {
            var dato = _enrollmentSequenceRepository.GetAllList(x => x.Sequence == Enrollment).FirstOrDefault();
            if (dato == null)
                return null;
            var mappedObject = ObjectMapper.Map<EnrollmentSequence.Dto.EnrollmentSequence2Dto>(dato);
            mappedObject.Enrollments.EnrollmentStudents = mappedObject.Enrollments.EnrollmentStudents.Where(f => f.IsActive == true).ToList();
            foreach (var item in mappedObject.Enrollments.EnrollmentStudents)
            {
                item.Students.Course = item.CourseEnrollmentStudents.First().Courses;
                GetStudentBalanceInput inputx = new GetStudentBalanceInput();
                inputx.EnrollmentStudentId = item.Id;
                var studentBalanceX = _transctionService.GetStudentBalance(inputx);
                item.StudentBalance = studentBalanceX.TotalBalance;
            }
            return mappedObject;
        }


        public EnrollmentSequence.Dto.EnrollmentSequenceDto CreateOrGetEnrollmentSequenceByPersonalId(long enrollmentId)
        {
            var dato = _enrollmentSequenceRepository.GetAllList(x => x.Id == enrollmentId).FirstOrDefault();
            var mappedObject = ObjectMapper.Map<EnrollmentSequence.Dto.EnrollmentSequenceDto>(dato);
            return mappedObject;
        }


        public void ChangeEnrollmentId(ChangeEnrollmentIdDto input)
        {
            string sqlcomand = "Update enrollments set id = '{1}' where id = {0}";
            var resultedString = EasySchoolManager.Helpers.ConnectionToDB.ExecuteSqlString(string.Format(sqlcomand, input.CurrentEnrollmentId, input.NewEnrollmentId));

            if (resultedString.Length > 0)
            {
                throw new UserFriendlyException(resultedString);
            }

        }

        public async Task<GetEnrollmentPaymentProyectionOutPut> GetEnrollmentPaymentProyection(GetEnrollmentPaymentProyectionInput input)
        {
            GetEnrollmentPaymentProyectionOutPut ouput = new GetEnrollmentPaymentProyectionOutPut();

            var courses = _coursesRepository.GetAllList(x => x.IsActive == true).OrderBy(x => x.Sequence).ToList();


            var currentTenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();

            if (currentTenant.Periods == null)
            {
                throw new UserFriendlyException(L("ThereIsNoDefinedTheCurrentPeriodInTheSchoolPleaseContactTheAdministration"));
            }
            Models.Periods periodToUse = null;

            if (input.PeriodId.HasValue && input.PeriodId.HasValue)
            {
                periodToUse = _periodRepository.FirstOrDefault(x => x.Id == input.PeriodId.Value);

                var StudentPeriodToUse = _enrollmentStudentsRepository.GetAll().Where(x => x.PeriodId == input.PeriodId.Value
                && x.EnrollmentId == input.EnrollmentId).ToList();


                var studentCurrentPeriod = _enrollmentStudentsRepository.GetAll().Where(x => x.PeriodId == currentTenant.PeriodId
                && x.EnrollmentId == input.EnrollmentId).ToList();

                foreach (var item in studentCurrentPeriod)
                {
                    if (!StudentPeriodToUse.Any(x => x.EnrollmentId == item.EnrollmentId && x.StudentId == item.StudentId))
                    {
                        StudentAppService studentService = new StudentAppService(_studentRepository,
                            _genderRepository,
                            _bloodRepository,
                            _enrollmentStudentRepository,
                            _relationshipsRepository,
                            _illnessesRepository,
                            _allergiesRepository,
                            _coursesRepository,
                            _courseEnrollmentStudentRepository,
                            _studentAllergiesRepository,
                            _studentIllnessesRepository,
                            _levelRepository,
                            _tenantManager,
                            _tenantRepository);

                        Models.Courses nextCourse = null;
                        int maxValue = courses.Count;
                        for (int i = 0; i < courses.Count; i++)
                        {
                            if (courses[i].Id == item.CourseEnrollmentStudents.FirstOrDefault().Courses.Id)
                            {
                                if (i < maxValue)
                                    nextCourse = courses[i + 1];
                                break;
                            }
                        }
                        if (nextCourse != null)
                            studentService.RegisterEnrollmentStudent(item, input.PeriodId.Value, nextCourse.Id, AbpSession.TenantId.Value, false);

                    }

                }

            }
            if (periodToUse == null)
                periodToUse = currentTenant.Periods;

            var enrollment = await _enrollmentRepository.GetAsync(input.EnrollmentId);
            var enrollmentUsed = ObjectMapper.Map<EnrollmentDto2>(enrollment);






            List<int> IndexToErase = new List<int>();
            for (int i = 0; i < enrollmentUsed.EnrollmentStudents.Count; i++)
            {
                if (enrollmentUsed.EnrollmentStudents[i].PeriodId != periodToUse.Id)
                    IndexToErase.Add(i);
            }

            for (int i = IndexToErase.Count; i > 0; i--)
            {
                var enX = enrollmentUsed.EnrollmentStudents[IndexToErase[i - 1]];
                enrollmentUsed.EnrollmentStudents.Remove(enX);
            }

            GdPagedResultRequestDto pagere = new GdPagedResultRequestDto();
            pagere.SkipCount = 0;
            pagere.MaxResultCount = 1000;

            var paymentDates = await _paymentDateRepository.GetAllPaymentDates(pagere);

            if (paymentDates.Items.Count == 0)
                throw new UserFriendlyException(L("YouMustCreateThePaymentDateBeforeCreateAPaymentProjection"));

            var startDate = new DateTime(periodToUse.StartDate.Year, periodToUse.StartDate.Month, periodToUse.StartDate.Day);

            var paymentProyectoExisting = _paymentProjectionsRepository.GetAllList(x =>
                    x.EnrollmentStudents.EnrollmentId == input.EnrollmentId &&
                    x.EnrollmentStudents.PeriodId == periodToUse.Id &&
                    x.PaymentDate >= startDate &&
                    x.IsDeleted == false

                    ).ToList(); //TODO: Verificar que se este buscando solo la del periodo actual

            //Si es el next period, siempre tendra valor el periodId, si es el current, este atributo estara vacio.
            if (!input.PeriodId.HasValue)
                enrollmentUsed.EnrollmentStudents = enrollmentUsed.EnrollmentStudents.Where(x => x.IsActive == true).ToList();
            if (paymentProyectoExisting.Count > 0)
            {
                ouput.HasAlreadyPaymentProjections = true;
                CreateTutorProyectionWithExistingStudentProyection(enrollmentUsed, paymentProyectoExisting);

                foreach (var item in enrollmentUsed.EnrollmentStudents)
                {
                    var courseValues = await _courseValueRepository.GetCourseValue(item.CourseEnrollmentStudents.First().CourseId,
                        periodToUse.Id);

                    if (courseValues == null)
                        throw new UserFriendlyException(L("YouMustSetTheCourseValuesBeforeCreatePaymentProjection"));

                    //Viendo que existten para un estudiante en especifico lo utilizamos para asignar los montos.
                    if (paymentProyectoExisting.Any(x => x.EnrollmentStudentId == item.Id))
                    {
                        item.Students.InscriptionAmount = paymentProyectoExisting.Where(x => x.Sequence == 0 && item.Id == x.EnrollmentStudentId).Sum(x => x.Amount);
                        item.Students.TotalAmount = paymentProyectoExisting.Where(x => x.Sequence != 0 && item.Id == x.EnrollmentStudentId).Sum(x => x.Amount);
                        item.Students.CourseDescription = courseValues.Courses_Name;

                        //CreateStudentProyection(item, paymentDates.Items, courseValues, currentTenant.Periods.StartDate.Year);
                        item.PaymentProjections = ObjectMapper.Map<List<PaymentProjecctions.Dto.PaymentProjectionDto>>
                            (paymentProyectoExisting.Where(x => x.EnrollmentStudentId == item.Id).ToList());
                    }
                    //Si no existen payment projection de un estudiante en particular, entonces lo creamos 
                    else
                    {

                        item.Students.InscriptionAmount = courseValues.InscriptionAmount;
                        item.Students.TotalAmount = courseValues.TotalAmount;
                        item.Students.CourseDescription = courseValues.Courses_Name;
                        var initialYear = periodToUse.StartDate.Year;
                        if (paymentDates.Items.Count > 0)
                            initialYear = paymentDates.Items.Where(x => x.Sequence == 0).FirstOrDefault().PaymentYear;
                        if (initialYear == 0)
                            initialYear = periodToUse.StartDate.Year;
                        CreateStudentProyection(item, paymentDates.Items, courseValues, initialYear);
                    }
                }
            }
            else
            {

                ouput.HasAlreadyPaymentProjections = false;
                foreach (var item in enrollmentUsed.EnrollmentStudents)
                {
                    var courseValues = await _courseValueRepository.GetCourseValue(item.CourseEnrollmentStudents.First().CourseId,
                       periodToUse.Id);

                    if (courseValues == null)
                        throw new UserFriendlyException(L("YouMustSetTheCourseValuesBeforeCreatePaymentProjection"));

                    item.Students.InscriptionAmount = courseValues.InscriptionAmount;
                    item.Students.TotalAmount = courseValues.TotalAmount;
                    item.Students.CourseDescription = courseValues.Courses_Name;
                    var initialYear = periodToUse.StartDate.Year;
                    if (paymentDates.Items.Count > 0)
                        initialYear = paymentDates.Items.Where(x => x.Sequence == 0).FirstOrDefault().PaymentYear;
                    if (initialYear == 0)
                        initialYear = periodToUse.StartDate.Year;

                    CreateStudentProyection(item, paymentDates.Items, courseValues, initialYear);
                }

                CreateTutorProyection(enrollmentUsed, input.PeriodId);

            }



            ouput.NumberOfPayments = paymentDates.Items.Count - 1;
            ouput.Enrollment = enrollmentUsed;



            return ouput;
        }


        public async Task<SetPaymentProjectionToNumberOuputDto> SetPaymentProjectionToNumber(SetPaymentProjectionToNumberInputDto input)
        {
            SetPaymentProjectionToNumberOuputDto ouput = new SetPaymentProjectionToNumberOuputDto();

            List<TutorPaymentProyectionDto> newPaymentProyections = new List<TutorPaymentProyectionDto>();
            var inscription = input.PaymentProjections.Where(x => x.Sequence == 0).FirstOrDefault();

            var firstMonth = input.PaymentProjections.OrderBy(x => x.Sequence).FirstOrDefault();

            var TotalAmount = input.PaymentProjections.ToList().Where(x => x.Sequence > 0).Sum(x => x.Amount);
            var monthlyPayment = TotalAmount / input.NumberOfPayments;
            var initialDate = input.PaymentProjections.Where(x => x.Sequence == 1).FirstOrDefault().Date;
            var inscriptionAmount = input.PaymentProjections.ToList().Where(x => x.Sequence == 0).Sum(x => x.Amount);

            if (inscription == null || inscriptionAmount == 0)
            {
                var student1 = input.EnrollmentStudents.FirstOrDefault();
                if (student1 != null)
                    inscriptionAmount = student1.Students.InscriptionAmount;
            }

            if (inscription != null)
                newPaymentProyections.Add(new TutorPaymentProyectionDto(0, inscription.Date, inscription.Amount == 0 ? inscriptionAmount : inscription.Amount));
            else
            {
                newPaymentProyections.Add(new TutorPaymentProyectionDto(0, firstMonth.Date, inscriptionAmount));
            }

            DateTime paymentDate = initialDate;
            for (int i = 0; i < input.NumberOfPayments; i++)
            {
                var newTutorPP = new TutorPaymentProyectionDto(i + 1, initialDate.AddMonths(i), monthlyPayment);
                newPaymentProyections.Add(newTutorPP);

            }


            ouput.PaymentProjections = await Task.Run(() => { return newPaymentProyections.ToList(); });

            ouput.EnrollmentStudents = ExtractEnrollmentStudentPaymentFromTutorPaymentProyection(input, ouput.PaymentProjections);


            return ouput;
        }

        private List<EnrollmentStudentDto2> ExtractEnrollmentStudentPaymentFromTutorPaymentProyection(SetPaymentProjectionToNumberInputDto input, List<TutorPaymentProyectionDto> TutorPaymentProjections)
        {
            List<EnrollmentStudentDto2> enrollmentStud = new List<EnrollmentStudentDto2>();

            foreach (var es in input.EnrollmentStudents)
            {
                EnrollmentStudents.Dto.EnrollmentStudentDto2 esNew = new EnrollmentStudentDto2();
                var inscript = es.PaymentProjections.Where(x => x.Sequence == 0).FirstOrDefault();
                //Si no aparece recibo de inscripcion continuaara con el estudiante siguiente, con esto indicamos que todos deben tener inscripcion
                //if (inscript == null)
                //    continue;
                var MontlyPaymentProjections = es.PaymentProjections.Where(x => x.Sequence > 0).ToList();

                var TotalAmountStudent = MontlyPaymentProjections.Sum(x => x.Amount);
                var MontlyPayment = TotalAmountStudent / input.NumberOfPayments;

                PaymentProjecctions.Dto.PaymentProjectionDto pptmp = new PaymentProjecctions.Dto.PaymentProjectionDto();
                pptmp.Sequence = 0;
                if (inscript == null)
                    pptmp.Amount = 0;
                else
                    pptmp.Amount = inscript.Amount;
                pptmp.EnrollmentStudentId = es.Id;
                var inscriptionRecord = input.PaymentProjections.Where(x => x.Sequence == 0).FirstOrDefault();
                var firstMonth = input.PaymentProjections.Where(x => x.Sequence == 1).FirstOrDefault();
                if (inscriptionRecord != null)
                    pptmp.PaymentDate = inscriptionRecord.Date;
                else
                {
                    if (firstMonth != null)
                        pptmp.PaymentDate = firstMonth.Date;
                }
                esNew.PaymentProjections.Add(pptmp);

                for (int i = 0; i < input.NumberOfPayments; i++)
                {
                    PaymentProjecctions.Dto.PaymentProjectionDto pptmpx = new PaymentProjecctions.Dto.PaymentProjectionDto();
                    pptmpx.Sequence = i + 1;
                    pptmpx.Amount = MontlyPayment;
                    pptmpx.EnrollmentStudentId = es.Id;
                    pptmpx.PaymentDate = TutorPaymentProjections.Where(x => x.Sequence == i).FirstOrDefault().Date;
                    esNew.PaymentProjections.Add(pptmpx);
                }
                esNew.CourseEnrollmentStudents = es.CourseEnrollmentStudents;
                esNew.EnrollmentId = es.EnrollmentId;
                esNew.Id = es.Id;
                esNew.RelationshipId = es.RelationshipId;
                esNew.StudentId = es.StudentId;
                esNew.Students = es.Students;

                enrollmentStud.Add(esNew);
            }
            return enrollmentStud;
        }

        private void CreateTutorProyection(EnrollmentDto2 enrollmentUsed, int? PeriodId)
        {

            enrollmentUsed.tutorPaymentProjection = new List<TutorPaymentProyectionDto>();
            var studentsToUse = enrollmentUsed.EnrollmentStudents.Where(x => x.IsActive == true).ToList();

            //Si es para el periodo siguiente, que se busquen todos los estudiantes, incluyendo los no activos
            if (PeriodId.HasValue)
                studentsToUse = enrollmentUsed.EnrollmentStudents.ToList();
            enrollmentUsed.EnrollmentStudents = studentsToUse;
            foreach (var item in studentsToUse)
            {
                //Si el paymentproyecto no esta creado lo creamos con los valores de todos los pagos del primer estudiante
                if (enrollmentUsed.tutorPaymentProjection.Count == 0)
                {
                    foreach (var stu in item.PaymentProjections)
                    {
                        enrollmentUsed.tutorPaymentProjection.Add(new TutorPaymentProyectionDto(stu.Sequence, stu.PaymentDate, stu.Amount));
                    }
                }
                //A partir de creado buscamos todos los pagos del primer estudinte y le sumamos el monto de los pagos del segundo estudiante y asi sucesivamente.
                else
                {
                    foreach (var stu in item.PaymentProjections)
                    {
                        var tutor = enrollmentUsed.tutorPaymentProjection.Where(x => x.Sequence == stu.Sequence).FirstOrDefault();
                        if (tutor != null)
                        {
                            tutor.Amount += stu.Amount;
                        }
                    }
                }
            }
        }


        private void CreateTutorProyectionWithExistingStudentProyection(EnrollmentDto2 enrollmentUsed, List<Models.PaymentProjections> studentProyections)
        {

            enrollmentUsed.tutorPaymentProjection = new List<TutorPaymentProyectionDto>();

            studentProyections = studentProyections.OrderBy(x => x.Sequence).ToList();

            var enrollmentStudentLast = studentProyections.First().Sequence;

            foreach (var item in studentProyections)
            {
                if (enrollmentUsed.tutorPaymentProjection.Count == 0 || item.Sequence != enrollmentStudentLast)
                {
                    enrollmentUsed.tutorPaymentProjection.Add(new TutorPaymentProyectionDto(item.Sequence, item.PaymentDate, item.Amount));
                }
                //A partir de creado buscamos todos los pagos del primer estudinte y le sumamos el monto de los pagos del segundo estudiante y asi sucesivamente.
                else
                {
                    enrollmentUsed.tutorPaymentProjection.Last().Amount += item.Amount;
                }
                enrollmentStudentLast = item.Sequence;
            }
        }


        public void SavePaymentProyection(SavePaymentProyectionInput input)
        {

            var currentTenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();

            if (currentTenant != null)
            {
                if (currentTenant.Periods != null)
                {


                    var startDate = currentTenant.Periods.StartDate;

                    var existingPayments = _paymentProjectionsRepository.GetAllList(x =>
                            x.EnrollmentStudents.EnrollmentId == input.EnrollmentId &&
                            x.TransactionId == null && x.IsDeleted == false &&
                            x.PaymentDate >= startDate).ToList();

                    foreach (var item in existingPayments)
                    {
                        _paymentProjectionsRepository.Delete(item.Id);
                        CurrentUnitOfWork.SaveChanges();
                    }




                    foreach (var item in input.PaymentProjections)
                    {

                        //   if (item.EnrollmentStudentId == nodelete.Where(x => x.EnrollmentStudentId) )
                        //    { 
                        if (!existingPayments.Any(x => x.Sequence == item.Sequence) && existingPayments.Count() > 0
                            && item.Sequence <= existingPayments.Max(x => x.Sequence))
                            continue;
                        PaymentProjections currentPP = new PaymentProjections();
                        currentPP.Amount = item.Amount;
                        currentPP.PaymentDate = item.PaymentDate;
                        currentPP.Sequence = item.Sequence;
                        currentPP.IsActive = true;
                        currentPP.EnrollmentStudentId = item.EnrollmentStudentId;
                        _paymentProjectionsRepository.Insert(currentPP);
                        CurrentUnitOfWork.SaveChanges();
                        //   }
                    }
                }
            }
        }


        public async void GenerateAllPaymentProyectionForASchool()
        {
            if (!AbpSession.TenantId.HasValue)
                throw new UserFriendlyException(L("ThisOptionJustBeUsedWithUserWithTenants"));


            var currentTenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();

            if (currentTenant.Periods == null)
                throw new UserFriendlyException(L("TheSchoolDoesNotHaveSelectedTheCurrentPeriod"));

            var currentPeriod = currentTenant.Periods;

            Helpers.Conexion.EjecutarComando("exec SearchAllStudentForPendingPaymentProjectionAndCreateProjections " + currentPeriod.Id + ", " + AbpSession.TenantId.ToString());

            //var query = _enrollmentStudentsRepository.
            //    GetAllList(x => x.TenantId == AbpSession.TenantId && x.PeriodId == currentPeriod.Id);

            //var EnrollmentStudentList = ObjectMapper.Map<List<EnrollmentStudentDto2>>(query.ToList());


            //GdPagedResultRequestDto pagere = new GdPagedResultRequestDto();
            //pagere.PeriodId = currentPeriod.Id;
            //var paymentDates = _paymentDateRepository.GetAllPaymentsDates(pagere);


            //foreach (var item in EnrollmentStudentList)
            //{
            //    var courseValues = _courseValueRepository.GetCourseValues(item.CourseEnrollmentStudents.First().CourseId
            //        , currentPeriod.Id);

            //    if (courseValues == null)
            //        continue;
            //    var initialYear = currentPeriod.StartDate.Year;
            //    if (paymentDates.Items.Count > 0)
            //        initialYear = paymentDates.Items.Where(x => x.Sequence == 0).FirstOrDefault().PaymentYear;
            //    CreateAndSaveStudentProyection(item, paymentDates.Items, courseValues, initialYear, currentPeriod);
            //}

        }


        public async void GenerateAllPaymentProyectionForASchoolPriorPeriod()
        {
            if (!AbpSession.TenantId.HasValue)
                throw new UserFriendlyException(L("ThisOptionJustBeUsedWithUserWithTenants"));


            var currentTenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();

            if (currentTenant.PreviousPeriods == null)
                throw new UserFriendlyException(L("TheSchoolDoesNotHaveSelectedThePreviousPeriod"));

            var usedPeriod = currentTenant.PreviousPeriods;

            var query = _enrollmentStudentsRepository.
                GetAllList(x => x.TenantId == AbpSession.TenantId && x.PeriodId == usedPeriod.Id);

            var EnrollmentStudentList = ObjectMapper.Map<List<EnrollmentStudentDto2>>(query.ToList());


            GdPagedResultRequestDto pagere = new GdPagedResultRequestDto();
            pagere.PeriodId = usedPeriod.Id;
            var paymentDates = _paymentDateRepository.GetAllPaymentsDates(pagere);
            var initialYear = usedPeriod.StartDate.Year;
            if (paymentDates.Items.Count > 0)
                initialYear = paymentDates.Items.First().PaymentYear;
            foreach (var item in EnrollmentStudentList)
            {
                var courseValues = _courseValueRepository.GetCourseValues(item.CourseEnrollmentStudents.First().CourseId,
                     usedPeriod.Id);

                if (courseValues == null)
                    continue;
                CreateAndSaveStudentProyection(item, paymentDates.Items, courseValues, initialYear, usedPeriod);
            }

        }

        private void CreateStudentProyection(EnrollmentStudentDto2 item, IReadOnlyList<PaymentDateDto> paymentsDateSugestions, CourseValueDto courseValues, int YearOfBeginOfPeriod)
        {
            int numberofpayments = paymentsDateSugestions.Count - 1;
            var amountPerPayment = courseValues.TotalAmount / numberofpayments;
            var year = YearOfBeginOfPeriod;
            foreach (var pd in paymentsDateSugestions.OrderBy(x => x.Sequence).ToList())
            {
                //El cero es para la inscripcion, los demas son para las mensualidades
                if (pd.Sequence == 0)
                {
                    item.PaymentProjections.Add(new PaymentProjecctions.Dto.PaymentProjectionDto()
                    {
                        Sequence = pd.Sequence,
                        Amount = courseValues.InscriptionAmount,
                        EnrollmentStudentId = item.Id,
                        PaymentDate = new DateTime(year, pd.PaymentMonth, pd.PaymentDay)
                    });
                }
                else
                {
                    item.PaymentProjections.Add(new PaymentProjecctions.Dto.PaymentProjectionDto()
                    {
                        Sequence = pd.Sequence,
                        Amount = amountPerPayment,
                        EnrollmentStudentId = item.Id,
                        PaymentDate = new DateTime(year, pd.PaymentMonth, pd.PaymentDay)
                    });
                }

                if (pd.PaymentMonth == 12)
                    year++;
            }

        }

        private void CreateAndSaveStudentProyection(EnrollmentStudentDto2 item, IReadOnlyList<PaymentDateDto> paymentsDateSugestions, CourseValueDto courseValues, int YearOfBeginOfPeriod, Models.Periods period)
        {
            try
            {
                //Si aparece un paymentprojection que corresponde al enrollment actual y que pertenece
                // al periodo actual, entonces no se crea el payment proyection, debido que si se hace
                // nuevamente se produciria un conflicto.
                if (_paymentProjectionsRepository.GetAllList(x => x.EnrollmentStudentId == item.Id
                 && x.IsActive == true && x.PaymentDate >= period.StartDate
                 && x.PaymentDate <= period.EndDate).Any())
                    return;

                int numberofpayments = paymentsDateSugestions.Count - 1;
                var amountPerPayment = courseValues.TotalAmount / numberofpayments;
                var year = YearOfBeginOfPeriod;
                var pss = paymentsDateSugestions.OrderBy(x => x.Sequence).ToList();
                foreach (var pd in pss)
                {
                    Models.PaymentProjections pp = new PaymentProjections();
                    //El cero es para la inscripcion, los demas son para las mensualidades
                    if (pd.Sequence == 0)
                    {
                        pp.Sequence = pd.Sequence;
                        pp.Amount = courseValues.InscriptionAmount;
                        pp.EnrollmentStudentId = item.Id;
                        pp.PaymentDate = new DateTime(year, pd.PaymentMonth, pd.PaymentDay);
                    }
                    else
                    {
                        pp.Sequence = pd.Sequence;
                        pp.Amount = amountPerPayment;
                        pp.EnrollmentStudentId = item.Id;
                        pp.PaymentDate = new DateTime(year, pd.PaymentMonth, pd.PaymentDay);
                    }
                    pp.IsActive = true;
                    _paymentProjectionsRepository.Insert(pp);
                    //CurrentUnitOfWork.SaveChanges();
                    if (pd.PaymentMonth == 12)
                        year++;
                }
            }
            catch (Exception err)
            {
                Logger.Error(err.Message);
                if (err.InnerException != null)
                    Logger.Error(err.InnerException.Message);
            }

        }

        public async Task<GetEnrollmentsByStudentNameOuput> GetEnrollmentsByStudentName(GdPagedResultRequestDto input)
        {
            GetEnrollmentsByStudentNameOuput output = new GetEnrollmentsByStudentNameOuput();

            var query = from x in _enrollmentRepository.GetAll()
                        join x2 in _enrollmentSequenceRepository.GetAll()
                        on x.Id equals x2.EnrollmentId
                        where x2.TenantId == AbpSession.TenantId.Value
                        orderby x.EnrollmentSequences.FirstOrDefault().Sequence
                        select x;

            var enrollmentStudentsQuery = await _enrollmentStudentsRepository.GetAllListAsync(x => (x.Students.FirstName + " " + x.Students.LastName).Contains(input.TextFilter));
            var enrollmentStudents = enrollmentStudentsQuery.Select(x => new EnrollmentStudentQuery
            {
                EnrollmentId = x.Enrollments.EnrollmentSequences.First().EnrollmentId,
                Matricula = x.Enrollments.EnrollmentSequences.First().Sequence,
                StudentName = x.Students.FirstName + " " + x.Students.LastName,
                EnrollmentName = x.Enrollments.FirstName + " " + x.Enrollments.LastName,
            });

            output.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                enrollmentStudents = enrollmentStudents.Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                enrollmentStudents = enrollmentStudents.Take(input.MaxResultCount);
            }

            output.EnrollmentStudents = enrollmentStudents.ToList();

            return output;
        }



        public async Task<PagedResultDto<EnrollmentDto>> GetAllEnrollmentsForQueries(GdPagedResultRequestDto input)
        {
            PagedResultDto<EnrollmentDto> ouput = new PagedResultDto<EnrollmentDto>();
            IQueryable<Models.Enrollments> query = query = from x in _enrollmentRepository.GetAll() select x;
            if (!input.PeriodId.HasValue)
            {
                throw new Exception(L("YouMustSelectThePeriodBeforeSearchATutor"));
            }


            if (AbpSession.TenantId.HasValue)
            {
                query = from x in query
                        from x2 in x.EnrollmentSequences
                        where x2.TenantId == AbpSession.TenantId.Value
                        orderby x2.Sequence
                        select x;
            }

            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                if (long.TryParse(input.TextFilter, out long searchId))
                {
                    query = from x in query
                            from x2 in x.EnrollmentSequences
                            where x.Id == searchId ||
                                    x2.Sequence == searchId
                            orderby x2.Sequence
                            select x;
                }
                else
                {
                    query = (from x in query
                             from x2 in x.EnrollmentSequences
                             from x3 in x.EnrollmentStudents
                             where ((x.FirstName + " " + x.LastName).Contains(input.TextFilter))
                                     && x3.IsActive == true
                             orderby x2.Sequence
                             select x).Distinct();
                }

            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Enrollments.Dto.EnrollmentDto>>(query.ToList());

            foreach (var item in ouput.Items)
            {
                item.Enrollment = item.EnrollmentSequences.First().Sequence;

                item.EnrollmentStudents = item.EnrollmentStudents.Where(x => x.IsActive == true && x.PeriodId == input.PeriodId).ToList();

                item.NumberOfStudents = item.EnrollmentStudents.Count;

                item.StudentsNames = "";
                foreach (var student2 in item.EnrollmentStudents)
                {
                    item.StudentsNames += student2.Students.FullName + ", ";
                }
            }

            return ouput;
        }
    }
};
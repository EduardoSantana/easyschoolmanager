﻿//Created from Templaste MG

namespace EasySchoolManager.Enrollments
{
    public class EnrollmentStudentQuery
    {
        public long EnrollmentId { get; set; }
        public int Matricula { get; set; }
        public string StudentName { get; set; }
        public string EnrollmentName { get; set; }
    }
}
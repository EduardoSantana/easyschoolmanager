﻿//Created from Templaste MG

using System.Collections.Generic;

namespace EasySchoolManager.Enrollments
{
    public class GetEnrollmentsByStudentNameOuput
    {
        public List<EnrollmentStudentQuery> EnrollmentStudents { get; set; }
        public int TotalCount { get; internal set; }
    }
}
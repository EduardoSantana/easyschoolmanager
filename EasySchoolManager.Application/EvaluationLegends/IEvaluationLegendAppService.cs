//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.EvaluationLegends.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.EvaluationLegends
{
      public interface IEvaluationLegendAppService : IAsyncCrudAppService<EvaluationLegendDto, int, PagedResultRequestDto, CreateEvaluationLegendDto, UpdateEvaluationLegendDto>
      {
            Task<PagedResultDto<EvaluationLegendDto>> GetAllEvaluationLegends(GdPagedResultRequestDto input);
			Task<List<Dto.EvaluationLegendDto>> GetAllEvaluationLegendsForCombo();
      }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.EvaluationLegends.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.EvaluationLegends 
{ 
    [AbpAuthorize(PermissionNames.Pages_EvaluationLegends)] 
    public class EvaluationLegendAppService : AsyncCrudAppService<Models.EvaluationLegends, EvaluationLegendDto, int, PagedResultRequestDto, CreateEvaluationLegendDto, UpdateEvaluationLegendDto>, IEvaluationLegendAppService 
    { 
        private readonly IRepository<Models.EvaluationLegends, int> _evaluationLegendRepository;
		


        public EvaluationLegendAppService( 
            IRepository<Models.EvaluationLegends, int> repository, 
            IRepository<Models.EvaluationLegends, int> evaluationLegendRepository 
            ) 
            : base(repository) 
        { 
            _evaluationLegendRepository = evaluationLegendRepository; 
			

			
        } 
        public override async Task<EvaluationLegendDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<EvaluationLegendDto> Create(CreateEvaluationLegendDto input) 
        { 
            CheckCreatePermission(); 
            var evaluationLegend = ObjectMapper.Map<Models.EvaluationLegends>(input); 
			try{
              await _evaluationLegendRepository.InsertAsync(evaluationLegend); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(evaluationLegend); 
        } 
        public override async Task<EvaluationLegendDto> Update(UpdateEvaluationLegendDto input) 
        { 
            CheckUpdatePermission(); 
            var evaluationLegend = await _evaluationLegendRepository.GetAsync(input.Id);
            MapToEntity(input, evaluationLegend); 
		    try{
               await _evaluationLegendRepository.UpdateAsync(evaluationLegend); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var evaluationLegend = await _evaluationLegendRepository.GetAsync(input.Id); 
               await _evaluationLegendRepository.DeleteAsync(evaluationLegend);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.EvaluationLegends MapToEntity(CreateEvaluationLegendDto createInput) 
        { 
            var evaluationLegend = ObjectMapper.Map<Models.EvaluationLegends>(createInput); 
            return evaluationLegend; 
        } 
        protected override void MapToEntity(UpdateEvaluationLegendDto input, Models.EvaluationLegends evaluationLegend) 
        { 
            ObjectMapper.Map(input, evaluationLegend); 
        } 
        protected override IQueryable<Models.EvaluationLegends> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.EvaluationLegends> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.EvaluationLegends> GetEntityByIdAsync(int id) 
        { 
            var evaluationLegend = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(evaluationLegend); 
        } 
        protected override IQueryable<Models.EvaluationLegends> ApplySorting(IQueryable<Models.EvaluationLegends> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<EvaluationLegendDto>> GetAllEvaluationLegends(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<EvaluationLegendDto> ouput = new PagedResultDto<EvaluationLegendDto>(); 
            IQueryable<Models.EvaluationLegends> query = query = from x in _evaluationLegendRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _evaluationLegendRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<EvaluationLegends.Dto.EvaluationLegendDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<EvaluationLegendDto>> GetAllEvaluationLegendsForCombo()
        {
            var evaluationLegendList = await _evaluationLegendRepository.GetAllListAsync(x => x.IsActive == true);

            var evaluationLegend = ObjectMapper.Map<List<EvaluationLegendDto>>(evaluationLegendList.ToList());

            return evaluationLegend;
        }
		
    } 
} ;
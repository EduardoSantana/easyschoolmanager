//Created from Templaste MG

using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace EasySchoolManager.EvaluationLegends.Dto
{
    [AutoMap(typeof(Models.EvaluationLegends))]
    public class EvaluationLegendDto : EntityDto<int>
    {
        public string Name { get; set; }
        public String Description { get; set; }
        public int Sequence { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

    }
}
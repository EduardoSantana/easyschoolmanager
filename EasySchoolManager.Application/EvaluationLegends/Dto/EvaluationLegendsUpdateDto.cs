//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.EvaluationLegends.Dto
{
    [AutoMap(typeof(Models.EvaluationLegends))]
    public class UpdateEvaluationLegendDto : EntityDto<int>
    {

        [StringLength(1)]
        public string Name { get; set; }

        [StringLength(255)]
        public String Description { get; set; }

        public int Sequence { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

    }
}
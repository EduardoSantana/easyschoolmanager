﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.StudentAllergies.Dto
{
    [AutoMap(typeof(Models.StudentAllergies))]
    public class StudentAlergyCreateDto : EntityDto
    {
        public int StudentId { get; set; }
        public int AllergyId { get; set; }
    }
}

﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.StudentAllergies.Dto
{
    [AutoMap(typeof(Models.StudentAllergies))]
    public class StudentAllergiesDto
    {
        public int StudentId { get; set; }
        public int AllergyId { get; set; }

        public Allergies.Dto.AllergyDto Allergies { get; set; }
    }
}

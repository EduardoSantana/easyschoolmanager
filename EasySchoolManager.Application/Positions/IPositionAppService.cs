//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Positions.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Positions
{
      public interface IPositionAppService : IAsyncCrudAppService<PositionDto, int, PagedResultRequestDto, CreatePositionDto, UpdatePositionDto>
      {
            Task<PagedResultDto<PositionDto>> GetAllPositions(GdPagedResultRequestDto input);
			Task<List<Dto.PositionDto>> GetAllPositionsForCombo();
      }
}
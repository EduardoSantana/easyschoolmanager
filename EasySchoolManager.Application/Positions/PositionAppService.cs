//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Positions.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Positions 
{ 
    [AbpAuthorize(PermissionNames.Pages_Positions)] 
    public class PositionAppService : AsyncCrudAppService<Models.Positions, PositionDto, int, PagedResultRequestDto, CreatePositionDto, UpdatePositionDto>, IPositionAppService 
    { 
        private readonly IRepository<Models.Positions, int> _positionRepository;
		


        public PositionAppService( 
            IRepository<Models.Positions, int> repository, 
            IRepository<Models.Positions, int> positionRepository 
            ) 
            : base(repository) 
        { 
            _positionRepository = positionRepository; 
			

			
        } 
        public override async Task<PositionDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<PositionDto> Create(CreatePositionDto input) 
        { 
            CheckCreatePermission(); 
            var position = ObjectMapper.Map<Models.Positions>(input); 
			try{
              await _positionRepository.InsertAsync(position); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(position); 
        } 
        public override async Task<PositionDto> Update(UpdatePositionDto input) 
        { 
            CheckUpdatePermission(); 
            var position = await _positionRepository.GetAsync(input.Id);
            MapToEntity(input, position); 
		    try{
               await _positionRepository.UpdateAsync(position); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var position = await _positionRepository.GetAsync(input.Id); 
               await _positionRepository.DeleteAsync(position);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Positions MapToEntity(CreatePositionDto createInput) 
        { 
            var position = ObjectMapper.Map<Models.Positions>(createInput); 
            return position; 
        } 
        protected override void MapToEntity(UpdatePositionDto input, Models.Positions position) 
        { 
            ObjectMapper.Map(input, position); 
        } 
        protected override IQueryable<Models.Positions> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Positions> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Positions> GetEntityByIdAsync(int id) 
        { 
            var position = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(position); 
        } 
        protected override IQueryable<Models.Positions> ApplySorting(IQueryable<Models.Positions> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<PositionDto>> GetAllPositions(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<PositionDto> ouput = new PagedResultDto<PositionDto>(); 
            IQueryable<Models.Positions> query = query = from x in _positionRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _positionRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Positions.Dto.PositionDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<PositionDto>> GetAllPositionsForCombo()
        {
            var positionList = await _positionRepository.GetAllListAsync(x => x.IsActive == true);

            var position = ObjectMapper.Map<List<PositionDto>>(positionList.ToList());

            return position;
        }
		
    } 
} ;
//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Transactions.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.Reports;
using EasySchoolManager.Reports.Dto;
using EasySchoolManager.ReportsFilters.Dto;
using Abp.Domain.Uow;
using EasySchoolManager.Articles.Dto;
using EasySchoolManager.TaxReceipts;

namespace EasySchoolManager.Transactions
{
    [AbpAuthorize(new string[] { PermissionNames.Pages_Receipts, PermissionNames.Pages_Transactions, PermissionNames.Pages_Deposits })]
    public class TransactionAppService : AsyncCrudAppService<Models.Transactions, TransactionDto, long,
        PagedResultRequestDto, CreateTransactionDto, UpdateTransactionDto>, ITransactionAppService
    {
        private readonly IRepository<Models.Transactions, long> _transactionRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly MultiTenancy.TenantManager _tenantManager;

        private readonly IRepository<Models.Banks, int> _bankRepository;
        private readonly IRepository<Models.TransactionQueues, long> _transactionQueuesRepository;
        private readonly IRepository<Models.PaymentMethods, int> _paymentMethodRepository;
        private readonly IRepository<Models.TransactionConcepts, int> _transactionConceptRepository;
        private readonly IRepository<Models.Enrollments, long> _enrollmentRepository;
        private readonly IRepository<Models.EnrollmentSequences, int> _enrollmentSequenceRepository;
        private readonly IRepository<Models.TransactionTypes, int> _transactionTypesRepository;
        private readonly IRepository<Models.EnrollmentStudents, int> _enrollmentStudentRepository;
        private readonly IRepository<Models.PaymentProjections, int> _paymentProjectionsRepository;
        private readonly IRepository<Models.ApplicationDates, int> _applicationDatesRepository;
        private readonly IRepository<Models.MasterTenants, int> _masterTenantsRepository;
        private readonly IReportAppService _reportService;
        private readonly int TRANSACTION_TYPE_RECEIPT = 1;
        private readonly int TRANSACTION_TYPE_DEPOSIT = 2;
        private readonly int TRANSACTION_TYPE_TRANSACTION = 3;
        private readonly IRepository<Models.TransactionStudents, int> _transactionStudentRepository;
        private readonly IRepository<Models.TransactionArticles, int> _transactionArticles;
        private readonly IRepository<Models.Inventory, int> _InventoryRepository;
        private readonly IRepository<Models.TaxReceipts, int> _TaxReceipts;
        private readonly TaxReceiptAppService _taxReceiptService;
        private readonly IRepository<Models.Companies, long> _companyRepository;

        public TransactionAppService(
            IRepository<Models.Transactions, long> repository,
            IRepository<Models.Transactions, long> transactionRepository,
            IRepository<Models.TransactionQueues, long> transactionQueuesRepository,
            IRepository<Models.Banks, int> bankRepository,
            IRepository<Models.PaymentMethods, int> paymentMethodRepository,
            IRepository<Models.Enrollments, long> enrollmentRepository,
            IRepository<Models.EnrollmentSequences, int> enrollmentSequenceRepository,
            IRepository<Models.TransactionTypes, int> transactionTypesRepository,
            IRepository<Models.ApplicationDates, int> applicationDatesRepository,
            IReportAppService reportService,
            IRepository<Models.PaymentProjections, int> paymentProjectionsRepository,
            IRepository<Models.MasterTenants, int> masterTenantsRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Models.TransactionStudents, int> transactionStudentRepository,
            IRepository<Models.TransactionConcepts, int> transactionConceptRepository,
            IRepository<Models.TransactionArticles, int> transactionArticles,
            IRepository<Models.Inventory, int> InventoryRepository,
            IRepository<Models.EnrollmentStudents, int> enrollmentStudentRepository,
            MultiTenancy.TenantManager tenantManager,
            IRepository<Models.TaxReceipts, int> taxReceipts,
            TaxReceiptAppService taxReceiptService,
            IRepository<Models.Companies, long> companyRepository
            )
            : base(repository)
        {
            _transactionRepository = transactionRepository;
            _transactionQueuesRepository = transactionQueuesRepository;
            _bankRepository = bankRepository;
            _paymentMethodRepository = paymentMethodRepository;
            _transactionStudentRepository = transactionStudentRepository;
            _transactionConceptRepository = transactionConceptRepository;
            _enrollmentStudentRepository = enrollmentStudentRepository;
            _enrollmentRepository = enrollmentRepository;
            _enrollmentSequenceRepository = enrollmentSequenceRepository;
            _transactionTypesRepository = transactionTypesRepository;
            _reportService = reportService;
            _paymentProjectionsRepository = paymentProjectionsRepository;
            _applicationDatesRepository = applicationDatesRepository;
            _masterTenantsRepository = masterTenantsRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _transactionArticles = transactionArticles;
            _InventoryRepository = InventoryRepository;
            this.LocalizationSourceName = EasySchoolManagerConsts.LocalizationSourceName;
            _tenantManager = tenantManager;
            _TaxReceipts = taxReceipts;
            _taxReceiptService = taxReceiptService;
            _companyRepository = companyRepository;
        }

        public static bool AppliyingPaymentProjections = false;

        [AbpAllowAnonymous]
        [UnitOfWork(isTransactional: false)]
        public async Task ApplyExpiredPaymentProjections()
        {
            //int WorkerId = 0;

            //int Iteracion = 0;

            //if (AppliyingPaymentProjections == true)
            //    return;

            //AppliyingPaymentProjections = true;

            //try
            //{
            //    var dt = DateTime.Now;

            //    var CurrentDateInit = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
            //    var CurrentDateEnd = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
            //    var appDates = _applicationDatesRepository.GetAllList(x => x.ApplicationDate >= CurrentDateInit &&
            //   x.ApplicationDate <= CurrentDateEnd &&
            //   x.WorkerId == WorkerId);

            //    var appDate = appDates.FirstOrDefault();

            //    ////Si aparece ya un registro de applicationDates entonces 
            //    //if (appDate != null)
            //    //    return;

            //    var masterTenants = await _masterTenantsRepository.GetAllListAsync(x => x.IsActive == true);
            //    var masterTenant = masterTenants.FirstOrDefault();
            //    if (masterTenant == null)
            //    {
            //        Logger.Error("MasterTenant is Empty, the administrator must create the master tenant");
            //        return;
            //    }
            //    if (!masterTenant.ConceptForInscriptionTransactionsId.HasValue)
            //    {
            //        Logger.Error("The Master Tenant does not have selected the concept tha will be used for inscription transactions");
            //        return;
            //    }
            //    if (!masterTenant.ConceptForMonthlyPaymentTransactionsId.HasValue)
            //    {
            //        Logger.Error("The Master Tenant does not have selected the concept tha will be used for payments transactions");
            //        return;
            //    }

            //    //var currentTenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();
            //    //if (currentTenant == null)
            //    //{
            //    //    Logger.Error("This try of apply automatic charged does no have tenant");
            //    //    return;
            //    //}

            //    //if (!currentTenant.ApplyAutomaticCharges)
            //    //{
            //    //    Logger.Error("This tenant does not have markedt the Apply Automatic Charges option");
            //    //    return;
            //    //}


            //    //Desabilitamos el filtro por tenant, para que se puedan buscar todos los tenants
            //    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            //    {

            //        //Buscamos todos los colegios que tengan activado el applicar los cambios automaticos
            //        var tenantsAplicable = _tenantManager.Tenants.Where(x => x.ApplyAutomaticCharges == true).ToList();
            //        foreach (var currentTenant in tenantsAplicable)
            //        {

            //            var query = _paymentProjectionsRepository.GetAllList(x => x.IsActive == true &&
            //            x.PaymentDate <= CurrentDateInit && x.Applied == false && x.TenantId == currentTenant.Id && x.EnrollmentStudents.IsDeleted == false);

            //            var paymentProjectionsExpired = query.OrderBy(x => x.EnrollmentStudentId).ThenBy(x => x.Sequence).ToList();

            //            var acumulators = new List<Helpers.EnrollmentAcumulator>();

            //            for (int i = 0; i < paymentProjectionsExpired.Count; i++)
            //            {
            //                Iteracion = i;
            //                var item = paymentProjectionsExpired[i];

            //                bool isNew = false;
            //                var acum = acumulators.Where(x => x.EnrollmentId == item.EnrollmentStudents.EnrollmentId
            //                 && x.Sequence == item.Sequence && x.TenantId == item.TenantId).FirstOrDefault();
            //                if (acum == null)
            //                {
            //                    acum = new Helpers.EnrollmentAcumulator();
            //                    acum.Sequence = item.Sequence;
            //                    acum.PaymentDate = item.PaymentDate;
            //                    acum.EnrollmentId = item.EnrollmentStudents.EnrollmentId;
            //                    acum.TenantId = item.TenantId;
            //                    acum.FullName = item.EnrollmentStudents.Enrollments.FirstName + " " + item.EnrollmentStudents.Enrollments.LastName;
            //                    isNew = true;
            //                }
            //                acum.Amount += item.Amount;
            //                acum.PaymentProjectionIdList.Add(item.Id);
            //                Helpers.TransactionStudentTmp newStudent = new Helpers.TransactionStudentTmp();
            //                newStudent.Amount = item.Amount;
            //                newStudent.EnrollmentStudentId = item.EnrollmentStudentId;
            //                newStudent.PaymentDate = item.PaymentDate;
            //                newStudent.Sequence = item.Sequence;
            //                newStudent.TenantId = item.TenantId;
            //                newStudent.PaymentProjectionId = item.Id;
            //                acum.Students.Add(newStudent);

            //                if (isNew)
            //                    acumulators.Add(acum);
            //            }

            //            foreach (var item in acumulators)
            //            {
            //                //Llamamos el m�todo que ira registrando las transacciones y cuando termine guardara
            //                //el registro en la rabla applicationDates
            //                CreateChargeTransaction(masterTenant, item);
            //            }


            //            //--------
            //        }

            //        //Agregamos la fecha de hoy al application date

            //        Models.ApplicationDates newAppDate = new Models.ApplicationDates();

            //        newAppDate.ApplicationDate = dt;
            //        newAppDate.IsActive = true;
            //        newAppDate.WorkerId = WorkerId;
            //        _applicationDatesRepository.Insert(newAppDate);
            //        CurrentUnitOfWork.SaveChanges();
            //    }
            //}
            //catch (Exception err)
            //{
            //    var ix = Iteracion;
            //    Logger.Error("Error while was running the process ApplyExpiredPaymentProjections");
            //    Logger.Error(err.Message);
            //}
            //finally
            //{
            //    AppliyingPaymentProjections = false;
            //}

        }

        [UnitOfWork(isTransactional: false)]
        public void CreateChargeTransaction(Models.MasterTenants masterTenant, Helpers.EnrollmentAcumulator item)
        {
            bool leave = false;
            try
            {
                //Si los paymentProjections ya fueron aplicados que se salga del metodo sin hacer nada;
                if (_paymentProjectionsRepository.GetAllList(x => item.PaymentProjectionIdList.Contains(x.Id) && x.Applied == true).Any())
                    return;

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var transaction = new Models.Transactions();
                    transaction.OriginId = EasySchoolManagerConsts.Origin.Debito; //Debito
                    transaction.Amount = item.Amount;
                    if (item.Sequence == 0)
                        transaction.ConceptId = masterTenant.ConceptForInscriptionTransactionsId;
                    else
                        transaction.ConceptId = masterTenant.ConceptForMonthlyPaymentTransactionsId;

                    transaction.Date = item.PaymentDate;
                    transaction.Name = item.FullName;
                    transaction.EnrollmentId = item.EnrollmentId;
                    transaction.TransactionTypeId = EasySchoolManagerConsts.TransactionType.Transaction;
                    transaction.StatusId = EasySchoolManagerConsts.Status.Creado;
                    transaction.TenantId = item.TenantId;
                    transaction.IsActive = true;
                    transaction.Sequence = getTransactionSequence(transaction.TransactionTypeId);
                    _transactionRepository.InsertAndGetId(transaction);
                    CurrentUnitOfWork.SaveChanges();

                    foreach (var subItem in item.PaymentProjectionIdList)
                    {
                        var elemen = _paymentProjectionsRepository.Get(subItem);
                        //paymentProjectionsExpired.Where(x => x.Id == subItem).FirstOrDefault();
                        if (elemen != null)
                        {
                            if (elemen.Applied == true)
                                leave = true;

                            elemen.Applied = true;
                            elemen.TransactionId = transaction.Id;

                            Models.TransactionStudents stu = new Models.TransactionStudents();
                            stu.Amount = elemen.Amount;
                            stu.EnrollmentStudentId = elemen.EnrollmentStudentId;
                            stu.IsActive = true;
                            stu.TransactionTypeId = EasySchoolManagerConsts.TransactionType.Transaction;
                            stu.TenantId = item.TenantId;
                            stu.TransactionId = transaction.Id;
                            _transactionStudentRepository.Insert(stu);

                            CurrentUnitOfWork.SaveChanges();
                        }
                    }
                    if (leave == false)
                        unitOfWork.Complete();
                }
            }
            catch (Exception err)
            { }
        }

        public override async Task<TransactionDto> Get(EntityDto<long> input)
        {
            var transaction = await base.Get(input);

            if (transaction.TransactionTypeId != TRANSACTION_TYPE_DEPOSIT)
            {
                if (transaction.Enrollments != null)
                {
                    transaction.Enrollment = transaction.Enrollments.EnrollmentSequences.FirstOrDefault().Sequence;

                    foreach (var item in transaction.Enrollments.EnrollmentStudents)
                    {
                        var course = item.CourseEnrollmentStudents.FirstOrDefault();
                        if (course != null)
                            item.Students.Course = course.Courses;
                        var transactionStudent = item.TransactionStudents.Where(x => x.TransactionId == input.Id && x.EnrollmentStudentId == item.Id).FirstOrDefault();
                        if (transactionStudent != null)
                            item.Students.Amount = transactionStudent.Amount;
                    }
                }

            }
            return transaction;
        }
        public override async Task<TransactionDto> Create(CreateTransactionDto input)
        {
            CheckCreatePermission();

            if (input.IsMultiConcept)
            {
                input.ConceptId = null;
            }
            if (input.IsMultiConcept && (input.TransactionConcepts == null || !input.TransactionConcepts.Any()))
            {
                throw new UserFriendlyException(L("NotConceptsReceipts"));
            }
            var transaction = ObjectMapper.Map<Models.Transactions>(input);
            transaction.Sequence = getTransactionSequence(transaction.TransactionTypeId);
            try
            {
                await _transactionRepository.InsertAndGetIdAsync(transaction);
                if (input.Students != null)
                    foreach (var item in input.Students)
                    {
                        Models.TransactionStudents ts = new Models.TransactionStudents();
                        ts.Amount = item.Amount;
                        ts.EnrollmentStudentId = item.EnrollmentStudentId;
                        ts.TransactionId = transaction.Id;
                        ts.TransactionTypeId = transaction.TransactionTypeId;
                        ts.IsActive = true;

                        _transactionStudentRepository.Insert(ts);

                    }

                var tq = new Models.TransactionQueues
                {
                    TransactionId = transaction.Id,
                    IsActive = true
                };
                _transactionQueuesRepository.Insert(tq);

            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return ObjectMapper.Map<TransactionDto>(transaction);
        }

        [UnitOfWork(IsDisabled = true)]
        public async Task<TransactionReportDto> CreateAndPrint(CreateTransactionDto input)
        {

            if (input.IsMultiConcept)
            {
                input.ConceptId = null;
            }
            if (input.IsMultiConcept && (input.TransactionConcepts == null || !input.TransactionConcepts.Any()))
            {
                throw new UserFriendlyException(L("NotConceptsReceipts"));
            }

            if (input.TransactionTypeId == EasySchoolManager.EasySchoolManagerConsts.TransactionType.Receipts &&
                    input.ReceiptTypeId == EasySchoolManager.EasySchoolManagerConsts.ReceiptType.MontlyCharge &&
                    !input.EnrollmentId.HasValue)
            {
                throw new UserFriendlyException(L("ThisReceiptIsForEnrollmentButYouDonHaveAnEnrollmentSelected"));
            }

            if ((input.TaxReceiptId.HasValue && input.TaxReceiptId.Value > 0) && string.IsNullOrEmpty(input.RncNumber))
            {
                throw new UserFriendlyException(L("WhenYouSelectATaxReceiptTypeYouMustTypeARNC"));
            }

            if ((!input.TaxReceiptId.HasValue || input.TaxReceiptId.Value == 0) && !string.IsNullOrEmpty(input.RncNumber))
            {
                throw new UserFriendlyException(L("WhenYouTypeARncYouMustSelectATaxReceiptType"));
            }



            if (input.IsActive) { 
            var dtt = DateTime.Now;
            input.Date = dtt;
            }
            else { input.IsActive = true; }

            CheckCreatePermission();

            var transaction = ObjectMapper.Map<Models.Transactions>(input);
            transaction.Sequence = getTransactionSequence(transaction.TransactionTypeId);

            var _reportCode = "04"; //This is the code by default of the tenant
            TransactionReportDto reportString = null;
            try
            {
                using (var unit = UnitOfWorkManager.Begin())
                {

                    //Codigo comentado, por que las empresas deben permitir llenar a mano el rnc y el nombre de la compania,
                    //Pero dejado aqui por si en algun momento hay que validar la empresa con el RNC dado.
                    //if (!string.IsNullOrEmpty(input.RncNumber))
                    //{
                    //    var company = _companyRepository.GetAll().Where(x => x.RNC == input.RncNumber).FirstOrDefault();
                    //    if (company == null)
                    //        throw new UserFriendlyException(L("TheRNCNumberThatYouHaveWroteIsNotInOurDatabase"));
                    //}


                    var tm = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();

                    if (tm == null)
                        throw new Exception("MasterTenantMustBeConfigured");

                    decimal CardPercent = tm.CreditCardPercent;


                    //Si el tipo de transaccion es un pago con tarjeta de credito se calcula el monto de la tarjeta y se guarda en el campo cardamount.
                    if (transaction.PaymentMethodId.HasValue && transaction.PaymentMethodId == EasySchoolManagerConsts.PaymentMethods.CreditCard)
                        transaction.CreditCardPercent = CardPercent * 0.01M;
                    else
                        transaction.CreditCardPercent = 0;

                    if (input.TaxReceiptId.HasValue && input.TaxReceiptId > 0)
                    {
                       var TaxReceiptResult   = await _taxReceiptService.GetCurrentSecuenceAndIncrement(input.TaxReceiptId.Value);
                        transaction.TaxReceiptSequence = TaxReceiptResult.Sequence;
                        transaction.TaxReceiptExpirationDate = TaxReceiptResult.ExpirationDate;
                    }
                    await _transactionRepository.InsertAndGetIdAsync(transaction);
                    if (input.Students != null)
                        foreach (var item in input.Students)
                        {
                            if (input.ReceiptTypeId.HasValue && input.ReceiptTypeId == EasySchoolManagerConsts.ReceiptType.Articles)
                                continue;
                            Models.TransactionStudents ts = new Models.TransactionStudents();
                            ts.Amount = item.Amount;
                            ts.EnrollmentStudentId = item.EnrollmentStudentId;
                            ts.TransactionId = transaction.Id;
                            ts.TransactionTypeId = transaction.TransactionTypeId;
                            ts.IsActive = true;

                            _transactionStudentRepository.Insert(ts);

                        }

                    if (input.Articles != null)
                        foreach (var art in input.Articles)
                        {
                            Models.TransactionArticles ta = new Models.TransactionArticles();
                            ta.ArticleId = art.Id;
                            ta.Quantity = art.Quantity.Value;
                            ta.Amount = art.Price;
                            ta.Total = art.Total.Value;
                            ta.TransactionId = transaction.Id;
                            ta.IsActive = true;
                            _transactionArticles.Insert(ta);

                            //TODO: Agregar la parte de disminuir del inventario con los articulos vendidos.
                            DecreaseInventory(art, AbpSession.TenantId.Value);

                        }

                    var tq = new Models.TransactionQueues
                    {
                        TransactionId = transaction.Id,
                        IsActive = true
                    };
                    _transactionQueuesRepository.Insert(tq);


                    if (AbpSession.TenantId == null)
                        throw new Exception(L("ThisReceiveCanBeJustUsedWithAUserThetBelongToASchool"));

                    var tenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();


                    if (tenant.PrinterTypeId.HasValue)
                        _reportCode = tenant.PrinterTypes.Reports.ReportCode;

                    if (input.ReceiptTypeId == 2)
                        _reportCode = "25";
                    else if (input.ReceiptTypeId == 3)
                        _reportCode = "26";

                    unit.Complete();
                }

                reportString = new TransactionReportDto
                {
                    Id = transaction.Id,
                    reportString = ""
                };
            }
            catch (Exception err)
            {
                Logger.Error(err.Message);
                throw new UserFriendlyException(err.Message);
            }
            return reportString;
        }

        private void DecreaseInventory(ArticleDto art, int TenId)
        {

            var inventory = _InventoryRepository.
                 GetAllList(x => x.ArticleId == art.Id && x.TenantInventoryId == TenId).FirstOrDefault();
            if (inventory == null)
                throw new UserFriendlyException("TheActualProductIsOutOfStock");

            if (inventory.Existence < art.Quantity.Value)
                throw new UserFriendlyException(L("TheExistenceIsLessThanTheRequiredQuantity"
                    , inventory.Existence, art.Quantity, inventory.Articles.Description));

            inventory.Existence -= art.Quantity.Value;
            CurrentUnitOfWork.SaveChanges();

        }


        private void IncreaseInventory(Models.TransactionArticles art, int TenId)
        {
            var inventory = _InventoryRepository.
                 GetAllList(x => x.ArticleId == art.ArticleId && x.TenantInventoryId == TenId).FirstOrDefault();
            if (inventory == null)
                throw new UserFriendlyException("TheActualProductIsOutOfStock");

            inventory.Existence += art.Quantity;
            // CurrentUnitOfWork.SaveChanges();
        }

        public async Task<DateTime> FreeDateReceiptTenant()
        {
            var ff = DateTime.Now;

            var  Fch = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();

            if (Fch.FreeDateReceipt.HasValue)
                return Fch.FreeDateReceipt.Value;
            else
            {
                return ff; 
                throw new UserFriendlyException("TheActualProductIsOutOfStock");              
            }
        }


        public int getTransactionSequence(int TransactionTypeId)
        {
            int sequence = 1;
            try
            {

                sequence = _transactionRepository.GetAllList(x => x.TransactionTypeId == TransactionTypeId).Max(x => x.Sequence) + 1;
            }
            catch
            {
            }

            return sequence;
        }

        public override async Task<TransactionDto> Update(UpdateTransactionDto input)
        {
            CheckUpdatePermission();
            var transaction = await _transactionRepository.GetAsync(input.Id);

            MapToEntity(input, transaction);
            try
            {
                if (transaction.CancelStatusId.HasValue)
                {
                    if (transaction.CancelStatusId == 1) // Requested
                    {
                        transaction.CancelRequested = DateTime.Now;
                    }
                    else if (transaction.CancelStatusId == 2) // Authorized
                    {
                        transaction.CancelAuthorized = DateTime.Now;
                    }
                }
                if (input.Enrollment > 0)
                {
                    var EnrollmentId = _enrollmentSequenceRepository.GetAllList(x => x.Sequence == input.Enrollment
                       ).FirstOrDefault().EnrollmentId;
                    transaction.EnrollmentId = EnrollmentId;
                }
                await _transactionRepository.UpdateAsync(transaction);
                if (input.Students != null)
                {
                    var transctionStudentsToModify = _transactionStudentRepository.GetAllList(x =>
                              x.TransactionId == input.Id &&
                              x.IsActive == true).ToList();
                    if (transctionStudentsToModify.Count > 0)
                    {
                        foreach (var studentX in transctionStudentsToModify)
                        {
                            studentX.Amount = 0;
                        }
                        CurrentUnitOfWork.SaveChanges();

                        foreach (var item in input.Students)
                        {

                            Models.TransactionStudents ts = _transactionStudentRepository.GetAllList(x =>
                                    x.TransactionId == input.Id &&
                                    x.EnrollmentStudentId == item.EnrollmentStudentId &&
                                    x.IsActive == true).FirstOrDefault();
                            if (ts != null)
                            {
                                if (input.Indicator == 0)
                                {
                                    ts.Amount = item.Amount;
                                }
                                else
                                {
                                    ts.Amount = 0;
                                }
                            }
                            else
                            {
                                if (input.Indicator == 0)
                                {
                                    ts = new Models.TransactionStudents();
                                    ts.Amount = item.Amount;
                                    ts.EnrollmentStudentId = item.EnrollmentStudentId;
                                    ts.TransactionId = transaction.Id;
                                    ts.TransactionTypeId = transaction.TransactionTypeId;
                                    ts.IsActive = true;
                                }
                                else
                                {
                                    ts = new Models.TransactionStudents();
                                    ts.Amount = 0;
                                    ts.EnrollmentStudentId = item.EnrollmentStudentId;
                                    ts.TransactionId = transaction.Id;
                                    ts.TransactionTypeId = transaction.TransactionTypeId;
                                    ts.IsActive = true;

                                }
                                _transactionStudentRepository.Insert(ts);
                            }

                            CurrentUnitOfWork.SaveChanges();
                        }
                    }
                    else
                    {
                        foreach (var item in input.Students)
                        {
                            Models.TransactionStudents ts = new Models.TransactionStudents();
                            ts.Amount = item.Amount;
                            ts.EnrollmentStudentId = item.EnrollmentStudentId;
                            ts.TransactionId = transaction.Id;
                            ts.TransactionTypeId = transaction.TransactionTypeId;
                            ts.IsActive = true;

                            _transactionStudentRepository.Insert(ts);

                        }
                    }
                }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<long> input)
        {
            try
            {
                var transaction = await _transactionRepository.GetAsync(input.Id);
                await _transactionRepository.DeleteAsync(transaction);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.Transactions MapToEntity(CreateTransactionDto createInput)
        {
            var transaction = ObjectMapper.Map<Models.Transactions>(createInput);
            return transaction;
        }
        protected override void MapToEntity(UpdateTransactionDto input, Models.Transactions transaction)
        {
            ObjectMapper.Map(input, transaction);
        }
        protected override IQueryable<Models.Transactions> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.Transactions> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.Transactions> GetEntityByIdAsync(long id)
        {
            var transaction = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(transaction);
        }
        protected override IQueryable<Models.Transactions> ApplySorting(IQueryable<Models.Transactions> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Name);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<TransactionDto>> GetAllTransactions(GdPagedResultRequestDto input)
        {
            PagedResultDto<TransactionDto> ouput = new PagedResultDto<TransactionDto>();
            IQueryable<Models.Transactions> query = query = from x in _transactionRepository.GetAll()
                                                            where x.TransactionTypeId == TRANSACTION_TYPE_TRANSACTION
                                                            select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _transactionRepository.GetAll()
                        where (x.Name.Contains(input.TextFilter) || x.Sequence.ToString() == input.TextFilter) && x.TransactionTypeId == TRANSACTION_TYPE_TRANSACTION
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Transactions.Dto.TransactionDto>>(query.ToList());
            foreach (var item in ouput.Items)
            {
                var enrolmenSeq = item.Enrollments.EnrollmentSequences.FirstOrDefault();
                if (enrolmenSeq != null)
                    item.Enrollment = enrolmenSeq.Sequence;
            }


            return ouput;
        }


        public async Task<PagedResultDto<TransactionDto>> GetAllDeposits(GdPagedResultRequestDto input)
        {
            PagedResultDto<TransactionDto> ouput = new PagedResultDto<TransactionDto>();
            IQueryable<Models.Transactions> query = query = from x in _transactionRepository.GetAll()
                                                            where x.TransactionTypeId == TRANSACTION_TYPE_DEPOSIT
                                                            select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _transactionRepository.GetAll()
                        where (x.BankAccounts.Number.ToString().Contains((input.TextFilter)) || x.Number.ToString().Contains((input.TextFilter)) || x.Sequence.ToString() == input.TextFilter) && x.TransactionTypeId == TRANSACTION_TYPE_DEPOSIT
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Transactions.Dto.TransactionDto>>(query.ToList());
            return ouput;
        }



        public async Task<PagedResultDto<TransactionDto>> GetAllReceipts(GdPagedResultRequestDto input)
        {
            PagedResultDto<TransactionDto> ouput = new PagedResultDto<TransactionDto>();
            IQueryable<Models.Transactions> query = query = from x in _transactionRepository.GetAll()
                                                            where x.TransactionTypeId == TRANSACTION_TYPE_RECEIPT
                                                            select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _transactionRepository.GetAll()
                        where (x.Name.Contains(input.TextFilter) || x.Sequence.ToString() == input.TextFilter) && x.TransactionTypeId == TRANSACTION_TYPE_RECEIPT
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Transactions.Dto.TransactionDto>>(query.ToList());
            if (ouput.Items != null || ouput.Items.Count > 0)
            {
                foreach (var item in ouput.Items)
                {
                    if (item.TransactionConcepts != null && item.TransactionConcepts.Any())
                    {
                        item.Concepts_Description = item.TransactionConcepts.FirstOrDefault().Concepts.Description + " ...";
                    }
                    if (item.Enrollments != null)
                    {
                        var enrolmenSeq = item.Enrollments.EnrollmentSequences.FirstOrDefault();
                        if (enrolmenSeq != null)
                            item.Enrollment = enrolmenSeq.Sequence;
                    }
                }
            }
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<TransactionDto>> GetAllTransactionsForCombo()
        {
            var transactionList = await _transactionRepository.GetAllListAsync(x => x.IsActive == true);

            var transaction = ObjectMapper.Map<List<TransactionDto>>(transactionList.ToList());

            return transaction;
        }


        public async Task<GetEnrollmentBalanceOutput> GetEnrollmentBalance(GetEnrollmentBalanceInput input)
        {
            GetEnrollmentBalanceOutput output = new GetEnrollmentBalanceOutput();

            try
            {

                List<AccountBalanceDto> query = new List<AccountBalanceDto>();
                List<AccountBalanceDto> query2 = new List<AccountBalanceDto>();

                if (input.Sequence.HasValue)
                {

                    query = (from x in _transactionRepository.GetAll()
                             join x2 in _enrollmentRepository.GetAll()
                             on x.EnrollmentId equals x2.Id
                             join x3 in _enrollmentSequenceRepository.GetAll()
                             on x2.Id equals x3.EnrollmentId
                             join x4 in _transactionTypesRepository.GetAll()
                             on x.TransactionTypeId equals x4.Id
                             where x3.Sequence == input.Sequence && x.ReceiptTypeId != 2 && x.ReceiptTypeId != 3 &&
                             (new int[] { TRANSACTION_TYPE_RECEIPT, TRANSACTION_TYPE_TRANSACTION }).Contains(x.TransactionTypeId) &&
                             x.ConceptId != null
                             select new { tran = x, enroll = x2, seq = x3, tranT = x4 }).ToList()
                               .Select(n => new AccountBalanceDto(
                                    TransactionNumber: n.tranT.Abbreviation + "-" + n.tran.Sequence.ToString(),
                                    EnrollmentId: n.enroll.Id,
                                    FirstName: n.enroll.FirstName,
                                    LastName: n.enroll.LastName,
                                    Phone1: n.enroll.Phone1,
                                    Phone2: n.enroll.Phone2,
                                    EmailAddress: n.enroll.EmailAddress,
                                    Date: n.tran.Date,
                                    Document: n.tran.Number,
                                        ConceptId: n.tran.Concepts == null ? 0 : n.tran.Concepts.Id,
                                        Concept: n.tran.Concepts == null ? " " : n.tran.Concepts.Description,
                                    Debit: n.tran.OriginId == 1 ? n.tran.Amount : 0,
                                    Credit: n.tran.OriginId == 2 ? n.tran.Amount : 0
                                    )).ToList();

                    query2 = (from x in _transactionRepository.GetAll()
                              join x2 in _enrollmentRepository.GetAll()
                              on x.EnrollmentId equals x2.Id
                              join x3 in _enrollmentSequenceRepository.GetAll()
                              on x2.Id equals x3.EnrollmentId
                              join x4 in _transactionTypesRepository.GetAll()
                              on x.TransactionTypeId equals x4.Id
                              join x5 in _transactionConceptRepository.GetAll()
                              on x.Id equals x5.TransactionId
                              where x3.Sequence == input.Sequence && x.ReceiptTypeId != 2 && x.ReceiptTypeId != 3 &&
                              (new int[] { TRANSACTION_TYPE_RECEIPT, TRANSACTION_TYPE_TRANSACTION }).Contains(x.TransactionTypeId)
                              select new { tran = x, enroll = x2, seq = x3, tranT = x4, tranConcep = x5 }).ToList()
                                    .Select(n => new AccountBalanceDto(
                                    TransactionNumber: n.tranT.Abbreviation + "-" + n.tran.Sequence.ToString(),
                                    EnrollmentId: n.enroll.Id,
                                    FirstName: n.enroll.FirstName,
                                    LastName: n.enroll.LastName,
                                    Phone1: n.enroll.Phone1,
                                    Phone2: n.enroll.Phone2,
                                    EmailAddress: n.enroll.EmailAddress,
                                    Date: n.tran.Date,
                                    Document: n.tran.Number,
                                    ConceptId:  n.tranConcep.ConceptId,
                                    Concept: n.tranConcep.Concepts.Description ,
                                    Debit: n.tran.OriginId == 1 ? n.tran.Amount : 0,
                                    Credit: n.tran.OriginId == 2 ? n.tran.Amount : 0
                                    )).ToList();

                    foreach (var item in query2)
                    {
                        query.Add(item);
                    }

                }

                output.Transactions = await Task.Run(() => query.ToList());
                Decimal Balance = 0;

                for (int i = 0; i < output.Transactions.Count; i++)
                {
                    var tran = output.Transactions[i];
                    Balance += tran.Debit - tran.Credit;
                    tran.Balance = Balance;
                    tran.AbsBalance = Math.Abs(Balance);
                }

                output.EnrollmentName = query.FirstOrDefault()?.FullName;
                output.TotalDebit = query.Sum(x => x.Debit);
                output.TotalCredit = query.Sum(x => x.Credit);
                output.TotalBalance = query.Sum(x => x.Debit - x.Credit);
                output.AbsTotalBalance = Math.Abs(output.TotalBalance);
            }
            catch (Exception err)
            {
                var mensaje = err.Message;
                var mensajeInterno = "";
                if (err.InnerException != null)
                    mensajeInterno = err.InnerException.Message;

            }


            return output;
        }

        public GetStudentBalanceOutput GetStudentBalance(GetStudentBalanceInput input)
        {
            GetStudentBalanceOutput output = new GetStudentBalanceOutput();
            List<StudentBalanceDto> query = new List<StudentBalanceDto>();
            if (input.EnrollmentStudentId.HasValue)
            {
                //var student = _enrollmentStudentRepository.GetAll().Where(x => x.Id == input.EnrollmentStudentId.Value).FirstOrDefault();
                //if (student == null)
                //    return output;

                query = (from x in _transactionRepository.GetAll()
                         join x4 in _transactionTypesRepository.GetAll()
                         on x.TransactionTypeId equals x4.Id
                         join x6 in _transactionStudentRepository.GetAll()
                         on x.Id equals x6.TransactionId 
                         where x6.EnrollmentStudents.Id == input.EnrollmentStudentId && x.ReceiptTypeId != 2 && x.ReceiptTypeId != 3 &&
                         (new int[] { TRANSACTION_TYPE_RECEIPT, TRANSACTION_TYPE_TRANSACTION }).Contains(x.TransactionTypeId)
                         select new { StudentBalance = x6, tran = x }).ToList()
                           .Select(n => new StudentBalanceDto(
                                TransactionNumber: n.tran.Sequence,
                                ConceptId: n.tran.Concepts.Id,
                                Concept: n.tran.Concepts.Description,
                                Debit: n.tran.OriginId == 1 ? n.StudentBalance.Amount : 0,
                                Credit: n.tran.OriginId == 2 ? n.StudentBalance.Amount : 0
                                )).ToList();
            }

            output.StudentTransactions =  query.ToList();
            Decimal Balance = 0;

/*            for (int i = 0; i < output.Transactions.Count; i++)
            //{
                var tran = output.Transactions[i];
                Balance += tran.Debit - tran.Credit;
                tran.Balance = Balance;
                tran.AbsBalance = Math.Abs(Balance);
            //}
*/
            output.TotalDebit = query.Sum(x => x.Debit);
            output.TotalCredit = query.Sum(x => x.Credit);
            output.TotalBalance = query.Sum(x => x.Debit - x.Credit);
            output.AbsTotalBalance = Math.Abs(output.TotalBalance);

            return output;
        }


        public void AnulateInventoryReceipt(AnulateInventoryReceiptInput input)
        {
            var transa = _transactionRepository.Get(input.TransactionId);
            if (transa == null)
            {
                throw new UserFriendlyException(L("TheReceiptYouWantToAnulateDoesNotExist"));
            }

            if (transa.TransactionTypeId != EasySchoolManagerConsts.TransactionType.Receipts)
            {
                throw new UserFriendlyException(L("TheTransactionYouWantToAnnulateIsNotAReceipt"));
            }
            if (transa.ReceiptTypeId != EasySchoolManagerConsts.ReceiptType.Articles)
            {
                throw new UserFriendlyException(L("TheReceiptYouWantToAnnulateIsNotForArticles"));
            }

            if (transa.StatusId == EasySchoolManagerConsts.Status.Anulado)
            {
                throw new UserFriendlyException(L("ThisReceiptIsAlreadyAnulated"));
            }


            transa.StatusId = EasySchoolManagerConsts.Status.Anulado;
            transa.Amount = 0;

            var artivles = _transactionArticles.GetAllList(x => x.TransactionId == transa.Id).ToList();

            foreach (var art in artivles)
            {
                IncreaseInventory(art, AbpSession.TenantId.Value);
            }

            //  CurrentUnitOfWork.SaveChanges();

        }

        public async Task<GetAdditionalValuesOuput> GetAdditionalValues()
        {
            GetAdditionalValuesOuput output = new GetAdditionalValuesOuput();

            if (AbpSession.TenantId == null)
                throw new Exception(L("ThisReceiveCanBeJustUsedWithAUserThetBelongToASchool"));

            var tenant = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();

            output.ReceipReportCodeForTenant = await Task.Run(() => { return ""; });  //This is the code by default of the tenant

            if (tenant.PrinterTypeId.HasValue)
                output.ReceipReportCodeForTenant = tenant.PrinterTypes.Reports.ReportCode;
            return output;
        }
    }
};
//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using System;
using EasySchoolManager.Transactions.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Transactions
{
      public interface ITransactionAppService : IAsyncCrudAppService<TransactionDto, long, PagedResultRequestDto, CreateTransactionDto, UpdateTransactionDto>
    {
        Task<PagedResultDto<TransactionDto>> GetAllTransactions(GdPagedResultRequestDto input);
        Task<PagedResultDto<TransactionDto>> GetAllReceipts(GdPagedResultRequestDto input);
        Task<PagedResultDto<TransactionDto>> GetAllDeposits(GdPagedResultRequestDto input);
        Task<List<Dto.TransactionDto>> GetAllTransactionsForCombo();
        Task<GetEnrollmentBalanceOutput> GetEnrollmentBalance(GetEnrollmentBalanceInput input);
        Task<TransactionReportDto> CreateAndPrint(CreateTransactionDto input);
        Task ApplyExpiredPaymentProjections();
        void AnulateInventoryReceipt(AnulateInventoryReceiptInput input);
        Task<GetAdditionalValuesOuput> GetAdditionalValues();
        Task<DateTime> FreeDateReceiptTenant();
        GetStudentBalanceOutput GetStudentBalance(GetStudentBalanceInput input);

      }
}
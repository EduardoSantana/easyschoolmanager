﻿//Created from Templaste MG

namespace EasySchoolManager.Transactions
{
    public class GetStudentBalanceInput
    {
        public int? EnrollmentStudentId { get; set; }
    }
}
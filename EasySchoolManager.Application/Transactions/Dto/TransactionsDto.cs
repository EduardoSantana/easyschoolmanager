//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.Transactions.Dto
{
    [AutoMap(typeof(Models.Transactions))]
    public class TransactionDto : EntityDto<long>
    {

        public int? TaxReceiptId { get; set; }
        public int Sequence { get; set; }
        public int? BankId { get; set; }
        public int? BankAccountId { get; set; }
        [StringLength(20)]
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public string Request { get; set; }
        public string CancelAuthorization { get; set; }
        public Decimal Amount { get; set; }
        public long? EnrollmentId { get; set; }
        public int ConceptId { get; set; }
        public int? PaymentMethodId { get; set; }
        public string CheckNumber { get; set; }
        public int OriginId { get; set; }
        public int TransactionTypeId { get; set; }
        public int? GrantEnterpriseId { get; set; }
        public int StatusId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string Concepts_Description { get; set; }
        public string Statuses_Name { get; set; }
        public string Banks_Name { get; set; }
        public Enrollments.Dto.EnrollmentDto Enrollments { get;set; }
        public int Enrollment { get; set; }
        public string BankAccounts_Number { get; set; }
        public string TaxReceiptSequence { get; set; }
        public string Comments { get; set; }
        public int? ReceiptTypeId { get; set; }

        [MaxLength(15)]
        public string RncNumber { get; set; }

        [MaxLength(150)]
        public string RncName { get; set; }

        public Enrollments.Dto.EnrollmentDto CurrentEnrollment { get; set; }
        public List<Students.Dto.StudentsForReceiptsDto> Students { get; set; }
        public List<TransactionConcepts.Dto.TransactionConceptsDto> TransactionConcepts { get; set; }
        public List<TransactionArticles.Dto.TransactionArticleDto> TransactionArticles { get; set; }

    }
}
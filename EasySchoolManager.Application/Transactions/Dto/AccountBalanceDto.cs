﻿//Created from Templaste MG

using System;

namespace EasySchoolManager.Transactions
{
    public class AccountBalanceDto
    {
        public long EnrollmentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public DateTime Date { get; set; }
        public string Document { get; set; }
        public int ConceptId { get; set; }
        public string Concept { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string EmailAddress { get; set; }
        public decimal Balance { get; set; }
        public string TransactionNumber { get; set; }
        public decimal AbsBalance { get; set; }

        public AccountBalanceDto(long EnrollmentId, string FirstName, string LastName, DateTime Date, string Document,
            int ConceptId, string Concept, decimal Debit, decimal Credit, string TransactionNumber, string Phone1,string Phone2,string EmailAddress)
        {
            this.EnrollmentId = EnrollmentId;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.FullName = this.FirstName + " " + this.LastName;
            this.Date = Date;
            this.Document = Document;
            this.ConceptId = ConceptId;
            this.Concept = Concept;
            this.Debit = Debit;
            this.Credit = Credit;
            this.TransactionNumber = TransactionNumber;
            this.Phone1 = Phone1;
            this.Phone2 = Phone2;
            this.EmailAddress = EmailAddress;
           
        }
    }
}
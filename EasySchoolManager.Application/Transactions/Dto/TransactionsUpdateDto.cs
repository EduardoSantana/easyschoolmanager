//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.Transactions.Dto
{
    [AutoMap(typeof(Models.Transactions))]
    public class UpdateTransactionDto : EntityDto<long>
    {

        public UpdateTransactionDto()
        {
            Students = new List<EasySchoolManager.Students.Dto.StudentsForReceiptsDto>();
            TransactionArticles = new List<Articles.Dto.ArticleDto>();
        }

        public int Sequence { get; set; }

        public int? TaxReceiptId { get; set; }

        public int? Enrollment { get; set; }

        public int? BankId { get; set; }

        public int? BankAccountId { get; set; }

        public int? CancelStatusId { get; set; }

        [StringLength(20)]
        public string Number { get; set; }

        public DateTime Date { get; set; }

        public Decimal Amount { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Comment { get; set; }

        [StringLength(100)]
        public string Request { get; set; }

        [StringLength(100)]
        public string CancelAuthorization { get; set; }

        public long? EnrollmentId { get; set; }

        public int ConceptId { get; set; }

        public int? PaymentMethodId { get; set; }

        public int? GrantEnterpriseId { get; set; }

        [StringLength(15)]
        public string CheckNumber { get; set; }

        public int OriginId { get; set; }

        public int TransactionTypeId { get; set; }

        public int Indicator { get; set; }
        public string Comments { get; set; }
        [MaxLength(15)]
        public string RncNumber { get; set; }

        [MaxLength(150)]
        public string RncName { get; set; }

        [MaxLength(11)]
        public string TaxReceiptSequence { get; set; }

        public int StatusId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public Enrollments.Dto.EnrollmentDto CurrentEnrollment { get; set; }
        public List<Students.Dto.StudentsForReceiptsDto> Students { get; set; }
        public List<Articles.Dto.ArticleDto> TransactionArticles { get; set; }

        public int? ReceiptTypeId { get; set; }
        public List<Articles.Dto.ArticleDto> Articles { get; set; }


    }
}
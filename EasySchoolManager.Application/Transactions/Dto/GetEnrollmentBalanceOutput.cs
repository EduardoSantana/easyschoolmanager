﻿//Created from Templaste MG

using System.Collections.Generic;

namespace EasySchoolManager.Transactions
{
    public class GetEnrollmentBalanceOutput
    {
        public List<AccountBalanceDto> Transactions { get; set; }
        public string EnrollmentName { get;  set; }
        public decimal TotalDebit { get;  set; }
        public decimal TotalCredit { get; set; }
        public decimal TotalBalance { get;  set; }
        public decimal AbsTotalBalance { get;  set; }
    }
}
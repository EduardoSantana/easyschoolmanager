﻿//Created from Templaste MG

namespace EasySchoolManager.Transactions
{
    public class GetEnrollmentBalanceInput
    {
        public int? Sequence { get; set; }
    }
}
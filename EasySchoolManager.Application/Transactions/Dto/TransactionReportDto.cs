﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Transactions.Dto
{
    public class TransactionReportDto
    {
        public long Id { get; set; }
        public string reportString { get; set; }
    }
}

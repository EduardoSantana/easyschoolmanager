﻿//Created from Templaste MG

namespace EasySchoolManager.Transactions
{
    public class StudentBalanceDto
    {
        public int TransactionNumber;
        public int ConceptId;
        public string Concept;
        public decimal Debit;
        public decimal Credit;

        public StudentBalanceDto(int TransactionNumber, int ConceptId, string Concept, decimal Debit, decimal Credit)
        {
            this.TransactionNumber = TransactionNumber;
            this.ConceptId = ConceptId;
            this.Concept = Concept;
            this.Debit = Debit;
            this.Credit = Credit;
        }
    }
}
﻿//Created from Templaste MG

using System.Collections.Generic;

namespace EasySchoolManager.Transactions
{
    public class GetStudentBalanceOutput
    {
        public List<StudentBalanceDto> StudentTransactions { get; internal set; }
        public string EnrollmentName { get; internal set; }
        public decimal TotalDebit { get; internal set; }
        public decimal TotalCredit { get; internal set; }
        public decimal TotalBalance { get; internal set; }
        public decimal AbsTotalBalance { get; internal set; }
    }
}
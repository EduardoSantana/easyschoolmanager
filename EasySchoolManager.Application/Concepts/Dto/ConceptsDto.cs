//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.Concepts.Dto
{
    [AutoMap(typeof(Models.Concepts))]
    public class ConceptDto : EntityDto<int>
    {
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public int CatalogId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string Catalogs_Name { get; set; }
        public int? TransactionTypeId { get; set; }
        public string TransactionTypes_Name { get; set; }
        public int? T1DimensionId { get; set; }
        public int? T2DimensionId { get; set; }
        public int? T3DimensionId { get; set; }
        public int? T4DimensionId { get; set; }
        public int? T5DimensionId { get; set; }
        public int? T6DimensionId { get; set; }
        public int? T7DimensionId { get; set; }
        public int? T8DimensionId { get; set; }
        public int? T9DimensionId { get; set; }
        public int? T10DimensionId { get; set; }
        public decimal? Amount { get; set; }
        public int? sequenceNext { get; set; }

        public bool AvailableForTransaction { get; set; }

        public bool AvailableForReceipts { get; set; }

        public bool AvailableForDeposits { get; set; }

        public bool AvailableForDiscounts { get; set; }


        public int? ReceiptTypeId { get; set; }
        public int? PeriodId { get; set; }
        public string Periods_Period { get; set; }

        public bool IsEns { get; set; }
        public bool IsMat { get; set; }
    }
}
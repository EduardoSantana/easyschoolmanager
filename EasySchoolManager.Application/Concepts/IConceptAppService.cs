//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Concepts.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Concepts
{
      public interface IConceptAppService : IAsyncCrudAppService<ConceptDto, int, PagedResultRequestDto, CreateConceptDto, UpdateConceptDto>
      {
            Task<PagedResultDto<ConceptDto>> GetAllConcepts(GdPagedResultRequestDto input);
			Task<List<Dto.ConceptDto>> GetAllConceptsForCombo();
			Task<List<Dto.ConceptDto>> GetAllConceptsForTransactionsCombo();
			Task<List<Dto.ConceptDto>> GetAllConceptsForReceiptsCombo();
			Task<List<Dto.ConceptDto>> GetAllConceptsForReceipts1Combo();
            Task<List<Dto.ConceptDto>> GetAllConceptsForReceipts2Combo();
            Task<List<Dto.ConceptDto>> GetAllConceptsForReceipts3Combo();
            Task<List<Dto.ConceptDto>> GetAllConceptsForDepositsCombo();
			Task<List<Dto.ConceptDto>> GetAllConceptsForDiscountsCombo();
			Task<List<Dto.ConceptDto>> GetAllConceptsForDiscountsByPeriodCombo(int PeriodId);
      }
}
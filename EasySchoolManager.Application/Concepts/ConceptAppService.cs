//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Concepts.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Concepts
{
    [AbpAuthorize(PermissionNames.Pages_Concepts)]
    public class ConceptAppService : AsyncCrudAppService<Models.Concepts, ConceptDto, int, PagedResultRequestDto, CreateConceptDto, UpdateConceptDto>, IConceptAppService
    {
        private readonly IRepository<Models.Concepts, int> _conceptRepository;

        private readonly IRepository<Models.Catalogs, int> _catalogRepository;


        public ConceptAppService(
            IRepository<Models.Concepts, int> repository,
            IRepository<Models.Concepts, int> conceptRepository,
            IRepository<Models.Catalogs, int> catalogRepository

            )
            : base(repository)
        {
            _conceptRepository = conceptRepository;

            _catalogRepository = catalogRepository;



        }
        public override async Task<ConceptDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<ConceptDto> Create(CreateConceptDto input)
        {
            CheckCreatePermission();
            var concept = ObjectMapper.Map<Models.Concepts>(input);
            try
            {
                await _conceptRepository.InsertAsync(concept);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(concept);
        }
        public override async Task<ConceptDto> Update(UpdateConceptDto input)
        {
            CheckUpdatePermission();
            var concept = await _conceptRepository.GetAsync(input.Id);
            MapToEntity(input, concept);
            try
            {
                await _conceptRepository.UpdateAsync(concept);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var concept = await _conceptRepository.GetAsync(input.Id);
                await _conceptRepository.DeleteAsync(concept);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.Concepts MapToEntity(CreateConceptDto createInput)
        {
            var concept = ObjectMapper.Map<Models.Concepts>(createInput);
            return concept;
        }
        protected override void MapToEntity(UpdateConceptDto input, Models.Concepts concept)
        {
            ObjectMapper.Map(input, concept);
        }
        protected override IQueryable<Models.Concepts> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.Concepts> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.Concepts> GetEntityByIdAsync(int id)
        {
            var concept = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(concept);
        }
        protected override IQueryable<Models.Concepts> ApplySorting(IQueryable<Models.Concepts> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Description);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<ConceptDto>> GetAllConcepts(GdPagedResultRequestDto input)
        {
            PagedResultDto<ConceptDto> ouput = new PagedResultDto<ConceptDto>();
            IQueryable<Models.Concepts> query = query = from x in _conceptRepository.GetAll()
                                                        select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _conceptRepository.GetAll()
                        where x.Description.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Concepts.Dto.ConceptDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<ConceptDto>> GetAllConceptsForCombo()
        {
            var conceptList = await _conceptRepository.GetAllListAsync(x => x.IsActive == true);

            var concept = ObjectMapper.Map<List<ConceptDto>>(conceptList.ToList());

            return concept;
        }

        [AbpAllowAnonymous]
        public async Task<List<ConceptDto>> GetAllConceptsForTransactionsCombo()
        {
            var conceptList = await _conceptRepository.GetAllListAsync(x => x.IsActive == true && x.AvailableForTransaction == true);

            var concept = ObjectMapper.Map<List<ConceptDto>>(conceptList.ToList());

            return concept;
        }

        [AbpAllowAnonymous]
        public async Task<List<ConceptDto>> GetAllConceptsForDepositsCombo()
        {
            var conceptList = await _conceptRepository.GetAllListAsync(x => x.IsActive == true && x.AvailableForDeposits == true);

            var concept = ObjectMapper.Map<List<ConceptDto>>(conceptList.ToList());

            return concept;
        }
        [AbpAllowAnonymous]
        public async Task<List<ConceptDto>> GetAllConceptsForDiscountsCombo()
        {
            var conceptList = await _conceptRepository.GetAllListAsync(x => x.IsActive == true && x.AvailableForDiscounts == true);

            var concept = ObjectMapper.Map<List<ConceptDto>>(conceptList.ToList());

            return concept;
        }

        [AbpAllowAnonymous]
        public async Task<List<ConceptDto>> GetAllConceptsForDiscountsByPeriodCombo(int PeriodId)
        {
            var conceptList = await _conceptRepository.GetAllListAsync(x => x.IsActive == true && x.AvailableForDiscounts == true
            && x.PeriodId == PeriodId);

            var concept = ObjectMapper.Map<List<ConceptDto>>(conceptList.ToList());

            return concept;
        }
        [AbpAllowAnonymous]
        public async Task<List<ConceptDto>> GetAllConceptsForReceipts1Combo()
        {
            var conceptList = await _conceptRepository.GetAllListAsync(x => x.IsActive == true && x.AvailableForReceipts == true && x.ReceiptTypeId == 1);

            var concept = ObjectMapper.Map<List<ConceptDto>>(conceptList.ToList());

            return concept;
        }
        [AbpAllowAnonymous]
        public async Task<List<ConceptDto>> GetAllConceptsForReceiptsCombo()
        {
            var conceptList = await _conceptRepository.GetAllListAsync(x => x.IsActive == true && x.AvailableForReceipts == true);

            var concept = ObjectMapper.Map<List<ConceptDto>>(conceptList.ToList());

            return concept;
        }
        [AbpAllowAnonymous]
        public async Task<List<ConceptDto>> GetAllConceptsForReceipts2Combo()
        {
            var conceptList = await _conceptRepository.GetAllListAsync(x => x.IsActive == true && x.AvailableForReceipts == true && x.ReceiptTypeId == 2);

            var concept = ObjectMapper.Map<List<ConceptDto>>(conceptList.ToList());

            return concept;
        }

        [AbpAllowAnonymous]
        public async Task<List<ConceptDto>> GetAllConceptsForReceipts3Combo()
        {
            var conceptList = await _conceptRepository.GetAllListAsync(x => x.IsActive == true && x.AvailableForReceipts == true && x.ReceiptTypeId == 3);

            var concept = ObjectMapper.Map<List<ConceptDto>>(conceptList.ToList());

            return concept;
        }

    }
};
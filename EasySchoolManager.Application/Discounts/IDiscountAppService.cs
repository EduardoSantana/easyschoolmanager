//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Discounts.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Discounts
{
      public interface IDiscountAppService : IAsyncCrudAppService<DiscountDto, int, PagedResultRequestDto, CreateDiscountDto, UpdateDiscountDto>
      {
            Task<PagedResultDto<DiscountDto>> GetAllDiscounts(GdPagedResultRequestDto input);
			Task<List<Dto.DiscountDto>> GetAllDiscountsForCombo();
            Task<List<Dto.DiscountDto>> GetAllDiscountPeriodForCombo();
    }
}
//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Discounts.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Discounts
{
    [AbpAuthorize(PermissionNames.Pages_Discounts)]
    public class DiscountAppService : AsyncCrudAppService<Models.Discounts, DiscountDto, int, PagedResultRequestDto, CreateDiscountDto, UpdateDiscountDto>, IDiscountAppService
    {
        private readonly IRepository<Models.Discounts, int> _discountRepository;

        private readonly IRepository<Models.Students, int> _studentRepository;
        private readonly IRepository<Models.Periods, int> _periodRepository;
        private readonly MultiTenancy.TenantManager _tenantManager;

        public DiscountAppService(
            IRepository<Models.Discounts, int> repository,
            IRepository<Models.Discounts, int> discountRepository,
            IRepository<Models.Students, int> studentRepository,
            IRepository<Models.Periods, int> periodRepository,
            MultiTenancy.TenantManager tenantManager

            )
            : base(repository)
        {
            _discountRepository = discountRepository;

            _studentRepository = studentRepository;

            _periodRepository = periodRepository;

            _tenantManager = tenantManager;

            LocalizationSourceName = EasySchoolManagerConsts.LocalizationSourceName;

        }
        public override async Task<DiscountDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            user.StudentName = user.Students_FullName;
            var enrollmentX = _discountRepository.GetAll().Where(x => x.Id == user.Id).FirstOrDefault().Students.EnrollmentStudents.FirstOrDefault();
            var discount = _discountRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();

            if (enrollmentX != null)
            {
                user.TutorName = enrollmentX.Enrollments.FirstName + " " + enrollmentX.Enrollments.LastName;
                var enrollment = enrollmentX.Enrollments.EnrollmentSequences.FirstOrDefault();
                if(enrollment != null)
                user.EnrollmentSequence = enrollment.Sequence;
                if (discount != null)
                    user.DiscountNameId = discount.PeriodDiscountDetails.PeriodDiscounts.DiscountNameId; 
            }
            return user;
        }

        public override async Task<DiscountDto> Create(CreateDiscountDto input)
        {
            CheckCreatePermission();
            var discount = ObjectMapper.Map<Models.Discounts>(input);


            try
            {
                if (_discountRepository.GetAll().Any(x => x.PeriodId == input.PeriodId && x.StudentId == input.StudentId && x.ConceptId == input.ConceptId))
                    throw new Exception(L("TheIsADiscountRegardingToThisStudentInTheSelectedPeriodAlready"));

                await _discountRepository.InsertAsync(discount);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(discount);
        }
        public override async Task<DiscountDto> Update(UpdateDiscountDto input)
        {
            CheckUpdatePermission();
            var discount = await _discountRepository.GetAsync(input.Id);
            MapToEntity(input, discount);
            try
            {
                await _discountRepository.UpdateAsync(discount);
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception err)
            {
                if (err.InnerException != null)
                    throw new UserFriendlyException(err.InnerException.Message);
                else
                    throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var discount = await _discountRepository.GetAsync(input.Id);
                await _discountRepository.DeleteAsync(discount);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.Discounts MapToEntity(CreateDiscountDto createInput)
        {
            var discount = ObjectMapper.Map<Models.Discounts>(createInput);
            return discount;
        }
        protected override void MapToEntity(UpdateDiscountDto input, Models.Discounts discount)
        {
            ObjectMapper.Map(input, discount);
        }
        protected override IQueryable<Models.Discounts> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.Discounts> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Students.FullName.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.Discounts> GetEntityByIdAsync(int id)
        {
            var discount = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(discount);
        }
        protected override IQueryable<Models.Discounts> ApplySorting(IQueryable<Models.Discounts> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Students.FullName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<DiscountDto>> GetAllDiscounts(GdPagedResultRequestDto input)
        {
            PagedResultDto<DiscountDto> ouput = new PagedResultDto<DiscountDto>();
            IQueryable<Models.Discounts> query = query = from x in _discountRepository.GetAll()
                                                         select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _discountRepository.GetAll()
                        where x.Students.FullName.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Discounts.Dto.DiscountDto>>(query.ToList());
            foreach (var item in ouput.Items)
            {
                try
                {
                    var student = query.ToList().Where(x => x.Id == item.Id).FirstOrDefault();
                    var tutor = student.Students.EnrollmentStudents.FirstOrDefault().Enrollments;
                    item.StudentName = student.Students.FullName;
                    if (tutor != null)
                    {
                        item.TutorName = tutor.FirstName + " " + tutor.LastName;
                        var sequence = tutor.EnrollmentSequences.FirstOrDefault();
                        if (sequence != null)
                            item.EnrollmentSequence = sequence.Sequence;
                    }
                    item.PercentString = string.Format("{0:00}", item.PeriodDiscountDetails.DiscountPercent) + "%";
                }
                catch (Exception err)
                { }
            }




            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<DiscountDto>> GetAllDiscountsForCombo()
        {
            var discountList = await _discountRepository.GetAllListAsync(x => x.IsActive == true);

            var discount = ObjectMapper.Map<List<DiscountDto>>(discountList.ToList());

            return discount;
        }


        [AbpAllowAnonymous]
        public async Task<List<DiscountDto>> GetAllDiscountPeriodForCombo()
        {

            var ten = _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();

            var discountList = await _discountRepository.GetAllListAsync(x => x.IsActive == true && (x.Id == ten.NextPeriodId || x.Id == ten.PeriodId));

            var discount = ObjectMapper.Map<List<DiscountDto>>(discountList.ToList());

            return discount;
        }

    }
};
//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.PeriodDiscountDetails.Dto;

namespace EasySchoolManager.Discounts.Dto
{
    [AutoMap(typeof(Models.Discounts))]
    public class DiscountDto : EntityDto<int>
    {
        public int StudentId { get; set; }
        public int PeriodDiscountDetailId { get; set; }
        public int EnrollmentSequence { get; set; }
        public string StudentName { get; set; }
        public string Concepts_Description { get; set; }
        public int ConceptId { get; set; }
        public string Students_FullName { get; set; }
        public string TutorName { get; set; }
        public int PeriodId { get; set; }
        public string Periods_Period { get; set; }
        public Decimal Percent { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public String PercentString { get; set; }
        public int DiscountNameId { get; set; }
        public string PeriodDiscountDetails_PeriodDiscounts_DiscountNames_Name { get; set; }

        public virtual PeriodDiscountDetailDto PeriodDiscountDetails { get; set; }
    }
}
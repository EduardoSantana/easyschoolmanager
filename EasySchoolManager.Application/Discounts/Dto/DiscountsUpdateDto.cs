using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace EasySchoolManager.Discounts.Dto
{
    [AutoMap(typeof(Models.Discounts))]
    public class UpdateDiscountDto : EntityDto<int>
    {
        public int StudentId { get; set; }
        public int PeriodId { get; set; }
        public Decimal Percent { get; set; }
        public int ConceptId { get; set; }

        public int PeriodDiscountDetailId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
    }
}
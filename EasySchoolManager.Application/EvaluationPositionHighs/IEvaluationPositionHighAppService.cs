//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.EvaluationPositionHighs.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.EvaluationPositionHighs
{
      public interface IEvaluationPositionHighAppService : IAsyncCrudAppService<EvaluationPositionHighDto, int, PagedResultRequestDto, CreateEvaluationPositionHighDto, UpdateEvaluationPositionHighDto>
      {
            Task<PagedResultDto<EvaluationPositionHighDto>> GetAllEvaluationPositionHighs(GdPagedResultRequestDto input);
			Task<List<Dto.EvaluationPositionHighDto>> GetAllEvaluationPositionHighsForCombo();
      }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.EvaluationPositionHighs.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.EvaluationPositionHighs 
{ 
    [AbpAuthorize(PermissionNames.Pages_EvaluationPositionHighs)] 
    public class EvaluationPositionHighAppService : AsyncCrudAppService<Models.EvaluationPositionHighs, EvaluationPositionHighDto, int, PagedResultRequestDto, CreateEvaluationPositionHighDto, UpdateEvaluationPositionHighDto>, IEvaluationPositionHighAppService 
    { 
        private readonly IRepository<Models.EvaluationPositionHighs, int> _evaluationPositionHighRepository;
		


        public EvaluationPositionHighAppService( 
            IRepository<Models.EvaluationPositionHighs, int> repository, 
            IRepository<Models.EvaluationPositionHighs, int> evaluationPositionHighRepository 
            ) 
            : base(repository) 
        { 
            _evaluationPositionHighRepository = evaluationPositionHighRepository; 
			

			
        } 
        public override async Task<EvaluationPositionHighDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<EvaluationPositionHighDto> Create(CreateEvaluationPositionHighDto input) 
        { 
            CheckCreatePermission(); 
            var evaluationPositionHigh = ObjectMapper.Map<Models.EvaluationPositionHighs>(input); 
			try{
              await _evaluationPositionHighRepository.InsertAsync(evaluationPositionHigh); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(evaluationPositionHigh); 
        } 
        public override async Task<EvaluationPositionHighDto> Update(UpdateEvaluationPositionHighDto input) 
        { 
            CheckUpdatePermission(); 
            var evaluationPositionHigh = await _evaluationPositionHighRepository.GetAsync(input.Id);
            MapToEntity(input, evaluationPositionHigh); 
		    try{
               await _evaluationPositionHighRepository.UpdateAsync(evaluationPositionHigh); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var evaluationPositionHigh = await _evaluationPositionHighRepository.GetAsync(input.Id); 
               await _evaluationPositionHighRepository.DeleteAsync(evaluationPositionHigh);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.EvaluationPositionHighs MapToEntity(CreateEvaluationPositionHighDto createInput) 
        { 
            var evaluationPositionHigh = ObjectMapper.Map<Models.EvaluationPositionHighs>(createInput); 
            return evaluationPositionHigh; 
        } 
        protected override void MapToEntity(UpdateEvaluationPositionHighDto input, Models.EvaluationPositionHighs evaluationPositionHigh) 
        { 
            ObjectMapper.Map(input, evaluationPositionHigh); 
        } 
        protected override IQueryable<Models.EvaluationPositionHighs> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.EvaluationPositionHighs> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.EvaluationPositionHighs> GetEntityByIdAsync(int id) 
        { 
            var evaluationPositionHigh = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(evaluationPositionHigh); 
        } 
        protected override IQueryable<Models.EvaluationPositionHighs> ApplySorting(IQueryable<Models.EvaluationPositionHighs> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Description); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<EvaluationPositionHighDto>> GetAllEvaluationPositionHighs(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<EvaluationPositionHighDto> ouput = new PagedResultDto<EvaluationPositionHighDto>(); 
            IQueryable<Models.EvaluationPositionHighs> query = query = from x in _evaluationPositionHighRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _evaluationPositionHighRepository.GetAll() 
                        where x.Description.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<EvaluationPositionHighs.Dto.EvaluationPositionHighDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<EvaluationPositionHighDto>> GetAllEvaluationPositionHighsForCombo()
        {
            var evaluationPositionHighList = await _evaluationPositionHighRepository.GetAllListAsync(x => x.IsActive == true);

            var evaluationPositionHigh = ObjectMapper.Map<List<EvaluationPositionHighDto>>(evaluationPositionHighList.ToList());

            return evaluationPositionHigh;
        }
		
    } 
} ;
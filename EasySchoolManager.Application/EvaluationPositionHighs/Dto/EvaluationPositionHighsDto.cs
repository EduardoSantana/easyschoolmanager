//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.EvaluationPositionHighs.Dto 
{
        [AutoMap(typeof(Models.EvaluationPositionHighs))] 
        public class EvaluationPositionHighDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public string Description {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
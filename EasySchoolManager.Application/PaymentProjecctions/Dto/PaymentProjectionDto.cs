﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.PaymentProjecctions.Dto
{
    [AutoMap(typeof(Models.PaymentProjections))]
    public class PaymentProjectionDto : EntityDto<int>
    {
        public int EnrollmentStudentId { get; set; }

        public decimal Amount { get; set; }

        public DateTime PaymentDate { get; set; }

        public int Sequence { get;  set; }

        public bool Applied { get; set; }

        public long? TransactionId { get; set; }

        //Fue comentado por pensar que en los momentos que fue creado no seria necesitado, si es necesitado luego
        //se puede descomentar, pero ver las implicaciones que tiene en otros procesos diferentes al que se este desarrollando.
        //public EnrollmentStudents.Dto.EnrollmentStudentDto2 EnrollmentStudents { get; set; }
    }
}

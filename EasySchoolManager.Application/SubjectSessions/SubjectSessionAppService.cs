//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.SubjectSessions.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.SubjectSessions 
{ 
    [AbpAuthorize(PermissionNames.Pages_SubjectSessions)] 
    public class SubjectSessionAppService : AsyncCrudAppService<Models.SubjectSessions, SubjectSessionDto, int, PagedResultRequestDto, CreateSubjectSessionDto, UpdateSubjectSessionDto>, ISubjectSessionAppService 
    { 
        private readonly IRepository<Models.SubjectSessions, int> _subjectSessionRepository;
		
		    private readonly IRepository<Models.Subjects, int> _subjectRepository;
		    private readonly IRepository<Models.TeacherTenants, int> _teacherTenantRepository;
		    private readonly IRepository<Models.CourseSessions, int> _courseSessionRepository;
		    private readonly IRepository<Models.Periods, int> _periodRepository;


        public SubjectSessionAppService( 
            IRepository<Models.SubjectSessions, int> repository, 
            IRepository<Models.SubjectSessions, int> subjectSessionRepository ,
            IRepository<Models.Subjects, int> subjectRepository
,
            IRepository<Models.TeacherTenants, int> teacherTenantRepository
,
            IRepository<Models.CourseSessions, int> courseSessionRepository
,
            IRepository<Models.Periods, int> periodRepository

            ) 
            : base(repository) 
        { 
            _subjectSessionRepository = subjectSessionRepository; 
			
            _subjectRepository = subjectRepository;

            _teacherTenantRepository = teacherTenantRepository;

            _courseSessionRepository = courseSessionRepository;

            _periodRepository = periodRepository;


			
        } 
        public override async Task<SubjectSessionDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<SubjectSessionDto> Create(CreateSubjectSessionDto input) 
        { 
            CheckCreatePermission(); 
            var subjectSession = ObjectMapper.Map<Models.SubjectSessions>(input); 
			try
            {
              await _subjectSessionRepository.InsertAsync(subjectSession); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(subjectSession); 
        } 
        public override async Task<SubjectSessionDto> Update(UpdateSubjectSessionDto input) 
        { 
            CheckUpdatePermission(); 
            var subjectSession = await _subjectSessionRepository.GetAsync(input.Id);
            MapToEntity(input, subjectSession); 
		    try
            {
               await _subjectSessionRepository.UpdateAsync(subjectSession); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try
            {
               var subjectSession = await _subjectSessionRepository.GetAsync(input.Id); 
               await _subjectSessionRepository.DeleteAsync(subjectSession);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.SubjectSessions MapToEntity(CreateSubjectSessionDto createInput) 
        { 
            var subjectSession = ObjectMapper.Map<Models.SubjectSessions>(createInput); 
            return subjectSession; 
        } 
        protected override void MapToEntity(UpdateSubjectSessionDto input, Models.SubjectSessions subjectSession) 
        { 
            ObjectMapper.Map(input, subjectSession); 
        } 
        protected override IQueryable<Models.SubjectSessions> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected IQueryable<Models.SubjectSessions> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado = Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.TeacherTenants.Teachers.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.SubjectSessions> GetEntityByIdAsync(int id) 
        { 
            var subjectSession = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(subjectSession); 
        } 
        protected override IQueryable<Models.SubjectSessions> ApplySorting(IQueryable<Models.SubjectSessions> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.TeacherTenants.Teachers.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<SubjectSessionDto>> GetAllSubjectSessions(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<SubjectSessionDto> ouput = new PagedResultDto<SubjectSessionDto>(); 
            IQueryable<Models.SubjectSessions> query = query = from x in _subjectSessionRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _subjectSessionRepository.GetAll() 
                        where x.TeacherTenants.Teachers.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<SubjectSessions.Dto.SubjectSessionDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<SubjectSessionDto>> GetAllSubjectSessionsForCombo()
        {
            var subjectSessionList = await _subjectSessionRepository.GetAllListAsync(x => x.IsActive == true);

            var subjectSession = ObjectMapper.Map<List<SubjectSessionDto>>(subjectSessionList.ToList());

            return subjectSession;
        }
		
    } 
};
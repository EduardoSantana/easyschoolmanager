//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.SubjectSessions.Dto 
{
    [AutoMap(typeof(Models.SubjectSessions))] 
    public class SubjectSessionDto : EntityDto<int> 
    {
        public int Id {get;set;} 
        public int SubjectId {get;set;} 
        public int TeacherTenantId {get;set;} 
        public int CourseSessionId {get;set;} 
        public int PeriodId {get;set;} 
        
        public bool IsActive {get;set;} 
        public DateTime CreationTime {get;set;} 
        public long? CreatorUserId {get;set;}

        public string Subjects_Name { get; set; }
        public string TeacherTenants_Teachers_Name { get; set; }
        public string CourseSessions_Courses_Name { get; set; }
        public string Periods_Description { get; set; }

    }
}
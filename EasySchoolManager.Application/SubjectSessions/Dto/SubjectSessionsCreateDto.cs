//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.SubjectSessions.Dto 
{
        [AutoMap(typeof(Models.SubjectSessions))] 
        public class CreateSubjectSessionDto : EntityDto<int> 
        {

              public int SubjectId {get;set;} 

              public int TeacherTenantId {get;set;} 

              public int CourseSessionId {get;set;} 

              public int PeriodId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
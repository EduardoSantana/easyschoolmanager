//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.SubjectSessions.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.SubjectSessions
{
      public interface ISubjectSessionAppService : IAsyncCrudAppService<SubjectSessionDto, int, PagedResultRequestDto, CreateSubjectSessionDto, UpdateSubjectSessionDto>
      {
            Task<PagedResultDto<SubjectSessionDto>> GetAllSubjectSessions(GdPagedResultRequestDto input);
			Task<List<Dto.SubjectSessionDto>> GetAllSubjectSessionsForCombo();
      }
}
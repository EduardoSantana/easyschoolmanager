//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.ChangeTypes.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.ChangeTypes
{
      public interface IChangeTypeAppService : IAsyncCrudAppService<ChangeTypeDto, int, PagedResultRequestDto, CreateChangeTypeDto, UpdateChangeTypeDto>
      {
            Task<PagedResultDto<ChangeTypeDto>> GetAllChangeTypes(GdPagedResultRequestDto input);
			Task<List<Dto.ChangeTypeDto>> GetAllChangeTypesForCombo();
      }
}
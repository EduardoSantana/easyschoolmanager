//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.ChangeTypes.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ChangeTypes 
{ 
    [AbpAuthorize(PermissionNames.Pages_ChangeTypes)] 
    public class ChangeTypeAppService : AsyncCrudAppService<Models.ChangeTypes, ChangeTypeDto, int, PagedResultRequestDto, CreateChangeTypeDto, UpdateChangeTypeDto>, IChangeTypeAppService 
    { 
        private readonly IRepository<Models.ChangeTypes, int> _changeTypeRepository;
		


        public ChangeTypeAppService( 
            IRepository<Models.ChangeTypes, int> repository, 
            IRepository<Models.ChangeTypes, int> changeTypeRepository 
            ) 
            : base(repository) 
        { 
            _changeTypeRepository = changeTypeRepository; 
			

			
        } 
        public override async Task<ChangeTypeDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ChangeTypeDto> Create(CreateChangeTypeDto input) 
        { 
            CheckCreatePermission(); 
            var changeType = ObjectMapper.Map<Models.ChangeTypes>(input); 
			try{
              await _changeTypeRepository.InsertAsync(changeType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(changeType); 
        } 
        public override async Task<ChangeTypeDto> Update(UpdateChangeTypeDto input) 
        { 
            CheckUpdatePermission(); 
            var changeType = await _changeTypeRepository.GetAsync(input.Id);
            MapToEntity(input, changeType); 
		    try{
               await _changeTypeRepository.UpdateAsync(changeType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var changeType = await _changeTypeRepository.GetAsync(input.Id); 
               await _changeTypeRepository.DeleteAsync(changeType);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.ChangeTypes MapToEntity(CreateChangeTypeDto createInput) 
        { 
            var changeType = ObjectMapper.Map<Models.ChangeTypes>(createInput); 
            return changeType; 
        } 
        protected override void MapToEntity(UpdateChangeTypeDto input, Models.ChangeTypes changeType) 
        { 
            ObjectMapper.Map(input, changeType); 
        } 
        protected override IQueryable<Models.ChangeTypes> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ChangeTypes> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ChangeTypes> GetEntityByIdAsync(int id) 
        { 
            var changeType = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(changeType); 
        } 
        protected override IQueryable<Models.ChangeTypes> ApplySorting(IQueryable<Models.ChangeTypes> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ChangeTypeDto>> GetAllChangeTypes(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ChangeTypeDto> ouput = new PagedResultDto<ChangeTypeDto>(); 
            IQueryable<Models.ChangeTypes> query = query = from x in _changeTypeRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _changeTypeRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ChangeTypes.Dto.ChangeTypeDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<ChangeTypeDto>> GetAllChangeTypesForCombo()
        {
            var changeTypeList = await _changeTypeRepository.GetAllListAsync(x => x.IsActive == true);

            var changeType = ObjectMapper.Map<List<ChangeTypeDto>>(changeTypeList.ToList());

            return changeType;
        }
		
    } 
} ;
﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.EnrollmentSequence.Dto
{

    [AutoMap(typeof(Models.EnrollmentSequences))]
    public class EnrollmentSequenceDto: EntityDto
    {
        public long EnrollmentId { get; set; }
        public int Sequence { get; set; }
        public int TenantId { get; set; }

        //public Enrollments.Dto.EnrollmentDto Enrollments { set; get; }
        
        public MultiTenancy.Dto.TenantDto Tenants { set; get; }

    }
}

//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.PurchaseShops.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;
using EasySchoolManager.ArticleShops.Dto;

namespace EasySchoolManager.PurchaseShops 
{ 
    [AbpAuthorize(PermissionNames.Pages_PurchaseShops)] 
    public class PurchaseShopAppService : AsyncCrudAppService<Models.PurchaseShops, PurchaseShopDto, int, PagedResultRequestDto, CreatePurchaseShopDto, UpdatePurchaseShopDto>, IPurchaseShopAppService 
    { 
        private readonly IRepository<Models.PurchaseShops, int> _purchaseShopRepository;
		
		    private readonly IRepository<Models.ProviderShops, int> _providerShopRepository;
            private readonly IRepository<Models.PurchaseArticleShops, int> _purchaseArticleShopRepository;
            private readonly IRepository<Models.InventoryShops, int> _InventoryShopRepository;
        

        public PurchaseShopAppService(
            IRepository<Models.PurchaseShops, int> repository,
            IRepository<Models.PurchaseShops, int> purchaseShopRepository,
            IRepository<Models.ProviderShops, int> providerShopRepository,
            IRepository<Models.PurchaseArticleShops, int> purchaseArticleShopRepository,
            IRepository<Models.InventoryShops, int> InventoryShopRepository
            

            ) 
            : base(repository) 
        { 
            _purchaseShopRepository = purchaseShopRepository; 
			
            _providerShopRepository = providerShopRepository;

            _purchaseArticleShopRepository = purchaseArticleShopRepository;

            _InventoryShopRepository = InventoryShopRepository;
        } 
        public override async Task<PurchaseShopDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<PurchaseShopDto> Create(CreatePurchaseShopDto input) 
        { 
            CheckCreatePermission(); 
            var purchaseShop = ObjectMapper.Map<Models.PurchaseShops>(input);
            purchaseShop.Sequence = getPurchaseShopSequence();
			try{
              await _purchaseShopRepository.InsertAsync(purchaseShop);
                if (input.ArticleShops != null)
                    foreach (var art in input.ArticleShops)
                    {
                        Models.PurchaseArticleShops ta = new Models.PurchaseArticleShops();
                        ta.ArticleShopId = art.Id;
                        ta.Quantity = art.Quantity.Value;
                        ta.Amount = art.Price;
                        ta.Total = art.Total.Value;
                        ta.PurchaseShopId = purchaseShop.Id;
                        ta.IsActive = true;
                        _purchaseArticleShopRepository.Insert(ta);

                        IncreaseInventory(art);
                    }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(purchaseShop); 
        }

        public int getPurchaseShopSequence()
        {
            int sequence = 1;
            try
            {
                sequence = _purchaseShopRepository.GetAllList().Max(x => x.Sequence) + 1;
            }
            catch
            {
            }

            return sequence;
        }

        private void IncreaseInventory(ArticleShopDto art)
        {

            var inventory = _InventoryShopRepository.
                 GetAllList(x => x.ArticleShopId == art.Id && x.TenantId == AbpSession.TenantId).FirstOrDefault();
            if (inventory == null)
            {
                inventory = new Models.InventoryShops();
                inventory.ArticleShopId = art.Id;
                inventory.Existence = 0;
                inventory.IsActive = true;
                _InventoryShopRepository.Insert(inventory);
                CurrentUnitOfWork.SaveChanges();
            }

            inventory.Existence += art.Quantity.Value;
            CurrentUnitOfWork.SaveChanges();

        }
        public override async Task<PurchaseShopDto> Update(UpdatePurchaseShopDto input) 
        { 
            CheckUpdatePermission(); 
            var purchaseShop = await _purchaseShopRepository.GetAsync(input.Id);
            MapToEntity(input, purchaseShop); 
		    try{
               await _purchaseShopRepository.UpdateAsync(purchaseShop); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var purchaseShop = await _purchaseShopRepository.GetAsync(input.Id); 
               await _purchaseShopRepository.DeleteAsync(purchaseShop);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.PurchaseShops MapToEntity(CreatePurchaseShopDto createInput) 
        { 
            var purchaseShop = ObjectMapper.Map<Models.PurchaseShops>(createInput); 
            return purchaseShop; 
        } 
        protected override void MapToEntity(UpdatePurchaseShopDto input, Models.PurchaseShops purchaseShop) 
        { 
            ObjectMapper.Map(input, purchaseShop); 
        } 
        protected override IQueryable<Models.PurchaseShops> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.PurchaseShops> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Document.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.PurchaseShops> GetEntityByIdAsync(int id) 
        { 
            var purchaseShop = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(purchaseShop); 
        } 
        protected override IQueryable<Models.PurchaseShops> ApplySorting(IQueryable<Models.PurchaseShops> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Document); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<PurchaseShopDto>> GetAllPurchaseShops(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<PurchaseShopDto> ouput = new PagedResultDto<PurchaseShopDto>(); 
            IQueryable<Models.PurchaseShops> query = query = from x in _purchaseShopRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _purchaseShopRepository.GetAll() 
                        where x.Document.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<PurchaseShops.Dto.PurchaseShopDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<PurchaseShopDto>> GetAllPurchaseShopsForCombo()
        {
            var purchaseShopList = await _purchaseShopRepository.GetAllListAsync(x => x.IsActive == true);

            var purchaseShop = ObjectMapper.Map<List<PurchaseShopDto>>(purchaseShopList.ToList());

            return purchaseShop;
        }
		
    } 
} ;
//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.PurchaseShops.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.PurchaseShops
{
      public interface IPurchaseShopAppService : IAsyncCrudAppService<PurchaseShopDto, int, PagedResultRequestDto, CreatePurchaseShopDto, UpdatePurchaseShopDto>
      {
            Task<PagedResultDto<PurchaseShopDto>> GetAllPurchaseShops(GdPagedResultRequestDto input);
			Task<List<Dto.PurchaseShopDto>> GetAllPurchaseShopsForCombo();
      }
}
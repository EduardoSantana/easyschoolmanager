//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.PurchaseShops.Dto 
{
        [AutoMap(typeof(Models.PurchaseShops))] 
        public class PurchaseShopDto : EntityDto<int> 
        {
              public int Id {get;set;}
              public int Sequence { get; set; }
              public string Document {get;set;}
              public decimal Amount { get; set; }
              public int ProviderShopId {get;set;} 
              public DateTime Date {get;set;} 
              public string Comnent1 {get;set;} 
              public string Comment2 {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;}
              public string ProviderShops_Name { get; set; }


    }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Periods.Dto 
{
        [AutoMap(typeof(Models.Periods))] 
        public class UpdatePeriodDto : EntityDto<int> 
        {

              public int TenantId {get;set;} 

              [StringLength(5)]  
              public string Period {get;set;} 

              [StringLength(50)]  
              public string Description {get;set;} 

              public DateTime StartDate {get;set;} 

              public DateTime EndDate {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Periods.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Periods 
{ 
    [AbpAuthorize(PermissionNames.Pages_Periods)] 
    public class PeriodAppService : AsyncCrudAppService<Models.Periods, PeriodDto, int, PagedResultRequestDto, CreatePeriodDto, UpdatePeriodDto>, IPeriodAppService 
    { 
        private readonly IRepository<Models.Periods, int> _periodRepository;
        private readonly MultiTenancy.TenantManager _tenantManager;



        public PeriodAppService( 
            IRepository<Models.Periods, int> repository, 
            IRepository<Models.Periods, int> periodRepository,
            MultiTenancy.TenantManager tenantManager
            ) 
            : base(repository) 
        { 
            _periodRepository = periodRepository;
            _tenantManager = tenantManager;



        } 
        public override async Task<PeriodDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<PeriodDto> Create(CreatePeriodDto input) 
        { 
            CheckCreatePermission(); 
            var period = ObjectMapper.Map<Models.Periods>(input); 
			try{
              await _periodRepository.InsertAsync(period); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(period); 
        } 
        public override async Task<PeriodDto> Update(UpdatePeriodDto input) 
        { 
            CheckUpdatePermission(); 
            var period = await _periodRepository.GetAsync(input.Id);
            MapToEntity(input, period); 
		    try{
               await _periodRepository.UpdateAsync(period); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var period = await _periodRepository.GetAsync(input.Id); 
               await _periodRepository.DeleteAsync(period);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Periods MapToEntity(CreatePeriodDto createInput) 
        { 
            var period = ObjectMapper.Map<Models.Periods>(createInput); 
            return period; 
        } 
        protected override void MapToEntity(UpdatePeriodDto input, Models.Periods period) 
        { 
            ObjectMapper.Map(input, period); 
        } 
        protected override IQueryable<Models.Periods> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Periods> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Periods> GetEntityByIdAsync(int id) 
        { 
            var period = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(period); 
        } 
        protected override IQueryable<Models.Periods> ApplySorting(IQueryable<Models.Periods> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Description); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<PeriodDto>> GetAllPeriods(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<PeriodDto> ouput = new PagedResultDto<PeriodDto>(); 
            IQueryable<Models.Periods> query = query = from x in _periodRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _periodRepository.GetAll() 
                        where x.Description.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Periods.Dto.PeriodDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<PeriodDto>> GetAllPeriodsForCombo()
        {
            var periodList = await _periodRepository.GetAllListAsync(x => x.IsActive == true);

            var period = ObjectMapper.Map<List<PeriodDto>>(periodList.ToList());

            return period;
        }


        [AbpAllowAnonymous]
        public async Task<List<PeriodDto>> GetAllPeriodsForDiscountForCombo()
        {

            var ten= _tenantManager.Tenants.Where(x => x.Id == AbpSession.TenantId).FirstOrDefault();

            var periodList = await _periodRepository.GetAllListAsync(x => x.IsActive == true && (x.Id == ten.NextPeriodId || x.Id == ten.PeriodId));

            var period = ObjectMapper.Map<List<PeriodDto>>(periodList.ToList());

            return period;
        }

    } 
} ;
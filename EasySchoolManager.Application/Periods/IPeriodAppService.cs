//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Periods.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Periods
{
      public interface IPeriodAppService : IAsyncCrudAppService<PeriodDto, int, PagedResultRequestDto, CreatePeriodDto, UpdatePeriodDto>
      {
            Task<PagedResultDto<PeriodDto>> GetAllPeriods(GdPagedResultRequestDto input);
			Task<List<Dto.PeriodDto>> GetAllPeriodsForCombo();
            Task<List<Dto.PeriodDto>> GetAllPeriodsForDiscountForCombo();
        
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.Authorization.Permissions.Dto;

namespace EasySchoolManager.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}

namespace EasySchoolManager.Authorization.Permissions.Dto
{
    public class FlatPermissionWithLevelDto : FlatPermissionDto
    {
        public int Level { get; set; }

        public override string ToString()
        {
            return $"Description={this.Description}, Name={this.Name}, DisplayName= {this.DisplayName}";
        }
    }
}
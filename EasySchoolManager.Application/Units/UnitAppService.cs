//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Units.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Units 
{ 
    [AbpAuthorize(PermissionNames.Pages_Units)] 
    public class UnitAppService : AsyncCrudAppService<Models.Units, UnitDto, int, PagedResultRequestDto, CreateUnitDto, UpdateUnitDto>, IUnitAppService 
    { 
        private readonly IRepository<Models.Units, int> _UnitRepository;
		


        public UnitAppService( 
            IRepository<Models.Units, int> repository, 
            IRepository<Models.Units, int> UnitRepository 
            ) 
            : base(repository) 
        { 
            _UnitRepository = UnitRepository; 
			

			
        } 
        public override async Task<UnitDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<UnitDto> Create(CreateUnitDto input) 
        { 
            CheckCreatePermission(); 
            var Unit = ObjectMapper.Map<Models.Units>(input); 
			try{
              await _UnitRepository.InsertAsync(Unit); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(Unit); 
        } 
        public override async Task<UnitDto> Update(UpdateUnitDto input) 
        { 
            CheckUpdatePermission(); 
            var Unit = await _UnitRepository.GetAsync(input.Id);
            MapToEntity(input, Unit); 
		    try{
               await _UnitRepository.UpdateAsync(Unit); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var Unit = await _UnitRepository.GetAsync(input.Id); 
               await _UnitRepository.DeleteAsync(Unit);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Units MapToEntity(CreateUnitDto createInput) 
        { 
            var Unit = ObjectMapper.Map<Models.Units>(createInput); 
            return Unit; 
        } 
        protected override void MapToEntity(UpdateUnitDto input, Models.Units Unit) 
        { 
            ObjectMapper.Map(input, Unit); 
        } 
        protected override IQueryable<Models.Units> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Units> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Units> GetEntityByIdAsync(int id) 
        { 
            var Unit = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(Unit); 
        } 
        protected override IQueryable<Models.Units> ApplySorting(IQueryable<Models.Units> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Description); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<UnitDto>> GetAllUnits(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<UnitDto> ouput = new PagedResultDto<UnitDto>(); 
            IQueryable<Models.Units> query = query = from x in _UnitRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _UnitRepository.GetAll() 
                        where x.Description.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Units.Dto.UnitDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<UnitDto>> GetAllUnitsForCombo()
        {
            var UnitList = await _UnitRepository.GetAllListAsync(x => x.IsActive == true);

            var Unit = ObjectMapper.Map<List<UnitDto>>(UnitList.ToList());

            return Unit;
        }
		
    } 
} ;
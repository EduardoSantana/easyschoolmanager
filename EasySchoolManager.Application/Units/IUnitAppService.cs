//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Units.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Units
{
      public interface IUnitAppService : IAsyncCrudAppService<UnitDto, int, PagedResultRequestDto, CreateUnitDto, UpdateUnitDto>
      {
            Task<PagedResultDto<UnitDto>> GetAllUnits(GdPagedResultRequestDto input);
			Task<List<Dto.UnitDto>> GetAllUnitsForCombo();
      }
}
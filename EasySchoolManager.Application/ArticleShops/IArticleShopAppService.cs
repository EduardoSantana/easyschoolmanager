//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.ArticleShops.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.ArticleShops
{
      public interface IArticleShopAppService : IAsyncCrudAppService<ArticleShopDto, int, PagedResultRequestDto, CreateArticleShopDto, UpdateArticleShopDto>
      {
            Task<PagedResultDto<ArticleShopDto>> GetAllArticleShops(GdPagedResultRequestDto input);
			Task<List<Dto.ArticleShopDto>> GetAllArticleShopsForCombo();
            Task<ArticleShopDto> GetSpecific(int art);
            Task<string> GetReference(int art);
    }
}
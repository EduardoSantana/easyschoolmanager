//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.ArticleShops.Dto
{
    [AutoMap(typeof(Models.ArticleShops))]
    public class ArticleShopDto : EntityDto<int>
    {
        public int Sequence { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public Decimal Price { get; set; }
        public int? UnitId { get; set; }
        public int? BrandId { get; set; }
        public bool IsActive { get; set; }
        public int? ButtonPositionId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        public long? Quantity { get; set; }
        public Decimal? Total { get; set; }
        public string Units_Description { get; set; }
        public string ButtonPositions_Number { get; set; }
        public List<InventoryShops.Dto.InventoryShopDto> InventoryShops { get; set; }
    }
}
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.ArticleShops.Dto 
{
        [AutoMap(typeof(Models.ArticleShops))] 
        public class CreateArticleShopDto : EntityDto<int> 
        {

              public int Sequence {get;set;} 

              [StringLength(200)] 
              public string Description {get;set;} 

              [StringLength(20)] 
              public string Reference {get;set;} 

              public Decimal Price {get;set;} 

              public int? UnitId {get;set;}

              public int? ButtonPositionId { get; set; }

              public int? BrandId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
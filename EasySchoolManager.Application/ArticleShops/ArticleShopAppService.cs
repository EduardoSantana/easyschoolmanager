//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.ArticleShops.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ArticleShops 
{ 
    [AbpAuthorize(PermissionNames.Pages_ArticleShops)] 
    public class ArticleShopAppService : AsyncCrudAppService<Models.ArticleShops, ArticleShopDto, int, PagedResultRequestDto, CreateArticleShopDto, UpdateArticleShopDto>, IArticleShopAppService 
    { 
        private readonly IRepository<Models.ArticleShops, int> _articleShopRepository;
		
		    private readonly IRepository<Models.ArticlesTypes, int> _articlesTypeRepository;
		    private readonly IRepository<Models.Units, int> _unitRepository;
		    private readonly IRepository<Models.Brands, int> _BrandRepository;


        public ArticleShopAppService( 
            IRepository<Models.ArticleShops, int> repository, 
            IRepository<Models.ArticleShops, int> articleShopRepository ,
            IRepository<Models.ArticlesTypes, int> articlesTypeRepository,
            IRepository<Models.Units, int> unitRepository,
            IRepository<Models.Brands, int> BrandRepository

            ) 
            : base(repository) 
        { 
            _articleShopRepository = articleShopRepository; 
			
            _articlesTypeRepository = articlesTypeRepository;

            _unitRepository = unitRepository;

            _BrandRepository = BrandRepository;
			
        } 
        public override async Task<ArticleShopDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ArticleShopDto> Create(CreateArticleShopDto input) 
        { 
            CheckCreatePermission(); 
            var articleShop = ObjectMapper.Map<Models.ArticleShops>(input);
            articleShop.Sequence = getArticleShopSequence();
			try{
              await _articleShopRepository.InsertAsync(articleShop); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(articleShop); 
        }

        public async Task<ArticleShopDto> GetSpecific(int art)
        {
            var query = await _articleShopRepository.GetAllListAsync(x => x.Sequence == art);
            var devw = query.FirstOrDefault();
            var dev = AutoMapper.Mapper.Map<ArticleShopDto>(devw);
            return dev;
        }

        public int getArticleShopSequence()
        {
           int sequence = 1;
            try
            {
                sequence = _articleShopRepository.GetAllList().Max(x => x.Sequence) + 1;
            }
            catch
            {
            }

            return sequence;
        }


        public async Task<string> GetReference(int art)
        {
            var query = await _articleShopRepository.GetAllListAsync(x => x.ButtonPositionId == art);
            var res = query.Max(x => x.Reference);
            return res;
        }


        public override async Task<ArticleShopDto> Update(UpdateArticleShopDto input) 
        { 
            CheckUpdatePermission(); 
            var articleShop = await _articleShopRepository.GetAsync(input.Id);
            MapToEntity(input, articleShop); 
		    try{
               await _articleShopRepository.UpdateAsync(articleShop); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var articleShop = await _articleShopRepository.GetAsync(input.Id); 
               await _articleShopRepository.DeleteAsync(articleShop);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.ArticleShops MapToEntity(CreateArticleShopDto createInput) 
        { 
            var articleShop = ObjectMapper.Map<Models.ArticleShops>(createInput); 
            return articleShop; 
        } 
        protected override void MapToEntity(UpdateArticleShopDto input, Models.ArticleShops articleShop) 
        { 
            ObjectMapper.Map(input, articleShop); 
        } 
        protected override IQueryable<Models.ArticleShops> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ArticleShops> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Description.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ArticleShops> GetEntityByIdAsync(int id) 
        { 
            var articleShop = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(articleShop); 
        } 
        protected override IQueryable<Models.ArticleShops> ApplySorting(IQueryable<Models.ArticleShops> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Description); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ArticleShopDto>> GetAllArticleShops(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ArticleShopDto> ouput = new PagedResultDto<ArticleShopDto>(); 
            IQueryable<Models.ArticleShops> query = query = from x in _articleShopRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _articleShopRepository.GetAll() 
                        where x.Description.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ArticleShops.Dto.ArticleShopDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<ArticleShopDto>> GetAllArticleShopsForCombo()
        {
            var articleShopList = await _articleShopRepository.GetAllListAsync(x => x.IsActive == true);

            var articleShop = ObjectMapper.Map<List<ArticleShopDto>>(articleShopList.ToList());

            return articleShop;
        }

    } 
} ;
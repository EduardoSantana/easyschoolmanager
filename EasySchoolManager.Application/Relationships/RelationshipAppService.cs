//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Relationships.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Relationships 
{ 
    [AbpAuthorize(PermissionNames.Pages_Relationships)] 
    public class RelationshipAppService : AsyncCrudAppService<Models.Relationships, RelationshipDto, int, PagedResultRequestDto, CreateRelationshipDto, UpdateRelationshipDto>, IRelationshipAppService 
    { 
        private readonly IRepository<Models.Relationships, int> _relationshipRepository;
		


        public RelationshipAppService( 
            IRepository<Models.Relationships, int> repository, 
            IRepository<Models.Relationships, int> relationshipRepository 
            ) 
            : base(repository) 
        { 
            _relationshipRepository = relationshipRepository; 
			

			
        } 
        public override async Task<RelationshipDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<RelationshipDto> Create(CreateRelationshipDto input) 
        { 
            CheckCreatePermission(); 
            var relationship = ObjectMapper.Map<Models.Relationships>(input); 
			try{
              await _relationshipRepository.InsertAsync(relationship); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(relationship); 
        } 
        public override async Task<RelationshipDto> Update(UpdateRelationshipDto input) 
        { 
            CheckUpdatePermission(); 
            var relationship = await _relationshipRepository.GetAsync(input.Id);
            MapToEntity(input, relationship); 
		    try{
               await _relationshipRepository.UpdateAsync(relationship); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var relationship = await _relationshipRepository.GetAsync(input.Id); 
               await _relationshipRepository.DeleteAsync(relationship);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Relationships MapToEntity(CreateRelationshipDto createInput) 
        { 
            var relationship = ObjectMapper.Map<Models.Relationships>(createInput); 
            return relationship; 
        } 
        protected override void MapToEntity(UpdateRelationshipDto input, Models.Relationships relationship) 
        { 
            ObjectMapper.Map(input, relationship); 
        } 
        protected override IQueryable<Models.Relationships> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Relationships> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Relationships> GetEntityByIdAsync(int id) 
        { 
            var relationship = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(relationship); 
        } 
        protected override IQueryable<Models.Relationships> ApplySorting(IQueryable<Models.Relationships> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<RelationshipDto>> GetAllRelationships(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<RelationshipDto> ouput = new PagedResultDto<RelationshipDto>(); 
            IQueryable<Models.Relationships> query = query = from x in _relationshipRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _relationshipRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Relationships.Dto.RelationshipDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<RelationshipDto>> GetAllRelationshipsForCombo()
        {
            var relationshipList = await _relationshipRepository.GetAllListAsync(x => x.IsActive == true);

            var relationship = ObjectMapper.Map<List<RelationshipDto>>(relationshipList.ToList());

            return relationship;
        }
		
    } 
} ;
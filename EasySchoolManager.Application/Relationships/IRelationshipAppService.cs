//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Relationships.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Relationships
{
      public interface IRelationshipAppService : IAsyncCrudAppService<RelationshipDto, int, PagedResultRequestDto, CreateRelationshipDto, UpdateRelationshipDto>
      {
            Task<PagedResultDto<RelationshipDto>> GetAllRelationships(GdPagedResultRequestDto input);
			Task<List<Dto.RelationshipDto>> GetAllRelationshipsForCombo();
      }
}
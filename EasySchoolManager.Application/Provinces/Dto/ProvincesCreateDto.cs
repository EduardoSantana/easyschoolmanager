//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Provinces.Dto 
{
        [AutoMap(typeof(Models.Provinces))] 
        public class CreateProvinceDto : EntityDto<int> 
        {

              [StringLength(50)] 
              public string Name {get;set;} 

              public int RegionId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}
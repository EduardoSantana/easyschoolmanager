//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Provinces.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Provinces
{
      public interface IProvinceAppService : IAsyncCrudAppService<ProvinceDto, int, PagedResultRequestDto, CreateProvinceDto, UpdateProvinceDto>
      {
            Task<PagedResultDto<ProvinceDto>> GetAllProvinces(GdPagedResultRequestDto input);
			Task<List<Dto.ProvinceDto>> GetAllProvincesForCombo();
      }
}
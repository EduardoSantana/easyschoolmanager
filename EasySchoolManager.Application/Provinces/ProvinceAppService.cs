//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Provinces.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Provinces 
{ 
    [AbpAuthorize(PermissionNames.Pages_Provinces)] 
    public class ProvinceAppService : AsyncCrudAppService<Models.Provinces, ProvinceDto, int, PagedResultRequestDto, CreateProvinceDto, UpdateProvinceDto>, IProvinceAppService 
    { 
        private readonly IRepository<Models.Provinces, int> _provinceRepository;
		
		    private readonly IRepository<Models.Regions, int> _regionRepository;


        public ProvinceAppService( 
            IRepository<Models.Provinces, int> repository, 
            IRepository<Models.Provinces, int> provinceRepository ,
            IRepository<Models.Regions, int> regionRepository

            ) 
            : base(repository) 
        { 
            _provinceRepository = provinceRepository; 
			
            _regionRepository = regionRepository;


			
        } 
        public override async Task<ProvinceDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ProvinceDto> Create(CreateProvinceDto input) 
        { 
            CheckCreatePermission(); 
            var province = ObjectMapper.Map<Models.Provinces>(input); 
			try{
              await _provinceRepository.InsertAsync(province); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(province); 
        } 
        public override async Task<ProvinceDto> Update(UpdateProvinceDto input) 
        { 
            CheckUpdatePermission(); 
            var province = await _provinceRepository.GetAsync(input.Id);
            MapToEntity(input, province); 
		    try{
               await _provinceRepository.UpdateAsync(province); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var province = await _provinceRepository.GetAsync(input.Id); 
               await _provinceRepository.DeleteAsync(province);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Provinces MapToEntity(CreateProvinceDto createInput) 
        { 
            var province = ObjectMapper.Map<Models.Provinces>(createInput); 
            return province; 
        } 
        protected override void MapToEntity(UpdateProvinceDto input, Models.Provinces province) 
        { 
            ObjectMapper.Map(input, province); 
        } 
        protected override IQueryable<Models.Provinces> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Provinces> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Provinces> GetEntityByIdAsync(int id) 
        { 
            var province = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(province); 
        } 
        protected override IQueryable<Models.Provinces> ApplySorting(IQueryable<Models.Provinces> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ProvinceDto>> GetAllProvinces(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ProvinceDto> ouput = new PagedResultDto<ProvinceDto>(); 
            IQueryable<Models.Provinces> query = query = from x in _provinceRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _provinceRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Provinces.Dto.ProvinceDto>>(query.ToList()); 
            return ouput; 
        } 
		
        [AbpAllowAnonymous]
		public async Task<List<ProvinceDto>> GetAllProvincesForCombo()
        {
            var provinceList = await _provinceRepository.GetAllListAsync(x => x.IsActive == true);

            var province = ObjectMapper.Map<List<ProvinceDto>>(provinceList.ToList());

            return province;
        }
		
    } 
} ;
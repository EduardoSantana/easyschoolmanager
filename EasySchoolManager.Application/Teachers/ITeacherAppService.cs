//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Teachers.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Teachers
{
      public interface ITeacherAppService : IAsyncCrudAppService<TeacherDto, int, PagedResultRequestDto, CreateTeacherDto, UpdateTeacherDto>
      {
            Task<PagedResultDto<TeacherDto>> GetAllTeachers(GdPagedResultRequestDto input);
			Task<List<Dto.TeacherDto>> GetAllTeachersForCombo();
      }
}
//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.Teachers.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Teachers 
{ 
    [AbpAuthorize(PermissionNames.Pages_Teachers)] 
    public class TeacherAppService : AsyncCrudAppService<Models.Teachers, TeacherDto, int, PagedResultRequestDto, CreateTeacherDto, UpdateTeacherDto>, ITeacherAppService 
    { 
        private readonly IRepository<Models.Teachers, int> _teacherRepository;
		
		    private readonly IRepository<Models.Genders, int> _genderRepository;


        public TeacherAppService( 
            IRepository<Models.Teachers, int> repository, 
            IRepository<Models.Teachers, int> teacherRepository ,
            IRepository<Models.Genders, int> genderRepository

            ) 
            : base(repository) 
        { 
            _teacherRepository = teacherRepository; 
			
            _genderRepository = genderRepository;


			
        } 
        public override async Task<TeacherDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<TeacherDto> Create(CreateTeacherDto input) 
        { 
            CheckCreatePermission(); 
            var teacher = ObjectMapper.Map<Models.Teachers>(input); 
			try{
              await _teacherRepository.InsertAsync(teacher); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(teacher); 
        } 
        public override async Task<TeacherDto> Update(UpdateTeacherDto input) 
        { 
            CheckUpdatePermission(); 
            var teacher = await _teacherRepository.GetAsync(input.Id);
            MapToEntity(input, teacher); 
		    try{
               await _teacherRepository.UpdateAsync(teacher); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var teacher = await _teacherRepository.GetAsync(input.Id); 
               await _teacherRepository.DeleteAsync(teacher);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.Teachers MapToEntity(CreateTeacherDto createInput) 
        { 
            var teacher = ObjectMapper.Map<Models.Teachers>(createInput); 
            return teacher; 
        } 
        protected override void MapToEntity(UpdateTeacherDto input, Models.Teachers teacher) 
        { 
            ObjectMapper.Map(input, teacher); 
        } 
        protected override IQueryable<Models.Teachers> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Teachers> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Teachers> GetEntityByIdAsync(int id) 
        { 
            var teacher = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(teacher); 
        } 
        protected override IQueryable<Models.Teachers> ApplySorting(IQueryable<Models.Teachers> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<TeacherDto>> GetAllTeachers(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<TeacherDto> ouput = new PagedResultDto<TeacherDto>(); 
            IQueryable<Models.Teachers> query = query = from x in _teacherRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _teacherRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Teachers.Dto.TeacherDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<TeacherDto>> GetAllTeachersForCombo()
        {
            var teacherList = await _teacherRepository.GetAllListAsync(x => x.IsActive == true);

            var teacher = ObjectMapper.Map<List<TeacherDto>>(teacherList.ToList());

            return teacher;
        }
		
    } 
} ;
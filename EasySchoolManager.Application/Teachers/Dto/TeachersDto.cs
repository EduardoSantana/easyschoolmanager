//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Teachers.Dto 
{
        [AutoMap(typeof(Models.Teachers))] 
        public class TeacherDto : EntityDto<int> 
        {
              public string Name {get;set;} 
              public DateTime DateOfBirth {get;set;} 
              public string PersonalId {get;set;} 
              public bool IsGraduated {get;set;} 
              public bool HasMasterDegree {get;set;} 
              public bool IsTechnician {get;set;} 
              public int GenderId {get;set;} 
              public long? UserId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

              public Genders.Dto.GenderDto Genders { set; get; }
         }
}
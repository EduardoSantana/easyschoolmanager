//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.PeriodDiscounts.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.PeriodDiscounts
{
    [AbpAllowAnonymous]
    public class PeriodDiscountAppService : AsyncCrudAppService<Models.PeriodDiscounts, PeriodDiscountDto, int, PagedResultRequestDto, CreatePeriodDiscountDto, UpdatePeriodDiscountDto>, IPeriodDiscountAppService
    {
        private readonly IRepository<Models.PeriodDiscounts, int> _periodDiscountRepository;
        private readonly IRepository<Models.PeriodDiscountDetails, int> _periodDiscountDetailsRepository;

        private readonly IRepository<Models.DiscountNames, int> _discountNameRepository;
        private readonly IRepository<Models.Periods, int> _periodRepository;
        private readonly IRepository<Models.Catalogs, int> _catalogRepository;


        public PeriodDiscountAppService(
            IRepository<Models.PeriodDiscounts, int> repository,
            IRepository<Models.PeriodDiscounts, int> periodDiscountRepository,
            IRepository<Models.PeriodDiscountDetails, int> periodDiscountDetailRepository,
            IRepository<Models.DiscountNames, int> discountNameRepository
,
            IRepository<Models.Periods, int> periodRepository
,
            IRepository<Models.Catalogs, int> catalogRepository

            )
            : base(repository)
        {
            _periodDiscountRepository = periodDiscountRepository;

            _periodDiscountDetailsRepository = periodDiscountDetailRepository;

            _discountNameRepository = discountNameRepository;

            _periodRepository = periodRepository;

            _catalogRepository = catalogRepository;

        }
        public override async Task<PeriodDiscountDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<PeriodDiscountDto> Create(CreatePeriodDiscountDto input)
        {
            CheckCreatePermission();
            var periodDiscount = ObjectMapper.Map<Models.PeriodDiscounts>(input);
            try
            {
                periodDiscount.PeriodDiscountDetails = null;
                await _periodDiscountRepository.InsertAndGetIdAsync(periodDiscount);

                foreach (var item in input.PeriodDiscountDetails)
                {

                    if (item.DiscountPercent <= 0 || item.DiscountPercent > 100)
                        throw new Exception(L("TheValueMustBeBetween1And100"));

                    if (item.Deleted != true && item.Id == 0)
                    {
                        Models.PeriodDiscountDetails det = new Models.PeriodDiscountDetails();
                        det.PeriodDiscountId = periodDiscount.Id;
                        det.DiscountPercent = item.DiscountPercent;
                        det.IsActive = true;
                        await _periodDiscountDetailsRepository.InsertAsync(det);
                    }

                    CurrentUnitOfWork.SaveChanges();
                }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(periodDiscount);
        }
        public override async Task<PeriodDiscountDto> Update(UpdatePeriodDiscountDto input)
        {
            CheckUpdatePermission();
            var periodDiscount = await _periodDiscountRepository.GetAsync(input.Id);
            periodDiscount.DiscountNameId = input.DiscountNameId;
            periodDiscount.ConceptId = input.ConceptId;
            periodDiscount.PeriodId = input.PeriodId;
            periodDiscount.PeriodDiscountDetails = null;
            try
            {
                await _periodDiscountRepository.UpdateAsync(periodDiscount);


                foreach (var item in input.PeriodDiscountDetails)
                {

                    if (item.DiscountPercent <= 0 || item.DiscountPercent > 100)
                        throw new Exception(L("TheValueMustBeBetween1And100"));

                    if (item.Deleted != true && item.Id == 0)
                    {
                        Models.PeriodDiscountDetails det = new Models.PeriodDiscountDetails();
                        det.PeriodDiscountId = input.Id;
                        det.DiscountPercent = item.DiscountPercent;
                        det.IsActive = true;
                        await _periodDiscountDetailsRepository.InsertAsync(det);
                    }
                    else if (item.Deleted == true)
                    {
                        if (item.Id > 0)
                        {
                            var ABorrar = _periodDiscountDetailsRepository.GetAll().Where(x => x.Id == item.Id).FirstOrDefault();
                            if (ABorrar != null)
                            {
                                ABorrar.IsDeleted = true;
                                ABorrar.IsActive = false;
                            }
                        }
                    }
                    CurrentUnitOfWork.SaveChanges();
                }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var periodDiscount = await _periodDiscountRepository.GetAsync(input.Id);
                await _periodDiscountRepository.DeleteAsync(periodDiscount);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.PeriodDiscounts MapToEntity(CreatePeriodDiscountDto createInput)
        {
            var periodDiscount = ObjectMapper.Map<Models.PeriodDiscounts>(createInput);
            return periodDiscount;
        }
        protected override void MapToEntity(UpdatePeriodDiscountDto input, Models.PeriodDiscounts periodDiscount)
        {
            ObjectMapper.Map(input, periodDiscount);
        }
        protected override IQueryable<Models.PeriodDiscounts> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.PeriodDiscounts> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.DiscountNames.Name.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.PeriodDiscounts> GetEntityByIdAsync(int id)
        {
            var periodDiscount = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(periodDiscount);
        }
        protected override IQueryable<Models.PeriodDiscounts> ApplySorting(IQueryable<Models.PeriodDiscounts> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.DiscountNames.Name);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<PeriodDiscountDto>> GetAllPeriodDiscounts(GdPagedResultRequestDto input)
        {
            PagedResultDto<PeriodDiscountDto> ouput = new PagedResultDto<PeriodDiscountDto>();
            IQueryable<Models.PeriodDiscounts> query = query = from x in _periodDiscountRepository.GetAll()
                                                               select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _periodDiscountRepository.GetAll()
                        where x.DiscountNames.Name.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<PeriodDiscounts.Dto.PeriodDiscountDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<PeriodDiscountDto>> GetAllPeriodDiscountsForCombo()
        {
            var periodDiscountList = await _periodDiscountRepository.GetAllListAsync(x => x.IsActive == true);

            var periodDiscount = ObjectMapper.Map<List<PeriodDiscountDto>>(periodDiscountList.ToList());

            return periodDiscount;
        }

    }
};
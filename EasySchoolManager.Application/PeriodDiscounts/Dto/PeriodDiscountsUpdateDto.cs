//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper;
using System.Collections.Generic;

namespace EasySchoolManager.PeriodDiscounts.Dto 
{
        [AutoMap(typeof(Models.PeriodDiscounts))] 
        public class UpdatePeriodDiscountDto : EntityDto<int> 
        {
              public int DiscountNameId {get;set;} 
              public int PeriodId {get;set;} 
              public int ConceptId { get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 
              public List<PeriodDiscountDetails.Dto.UpdatePeriodDiscountDetailDto> PeriodDiscountDetails { get; set; }


    }

}
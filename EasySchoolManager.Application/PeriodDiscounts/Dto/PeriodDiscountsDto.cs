//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.Periods.Dto;
using EasySchoolManager.Catalogs.Dto;
using EasySchoolManager.DiscountNames.Dto;
using EasySchoolManager.Concepts.Dto;
using System.Collections.Generic;
using EasySchoolManager.PeriodDiscountDetails.Dto;

namespace EasySchoolManager.PeriodDiscounts.Dto
{
    [AutoMap(typeof(Models.PeriodDiscounts))]
    public class PeriodDiscountDto : EntityDto<int>
    {
        public int DiscountNameId { get; set; }
        public int PeriodId { get; set; }
        public int ConceptId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public PeriodDto Periods { get; set; }
        public ConceptDto Concepts { get; set; }
        public DiscountNameDto DiscountNames { get; set; }

        public List<PeriodDiscountDetailDto> PeriodDiscountDetails { get; set; }

    }
}
//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.PeriodDiscounts.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.PeriodDiscounts
{
      public interface IPeriodDiscountAppService : IAsyncCrudAppService<PeriodDiscountDto, int, PagedResultRequestDto, CreatePeriodDiscountDto, UpdatePeriodDiscountDto>
      {
            Task<PagedResultDto<PeriodDiscountDto>> GetAllPeriodDiscounts(GdPagedResultRequestDto input);
			Task<List<Dto.PeriodDiscountDto>> GetAllPeriodDiscountsForCombo();
      }
}
//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.PeriodDiscountDetails.Dto
{
    [AutoMap(typeof(Models.PeriodDiscountDetails))]
    public class CreatePeriodDiscountDetailDto : EntityDto<int>
    {
        public int PeriodDiscountId { get; set; }
        public decimal DiscountPercent { get; set; }
        public bool Deleted { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

    }
}
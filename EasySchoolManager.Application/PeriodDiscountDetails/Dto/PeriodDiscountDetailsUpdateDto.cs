//Created from Templaste MG

using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace EasySchoolManager.PeriodDiscountDetails.Dto
{
    [AutoMap(typeof(Models.PeriodDiscountDetails))]
    public class UpdatePeriodDiscountDetailDto : EntityDto<int>
    {
        public int PeriodDiscountId { get; set; }
        public decimal DiscountPercent { get; set; }
        public bool IsActive { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
    }
}
//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.PeriodDiscountDetails.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.PeriodDiscountDetails
{
    public interface IPeriodDiscountDetailAppService : IAsyncCrudAppService<PeriodDiscountDetailDto, int, PagedResultRequestDto, CreatePeriodDiscountDetailDto, UpdatePeriodDiscountDetailDto>
    {
        Task<PagedResultDto<PeriodDiscountDetailDto>> GetAllPeriodDiscountDetails(GdPagedResultRequestDto input);
        Task<List<Dto.PeriodDiscountDetailDto>> GetAllPeriodDiscountDetailsForCombo();
        Task<List<PeriodDiscountDetailDto>> GetAllPeriodDiscountDetailsByPeriodAndConceptForCombo(GetAllPeriodDiscountDetailsByPeriodAndConceptForComboInput input);
      }
}
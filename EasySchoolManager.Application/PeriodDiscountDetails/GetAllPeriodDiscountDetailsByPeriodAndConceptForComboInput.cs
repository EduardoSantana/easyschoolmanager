﻿//Created from Templaste MG

namespace EasySchoolManager.PeriodDiscountDetails
{
    public class GetAllPeriodDiscountDetailsByPeriodAndConceptForComboInput
    {
        public int? PeriodId { get; set; }

        public int? ConceptId { get; set; }

        public int? DiscountNameId { get; set; }
    }
}
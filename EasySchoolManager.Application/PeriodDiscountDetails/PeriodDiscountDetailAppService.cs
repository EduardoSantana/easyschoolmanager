//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.PeriodDiscountDetails.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.PeriodDiscountDetails
{
    public class PeriodDiscountDetailAppService : AsyncCrudAppService<Models.PeriodDiscountDetails, PeriodDiscountDetailDto, int, PagedResultRequestDto, CreatePeriodDiscountDetailDto, UpdatePeriodDiscountDetailDto>, IPeriodDiscountDetailAppService
    {
        private readonly IRepository<Models.PeriodDiscountDetails, int> _periodDiscountDetailRepository;
        private readonly IRepository<Models.PeriodDiscounts, int> _periodDiscountRepository;



        public PeriodDiscountDetailAppService(
            IRepository<Models.PeriodDiscountDetails, int> repository,
            IRepository<Models.PeriodDiscountDetails, int> periodDiscountDetailRepository,
            IRepository<Models.PeriodDiscounts, int> periodDiscountRepository
            )
            : base(repository)
        {
            _periodDiscountDetailRepository = periodDiscountDetailRepository;
            _periodDiscountRepository = periodDiscountRepository;



        }
        public override async Task<PeriodDiscountDetailDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<PeriodDiscountDetailDto> Create(CreatePeriodDiscountDetailDto input)
        {
            CheckCreatePermission();
            var periodDiscountDetail = ObjectMapper.Map<Models.PeriodDiscountDetails>(input);
            try
            {
                await _periodDiscountDetailRepository.InsertAsync(periodDiscountDetail);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(periodDiscountDetail);
        }
        public override async Task<PeriodDiscountDetailDto> Update(UpdatePeriodDiscountDetailDto input)
        {
            CheckUpdatePermission();
            var periodDiscountDetail = await _periodDiscountDetailRepository.GetAsync(input.Id);
            MapToEntity(input, periodDiscountDetail);
            try
            {
                await _periodDiscountDetailRepository.UpdateAsync(periodDiscountDetail);



            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var periodDiscountDetail = await _periodDiscountDetailRepository.GetAsync(input.Id);
                await _periodDiscountDetailRepository.DeleteAsync(periodDiscountDetail);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.PeriodDiscountDetails MapToEntity(CreatePeriodDiscountDetailDto createInput)
        {
            var periodDiscountDetail = ObjectMapper.Map<Models.PeriodDiscountDetails>(createInput);
            return periodDiscountDetail;
        }
        protected override void MapToEntity(UpdatePeriodDiscountDetailDto input, Models.PeriodDiscountDetails periodDiscountDetail)
        {
            ObjectMapper.Map(input, periodDiscountDetail);
        }
        protected override IQueryable<Models.PeriodDiscountDetails> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.PeriodDiscountDetails> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.PeriodDiscounts.DiscountNames.Name.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.PeriodDiscountDetails> GetEntityByIdAsync(int id)
        {
            var periodDiscountDetail = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(periodDiscountDetail);
        }
        protected override IQueryable<Models.PeriodDiscountDetails> ApplySorting(IQueryable<Models.PeriodDiscountDetails> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.PeriodDiscounts.DiscountNames.Name);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<PeriodDiscountDetailDto>> GetAllPeriodDiscountDetails(GdPagedResultRequestDto input)
        {
            PagedResultDto<PeriodDiscountDetailDto> ouput = new PagedResultDto<PeriodDiscountDetailDto>();
            IQueryable<Models.PeriodDiscountDetails> query = query = from x in _periodDiscountDetailRepository.GetAll()
                                                                     select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _periodDiscountDetailRepository.GetAll()
                        where x.PeriodDiscounts.DiscountNames.Name.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<PeriodDiscountDetails.Dto.PeriodDiscountDetailDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<PeriodDiscountDetailDto>> GetAllPeriodDiscountDetailsForCombo()
        {
            var periodDiscountDetailList = await _periodDiscountDetailRepository.GetAllListAsync(x => x.IsActive == true);

            var periodDiscountDetail = ObjectMapper.Map<List<PeriodDiscountDetailDto>>(periodDiscountDetailList.ToList());

            return periodDiscountDetail;
        }



        [AbpAllowAnonymous]
        public async Task<List<PeriodDiscountDetailDto>> GetAllPeriodDiscountDetailsByPeriodAndConceptForCombo(GetAllPeriodDiscountDetailsByPeriodAndConceptForComboInput input)
        {
            List<PeriodDiscountDetailDto> periodDiscountDetail = new List<PeriodDiscountDetailDto>();
            //var periodDiscountDetailList = await _periodDiscountDetailRepository.GetAllListAsync(x =>
            //    x.IsActive == true &&
            //    x.PeriodDiscounts.ConceptId == input.ConceptId &&
            //    x.PeriodDiscounts.PeriodId == input.PeriodId
            //    );

            if (!(input.PeriodId.HasValue && input.DiscountNameId.HasValue && input.ConceptId.HasValue))
                return periodDiscountDetail;

            var quey = (from x in _periodDiscountDetailRepository.GetAll()
                        join x2 in _periodDiscountRepository.GetAll()
                        on x.PeriodDiscountId equals x2.Id
                        where x.IsActive == true && x2.PeriodId == input.PeriodId && x2.ConceptId == input.ConceptId && x2.DiscountNameId == input.DiscountNameId
                        select x);

            var periodDiscountDetailList = await Task.Run(()=> { return quey.ToList();});

            periodDiscountDetail = ObjectMapper.Map<List<PeriodDiscountDetailDto>>(periodDiscountDetailList.ToList());

            return periodDiscountDetail;
        }

    }
};
//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using EasySchoolManager.Reports.Dto;

namespace EasySchoolManager.PrinterTypes.Dto
{
    [AutoMap(typeof(Models.PrinterTypes))]
    public class PrinterTypeDto : EntityDto<int>
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        public int ReportId { get; set; }

        public  _ReportDto Reports { get; set; }

    }
}
//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace EasySchoolManager.PrinterTypes.Dto
{
    [AutoMap(typeof(Models.PrinterTypes))]
    public class UpdatePrinterTypeDto : EntityDto<int>
    {

        [StringLength(25)]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        public int ReportId { get; set; }

    }
}
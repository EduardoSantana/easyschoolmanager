//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.PrinterTypes.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.PrinterTypes 
{ 
    [AbpAuthorize(PermissionNames.Pages_PrinterTypes)] 
    public class PrinterTypeAppService : AsyncCrudAppService<Models.PrinterTypes, PrinterTypeDto, int, PagedResultRequestDto, CreatePrinterTypeDto, UpdatePrinterTypeDto>, IPrinterTypeAppService 
    { 
        private readonly IRepository<Models.PrinterTypes, int> _printerTypeRepository;
		


        public PrinterTypeAppService( 
            IRepository<Models.PrinterTypes, int> repository, 
            IRepository<Models.PrinterTypes, int> printerTypeRepository 
            ) 
            : base(repository) 
        { 
            _printerTypeRepository = printerTypeRepository; 
			

			
        } 
        public override async Task<PrinterTypeDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<PrinterTypeDto> Create(CreatePrinterTypeDto input) 
        { 
            CheckCreatePermission(); 
            var printerType = ObjectMapper.Map<Models.PrinterTypes>(input); 
			try{
              await _printerTypeRepository.InsertAsync(printerType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(printerType); 
        } 
        public override async Task<PrinterTypeDto> Update(UpdatePrinterTypeDto input) 
        { 
            CheckUpdatePermission(); 
            var printerType = await _printerTypeRepository.GetAsync(input.Id);
            MapToEntity(input, printerType); 
		    try{
               await _printerTypeRepository.UpdateAsync(printerType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var printerType = await _printerTypeRepository.GetAsync(input.Id); 
               await _printerTypeRepository.DeleteAsync(printerType);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.PrinterTypes MapToEntity(CreatePrinterTypeDto createInput) 
        { 
            var printerType = ObjectMapper.Map<Models.PrinterTypes>(createInput); 
            return printerType; 
        } 
        protected override void MapToEntity(UpdatePrinterTypeDto input, Models.PrinterTypes printerType) 
        { 
            ObjectMapper.Map(input, printerType); 
        } 
        protected override IQueryable<Models.PrinterTypes> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.PrinterTypes> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.PrinterTypes> GetEntityByIdAsync(int id) 
        { 
            var printerType = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(printerType); 
        } 
        protected override IQueryable<Models.PrinterTypes> ApplySorting(IQueryable<Models.PrinterTypes> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<PrinterTypeDto>> GetAllPrinterTypes(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<PrinterTypeDto> ouput = new PagedResultDto<PrinterTypeDto>(); 
            IQueryable<Models.PrinterTypes> query = query = from x in _printerTypeRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _printerTypeRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<PrinterTypes.Dto.PrinterTypeDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<PrinterTypeDto>> GetAllPrinterTypesForCombo()
        {
            var printerTypeList = await _printerTypeRepository.GetAllListAsync(x => x.IsActive == true);

            var printerType = ObjectMapper.Map<List<PrinterTypeDto>>(printerTypeList.ToList());

            return printerType;
        }
		
    } 
} ;
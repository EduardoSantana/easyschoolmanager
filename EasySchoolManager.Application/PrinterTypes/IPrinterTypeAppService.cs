//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.PrinterTypes.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.PrinterTypes
{
      public interface IPrinterTypeAppService : IAsyncCrudAppService<PrinterTypeDto, int, PagedResultRequestDto, CreatePrinterTypeDto, UpdatePrinterTypeDto>
      {
            Task<PagedResultDto<PrinterTypeDto>> GetAllPrinterTypes(GdPagedResultRequestDto input);
			Task<List<Dto.PrinterTypeDto>> GetAllPrinterTypesForCombo();
      }
}
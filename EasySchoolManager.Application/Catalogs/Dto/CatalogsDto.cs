//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Catalogs.Dto 
{
        [AutoMap(typeof(Models.Catalogs))] 
        public class CatalogDto : EntityDto<int> 
        {
              public string AccountNumber {get;set;} 
              public string Name {get;set;} 
              public string Descsription {get;set;} 
              public string FullAccount { get; set; }
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;}
              public string Code1 { get; set; }
              public string Code10 { get; set; }
    }
}
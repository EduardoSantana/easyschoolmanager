//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace EasySchoolManager.Catalogs.Dto 
{
        [AutoMap(typeof(Models.Catalogs))] 
        public class UpdateCatalogDto : EntityDto<int> 
        {

              [StringLength(20)]  
              public string AccountNumber {get;set;} 

              [StringLength(85)]  
              public string Name {get;set;} 

              [StringLength(300)]  
              public string Descsription {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;}
              public string Code1 { get; set; }
              public string Code10 { get; set; }
    }
}
//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.Catalogs.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.Catalogs
{
    public interface ICatalogAppService : IAsyncCrudAppService<CatalogDto, int, PagedResultRequestDto, CreateCatalogDto, UpdateCatalogDto>
    {
        Task<PagedResultDto<CatalogDto>> GetAllCatalogs(GdPagedResultRequestDto input);
        Task<List<Dto.CatalogDto>> GetAllCatalogsForCombo();
        Task<List<CatalogDto>> GetAllCatalogsForComboForDiscounts(int PeriodId);
    }
}
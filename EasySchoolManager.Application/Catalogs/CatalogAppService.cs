//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.Catalogs.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.Catalogs
{
    [AbpAuthorize(PermissionNames.Pages_Catalogs)]
    public class CatalogAppService : AsyncCrudAppService<Models.Catalogs, CatalogDto, int, PagedResultRequestDto, CreateCatalogDto, UpdateCatalogDto>, ICatalogAppService
    {
        private readonly IRepository<Models.Catalogs, int> _catalogRepository;
        private readonly IRepository<Models.Concepts, int> _conceptRepository;



        public CatalogAppService(
            IRepository<Models.Catalogs, int> repository,
            IRepository<Models.Catalogs, int> catalogRepository,
            IRepository<Models.Concepts, int> conceptRepository
            )
            : base(repository)
        {
            _catalogRepository = catalogRepository;
            _conceptRepository = conceptRepository;



        }
        public override async Task<CatalogDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<CatalogDto> Create(CreateCatalogDto input)
        {
            CheckCreatePermission();
            var catalog = ObjectMapper.Map<Models.Catalogs>(input);
            try
            {
                try
                {
                    _catalogRepository.Insert(catalog);

                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception err)
                {
                    throw new UserFriendlyException(err.Message);
                }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(catalog);
        }


        public override async Task<CatalogDto> Update(UpdateCatalogDto input)
        {
            CheckUpdatePermission();
            var catalog = await _catalogRepository.GetAsync(input.Id);
            MapToEntity(input, catalog);
            try
            {
                await _catalogRepository.UpdateAsync(catalog);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var catalog = await _catalogRepository.GetAsync(input.Id);
                await _catalogRepository.DeleteAsync(catalog);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        protected override Models.Catalogs MapToEntity(CreateCatalogDto createInput)
        {
            var catalog = ObjectMapper.Map<Models.Catalogs>(createInput);
            return catalog;
        }
        protected override void MapToEntity(UpdateCatalogDto input, Models.Catalogs catalog)
        {
            ObjectMapper.Map(input, catalog);
        }
        protected override IQueryable<Models.Catalogs> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAll();
        }
        protected IQueryable<Models.Catalogs> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input)
        {
            var resultado = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.TextFilter))
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter));
            return resultado;
        }
        protected override async Task<Models.Catalogs> GetEntityByIdAsync(int id)
        {
            var catalog = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(catalog);
        }
        protected override IQueryable<Models.Catalogs> ApplySorting(IQueryable<Models.Catalogs> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.Name);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<CatalogDto>> GetAllCatalogs(GdPagedResultRequestDto input)
        {
            PagedResultDto<CatalogDto> ouput = new PagedResultDto<CatalogDto>();
            IQueryable<Models.Catalogs> query = query = from x in _catalogRepository.GetAll()
                                                        select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _catalogRepository.GetAll()
                        where x.Name.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<Catalogs.Dto.CatalogDto>>(query.ToList());
            return ouput;
        }

        [AbpAllowAnonymous]
        public async Task<List<CatalogDto>> GetAllCatalogsForCombo()
        {
            var catalogList = await _catalogRepository.GetAllListAsync(x => x.IsActive == true);

            var catalog = ObjectMapper.Map<List<CatalogDto>>(catalogList.ToList());

            return catalog;
        }



        [AbpAllowAnonymous]
        public async Task<List<CatalogDto>> GetAllCatalogsForComboForDiscounts(int PeriodId)
        {
            var catalogList = new List<CatalogDto>();
            try
            {
                var quey = (from x in _catalogRepository.GetAll()
                            join x2 in _conceptRepository.GetAll()
                            on x.Id equals x2.CatalogId
                            where x2.AvailableForDiscounts == true && x2.PeriodId == PeriodId
                            select x);

                var catalogs = await Task.Run(() => { return quey.ToList(); });

                catalogList = ObjectMapper.Map<List<CatalogDto>>(catalogs.ToList());
            }
            catch 
            {
            }
            return catalogList;
        }

    }
};
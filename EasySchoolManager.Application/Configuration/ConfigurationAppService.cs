﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using EasySchoolManager.Configuration.Dto;

namespace EasySchoolManager.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : EasySchoolManagerAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}

﻿using System.Threading.Tasks;
using Abp.Application.Services;
using EasySchoolManager.Configuration.Dto;

namespace EasySchoolManager.Configuration
{
    public interface IConfigurationAppService: IApplicationService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
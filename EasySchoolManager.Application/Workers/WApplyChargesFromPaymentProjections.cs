﻿using Abp.Dependency;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using EasySchoolManager.Transactions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Workers
{
    public class WApplyChargesFromPaymentProjections : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        public readonly ITransactionAppService _transactionService;

        public WApplyChargesFromPaymentProjections(AbpTimer timer, ITransactionAppService transactionService) : base(timer)
        {
            try
            {
                Timer.Period = Convert.ToInt32(ConfigurationManager.AppSettings["WApplyChargesFromPaymentProjections"]);
            }
            catch (Exception) { }
            if (Timer.Period < 1)
                Timer.Period = 2000;
            _transactionService = transactionService;
        }

        protected override void DoWork()
        {
            _transactionService.ApplyExpiredPaymentProjections();
        }
    }
}

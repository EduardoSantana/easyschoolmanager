﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using EasySchoolManager.RncFileDownload;
using System;
using System.Configuration;
using System.Linq;

namespace EasySchoolManager.Workers
{
    public class WDownloadDgiiRncFile : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private int WorkerId = 1;
        private readonly IRepository<Models.ApplicationDates, int> _applicationDatesRepository;

        public WDownloadDgiiRncFile(AbpTimer timer, IRepository<Models.ApplicationDates, int> applicationDatesRepository) : base(timer)
        {
            try
            {
                Timer.Period = Convert.ToInt32(ConfigurationManager.AppSettings["WDownloadDgiiRncFile"]);
            }
            catch (Exception) { }
            if (Timer.Period < 1)
                Timer.Period = 86400000;
            _applicationDatesRepository = applicationDatesRepository;
        }

        protected override void DoWork()
        {
            var dt = DateTime.Now;
            var CurrentDateInit = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
            var CurrentDateEnd = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
            var appDates = _applicationDatesRepository
                .GetAllList(x => x.WorkerId == WorkerId)
                .OrderByDescending(x => x.Id)
                .Select(x => new {
                    LastModifiedDate = x.LastModifiedDate,
                    StopDoWork = (x.ApplicationDate >= CurrentDateInit && x.ApplicationDate <= CurrentDateEnd) })
                 .FirstOrDefault();

            //Si aparece ya un registro de applicationDates entonces 
            if (appDates.StopDoWork)
                return;

            var rncDownload = new LoadFileRNCWorker();
            var newAppDate = new Models.ApplicationDates();
            newAppDate.LastModifiedDate = rncDownload.LoadFiles(appDates.LastModifiedDate.Value); 
            newAppDate.CreationTime = DateTime.Now;
            newAppDate.ApplicationDate = dt;
            newAppDate.WorkerId = WorkerId;
            newAppDate.IsActive = true;
            newAppDate.IsDeleted = false;
            _applicationDatesRepository.Insert(newAppDate);
            CurrentUnitOfWork.SaveChanges();
        }
    }
}

﻿using Abp.Dependency;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using EasySchoolManager.TransactionQueues;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Workers
{
    public class WSendTransactionQueues : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        public readonly ITransactionQueueAppService _transactionQueueService;
                        
        public WSendTransactionQueues(AbpTimer timer, ITransactionQueueAppService transactionService) : base(timer)
        {
            try
            {
                Timer.Period = Convert.ToInt32(ConfigurationManager.AppSettings["WSendTransactionQueues"]);
            }
            catch (Exception) { }
            if (Timer.Period < 1)
                Timer.Period = 2000;
            _transactionQueueService = transactionService;
        }

        protected override void DoWork()
        {
            _transactionQueueService.TryToSend();
        }
    }
}

﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.TransactionStudents.Dto
{
    [AutoMap(typeof(Models.TransactionStudents))]
    public class TransactionStudentDto
    {
        public long TransactionId { get; set; }

        public int TransactionTypeId { get; set; }

        public int EnrollmentStudentId { get; set; }

        public decimal Amount { get; set; }
        
       // public EnrollmentStudents.Dto.EnrollmentStudentDto EnrollmentStudents { get; set; }

       // public Transactions.Dto.TransactionDto Transactions { get; set; }
    }
}

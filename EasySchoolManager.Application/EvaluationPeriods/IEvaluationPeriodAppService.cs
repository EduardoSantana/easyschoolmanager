//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.EvaluationPeriods.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.EvaluationPeriods
{
      public interface IEvaluationPeriodAppService : IAsyncCrudAppService<EvaluationPeriodDto, int, PagedResultRequestDto, CreateEvaluationPeriodDto, UpdateEvaluationPeriodDto>
      {
            Task<PagedResultDto<EvaluationPeriodDto>> GetAllEvaluationPeriods(GdPagedResultRequestDto input);
			Task<List<Dto.EvaluationPeriodDto>> GetAllEvaluationPeriodsForCombo();
      }
}
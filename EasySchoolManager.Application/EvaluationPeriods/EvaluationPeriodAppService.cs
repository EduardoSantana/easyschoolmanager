//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.EvaluationPeriods.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.EvaluationPeriods 
{ 
    [AbpAuthorize(PermissionNames.Pages_EvaluationPeriods)] 
    public class EvaluationPeriodAppService : AsyncCrudAppService<Models.EvaluationPeriods, EvaluationPeriodDto, int, PagedResultRequestDto, CreateEvaluationPeriodDto, UpdateEvaluationPeriodDto>, IEvaluationPeriodAppService 
    { 
        private readonly IRepository<Models.EvaluationPeriods, int> _evaluationPeriodRepository;
		
		    private readonly IRepository<Models.Periods, int> _periodRepository;


        public EvaluationPeriodAppService( 
            IRepository<Models.EvaluationPeriods, int> repository, 
            IRepository<Models.EvaluationPeriods, int> evaluationPeriodRepository ,
            IRepository<Models.Periods, int> periodRepository

            ) 
            : base(repository) 
        { 
            _evaluationPeriodRepository = evaluationPeriodRepository; 
			
            _periodRepository = periodRepository;


			
        } 
        public override async Task<EvaluationPeriodDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<EvaluationPeriodDto> Create(CreateEvaluationPeriodDto input) 
        { 
            CheckCreatePermission(); 
            var evaluationPeriod = ObjectMapper.Map<Models.EvaluationPeriods>(input); 
			try{
              await _evaluationPeriodRepository.InsertAsync(evaluationPeriod); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(evaluationPeriod); 
        } 
        public override async Task<EvaluationPeriodDto> Update(UpdateEvaluationPeriodDto input) 
        { 
            CheckUpdatePermission(); 
            var evaluationPeriod = await _evaluationPeriodRepository.GetAsync(input.Id);
            MapToEntity(input, evaluationPeriod); 
		    try{
               await _evaluationPeriodRepository.UpdateAsync(evaluationPeriod); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var evaluationPeriod = await _evaluationPeriodRepository.GetAsync(input.Id); 
               await _evaluationPeriodRepository.DeleteAsync(evaluationPeriod);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.EvaluationPeriods MapToEntity(CreateEvaluationPeriodDto createInput) 
        { 
            var evaluationPeriod = ObjectMapper.Map<Models.EvaluationPeriods>(createInput); 
            return evaluationPeriod; 
        } 
        protected override void MapToEntity(UpdateEvaluationPeriodDto input, Models.EvaluationPeriods evaluationPeriod) 
        { 
            ObjectMapper.Map(input, evaluationPeriod); 
        } 
        protected override IQueryable<Models.EvaluationPeriods> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.EvaluationPeriods> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.EvaluationPeriods> GetEntityByIdAsync(int id) 
        { 
            var evaluationPeriod = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(evaluationPeriod); 
        } 
        protected override IQueryable<Models.EvaluationPeriods> ApplySorting(IQueryable<Models.EvaluationPeriods> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<EvaluationPeriodDto>> GetAllEvaluationPeriods(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<EvaluationPeriodDto> ouput = new PagedResultDto<EvaluationPeriodDto>(); 
            IQueryable<Models.EvaluationPeriods> query = query = from x in _evaluationPeriodRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _evaluationPeriodRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<EvaluationPeriods.Dto.EvaluationPeriodDto>>(query.ToList()); 
            return ouput; 
        } 
		
		[AbpAllowAnonymous]
		public async Task<List<EvaluationPeriodDto>> GetAllEvaluationPeriodsForCombo()
        {
            var evaluationPeriodList = await _evaluationPeriodRepository.GetAllListAsync(x => x.IsActive == true);

            var evaluationPeriod = ObjectMapper.Map<List<EvaluationPeriodDto>>(evaluationPeriodList.ToList());

            return evaluationPeriod;
        }
		
    } 
} ;
//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper;
using EasySchoolManager.Periods.Dto;

namespace EasySchoolManager.EvaluationPeriods.Dto 
{
        [AutoMap(typeof(Models.EvaluationPeriods))] 
        public class EvaluationPeriodDto : EntityDto<int> 
        {
              public string Name {get;set;} 
              public int Position {get;set;} 
              public int InitialMonth {get;set;} 
              public int FinalMonth {get;set;} 
              public int PeriodId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 
              public PeriodDto Periods { get; set; }
         }
}
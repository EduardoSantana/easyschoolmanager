//Created from Templaste MG

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using EasySchoolManager.Authorization;
using Microsoft.AspNet.Identity;
using EasySchoolManager.ReportsFilters.Dto;
using System.Collections.Generic;
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.ReportsFilters
{
    [AbpAuthorize(PermissionNames.Pages_ReportsFilters)]
    public class ReportsFilterAppService : AsyncCrudAppService<Models.ReportsFilters, _ReportsFiltersDto, int, PagedResultRequestDto, CreateReportsFilterDto, UpdateReportsFilterDto>, IReportsFilterAppService
    {
        private readonly IRepository<Models.ReportsFilters, int> _reportsFilterRepository;

        public ReportsFilterAppService(
            IRepository<Models.ReportsFilters, int> repository,
            IRepository<Models.ReportsFilters, int> reportsFilterRepository
            )
            : base(repository)
        {
            _reportsFilterRepository = reportsFilterRepository;
        }
        public override async Task<_ReportsFiltersDto> Get(EntityDto<int> input)
        {
            var user = await base.Get(input);
            return user;
        }
        public override async Task<_ReportsFiltersDto> Create(CreateReportsFilterDto input)
        {
            CheckCreatePermission();
            var reportsFilter = ObjectMapper.Map<Models.ReportsFilters>(input);
            try
            {
                reportsFilter.IsActive = true;
                await _reportsFilterRepository.InsertAsync(reportsFilter);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(reportsFilter);
        }

        public override async Task<_ReportsFiltersDto> Update(UpdateReportsFilterDto input)
        {
            CheckUpdatePermission();
            var reportsFilter = await _reportsFilterRepository.GetAsync(input.Id);
            MapToEntity(input, reportsFilter);
            try
            {
                await _reportsFilterRepository.UpdateAsync(reportsFilter);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input);
        }
        public override async Task Delete(EntityDto<int> input)
        {
            try
            {
                var reportsFilter = await _reportsFilterRepository.GetAsync(input.Id);
                await _reportsFilterRepository.DeleteAsync(reportsFilter);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }
        
        protected override async Task<Models.ReportsFilters> GetEntityByIdAsync(int id)
        {
            var reportsFilter = Repository.GetAllList().FirstOrDefault(x => x.Id == id);
            return await Task.FromResult(reportsFilter);
        }
  
        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<PagedResultDto<_ReportsFiltersDto>> GetAllReportsFilters(GdPagedResultRequestDto input)
        {
            PagedResultDto<_ReportsFiltersDto> ouput = new PagedResultDto<_ReportsFiltersDto>();
            IQueryable<Models.ReportsFilters> query = query = from x in _reportsFilterRepository.GetAll()
                                                              select x;
            if (!string.IsNullOrEmpty(input.TextFilter))
            {
                query = from x in _reportsFilterRepository.GetAll()
                        where x.DisplayName.Contains(input.TextFilter)
                        select x;
            }
            ouput.TotalCount = await Task.Run(() => { return query.Count(); });
            if (input.SkipCount > 0)
            {
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount);
            }
            if (input.MaxResultCount > 0)
            {
                query = query.Take(input.MaxResultCount);
            }
            ouput.Items = ObjectMapper.Map<List<ReportsFilters.Dto._ReportsFiltersDto>>(query.ToList());
            return ouput;
        }

        public async Task<List<_ReportsFiltersDto>> GetAllReportsFilterByReportId(FilterInputDto input)
        {
            input.TextFilter = input.TextFilter ?? "";
            List<Models.ReportsFilters> query = await _reportsFilterRepository
                .GetAllListAsync(t => t.IsActive == true && t.ReportsId == input.Id && (t.Name.Contains(input.TextFilter) || t.UrlService.Contains(input.TextFilter) ));
            return ObjectMapper.Map<List<_ReportsFiltersDto>>(query);
        }

        [AbpAllowAnonymous]
        public async Task<List<_ReportsFiltersDto>> GetAllReportsFiltersForCombo()
        {
            var reportsFilterList = await _reportsFilterRepository.GetAllListAsync(x => x.IsActive == true);

            var reportsFilter = ObjectMapper.Map<List<_ReportsFiltersDto>>(reportsFilterList.ToList());

            return reportsFilter;
        }

    }
};
﻿using Abp.AutoMapper;

namespace EasySchoolManager.ReportsFilters.Dto
{
    [AutoMap(typeof(Models.DataType))]
    public class DataTypeDto
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public string Description { get; set; }
        public string TypeHTML { get; set; }
    }
}
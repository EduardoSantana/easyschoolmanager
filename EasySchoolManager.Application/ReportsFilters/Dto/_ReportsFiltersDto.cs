using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace EasySchoolManager.ReportsFilters.Dto
{
    [AutoMap(typeof(Models.ReportsFilters))]
    public class _ReportsFiltersDto : EntityDto<int>
    {
        public int ReportsId { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string DataField { get; set; }
        public int DataTypeId { get; set; }
        public bool Range { get; set; }
        public bool OnlyParameter { get; set; }
        public string UrlService { get; set; }
        public string FieldService { get; set; }
        public string DisplayNameService { get; set; }
        public string DependencyField { get; set; }
        public string Operator { get; set; }
        public string DisplayNameRange { get; set; }
        public bool Required { get; set; }
        public bool IsActive { get; set; }

        [AutoMapper.IgnoreMap]
        public string Value { get; set; }

        [AutoMapper.IgnoreMap]
        public string RangeValue { get; set; }

        [AutoMapper.IgnoreMap]
        public string ValueText { get; set; }

        public virtual DataTypeDto DataType { get; set; }
        public string DefaultValue { get; set; }
        public string DefaultValueComputed
        {
            get
            {
                if (!string.IsNullOrEmpty(DefaultValue) && DefaultValue.Contains("#"))
                    return DefaultValue.Replace("#", "").Trim();
                return "";
            }
        }
    }
}
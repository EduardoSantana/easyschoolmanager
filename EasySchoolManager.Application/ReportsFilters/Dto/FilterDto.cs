﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.ReportsFilters.Dto
{
  public  class FilterInputDto
    {
        public int Id { get; set; }
        public string TextFilter { get; set; }
    }
}

//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;
using EasySchoolManager.ReportsQueries.Dto;

namespace EasySchoolManager.ReportsFilters.Dto
{
    [AutoMap(typeof(Models.ReportsFilters))]
    public class UpdateReportsFilterDto : EntityDto<int>
    {
        public int ReportsId { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string DataField { get; set; }
        public int DataTypeId { get; set; }
        public bool Range { get; set; }
        public bool OnlyParameter { get; set; }
        public string UrlService { get; set; }
        public string FieldService { get; set; }
        public string DisplayNameService { get; set; }
        public string DependencyField { get; set; }
        public string Operator { get; set; }
        public string DisplayNameRange { get; set; }
        public bool Required { get; set; }
        public bool IsActive { get; set; }
        public string DefaultValue { get; set; }
    }
}
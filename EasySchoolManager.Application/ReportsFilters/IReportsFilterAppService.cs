//Created from Templaste MG

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.ReportsFilters.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.ReportsFilters
{
    public interface IReportsFilterAppService : IAsyncCrudAppService<_ReportsFiltersDto, int, PagedResultRequestDto, CreateReportsFilterDto, UpdateReportsFilterDto>
    {
        Task<PagedResultDto<_ReportsFiltersDto>> GetAllReportsFilters(GdPagedResultRequestDto input);
        Task<List<Dto._ReportsFiltersDto>> GetAllReportsFiltersForCombo();
        Task<List<_ReportsFiltersDto>> GetAllReportsFilterByReportId(FilterInputDto input);
    }
}
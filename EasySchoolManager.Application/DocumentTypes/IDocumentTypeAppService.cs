//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EasySchoolManager.GD;
using EasySchoolManager.DocumentTypes.Dto;
using System.Collections.Generic;

namespace EasySchoolManager.DocumentTypes
{
      public interface IDocumentTypeAppService : IAsyncCrudAppService<DocumentTypeDto, int, PagedResultRequestDto, CreateDocumentTypeDto, UpdateDocumentTypeDto>
      {
            Task<PagedResultDto<DocumentTypeDto>> GetAllDocumentTypes(GdPagedResultRequestDto input);
			Task<List<Dto.DocumentTypeDto>> GetAllDocumentTypesForCombo();
      }
}
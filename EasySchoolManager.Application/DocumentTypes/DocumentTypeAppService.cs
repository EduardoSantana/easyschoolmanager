//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using EasySchoolManager.Authorization; 
using Microsoft.AspNet.Identity; 
using EasySchoolManager.DocumentTypes.Dto; 
using System.Collections.Generic; 
using EasySchoolManager.GD;
using Abp.UI;
using System;

namespace EasySchoolManager.DocumentTypes 
{ 
    [AbpAuthorize(PermissionNames.Pages_DocumentTypes)] 
    public class DocumentTypeAppService : AsyncCrudAppService<Models.DocumentTypes, DocumentTypeDto, int, PagedResultRequestDto, CreateDocumentTypeDto, UpdateDocumentTypeDto>, IDocumentTypeAppService 
    { 
        private readonly IRepository<Models.DocumentTypes, int> _documentTypeRepository;
		


        public DocumentTypeAppService( 
            IRepository<Models.DocumentTypes, int> repository, 
            IRepository<Models.DocumentTypes, int> documentTypeRepository 
            ) 
            : base(repository) 
        { 
            _documentTypeRepository = documentTypeRepository; 
			

			
        } 
        public override async Task<DocumentTypeDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<DocumentTypeDto> Create(CreateDocumentTypeDto input) 
        { 
            CheckCreatePermission(); 
            var documentType = ObjectMapper.Map<Models.DocumentTypes>(input); 
			try{
              await _documentTypeRepository.InsertAsync(documentType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return MapToEntityDto(documentType); 
        } 
        public override async Task<DocumentTypeDto> Update(UpdateDocumentTypeDto input) 
        { 
            CheckUpdatePermission(); 
            var documentType = await _documentTypeRepository.GetAsync(input.Id);
            MapToEntity(input, documentType); 
		    try{
               await _documentTypeRepository.UpdateAsync(documentType); 
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        {
			try{
               var documentType = await _documentTypeRepository.GetAsync(input.Id); 
               await _documentTypeRepository.DeleteAsync(documentType);
			}
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }			
        } 
        protected override Models.DocumentTypes MapToEntity(CreateDocumentTypeDto createInput) 
        { 
            var documentType = ObjectMapper.Map<Models.DocumentTypes>(createInput); 
            return documentType; 
        } 
        protected override void MapToEntity(UpdateDocumentTypeDto input, Models.DocumentTypes documentType) 
        { 
            ObjectMapper.Map(input, documentType); 
        } 
        protected override IQueryable<Models.DocumentTypes> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.DocumentTypes> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Name.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.DocumentTypes> GetEntityByIdAsync(int id) 
        { 
            var documentType = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(documentType); 
        } 
        protected override IQueryable<Models.DocumentTypes> ApplySorting(IQueryable<Models.DocumentTypes> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Name); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<DocumentTypeDto>> GetAllDocumentTypes(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<DocumentTypeDto> ouput = new PagedResultDto<DocumentTypeDto>(); 
            IQueryable<Models.DocumentTypes> query = query = from x in _documentTypeRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _documentTypeRepository.GetAll() 
                        where x.Name.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<DocumentTypes.Dto.DocumentTypeDto>>(query.ToList()); 
            return ouput; 
        }

        [AbpAllowAnonymous]
        public async Task<List<DocumentTypeDto>> GetAllDocumentTypesForCombo()
        {
            var documentTypeList = await _documentTypeRepository.GetAllListAsync(x => x.IsActive == true);

            var documentType = ObjectMapper.Map<List<DocumentTypeDto>>(documentTypeList.ToList());

            return documentType;
        }
		
    } 
} ;
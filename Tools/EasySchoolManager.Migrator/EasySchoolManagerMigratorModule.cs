using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using EasySchoolManager.EntityFramework;

namespace EasySchoolManager.Migrator
{
    [DependsOn(typeof(EasySchoolManagerDataModule))]
    public class EasySchoolManagerMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<EasySchoolManagerDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
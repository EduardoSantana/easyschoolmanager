﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Reports : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [MaxLength(80)]
        public string ReportCode { get; set; }
        [Required]
        [MaxLength(100)]
        public string ReportName { get; set; }
        [Required]
        public string View { get; set; }
        [Required]
        [MaxLength(100)]
        public string ReportRoot { get; set; }
        [Required]
        [MaxLength(500)]
        public string ReportPath { get; set; }

        public int ReportsTypesId { get; set; } 
        public int ReportDataSourceId { get; set; }
        public bool FilterByTenant { get; set; }
        public int IsForTenant { get; set; }
        [ForeignKey("ReportsTypesId")]
        public virtual ReportsTypes ReportsTypes { get; set; }

        public string PermissionName { get; set; }

        [ForeignKey("ReportDataSourceId")]
        public virtual ReportDataSources ReportDataSource { get; set; }

        public virtual ICollection<ReportsFilters> ReportsFilters { get; set; }
    }
}

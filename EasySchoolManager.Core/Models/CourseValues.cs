﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class CourseValues : GD.GdEntityWithTenant<int>
    {
        public int PeriodId { get; set; }

        public int CourseId { get; set; }

        public Decimal InscriptionAmount { get; set; }
        public Decimal TotalAmount { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }

        [ForeignKey("CourseId")]
        public virtual Courses Courses { get; set; }
    }
}

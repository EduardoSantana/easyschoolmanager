﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class TaxReceipts : GD.GdEntityWithoutTenant<int>
    {
        [Index("IX_TaxReceiptTypeRegionBase", 1, IsUnique = true)]
        [Required(ErrorMessage ="Debe seleccionar el tipo de comprobante fiscal")]
        public int TaxReceiptTypeId { get; set; }

        [Required(ErrorMessage ="Debe indicar un nombre para el comprobante fiscal")]
        [MaxLength(50)]
        public string Name { get; set; }

        [DefaultValue(1)]
        public long InitialNumber { get; set; }
        public long FinalNumber { get; set; }
        public long CurrentSequence { get; set; }

        [Index("IX_TaxReceiptTypeRegionBase", 2, IsUnique = true)]
        [Required(ErrorMessage = "Debe seleccionar la region a la cual pertenece el comprobante fiscal")]
        public int RegionId { get; set; }
        
        [Required(ErrorMessage ="Debe indicar la serie del comprobante fiscal")]
        [MaxLength(1)]
        public string Serie { get; set; }


        [Index("IX_TaxReceiptTypeRegionBase", 3, IsUnique = true)]
        public DateTime ExpirationDate { get; set; }

        [ForeignKey("RegionId")]
        public virtual Regions Regions { get; set; }

        [ForeignKey("TaxReceiptTypeId")]
        public virtual TaxReceiptTypes TaxReceiptTypes { get; set; }

    }
}

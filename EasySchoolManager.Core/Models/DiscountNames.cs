﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class DiscountNames : GD.GdEntityWithoutTenant<int>
    {
        public DiscountNames()
        {
            PeriodDiscounts = new HashSet<PeriodDiscounts>();
        }

        [Required]
        [StringLength(30)]
        [Index("IX_DiscountNamesName", 0, IsUnique = true)]
        public string Name { get; set; }

        public bool HasGrantEnterprise { get; set; }

        public virtual ICollection<PeriodDiscounts> PeriodDiscounts { get; set; }
    }
}

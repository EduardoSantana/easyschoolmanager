﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class Purchases : GD.GdEntityWithoutTenant<int>
    {
        [StringLength(25)]
        public string Document { get; set; }

        public int ProviderId { get; set; }

        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        [StringLength(250)]
        public string Comnent1 { get; set; }

        [StringLength(250)]
        public string Comment2 { get; set; }

        [ForeignKey("ProviderId")]
        public virtual Providers Providers { get; set; }

    }
}
namespace EasySchoolManager.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class TransactionTypes : GD.GdEntityWithoutTenant<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TransactionTypes()
        {
            Transactions = new HashSet<Transactions>();
        }
        
        [Required]
        [Index("IX_TransactionTypesName", 1, IsUnique = true)]
        [StringLength(25)]
        public string Name { get; set; }

        [StringLength(3)]
        public string Abbreviation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Transactions> Transactions { get; set; }
    }
}

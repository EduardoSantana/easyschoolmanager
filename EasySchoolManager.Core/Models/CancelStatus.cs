namespace EasySchoolManager.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class CancelStatus : GD.GdEntityWithoutTenant<int>
    {
        public CancelStatus()
        {
            Transactions = new HashSet<Transactions>();
        }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }

        public virtual ICollection<Transactions> Transactions { get; set; }

    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class PeriodDiscounts : GD.GdEntityWithoutTenant<int>
    {
        public PeriodDiscounts()
        {
            PeriodDiscountDetails = new HashSet<PeriodDiscountDetails>();
            DiscountNameTenants = new HashSet<DiscountNameTenants>();
        }

        public virtual ICollection<PeriodDiscountDetails> PeriodDiscountDetails { get; set; }

        [Required]
        [Index("IX_PeriodDiscountsPeriodNamePeriod", 0, IsUnique = true)]
        public int DiscountNameId { get; set; }

        [Required]
        [Index("IX_PeriodDiscountsPeriodNamePeriod", 1, IsUnique = true)]
        public int PeriodId { get; set; }

        [Required]
        [Index("IX_PeriodDiscountsPeriodNamePeriod", 2, IsUnique = true)]
        public int ConceptId { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }

        [ForeignKey("ConceptId")]
        public virtual Concepts Concepts { get; set; }

        [ForeignKey("DiscountNameId")]
        public virtual DiscountNames DiscountNames { get; set; }

        public virtual ICollection<DiscountNameTenants> DiscountNameTenants { get; set; }

    }
}

﻿using EasySchoolManager.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class MessageRead : EasySchoolManager.GD.GdEntityWithoutTenant<long>
    {
        public long MessageId { get; set; }
        public long UserId { get; set; }

        [ForeignKey("MessageId")]
        public Messages Message { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}

﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class TransactionShops : GD.GdEntityWithTenant<long>
    {
        public TransactionShops()
        {
            TransactionArticleShops = new HashSet<TransactionArticleShops>();
        }

        public int Sequence { get; set; }

        [StringLength(20)]
        public string Number { get; set; }

        public DateTime Date { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public decimal Amount { get; set; }

        public int ClientShopId { get; set; }

        public int? PaymentMethodId { get; set; }

        public int OriginId { get; set; }

        public int TransactionTypeId { get; set; }

        [ForeignKey("ClientShopId")]
        public virtual ClientShops ClientShops { get; set; }

        [ForeignKey("OriginId")]
        public virtual Origins Origins { get; set; }

        [ForeignKey("PaymentMethodId")]
        public virtual PaymentMethods PaymentMethods { get; set; }

        [ForeignKey("TransactionTypeId")]
        public virtual TransactionTypes TransactionTypes { get; set; }

        public virtual ICollection<TransactionArticleShops> TransactionArticleShops { get; set; }

    }
}

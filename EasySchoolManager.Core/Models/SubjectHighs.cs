﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace EasySchoolManager.Models
{

    /// <summary>
    /// Para registrar las difirentes materias de una cursos del nivel medio
    /// </summary>
    public partial class SubjectHighs : GD.GdEntityWithoutTenant<int>
    {

        [Required]
        [StringLength(80)]
        public string Name { get; set; }

        [Required]
        [StringLength(350)]
        public string LongName { get; set; }

        [Required]
        [StringLength(20)]
        public string Code { get; set; }

        public int CourseId { get; set; }

        [ForeignKey("CourseId")]
        public virtual Courses Courses { get; set; }

    }
}
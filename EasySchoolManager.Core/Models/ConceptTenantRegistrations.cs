﻿using EasySchoolManager.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class ConceptTenantRegistrations : GD.GdEntityWithTenant<int>
    {
        public int ConceptId { get; set; }

        [ForeignKey("ConceptId")]
        public virtual Concepts Concepts { set; get; }

        [ForeignKey("TenantId")]
        public virtual MultiTenancy.Tenant Tenant { get; set; }
    }
}

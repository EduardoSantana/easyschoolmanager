namespace EasySchoolManager.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    //Usata para tener las sugerencia de los pagos que realizaran los tutores, es decir, la fecha
    //en que deben ser pagadas las mensualidades.
    public partial class PaymentDates : GD.GdEntityWithTenant<int>
    {
        public int Sequence { get; set; }
        public int PaymentDay { get; set; }
        public int PaymentMonth { get; set; }
        public int PaymentYear{ get; set; }
    }
}

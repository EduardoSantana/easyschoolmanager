﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class UserTypes : EasySchoolManager.GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [MaxLength(100)]
        [Index(IsUnique = true)]
        public string Description { get; set; }
    }
}

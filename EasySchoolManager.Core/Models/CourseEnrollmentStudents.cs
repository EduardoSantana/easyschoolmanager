using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class CourseEnrollmentStudents : GD.GdEntityWithTenant<int>
    {
        public int CourseId { get; set; }

        public int EnrollmentStudentId { get; set; }

        public int? SessionId { get; set; }

        public int? Sequence { get; set; }

        [ForeignKey("CourseId")]
        public virtual Courses Courses { get; set; }

        [ForeignKey("EnrollmentStudentId")]
        public virtual EnrollmentStudents EnrollmentStudents { get; set; }

        [ForeignKey("SessionId")]
        public virtual CourseSessions Sessions { get; set; }
    }
}

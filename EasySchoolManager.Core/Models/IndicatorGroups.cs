﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    /// <summary>
    /// Contiene todos los grupos de indicadores, los cuales tienen hijos a su vez, y los hijos son los que reciben los indicadores
    /// </summary>
    public class IndicatorGroups : GD.GdEntityWithoutTenant<int>
    {
        public IndicatorGroups()
        {
            Indicators = new HashSet<Indicators>();
            ChildIndicatorGrupos = new HashSet<IndicatorGroups>();
        }

        [StringLength(200)]
        public string Name { get; set; }
        public int SubjectId { get; set; }
        public int? ParentIndicatorGroupId { get; set; }

        [ForeignKey("SubjectId")]
        public virtual Subjects Subjects { get; set; }

        [ForeignKey("ParentIndicatorGroupId")]
        public virtual IndicatorGroups ParentIndicatorGroups { get; set; }

        public virtual ICollection<Indicators> Indicators { get; set; }

        public virtual ICollection<IndicatorGroups> ChildIndicatorGrupos { get; set; }

    }
}

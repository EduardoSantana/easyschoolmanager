﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    /// <summary>
    /// Esta tabla es usada para poner por periodo las posicioes de las materias, estas posiciones son las que haran que las materias
    /// se muestren en la posicion adecuada en el reporte acta final de calificaciones
    /// </summary>
    public class CourseSubjectPositions : GD.GdEntityWithoutTenant<int>
    {
        public int CourseId { get; set; }
        public int SubjectHighId { get; set; }
        public int PeriodId { get; set; }
        public int Position { get; set; }



        [ForeignKey("SubjectHighId")]
        public virtual SubjectHighs SubjectHighs { get; set; }

        [ForeignKey("CourseId")]
        public virtual Courses Courses { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }
    }
}

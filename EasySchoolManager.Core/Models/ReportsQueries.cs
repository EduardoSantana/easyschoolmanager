﻿namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ReportsQueries : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [MaxLength(20)]
        public string ValueId { get; set; }
        [Required]
        [MaxLength(40)]
        public string ValueDisplay { get; set; }
        [Required]
        public int ReportsFiltersId { get; set; }

        [ForeignKey("ReportsFiltersId")]
        public virtual ReportsFilters ReportsFilters { get; set; }

    }
}

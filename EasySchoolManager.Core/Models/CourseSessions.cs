﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class CourseSessions: GD.GdEntityWithoutTenant<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CourseSessions()
        {
            CourseEnrollmentStudents = new HashSet<CourseEnrollmentStudents>();
        }
        
        [Index("IX_CourseSessions", 2, IsUnique = true)]
        public int CourseId { get; set; }

        [Required]
        [StringLength(1)]
        [Index("IX_CourseSessions", 1, IsUnique = true)]
        public string Name { get; set; }


        [ForeignKey("CourseId")]
        public virtual Courses Courses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CourseEnrollmentStudents> CourseEnrollmentStudents { get; set; }
    }
}

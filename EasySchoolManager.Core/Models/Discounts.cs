﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class Discounts: GD.GdEntityWithTenant<int>
    {
        public int StudentId { get; set; }
        
        public int PeriodId { get; set; }
        
        public int ConceptId { get; set; }
        
        public int PeriodDiscountDetailId { get; set; }
        
        [ForeignKey("ConceptId")]
        public virtual Concepts Concepts { get; set; }

        [ForeignKey("StudentId")]
        public virtual Students Students { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }

        [ForeignKey("PeriodDiscountDetailId")]
        public virtual PeriodDiscountDetails PeriodDiscountDetails { get; set; }
        
    }
}

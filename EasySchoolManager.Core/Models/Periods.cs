namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    //Corresponde al a�o escolar 
    public partial class Periods : GD.GdEntityWithoutTenant<int>
    {
        public Periods()
        {
            MonthlyPeriods = new HashSet<MonthlyPeriods>();
        }

        [Required]
        [StringLength(5)]
        public string Period { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        [Column(TypeName = "date")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime EndDate { get; set; }
        
        public virtual ICollection<MonthlyPeriods> MonthlyPeriods { get; set; }
    }
}

namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Blood")]
    public partial class Bloods : GD.GdEntityWithoutTenant<int>
    {
        [Index("IX_BloodsName", 1, IsUnique = true)]
        [StringLength(3)]
        public string Name { get; set; }
    }
}

﻿using EasySchoolManager.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class Messages : EasySchoolManager.GD.GdEntityWithoutTenant<long>
    {
        public long? DestinationUserId { get; set; }
        public int? TenantId { get; set; }

        [Required]
        [MaxLength(200)]
        public string Subject { get; set; }
        [Required]
        public string Body { get; set; }

        public bool AllTenant { get; set; }
        public bool AllUser { get; set; }

        [ForeignKey("TenantId")]
        public virtual MultiTenancy.Tenant Tenant { get; set; }

        public virtual ICollection<MessageRead> MessageRead { get; set; }
    }
}

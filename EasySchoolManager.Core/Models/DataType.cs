﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
   public class DataType : GD.GdEntityWithoutTenant<int>
    {
        public string Code { get; set; }

        public string Description { get; set; }
        public string TypeHTML { get; set; }
    }
}

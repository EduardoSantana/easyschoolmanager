using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class StudentIllnesses : GD.GdEntityWithTenant<int>
    {
        public int IllnessId { get; set; }

        public int StudentId { get; set; }

        [ForeignKey("IllnessId")]
        public virtual Illnesses Illnesses { get; set; }

        [ForeignKey("StudentId")]
        public virtual Students Students { get; set; }
    }
}

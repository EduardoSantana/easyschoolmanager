﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class ProviderShops : GD.GdEntityWithTenant<int>
    {
        public int Sequence { get; set; }

        [Required]
        [Index("IX_ProviderShopName", 1)]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        [Index("IX_ProviderShopReference", 1)]
        [StringLength(20)]
        public string Reference { get; set; }

        [StringLength(15)]
        public string Rnc { get; set; }

        [StringLength(15)]
        public string Phone1 { get; set; }

        [StringLength(15)]
        public string Phone2 { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

    }
}

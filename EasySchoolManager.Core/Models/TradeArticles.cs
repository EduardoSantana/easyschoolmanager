﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class TradeArticles : GD.GdEntityWithoutTenant<int>

    {
        public int TradeId { get; set; }

        public int ArticleId { get; set; }

        public decimal Amount { get; set; }

        public decimal Total { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Articles Articles { get; set; }

        [ForeignKey("TradeId")]
        public virtual Trades Trades { get; set; }

        public long Quantity { get; set; }

    }
}
﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class Courses : GD.GdEntityWithoutTenant<int>
    {
        public Courses()
        {
            CourseEnrollmentStudents = new HashSet<CourseEnrollmentStudents>();
        }
        
        [Required]
        [Index("IX_CoursesNameLevels", 1, IsUnique = true)]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [Index("IX_CoursesAbbreviationLevels", 1, IsUnique = true)]
        [StringLength(5)]
        public string Abbreviation { get; set; }

        [Index("IX_CoursesAbbreviationLevels", 2, IsUnique = true)]
        [Index("IX_CoursesNameLevels", 2, IsUnique = true)]
        public int LevelId { get; set; }

        [DefaultValue(1)]
        public int Sequence { get; set; }

        [ForeignKey("LevelId")]
        public virtual Levels Levels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CourseEnrollmentStudents> CourseEnrollmentStudents { get; set; }
    }
}

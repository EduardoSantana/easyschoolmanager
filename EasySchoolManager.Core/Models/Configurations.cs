namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Configurations : GD.GdEntityWithTenant<int>
    {
        [StringLength(15)]
        public string Phone1 { get; set; }

        [StringLength(15)]
        public string Phone2 { get; set; }

        [StringLength(15)]
        public string Phone3 { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        public int? CityId { get; set; }

        [StringLength(100)]
        public string DirectorName { get; set; }

        [StringLength(100)]
        public string RegisterName { get; set; }

        public int? PeriodId { get; set; }

        [StringLength(15)]
        public string AccreditationNumber { get; set; }

        [StringLength(15)]
        public string EducationalDistrict { get; set; }

        [StringLength(60)]
        public string Regional { get; set; }

        [StringLength(60)]
        public string RegionalDirector { get; set; }

        [StringLength(60)]
        public string DistritctDirector { get; set; }

        [ForeignKey("CityId")]
        public virtual Cities Cities { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class TransactionArticleShops : GD.GdEntityWithTenant<int>
    {
        public long TransactionShopId { get; set; }

        public int ArticleShopId { get; set; }
        
        public decimal Amount { get; set; }

        public decimal Total { get; set; }

        [ForeignKey("ArticleShopId")]
        public virtual ArticleShops ArticleShops { get; set; }
        
        [ForeignKey("TransactionShopId")]
        public virtual TransactionShops TransactionShops { get; set; }
        public long Quantity { get; set; }
    }
}

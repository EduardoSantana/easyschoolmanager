﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class VerifoneBreaks : GD.GdEntityWithTenant<int>
    {
       
        public int Sequence { get; set; }

        public int? BankId { get; set; }

        public int? BankAccountId { get; set; }

        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        [DefaultValue(0.00)]
        public decimal CreditCardPercent { get; set; }

        public string Comments { get; set; }

        [ForeignKey("TenantId")]
        public virtual MultiTenancy.Tenant Tenant { get; set; }

        [ForeignKey("BankId")]
        public virtual Banks Banks { get; set; }

        [ForeignKey("BankAccountId")]
        public virtual BankAccounts BankAccounts { get; set; }
    }
}


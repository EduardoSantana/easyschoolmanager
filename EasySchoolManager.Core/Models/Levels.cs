﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class Levels: GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [StringLength(15)]
        public string Name { get; set; }

        [StringLength(9)]
        public string Code { get; set; }
    }
}

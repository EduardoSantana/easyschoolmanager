﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class Trades : GD.GdEntityWithoutTenant<int>
    {
        [StringLength(250)]
        public string Comment { get; set; }

        [StringLength(250)]
        public string CommentSend { get; set; }

        [StringLength(250)]
        public string CommentReceive { get; set; }

        public int CourrierPersonId { get; set; }

        public DateTime Date { get; set; }

        public DateTime DateSend { get; set; }

        public DateTime? DateReceive { get; set; }

        public int? TenantReceiveId { get; set; }

        public long? UserReceiveId { get; set; }

        [ForeignKey("CourrierPersonId")]
        public virtual CourrierPersons CourrierPersons { get; set; }

        [ForeignKey("TenantReceiveId")]
        public virtual MultiTenancy.Tenant TenantReceive { get; set; }

        [ForeignKey("UserReceiveId")]
        public virtual Authorization.Users.User UserReceive { get; set; }

    }
}
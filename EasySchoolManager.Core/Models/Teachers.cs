namespace EasySchoolManager.Models
{
    using EasySchoolManager.Authorization.Users;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Teachers : GD.GdEntityWithoutTenant<int>
    {
        public Teachers()
        {
            Subjects = new HashSet<Subjects>();
        }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [StringLength(11)]
        public string PersonalId { get; set; }

        public bool IsGraduated { get; set; }

        public bool HasMasterDegree { get; set; }

        public bool IsTechnician { get; set; }

        public int GenderId { get; set; }

        public long? UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User Users { get; set; }

        [ForeignKey("GenderId")]
        public virtual Genders Genders { get; set; }

        public virtual ICollection<Subjects> Subjects { get; set; }
    }
}

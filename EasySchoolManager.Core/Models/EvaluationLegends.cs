﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    /// <summary>
    /// Contiene las leyenda de las calificaciones, cada letra con su correspondiente
    /// descripcion (ej. C. completado, E.En proceso)
    /// </summary>
    public class EvaluationLegends: GD.GdEntityWithoutTenant<int>
    {
        [StringLength(1)]
        public string Name { get; set; }
        
        [StringLength(255)]
        public String Description { get; set; }

        /// <summary>
        /// Este campo es para indicar la secuencia de seleción, donde en un periodo no se puede seleccionar una legenda
        /// que sea inferior a la leyenda del periodo anterior.
        /// </summary>
        [DefaultValue(1)]
        public int Sequence { get; set; }


    }
}

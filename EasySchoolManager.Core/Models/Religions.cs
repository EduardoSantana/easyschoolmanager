﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class Religions : GD.GdEntityWithoutTenant<int>
    {
        [Index("IX_ReligionsShortName", 1, IsUnique = true)]
        [MaxLength(50)]
        public String ShortName { get; set; }

        [MaxLength(120)]
        public String LongName { get; set; }
    }
}

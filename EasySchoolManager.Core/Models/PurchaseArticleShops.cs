﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class PurchaseArticleShops : GD.GdEntityWithTenant<int>

    {

        public int PurchaseShopId { get; set; }

        public int ArticleShopId { get; set; }

        public decimal Amount { get; set; }

        public decimal Total { get; set; }

        [ForeignKey("ArticleShopId")]
        public virtual ArticleShops ArticleShops { get; set; }

        [ForeignKey("PurchaseShopId")]
        public virtual PurchaseShops PurchaseShops { get; set; }

        public long Quantity { get; set; }

    }
}


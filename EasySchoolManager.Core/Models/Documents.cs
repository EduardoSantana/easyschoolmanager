namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Documents : GD.GdEntityWithTenant<int>
    {
        public int DocumentTypeId { get; set; }

        [Column(TypeName = "Image")]
        [Required]
        public byte[] DocumentData { get; set; }

        [Required]
        public string Basa64Type { get; set; }

        public int? StudentId { get; set; }

        public long? TransactionId { get; set; }

        public int TransactionTypeId { get; set; }

        public int? TeacherId { get; set; }

        [ForeignKey("TeacherId")]
        public virtual Teachers Teachers { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transactions Transactions { get; set; }

        [ForeignKey("StudentId")]
        public virtual Students Students{ get; set; }
    }
}

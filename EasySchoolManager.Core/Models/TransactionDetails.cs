using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class TransactionDetails : GD.GdEntityWithTenant<int>
    {
        public long TransactionId { get; set; }

        public int CatalogId { get; set; }

        public decimal Amount { get; set; }

        public int TransactionTypeId { get; set; }

        [ForeignKey("CatalogId")]
        public virtual Catalogs Catalogs { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transactions Transactions { get; set; }
    }
}

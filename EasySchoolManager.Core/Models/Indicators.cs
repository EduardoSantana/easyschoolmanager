﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    /// <summary>
    /// Contiene cada uno de los indicadores ( son como las preguntas ) por los cuales seran evaluados los estudiantes ( cada indicador pertenece a un grupo)
    /// </summary>

    public class Indicators : GD.GdEntityWithoutTenant<int>
    {

        [StringLength(1000)]
        public string Name { get; set; }

        public int IndicatorGroupId { get; set; }

        public int Sequence { get; set; }

        [ForeignKey("IndicatorGroupId")]
        public virtual IndicatorGroups IndicatorGroups { get; set; }



        //Fue comentada la variable, Evaluation, por que carga toda la base de datos.
        public Indicators()
        {
            //Evaluations = new HashSet<Evaluations>();
        }
        //public virtual ICollection<Evaluations> Evaluations { get; set; }

    }
}

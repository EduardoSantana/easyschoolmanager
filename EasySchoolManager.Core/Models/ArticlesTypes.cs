﻿
namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class ArticlesTypes : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [Index("IX_ArticleTypeDescription", 1, IsUnique = true)]
        [StringLength(50)]
        public string Description { get; set; }
    }
}

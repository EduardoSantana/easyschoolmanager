﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class Sequences : GD.GdEntityWithoutTenant<int>
    {

        [Index("IX_PassportSequencesCode", 1, IsUnique = true)]
        [StringLength(5)]
        public string Code { get; set; }

        [Index("IX_PassportSequencesName", 1, IsUnique = true)]
        [StringLength(30)]
        public string Name { get; set; }

        public int Sequence { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class PurchaseArticles : GD.GdEntityWithoutTenant<int>

    {
        public int PurchaseId { get; set; }

        public int ArticleId { get; set; }

        public decimal Amount { get; set; }

        public decimal Total { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Articles Articles { get; set; }

        [ForeignKey("PurchaseId")]
        public virtual Purchases Purchases { get; set; }

        public long Quantity { get; set; }

    }
}

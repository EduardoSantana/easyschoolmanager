﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class InventoryDateDetails : GD.GdEntityWithTenant<int>

    {
        public int InventoryDateId { get; set; }

        public int ArticleId { get; set; }

        public long excistence { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Articles Articles { get; set; }

        [ForeignKey("InventoryDateId")]
        public virtual InventoryDates InventoryDates { get; set; }

    }
}
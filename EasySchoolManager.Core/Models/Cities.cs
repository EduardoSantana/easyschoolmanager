namespace EasySchoolManager.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Cities : GD.GdEntityWithoutTenant<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cities()
        {
            Configurations = new HashSet<Configurations>();
            Enrollments = new HashSet<Enrollments>();
        }
        
        public int ProvinceId { get; set; }

        [Required]
        [Index("IX_CitiesName", 1, IsUnique = true)]
        [StringLength(50)]
        public string Name { get; set; }

        [ForeignKey("ProvinceId")]
        public virtual Provinces Provinces { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Configurations> Configurations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Enrollments> Enrollments { get; set; }
    }
}

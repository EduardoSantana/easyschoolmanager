﻿namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class ReceiptTypes : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [Index("IX_ReceiptTypesName", 1, IsUnique = true)]
        [StringLength(25)]
        public string Name { get; set; }
    }
}

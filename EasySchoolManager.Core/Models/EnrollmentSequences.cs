﻿using EasySchoolManager.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class EnrollmentSequences : GD.GdEntityWithTenant<int>
    {
        [Index(name: "IX_EnrollmenSequenceTenantId", Order = 1, IsUnique = true)]
        public long EnrollmentId { get; set; }

        [Index(name: "IX_EnrollmenSequenceTenantId", Order = 2, IsUnique = true)]
        public new int TenantId { get; set; }

        public int Sequence { get; set; }

        [ForeignKey("EnrollmentId")]
        public virtual Enrollments Enrollments { set; get; }
        
        [ForeignKey("TenantId")]
        public virtual Abp.MultiTenancy.AbpTenant<User> Tenants { set; get; }

    }
}

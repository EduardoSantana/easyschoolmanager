using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class TransactionStudents: GD.GdEntityWithTenant<int>
    {
        public long TransactionId { get; set; }

        public int TransactionTypeId { get; set; }

        public int EnrollmentStudentId { get; set; }

        public decimal Amount { get; set; }

        [ForeignKey("EnrollmentStudentId")]
        public virtual EnrollmentStudents EnrollmentStudents { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transactions Transactions { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class Inventory : GD.GdEntityWithoutTenant<int>
    {
        public int ArticleId { get; set; }

        public long Existence { get; set; }

        public int? TenantInventoryId { get; set; }

        public Decimal Price { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Articles Articles { get; set; }

        [ForeignKey("TenantInventoryId")]
        public virtual MultiTenancy.Tenant TenantInventories { get; set; }


    }
}
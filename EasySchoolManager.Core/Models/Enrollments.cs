namespace EasySchoolManager.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Enrollments : GD.GdEntityWithoutTenant<long>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Enrollments()
        {
            EnrollmentStudents = new HashSet<EnrollmentStudents>();
            Transactions = new HashSet<Transactions>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override long Id { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        public string Address { get; set; }

        [Required]
        [StringLength(300)]
        public string FullAddress { get; set; }

        public int CityId { get; set; }

        [StringLength(20)]
        public string Phone1 { get; set; }

        public int GenderId { get; set; }

        [StringLength(20)]
        public string Phone2 { get; set; }

        public int OccupationId { get; set; }

        public int ReligionId { get; set; }

        [StringLength(250)]
        public string Comment { get; set; }

        [StringLength(255)]
        public string EmailAddress { get; set; }

        [ForeignKey("CityId")]
        public virtual Cities Cities { get; set; }

        [ForeignKey("OccupationId")]
        public virtual Occupations Occupations { get; set; }

        [ForeignKey("ReligionId")]
        public virtual Religions Religions { get; set; }

        public virtual ICollection<EnrollmentStudents> EnrollmentStudents { get; set; }

        public virtual ICollection<Transactions> Transactions { get; set; }

        public virtual ICollection<EnrollmentSequences> EnrollmentSequences { get; set; }

        [ForeignKey("GenderId")]
        public virtual Genders Genders { get; set; }

        [StringLength(20)]
        public string OtherDocument { get; set; }
    }
}

﻿
namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class Brands : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [Index("IX_BrandName", 1, IsUnique = true)]
        [StringLength(75)]
        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class Regions : GD.GdEntityWithoutTenant<int>
    {
        [Index("IX_RegionsName", 1, IsUnique = true)]
        [MaxLength(20)]
        public string Name { get; set; }

        [Required]
        [MaxLength(3)]
        public string Code { get; set; }

        [MaxLength(15)]
        public string Rnc { get; set; }

        [MaxLength(60)]
        public string LegalName { get; set; }

    }
}

﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ReportsFilters : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        public int ReportsId { get; set; }

        public int ReportId { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string DataField { get; set; }
        public int DataTypeId { get; set; }
        public bool Range { get; set; }
        public bool OnlyParameter { get; set; }
        public string UrlService { get; set; }
        public string FieldService { get; set; }
        public string DisplayNameService { get; set; }
        public string DependencyField { get; set; }
        public string Operator { get; set; }
        public string DefaultValue { get; set; }
        public string DisplayNameRange { get; set; }
        public bool Required { get; set; }

        [ForeignKey("ReportsId")]
        public virtual Reports Reports { get; set; }

        [ForeignKey("DataTypeId")]
        public virtual DataType DataType { get; set; }

    }
}

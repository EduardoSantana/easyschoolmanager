﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    /// <summary>
    /// La descripción que  lleva el encabezado de cada columna por posición
    /// </summary>

    public class EvaluationPositionHighs : GD.GdEntityWithoutTenant<int>
    {

        public EvaluationPositionHighs()
        {
            EvaluationOrderHighs = new HashSet<EvaluationOrderHighs>();
        }

        [StringLength(15)]
        public string Description { get; set; }

        public virtual ICollection<EvaluationOrderHighs> EvaluationOrderHighs { get; set; }
    }
}

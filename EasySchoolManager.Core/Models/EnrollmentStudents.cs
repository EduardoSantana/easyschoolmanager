namespace EasySchoolManager.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class EnrollmentStudents : GD.GdEntityWithTenant<int>
    {
        public EnrollmentStudents()
        {
            CourseEnrollmentStudents = new HashSet<CourseEnrollmentStudents>();
            PaymentProjections = new HashSet<PaymentProjections>();
            TransactionStudents = new List<TransactionStudents>();
        }

        public int PeriodId { get; set; }

        public long EnrollmentId { get; set; }

        public int StudentId { get; set; }

        public int RelationshipId { get; set; }

        public virtual ICollection<CourseEnrollmentStudents> CourseEnrollmentStudents { get; set; }

        [ForeignKey("EnrollmentId")]
        public virtual Enrollments Enrollments { get; set; }

        [ForeignKey("RelationshipId")]
        public virtual Relationships Relationships { get; set; }

        [ForeignKey("StudentId")]
        public virtual Students Students { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }

        public virtual ICollection<PaymentProjections> PaymentProjections { get; set; }

        public virtual List<TransactionStudents> TransactionStudents { get; set; }
    }
}

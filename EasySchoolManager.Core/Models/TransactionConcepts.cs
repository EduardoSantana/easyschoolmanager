using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class TransactionConcepts : GD.GdEntityWithTenant<int>
    {
        public long TransactionId { get; set; }

        public int ConceptId { get; set; }

        public decimal Amount { get; set; }

        [ForeignKey("ConceptId")]
        public virtual Concepts Concepts { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transactions Transactions { get; set; }
    }
}

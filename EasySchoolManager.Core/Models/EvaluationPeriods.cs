﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class EvaluationPeriods : GD.GdEntityWithoutTenant<int>
    {

        public EvaluationPeriods()
        {
            Evaluations = new HashSet<Evaluations>();
        }

        [StringLength(15)]
        public string Name { get; set; }

        public int Position { get; set; }

        public int InitialMonth { get; set; }

        public int FinalMonth { get; set; }

        public int PeriodId { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }
        public virtual ICollection<Evaluations> Evaluations { get; set; }

    }
}
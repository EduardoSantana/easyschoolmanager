﻿using EasySchoolManager.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class TeacherTenants : GD.GdEntityWithTenant<int>
    {
        public int TeacherId { get; set; }

        [ForeignKey("TeacherId")]
        public virtual Teachers Teachers { set; get; }

        [ForeignKey("TenantId")]
        public virtual MultiTenancy.Tenant Tenant { get; set; }
    }
}

﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public partial class ArticleShops : GD.GdEntityWithTenant<int>
    {
        public int Sequence { get; set; }

        [Required]
        [Index("IX_ArticleDescription", 1)]
        [StringLength(200)]
        public string Description { get; set; }

        [Required]
        [Index("IX_ArticleReference", 1)]
        [StringLength(4)]
        public string Reference { get; set; }

        public Decimal Price { get; set; }

        public int? UnitId { get; set; }

        public int? BrandId { get; set; }

        public int? ButtonPositionId { get; set; }

        [ForeignKey("UnitId")]
        public virtual Units Units { get; set; }

        [ForeignKey("BrandId")]
        public virtual Brands Brands { get; set; }

        [ForeignKey("ButtonPositionId")]
        public virtual ButtonPositions ButtonPositions { get; set; }

        public virtual List<InventoryShops> InventoryShops { get; set; }
    }
}

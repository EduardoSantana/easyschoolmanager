namespace EasySchoolManager.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Concepts : GD.GdEntityWithoutTenant<int>
    {
        public Concepts()
        {
            Transactions = new HashSet<Transactions>();
        }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }

        [Required]
        [StringLength(30)]
        public string ShortDescription { get; set; }
        
        public int CatalogId { get; set; }

        public int? PeriodId { get; set; }

        public int? ReceiptTypeId { get; set; }

        public bool AvailableForTransaction { get; set; }

        public bool AvailableForReceipts { get; set; }
        
        public bool AvailableForDeposits { get; set; }
        
        public bool AvailableForDiscounts { get; set; }

        public virtual ICollection<Transactions> Transactions { get; set; }

        public decimal? Amount { get; set; }

        public int? sequenceNext { get; set; }

        [ForeignKey("CatalogId")]
        public virtual Catalogs Catalogs { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }

        [ForeignKey("ReceiptTypeId")]
        public virtual ReceiptTypes ReceiptTypes { get; set; }

        public bool IsEns { get; set; }

        public bool IsMat { get; set; }

    }
}

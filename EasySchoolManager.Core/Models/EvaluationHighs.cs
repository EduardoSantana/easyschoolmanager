﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    /// <summary>
    /// Para registrar la evaluacion de secundaria
    /// Cada registro es una calificación de una materia de un estudiante de media con un orden específico establecido en un período escolar indicado
    /// </summary>
    /// 
    public class EvaluationHighs : GD.GdEntityWithTenant<int>
    {
        public int SubjectHighId { get; set; }

        public int EnrollmentStudentId { get; set; }

        public int EvaluationOrderHighId { get; set; }

        public int Expression { get; set; }

        [ForeignKey("SubjectHighId")]
        public virtual SubjectHighs SubjectHighs { get; set; }

        [ForeignKey("EnrollmentStudentId")]
        public virtual EnrollmentStudents EnrollmentStudents { get; set; }

        [ForeignKey("EvaluationOrderHighId")]
        public virtual EvaluationOrderHighs EvaluationOrderHighs { get; set; }

    }
}

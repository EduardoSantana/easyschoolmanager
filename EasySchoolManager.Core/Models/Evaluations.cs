﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class Evaluations : GD.GdEntityWithTenant<long>
    {
        public int IndicatorId { get; set; }

        public int? EvaluationLegendId { get; set; }

        public int EnrollmentStudentId { get; set; }

        public int EvaluationPeriodId { get; set; }

        /// <summary>
        /// Este campo es para permitir a la entidad borrar registro sin interferir con el constraint unique, se pone 0 para indicar que esta activo
        /// y el numero de id del registro para indicar que ya fue eliminado, esto no te dira que fue eliminado, por que hay un campo delete, pero te permitira
        /// tener otro registro en el constrain del campo.
        /// </summary>
        [DefaultValue(0)]
        public long ComplementForDeletion { get; set; }

        [ForeignKey("IndicatorId")]
        public virtual Indicators Indicators { get; set; }

        [ForeignKey("EvaluationLegendId")]
        public virtual EvaluationLegends EvaluationLegends { get; set; }

        [ForeignKey("EnrollmentStudentId")]
        public virtual EnrollmentStudents EnrollmentStudents { get; set; }

        [ForeignKey("EvaluationPeriodId")]
        public virtual EvaluationPeriods EvaluationPerios { get; set; }

    }
}

namespace EasySchoolManager.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Provinces : GD.GdEntityWithoutTenant<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Provinces()
        {
            Cities = new HashSet<Cities>();
        }

        [Required]
        [Index("IX_ProvincesName", 1, IsUnique = true)]
        [StringLength(50)]
        public string Name { get; set; }

        public int RegionId { get; set; }

        [ForeignKey("RegionId")]
        public virtual Regions Regions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cities> Cities { get; set; }
    }
}

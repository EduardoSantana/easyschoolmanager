﻿using Abp.Authorization.Users;
using EasySchoolManager.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
   public class UserTenants: GD.GdEntityWithoutTenant<long>
    {
        public long UserId { get; set; }
        public int TenantId { get; set; }

        [ForeignKey("UserId")]
        public virtual User Users { get; set; }
        [ForeignKey("TenantId")]
        public virtual MultiTenancy.Tenant Tenant { get; set; }
    }
}

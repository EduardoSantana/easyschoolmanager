﻿using EasySchoolManager.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
   public class UserPictures : GD.GdEntityWithoutTenant<long>
    {
        public long UserId { get; set; }
        [Required]
        public string Picture { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}

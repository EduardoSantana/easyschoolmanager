namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class PaymentProjections : GD.GdEntityWithTenant<int>
    {
        public int EnrollmentStudentId { get; set; }

        public int Sequence { get; set; }

        public decimal Amount { get; set; }

        [Column(TypeName = "date")]
        public DateTime PaymentDate { get; set; }

        [ForeignKey("EnrollmentStudentId")]
        public virtual EnrollmentStudents EnrollmentStudents { get; set; }

        public bool Applied { get; set; }

        public long? TransactionId { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transactions Transactions { get; set; }

        public override string ToString()
        {
            return $"id={this.Id}, Sequence={this.Sequence}, Amount={this.Amount}, PaymentDate={this.PaymentDate},TransctionId={this.TransactionId}, EnrollmentStudentId={this.EnrollmentStudentId}";
        }
    }
}

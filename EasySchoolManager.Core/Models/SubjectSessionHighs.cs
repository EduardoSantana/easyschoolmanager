﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class SubjectSessionHighs : GD.GdEntityWithTenant<int>
    {
        public int SubjectHighId { get; set; }
        public int TeacherTenantId { get; set; }
        public int CourseSessionId { get; set; }
        public int PeriodId { get; set; }

        [ForeignKey("SubjectHighId")]
        public virtual SubjectHighs SubjectHighs { get; set; }

        [ForeignKey("TeacherTenantId")]
        public virtual TeacherTenants TeacherTenants { get; set; }

        [ForeignKey("CourseSessionId")]
        public virtual CourseSessions CourseSessions { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }

    }
}

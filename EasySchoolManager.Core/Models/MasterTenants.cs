﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class MasterTenants : GD.GdEntityWithoutTenant<int>
    {
        [StringLength(200)]
        public string Name { get; set; }
        [StringLength(100)]
        public string ShortName { get; set; }
        [StringLength(200)]
        public string Address { get; set; }

        public int CityId { get; set; }

        [ForeignKey("CityId")]
        public virtual Cities Cities { get; set; }

        [StringLength(15)]
        public string Phone1 { get; set; }

        [StringLength(15)]
        public string Phone2 { get; set; }

        [StringLength(15)]
        public string Phone3 { get; set; }

        [StringLength(100)]
        public string DirectorName { get; set; }

        [StringLength(100)]
        public string AccountantName { get; set; }

        [StringLength(100)]
        public string TreasurerName { get; set; }

        [StringLength(4)]
        public string CurrencyCode { get; set; }


        /// <summary>
        /// Porcentaje que sera retenido de los pagos por tarjeta de credito
        /// </summary>
        [DefaultValue(0.0000)]
        public decimal CreditCardPercent { get; set; }


        /// <summary>
        /// Cuenta contable para registrar los intereses o cargos de tarjetas de 
        /// credito en los pagos que sean realizados con forma de pago tarjeta de credito.
        /// </summary>
        public int? CatalogForCreditCardId { get; set; }

        public int?  ConceptForInscriptionTransactionsId { get; set; }


        /// <summary>
        /// Referencia a la tabla del catalogo de cuentas.
        /// </summary>
        [ForeignKey("CatalogForCreditCardId")]
        public virtual Models.Catalogs CatalogForCreditCards{ get; set; }

        [ForeignKey("ConceptForInscriptionTransactionsId")]
        public virtual Models.Concepts InscripcionConcepts{ get; set; }

        public int?  ConceptForMonthlyPaymentTransactionsId { get; set; }

        [ForeignKey("ConceptForMonthlyPaymentTransactionsId")]
        public virtual Models.Concepts ConceptForPayments { get; set; }
        
    }
}

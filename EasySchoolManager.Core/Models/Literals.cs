﻿using System.ComponentModel.DataAnnotations;

namespace EasySchoolManager.Models
{
    public class Literals: GD.GdEntityWithoutTenant<int>
    {
        [StringLength(1)]
        public string Letter { get; set; }
        public int FinalPercent { get; set; }
        public int InitialPercent { get; set; }
    }
}
﻿namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

  
    public partial class ButtonPositions : GD.GdEntityWithoutTenant<int>

    {
        [Index("IX_ButtonPositionNumber", 1, IsUnique = true)]
        [StringLength(2)]
        public string Number { get; set; }
    }
}

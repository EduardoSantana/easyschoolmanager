﻿
namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ReportsTypes : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [MaxLength(50)]
        public string Nombre { get; set; } 
        [MaxLength(120)]
        public string Descripcion { get; set; } 

        public virtual ICollection<Reports> Reports { get; set; }
    }
}

namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class PrinterTypes : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [Index("IX_PrinterTypesName", 1, IsUnique = true)]
        [StringLength(25)]
        public string Name { get; set; }

        public int ReportId { get; set; }

        [ForeignKey("ReportId")]
        public virtual Models.Reports Reports{ get; set; }

    }
}

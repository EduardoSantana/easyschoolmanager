﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class ApplicationDates :GD.GdEntityWithoutTenant<int>
    {
        public DateTime ApplicationDate { get; set; }
        public int WorkerId { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}

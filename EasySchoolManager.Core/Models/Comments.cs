namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Comments : GD.GdEntityWithTenant<int>
    {
        public int StudentId { get; set; }

        [Required]
        [StringLength(500)]
        public string Comment { get; set; }

        public int CommentTypeId { get; set; }

        [ForeignKey("CommentTypeId")]
        public virtual CommentTypes CommentTypes { get; set; }

        [ForeignKey("StudentId")]
        public virtual Students Students { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class TransactionArticles : GD.GdEntityWithTenant<int>
    {
        public long TransactionId { get; set; }

        public int ArticleId { get; set; }

        public decimal Amount { get; set; }

        public decimal Total { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Articles Articles { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transactions Transactions { get; set; }
        public long Quantity { get; set; }
    }
}

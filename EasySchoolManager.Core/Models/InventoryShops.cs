﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class InventoryShops : GD.GdEntityWithTenant<int>
    {
        public int ArticleShopId { get; set; }

        public long Existence { get; set; }

        public Decimal Price { get; set; }

        [ForeignKey("ArticleShopId")]
        public virtual ArticleShops ArticleShops { get; set; }

    }
}
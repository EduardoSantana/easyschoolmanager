using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class TransactionHistories : GD.GdEntityWithTenant<int>
    {
        public decimal Amount { get; set; }

        public int StatusId { get; set; }

        public int ChangeTypeId { get; set; }

        public long TransactionId { get; set; }

        public int TransactionTypeId { get; set; }

        [ForeignKey("ChangeTypeId")]
        public virtual ChangeTypes ChangeTypes { get; set; }

        [ForeignKey("StatusId")]
        public virtual Statuses Statuses { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transactions Transactions { get; set; }
    }
}

﻿namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class GrantEnterprises : GD.GdEntityWithTenant<int>
    {
        public int Sequence { get; set; }

        [Required]
        [Index("IX_GrantEnterpriseName", 1)]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

    }
}


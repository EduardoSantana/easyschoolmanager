namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Students : GD.GdEntityWithoutTenant<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Students()
        {
            Comments = new HashSet<Comments>();
            Documents = new HashSet<Documents>();
            EnrollmentStudents = new HashSet<EnrollmentStudents>();
            StudentAllergies = new HashSet<StudentAllergies>();
            StudentIllnesses = new HashSet<StudentIllnesses>();
        }
       
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        public string FullName { get { return this.FirstName + " " + this.LastName; } }
        public string FullNameInverso { get { return this.LastName + ", " + this.FirstName; } }

        [Column(TypeName = "date")]
        public DateTime DateOfBirth { get; set; }

        [StringLength(250)]
        public string EmergencyNote { get; set; }

        public short Position { get; set; }

        public bool? EmployeeSon { get; set; }

        public int GenderId { get; set; }

        [StringLength(100)]
        public string Derivation { get; set; }

        [StringLength(200)]
        public string Medication { get; set; }

        [StringLength(200)]
        public string Nutrition { get; set; }

        public int BloodId { get; set; }

        [StringLength(255)]
        public string EmailAddress { get; set; }

        [Required]
        [Index("IX_StudentsStudentPersonalId", 1, IsUnique = true)]
        [StringLength(11)]
        public string StudentPersonalId { get; set; }

        [StringLength(11)]
        public string FatherPersonalId { get; set; }

        [StringLength(100)]
        public string FatherName { get; set; }

        [StringLength(11)]
        public string MotherPersonalId { get; set; }

        [StringLength(100)]
        public string MotherName { get; set; }

        public bool? FatherAttendedHere { get; set; }

        public bool? MotherAttendedHere { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comments> Comments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Documents> Documents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EnrollmentStudents> EnrollmentStudents { get; set; }

        [ForeignKey("GenderId")]
        public virtual Genders Genders { get; set; }

        [ForeignKey("BloodId")]
        public virtual Bloods Bloods { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentAllergies> StudentAllergies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentIllnesses> StudentIllnesses { get; set; }

    }
}

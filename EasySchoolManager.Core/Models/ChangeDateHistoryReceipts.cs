﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ChangeDateHistoryReceipts : GD.GdEntityWithoutTenant<int>
    {

        public long TransactionId { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        public DateTime DateLast { get; set; }
        public DateTime DatePrior { get; set; }
        public int UserId { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transactions Transactions { get; set; }

    }
}
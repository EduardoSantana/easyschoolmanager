﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class InventoryDates : GD.GdEntityWithTenant<int>
    {

        public DateTime Date { get; set; }

        [StringLength(150)]
        public string Description { get; set; }

    }
}


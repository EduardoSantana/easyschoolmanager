namespace EasySchoolManager.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Catalogs : GD.GdEntityWithoutTenant<int>
    {
        public Catalogs()
        {
            Concepts = new HashSet<Concepts>();
        }

        [Required]
        [StringLength(20)]
        public string AccountNumber { get; set; }

        [Required]
        [StringLength(85)]
        public string Name { get; set; }

        [Required]
        [StringLength(300)]
        public string Descsription { get; set; }

        [StringLength(4)]
        public string Code1 { get; set; }

        [StringLength(9)]
        public string Code10 { get; set; }

        public virtual ICollection<Concepts> Concepts { get; set; }

    }
}

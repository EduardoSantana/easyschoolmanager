﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class HistoryCancelReceipts : GD.GdEntityWithTenant<int>
    {

        public long TransactionId { get; set; }

        [StringLength(100)]
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public int CA { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transactions Transactions { get; set; }

    }
}

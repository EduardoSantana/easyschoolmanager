﻿namespace EasySchoolManager.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Usado para saber el tipo de calificacion como sera, si es para bachiller o para basica, por que llevan calculos diferentes ( validar esto)
    /// </summary>
    public partial class ScoreTypes : GD.GdEntityWithoutTenant<int>
    {
        public ScoreTypes()
        {
            Subjects = new HashSet<Subjects>();
        }
        [Required]
        [Index("IX_ScoreTypeName", 1, IsUnique = true)]
        [StringLength(35)]
        public string Name { get; set; }
        public ICollection<Subjects> Subjects { get; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class DiscountNameTenants : GD.GdEntityWithoutTenant<int>
    {
        public int PeriodDiscountId { get; set; }
        public int TenantId { get; set; }

        [ForeignKey("PeriodDiscountId")]
        public virtual PeriodDiscounts PeriodDiscounts { get; set; }

        [ForeignKey("TenantId")]
        public virtual MultiTenancy.Tenant Tenant { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class PurchaseShops : GD.GdEntityWithTenant<int>
    {

        public int Sequence { get; set; }

        [StringLength(25)]
        public string Document { get; set; }

        public int ProviderShopId { get; set; }

        public decimal Amount { get; set; }

        public DateTime Date { get; set; }

        [StringLength(250)]
        public string Comnent1 { get; set; }

        [StringLength(250)]
        public string Comment2 { get; set; }

        [ForeignKey("ProviderShopId")]
        public virtual ProviderShops ProviderShops { get; set; }


    }
}
namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class BankAccounts : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [Index("IX_BankAccountsName", 1, IsUnique = true)]
        [StringLength(25)]
        public string Number { get; set; }

        public int BanktId { get; set; }
        public int? RegionId { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [ForeignKey("BanktId")]
        public virtual Banks Banks { get; set; }

        [ForeignKey("RegionId")]
        public virtual Regions Regions { get; set; }
    }
}

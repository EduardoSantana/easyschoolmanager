﻿namespace EasySchoolManager.Models
{
    using System;
    public partial class ErrorImportings : GD.GdEntityWithoutTenant<int>
    {
        public string Details { get; set; }

        public string InnserException { get; set; }

        public string CurrentItem { get; set; }
    }
}

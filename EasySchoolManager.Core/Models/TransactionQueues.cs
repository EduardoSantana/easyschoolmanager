namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class TransactionQueues : GD.GdEntityWithTenant<long>
    {
        public TransactionQueues()
        {
           
        }

        public long TransactionId { get; set; }
        public bool Send { get; set; }
        public DateTime? SendDate { get; set; }
        public DateTime? LastTryDate { get; set; }
        public long? Retry { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transactions Transaction { get; set; }
        
    }
}

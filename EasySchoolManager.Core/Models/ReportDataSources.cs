﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ReportDataSources : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [MaxLength(80)]
        public string Name { get; set; } 
        [MaxLength(20)]
        public string Code { get; set; }
    
    }
}

﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class ClientShops : GD.GdEntityWithTenant<int>
    {

        public int Sequence { get; set; }

        [Required]
        [Index("IX_ClientShopName", 1)]
        [StringLength(200)]
        public string Name { get; set; }
  
        [Required]
        [Index("IX_ClientShopReference", 1)]
        [StringLength(20)]
        public string Reference { get; set; }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace EasySchoolManager.Models
{

    /// <summary>
    /// Son las diferentes materias que tiene un curso en particular
    /// </summary>
    public partial class Subjects : GD.GdEntityWithoutTenant<int>
    {
        public Subjects()
        {
            IndicatorGroups = new HashSet<IndicatorGroups>();
        }

        [Required]
        [StringLength(80)]
        public string Name { get; set; }

        [Required]
        [StringLength(350)]
        public string LongName { get; set; }

        [Required]
        [StringLength(20)]
        public string Code { get; set; }

        public int CourseId { get; set; }

        [ForeignKey("CourseId")]
        public virtual Courses Courses { get; set; }

        public virtual ICollection<IndicatorGroups> IndicatorGroups { get; set; }
    }
}


using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class StudentAllergies : GD.GdEntityWithTenant<int>
    {
        public int AllergyId { get; set; }

        public int StudentId { get; set; }

        [ForeignKey("AllergyId")]
        public virtual Allergies Allergies { get; set; }
        
        [ForeignKey("StudentId")]
        public virtual Students Students { get; set; }
    }
}

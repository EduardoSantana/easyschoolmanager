﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace EasySchoolManager.Models
{

    /// <summary>
    /// Aquí se define el orden de los meses de evaluación
    /// </summary>
    public partial class EvaluationOrderHighs : GD.GdEntityWithoutTenant<int>
    {
        
        public int EvaluationPositionHighId { get; set; }

        public int Month { get; set; }

        [StringLength(15)]
        public string Description { get; set; }

        [ForeignKey("EvaluationPositionHighId")]
        public virtual EvaluationPositionHighs EvaluationPositionHighs { get; set; }


    }
}
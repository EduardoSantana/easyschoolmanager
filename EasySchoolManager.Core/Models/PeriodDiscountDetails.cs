﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class PeriodDiscountDetails : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [Index("IX_PeriodDiscountDetailsPeriodPercent", 0, IsUnique = true)]
        public int PeriodDiscountId { get; set; }

        [Required]
        [Index("IX_PeriodDiscountDetailsPeriodPercent", 1, IsUnique = true)]
        public decimal DiscountPercent { get; set; }


        [ForeignKey("PeriodDiscountId")]
        public virtual PeriodDiscounts PeriodDiscounts { get; set; }

    }
}

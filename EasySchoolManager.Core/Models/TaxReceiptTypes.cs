﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public class TaxReceiptTypes: GD.GdEntityWithoutTenant<int>
    {

        [Index("IX_TaxReceiptTypesName", 1, IsUnique = true)]
        [Required(ErrorMessage ="Debe indicar el nombre del tipo de comprobante fiscal")]
        [MaxLength(50)]
        public string Name { get; set; }

        [Index("IX_TaxReceiptTypesCode", 1, IsUnique = true)]
        [Required(ErrorMessage = "Debe indicar el código del tipo de comprobante fiscal")]
        [MaxLength(2)]
        public string Code { get; set; }
    }
}

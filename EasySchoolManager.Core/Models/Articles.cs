﻿namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public partial class Articles : GD.GdEntityWithoutTenant<int>
    {
        public Articles()
        {
            Inventories = new HashSet<Inventory>();
        }

        [Required]
        [Index("IX_ArticleDescription", 1, IsUnique = true)]
        [StringLength(200)]
        public string Description { get; set; }

        [Required]
        [Index("IX_ArticleReference", 1, IsUnique = true)]
        [StringLength(20)]
        public string Reference { get; set; }

        public Decimal Price { get; set; }

        public int? ArticleTypeId { get; set; }

        public int? UnitId { get; set; }

        public int? BrandId { get; set; }

        [ForeignKey("ArticleTypeId")]
        public virtual ArticlesTypes ArticlesTypes { get; set; }
        
        [ForeignKey("UnitId")]
        public virtual Units Units { get; set; }

        [ForeignKey("BrandId")]
        public virtual Brands Brands { get; set; }
        
        public virtual ICollection<Inventory> Inventories { get; set; }
    }
}

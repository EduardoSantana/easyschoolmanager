using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    //Corresponde a un mes de un periodo determinado
    public partial class MonthlyPeriods : GD.GdEntityWithTenant<int>
    {
        [Index("IX_MonthlyPeriodsYearMonth", 1, IsUnique = true)]
        public int PeriodId { get; set; }

        [Index("IX_MonthlyPeriodsYearMonth", 2, IsUnique = true)]
        public short Month { get; set; }

        [Index("IX_MonthlyPeriodsYearMonth", 3, IsUnique = true)]
        public short Year { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }
    }
}

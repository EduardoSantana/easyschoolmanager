﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.Models
{
    public partial class InventoryShopDateDetails : GD.GdEntityWithTenant<int>

    {
        public int InventoryShopDateId { get; set; }

        public int ArticleShopId { get; set; }
        
        public long excistence { get; set; }
        
        [ForeignKey("ArticleShopId")]
        public virtual ArticleShops ArticleShops { get; set; }

        [ForeignKey("InventoryShopDateId")]
        public virtual InventoryShopDates InventoryShopDates { get; set; }
        
    }
}

namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Transactions : GD.GdEntityWithTenant<long>
    {
        public Transactions()
        {
            TransactionDetails = new HashSet<TransactionDetails>();
            TransactionStudents = new HashSet<TransactionStudents>();
            TransactionArticles = new HashSet<TransactionArticles>();
            TransactionConcepts = new HashSet<TransactionConcepts>();
        }

        public int? TaxReceiptId { get; set; }

        /// <summary>
        /// Fecha de expiracion del comprobante fiscal en caso de que sea utilizado uno.
        /// </summary>
        public DateTime? TaxReceiptExpirationDate { get; set; }

        public int Sequence { get; set; }

        public int? BankId { get; set; }

        public int? BankAccountId { get; set; }

        [StringLength(20)]
        public string Number { get; set; }

        public DateTime Date { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Comment { get; set; }

        [StringLength(100)]
        public string Request { get; set; }

        [StringLength(100)]
        public string CancelAuthorization { get; set; }

        public long? EnrollmentId { get; set; }

        public int? ConceptId { get; set; }

        public int? GrantEnterpriseId { get; set; }

        public int? ReceiptTypeId { get; set; }

        public decimal Amount { get; set; }

        public int? PaymentMethodId { get; set; }

        public int OriginId { get; set; }

        public int TransactionTypeId { get; set; }

        [DefaultValue(0.00)]
        public decimal CreditCardPercent { get; set; }

        /// <summary>   
        /// Numero de comprobante fiscal (NCF)
        /// </summary>
        [MaxLength(11)]
        public string TaxReceiptSequence { get; set; }

        public int StatusId { get; set; }

        [MaxLength(15)]
        public string RncNumber { get; set; }

        [MaxLength(150)]
        public string RncName { get; set; }

        public string Comments { get; set; }

        public int? CancelStatusId { get; set; }

        public DateTime? CancelRequested { get; set; }

        public DateTime? CancelAuthorized { get; set; }
        
        [ForeignKey("CancelStatusId")]
        public virtual CancelStatus CancelStatus { get; set; }

        [ForeignKey("TenantId")]
        public virtual MultiTenancy.Tenant Tenant { get; set; }

        [ForeignKey("BankId")]
        public virtual Banks Banks { get; set; }

        [ForeignKey("ConceptId")]
        public virtual Concepts Concepts { get; set; }

        [ForeignKey("EnrollmentId")]
        public virtual Enrollments Enrollments { get; set; }

        [ForeignKey("OriginId")]
        public virtual Origins Origins { get; set; }

        [ForeignKey("PaymentMethodId")]
        public virtual PaymentMethods PaymentMethods { get; set; }

        [ForeignKey("StatusId")]
        public virtual Statuses Statuses { get; set; }

        [ForeignKey("BankAccountId")]
        public virtual BankAccounts BankAccounts { get; set; }
       
        public virtual ICollection<TransactionDetails> TransactionDetails { get; set; }

        [ForeignKey("TransactionTypeId")]
        public virtual TransactionTypes TransactionTypes { get; set; }

        [ForeignKey("ReceiptTypeId")]
        public virtual ReceiptTypes ReceiptTypes { get; set; }

        [ForeignKey("GrantEnterpriseId")]
        public virtual GrantEnterprises GrantEnterprises { get; set; }

        public virtual ICollection<TransactionStudents> TransactionStudents { get; set; }

        public virtual ICollection<TransactionArticles> TransactionArticles { get; set; }

        public virtual ICollection<TransactionConcepts> TransactionConcepts { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class CourrierPersons : GD.GdEntityWithoutTenant<int>
    {
        [Index("IX_CourrierPersonName", 1, IsUnique = true)]
        [MaxLength(75)]
        public String Name { get; set; }

        [MaxLength(150)]
        public String FuntionDescription { get; set; }

    }
}
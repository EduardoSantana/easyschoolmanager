﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class Companies : GD.GdEntityWithoutTenant<long>
    {
        [MaxLength(15)]
        public string RNC { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string CommercialName { get; set; }

        [MaxLength(200)]
        public string CommercialActivity { get; set; }

        [MaxLength(10)]
        public string CompanyDate { get; set; }

        [MaxLength(15)]
        public string Status { get; set; }

        [MaxLength(100)]
        public string PaymentSystem { get; set; }
        
    }
}

namespace EasySchoolManager.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class DocumentTypes: GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [Index("IX_DocumentTypesName", 1, IsUnique = true)]
        [StringLength(50)]
        public string Name { get; set; }
    }
}

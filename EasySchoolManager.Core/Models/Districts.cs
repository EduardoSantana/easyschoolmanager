﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Models
{
    public class Districts : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [Index("IX_DistricName", 1, IsUnique = true)]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [Index("IX_DistricAbbrev", 1, IsUnique = true)]
        [StringLength(9)]
        public string Abbreviation { get; set; }
    }
}

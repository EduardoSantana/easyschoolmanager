﻿

namespace EasySchoolManager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class Providers : GD.GdEntityWithoutTenant<int>
    {

        [Required]
        [Index("IX_ProviderName", 1, IsUnique = true)]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        [Index("IX_ProviderReference", 1, IsUnique = true)]
        [StringLength(20)]
        public string Reference { get; set; }

        [StringLength(15)]
        public string Rnc { get; set; }

        [StringLength(15)]
        public string Phone1 { get; set; }

        [StringLength(15)]
        public string Phone2 { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

    }
}

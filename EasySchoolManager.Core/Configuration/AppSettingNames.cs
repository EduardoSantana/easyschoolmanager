﻿namespace EasySchoolManager.Configuration
{
    public static class AppSettingNames
    {
        public const string UiTheme = "App.UiTheme";
        public const string UrlSiteHost = "App.UrlSiteHost";
    }
}

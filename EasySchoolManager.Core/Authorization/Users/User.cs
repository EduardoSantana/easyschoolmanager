﻿using System;
using Abp.Authorization.Users;
using Abp.Extensions;
using Microsoft.AspNet.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using EasySchoolManager.Models;

namespace EasySchoolManager.Authorization.Users
{
    public class User : AbpUser<User>
    {
        public const string DefaultPassword = "123qwe";

        [Index(name: "IX_UserUserName", IsUnique = true)]
        public override string UserName { get; set; }

        public long? ParentUserId { get; set; }

        public User()
        {
            UserTenants = new List<Models.UserTenants>();
        }

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public static User CreateTenantAdminUser(int tenantId, string emailAddress, string password, string name, string surName, string userName)
        {
            var user = new User
            {
                TenantId = tenantId,
                UserName = userName,
                Name = name,
                Surname = surName,
                EmailAddress = emailAddress,
                Password = new PasswordHasher().HashPassword(password),
                MustChangePassword = true,
                ParentUserId = 1 //Tomamos el usuario administrador para decir que es el usuario padre
            };
            return user;
        }

        public static User CreateTenantUser(int tenantId, string emailAddress, string password, string name, string surName, string userName)
        {
            var user = new User
            {
                TenantId = tenantId,
                UserName = userName,
                Name = name,
                Surname = surName,
                EmailAddress = emailAddress,
                Password = new PasswordHasher().HashPassword(password),
                MustChangePassword = true
            };
            return user;
        }

        public bool? MustChangePassword { get; set; }

        [ForeignKey("TenantId")]
        public virtual MultiTenancy.Tenant Tenant { get; set; }

        public int? UserTypeId { get; set; }

        [ForeignKey("UserTypeId")]
        public UserTypes UserType { get; set; }

        public int? PositionId { get; set; }

        [ForeignKey("PositionId")]
        public Positions Positions { get; set; }

        public virtual List<Models.UserTenants> UserTenants { get; set; }
        public virtual List<Models.UserPictures> UserPictures { get; set; }

    }
}
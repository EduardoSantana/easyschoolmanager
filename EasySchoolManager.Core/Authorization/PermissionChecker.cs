﻿using Abp.Authorization;
using EasySchoolManager.Authorization.Roles;
using EasySchoolManager.Authorization.Users;

namespace EasySchoolManager.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}

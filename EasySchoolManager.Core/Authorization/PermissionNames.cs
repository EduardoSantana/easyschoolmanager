namespace EasySchoolManager.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_ScoreTypes = "Pages.ScoreTypes";

        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";

        public const string Pages_Courses = "Pages.Courses";

        public const string Pages_Reports = "Pages.Reports";

        public const string Pages_Profile = "Pages.Profile";

        //No borrar la linea siguiente, ya que se usa para agregar los permisos automaticamente.
        public const string Pages_EvaluationPeriods = "Pages.EvaluationPeriods";

        public const string Pages_Evaluations = "Pages.Evaluations";

        public const string Pages_GrantEnterprises = "Pages.GrantEnterprises";

        //   public const string Pages_HistoryCancelReceipts = "Pages.HistoryCancelReceipts";

        public const string Pages_SubjectHighs = "Pages.SubjectHighs";

        public const string Pages_EvaluationPositionHighs = "Pages.EvaluationPositionHighs";

        public const string Pages_EvaluationOrderHighs = "Pages.EvaluationOrderHighs";

        public const string Pages_EvaluationHighs = "Pages.EvaluationHighs";

        public const string Pages_EvaluationHighsByPos = "Pages.EvaluationHighsByPos";

        public const string Pages_EvaluationHighsByPosPrevious = "Pages.EvaluationHighsByPosPrevious";

        public const string Pages_TransactionQueues = "Pages.TransactionQueues";

        public const string Pages_TaxReceiptTypes = "Pages.TaxReceiptTypes";

        public const string Pages_TaxReceipts = "Pages.TaxReceipts";

        public const string Pages_Companies = "Pages.Companies";

        public const string Pages_CancelStatus = "Pages.CancelStatus";

        public const string Pages_SubjectSessions = "Pages.SubjectSessions";

        public const string Pages_SubjectSessionHighs = "Pages.SubjectSessionHighs";

        public const string Pages_ConceptTenantRegistrations = "Pages.ConceptTenantRegistrations";

        public const string Pages_VerifoneBreaks = "Pages.VerifoneBreaks";

        public const string Pages_Discounts = "Pages.Discounts";

        public const string Pages_DiscountNames = "Pages.DiscountNames";

        public const string Pages_PeriodDiscounts = "Pages.PeriodDiscounts";

        //XXXInsertPermissionNamesXXX

        public const string Pages_Indicators = "Pages.Indicators";

        public const string Pages_EvaluationLegends = "Pages.EvaluationLegends";

        public const string Pages_IndicatorGroups = "Pages.IndicatorGroups";

        public const string Pages_Subjects = "Pages.Subjects";

        public const string Pages_Teachers = "Pages.Teachers";

        //public const string Pages_T2Dimensions = "Pages.T2Dimensions";

        //public const string Pages_T10Dimensions = "Pages.T10Dimensions";

        //public const string Pages_T9Dimensions = "Pages.T9Dimensions";

        //public const string Pages_T8Dimensions = "Pages.T8Dimensions";

        //public const string Pages_T7Dimensions = "Pages.T7Dimensions";

        //public const string Pages_T6Dimensions = "Pages.T6Dimensions";

        //public const string Pages_T5Dimensions = "Pages.T5Dimensions";

        //public const string Pages_T4Dimensions = "Pages.T4Dimensions";

        //public const string Pages_T1Dimensions = "Pages.T1Dimensions";

        //public const string Pages_T3Dimensions = "Pages.T3Dimensions";

        public const string Pages_Positions = "Pages.Positions";

        public const string Pages_InventoryShops = "Pages.InventoryShops";

        public const string Pages_Inventory = "Pages.Inventory";

        public const string Pages_ButtonPositions = "Pages.ButtonPositions";

        public const string Pages_TransactionShops = "Pages.TransactionShops";

        public const string Pages_ClientShops = "Pages.ClientShops";

        public const string Pages_PurchaseShops = "Pages.PurchaseShops";

        public const string Pages_ProviderShops = "Pages.ProviderShops";

        public const string Pages_ArticleShops = "Pages.ArticleShops";

        public const string Pages_Trades = "Pages.Trades";

        public const string Pages_CourrierPersons = "Pages.CourrierPersons";

        public const string Pages_Purchases = "Pages.Purchases";

        public const string Pages_ReceiptTypes = "Pages.ReceiptTypes";

        public const string Pages_Messages = "Pages.Messages";

        public const string Pages_Messages_Create = "Pages.Messages.Create";

        public const string Pages_Messages_Update = "Pages.Messages.Update";

        public const string Pages_Messages_Delete = "Pages.Messages.Delete";

        public const string Pages_Messages_SendAllTenant = "Pages.Messages.SendAllTenant";

        public const string Pages_Messages_SendAllUser = "Pages.Messages.SendAllUser";

        public const string Pages_TransactionTypes = "Pages.TransactionTypes";

        public const string Pages_Providers = "Pages.Providers";

        public const string Pages_Articles = "Pages.Articles";

        public const string Pages_ArticlesTypes = "Pages.ArticlesTypes";

        public const string Pages_Brands = "Pages.Brands";

        public const string Pages_Units = "Pages.Units";

        public const string Pages_AccountBalance = "Pages.AccountBalances";

        public const string Pages_ViewPaymentProjection = "Pages.ViewPaymentProjections";

        public const string Pages_GenerateAllPaymentProjections = "Pages.GenerateAllPaymentProjections";

        public const string Pages_Districts = "Pages.Districts";

        public const string Pages_MasterTenants = "Pages.MasterTenants";

        public const string Pages_PaymentDates = "Pages.PaymentDates";

        public const string Pages_BankAccounts = "Pages.BankAccounts";

        public const string Pages_CourseValues = "Pages.CourseValues";

        public const string Pages_Periods = "Pages.Periods";

        public const string Pages_Concepts = "Pages.Concepts";

        public const string Pages_PaymentMethods = "Pages.PaymentMethods";

        public const string Pages_Receipts = "Pages.Receipts";
        public const string Pages_Receipts_Date = "Pages.Receipts.Date";
        public const string Pages_Receipts_DateRest = "Pages.Receipts.DateRest";
        public const string Pages_Receipts_Edit = "Pages.Receipts.Edit";
        public const string Pages_Receipts_Cancel = "Pages.Receipts.Cancel";
        public const string Pages_Receipts_CancelAuthorization = "Pages.Receipts.CancelAuthorization";
        public const string Pages_Receipts_Request = "Pages.Receipts.Request";
        public const string Pages_Receipts_Enrollment = "Pages.Receipts.Enrollment";
        public const string Pages_Receipts_General = "Pages.Receipts.General";
        public const string Pages_Receipts_Inventory = "Pages.Receipts.Inventory";

        public const string Pages_Transactions = "Pages.Transactions";
        public const string Pages_Transactions_Create = "Pages.Transactions.Create";
        public const string Pages_Transactions_Edit = "Pages.Transactions.Edit";
        public const string Pages_Transactions_Cancel = "Pages.Transactions.Cancel";
        public const string Pages_Transactions_CancelAuthorization = "Pages.Transactions.CancelAuthorization";
        public const string Pages_Transactions_Request = "Pages.Transactions.Request";

        public const string Pages_Deposits = "Pages.Deposits";

        public const string Pages_Students = "Pages_Students";

        public const string Pages_Catalogs = "Pages.Catalogs";

        public const string Pages_ReportsQueries = "Pages.ReportsQueries";

        public const string Pages_ReportsFilters = "Pages.ReportsFilters";

        public const string Pages_ReportsMain = "Pages.ReportsMain";

        public const string Pages_Relationships = "Pages.Relationships";

        public const string Pages_Cities = "Pages.Cities";

        public const string Pages_PreviousEnrollments = "Pages.PreviousEnrollments";

        public const string Pages_NextEnrollments = "Pages.NextEnrollments";

        public const string Pages_Enrollments = "Pages.Enrollments";
        public const string Pages_Enrollments_Create = "Pages.Enrollments.Create";
        public const string Pages_Enrollments_Edit = "Pages.Enrollments.Edit";
        public const string Pages_Enrollments_AddStudent = "Pages.Enrollments.AddStudent";
        public const string Pages_Enrollments_AssignPaymentProyection = "Pages.Enrollments.AssignPaymentProyection";
        public const string Pages_Enrollments_ViewStudents = "Pages.Enrollments.ViewStudents";
        public const string Pages_Enrollments_Delete = "Pages.Enrollments.Delete";
        public const string Pages_Students_Delete = "Pages.Students.Delete";
        public const string Pages_Students_PreviousDelete = "Pages.Students.PreviousDelete";
        public const string Pages_Students_NextDelete = "Pages.Students.NextDelete";
        public const string Pages_Enrollments_ChangePersonalId = "Pages.Enrollments.ChangePersonalId";

        public const string Pages_Bloods = "Pages.Bloods";

        public const string Pages_Provinces = "Pages.Provinces";

        public const string Pages_CourseSessions = "Pages.CourseSessions";

        public const string Pages_Religions = "Pages.Religions";

        public const string Pages_Regions = "Pages.Regions";

        public const string Pages_PrinterTypes = "Pages.PrinterTypes";

        public const string Pages_Origins = "Pages.Origins";

        public const string Pages_Illnesses = "Pages.Illnesses";

        public const string Pages_Genders = "Pages.Genders";

        public const string Pages_DocumentTypes = "Pages.DocumentTypes";

        public const string Pages_CommentTypes = "Pages.CommentTypes";

        public const string Pages_ChangeTypes = "Pages.ChangeTypes";

        public const string Pages_Banks = "Pages.Banks";

        public const string Pages_Allergies = "Pages.Allergies";

        public const string Pages_Occupations = "Pages.Occupations";

        public const string Pages_Levels = "Pages.Levels";

        public const string Pages_SessionStudentSelections = "Pages.SessionStudentSelections";
        public const string Pages_SessionStudentSelectionPrevious = "Pages.SessionStudentSelectionPrevious";


        //--------------------------------//
    }
}
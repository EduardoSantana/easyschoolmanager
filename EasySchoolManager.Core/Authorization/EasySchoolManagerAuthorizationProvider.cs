using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;
using System;

namespace EasySchoolManager.Authorization
{
    public class EasySchoolManagerAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Courses, L("Courses"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

            //No Borrar la linea siguiente, ya que se usa para agregar los nuevos permisos automaticamente.
            context.CreatePermission(PermissionNames.Pages_EvaluationPeriods, L("EvaluationPeriods"));

            context.CreatePermission(PermissionNames.Pages_Evaluations, L("Evaluations"));

            context.CreatePermission(PermissionNames.Pages_GrantEnterprises, L("GrantEnterprises"));


            //    context.CreatePermission(PermissionNames.Pages_HistoryCancelReceipts, L("HistoryCancelReceipts"));

            context.CreatePermission(PermissionNames.Pages_SubjectHighs, L("SubjectHighs"));

            context.CreatePermission(PermissionNames.Pages_EvaluationPositionHighs, L("EvaluationPositionHighs"));


            context.CreatePermission(PermissionNames.Pages_EvaluationOrderHighs, L("EvaluationOrderHighs"));


            context.CreatePermission(PermissionNames.Pages_EvaluationHighs, L("EvaluationHighs"));

            context.CreatePermission(PermissionNames.Pages_EvaluationHighsByPos, L("EvaluationHighsByPos"));

            context.CreatePermission(PermissionNames.Pages_EvaluationHighsByPosPrevious, L("EvaluationHighsByPosPrevious"));

            context.CreatePermission(PermissionNames.Pages_TransactionQueues, L("TransactionQueues"));


            context.CreatePermission(PermissionNames.Pages_TaxReceipts, L("TaxReceipts"));

            context.CreatePermission(PermissionNames.Pages_TaxReceiptTypes, L("TaxReceiptTypes"));


            context.CreatePermission(PermissionNames.Pages_Companies, L("Companies"));

            context.CreatePermission(PermissionNames.Pages_CancelStatus, L("CancelStatus"));

            context.CreatePermission(PermissionNames.Pages_SubjectSessions, L("SubjectSessions"));

            context.CreatePermission(PermissionNames.Pages_SubjectSessionHighs, L("SubjectSessionHighs"));

            context.CreatePermission(PermissionNames.Pages_ConceptTenantRegistrations, L("ConceptTenantRegistrations"));

            context.CreatePermission(PermissionNames.Pages_VerifoneBreaks, L("VerifoneBreaks"));

            context.CreatePermission(PermissionNames.Pages_Discounts, L("Discounts"));

            context.CreatePermission(PermissionNames.Pages_DiscountNames, L("DiscountNames"));

            context.CreatePermission(PermissionNames.Pages_PeriodDiscounts, L("PeriodDiscounts"));

            //XXXInsertAuthorizationProviderXXX

            context.CreatePermission(PermissionNames.Pages_Indicators, L("Indicators"));

            context.CreatePermission(PermissionNames.Pages_EvaluationLegends, L("EvaluationLegends"));

            context.CreatePermission(PermissionNames.Pages_IndicatorGroups, L("IndicatorGroups"));

            context.CreatePermission(PermissionNames.Pages_Subjects, L("Subjects"));

            context.CreatePermission(PermissionNames.Pages_Teachers, L("Teachers"));

            //context.CreatePermission(PermissionNames.Pages_T2Dimensions, L("T2Dimensions"));

            //context.CreatePermission(PermissionNames.Pages_T10Dimensions, L("T10Dimensions"));

            //context.CreatePermission(PermissionNames.Pages_T9Dimensions, L("T9Dimensions"));

            //context.CreatePermission(PermissionNames.Pages_T8Dimensions, L("T8Dimensions"));

            //context.CreatePermission(PermissionNames.Pages_T7Dimensions, L("T7Dimensions"));

            //context.CreatePermission(PermissionNames.Pages_T6Dimensions, L("T6Dimensions"));

            //context.CreatePermission(PermissionNames.Pages_T5Dimensions, L("T5Dimensions"));

            //context.CreatePermission(PermissionNames.Pages_T4Dimensions, L("T4Dimensions"));

            //context.CreatePermission(PermissionNames.Pages_T1Dimensions, L("T1Dimensions"));

            //context.CreatePermission(PermissionNames.Pages_T3Dimensions, L("T3Dimensions"));

            context.CreatePermission(PermissionNames.Pages_Positions, L("Positions"));

            context.CreatePermission(PermissionNames.Pages_InventoryShops, L("InventoryShops"));

            context.CreatePermission(PermissionNames.Pages_Inventory, L("Inventory"));

            context.CreatePermission(PermissionNames.Pages_ButtonPositions, L("ButtonPositions"));

            context.CreatePermission(PermissionNames.Pages_TransactionShops, L("TransactionShops"));

            context.CreatePermission(PermissionNames.Pages_ClientShops, L("ClientShops"));

            context.CreatePermission(PermissionNames.Pages_PurchaseShops, L("PurchaseShops"));

            context.CreatePermission(PermissionNames.Pages_ProviderShops, L("ProviderShops"));

            context.CreatePermission(PermissionNames.Pages_ArticleShops, L("ArticleShops"));

            context.CreatePermission(PermissionNames.Pages_Messages, L("Messages"));
            context.CreatePermission(PermissionNames.Pages_Messages_Create, L("Messages_Create"));
            context.CreatePermission(PermissionNames.Pages_Messages_Update, L("Messages_Update"));
            context.CreatePermission(PermissionNames.Pages_Messages_Delete, L("Messages_Delete"));
            context.CreatePermission(PermissionNames.Pages_Messages_SendAllTenant, L("Messages_SendAllTenant"));
            context.CreatePermission(PermissionNames.Pages_Messages_SendAllUser, L("Messages_SendAllUser"));

            context.CreatePermission(PermissionNames.Pages_Trades, L("Trades"));

            context.CreatePermission(PermissionNames.Pages_CourrierPersons, L("CourrierPersons"));

            context.CreatePermission(PermissionNames.Pages_Purchases, L("Purchases"));

            context.CreatePermission(PermissionNames.Pages_ReceiptTypes, L("ReceiptTypes"));

            context.CreatePermission(PermissionNames.Pages_TransactionTypes, L("TransactionTypes"));

            context.CreatePermission(PermissionNames.Pages_Providers, L("Providers"));

            context.CreatePermission(PermissionNames.Pages_Articles, L("Articles"));

            context.CreatePermission(PermissionNames.Pages_ArticlesTypes, L("ArticlesTypes"));

            context.CreatePermission(PermissionNames.Pages_Brands, L("Brands"));

            context.CreatePermission(PermissionNames.Pages_Units, L("Units"));

            context.CreatePermission(PermissionNames.Pages_Districts, L("Districts"));

            context.CreatePermission(PermissionNames.Pages_AccountBalance, L("AccountBalances"));

            context.CreatePermission(PermissionNames.Pages_ViewPaymentProjection, L("ViewPaymentProjections"));

            context.CreatePermission(PermissionNames.Pages_GenerateAllPaymentProjections, L("GenerateAllPaymentProjections"));

            context.CreatePermission(PermissionNames.Pages_MasterTenants, L("MasterTenants"));

            context.CreatePermission(PermissionNames.Pages_BankAccounts, L("BankAccounts"));

            context.CreatePermission(PermissionNames.Pages_PaymentDates, L("PaymentDates"));

            context.CreatePermission(PermissionNames.Pages_CourseValues, L("CourseValues"));

            context.CreatePermission(PermissionNames.Pages_Periods, L("Periods"));

            context.CreatePermission(PermissionNames.Pages_Concepts, L("Concepts"));

            context.CreatePermission(PermissionNames.Pages_PaymentMethods, L("PaymentMethods"));

            context.CreatePermission(PermissionNames.Pages_Receipts, L("Receipts"));
            context.CreatePermission(PermissionNames.Pages_Receipts_Edit, L("Receipts_Edit"));
            context.CreatePermission(PermissionNames.Pages_Receipts_Date, L("Receipts_Date"));
            context.CreatePermission(PermissionNames.Pages_Receipts_DateRest, L("Receipts_DateRest"));
            context.CreatePermission(PermissionNames.Pages_Receipts_Cancel, L("Receipts_Cancel"));
            context.CreatePermission(PermissionNames.Pages_Receipts_CancelAuthorization, L("Receipts_CancelAuthorization"));
            context.CreatePermission(PermissionNames.Pages_Receipts_Request, L("Receipts_Request"));
            context.CreatePermission(PermissionNames.Pages_Receipts_Enrollment, L("Receipts_Enrollment"));
            context.CreatePermission(PermissionNames.Pages_Receipts_General, L("Receipts_General"));
            context.CreatePermission(PermissionNames.Pages_Receipts_Inventory, L("Receipts_Inventory"));

            context.CreatePermission(PermissionNames.Pages_Transactions, L("Transactions"));
            context.CreatePermission(PermissionNames.Pages_Transactions_Create, L("Transactions_Create"));
            context.CreatePermission(PermissionNames.Pages_Transactions_Edit, L("Transactions_Edit"));
            context.CreatePermission(PermissionNames.Pages_Transactions_Cancel, L("Transactions_Cancel"));
            context.CreatePermission(PermissionNames.Pages_Transactions_CancelAuthorization, L("Transactions_CancelAuthorization"));
            context.CreatePermission(PermissionNames.Pages_Transactions_Request, L("Transactions_Request"));

            context.CreatePermission(PermissionNames.Pages_Deposits, L("Deposits"));

            context.CreatePermission(PermissionNames.Pages_Catalogs, L("Catalogs"));

            context.CreatePermission(PermissionNames.Pages_ReportsQueries, L("ReportsQueries"));

            context.CreatePermission(PermissionNames.Pages_ReportsFilters, L("ReportsFilters"));

            context.CreatePermission(PermissionNames.Pages_ReportsMain, L("Reports"));

            context.CreatePermission(PermissionNames.Pages_Relationships, L("Relationships"));

            context.CreatePermission(PermissionNames.Pages_Cities, L("Cities"));

            context.CreatePermission(PermissionNames.Pages_PreviousEnrollments, L("PreviousEnrollments"));

            context.CreatePermission(PermissionNames.Pages_NextEnrollments, L("NextEnrollments"));

            context.CreatePermission(PermissionNames.Pages_Enrollments, L("Enrollments"));
            context.CreatePermission(PermissionNames.Pages_Enrollments_ChangePersonalId, L("Enrollments_ChangeEnrollmentId"));
            context.CreatePermission(PermissionNames.Pages_Enrollments_Create, L("Enrollments_Create"));
            context.CreatePermission(PermissionNames.Pages_Enrollments_Edit, L("Enrollments_Edit"));
            context.CreatePermission(PermissionNames.Pages_Enrollments_AddStudent, L("Enrollments_AddStudent"));
            context.CreatePermission(PermissionNames.Pages_Enrollments_ViewStudents, L("Enrollments_ViewStudents"));
            context.CreatePermission(PermissionNames.Pages_Enrollments_AssignPaymentProyection, L("Enrollments_AssignPaymentProyection"));
            context.CreatePermission(PermissionNames.Pages_Enrollments_Delete, L("Enrollments_Delete"));
            context.CreatePermission(PermissionNames.Pages_Students_Delete, L("Students_Delete"));
            context.CreatePermission(PermissionNames.Pages_Students_PreviousDelete, L("Students_PreviousDelete"));
            context.CreatePermission(PermissionNames.Pages_Students_NextDelete, L("Students_NextDelete"));

            context.CreatePermission(PermissionNames.Pages_Bloods, L("Bloods"));

            context.CreatePermission(PermissionNames.Pages_Provinces, L("Provinces"));

            context.CreatePermission(PermissionNames.Pages_CourseSessions, L("CourseSessions"));

            context.CreatePermission(PermissionNames.Pages_Religions, L("Religions"));

            context.CreatePermission(PermissionNames.Pages_Regions, L("Regions"));

            context.CreatePermission(PermissionNames.Pages_PrinterTypes, L("PrinterTypes"));

            context.CreatePermission(PermissionNames.Pages_Origins, L("Origins"));

            context.CreatePermission(PermissionNames.Pages_Illnesses, L("Illnesses"));

            context.CreatePermission(PermissionNames.Pages_Genders, L("Genders"));

            context.CreatePermission(PermissionNames.Pages_DocumentTypes, L("DocumentTypes"));

            context.CreatePermission(PermissionNames.Pages_CommentTypes, L("CommentTypes"));

            context.CreatePermission(PermissionNames.Pages_ChangeTypes, L("ChangeTypes"));

            context.CreatePermission(PermissionNames.Pages_Banks, L("Banks"));

            context.CreatePermission(PermissionNames.Pages_Allergies, L("Allergies"));

            context.CreatePermission(PermissionNames.Pages_Occupations, L("Occupations"));

            context.CreatePermission(PermissionNames.Pages_Levels, L("Levels"));

            context.CreatePermission(PermissionNames.Pages_Reports, L("Reports"));

            try
            {

                var query = Abp.Dependency.IocManager.Instance.Resolve<Abp.Domain.Repositories.IRepository<Models.Reports>>().GetAllList();
                foreach (var item in query)
                {
                    if (!string.IsNullOrEmpty(item.PermissionName))
                    {
                        context.CreatePermission(item.PermissionName.Replace(" ", ""), L(item.PermissionName.Replace(" ", "")));
                    }
                }

            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.StackTrace != null)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }

            context.CreatePermission(PermissionNames.Pages_Profile, L("Profile"));

            context.CreatePermission(PermissionNames.Pages_SessionStudentSelections, L("SessionStudentSelections"));

            context.CreatePermission(PermissionNames.Pages_SessionStudentSelectionPrevious, L("SessionStudentSelectionPrevious"));

            //-----------------------------------------/           
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, EasySchoolManagerConsts.LocalizationSourceName);
        }
    }
}

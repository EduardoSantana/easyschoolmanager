﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.GD
{
    public class GdPagedResultRequestDto : LimitedResultRequestDto, IPagedResultRequest, ILimitedResultRequest
    {
        public string TextFilter { get; set; }

        [Range(0, int.MaxValue)]
        public virtual int SkipCount { get; set; }

        public int? PeriodId { get; set; }

        public int? TenantId { get; set; }

        public bool? PreviousPeriod { get; set; }

        public bool? NextPeriod { get; set; }


    }

}

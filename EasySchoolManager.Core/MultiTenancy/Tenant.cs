﻿using Abp.MultiTenancy;
using EasySchoolManager.Authorization.Users;
using EasySchoolManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasySchoolManager.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            TeacherTenants = new HashSet<TeacherTenants>();
        }
        [StringLength(150)]
        public override string TenancyName { get => base.TenancyName; set => base.TenancyName = value; }

        [StringLength(150)]
        public override string Name { get => base.Name; set => base.Name = value; }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
            TeacherTenants = new HashSet<TeacherTenants>();
        }

        [StringLength(15)]
        public string Phone1 { get; set; }

        [StringLength(15)]
        public string Phone2 { get; set; }

        [StringLength(15)]
        public string Phone3 { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        public int? CityId { get; set; }

        [StringLength(100)]
        public string DirectorName { get; set; }

        [StringLength(100)]
        public string RegisterName { get; set; }

        public int? PeriodId { get; set; }

        public int? PreviousPeriodId { get; set; }

        public int? NextPeriodId { get; set; }

        public int? DistrictId { get; set; }

        [StringLength(15)]
        public string AccreditationNumber { get; set; }

        [StringLength(15)]
        public string EducationalDistrict { get; set; }

        [StringLength(60)]
        public string Regional { get; set; }

        [StringLength(60)]
        public string RegionalDirector { get; set; }

        [StringLength(60)]
        public string DistritctDirector { get; set; }

        [StringLength(350)]
        public string Comment { get; set; }

        [ForeignKey("CityId")]
        public virtual Cities Cities { get; set; }

        [ForeignKey("PeriodId")]
        public virtual Periods Periods { get; set; }

        [ForeignKey("PreviousPeriodId")]
        public virtual Periods PreviousPeriods { get; set; }

        [ForeignKey("NextPeriodId")]
        public virtual Periods NextPeriods { get; set; }

        [ForeignKey("DistrictId")]
        public virtual Districts Districts { get; set; }

        public int? PrinterTypeId { get; set; }

        //Es para ver el tipo de impresora que se estara utilizando en un colegio determinado.
        [ForeignKey("PrinterTypeId")]
        public virtual PrinterTypes PrinterTypes { get; set; }

        [StringLength(15)]
        public string Abbreviation { get; set; }
        
        [DefaultValue(false)]
        public bool ApplyAutomaticCharges { get; set; }


        public string LegalName
        {
            get
            {
                if (this.Cities != null && 
                    this.Cities.Provinces != null && 
                    this.Cities.Provinces.Regions != null && 
                    !string.IsNullOrEmpty(this.Cities.Provinces.Regions.LegalName))
                {
                    return this.Cities.Provinces.Regions.LegalName;
                }

                return "";
            }
        }

        public string Rnc
        {
            get
            {
                if (this.Cities != null &&
                    this.Cities.Provinces != null &&
                    this.Cities.Provinces.Regions != null &&
                    !string.IsNullOrEmpty(this.Cities.Provinces.Regions.Rnc))
                {
                    return this.Cities.Provinces.Regions.Rnc;
                }

                return "";
            }
        }

        public string Email { get; set; }
        /// <summary>
        /// Porcentaje que sera retenido de los pagos por tarjeta de credito
        /// </summary>
        [DefaultValue(0.0000)]
        public decimal CreditCardPercent { get; set; }
        public DateTime? FreeDateReceipt { get; set; }


        public virtual ICollection<TeacherTenants> TeacherTenants { get; set; }
        public virtual ICollection<ConceptTenantRegistrations> ConceptTenantRegistrations { get; set; }

    }
}
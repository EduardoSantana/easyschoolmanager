﻿namespace EasySchoolManager
{
    public class EasySchoolManagerConsts
    {
        public const string LocalizationSourceName = "EasySchoolManager";

        public const bool MultiTenancyEnabled = true;
        public static class Origin
        {
            public const int Debito = 1;
            public const int Credito = 2;
        }

        public static class ReceiptType {
            public const int MontlyCharge = 1;
            public const int Aditional = 2;
            public const int Articles = 3;
        }
        public static class TransactionType
        {
            public const int Receipts = 1;
            public const int Deposits = 2;
            public const int Transaction = 3;
            public const int CreditCardReceipts = 4;
        }
        public static class PaymentMethods
        {
            public const int Cash = 1;
            public const int CreditCard = 2;
            public const int Transfer = 3;
        }
        public static class Status
        {
            public const int Creado = 1;
            public const int Anulado = 2;
        }

        public static class DataType
        {
            public const string Number = "01";
            public const string Text = "02";
            public const string Date = "03";
            public const string Bit = "04";
        }

        public static class DataSource
        {
            public const string ViewOrTable = "01";
            public const string Procedure = "02";
            public const string Query = "03";
            public const string Report = "04";
        }

        public static class User {
            public const string DefaultUserPicture = @"data:image/png;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCABKAEoDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD5/p8MMtxMkMEbySuwVERSWYnsAOtEMMlxNHDCjSSyMERFGSxPAA96+kPh78PbbwjZJd3iJLrUq/vJOogB/gT39T3+nUA4jwv8E7m5RLrxHctaIeRaQYMv/Am6L9OT9K9O0zwH4W0hVFrolqzL/wAtJ181/wA2z+ldFWDrvi3TdBbyZS091jPkRYyP949B/OgDYW0tVXatpbqvoIVA/lWdf+FfD+qKRe6LYS5/i8gK3/fS4P61yLfE6ff8mkw7P9qY5/lW3o3jzTdTmW3uUaync4XewKMfTd2/GgDkfEfwR0+5jebw9dvaTdRb3DF429g3VfxzXjWsaLqOgag9jqdrJbXCc7WHDD1B6Ee4r69rF8T+F9N8WaU1jqMfIBMM6j54W9R7eo6GgD5NorV8ReH73wzrc+l36ASxHKuPuyKejL7GsqgD1v4J+F1ur648R3UeUtD5NqCP+WpHzN/wEEfi3tXuNc54C0xdI8CaPahdrNbieT/ef5j/ADA/CujoAo6zqH9laNeXwALQxllB7t0H6kV4fLLJPM80zl5ZGLO7dWJ6mvaPE8AuPC+pxkgfuGYEnAyOf6V4pQAUdRiiigD1rwLq8uqaEYrhi89o4iLHqy4ypPv1H4V09cV8NYQmjXk+QTJcBcZ6bVH+NdrQB5/8W/C6a54VfUYY832mAyqQOWi/jX8PvD6H1r5zr7LZElRo5FDRuCrg9weCPyr5K1bQrrT9ZvrJIXZbe4kiB9QrEf0oA+r7JVXTrRV+6IIwPptFT1j+E78ap4P0e9BB8y0jDY/vKNrfqDWxQBk+KLaa88MajBAC0jRZCjq2CCR+QrxTqM19A1494004ad4nuAihYrgCdABgc9R+YNAHP0UVLbW0l5dw2sQJkmcRrj1JxQB6P8NbaWPSLy4cERTTDy899owT+Zx+FdrUcEEdrbRW8QAjiQIoHoBipKACvNNYsrN9bv2ZV3G5kJ+u416YBlgOma+YNd8Y3c/iHU5oGBhku5WjOf4S5I/SgD0T4IeI1n0y68OzP++t2Nxbg/xIfvAfQ8/8CNetV8g6Pq13oWr22p2Mmy4t33Kex9QfUEZB+tfTOieOtD1rw8mr/a4rYD5Z4Hb54n/u46n2I6j8aAOlrzj4l3kEl5ZWabWnhVnkI6qGxhf0zS6z8RZpd0Ojw+SnT7RKMufovQfjmuGkkkmleWV2eRzuZ2OSx9SaAG1teEr230/xPZz3W0RZKF26IWGA34GsWigD6BoryfQvHGoaRGltcL9stF4VWOHQegb+hrvLXxfoV1p816b+OCOBN8yz/KyD6d/wzQBS+IfiNfDXg68uFfbdXCm3thnnew5P4DJ/Kvlyur8feMpvGOvG4UNHYQAx2kJ6he7H/aPU/gO1cpQAVPaXk9jOJoH2sOvoR6GoKKAOzsPEVpdALMRby+jH5T9D/jWwCGXcpBU9CDkV5pVi1uJ4ZVEU0iAn+FiKAPQ6Kw4rmcwZM0hOOu81g6ld3LTFGuJSvoXOKAOrvdYsrEEPKHkH/LOM5P8A9auT1PV59ScB/khU5WMdB7n1NZ9FABRRRQB//9k=";
        }
    }
}
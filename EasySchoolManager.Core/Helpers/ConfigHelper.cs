﻿using System.Configuration;

namespace EasySchoolManager.Helpers
{
    public class ConfigHelper
    {
        /// <summary>
        /// Obtiene la ruta donde sera guardado el log de errores de la aplicacion
        /// </summary>
        /// <returns></returns>
        public static string GetErrorTextLogPath()
        {
            string TextLogPath = @"C:\EasySchoolManager";
            try
            {
                TextLogPath = ConfigurationManager.AppSettings["ErrorTextLogPath"].ToString();
            }
            catch
            {
            }
            return TextLogPath;
        }

        /// <summary>
        /// Obtiene la ruta donde sera guardado el log de errores de la aplicacion
        /// </summary>
        /// <returns></returns>
        public static string GetMainConnection()
        {
            string conection = "";
            try
            {
                conection = ConfigurationManager.ConnectionStrings["Default"].ToString();
            }
            catch
            {
            }
            return conection;
        }

        /// <summary>
        /// Obtiene la ruta donde sera guardado el log informaciones ( no errores) que ocurren en el sistema y que desean mantenerse en un archivo de texto.
        /// </summary>
        /// <returns></returns>
        public static string GetInfoTextLogPath()
        {
            string InfoTextLogPath = @"C:\EasySchoolManager";
            try
            {
                 InfoTextLogPath = ConfigurationManager.AppSettings["InfoTextLogPath"].ToString();
            }
            catch
            {
            }
            return InfoTextLogPath;
        }

    }
}

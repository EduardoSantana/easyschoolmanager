﻿using Castle.Core.Logging;
using System;

namespace EasySchoolManager.Helpers
{
    public class Utils
    {


        /// <summary>
        /// Funcion utilizada para devolver un valor no null de un nulleable boolean
        /// </summary>
        /// <param name="ValueToReturn"></param>
        /// <returns></returns>
        public static bool GetBoolValue(bool? ValueToReturn)
        {
            if (ValueToReturn.HasValue)
                return ValueToReturn.Value;
            else
                return false;
        }

        /// <summary>
        /// Funcion utilizada para devolver un valor no null de un nulleable boolean
        /// </summary>
        /// <param name="ValueToReturn"></param>
        /// <returns></returns>
        public static bool GetBoolValue(string ValueToReturn)
        {
            if (string.IsNullOrEmpty(ValueToReturn))
                return false;

            if (ValueToReturn.ToUpper() == "S")
                return true;
            else
                return false;
        }

        /// <summary>
        /// Funcion utilizada para devolver un valor no null de un string   
        /// </summary>
        /// <param name="ValueToReturn"></param>
        /// <returns></returns>
        public static String GetStringValue(string ValueToReturn)
        {
            if (string.IsNullOrEmpty(ValueToReturn))
                return "";

            return ValueToReturn;
        }

        /// <summary>
        /// This method get the final date ( the last second of the date day)
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime getInitialDate(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
        }

        /// <summary>
        /// This method get the final date ( the last second of the date day)
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime getFinalDate(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }

        public const String MessageFieldLengh = "No puede exceder los {1} caracteres de longitud";
        public static string rutaArchivoError = string.Empty;
        public static string rutaArchivoInfo = string.Empty;

        /// <summary>
        /// Guarda un mensaje en el log de errores
        /// </summary>
        /// <param name="err"></param>
        /// <param name="Logger"></param>
        public static void LogException(Exception err, ILogger Logger)
        {
            LogException(err, Logger, "MyCollege");
        }

        /// <summary>
        /// Guardar el mensaje en el log de informacion
        /// </summary>
        /// <param name="Message"></param>
        public static void LogInfo(String Message)
        {
            GuardarInfoEnLog(Message, "MyCollege");
        }

        public static void LogException(Exception err)
        {
            LogException(err, "MyCollege");
        }

        public static void LogException(Exception err, ILogger Logger, String MetodoDondeOcurrioElError)
        {
            GuardarMensageEnLog(err.Message, MetodoDondeOcurrioElError, Logger);
            GuardarMensageEnLog(err.StackTrace, MetodoDondeOcurrioElError, Logger);

            Logger.Error(err.Message);
            Logger.Error(err.StackTrace);
            if (err.InnerException != null)
            {
                GuardarMensageEnLog(err.InnerException.Message, MetodoDondeOcurrioElError, Logger);
                GuardarMensageEnLog(err.InnerException.StackTrace, MetodoDondeOcurrioElError, Logger);

                Logger.Error(err.InnerException.Message);
                Logger.Error(err.InnerException.StackTrace);
            }
        }

        public static void LogException(Exception err, String MetodoDondeOcurrioElError, bool IsError = true)
        {
            if (string.IsNullOrEmpty(MetodoDondeOcurrioElError))
                MetodoDondeOcurrioElError = "";
            if (IsError)
            {
                GuardarErrorEnLog(err.Message, MetodoDondeOcurrioElError);
                GuardarErrorEnLog(err.StackTrace, MetodoDondeOcurrioElError);
                if (err.InnerException != null)
                {
                    GuardarErrorEnLog(err.InnerException.Message, MetodoDondeOcurrioElError);
                    GuardarErrorEnLog(err.InnerException.StackTrace, MetodoDondeOcurrioElError);
                }
            }
            else
            {
                GuardarInfoEnLog(err.Message, MetodoDondeOcurrioElError);
                GuardarInfoEnLog(err.StackTrace, MetodoDondeOcurrioElError);
                if (err.InnerException != null)
                {
                    GuardarInfoEnLog(err.InnerException.Message, MetodoDondeOcurrioElError);
                    GuardarInfoEnLog(err.InnerException.StackTrace, MetodoDondeOcurrioElError);
                }
            }

        }


        public static void LogException(Exception ex, object logger)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Metodo para repetir algun caracter o frase un numero determinado de veces.
        /// </summary>
        /// <param name="StringToReplicate"></param>
        /// <param name="NumeroDeRepeticiones"></param>
        /// <returns></returns>
        public static string Replicate(string StringToReplicate, int NumeroDeRepeticiones)
        {
            string returnedValue = "";
            if (string.IsNullOrEmpty(StringToReplicate) || NumeroDeRepeticiones < 1)
                return returnedValue;
            for (int i = 0; i < NumeroDeRepeticiones; i++)
            {
                returnedValue += StringToReplicate;
            }
            return returnedValue;
        }



        public static DateTime getDAte(String value)
        {
            if (string.IsNullOrEmpty(value))
                return new DateTime(1900, 1, 1);
            try
            {
                return Convert.ToDateTime(value);
            }
            catch (Exception)
            {
                try
                {
                    var DateNew = new DateTime(int.Parse(value.Substring(0, 2)),
                                      int.Parse(value.Substring(3, 2)),
                                    int.Parse(value.Substring(6, 4)));
                    return DateNew;
                }
                catch (Exception)
                {
                    return new DateTime(1900, 1, 1);
                }
            }
        }

        public static DateTime getDateFrom_DDMMYYYY(String value)
        {
            if (string.IsNullOrEmpty(value))
                return new DateTime(1900, 1, 1);
            try
            {
                var DateNew = new DateTime(
                                     day: int.Parse(value.Substring(0, 2)),
                                     month: int.Parse(value.Substring(3, 2)),
                                     year: int.Parse(value.Substring(6, 4)));
                return DateNew;
            }
            catch (Exception)
            {
                try
                {

                    return Convert.ToDateTime(value);
                }
                catch (Exception)
                {
                    return new DateTime(1900, 1, 1);
                }
            }
        }

        public static DateTime? getDateFrom_DDMMYYYY_Nulleable(String value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            try
            {
                var DateNew = new DateTime(
                                     day: int.Parse(value.Substring(0, 2)),
                                     month: int.Parse(value.Substring(3, 2)),
                                     year: int.Parse(value.Substring(6, 4)));
                return DateNew;
            }
            catch (Exception)
            {
                try
                {
                    return Convert.ToDateTime(value);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }



        public static void GuardarInfoEnLog(String Information, String DondeOcurrioElError)
        {
            GuardarMensageEnLog(
                Information: Information,
                DondeOcurrioElError: DondeOcurrioElError,
                logger: null,
                IsError: false);
        }

        public static void GuardarErrorEnLog(String Error, String DondeOcurrioElError)
        {
            GuardarMensageEnLog(
                Information: Error,
                DondeOcurrioElError: DondeOcurrioElError,
                logger: null,
                IsError: true);
        }

        /// <summary>
        /// Guardar un mensaje indicando el error ocurrido.
        /// </summary>
        /// <param name="Information">Informacion que se pretende mostrar en el error</param>
        /// <param name="DondeOcurrioElError">Metodo en donde ocurrió el error.</param>
        public static void GuardarMensageEnLog(String Information, String DondeOcurrioElError, ILogger logger, bool IsError = true)
        {
            var DateOfDay = DateTime.Now.ToString("ddMMyyyy");

            if (!System.IO.Directory.Exists(ConfigHelper.GetErrorTextLogPath()))
                System.IO.Directory.CreateDirectory(ConfigHelper.GetErrorTextLogPath());

            if (!System.IO.Directory.Exists(ConfigHelper.GetInfoTextLogPath()))
                System.IO.Directory.CreateDirectory(ConfigHelper.GetInfoTextLogPath());

            if (string.IsNullOrEmpty(rutaArchivoError))
                rutaArchivoError = ConfigHelper.GetErrorTextLogPath() + @"\" + DateOfDay;

            if (string.IsNullOrEmpty(rutaArchivoInfo))
                rutaArchivoInfo = ConfigHelper.GetInfoTextLogPath() + @"\" + DateOfDay;

            var rutaArchivoErrorSeleccionado = rutaArchivoError;

            if (IsError)
            {
                rutaArchivoErrorSeleccionado += "_ErrorLog.txt";
            }
            else
            {
                rutaArchivoErrorSeleccionado = rutaArchivoInfo;

                rutaArchivoErrorSeleccionado += "_InfoLog.txt";
            }


            //.IO.StreamReader lectorY;
            try
            {
                System.IO.StreamWriter sw = null;
                System.IO.StreamWriter swi = null;
                if (!System.IO.File.Exists(rutaArchivoErrorSeleccionado))
                {
                    System.IO.File.Create(rutaArchivoErrorSeleccionado).Dispose();
                    swi = System.IO.File.AppendText(rutaArchivoErrorSeleccionado);
                    swi.WriteLine("Fecha|Hora|LugarDelError|Mensaje");
                    swi.Close();
                    swi.Dispose();
                }
                try
                {
                    sw = System.IO.File.AppendText(rutaArchivoErrorSeleccionado);
                    sw.WriteLine(string.Format("{0:dd/MM/yyyy}|{1:hh:mm:ss tt}|{2}|{3}", DateTime.Now, DateTime.Now, DondeOcurrioElError, Information));
                }
                catch (Exception err)
                {
                    if (logger != null)
                        logger.Error(err.Message);
                }
                finally
                {
                    sw.Close();
                    sw.Dispose();
                }
            }
            catch (Exception err)
            {
                // System.Windows.Forms.MessageBox.Show("Error : " + Err.Message);
            }
        }


        public static DateTime? getDateNullable(String value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            try
            {
                return Convert.ToDateTime(value);
            }
            catch (Exception)
            {
                try
                {
                    return new DateTime(day: int.Parse(value.Substring(0, 2)),
                                     month: int.Parse(value.Substring(3, 2)),
                                   year: int.Parse(value.Substring(6, 4)));
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}

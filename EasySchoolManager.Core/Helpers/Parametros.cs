﻿using System;

namespace EasySchoolManager.Helpers
{
    public class Parametros
    {
        public String Nombre { get; set; }
        public object Valor { get; set; }
        public Parametros()
        { }
        public Parametros(String nombre, object valor)
        {
            this.Nombre = nombre;
            this.Valor = valor;
        }

        public override string ToString()
        {
            return $"Nombre {this.Nombre}, Valor {this.Valor}";
        }
    }
}
﻿using Abp.Application.Services;
using Abp.Domain.Uow;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace EasySchoolManager.Helpers
{
    public class Conexion : ApplicationService, IApplicationService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        public Conexion(IUnitOfWorkManager unitOfWorkManager)
        {
            _unitOfWorkManager = unitOfWorkManager;
        }


        public Conexion()
        { }
        

        public static DataTable EjecutarConsulta(String consulta)
        {
            SqlConnection cn = new SqlConnection(ConfigHelper.GetMainConnection());

            return EjecutarConsulta(consulta, new List<Parametros>(), cn);
        }



        public static DataTable EjecutarComando(String consulta)
        {
            SqlConnection cn = new SqlConnection(ConfigHelper.GetMainConnection());

            return EjecutarComando(consulta, new List<Parametros>(), cn);
        }

        public static DataTable EjecutarConsulta(String consulta, SqlConnection conexion)
        {
            return EjecutarConsulta(consulta, new List<Parametros>(), conexion);
        }

        public static DataTable EjecutarConsulta(String consulta, List<Parametros> parametros)
        {
            SqlConnection cn = new SqlConnection(ConfigHelper.GetMainConnection());
            var tabla = EjecutarConsulta(consulta, parametros, cn);
            if (cn.State == ConnectionState.Open)
                cn.Close();
            cn.Dispose();
            return tabla;
        }

        public static DataTable EjecutarComando(String consulta, List<Parametros> parametros)
        {
            SqlConnection cn = new SqlConnection(ConfigHelper.GetMainConnection());
            var tabla = EjecutarComando(consulta, parametros, cn);
            if (cn.State == ConnectionState.Open)
                cn.Close();
            cn.Dispose();
            return tabla;
        }

        [UnitOfWork(isTransactional: false, IsDisabled = true)]
        public static DataTable EjecutarConsulta(String consulta, List<Parametros> parametros, SqlConnection conexion)
        {
            DataTable tb = new DataTable();



            try
            {
                SqlCommand cmd = new SqlCommand(consulta, conexion);
                foreach (var item in parametros)
                {
                    cmd.Parameters.AddWithValue(item.Nombre, item.Valor);
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                if (conexion.State != ConnectionState.Open)
                    conexion.Open();
                da.Fill(tb);
            }
            catch (Exception err)
            {
                Utils.LogException(err);
            }

            return tb;
        }


        [UnitOfWork(isTransactional: false, IsDisabled = true)]
        public static DataTable EjecutarComando(String consulta, List<Parametros> parametros, SqlConnection conexion)
        {

            DataTable tb = new DataTable(); try
            {
                SqlCommand cmd = new SqlCommand(consulta, conexion);
                foreach (var item in parametros)
                {
                    cmd.Parameters.AddWithValue(item.Nombre, item.Valor);
                }

                if (conexion.State != ConnectionState.Open)
                    conexion.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                throw err;
            }


            return tb;
        }
        

        [UnitOfWork(isTransactional: false, IsDisabled = true)]
        public DataTable EjecutarConsultaNonStatic(String consulta, List<Parametros> parametros, SqlConnection conexion)
        {
            DataTable tb = new DataTable();

            using (var uow = _unitOfWorkManager.Begin(System.Transactions.TransactionScopeOption.Suppress))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(consulta, conexion);
                    foreach (var item in parametros)
                    {
                        cmd.Parameters.AddWithValue(item.Nombre, item.Valor);
                    }
                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    if (conexion.State != ConnectionState.Open)
                        conexion.Open();
                    da.Fill(tb);
                }
                catch (Exception err)
                {
                    Utils.LogException(err);
                     throw new UserFriendlyException(err.Message);
                }

                return tb;
            }
        }

    }
}

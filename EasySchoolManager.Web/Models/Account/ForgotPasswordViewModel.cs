﻿using System.ComponentModel.DataAnnotations;
using Abp.Auditing;

namespace EasySchoolManager.Web.Models.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        public string UsernameOrEmailAddress { get; set; }

        public bool IsLogin { get; set; }
    }
}
using Abp.AutoMapper;
using EasySchoolManager.Sessions.Dto;

namespace EasySchoolManager.Web.Models.Account
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}
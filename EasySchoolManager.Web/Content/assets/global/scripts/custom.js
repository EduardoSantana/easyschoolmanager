﻿//Global Variables
var PERSONAL_ID_LENGHT = 11;
var PERSONAL_ID_WITH_HYPEN_LENGHT = 13;


var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

function getMonthByNumber(number)
{
    try {
        return meses[number - 1 ];
    } catch (e)
    { }
    return '';
}


/**
 * //Funcion usada para repetir un caracter una cantidad de veces determinada.   (CustomJS to be Found it XXXJS keyword frase)
 * @param {any} letter Letra que e pretende repetir
 * @param {any} numberOfTimes Numero de veces que desea repetir
 */
function repeat(letter, numberOfTimes) {
    var r = "";
    if (letter == null || numberOfTimes === 0 || numberOfTimes == null)
        return r;
    for (var i = 0; i < numberOfTimes; i++) {
        r += letter;
    }
    return r;
}

/**
 * Ordena un array por un campo en particular
 * @param {any} array Lista que contiene todos los elementos del arreglo
 * @param {any} key Campo por el cual se estará ordenando el arreglo
 */
function sortArrayByKey(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

/**
 * funcion usada para completar con ceros a la izquierda una determinada cadena
 * @param {any} value Numero que deseamos usar para completar un formato de ceros a la izquierda
 * @param {any} numberOfPositions Cantidad de posiciones que deseamos repetir para formar el formato.
 */
function completeWithCeros(value, numberOfPositions) {
    var characer = "0";
    var returnedValue = "";
    try {
        var _long = numberOfPositions - value.toString().length;
        returnedValue += repeat(characer, _long);
        returnedValue += value;
    } catch (e) { }
    return returnedValue;
}

/**
 * Se valida que sea un email correcto
 * @param {any} email Email que se desea validar
 */
function isValidEmail(email) {
    var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
    if (caract.test(email) == false) {
        return false;
    } else {
        return true;
    }
}

/**
 * funcion usada para que siempre devuelva un valor o en caso de que no exista devolvera un string en blanco
 * @param {any} o Valor que se desea obtener su correspondiente string.
 */
function getObjectOrString(o) {
    var returnedValue = "";
    try {
        if (o === undefined || o === null || o.length == 0)
            return returnedValue;
        returnedValue = o;
    } catch (e)
    { }
    return returnedValue;
}


//funcion usada para verificar si una cedula tiene la longitud adecuada
validatePersonalId = function(personalId, canBeEmpty)
{
    var isValidated = false;
    try {
        if (canBeEmpty === true) {
            if (personalId == null || personalId.length === 0) {
                return true;
            }
        }
        if (personalId.length === PERSONAL_ID_LENGHT)
            isValidated = true;
    } catch (e) { }
    return isValidated;
}

//function usada para permitir solo numeros, espacios y guiones en un input type text
function JustNumberSpaceAndHyphen(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8) return true; // para la tecla de retroseso
    else if (tecla == 0 || tecla == 9) return true; //<-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla se esta transformando a 0 asi que porsiacaso los dos
    patron = /[0-9\s-]/;// -> solo letras
    // patron =/[0-9\s]/;// -> solo numeros
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

//function usada para permitir solo numeros
function JustNumbers(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8) return true; // para la tecla de retroseso
    else if (tecla == 0 || tecla == 9) return true; //<-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla se esta transformando a 0 asi que porsiacaso los dos
    patron = /[0-9\s]/;// -> solo letras
    // patron =/[0-9\s]/;// -> solo numeros
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function getObjectValueOrEmptyString(o) {
    if (o === undefined || o === null)
        return "";
    return o;
}

function getJustDate(DateAndTimeValue)
{
    return DateAndTimeValue.toString().substr(0, 10).replace("-","/").replace("-","/");
}
function getDateYMD(DateAndTimeValue)
{ 
    var year = DateAndTimeValue.getFullYear().toString();
    var month = (DateAndTimeValue.getMonth() + 1).toString();
    if (month.length == 1)
        month = "0" + month;
    var day = DateAndTimeValue.getDate().toString();
    if (day.length == 1)
        day = "0" + day;

    return year + "/" + month + "/" + day;
}

function getDateDMY(DateAndTimeValue) {
    var year = DateAndTimeValue.getFullYear().toString();
    var month = (DateAndTimeValue.getMonth() + 1).toString();
    if (month.length == 1)
        month = "0" + month;
    var day = DateAndTimeValue.getDate().toString();
    if (day.length == 1)
        day = "0" + day;

    return day + "/" + month + "/" +year;
}


get2Dec = function (decimalValue) {
    var returnedValue = parseFloat(decimalValue.toFixed(2));
    return returnedValue;
}


//Utilizado para que siempre devuelva un valor double, en caso de que exista error en el tipo de dato devolvera cero
getDoubleOrCero = function (vDouble) {
    if (vDouble === undefined || vDouble === null || vDouble.length == 0)
        return 0.00;
    try {
        return parseFloat(vDouble);
    } catch (e) {
        return 0.00;
    }
}

//Utilizado para que siempre devuelva un valor numerico, en caso de que exista error en el tipo de dato devolvera cero
getIntOrCero = function (vint) {
    if (vint === undefined || vint === null || vint.length == 0)
        return 0;
    try {
        return parseInt(vint);
    } catch (e) {
        return 0;
    }
}

//Utilizado para sumar un arregle de valores doubles o enteros
function sumArrayOfValues(array) {
    var total = 0.00;
    for (var i = 0; i < array.length; i++) {
        total += parseFloat(array[i]);
    }
    return total;
}

function enterToTab(e,object)
{
    if (e.which == 13) {
        var inputs = $(':input:enabled,:button');
        e.preventDefault();
        var nextInput = inputs.get(inputs.index(object) + 1);
        if (nextInput) {
            nextInput.focus();
            
        }
    }
}





//funcion usada para istribuir un monto en un numero de veces determinado 
distributeAmount_SimpleMethod = function (amount, numberOfItems) {
    var distribution = [];
    if (amount == null || amount === 0 || numberOfItems == null || numberOfItems.length === 0)
        return distribution;

    var amountSplited = parseFloat(getDoubleOrCero(amount / numberOfItems).toFixed(2), 2);

    console.log(amountSplited);

    for (var i = 0; i < numberOfItems; i++) {
        distribution.push(amountSplited);
    }

    console.log(distribution);

    var totalAmount = sumArrayOfValues(distribution)
    console.log("total Amount " + totalAmount.toString());

    var difference = get2Dec(amount - totalAmount);
    if(difference > 0)
        distribution[0] = get2Dec(parseFloat(distribution[0]) + get2Dec(difference));
    else if (difference < 0)
    {
        distribution[distribution.length - 1] = get2Dec(parseFloat(distribution[distribution.length - 1]) + get2Dec(difference));
    }

    console.log(distribution);
    return distribution;
}


/*************************************************************/
// NumeroALetras
// The MIT License (MIT)
// 
// Copyright (c) 2015 Luis Alfredo Chee 
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// @author Rodolfo Carmona
// @contributor Jean (jpbadoino@gmail.com)
/*************************************************************/
function Unidades(num) {

    switch (num) {
        case 1: return "UN";
        case 2: return "DOS";
        case 3: return "TRES";
        case 4: return "CUATRO";
        case 5: return "CINCO";
        case 6: return "SEIS";
        case 7: return "SIETE";
        case 8: return "OCHO";
        case 9: return "NUEVE";
    }

    return "";
}//Unidades()

function Decenas(num) {

    decena = Math.floor(num / 10);
    unidad = num - (decena * 10);

    switch (decena) {
        case 1:
            switch (unidad) {
                case 0: return "DIEZ";
                case 1: return "ONCE";
                case 2: return "DOCE";
                case 3: return "TRECE";
                case 4: return "CATORCE";
                case 5: return "QUINCE";
                default: return "DIECI" + Unidades(unidad);
            }
        case 2:
            switch (unidad) {
                case 0: return "VEINTE";
                default: return "VEINTI" + Unidades(unidad);
            }
        case 3: return DecenasY("TREINTA", unidad);
        case 4: return DecenasY("CUARENTA", unidad);
        case 5: return DecenasY("CINCUENTA", unidad);
        case 6: return DecenasY("SESENTA", unidad);
        case 7: return DecenasY("SETENTA", unidad);
        case 8: return DecenasY("OCHENTA", unidad);
        case 9: return DecenasY("NOVENTA", unidad);
        case 0: return Unidades(unidad);
    }
}//Unidades()

function DecenasY(strSin, numUnidades) {
    if (numUnidades > 0)
        return strSin + " Y " + Unidades(numUnidades)

    return strSin;
}//DecenasY()

function Centenas(num) {
    centenas = Math.floor(num / 100);
    decenas = num - (centenas * 100);

    switch (centenas) {
        case 1:
            if (decenas > 0)
                return "CIENTO " + Decenas(decenas);
            return "CIEN";
        case 2: return "DOSCIENTOS " + Decenas(decenas);
        case 3: return "TRESCIENTOS " + Decenas(decenas);
        case 4: return "CUATROCIENTOS " + Decenas(decenas);
        case 5: return "QUINIENTOS " + Decenas(decenas);
        case 6: return "SEISCIENTOS " + Decenas(decenas);
        case 7: return "SETECIENTOS " + Decenas(decenas);
        case 8: return "OCHOCIENTOS " + Decenas(decenas);
        case 9: return "NOVECIENTOS " + Decenas(decenas);
    }

    return Decenas(decenas);
}//Centenas()

function Seccion(num, divisor, strSingular, strPlural) {
    cientos = Math.floor(num / divisor)
    resto = num - (cientos * divisor)

    letras = "";

    if (cientos > 0)
        if (cientos > 1)
            letras = Centenas(cientos) + " " + strPlural;
        else
    letras = strSingular;

    if (resto > 0)
        letras += "";

    return letras;
}//Seccion()

function Miles(num) {
    divisor = 1000;
    cientos = Math.floor(num / divisor)
    resto = num - (cientos * divisor)

    strMiles = Seccion(num, divisor, "UN MIL", "MIL");
    strCentenas = Centenas(resto);

    if (strMiles == "")
    return strCentenas;

    return strMiles + " " + strCentenas;
}//Miles()

function Millones(num) {
    divisor = 1000000;
    cientos = Math.floor(num / divisor)
    resto = num - (cientos * divisor)

    strMillones = Seccion(num, divisor, "UN MILLON DE", "MILLONES DE");
    strMiles = Miles(resto);

    if (strMillones == "")
    return strMiles;

    return strMillones + " " + strMiles;
}//Millones()

function NumeroALetras(num) {
    var data = {
        numero: num,
        enteros: Math.floor(num),
        centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
        letrasCentavos: "",
        letrasMonedaPlural: 'PESOS',//"PESOS", 'Dólares', 'Bolívares', 'etcs'
        letrasMonedaSingular: 'PESO', //"PESO", 'Dólar', 'Bolivar', 'etc'

        letrasMonedaCentavoPlural: "CENTAVOS",
        letrasMonedaCentavoSingular: "CENTAVO"
};

if (data.centavos > 0) {
    data.letrasCentavos = "CON " + (function () {
        if (data.centavos == 1)
            return Millones(data.centavos) + " " + data.letrasMonedaCentavoSingular;
            else
        return Millones(data.centavos) + " " + data.letrasMonedaCentavoPlural;
    })();
};

if (data.enteros == 0)
    return "CERO " + data.letrasMonedaPlural + " " + data.letrasCentavos;
if (data.enteros == 1)
    return Millones(data.enteros) + " " + data.letrasMonedaSingular + " " + data.letrasCentavos;
    else
return Millones(data.enteros) + " " + data.letrasMonedaPlural + " " + data.letrasCentavos;
}//NumeroALetras()



function convertDateFormat_DMY_to_YMD(date, digitYear)
{
    try {
        var year = date.substr(6, digitYear);
        var month = date.substr(3, 2);
        var day = date.substr(0, 2);
        return new Date(year, month - 1, day);
    } catch (e)
    {
        return null;
    }
}

function convertDateFormat_YMD_to_DMY(dateString, digitYear) {
    try {
        var year = dateString.substr(0, digitYear);
        var month = dateString.substr(digitYear + 1, 2);
        var day = dateString.substr(digitYear + 4, 2);
        
        if (month.length == 1)
            month = "0" + month;
        if (day.length == 1)
            day = "0" + day;

        return day + "/" + month + "/" + year;
    } catch (e) {
        return null;
    }
}

function getDateDMY(DateAndTimeValue) {
    var year = DateAndTimeValue.getFullYear();
    var month = (DateAndTimeValue.getMonth() + 1).toString();
    if (month.length == 1)
        month = "0" + month;
    var day = DateAndTimeValue.getDate();
    if (day.length == 1)
        day = "0" + day;

    return day + "/" + month + "/" + year;
}



var regex = /^[0-9]{2}[\/][0-9]{2}[\/][0-9]{4}$/g;

function validateDateDMY(dateToValidate)
{
    var validated = false;
    try {
        var formatIsValid = regex.test(dateToValidate);

        if (formatIsValid == true) {
            var year = parseInt(dateToValidate.substr(6, 4));
            var month = parseInt(dateToValidate.substr(3, 2));
            var day = parseInt(dateToValidate.substr(0, 2));

            if (day < 1)
                validated = false;
            else if (day > 31)
                validated = false;
            else if (month < 0)
                validated = false;
            else if (month > 12)
                validated = false;
            else if (year < 1000)
                validated = false;
            else
                validated = true;
        }
    }
    catch (e)
    {
        validated = false;
    }
    return validated;
}

function isDateDMY(str) {
    var parms = str.split(/[\.\-\/]/);
    var yyyy = parseInt(parms[2], 10);
    var mm = parseInt(parms[1], 10);
    var dd = parseInt(parms[0], 10);
    var date = new Date(yyyy, mm - 1, dd, 12, 0, 0, 0);
    return mm === (date.getMonth() + 1) &&
        dd === date.getDate() &&
        yyyy === date.getFullYear();
}


//Este metodo es para obtener el nombre de un elemento de un arrego de combobox
function getNameById(arrayList, id) {
    var element = "";
    var itemnSelected = Object.values(arrayList).find(function (x) { return x.id == id });
    if (itemnSelected != null)
        element = itemnSelected.name;
    return element;
}

    /**
    * Function usada para obtener un elemento de un arreglo, indicando el campo clave y el valor
    *
    *@param {array} array The array that contains all the records
    *
    *@param {string} keyField The field name used to find the record.
    *
    *@param {string} value The value used to filter the element.
    *
    *return {row} Return the found element that matcheed with the values.
    */
function getElementByKeyField(array, keyField, value)
{
    var returnedValued = null;
    for (var i = 0; i < array.length; i++) {
        var ele = array[i];
        if (ele[keyField] == value) {
            returnedValued = ele;
            break;
        }
    }
    return returnedValued;
}

function getMax(array, keyField) {
    if (array == null)
        return null;
    var returnedValued = array[0] ;
    for (var i = 0; i < array.length; i++) {
        var ele = array[i];
        if (ele[keyField] > returnedValued[keyField]) {
            returnedValued = ele;
        }
    }
    return returnedValued;
}




﻿function Pagination(skipCount, maxResultCount) {
    this.skipCount = skipCount;
    this.maxResultCount = maxResultCount;
    this.currentPage = 1;
    this.textFilter = "";
    this.totalRecords = 0;
    this.totalPages = 0;

    this.reload = null;

    //Determinamos cuantas paginas tiene la cantidad de paginas que tenemos con la cantidad de registros encontrados.
    this.calculateTotalPages = function () {
        var pages = Math.floor(this.totalRecords / this.maxResultCount);
        if ((this.totalRecords % this.maxResultCount) > 0)
            pages++;
        this.totalPages = pages;
    }

    this.calculateRecordsSkip = function () {
        this.skipCount = (this.currentPage - 1) * this.maxResultCount;
    }

    //Se asignan el total de registros encontrados en la consulta
    this.assignTotalRecords = function (totalRecords) {
        this.totalRecords = totalRecords;
        this.calculateTotalPages();
    }

    this.goToPage = function (pageNumber) {
        if (this.totalPages < pageNumber || pageNumber === undefined || pageNumber ===null)
            this.currentPage = this.totalPages;
        else
            this.currentPage = pageNumber;
        this.calculateRecordsSkip();
        if (this.reload != null)
            this.reload();
    }

    //Se va a la primera pagina
    this.first = function () {
        if (this.currentPage > 1) {
            this.currentPage = 1;
            this.calculateRecordsSkip();
            if (this.reload != null)
                this.reload();
        }
    }

    //Se va a la pagina prior
    this.prior = function () {
        if (this.currentPage > 1) {
            this.currentPage--;
            this.calculateRecordsSkip();
            if (this.reload != null)
                this.reload();
        }
    }

    //se va a la pagina next
    this.next = function () {
        if (this.currentPage < this.totalPages) {
            this.currentPage++;
            this.calculateRecordsSkip();
            if (this.reload != null)
                this.reload();
        }
    }

    //se va a la last pagina
    this.last = function () {
        if (this.currentPage < this.totalPages) {
            this.currentPage = this.totalPages;
            this.calculateRecordsSkip();
            if (this.reload != null)
                this.reload();
        }
    }
}

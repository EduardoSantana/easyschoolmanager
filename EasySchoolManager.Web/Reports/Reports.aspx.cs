﻿using Microsoft.Reporting.WebForms;
using EasySchoolManager.EntityFramework;
using EasySchoolManager.Web.Reports.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EasySchoolManager.Reports;
using EasySchoolManager.Web.Controllers;
using Abp.Domain.Repositories;
using EasySchoolManager.Authorization.Users;
using Abp.Localization;
using Abp.Application.Services;

namespace EasySchoolManager.Web.Reports
{
    public partial class Reports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Request.Form.Count > 0 || Request.QueryString.Count > 0)
            //{
            //    var r = new AutomaticReports(3, Request);
            //    var d = r.ImprimirReporteAutomatico();
            //    EjecuarReporte(d);
            //}
            //else
            //{
            //    ReportViewer1.Visible = false;
            //    divMensaje.Visible = true;
            //}
        }
        public void EjecuarReporte(ReportsResults data)
        {
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath(data.ReportsUrlName);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(data.DataSetName, data.Data));

            //var claseDosEjecuar = new HomeController();
            int indice = 1;
            foreach (ReportParameterInfo item in ReportViewer1.LocalReport.GetParameters())
            {
                //var rp = new ReportParameter(item.Name, claseDosEjecuar.TraductorValue(item.Name));
                var rp = new ReportParameter(item.Name, item.Name + indice.ToString());
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp });
                indice++;
            }

            if (!data.ReportsType.Contains("web"))
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                byte[] bytes = ReportViewer1.LocalReport.Render(
                    data.ReportsType,
                    null,
                    out mimeType,
                    out encoding,
                    out filenameExtension,
                    out streamids,
                    out warnings);

                using (FileStream fs = new FileStream(Server.MapPath(data.ReportsOutPutFileName + "." + filenameExtension), FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }

                string pdfPath = MapPath(data.ReportsOutPutFileName + "." + filenameExtension);
                Response.ContentType = mimeType;
                Response.AppendHeader("content-disposition", "attachment; filename=" + data.ReportsOutPutFileName + "." + filenameExtension);
                Response.TransmitFile(pdfPath);
                Response.End();
            }
        }
    }
}
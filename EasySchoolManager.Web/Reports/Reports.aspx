﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="EasySchoolManager.Web.Reports.Reports" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reportes - EasySchoolManager</title>
</head>
<body style="margin: 0px;background-color: #F44336;">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
            <div runat="server" id="divMensaje" visible="false" style="margin-left: auto;
                    margin-right: auto;
                    text-align: center;
                    color: white;
                }">
                <h1>NO REPORTE CARGADO</h1>
            </div>
        </div>
    </form>
</body>
</html>

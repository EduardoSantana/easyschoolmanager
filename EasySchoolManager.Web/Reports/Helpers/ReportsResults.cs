﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace EasySchoolManager.Web.Reports.Helpers
{
    public class ReportsResults
    {
        public ReportsResults()
        {
            this.Data = new DataTable();
        }
        private string _ReportsOutPutFileName { get; set; }
        public string ReportsType { get; set; }
        public string ReportsUrlName { get; set; }
        public string ReportsOutPutFileName { get { return _ReportsOutPutFileName; } set { _ReportsOutPutFileName = value.Replace(" ", ""); } }
        public string DataSetName { get; set; }
        public string ResourceFile { get; set; }
        public List<FilterItem> filters { get; set; }
        public DataTable Data { get; set; }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasySchoolManager.Web.Reports.Helpers
{
    public class FilterItem
    {
        public string key { get; set; }
        public string value { get; set; }
        public string valueHasta { get; set; }
        public string type { get; set; }
    }
}
﻿namespace EasySchoolManager.Web.Controllers
{
    public class UserTenantLocalDto
    {
        public long userId;
        public int tenantId;
        public string name;
        public string tenancyName;

        public UserTenantLocalDto(long userId, int tenantId, string name, string tenancyName)
        {
            this.userId = userId;
            this.tenantId = tenantId;
            this.name = name;
            this.tenancyName = tenancyName;
        }
    }
}
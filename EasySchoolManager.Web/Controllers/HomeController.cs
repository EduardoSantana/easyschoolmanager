﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using EasySchoolManager.Authorization.Users;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using EasySchoolManager.Transactions;
using System.Threading.Tasks;
using System;
using System.Linq;
using Abp.Runtime.Caching;

namespace EasySchoolManager.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : EasySchoolManagerControllerBase
    {
        private readonly UserManager _userManager;
        private readonly ICacheManager _cacheManager;

        public HomeController(UserManager userManager, ITransactionAppService transactionsRepository,
            ICacheManager cacheManager)
        {
            _userManager = userManager;
            _cacheManager = cacheManager;
        }
        public ActionResult Index()
        {


            var user = _userManager.FindById(AbpSession.UserId.Value);

            if (user.MustChangePassword != null && (bool)user.MustChangePassword)
            {
                return Redirect(Url.Action("ChangePassword", "Account"));
            }
            
            return View("~/App/Main/views/layout/layout.cshtml"); //Layout of the angular application.
        }



        public async Task<JsonResult> SessionToken()
        {

            //Create a cache item
            var cacheItem = new SwitchToLinkedAccountCacheItem(
                AbpSession.TenantId,
                AbpSession.UserId ?? 0,
                AbpSession.ImpersonatorTenantId,
                AbpSession.ImpersonatorUserId
                );

            //Create a random token and save to the cache
            var TokenId = Guid.NewGuid().ToString();
            await _cacheManager
                .GetSwitchToLinkedAccountCache()
                .SetAsync(TokenId, cacheItem, TimeSpan.FromMinutes(1));

            int _tenantId = 0;
            //var claimsIdentity = System.Threading.Thread.CurrentPrincipal as System.Security.Claims.ClaimsPrincipal;
            //var currentAgency = claimsIdentity?.Claims?.FirstOrDefault(c => c.Type == PersonalizedClaimTypes.DefaultCurrentAgencyId);
            //Int32.TryParse(currentAgency?.Value, out _CurrentAgencyId);


            string host = $"{Request.Url.Scheme}://{Request.Url.Host}:{Request.Url.Port}{Request.ApplicationPath}";
            var result = new
            {
                LoginPath = $"Account/SignInToken?tokenId={TokenId}",
                ReportPath = "api/services/app/report/GetReportDirect",
                ChangeTenantPath = "Account/ChangeTenant",
                TenantId = _tenantId,
                ApplicationPath = host
            };

            return AbpJson(result, contentType: "application/json", behavior: JsonRequestBehavior.AllowGet);
        }
    }
}
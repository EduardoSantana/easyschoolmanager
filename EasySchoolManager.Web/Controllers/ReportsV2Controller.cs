﻿using Abp.Auditing;
using Abp.Web.Security.AntiForgery;
using EasySchoolManager.Authorization.Users;
using EasySchoolManager.MasterTenants;
using EasySchoolManager.MultiTenancy;
using EasySchoolManager.Reports;
using EasySchoolManager.Reports.Dto;
using EasySchoolManager.Web.Helpers;
using EasySchoolManager.Web.Reports.Helpers;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace EasySchoolManager.Web.Controllers
{
    public class RetValReportJson
    {
        public string data64 { get; set; }
        public string contentType { get; set; }
        public string nombre { get; set; }
    }
    public class ReportsV2Controller : EasySchoolManagerControllerBase
    {

        private const string FOLDER_REPORTS = "/Reports/";
        private readonly UserManager _userManager;
        private readonly TenantManager _tenantManager;
        private readonly MasterTenantAppService _masterTenant;
        private readonly IReportAppService _reportService;

        public ReportsV2Controller(UserManager userManager, TenantManager tenantManager,
            MasterTenantAppService masterTenant, IReportAppService reportService)
        {
            _userManager = userManager;
            _tenantManager = tenantManager;
            _masterTenant = masterTenant;
            _reportService = reportService;
        }

        [HttpPost]
        [DisableAuditing]
        [DisableAbpAntiForgeryTokenValidation]
        public ActionResult Index()
        {
            //var r = new AutomaticReports(3, Request);
            //var d = r.ImprimirReporteAutomatico(AbpSession.TenantId == null ? 0 : AbpSession.TenantId.Value);
            //return EjecuarReporte(d);
            return View();
        }
        public ActionResult EjecuarReporte(ReportsResults data)
        {

            var ReportViewer1 = new ReportViewer();
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath(FOLDER_REPORTS + data.ReportsUrlName);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(data.DataSetName, data.Data));

            foreach (ReportParameterInfo item in ReportViewer1.LocalReport.GetParameters())
            {
                if (item.Name.Contains("Label"))
                {
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { new ReportParameter(item.Name, L((!string.IsNullOrEmpty(data.ResourceFile) ? data.ResourceFile.ToTitleCase() : "") + item.Name.Replace("Label", "").ToTitleCase())) });
                }
                if (item.Name.Contains("Filter"))
                {
                    string name = item.Name.Replace("Filter", "").ToTitleCase();
                    var filter = data.filters.FirstOrDefault(p => p.key == name);
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { new ReportParameter(item.Name, filter.value) });
                }
                if (item.Name.Contains("AbpTenants"))
                {
                    string r = "";
                    if (AbpSession.TenantId.HasValue)
                    {
                        var tenant = _tenantManager.Tenants.Where(f => f.Id == AbpSession.TenantId.Value).FirstOrDefault();
                        r = tenant.GetAttributeValue(item.Name.Replace("AbpTenants", "").ToTitleCase());
                    }
                    else
                    {
                        var tenant = _masterTenant.MasterTenants().FirstOrDefault();
                        r = tenant.GetAttributeValue(item.Name.Replace("AbpTenants", "").ToTitleCase());
                    }
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { new ReportParameter(item.Name, r) });
                }
                if (item.Name.Contains("AbpUsers"))
                {
                    if (AbpSession.UserId.HasValue)
                    {
                        var user = _userManager.Users.Where(f => f.Id == AbpSession.UserId.Value).FirstOrDefault();
                        var r = user.GetAttributeValue(item.Name.Replace("AbpUsers", "").ToTitleCase());
                        ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { new ReportParameter(item.Name, r) });
                    }
                }
                if (!item.Name.Contains("Label") &&
                    !item.Name.Contains("Filter") &&
                    !item.Name.Contains("AbpTenants") &&
                    !item.Name.Contains("AbpUsers"))
                {
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { new ReportParameter(item.Name, L(item.Name.ToTitleCase())) });
                }
            }

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            if (data.ReportsType.ToUpper() == "PDF")
            {
                return View(new RetValReportJson()
                {
                    data64 = Convert.ToBase64String(ReportViewer1.LocalReport.Render(data.ReportsType.ToUpper(),
                                                null,
                                                out mimeType,
                                                out encoding,
                                                out filenameExtension,
                                                out streamids,
                                                out warnings)),
                    contentType = mimeType,
                    nombre = data.ReportsOutPutFileName
                });
            }

            byte[] bytes = ReportViewer1.LocalReport.Render(
             data.ReportsType,
             null,
             out mimeType,
             out encoding,
             out filenameExtension,
             out streamids,
             out warnings);

            string pdfPath = Server.MapPath(FOLDER_REPORTS + data.ReportsOutPutFileName + "." + filenameExtension);
            using (FileStream fs = new FileStream(pdfPath, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            Response.ContentType = mimeType;
            Response.AppendHeader("content-disposition", "attachment; filename=" + data.ReportsOutPutFileName + "." + filenameExtension);
            Response.TransmitFile(pdfPath);
            Response.End();

            return View(new RetValReportJson());
        }

        [HttpPost]
        [DisableAuditing]
        [DisableAbpAntiForgeryTokenValidation]
        public ActionResult GetReport([Bind(Include = "Id,ReportExport,ReportCode,ReportsFilters")] _ReportDto report)
        {
            var reportResult = (byte[])_reportService.GetDynamicReport(report, out string mimeType);
            return new FileStreamResult(new MemoryStream(reportResult), mimeType);
        }

    }
}
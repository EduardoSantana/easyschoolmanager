﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Configuration.Startup;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Localization;
using Abp.MultiTenancy;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.UI;
using Abp.Web.Models;
using EasySchoolManager.Authorization;
using EasySchoolManager.Authorization.Roles;
using EasySchoolManager.Authorization.Users;
using EasySchoolManager.MultiTenancy;
using EasySchoolManager.Sessions;
using EasySchoolManager.Web.Controllers.Results;
using EasySchoolManager.Web.Models;
using EasySchoolManager.Web.Models.Account;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Abp.Web.Mvc.Authorization;
using Abp.Net.Mail;
using Abp.Configuration;
using EasySchoolManager.Configuration;
using Abp.Domain.Repositories;
using EasySchoolManager.Transactions;
using EasySchoolManager.Helpers;
using System.IO;
using Abp.Runtime.Caching;
using Abp.Runtime.Security;
using Abp.Timing;
using System.Globalization;
using EasySchoolManager.Web.Auth;

namespace EasySchoolManager.Web.Controllers
{
    public class AccountController : EasySchoolManagerControllerBase
    {
        private readonly TenantManager _tenantManager;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IMultiTenancyConfig _multiTenancyConfig;
        private readonly LogInManager _logInManager;
        private readonly ISessionAppService _sessionAppService;
        private readonly ILanguageManager _languageManager;
        private readonly ITenantCache _tenantCache;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<EasySchoolManager.Models.UserTenants, long> _userTenantRepository;
        private readonly IRepository<EasySchoolManager.Models.UserPictures, long> _userPictureRepository;
        private readonly ITransactionAppService _transactionsRepository;
        private readonly ICacheManager _cacheManager;
        IAuthenticationManager _authenticationManager;

        private string CurrentUserPassword
        {
            get
            {
                return Convert.ToString(Session["CurrentUserPassword"]);
            }
            set
            {
                Session["CurrentUserPassword"] = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public AccountController(
            TenantManager tenantManager,
            UserManager userManager,
            RoleManager roleManager,
            IUnitOfWorkManager unitOfWorkManager,
            IMultiTenancyConfig multiTenancyConfig,
            LogInManager logInManager,
            ISessionAppService sessionAppService,
            ILanguageManager languageManager,
            ITenantCache tenantCache,
            IEmailSender emailSender,
            ITransactionAppService transactionsRepository,
            IRepository<EasySchoolManager.Models.UserTenants, long> userTenantRepository,
            IRepository<EasySchoolManager.Models.UserPictures, long> userPictureRepository,
             ICacheManager cacheManager,
           IAuthenticationManager authenticationManager)
        {
            _tenantManager = tenantManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _unitOfWorkManager = unitOfWorkManager;
            _multiTenancyConfig = multiTenancyConfig;
            _logInManager = logInManager;
            _sessionAppService = sessionAppService;
            _languageManager = languageManager;
            _tenantCache = tenantCache;
            _emailSender = emailSender;
            _userTenantRepository = userTenantRepository;
            _transactionsRepository = transactionsRepository;
            _userPictureRepository = userPictureRepository;
            _cacheManager = cacheManager;
            _authenticationManager = authenticationManager;
        }

        #region Login / Logout

        public async Task<ActionResult> ChangeTenantCbo(int id, string returnUrl = "")
        {

            AbpLoginResult<Tenant, User> loginResult = null;

            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var tenancyName = "";
                if (id > -1)
                {
                    var t = await _tenantManager.GetByIdAsync(id);
                    tenancyName = t.TenancyName;
                }
                
                var currentUser = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
                if (currentUser != null)
                {
                    var userId = (AbpSession.TenantId.HasValue ? currentUser.ParentUserId : AbpSession.UserId);
                    var userInSelectedTenant = "";

                    if (id > -1)
                    {
                        var userInSelectedTenants = _userManager.Users.Where(x => x.ParentUserId == userId).ToList();
                        userInSelectedTenant = userInSelectedTenants.Where(x => x.Tenant.TenancyName == tenancyName).FirstOrDefault().UserName;
                    }
                    else
                    {
                        var userInSelectedTenants = _userManager.Users.Where(x => x.Id == userId).ToList();
                        userInSelectedTenant = userInSelectedTenants.FirstOrDefault().UserName;
                    }

                    if (!string.IsNullOrEmpty(userInSelectedTenant))
                    {
                        loginResult = await GetLoginResultAsync(
                                                            userInSelectedTenant,
                                                            CurrentUserPassword,
                                                            tenancyName
                                                        );
                    }

                    await SignInAsync(loginResult.User, loginResult.Identity, false);

                }

               
            }

            string retVal = "";
            if (!string.IsNullOrEmpty(returnUrl))
                retVal = Server.UrlDecode(returnUrl);

            return Redirect((Url.Action("Index", "Home") == "/" ? "" : Url.Action("Index", "Home")) + retVal);

        }


        public async Task<ActionResult> ValidateUserCode(long id, string q)
        {
            if (id < 1 || q.IsNullOrEmpty())
            {
                return HttpNotFound();
                //return RedirectToAction("Login", "Account", null);
            }

            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                //Now, we can search for a user name in all tenants
                var user = _userManager.Users.Where(u => u.Id == id && u.PasswordResetCode == q).FirstOrDefault();

                if (user != null && user.IsActive)
                {
                    string tempPassword = Authorization.Users.User.CreateRandomPassword();
                    user.Password = new PasswordHasher().HashPassword(tempPassword);
                    user.MustChangePassword = true;
                    user.PasswordResetCode = null;
                    await _unitOfWorkManager.Current.SaveChangesAsync();

                    var loginResult = await GetLoginResultAsync(
                        user.UserName,
                        tempPassword,
                        GetTenancyNameOrNull(user.UserName)
                    );

                    await SignInAsync(loginResult.User, loginResult.Identity, false);
                }
                else
                {
                    return HttpNotFound();
                    //return RedirectToAction("Login", "Account", null);
                }
            }

            return RedirectToAction("Index", "Home", null);

        }

        public ActionResult Login(string returnUrl = "")
        {
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                returnUrl = Request.ApplicationPath;
            }

            if (AbpSession.UserId.HasValue)
            {
                return RedirectToAction("Index", "Home", null); 
            }

            if (!AbpSession.UserId.HasValue && AbpSession.TenantId.HasValue)
            {
                AuthenticationManager.SignOut();
            }

            ViewBag.IsMultiTenancyEnabled = false; // _multiTenancyConfig.IsEnabled;

            return View(
                new LoginFormViewModel
                {
                    ReturnUrl = returnUrl,
                    IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled,
                    IsSelfRegistrationAllowed = IsSelfRegistrationEnabled(),
                    MultiTenancySide = AbpSession.MultiTenancySide,
                    IsLogin = true
                });
        }

        [HttpPost]
        [DisableAuditing]
        public async Task<JsonResult> Login(LoginViewModel loginModel, string returnUrl = "", string returnUrlHash = "")
        {
            CheckModelState();



            AbpLoginResult<Tenant, User> loginResult = null;
            if (loginModel.TenancyName == null || loginModel.TenancyName.Length == 0)
            {
                loginResult = await GetLoginResultAsync(
                   loginModel.UsernameOrEmailAddress,
                   loginModel.Password,
                   GetTenancyNameOrNull(loginModel.UsernameOrEmailAddress)
               );
            }
            else
            {
                var currentUser = _userManager.Users.Where(x => x.UserName == loginModel.UsernameOrEmailAddress).FirstOrDefault();
                if (currentUser != null)
                {
                    //Disabling MayHaveTenant filter, so we can reach to all users
                    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                    {
                        var userInSelectedTenants = _userManager.Users.Where(x => x.ParentUserId == currentUser.Id).ToList();
                        var userInSelectedTenant = userInSelectedTenants.Where(x => x.Tenant.TenancyName == loginModel.TenancyName).FirstOrDefault();
                        if (userInSelectedTenant != null)
                        {
                            loginResult = await GetLoginResultAsync(
                                                              userInSelectedTenant.UserName,
                                                              loginModel.Password,
                                                              loginModel.TenancyName
                                                          );
                        }
                    }
                }
                else
                {
                    loginResult = await GetLoginResultAsync(
                                                      loginModel.UsernameOrEmailAddress,
                                                      loginModel.Password,
                                                      loginModel.TenancyName
                                                  );
                }
            }

            //Prueba para ver si se cambiaba de tenant, y funciono exitosamente, solo que se busco otra forma de hacer loque se requeria.
            //Helpers.ChangeTenantService sv = new Helpers.ChangeTenantService(AbpSession);
            //sv.Test(1);
            //var sessionx = AbpSession.TenantId;

            await SignInAsync(loginResult.User, loginResult.Identity, loginModel.RememberMe);

            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                returnUrl = Request.ApplicationPath;
            }

            if (!string.IsNullOrWhiteSpace(returnUrlHash))
            {
                returnUrl = returnUrl + returnUrlHash;
            }

            return Json(new AjaxResponse { TargetUrl = returnUrl });
        }

        [HttpPost]
        [DisableAuditing]
        public async Task<JsonResult> ForgotPassword(ForgotPasswordViewModel loginModel, string returnUrl = "", string returnUrlHash = "")
        {

            if (ModelState.IsValid)
            {
                CheckModelState();

                string temporalPassword = Guid.NewGuid().ToString() + Guid.NewGuid().ToString() + Guid.NewGuid().ToString() + Guid.NewGuid().ToString();
                temporalPassword = temporalPassword.Replace("-", "");
                //Disabling MayHaveTenant filter, so we can reach to all users
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                {
                    //Now, we can search for a user name in all tenants
                    var user = await _userManager.FindByNameOrEmailAsync(loginModel.UsernameOrEmailAddress);

                    if (user == null)
                    {
                        // Don't reveal that the user does not exist or is not confirmed
                        ViewBag.IsValid = false;
                        //return View("Login", vm);
                        throw CreateExceptionForFailedLoginAttempt(AbpLoginResultType.InvalidUserNameOrEmailAddress, loginModel.UsernameOrEmailAddress, "");
                    }

                    //Save user
                    user.PasswordResetCode = temporalPassword;
                    await _unitOfWorkManager.Current.SaveChangesAsync();

                    string UrlSiteHost = SettingManager.GetSettingValue(AppSettingNames.UrlSiteHost);

                    dynamic email = new Postal.Email("ForgotPassword");
                    email.User = user;
                    email.UrlReset = $"{UrlSiteHost}/Account/ValidateUserCode/{user.Id}?q={temporalPassword}";

                    _emailSender.Send(
                           (new Postal.EmailService()).CreateMailMessage(email)
                        );

                }

                if (string.IsNullOrWhiteSpace(returnUrl))
                {
                    returnUrl = Request.ApplicationPath;
                }

                if (!string.IsNullOrWhiteSpace(returnUrlHash))
                {
                    returnUrl = returnUrl + returnUrlHash;
                }

                return Json(new AjaxResponse { TargetUrl = returnUrl });

            }

            // If we got this far, something failed, redisplay form
            throw CreateExceptionForFailedLoginAttempt(AbpLoginResultType.InvalidUserNameOrEmailAddress, loginModel.UsernameOrEmailAddress, "");
        }

        private async Task<AbpLoginResult<Tenant, User>> GetLoginResultAsync(string usernameOrEmailAddress, string password, string tenancyName)
        {
            try
            {
                var loginResult = await _logInManager.LoginAsync(usernameOrEmailAddress, password, tenancyName);

                switch (loginResult.Result)
                {
                    case AbpLoginResultType.Success:
                        CurrentUserPassword = password;
                        return loginResult;
                    default:
                        CurrentUserPassword = "";
                        throw CreateExceptionForFailedLoginAttempt(loginResult.Result, usernameOrEmailAddress, tenancyName);
                }
            }
            catch (Exception err)
            {
                throw new UserFriendlyException(err.Message);
            }
        }

        private async Task SignInAsync(User user, ClaimsIdentity identity = null, bool rememberMe = false)
        {
            if (identity == null)
            {
                identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            }

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = rememberMe }, identity);
        }

        private void SignIn(User user, ClaimsIdentity identity = null, bool rememberMe = false)
        {
            if (identity == null)
            {
                identity = _userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            }

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = rememberMe }, identity);
        }

        private Exception CreateExceptionForFailedLoginAttempt(AbpLoginResultType result, string usernameOrEmailAddress, string tenancyName)
        {
            switch (result)
            {
                case AbpLoginResultType.Success:
                    return new ApplicationException("Don't call this method with a success result!");
                case AbpLoginResultType.InvalidUserNameOrEmailAddress:
                case AbpLoginResultType.InvalidPassword:
                    return new UserFriendlyException(L("LoginFailed"), L("InvalidUserNameOrPassword"));
                case AbpLoginResultType.InvalidTenancyName:
                    return new UserFriendlyException(L("LoginFailed"), L("ThereIsNoTenantDefinedWithName{0}", tenancyName));
                case AbpLoginResultType.TenantIsNotActive:
                    return new UserFriendlyException(L("LoginFailed"), L("TenantIsNotActive", tenancyName));
                case AbpLoginResultType.UserIsNotActive:
                    return new UserFriendlyException(L("LoginFailed"), L("UserIsNotActiveAndCanNotLogin", usernameOrEmailAddress));
                case AbpLoginResultType.UserEmailIsNotConfirmed:
                    return new UserFriendlyException(L("LoginFailed"), "UserEmailIsNotConfirmedAndCanNotLogin");
                case AbpLoginResultType.LockedOut:
                    return new UserFriendlyException(L("LoginFailed"), L("UserLockedOutMessage"));
                default: //Can not fall to default actually. But other result types can be added in the future and we may forget to handle it
                    Logger.Warn("Unhandled login fail reason: " + result);
                    return new UserFriendlyException(L("LoginFailed"));
            }
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        #endregion

        #region Register

        public ActionResult Register()
        {
            return RegisterView(new RegisterViewModel());
        }

        [AbpMvcAuthorize]
        public ActionResult ChangePassword()
        {
            var user = _userManager.FindById(AbpSession.UserId.Value);
            var model = new ChangePasswordViewModel { EmailAddress = user.EmailAddress, Name = user.Name, Surname = user.Surname, UserName = user.UserName };
            return View(model);
        }

        private ActionResult RegisterView(RegisterViewModel model)
        {
            ViewBag.IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled;

            return View("Register", model);
        }

        private bool IsSelfRegistrationEnabled()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                return false; //No registration enabled for host users!
            }

            return false;
        }

        [HttpPost]
        public virtual async Task<ActionResult> Register(RegisterViewModel model)
        {
            return null;
            //try
            //{
            //    CheckModelState();

            //    //Create user
            //    var user = new User
            //    {
            //        Name = model.Name,
            //        Surname = model.Surname,
            //        EmailAddress = model.EmailAddress,
            //        IsActive = true
            //    };

            //    //Get external login info if possible
            //    ExternalLoginInfo externalLoginInfo = null;
            //    if (model.IsExternalLogin)
            //    {
            //        externalLoginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            //        if (externalLoginInfo == null)
            //        {
            //            throw new ApplicationException("Can not external login!");
            //        }

            //        user.Logins = new List<UserLogin>
            //        {
            //            new UserLogin
            //            {
            //                LoginProvider = externalLoginInfo.Login.LoginProvider,
            //                ProviderKey = externalLoginInfo.Login.ProviderKey
            //            }
            //        };

            //        if (model.UserName.IsNullOrEmpty())
            //        {
            //            model.UserName = model.EmailAddress;
            //        }

            //        model.Password = Authorization.Users.User.CreateRandomPassword();

            //        if (string.Equals(externalLoginInfo.Email, model.EmailAddress, StringComparison.InvariantCultureIgnoreCase))
            //        {
            //            user.IsEmailConfirmed = true;
            //        }
            //    }
            //    else
            //    {
            //        //Username and Password are required if not external login
            //        if (model.UserName.IsNullOrEmpty() || model.Password.IsNullOrEmpty())
            //        {
            //            throw new UserFriendlyException(L("FormIsNotValidMessage"));
            //        }
            //    }

            //    user.UserName = model.UserName;
            //    user.Password = new PasswordHasher().HashPassword(model.Password);

            //    //Switch to the tenant
            //    _unitOfWorkManager.Current.EnableFilter(AbpDataFilters.MayHaveTenant); //TODO: Needed?
            //    _unitOfWorkManager.Current.SetTenantId(AbpSession.GetTenantId());

            //    //Add default roles
            //    user.Roles = new List<UserRole>();
            //    foreach (var defaultRole in await _roleManager.Roles.Where(r => r.IsDefault).ToListAsync())
            //    {
            //        user.Roles.Add(new UserRole { RoleId = defaultRole.Id });
            //    }

            //    //Save user
            //    CheckErrors(await _userManager.CreateAsync(user));
            //    await _unitOfWorkManager.Current.SaveChangesAsync();

            //    //Directly login if possible
            //    if (user.IsActive)
            //    {
            //        AbpLoginResult<Tenant, User> loginResult;
            //        if (externalLoginInfo != null)
            //        {
            //            loginResult = await _logInManager.LoginAsync(externalLoginInfo.Login, GetTenancyNameOrNull());
            //        }
            //        else
            //        {
            //            loginResult = await GetLoginResultAsync(user.UserName, model.Password, GetTenancyNameOrNull());
            //        }

            //        if (loginResult.Result == AbpLoginResultType.Success)
            //        {
            //            await SignInAsync(loginResult.User, loginResult.Identity);
            //            return Redirect(Url.Action("Index", "Home"));
            //        }

            //        Logger.Warn("New registered user could not be login. This should not be normally. login result: " + loginResult.Result);
            //    }

            //    //If can not login, show a register result page
            //    return View("RegisterResult", new RegisterResultViewModel
            //    {
            //        TenancyName = GetTenancyNameOrNull(),
            //        NameAndSurname = user.Name + " " + user.Surname,
            //        UserName = user.UserName,
            //        EmailAddress = user.EmailAddress,
            //        IsActive = user.IsActive
            //    });
            //}
            //catch (UserFriendlyException ex)
            //{
            //    ViewBag.IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled;
            //    ViewBag.ErrorMessage = ex.Message;

            //    return View("Register", model);
            //}
        }

        [HttpPost]
        [AbpMvcAuthorize]
        public virtual async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                CheckModelState();

                if (model.Password.IsNullOrEmpty() || model.ConfirmPassword.IsNullOrEmpty())
                {
                    ViewBag.IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled;
                    ViewBag.ErrorMessage = "Password can not be null or empty!";
                    return View(model);
                }

                if (model.Password != model.ConfirmPassword)
                {
                    ViewBag.IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled;
                    ViewBag.ErrorMessage = "Password and Confirm Password must be the same!";
                    return View(model);
                }

                //Get current user
                var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);

                //ConfirmPassword and Password are required
                if (model.ConfirmPassword.IsNullOrEmpty() || model.Password.IsNullOrEmpty())
                {
                    throw new UserFriendlyException(L("FormIsNotValidMessage"));
                }

                //Save user
                CheckErrors(await _userManager.ChangePasswordAsync(user, model.Password));
                user.MustChangePassword = false;
                await _unitOfWorkManager.Current.SaveChangesAsync();


                //Si el usuario no posee ningun roll, le pondremos un rol de enrollment.
                var roles = await _userManager.GetRolesAsync(user.Id);
                if (roles.Count == 0)
                {
                    var enrollmentRole = _roleManager.Roles.FirstOrDefault(r => r.Name == StaticRoleNames.Tenants.Enrollment);
                    if (enrollmentRole != null)
                        CheckErrors(_userManager.AddToRole(user.Id, enrollmentRole.Name));
                }
                await ChangeAllUsertUnderThisUser(model.Password, user);

                return Redirect(Url.Action("Index", "Home"));

            }
            catch (UserFriendlyException ex)
            {
                ViewBag.IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled;
                ViewBag.ErrorMessage = ex.Message;

                return View(model);
            }
        }


        /// <summary>
        /// Este metodo es usado para asignar la misma contrase;a del usuario general a todos los usuarios que este tiene por debajo en los diferentes colegios.
        /// </summary>
        /// <param name="Password"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task ChangeAllUsertUnderThisUser(String Password, User user)
        {
            //Disabling MayHaveTenant filter, so we can reach to all users
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var usersInTenants = _userManager.Users.Where(x => x.ParentUserId == user.Id).ToList();

                foreach (var item in usersInTenants)
                {
                    await _userManager.ChangePasswordAsync(item, Password);
                }
            }
        }

        #endregion

        #region External Login

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ChallengeResult(
                provider,
                Url.Action(
                    "ExternalLoginCallback",
                    "Account",
                    new
                    {
                        ReturnUrl = returnUrl
                    })
            );
        }

        public virtual async Task<ActionResult> ExternalLoginCallback(string returnUrl, string tenancyName = "")
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            //Try to find tenancy name
            if (tenancyName.IsNullOrEmpty())
            {
                var tenants = await FindPossibleTenantsOfUserAsync(loginInfo.Login);
                switch (tenants.Count)
                {
                    case 0:
                        return await RegisterView(loginInfo);
                    case 1:
                        tenancyName = tenants[0].TenancyName;
                        break;
                    default:
                        return View("TenantSelection", new TenantSelectionViewModel
                        {
                            Action = Url.Action("ExternalLoginCallback", "Account", new { returnUrl }),
                            Tenants = tenants.MapTo<List<TenantSelectionViewModel.TenantInfo>>()
                        });
                }
            }

            var loginResult = await _logInManager.LoginAsync(loginInfo.Login, tenancyName);

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    await SignInAsync(loginResult.User, loginResult.Identity);

                    if (string.IsNullOrWhiteSpace(returnUrl))
                    {
                        returnUrl = Url.Action("Index", "Home");
                    }

                    return Redirect(returnUrl);
                case AbpLoginResultType.UnknownExternalLogin:
                    return await RegisterView(loginInfo, tenancyName);
                default:
                    throw CreateExceptionForFailedLoginAttempt(loginResult.Result, loginInfo.Email ?? loginInfo.DefaultUserName, tenancyName);
            }
        }

        private async Task<ActionResult> RegisterView(ExternalLoginInfo loginInfo, string tenancyName = null)
        {
            var name = loginInfo.DefaultUserName;
            var surname = loginInfo.DefaultUserName;

            var extractedNameAndSurname = TryExtractNameAndSurnameFromClaims(loginInfo.ExternalIdentity.Claims.ToList(), ref name, ref surname);

            var viewModel = new RegisterViewModel
            {
                EmailAddress = loginInfo.Email,
                Name = name,
                Surname = surname,
                IsExternalLogin = true
            };

            if (!tenancyName.IsNullOrEmpty() && extractedNameAndSurname)
            {
                return await Register(viewModel);
            }

            return RegisterView(viewModel);
        }

        protected virtual async Task<List<Tenant>> FindPossibleTenantsOfUserAsync(UserLoginInfo login)
        {
            List<User> allUsers;
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                allUsers = await _userManager.FindAllAsync(login);
            }

            return allUsers
                .Where(u => u.TenantId != null)
                .Select(u => AsyncHelper.RunSync(() => _tenantManager.FindByIdAsync(u.TenantId.Value)))
                .ToList();
        }

        private static bool TryExtractNameAndSurnameFromClaims(List<Claim> claims, ref string name, ref string surname)
        {
            string foundName = null;
            string foundSurname = null;

            var givennameClaim = claims.FirstOrDefault(c => c.Type == ClaimTypes.GivenName);
            if (givennameClaim != null && !givennameClaim.Value.IsNullOrEmpty())
            {
                foundName = givennameClaim.Value;
            }

            var surnameClaim = claims.FirstOrDefault(c => c.Type == ClaimTypes.Surname);
            if (surnameClaim != null && !surnameClaim.Value.IsNullOrEmpty())
            {
                foundSurname = surnameClaim.Value;
            }

            if (foundName == null || foundSurname == null)
            {
                var nameClaim = claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
                if (nameClaim != null)
                {
                    var nameSurName = nameClaim.Value;
                    if (!nameSurName.IsNullOrEmpty())
                    {
                        var lastSpaceIndex = nameSurName.LastIndexOf(' ');
                        if (lastSpaceIndex < 1 || lastSpaceIndex > (nameSurName.Length - 2))
                        {
                            foundName = foundSurname = nameSurName;
                        }
                        else
                        {
                            foundName = nameSurName.Substring(0, lastSpaceIndex);
                            foundSurname = nameSurName.Substring(lastSpaceIndex);
                        }
                    }
                }
            }

            if (!foundName.IsNullOrEmpty())
            {
                name = foundName;
            }

            if (!foundSurname.IsNullOrEmpty())
            {
                surname = foundSurname;
            }

            return foundName != null && foundSurname != null;
        }

        #endregion

        #region Common private methods

        private async Task<Tenant> GetActiveTenantAsync(string tenancyName)
        {
            var tenant = await _tenantManager.FindByTenancyNameAsync(tenancyName);
            if (tenant == null)
            {
                throw new UserFriendlyException(L("ThereIsNoTenantDefinedWithName{0}", tenancyName));
            }

            if (!tenant.IsActive)
            {
                throw new UserFriendlyException(L("TenantIsNotActive", tenancyName));
            }

            return tenant;
        }

        private string GetTenancyNameOrNull(string userName = "")
        {
            if (!AbpSession.TenantId.HasValue || !AbpSession.UserId.HasValue)
            {
                var tenanId = GetUserTenantId(userName);
                if (tenanId.HasValue)
                {
                    return _tenantCache.GetOrNull(tenanId.Value)?.TenancyName;
                }
                return null;
            }
            return _tenantCache.GetOrNull(AbpSession.TenantId.Value)?.TenancyName;
        }

        private int? GetUserTenantId(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return null;
            }
            //Disabling MayHaveTenant filter, so we can reach to all users
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                //Now, we can search for a user name in all tenants
                var users = _userManager.Users.Where(u => u.UserName == userName).ToList();

                if (users.Any())
                {
                    return users.FirstOrDefault().TenantId;
                }
            }

            return null;
        }

        #endregion

        #region Common Partial Views


        [ChildActionOnly]
        public PartialViewResult TenantChange()
        {
            var loginInformations = AsyncHelper.RunSync(() => _sessionAppService.GetCurrentLoginInformations());

            return PartialView("_TenantChange", new TenantChangeViewModel
            {
                Tenant = loginInformations.Tenant
            });
        }

        public async Task<PartialViewResult> TenantChangeModal()
        {
            var loginInfo = await _sessionAppService.GetCurrentLoginInformations();
            return PartialView("_TenantChangeModal", new TenantChangeModalViewModel
            {
                TenancyName = loginInfo.Tenant?.TenancyName
            });
        }


        [ChildActionOnly]
        public PartialViewResult _AccountLanguages()
        {
            var model = new LanguageSelectionViewModel
            {
                CurrentLanguage = _languageManager.CurrentLanguage,
                Languages = _languageManager.GetLanguages().Where(l => !l.IsDisabled).ToList()
                    .Where(l => !l.IsDisabled)
                    .ToList(),
                CurrentUrl = Request.Path
            };

            return PartialView("_AccountLanguages", model);
        }

        #endregion



        [AllowAnonymous]
        public ActionResult GetTenatListFromUser(GetTenantListFromUserInput input)
        {


            var usersTenants = _userTenantRepository.GetAllList(x => x.Users.UserName == input.UserName).ToList();

            List<UserTenantLocalDto> TenantList = new List<UserTenantLocalDto>();

            foreach (var item in usersTenants)
            {
                TenantList.Add(new UserTenantLocalDto(item.UserId, item.TenantId, item.Tenant.Name, item.Tenant.TenancyName));
            }


            return Json(TenantList, JsonRequestBehavior.AllowGet);
        }

        public async Task<FileStreamResult> GetUserPicture(int Id)
        {

            return await Task.Run(() =>
            {
                var picture = _userPictureRepository.GetAll()
                    .Where(t => t.UserId == Id)
                    .Select(t => t.Picture)
                    .FirstOrDefault();

                picture = picture ?? EasySchoolManagerConsts.User.DefaultUserPicture;
                var _picture = FileHelper.ConvertToBytes(picture, out string mimeType) ?? new byte[] { };
                var result = new FileStreamResult(new MemoryStream(_picture), mimeType);

                return result;
            });
        }



        [UnitOfWork]
        public virtual async Task<ActionResult> SignInToken(string tokenId)
        {
            var cacheItem = await _cacheManager.GetSwitchToLinkedAccountCache().GetOrDefaultAsync(tokenId);
            if (cacheItem == null)
                throw new UserFriendlyException(L("InvalidTokenUser"));

            _unitOfWorkManager.Current.SetTenantId(cacheItem.TargetTenantId);
            var user = await _userManager.FindByIdAsync(cacheItem.TargetUserId);
            var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

            if (cacheItem.ImpersonatorTenantId.HasValue)
                identity.AddClaim(new Claim(AbpClaimTypes.ImpersonatorTenantId, cacheItem.ImpersonatorTenantId.Value.ToString(CultureInfo.InvariantCulture)));

            if (cacheItem.ImpersonatorUserId.HasValue)
                identity.AddClaim(new Claim(AbpClaimTypes.ImpersonatorUserId, cacheItem.ImpersonatorUserId.Value.ToString(CultureInfo.InvariantCulture)));

            _authenticationManager.SignOutAllAndSignIn(identity);

            user.LastLoginTime = Clock.Now;

            await _cacheManager.GetSwitchToLinkedAccountCache().RemoveAsync(tokenId);
            return RedirectToAction("Index", "Home");
        }

    }
}
﻿using Abp.Runtime.Caching;
using Abp.UI;
using Abp.Web.Mvc.Authorization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasySchoolManager.Web.Controllers
{
   [AbpMvcAuthorize]
    public class FileController : EasySchoolManagerControllerBase
    {
        private readonly ICacheManager cacheManager;

        public FileController(ICacheManager cacheManager)
        {
            this.cacheManager = cacheManager;
        }

        public FileResult DownloadXmlFile(string token)
        {
            try
            {
                // Loading file from cache.
                MemoryStream ms = (MemoryStream)this.cacheManager.GetCache("xml").GetOrDefault(token);

                // Set the proper HTTP response content type
                FileContentResult fileContentResult =
                        File(ms.ToArray(), "application/xml", token);
                return fileContentResult;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException("Error", "Error downloading file. Please try again.", e);
            }
            finally
            {
                this.cacheManager.GetCache("pdf").Remove(token);
            }
        }
    }
}
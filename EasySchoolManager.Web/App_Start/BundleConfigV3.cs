﻿using System.Web.Optimization;

namespace EasySchoolManager.Web
{

    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(
                new StyleBundle("~/Bundles/mandatory-styles/css")
                    .Include("~/Content/assets/global/plugins/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/simple-line-icons/simple-line-icons.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/bootstrap/css/bootstrap.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/fontff-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/bootstrap-toastr/toastr.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/bootstrap-sweetalert/sweetalert.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/layouts/layout2/css/famfamfam-flags.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/waves/waves.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/ui-grid/ui-grid.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/chosen/chosen.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/angular-datepicker/angular-datepicker.css", new CssRewriteUrlTransform())
            //.Include("~/Content/assets/global/plugins/pagination/pagination.css", new CssRewriteUrlTransform()) // Ver si es necesario
            );

            bundles.Add(
                new StyleBundle("~/Bundles/theme-styles/css")
                    .Include("~/Content/assets/global/css/components.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/css/plugins.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/layouts/layout2/css/layout.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/layouts/layout2/css/themes/blue.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/layouts/layout2/css/custom.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/select2/css/select2.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/select2/css/select2-bootstrap.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/angular-material/angular-material/angular-material.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/assets/global/plugins/angucomplete/angucomplete-alt.css", new CssRewriteUrlTransform())
            );

            bundles.Add(
                new ScriptBundle("~/Bundles/jquery-plugins/js")
                    .Include(
                        "~/Content/assets/global/plugins/json2.js",
                        "~/Content/assets/global/plugins/jquery.min.js",
                        "~/Content/assets/global/plugins/bootstrap/js/bootstrap.min.js",
                        "~/Content/assets/global/plugins/moment-with-locales.min.js",
                        "~/Content/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
                        "~/Content/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                        "~/Content/assets/global/plugins/jquery.blockui.min.js",
                        "~/Content/assets/global/plugins/js.cookie.min.js",
                        "~/Content/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
                        "~/Content/assets/global/plugins/bootstrap-toastr/toastr.min.js", // do have css
                        "~/Content/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js", // do have css
                        "~/Content/assets/global/plugins/select2/js/select2.full.min.js",
                        "~/Content/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
                        "~/Content/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js",
                        "~/Content/assets/global/plugins/ladda/spin.min.js",
                        "~/Content/assets/global/plugins/jquery.spin.js",
                        "~/Content/assets/global/plugins/waves/waves.js",
                        "~/Content/assets/global/plugins/push.js",
                        "~/Content/assets/global/scripts/custom.js"
                    )
            );

            bundles.Add(
               new ScriptBundle("~/Bundles/angularjs-plugins/js")
                   .Include(
                        "~/Content/assets/global/plugins/jquery-validation/js/jquery.validate.js",
                        "~/Content/assets/global/plugins/angularjs/angular.min.js",
                        "~/Content/assets/global/plugins/angularjs/angular-sanitize.min.js",
                        "~/Content/assets/global/plugins/angularjs/angular-touch.min.js",
                        "~/Content/assets/global/plugins/angularjs/plugins/angular-ui-router.min.js",
                        "~/Content/assets/global/plugins/angularjs/plugins/ocLazyLoad.min.js",
                        "~/Content/assets/global/plugins/angularjs/plugins/ui-bootstrap.min.js", // puesto para ver.
                        "~/Content/assets/global/plugins/angularjs/plugins/ui-bootstrap-tpls.min.js",
                        "~/Content/assets/global/plugins/angularjs/plugins/ui-utils.min.js",
                        "~/Content/assets/global/plugins/signalr/jquery.signalR.js",
                        "~/Content/assets/global/plugins/ui-grid/ui-grid.min.js",
                        "~/Content/assets/global/plugins/pagination/pagination.js",
                        "~/Abp/Framework/scripts/abp.js",
                        "~/Abp/Framework/scripts/libs/abp.jquery.js",
                        "~/Abp/Framework/scripts/libs/abp.toastr.js",
                        "~/Abp/Framework/scripts/libs/abp.blockUI.js",
                        "~/Abp/Framework/scripts/libs/abp.spin.js",
                        "~/Abp/Framework/scripts/libs/abp.sweet-alert.js",
                        "~/Abp/Framework/scripts/libs/angularjs/abp.ng.js",
                        "~/Content/assets/global/plugins/chosen/chosen.jquery.min.js",
                        // "~/Content/assets/global/plugins/angular-chosen-localytics/chosen-js/chosen.proto.min.js",
                        "~/Content/assets/global/plugins/chosen/angular-chosen.min.js",
                        "~/Content/assets/global/plugins/angular-datepicker/angular-datepicker.js",
                        "~/Content/assets/global/plugins/angucomplete/angucomplete-alt.js"

                 )
            );

            bundles.Add(
               new ScriptBundle("~/Bundles/angularjs-app/js")
                   .Include(
                        "~/App/Main/_libconfig",
                        "~/Content/assets/global/plugins/angular-material/angular-animate/angular-animate.min.js",
                        "~/Content/assets/global/plugins/angular-material/angular-aria/angular-aria.min.js",
                        "~/Content/assets/global/plugins/angular-material/angular-material/angular-material.min.js",
                        "~/App/Main/app.js",
                        "~/Content/assets/global/scripts/app.js",
                        "~/Content/assets/layouts/layout2/scripts/layout.js",
                        "~/Content/assets/layouts/global/scripts/quick-sidebar.min.js",
                        "~/Content/assets/layouts/global/scripts/quick-nav.min.js",
                        "~/Abp/Framework/scripts/utils/helpers.js"
                   )
            );

            bundles.Add(
               new ScriptBundle("~/Bundles/angularjs-app/js-views")
                   .IncludeDirectory("~/App/Main/views", "*.js", true)
            );

            bundles.Add(
           new ScriptBundle("~/Bundles/app-services")
           .IncludeDirectory("~/App/Main/Services", "*.js", true)
           );

            bundles.Add(
            new ScriptBundle("~/Bundles/app-directives")
            .IncludeDirectory("~/App/Main/Directives", "*.js", true)
            );

            //bundles.Add(
            //   new ScriptBundle("~/Bundles/angular-material/js")
            //       .IncludeDirectory("~/Content/assets/global/plugins/angular-material/angular-material", "*.js", true)
            //);

            #region loginBundle

            bundles.Add(
                new ScriptBundle("~/Bundles/jquery-plugins/js/login")
                        .Include(
                            "~/Content/assets/global/plugins/json2.js",
                            "~/Content/assets/global/plugins/jquery.min.js",
                            "~/Content/assets/global/plugins/bootstrap/js/bootstrap.min.js",
                            "~/Content/assets/global/plugins/moment-with-locales.min.js",
                            "~/Content/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
                            "~/Content/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                            "~/Content/assets/global/plugins/jquery.blockui.min.js",
                            "~/Content/assets/global/plugins/js.cookie.min.js",
                            "~/Content/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
                            "~/Content/assets/global/plugins/bootstrap-toastr/toastr.min.js", // do have css
                            "~/Content/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js", // do have css
                            "~/Content/assets/global/plugins/select2/js/select2.full.min.js",
                            "~/Content/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
                            "~/Content/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js",
                            "~/Content/assets/global/plugins/ladda/spin.min.js",
                            "~/Content/assets/global/plugins/jquery.spin.js",
                            "~/Content/assets/global/plugins/waves/waves.js",
                            "~/Content/assets/global/plugins/push.js",
                            "~/Content/assets/global/plugins/jquery-validation/js/jquery.validate.js",
                            "~/Abp/Framework/scripts/abp.js",
                            "~/Abp/Framework/scripts/libs/abp.jquery.js",
                            "~/Abp/Framework/scripts/libs/abp.toastr.js",
                            "~/Abp/Framework/scripts/libs/abp.blockUI.js",
                            "~/Abp/Framework/scripts/libs/abp.spin.js",
                            "~/Abp/Framework/scripts/libs/abp.sweet-alert.js"
                        ).ForceOrdered()
                );

            bundles.Add(
                   new StyleBundle("~/Bundles/mandatory-styles/css/login")
                       .Include("~/Content/assets/global/plugins/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform())
                       .Include("~/Content/assets/global/plugins/simple-line-icons/simple-line-icons.min.css", new CssRewriteUrlTransform())
                       .Include("~/Content/assets/global/plugins/bootstrap/css/bootstrap.min.css", new CssRewriteUrlTransform())
                       .Include("~/Content/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css", new CssRewriteUrlTransform())
                       .Include("~/Content/assets/global/plugins/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform())
                       .Include("~/Content/assets/global/plugins/bootstrap-toastr/toastr.css", new CssRewriteUrlTransform())
                       .Include("~/Content/assets/global/plugins/bootstrap-sweetalert/sweetalert.css", new CssRewriteUrlTransform())
                       .Include("~/Content/assets/layouts/layout2/css/famfamfam-flags.css", new CssRewriteUrlTransform())
                       .Include("~/Content/assets/global/plugins/waves/waves.min.css", new CssRewriteUrlTransform())
                       .Include("~/Content/Login/material-icons/materialicons.css", new CssRewriteUrlTransform())
                       .Include("~/Content/Login/material-icons/materialize.css", new CssRewriteUrlTransform())
                       .Include("~/Content/Login/style-login.css", new CssRewriteUrlTransform())
                       .Include("~/Views/Account/_Layout.min.css", new CssRewriteUrlTransform())
               );

            #endregion

        }
    }
}
using Abp.Application.Navigation;
using Abp.Localization;
using EasySchoolManager.Authorization;

namespace EasySchoolManager.Web
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class EasySchoolManagerNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        "Home",
                        new LocalizableString("HomePage", EasySchoolManagerConsts.LocalizationSourceName),
                        url: "#/",
                        icon: "fa fa-home",
                        requiresAuthentication: true
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Tenants",
                        L("Tenants"),
                        url: "#tenants",
                        icon: "fa fa-globe",
                        requiredPermissionName: PermissionNames.Pages_Tenants
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Users",
                        L("Users"),
                        url: "#users",
                        icon: "fa fa-users",
                        requiredPermissionName: PermissionNames.Pages_Users
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Courses",
                        L("Courses"),
                        url: "#courses",
                        icon: "fa fa-users",
                        requiredPermissionName: PermissionNames.Pages_Courses
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Roles",
                        L("Roles"),
                        url: "#users",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Roles
                    )
                )

                                //No borrar esta linea, se usa para agregar los siguientes menu, que son agregados automaticamente.
                                .AddItem(
                    new MenuItemDefinition(
                        "EvaluationPeriods",
                        L("EvaluationPeriods"),
                        url: "#evaluationPeriods",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_EvaluationPeriods
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Evaluations",
                        L("Evaluations"),
                        url: "#evaluations",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Evaluations
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "GrantEnterprises",
                        L("GrantEnterprises"),
                        url: "#grantEnterprises",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_GrantEnterprises
                    )
                )


                ////.AddItem(
                ////    new MenuItemDefinition(
                ////        "HistoryCancelReceipts",
                ////        L("HistoryCancelReceipts"),
                ////        url: "#historyCancelReceipts",
                ////        icon: "fa fa-tag",
                ////        requiredPermissionName: PermissionNames.Pages_HistoryCancelReceipts
                ////    )
                ////)

                .AddItem(
                    new MenuItemDefinition(
                        "SubjectHighs",
                        L("SubjectHighs"),
                        url: "#subjectHighs",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_SubjectHighs
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "EvaluationPositionHighs",
                        L("EvaluationPositionHighs"),
                        url: "#evaluationPositionHighs",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_EvaluationPositionHighs
                    )
                )


                .AddItem(
                    new MenuItemDefinition(
                        "EvaluationOrderHighs",
                        L("EvaluationOrderHighs"),
                        url: "#evaluationOrderHighs",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_EvaluationOrderHighs
                    )
                )

                 .AddItem(
                    new MenuItemDefinition(
                        "EvaluationHighs",
                        L("EvaluationHighs"),
                        url: "#evaluationHighs",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_EvaluationHighs
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "EvaluationHighsByPos",
                        L("EvaluationHighsByPos"),
                        url: "#evaluationHighsByPos",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_EvaluationHighsByPos
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "EvaluationHighsByPosPrevious",
                        L("EvaluationHighsByPosPrevious"),
                        url: "#evaluationHighsByPosPrevious",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_EvaluationHighsByPosPrevious
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "TransactionQueues",
                        L("TransactionQueues"),
                        url: "#TransactionQueues",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_TransactionQueues
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "TaxReceiptTypes",
                        L("TaxReceiptTypes"),
                        url: "#taxReceiptTypes",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_TaxReceiptTypes
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "TaxReceipts",
                        L("TaxReceipts"),
                        url: "#taxReceipts",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_TaxReceipts
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Companies",
                        L("Companies"),
                        url: "#companies",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Companies
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "CancelStatus",
                        L("CancelStatus"),
                        url: "#cancelStatus",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_CancelStatus
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "SubjectSessions",
                        L("SubjectSessions"),
                        url: "#subjectSessions",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_SubjectSessions
                    )
                )

                                .AddItem(
                    new MenuItemDefinition(
                        "SubjectSessionHighs",
                        L("SubjectSessionHighs"),
                        url: "#subjectSessionHighs",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_SubjectSessionHighs
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "ConceptTenantRegistrations",
                        L("ConceptTenantRegistrations"),
                        url: "#conceptTenantRegistrations",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_ConceptTenantRegistrations
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "VerifoneBreaks",
                        L("VerifoneBreaks"),
                        url: "#verifoneBreaks",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_VerifoneBreaks
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Discounts",
                        L("Discounts"),
                        url: "#discounts",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Discounts
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "DiscountNames",
                        L("DiscountNames"),
                        url: "#discountNames",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_DiscountNames
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "PeriodDiscounts",
                        L("PeriodDiscounts"),
                        url: "#periodDiscounts",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_PeriodDiscounts
                    )
                )

                //XXXInsertNavitationProviderXXX

                .AddItem(
                    new MenuItemDefinition(
                        "Indicators",
                        L("Indicators"),
                        url: "#indicators",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Indicators
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "EvaluationLegends",
                        L("EvaluationLegends"),
                        url: "#evaluationLegends",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_EvaluationLegends
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "IndicatorGroups",
                        L("IndicatorGroups"),
                        url: "#indicatorGroups",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_IndicatorGroups
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Subjects",
                        L("Subjects"),
                        url: "#subjects",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Subjects
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Teachers",
                        L("Teachers"),
                        url: "#teachers",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Teachers
                    )
                )


                //.AddItem(
                //    new MenuItemDefinition(
                //        "T2Dimensions",
                //        L("T2Dimensions"),
                //        url: "#t2Dimensions",
                //        icon: "fa fa-tag",
                //        requiredPermissionName: PermissionNames.Pages_T2Dimensions
                //    )
                //)

                //.AddItem(
                //    new MenuItemDefinition(
                //        "T10Dimensions",
                //        L("T10Dimensions"),
                //        url: "#t10Dimensions",
                //        icon: "fa fa-tag",
                //        requiredPermissionName: PermissionNames.Pages_T10Dimensions
                //    )
                //)

                //.AddItem(
                //    new MenuItemDefinition(
                //        "T9Dimensions",
                //        L("T9Dimensions"),
                //        url: "#t9Dimensions",
                //        icon: "fa fa-tag",
                //        requiredPermissionName: PermissionNames.Pages_T9Dimensions
                //    )
                //)

                //.AddItem(
                //    new MenuItemDefinition(
                //        "T8Dimensions",
                //        L("T8Dimensions"),
                //        url: "#t8Dimensions",
                //        icon: "fa fa-tag",
                //        requiredPermissionName: PermissionNames.Pages_T8Dimensions
                //    )
                //)

                //.AddItem(
                //    new MenuItemDefinition(
                //        "T7Dimensions",
                //        L("T7Dimensions"),
                //        url: "#t7Dimensions",
                //        icon: "fa fa-tag",
                //        requiredPermissionName: PermissionNames.Pages_T7Dimensions
                //    )
                //)

                //.AddItem(
                //    new MenuItemDefinition(
                //        "T6Dimensions",
                //        L("T6Dimensions"),
                //        url: "#t6Dimensions",
                //        icon: "fa fa-tag",
                //        requiredPermissionName: PermissionNames.Pages_T6Dimensions
                //    )
                //)

                //.AddItem(
                //    new MenuItemDefinition(
                //        "T5Dimensions",
                //        L("T5Dimensions"),
                //        url: "#t5Dimensions",
                //        icon: "fa fa-tag",
                //        requiredPermissionName: PermissionNames.Pages_T5Dimensions
                //    )
                //)

                //.AddItem(
                //    new MenuItemDefinition(
                //        "T4Dimensions",
                //        L("T4Dimensions"),
                //        url: "#t4Dimensions",
                //        icon: "fa fa-tag",
                //        requiredPermissionName: PermissionNames.Pages_T4Dimensions
                //    )
                //)

                //.AddItem(
                //    new MenuItemDefinition(
                //        "T1Dimensions",
                //        L("T1Dimensions"),
                //        url: "#t1Dimensions",
                //        icon: "fa fa-tag",
                //        requiredPermissionName: PermissionNames.Pages_T1Dimensions
                //    )
                //)

                //.AddItem(
                //    new MenuItemDefinition(
                //        "T3Dimensions",
                //        L("T3Dimensions"),
                //        url: "#t3Dimensions",
                //        icon: "fa fa-tag",
                //        requiredPermissionName: PermissionNames.Pages_T3Dimensions
                //    )
                //)

                .AddItem(
                    new MenuItemDefinition(
                        "Positions",
                        L("Positions"),
                        url: "#positions",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Positions
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "InventoryShops",
                        L("InventoryShops"),
                        url: "#inventoryShops",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_InventoryShops
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Inventory",
                        L("Inventory"),
                        url: "#inventory",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Inventory
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "ButtonPositions",
                        L("ButtonPositions"),
                        url: "#buttonPositions",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_ButtonPositions
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "TransactionShops",
                        L("TransactionShops"),
                        url: "#transactionShops",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_TransactionShops
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "ClientShops",
                        L("ClientShops"),
                        url: "#clientShops",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_ClientShops
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "PurchaseShops",
                        L("PurchaseShops"),
                        url: "#purchaseShops",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_PurchaseShops
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "ProviderShops",
                        L("ProviderShops"),
                        url: "#providerShops",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_ProviderShops
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "ArticleShops",
                        L("ArticleShops"),
                        url: "#articleShops",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_ArticleShops
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Trades",
                        L("Trades"),
                        url: "#trades",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Trades
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "CourrierPersons",
                        L("CourrierPersons"),
                        url: "#courrierPersons",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_CourrierPersons
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Purchases",
                        L("Purchases"),
                        url: "#purchases",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Purchases
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "ReceiptTypes",
                        L("ReceiptTypes"),
                        url: "#receiptTypes",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_ReceiptTypes
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Messages",
                        L("Messages"),
                        url: "#messages",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Messages
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "TransactionTypes",
                        L("TransactionTypes"),
                        url: "#transactionTypes",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_TransactionTypes
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Providers",
                        L("Providers"),
                        url: "#providers",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Providers
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Articles",
                        L("Articles"),
                        url: "#articles",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Articles
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "ArticlesTypes",
                        L("ArticlesTypes"),
                        url: "#articlesTypes",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_ArticlesTypes
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Brands",
                        L("Brands"),
                        url: "#brands",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Brands
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Units",
                        L("Units"),
                        url: "#units",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Units
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Districts",
                        L("Districts"),
                        url: "#districts",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Districts
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "AccountBalances",
                        L("AccountBalances"),
                        url: "#accountBalances",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_AccountBalance
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "ViewPaymentProjections",
                        L("ViewPaymentProjections"),
                        url: "#viewPaymentProjections",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_ViewPaymentProjection

                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "GenerateAllPaymentProjections",
                        L("GenerateAllPaymentProjections"),
                        url: "#generateAllPaymentProjections",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_GenerateAllPaymentProjections
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "MasterTenants",
                        L("MasterTenants"),
                        url: "#masterTenants",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_MasterTenants
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "BankAccounts",
                        L("BankAccounts"),
                        url: "#bankAccounts",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_BankAccounts
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "PaymentDates",
                        L("PaymentDates"),
                        url: "#paymentDates",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_PaymentDates
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "CourseValues",
                        L("CourseValues"),
                        url: "#courseValues",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_CourseValues
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Periods",
                        L("Periods"),
                        url: "#periods",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Periods
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Concepts",
                        L("Concepts"),
                        url: "#concepts",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Concepts
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "PaymentMethods",
                        L("PaymentMethods"),
                        url: "#paymentMethods",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_PaymentMethods
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Receipts",
                        L("Receipts"),
                        url: "#receipts",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Receipts
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Transactions",
                        L("Transactions"),
                        url: "#transactions",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Transactions
                    )
                )


                .AddItem(
                    new MenuItemDefinition(
                        "Deposits",
                        L("Deposits"),
                        url: "#deposits",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Deposits
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Catalogs",
                        L("Catalogs"),
                        url: "#catalogs",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Catalogs
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "ReportsMain",
                        L("ReportsMain"),
                        url: "#reportsmain",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_ReportsMain
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Relationships",
                        L("Relationships"),
                        url: "#relationships",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Relationships
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Cities",
                        L("Cities"),
                        url: "#cities",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Cities
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Enrollments",
                        L("Enrollments"),
                        url: "#enrollments",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Enrollments
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "PreviousEnrollments",
                        L("PreviousEnrollments"),
                        url: "#previousEnrollments",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_PreviousEnrollments
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "NextEnrollments",
                        L("NextEnrollments"),
                        url: "#nextEnrollments",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_NextEnrollments
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Bloods",
                        L("Bloods"),
                        url: "#bloods",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Bloods
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Provinces",
                        L("Provinces"),
                        url: "#provinces",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Provinces
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "CourseSessions",
                        L("CourseSessions"),
                        url: "#courseSessions",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_CourseSessions
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Religions",
                        L("Religions"),
                        url: "#religions",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Religions
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Regions",
                        L("Regions"),
                        url: "#regions",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Regions
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "PrinterTypes",
                        L("PrinterTypes"),
                        url: "#printerTypes",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_PrinterTypes
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Origins",
                        L("Origins"),
                        url: "#origins",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Origins
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Illnesses",
                        L("Illnesses"),
                        url: "#illnesses",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Illnesses
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Genders",
                        L("Genders"),
                        url: "#genders",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Genders
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "DocumentTypes",
                        L("DocumentTypes"),
                        url: "#documentTypes",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_DocumentTypes
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "CommentTypes",
                        L("CommentTypes"),
                        url: "#commentTypes",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_CommentTypes
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "ChangeTypes",
                        L("ChangeTypes"),
                        url: "#changeTypes",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_ChangeTypes
                    )
                )



                .AddItem(
                    new MenuItemDefinition(
                        "Banks",
                        L("Banks"),
                        url: "#banks",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Banks
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Allergies",
                        L("Allergies"),
                        url: "#allergies",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Allergies
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Occupations",
                        L("Occupations"),
                        url: "#occupations",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Occupations
                    )
                )

                .AddItem(
                    new MenuItemDefinition(
                        "Levels",
                        L("Levels"),
                        url: "#levels",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Levels
                    )
                )
               .AddItem(
                    new MenuItemDefinition(
                        "Reports",
                        L("Reports"),
                        url: "#Reports",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_Reports
                    )
                )
               .AddItem(
                    new MenuItemDefinition(
                        "SessionStudentSelections",
                        L("SessionStudentSelections"),
                        url: "#SessionStudentSelections",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_SessionStudentSelections
                    )
                )
               .AddItem(
                    new MenuItemDefinition(
                        "SessionStudentSelectionPrevious",
                        L("SessionStudentSelectionPrevious"),
                        url: "#SessionStudentSelectionPrevious",
                        icon: "fa fa-tag",
                        requiredPermissionName: PermissionNames.Pages_SessionStudentSelectionPrevious
                    )
                )
                //-----------------------------/
                .AddItem(
                    new MenuItemDefinition(
                        "About",
                        new LocalizableString("About", EasySchoolManagerConsts.LocalizationSourceName),
                        url: "#/about",
                        icon: "fa fa-info"
                        )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, EasySchoolManagerConsts.LocalizationSourceName);
        }
    }
}

﻿using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.Modules;
using Abp.Web.Mvc;
using Abp.Web.SignalR;
using Abp.Zero.Configuration;
using EasySchoolManager.Api;
using Hangfire;
using Abp.Threading.BackgroundWorkers;
using EasySchoolManager.Workers;
using Microsoft.Owin.Security;
using Castle.MicroKernel.Registration;
using System.Web;

namespace EasySchoolManager.Web
{
    [DependsOn(
        typeof(EasySchoolManagerDataModule),
        typeof(EasySchoolManagerApplicationModule),
        typeof(EasySchoolManagerWebApiModule),
        typeof(AbpWebSignalRModule),
        //typeof(AbpHangfireModule), - ENABLE TO USE HANGFIRE INSTEAD OF DEFAULT JOB MANAGER
        typeof(AbpWebMvcModule))]
    public class EasySchoolManagerWebModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Enable database based localization
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();

            //Configure navigation/menu
            Configuration.Navigation.Providers.Add<EasySchoolManagerNavigationProvider>();

            //Configure Hangfire - ENABLE TO USE HANGFIRE INSTEAD OF DEFAULT JOB MANAGER
            //Configuration.BackgroundJobs.UseHangfire(configuration =>
            //{
            //    configuration.GlobalConfiguration.UseSqlServerStorage("Default");
            //});
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            IocManager.IocContainer.Register(
             Component
                 .For<IAuthenticationManager>()
                 .UsingFactoryMethod(() => HttpContext.Current.GetOwinContext().Authentication)
                 .LifestyleTransient()
             );

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public override void PostInitialize()
        {
            var workManager = IocManager.Resolve<IBackgroundWorkerManager>();
            workManager.Add(IocManager.Resolve<WApplyChargesFromPaymentProjections>());
            workManager.Add(IocManager.Resolve<WSendTransactionQueues>());
        }
    }
}

﻿var Login = function () {

    
    var handleLogin = function () {

        var $loginForm = $('#LoginForm');

        $loginForm.submit(function (e) {
            e.preventDefault();

            if (!$loginForm.valid()) {
                return;
            }

            abp.ui.setBusy(
                null,
                abp.ajax({
                    contentType: 'application/x-www-form-urlencoded',
                    url: $loginForm.attr('asp-action'),
                    data: $loginForm.serialize()
                })
            );
        });

        $('#ReturnUrlHash').val(location.hash);

        $('#LoginForm input:first-child').focus();
    }


    var handleUserNameChange = function ()
    {
        $("#UsernameOrEmailAddress").change(function () {
            getTenantList();
        });
    }

    function getTenantList()
    {
        var username = $("#UsernameOrEmailAddress").val();

        $.post("GetTenatListFromUser", { 'UserName': username },
            function (data) {
                try {

                    CreateComboTenants(data.result);


                } catch (e)
                { console.log("Error Getting Tenancy Names"); }
            });
    }

    function CreateComboTenants(data)
    {
        var combo = $("#TenancyName");
        combo.html("");

        var contenedor =  $("#ddTenancyName");

        if (data.length > 0) {
            var options = "<option value=''> </option>";
            for (var i = 0; i < data.length; i++) {
                var tn = data[i];
                options += "<option value='" + tn.tenancyName + "'> " + tn.name + "</option>";
            }
            combo.html(options);
            contenedor.show();
        }
        else
        {
            contenedor.hide();
        }
    }


    var handleForgetPassword = function () {
        $('.forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                UsernameOrEmailAddress: {
                    required: true
                }
            },

            messages: {
                UsernameOrEmailAddress: {
                    required: "Email or Username is required."
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   

            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            }

           
        });

        //$('.forget-form input').keypress(function (e) {
        //    if (e.which == 13) {
        //        if ($('.forget-form').validate().form()) {
        //            $('.forget-form').submit();
        //        }
        //        return false;
        //    }
        //});

        var $loginForm2 = $('.forget-form');

        $loginForm2.submit(function (e) {
            e.preventDefault();

            if (!$loginForm2.valid()) {
                return;
            }

            abp.ui.setBusy(
                null,
                abp.ajax({
                    contentType: 'application/x-www-form-urlencoded',
                    url: $loginForm2.attr('asp-action'),
                    data: $loginForm2.serialize()
                })
            );
        });

        $('#ReturnUrlHash').val(location.hash);
        
        jQuery('#forget-password').click(function () {
            jQuery('#LoginForm').hide();
            jQuery('.forget-form').show();
        });

        jQuery('#back-btn').click(function () {
            jQuery('#LoginForm').show();
            jQuery('.forget-form').hide();
        });

    }

    return {
        //main function to initiate the module
        init: function () {

            handleLogin();
            handleForgetPassword();
            handleUserNameChange();
        }

    };

}();

jQuery(document).ready(function () {
    Login.init();
});
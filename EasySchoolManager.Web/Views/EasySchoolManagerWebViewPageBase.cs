﻿using Abp.Web.Mvc.Views;

namespace EasySchoolManager.Web.Views
{
    public abstract class EasySchoolManagerWebViewPageBase : EasySchoolManagerWebViewPageBase<dynamic>
    {

    }

    public abstract class EasySchoolManagerWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected EasySchoolManagerWebViewPageBase()
        {
            LocalizationSourceName = EasySchoolManagerConsts.LocalizationSourceName;
        }
    }
}
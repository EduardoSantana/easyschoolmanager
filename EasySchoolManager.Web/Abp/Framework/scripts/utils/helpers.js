﻿var App = App || {};
(function () {

    var appLocalizationSource = abp.localization.getSource('EasySchoolManager');
    App.localize = function () {
        return appLocalizationSource.apply(this, arguments);
    };

})(App);
(function () {
    angular.module('MetronicApp').controller('app.views.bankAccounts.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.bankAccount','settings',
        function ($scope, $timeout, $uibModal, bankAccountService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getBankAccounts(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.bankAccounts = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openBankAccountEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
		
                    {
                    name: App.localize('BankAccountNumber'),
                    field: 'number',
                    minWidth: 125
                    },
                    {
                    name: App.localize('BankAccountBank'),
                    field: 'banks_Name',
                    minWidth: 125
                    },
                    {
                        name: App.localize('BankAccountRegion'),
                        field: 'regions_Name',
                        minWidth: 125
                    },
                    {
                    name: App.localize('BankAccountDescription'),
                    field: 'description',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getBankAccounts(showTheLastPage) {
                bankAccountService.getAllBankAccounts($scope.pagination).then(function (result) {
                    vm.bankAccounts = result.data.items;

                    $scope.gridOptions.data = vm.bankAccounts;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openBankAccountCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/bankAccounts/createModal.cshtml',
                    controller: 'app.views.bankAccounts.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getBankAccounts(true);
                });
            };

            vm.openBankAccountEditModal = function (bankAccount) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/bankAccounts/editModal.cshtml',
                    controller: 'app.views.bankAccounts.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return bankAccount.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getBankAccounts(false);
                });
            };

            vm.delete = function (bankAccount) {
                abp.message.confirm(
                    "Delete bankAccount '" + bankAccount.name + "'?",
                    function (result) {
                        if (result) {
                            bankAccountService.delete({ id: bankAccount.id })
                                .then(function (result) {
                                    getBankAccounts(false);
                                    abp.notify.info("Deleted bankAccount: " + bankAccount.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getBankAccounts(false);
            };

            getBankAccounts(false);
        }
    ]);
})();

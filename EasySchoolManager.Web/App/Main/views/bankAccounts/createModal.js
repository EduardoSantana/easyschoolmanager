(function () {
    angular.module('MetronicApp').controller('app.views.bankAccounts.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.bankAccount', 'abp.services.app.bank', 'abp.services.app.region',
        function ($scope, $uibModalInstance, bankAccountService , bankService, regionService) {
            var vm = this;
            vm.saving = false;

            vm.bankAccount = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     bankAccountService.create(vm.bankAccount)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.banks = [];
            vm.getBanks	 = function()
            {
                bankService.getAllBanksForCombo().then(function (result) {
                    vm.banks = result.data;
					App.initAjax();
                });
            }

            vm.regions = [];
            vm.getRegions = function () {
                regionService.getAllRegionsForCombo().then(function (result) {
                    vm.regions = result.data;
                    App.initAjax();
                });
            }
			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getBanks();
            vm.getRegions();

		    App.initAjax();
			setTimeout(function () { $("#bankAccountNumber").focus(); }, 100);
        }
    ]);
})();

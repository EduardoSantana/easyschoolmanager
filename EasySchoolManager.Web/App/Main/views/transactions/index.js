(function () {
    angular.module('MetronicApp').controller('app.views.transactions.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.transaction','settings','$stateParams',
        function ($scope, $timeout, $uibModal, transactionService, settings,$stateParams) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getTransactions(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.transactions = [];

            vm.permissions = {
                edit: abp.auth.hasPermission('Pages.Transactions.Edit'),
                create: abp.auth.hasPermission('Pages.Transactions.Create'),
                cancel: abp.auth.hasPermission('Pages.Transactions.Cancel'),
                request: abp.auth.hasPermission('Pages.Transactions.Request'),
                cancelAuthorization: abp.auth.hasPermission('Pages.Transactions.CancelAuthorization'),

            };

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 100,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-if="grid.appScope.permissions.edit"  ng-click="grid.appScope.openTransactionEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.request && row.entity.request==null"  ng-click="grid.appScope.openTransactionRequestModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Request') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.cancel && row.entity.cancelAuthorization != null"  ng-click="grid.appScope.openTransactionCancelModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Cancel') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.cancelAuthorization && row.entity.request != null"  ng-click="grid.appScope.openTransactionCancelAuthorizationModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Cancel') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    }

                    
                    //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                    ,
                    {
                    name: App.localize('TransactionNumber'),
                    field: 'sequence',
                    width: 100
                    },
                    {
                    name: App.localize('TransactionDate'),
                    field: 'date',
                    width: 110,
                    cellFilter: 'date:"dd-MM-yyyy\"'
                    },
                    {
                    name: App.localize('TransactionName'),
                    field: 'name',
                    minWidth: 370
                    },
                    {
                    name: App.localize('TransactionEnrollment'),
                    field: 'enrollment',
                    width: 100
                    },
                    {
                    name: App.localize('TransactionConcept'),
                    field: 'concepts_Description',
                    minWidth: 250
                    },
                    {
                        name: App.localize('TransactionAmount'),
                        field: 'amount',
                        width: 120,
                        cellFilter: 'number:2',
                        cellClass: "font-uigrid-right",
                        headerClass: "font-uigrid-right"
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 90
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getTransactions(showTheLastPage) {
                transactionService.getAllTransactions($scope.pagination).then(function (result) {
                    vm.transactions = result.data.items;

                    $scope.gridOptions.data = vm.transactions;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openTransactionCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/transactions/createModal.cshtml',
                    controller: 'app.views.transactions.createModal as vm',
                    size: 'lg',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getTransactions(true);
                    vm.openTransactionCreationModal();
                });
            };

            vm.openTransactionEditModal = function (transaction) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/transactions/editModal.cshtml',
                    controller: 'app.views.transactions.editModal as vm',
                    backdrop: 'static',
                    size: 'lg', 
                    resolve: {
                        id: function () {
                            return transaction.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getTransactions(false);
                });
            };

            vm.openTransactionRequestModal = function (transaction) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/transactions/requestModal.cshtml',
                    controller: 'app.views.transactions.requestModal as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        id: function () {
                            return transaction.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getTransactions(false);
                });
            };

            vm.openTransactionCancelModal = function (transaction) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/transactions/cancelModal.cshtml',
                    controller: 'app.views.transactions.cancelModal as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        id: function () {
                            return transaction.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getTransactions(false);
                });
            };

            vm.openTransactionCancelAuthorizationModal = function (transaction) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/transactions/cancelAuthorizationModal.cshtml',
                    controller: 'app.views.transactions.cancelAuthorizationModal as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        id: function () {
                            return transaction.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getTransactions(false);
                });
            };

            vm.delete = function (transaction) {
                abp.message.confirm(
                    "Delete transaction '" + transaction.name + "'?",
                    function (result) {
                        if (result) {
                            transactionService.delete({ id: transaction.id })
                                .then(function (result) {
                                    getTransactions(false);
                                    abp.notify.info("Deleted transaction: " + transaction.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getTransactions(false);
            };

            vm.printReportTransaction = function () {
                abp.message.info(App.localize("NotAvailable"));
            };

            getTransactions(false);

            try {
                if ($stateParams.origin == 'new')
                    vm.openTransactionCreationModal();
            } catch (e) { }
        }
    ]);
})();

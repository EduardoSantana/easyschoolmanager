(function () {
    angular.module('MetronicApp').controller('app.views.transactions.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.transaction', 'id', 'abp.services.app.bank', 'abp.services.app.concept', 'abp.services.app.origin', 'abp.services.app.enrollment',
        function ($scope, $uibModalInstance, transactionService, id, bankService, conceptService, originService,enrollmentService) {
            var vm = this;
            vm.saving = false;

            vm.transaction = {
                isActive: true
            };
            $scope.pagination = new Pagination(0, 10);

            var init = function () {
                transactionService.get({ id: id })
                    .then(function (result) {
                        vm.transaction = result.data;
                        vm.transaction.currentEnrollment = result.data.enrollments;
                       // vm.transaction.enrollment = vm.transaction.currentEnrollment.enrollmentSequences[0].sequence;
                        vm.transaction.dateTemp = convertDateFormat_YMD_to_DMY(vm.transaction.date, 4);

                        vm.getBanks();
                        vm.getOrigins();
                        vm.getConcepts();

                        $scope.gridOptions.data = vm.transaction.currentEnrollment.enrollmentStudents;
                        calculateTotal();

                        App.initAjax();
                        setTimeout(function () { $("#transactionSequence").focus(); }, 100);
                    });
            }

            function cleanTransaction() {
                vm.transaction.name = null;
                vm.transaction.enrolmentId = null;
                vm.transaction.currentEnrollment = null;

                $scope.$broadcast('angucomplete-alt:clearInput', 'enrollmentsAngu');
                $scope.gridOptions.data = [];
            }

            //Function para asignar los montos distribuidos a los diferentes estudiantes
            function assignDistributedValuesToStudents(distAmount) {
                if (vm.transaction.currentEnrollment.enrollmentStudents == null ||
                    vm.transaction.currentEnrollment.enrollmentStudents.length == 0)
                    return;

                for (var i = 0; i < distAmount.length; i++) {
                    var ele = distAmount[i];
                    vm.transaction.currentEnrollment.enrollmentStudents[i].students.amount = ele;
                }

                vm.transaction.totalCalculed = getCalculatedTotalByEnrollment();
            }


            $scope.$watch("vm.transaction.enrollment", function (newValue, oldValue) {
                if (newValue != undefined) {
                    if (newValue == null || newValue.length === 0) {
                        cleanTransaction();
                        return;
                    }


                    enrollmentService.getEnrollmentSequenceByEnrollment(newValue).then(function (result) {

                        if (result.data == null) {
                            cleanTransaction();
                        }
                        else {
                            vm.enrollment = result.data;
                            vm.transaction.enrolmentId = vm.enrollment.enrollmentId;
                            vm.transaction.currentEnrollment = vm.enrollment.enrollments;
                            if (vm.transaction.currentEnrollment != null) {
                                vm.transaction.currentEnrollment.fullName = vm.transaction.currentEnrollment.firstName + ' ' + vm.transaction.currentEnrollment.lastName;
                                vm.transaction.name = vm.transaction.currentEnrollment.fullName;

                                calculateChildrenAmounts(vm.transaction.amount);

                                $scope.gridOptions.data = vm.transaction.currentEnrollment.enrollmentStudents;

                                $scope.$broadcast('angucomplete-alt:changeInput', 'enrollmentsAngu', vm.transaction.name);

                            }
                        }
                    });
                }
                else
                { cleanTransaction(); }

            });

            function calculateChildrenAmounts(newValue)
            {

                var childrenNumber = vm.transaction.currentEnrollment.enrollmentStudents.length;
                if (childrenNumber == 0) {
                    vm.transaction.totalCalculed = newValue;
                    return;
                }

                var distributedAmount = distributeAmount_SimpleMethod(newValue, childrenNumber)

                assignDistributedValuesToStudents(distributedAmount);
            }


            $scope.$watch("vm.transaction.amount", function (newValue, oldValue) {
                if (newValue == undefined || newValue == null || newValue.length == 0) {
                    vm.transaction.letterAmount = null;
                    return;
                }
                try {
                    vm.transaction.letterAmount = NumeroALetras(newValue);
                    if (vm.amountIsFocused) {
                        if (vm.transaction.currentEnrollment == null) {
                            vm.transaction.totalCalculed = newValue;
                            return;
                        }
                        calculateChildrenAmounts(newValue);
                    }
                } catch (e) {
                    console.log(JSON.stringify(e))
                }
            });


            function getCalculatedTotalByEnrollment() {
                var total = 0.00;

                for (var i = 0; i < vm.transaction.currentEnrollment.enrollmentStudents.length; i++) {
                    var es = vm.transaction.currentEnrollment.enrollmentStudents[i];
                    total += getDoubleOrCero(es.students.amount);
                }

                return total;
            }

            $scope.calculateTotal = function () {
                vm.transaction.totalCalculed = getCalculatedTotalByEnrollment();
            }


            calculateTotal = function () {
                vm.transaction.totalCalculed = getCalculatedTotalByEnrollment();
                App.initAjax();
                $scope.$apply();
            }

            validatePayment = function () {
                var validated = false;
                if (getDoubleOrCero(vm.transaction.amount) != getDoubleOrCero(vm.transaction.totalCalculed)) {
                    abp.message.error(App.localize("TheTransactionAmountDoesNotCorrespondToTheirDistribution"));
                }
                else if (vm.transaction.enrollment == null && getObjectValueOrEmptyString(vm.transaction.name).length === 0) {
                    abp.message.error(App.localize("YouMustSpecifyANameToApplyTheTransaction"));
                }
                else if (vm.transaction.enrollment != null && getObjectValueOrEmptyString(vm.transaction.name).length === 0) {
                    abp.message.error(App.localize("YouMustSpecifyAnExistingEnrollmentToPallyTheTransaction"));
                }
                else validated = true;


                return validated;
            }

            validatePayment = function () {
                var validated = false;
                if (getDoubleOrCero(vm.transaction.amount) != getDoubleOrCero(vm.transaction.totalCalculed)) {
                    abp.message.error(App.localize("TheTransactionAmountDoesNotCorrespondToTheirDistribution"));
                }
                else if (vm.transaction.enrollment == null && getObjectValueOrEmptyString(vm.transaction.name).length === 0) {
                    abp.message.error(App.localize("YouMustSpecifyANameToApplyTheTransaction"));
                }
                else if (vm.transaction.enrollment != null && getObjectValueOrEmptyString(vm.transaction.name).length === 0) {
                    abp.message.error(App.localize("YouMustSpecifyAnExistingEnrollmentToPallyTheTransaction"));
                }
                else validated = true;


                return validated;
            }
            $scope.getEnrollmentsByName = function (userInputString) {
                $scope.pagination.maxResultCount = 10;
                $scope.pagination.textFilter = userInputString;
                return enrollmentService.getAllEnrollments($scope.pagination);
            }

            $scope.returnedValue = function (result) {
                if (!result) return;
                vm.transaction.enrollment = result.originalObject.enrollment;
                vm.transaction.name = result.originalObject.firstName + " " + result.originalObject.lastName;
            }
            vm.save = function () {

                if (!validatePayment())
                    return;

                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    vm.transaction.date = convertDateFormat_DMY_to_YMD(vm.transaction.dateTemp);

                    var transaction = $.extend({}, vm.transaction);
                    transaction.students = [];

                    if (transaction.currentEnrollment != null) {
                        transaction.enrollmentId = transaction.currentEnrollment.id;

                        for (var i = 0; i < transaction.currentEnrollment.enrollmentStudents.length; i++) {
                            var es = transaction.currentEnrollment.enrollmentStudents[i];
                            es.students.enrollmentStudentId = es.id;
                            es.isActive = true;
                            transaction.students.push(es.students);

                        }
                    }

                    transactionService.update(transaction)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };



            //       vm.save = function () {
            //           if (vm.saving === true)
            //               return;
            //           vm.saving = true;
            //           try {
            //	transactionService.update(vm.transaction)
            //	.then(function () {
            //		vm.saving = false;
            //		abp.notify.info(App.localize('SavedSuccessfully'));
            //		$uibModalInstance.close();
            //	}, function(e){
            //	   vm.saving = false;
            //                      console.log(e.data.message);
            //	});
            //} 
            //catch (e)
            //               {
            //                  vm.saving = false;
            //               }
            //       };

            //XXXInsertCallRelatedEntitiesXXX

            vm.banks = [];
            vm.getBanks = function () {
                bankService.getAllBanksForCombo().then(function (result) {
                    vm.banks = result.data;
                    App.initAjax();
                });
            }

            vm.paymentMethods = [];
            vm.getPaymentMethods = function () {
                paymentMethodService.getAllPaymentMethodsForCombo().then(function (result) {
                    vm.paymentMethods = result.data;
                    App.initAjax();
                });
            }

            vm.origins = [];
            vm.getOrigins = function () {
                originService.getAllOriginsForCombo().then(function (result) {
                    vm.origins = result.data;
                    App.initAjax();
                    $scope.$apply();
                });
            }

            vm.concepts = [];
            vm.getConcepts = function () {
                conceptService.getAllConceptsForCombo().then(function (result) {
                    vm.concepts = result.data;
                    App.initAjax();
                });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };


            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('student'),
                        field: 'students.fullName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Course'),
                        field: 'students.course.abbreviation',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Amount'),
                        field: 'amount',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class=\"form-group\" style=\"text-align:right; width:100%; margin-left:2px;\">' +
                        '<input type=\"number\" class="paymentBox form-control" min=\"0.00\" step=\"0.01\" id=\"txtAmount\" class="\md-check\" ng-model=\"row.entity.students.amount\" onchange=\"calculateTotal();\" onkeypress=\"return enterToTab(event,this);\"  readonly disable>' +
                        '   </div>'
                        ,
                        width: 150
                    }
                ]
            };

            function getStudents() {
                $scope.gridOptions.data = [{ students_name: '', Course: '' }];
            }


            getStudents();


            init();
        }
    ]);
})();

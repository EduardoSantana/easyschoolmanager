(function () {
    angular.module('MetronicApp').controller('app.views.transactions.accountBalance', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.transaction', 'settings', 'report', 'abp.services.app.enrollment',
        function ($scope, $timeout, $uibModal, transactionService, settings, report, enrollmentService) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getTransactions(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.transactions = [];

            function CleanTransaction()
            {
                vm.phone2 = '';
                vm.phone1 = '';
                vm.emailAddress = '';
                vm.transactions = [];
                $scope.$broadcast('angucomplete-alt:clearInput', 'enrollmentsAngu');
            }

            $scope.$watch("vm.sequence", function (newValue, oldValue) {
                CleanTransaction();
                vm.refresh();
            });

            vm.searchTransactions = function () {

                transactionService.getEnrollmentBalance({ sequence: vm.sequence }).then(function (result) {
                    if (result.data == null)
                    {
                        CleanTransaction();
                        return;
                    }
                    vm.enrollmentFullName = result.data.enrollmentName;
                    vm.totalBalance = result.data.totalBalance;
                    vm.totalDebit = result.data.totalDebit;
                    vm.totalCredit = result.data.totalCredit;
                    vm.absTotalBalance = result.data.absTotalBalance;
                    vm.transactions = result.data.transactions;
                    vm.phone1 = result.data.transactions[0].phone1;
                    vm.phone2 = result.data.transactions[0].phone2;
                    vm.emailAddress = result.data.transactions[0].emailAddress;
                    $scope.$broadcast('angucomplete-alt:changeInput', 'enrollmentsAngu', vm.enrollmentFullName);

                    vm.refresh();
                });
            }

            $scope.getEnrollmentsByName = function (userInputString) {
                $scope.pagination.maxResultCount = 10;
                $scope.pagination.textFilter = userInputString;
                return enrollmentService.getAllEnrollments($scope.pagination);
            }

            $scope.returnedValue = function (result) {
                if (!result) return;
                vm.sequence = result.originalObject.enrollment;
                vm.enrollmentFullName = result.originalObject.firstName + " " + result.originalObject.lastName;
                vm.phone1 = result.originalObject.phone1;
                vm.phone2 = result.originalObject.phone2;
                vm.emailAddress = result.originalObject.emailAddress;
                vm.searchTransactions();
            }
            vm.searchAccountBalance = function () {

                var objectReport = {};
                var reportsFilters = [];

                objectReport.reportCode = "05";
                objectReport.reportExport = "PDF";
                reportsFilters.push({ name: "Enrollment", value: vm.sequence });

                objectReport.reportsFilters = reportsFilters;
                report.print(objectReport);
            };

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('TransactionDate'),
                        field: 'date',
                        width: 125,
                        cellFilter: 'date:"dd-MM-yyyy\"'
                    },
                    {
                        name: App.localize('Number'),
                        field: 'transactionNumber',
                        width: 200
                    },
                    {
                        name: App.localize('TransactionConcept'),
                        field: 'concept',
                        minWidth: 300
                    },
                    {
                        name: App.localize('Debit'),
                        field: 'debit',
                        width: 125,
                        cellFilter: 'number:2',
                        cellClass: "font-uigrid-right"
                    },
                    {
                        name: App.localize('Credit'),
                        field: 'credit',
                        width: 125,
                        cellFilter: 'number:2',
                        cellClass: "font-uigrid-right",
                        headerClass: "font-uigrid-right"
                    },
                    {
                        name: App.localize('Balance'),
                        field: 'absBalance',
                        width: 125,
                        cellFilter: 'number:2',
                        cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                            if (row.entity.balance < 0) {
                                return 'red-font font-uigrid-right';
                            }
                            else
                                return 'green-font font-uigrid-right';
                        }
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });



            vm.refresh = function () {
                $scope.gridOptions.data = vm.transactions;
            };

        }
    ]);
})();

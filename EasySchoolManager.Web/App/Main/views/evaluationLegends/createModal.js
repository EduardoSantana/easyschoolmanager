(function () {
    angular.module('MetronicApp').controller('app.views.evaluationLegends.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.evaluationLegend', 
        function ($scope, $uibModalInstance, evaluationLegendService ) {
            var vm = this;
            vm.saving = false;

            vm.evaluationLegend = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     evaluationLegendService.create(vm.evaluationLegend)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#evaluationLegendName").focus(); }, 100);
        }
    ]);
})();

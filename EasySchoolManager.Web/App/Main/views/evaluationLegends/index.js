(function () {
    angular.module('MetronicApp').controller('app.views.evaluationLegends.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.evaluationLegend', 'settings',
        function ($scope, $timeout, $uibModal, evaluationLegendService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getEvaluationLegends(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.evaluationLegends = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 90,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openEvaluationLegendEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },


                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 60
                    },

                    {
                        name: App.localize('EvaluationLegendName'),
                        field: 'name',
                        width: 100
                    },
                    {
                        name: App.localize('EvaluationLegendDescription'),
                        field: 'description',
                        minWidth: 500
                    },
                    {
                        name: App.localize('EvaluationLegendSequence'),
                        field: 'sequence',
                        minWidth: 500
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getEvaluationLegends(showTheLastPage) {
                evaluationLegendService.getAllEvaluationLegends($scope.pagination).then(function (result) {
                    vm.evaluationLegends = result.data.items;

                    $scope.gridOptions.data = vm.evaluationLegends;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openEvaluationLegendCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/evaluationLegends/createModal.cshtml',
                    controller: 'app.views.evaluationLegends.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getEvaluationLegends(false);
                });
            };

            vm.openEvaluationLegendEditModal = function (evaluationLegend) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/evaluationLegends/editModal.cshtml',
                    controller: 'app.views.evaluationLegends.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return evaluationLegend.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getEvaluationLegends(false);
                });
            };

            vm.delete = function (evaluationLegend) {
                abp.message.confirm(
                    "Delete evaluationLegend '" + evaluationLegend.name + "'?",
                    function (result) {
                        if (result) {
                            evaluationLegendService.delete({ id: evaluationLegend.id })
                                .then(function (result) {
                                    getEvaluationLegends(false);
                                    abp.notify.info("Deleted evaluationLegend: " + evaluationLegend.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getEvaluationLegends(false);
            };

            getEvaluationLegends(false);
        }
    ]);
})();

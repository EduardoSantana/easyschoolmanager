(function () {
    angular.module('MetronicApp').controller('app.views.evaluationLegends.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.evaluationLegend', 'id', 
        function ($scope, $uibModalInstance, evaluationLegendService, id ) {
            var vm = this;
			vm.saving = false;

            vm.evaluationLegend = {
                isActive: true
            };
            var init = function () {
                evaluationLegendService.get({ id: id })
                    .then(function (result) {
                        vm.evaluationLegend = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#evaluationLegendName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						evaluationLegendService.update(vm.evaluationLegend)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

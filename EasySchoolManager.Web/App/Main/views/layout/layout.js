﻿(function () {
    var controllerId = 'MetronicApp.views.layout';
    angular.module('MetronicApp').controller(controllerId, [
        '$rootScope', '$scope', 'settings', function ($rootScope, $scope, settings) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                // initialize core components
                App.initAjax();

                // set default layout mode
                $rootScope.$settings.settings.layout.pageContentWhite = true;
                $rootScope.$settings.settings.layout.pageBodySolid = false;
                $rootScope.$settings.settings.layout.pageSidebarClosed = false;
            });
         
        }
    ]);
})();
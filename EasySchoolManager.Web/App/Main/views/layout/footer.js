﻿(function () {
    var controllerId = 'MetronicApp.views.layout.footer';
    angular.module('MetronicApp').controller(controllerId, [
        '$rootScope', '$scope', 'settings', function ($rootScope, $scope, settings) {
            var vm = this;

            $scope.$on('$includeContentLoaded', function() {
                Layout.initFooter(); // init footer
            });
         
        }
    ]);
})();
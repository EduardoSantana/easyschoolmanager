﻿(function () {
    var controllerId = 'MetronicApp.views.layout.theme-panel';
    angular.module('MetronicApp').controller(controllerId, [
        '$rootScope', '$scope', 'settings', function ($rootScope, $scope, settings) {
            var vm = this;

            $scope.$on('$includeContentLoaded', function () {
                //Demo.init(); // init theme panel
            });
         
        }
    ]);
})();
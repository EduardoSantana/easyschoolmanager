﻿(function () {
    var controllerId = 'MetronicApp.views.layout.quick-sidebar';
    angular.module('MetronicApp').controller(controllerId, [
        '$rootScope', '$scope', 'settings', function ($rootScope, $scope, settings) {
            var vm = this;

            $scope.$on('$includeContentLoaded', function () {
                setTimeout(function () {
                    QuickSidebar.init(); // init quick sidebar        
                }, 2000)
            });
         
        }
    ]);
})();
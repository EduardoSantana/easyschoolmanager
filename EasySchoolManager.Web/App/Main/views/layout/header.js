﻿(function () {
    var controllerId = 'MetronicApp.views.layout.header';
    angular.module('MetronicApp').controller(controllerId, [
        '$rootScope', '$scope', '$uibModal', 'settings', 'appSession', 'abp.services.app.transaction','abp.services.app.user','abp.services.app.message','$timeout',
        function ($rootScope, $scope, $uibModal, settings, appSession, transactionService, userService, messageService, $timeout) {
            var vm = this;

            vm.currentTenantId = (appSession.tenant ? appSession.tenant.id : -1);

            vm.settings = settings;
            vm.pagesProfile = abp.auth.hasPermission('Pages.Profile');
            vm.defaultPicture = vm.settings.settings.assetsPath + "/pages/media/users/avatar3.png";
            vm.message = {
                messages : [],
                count : 0
            };

            vm.messageFilter = {
            maxResultCount : 4,
            readedMessages : false
            };
            
            vm.getMessages= function() {
                    messageService.getAllMessagesSubject( vm.messageFilter).then(function(result){
                    vm.message.messages = result.data.items;
                    vm.message.count = result.data.totalCount;
                    $timeout(vm.getMessages,50000);
                });
            }

            vm.showLogo = function showLogo() {
                $("#logo-custom1-sidebar").toggle();
            };
            $scope.$on('$includeContentLoaded', function() {
                Layout.initHeader(); // init header
            });

            transactionService.applyExpiredPaymentProjections().then(function (result) {
                var data = result;
            });

            userService.get({id: abp.session.userId}).then(function(result){
                vm.currentUser = result.data;
            });
            
            vm.userBellongToSchool = false;
            if (appSession.tenant != null)
                vm.userBellongToSchool = true;
            
            vm.languages = [];
            vm.currentLanguage = {};
            vm.userEmailAddress = "";
            vm.getShownUserName = "";
            vm.iduser = 0;

            function init() {
                vm.languages = abp.localization.languages;
                vm.currentLanguage = abp.localization.currentLanguage;
                vm.userEmailAddress = appSession.user.emailAddress;
                vm.iduser = abp.session.userId;
                vm.getShownUserName = shownUserName();
                vm.getMessages();
            }

            function shownUserName() {
                if (!abp.multiTenancy.isEnabled) {
                    return appSession.user.userName;
                } else {
                    if (appSession.tenant) {
                        return appSession.user.userName + '/' + (appSession.tenant.abbreviation ? appSession.tenant.abbreviation : appSession.tenant.tenancyName);
                    } else {
                        return appSession.user.userName + '/.';
                    }
                }
            }

            //prueba
            vm.openReceiptCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/receipts/createModal.cshtml',
                    controller: 'app.views.receipts.createModal as vm',
                    size: 'lg',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getReceipts(true);
                });
            };
            //prueba

            vm.changeLanguage = function (languageName) {
                location.href = abp.appPath + 'AbpLocalization/ChangeCulture?cultureName=' + languageName + '&returnUrl=' + window.location.pathname + window.location.hash;
            }

            $scope.$watch("vm.currentTenantId", function (newValue, oldValue) {
                if (newValue != null && newValue != (appSession.tenant ? appSession.tenant.id : -1)) {
                    location.href = encodeURI(abp.appPath + 'Account/ChangeTenantCbo/' + newValue + '?returnUrl=/#/');
                }
            });

            init();

        }
    ]);
})();
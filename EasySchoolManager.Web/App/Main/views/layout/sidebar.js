(function () {
    var controllerId = 'MetronicApp.views.layout.sidebar';
    angular.module('MetronicApp').controller(controllerId, [
        '$rootScope', '$state', '$scope', 'appSession', 'abp.services.app.report',
        function ($rootScope, $state, $scope, appSession, reportService) {


            $scope.$on('$includeContentLoaded', function () {
                Layout.initSidebar($state); // init sidebar
            });
            var JustGeneral = 0;
            var JustTenants = 1;
            var GeneralAndTenants = 2;

            $scope.currentTenantId = appSession.tenant;

            var vm = this;
            vm.lengthMenu = 16;
            vm.reports = [];

            vm.setShowMenu = function setShowMenu() {
                $rootScope.$settings.settings.layout.showmenu = true;
            };
            vm.menuItems = [
                createMenuItem(App.localize("Maintenances"), "", "icon-settings", "", GeneralAndTenants, [
                    createMenuItem(App.localize("Generals"), "", "icon-briefcase", "", GeneralAndTenants, [
                        createMenuItem(App.localize("Relationships"), "Pages.Relationships", "icon-briefcase", "relationships", JustGeneral)
                        , createMenuItem(App.localize("Religions"), "Pages.Religions", "icon-briefcase", "religions", JustGeneral)
                        , createMenuItem(App.localize("Regions"), "Pages.Regions", "icon-briefcase", "regions", JustGeneral)
                        , createMenuItem(App.localize("Illnesses"), "Pages.Illnesses", "icon-briefcase", "illnesses", JustGeneral)
                        , createMenuItem(App.localize("Allergies"), "Pages.Allergies", "icon-briefcase", "allergies", JustGeneral)
                        , createMenuItem(App.localize("Occupations"), "Pages.Occupations", "icon-briefcase", "occupations", JustGeneral)
                        , createMenuItem(App.localize("Districts"), "Pages.Districts", "icon-briefcase", "districts", JustGeneral)
                        , createMenuItem(App.localize("Messages"), "Pages.Messages", "icon-briefcase", "messages", GeneralAndTenants)

                    ]),
                    createMenuItem(App.localize("Static"), "", "icon-graduation", "", GeneralAndTenants, [
                        createMenuItem(App.localize("Cities"), "Pages.Cities", "icon-briefcase", "cities", JustGeneral)
                        , createMenuItem(App.localize("Bloods"), "Pages.Bloods", "icon-briefcase", "bloods", JustGeneral)
                        , createMenuItem(App.localize("Provinces"), "Pages.Provinces", "icon-briefcase", "provinces", JustGeneral)
                    //    , createMenuItem(App.localize("Genders"), "Pages.Genders", "icon-briefcase", "genders", JustGeneral)
                       // , createMenuItem(App.localize("Origins"), "Pages.Origins", "icon-wallet", "origins", JustGeneral)
                        , createMenuItem(App.localize("PaymentMethods"), "Pages.PaymentMethods", "icon-wallet", "paymentMethods", JustGeneral)
                        , createMenuItem(App.localize("ButtonPositions"), "Pages.ButtonPositions", "icon-wallet", "buttonPositions", JustGeneral)
                    ]),
                    createMenuItem(App.localize("Academic"), "", "icon-graduation", "", GeneralAndTenants, [
                        createMenuItem(App.localize("Courses"), "Pages.Courses", "icon-badge", "courses", JustGeneral)
                        , createMenuItem(App.localize("CourseSessions"), "Pages.CourseSessions", "icon-action-undo", "courseSessions", JustGeneral)
                        , createMenuItem(App.localize("CommentTypes"), "Pages.CommentTypes", "icon-briefcase", "commentTypes", JustGeneral)
                        , createMenuItem(App.localize("DocumentTypes"), "Pages.DocumentTypes", "icon-briefcase", "documentTypes", JustGeneral)
                        , createMenuItem(App.localize("Levels"), "Pages.Levels", "icon-crop", "levels", JustGeneral)
                        , createMenuItem(App.localize("Teachers"), "Pages.Teachers", "icon-briefcase", "teachers", JustGeneral)
                        , createMenuItem(App.localize("Indicators"), "Pages.Indicators", "icon-briefcase", "indicators", JustGeneral)
                        , createMenuItem(App.localize("EvaluationLegends"), "Pages.EvaluationLegends", "icon-briefcase", "evaluationLegends", JustGeneral)
                        , createMenuItem(App.localize("IndicatorGroups"), "Pages.IndicatorGroups", "icon-briefcase", "indicatorGroups", JustGeneral)
                        , createMenuItem(App.localize("Subjects"), "Pages.Subjects", "icon-briefcase", "subjects", JustGeneral)
                        , createMenuItem(App.localize("EvaluationPeriods"), "Pages.EvaluationPeriods", "icon-briefcase", "evaluationPeriods", JustGeneral)

                        , createMenuItem(App.localize("SubjectHighs"), "Pages.SubjectHighs", "icon-briefcase", "subjectHighs", JustGeneral)
                        , createMenuItem(App.localize("EvaluationPositionHighs"), "Pages.EvaluationPositionHighs", "icon-briefcase", "evaluationPositionHighs", JustGeneral)
                        , createMenuItem(App.localize("EvaluationOrderHighs"), "Pages.EvaluationOrderHighs", "icon-briefcase", "evaluationOrderHighs", JustGeneral)


                    ]),
                    createMenuItem(App.localize("Financial"), "", "icon-wallet", "", GeneralAndTenants, [
                        createMenuItem(App.localize("Catalogs"), "Pages.Catalogs", "icon-wallet", "catalogs", JustGeneral)
                        , createMenuItem(App.localize("ChangeTypes"), "Pages.ChangeTypes", "icon-wallet", "changeTypes", JustGeneral)
                        , createMenuItem(App.localize("Banks"), "Pages.Banks", "icon-wallet", "banks", JustGeneral)
                        , createMenuItem(App.localize("Concepts"), "Pages.Concepts", "icon-wallet", "concepts", JustGeneral)
                        , createMenuItem(App.localize("Periods"), "Pages.Periods", "icon-wallet", "periods", JustGeneral)
                        , createMenuItem(App.localize("CourseValues"), "Pages.CourseValues", "icon-wallet", "courseValues", JustTenants)
                        , createMenuItem(App.localize("PaymentDates"), "Pages.PaymentDates", "icon-wallet", "paymentDates", JustTenants)
                        , createMenuItem(App.localize("BankAccounts"), "Pages.BankAccounts", "icon-wallet", "bankAccounts", JustGeneral)
                        , createMenuItem(App.localize("ReceiptTypes"), "Pages.ReceiptTypes", "icon-wallet", "receiptTypes", JustGeneral)
                        , createMenuItem(App.localize("GrantEnterprises"), "Pages.GrantEnterprises", "icon-wallet", "grantEnterprises", JustTenants)
                        , createMenuItem(App.localize("TaxReceiptTypes"), "Pages.TaxReceiptTypes", "icon-wallet", "taxReceiptTypes", JustGeneral)
                        , createMenuItem(App.localize("TaxReceipts"), "Pages.TaxReceipts", "icon-wallet", "taxReceipts", JustGeneral)
                        , createMenuItem(App.localize("Companies"), "Pages.Companies", "icon-wallet", "companies", JustGeneral)
                        , createMenuItem(App.localize("TransactionTypes"), "Pages.TransactionTypes", "icon-wallet", "transactionTypes", JustGeneral)
                        , createMenuItem(App.localize("DiscountNames"), "Pages.DiscountNames", "icon-wallet", "discountNames", JustGeneral)
                        , createMenuItem(App.localize("PeriodDiscounts"), "Pages.PeriodDiscounts", "icon-wallet", "periodDiscounts", JustGeneral)
                    ]),
                    createMenuItem(App.localize("Inventory"), "", "icon-notebook", "", GeneralAndTenants, [
                        createMenuItem(App.localize("Units"), "Pages.Units", "icon-briefcase", "Units", JustGeneral)
                        , createMenuItem(App.localize("Brands"), "Pages.Brands", "icon-briefcase", "brands", JustGeneral)
                        , createMenuItem(App.localize("ArticlesTypes"), "Pages.ArticlesTypes", "icon-briefcase", "articlesTypes", JustGeneral)
                        , createMenuItem(App.localize("Articles"), "Pages.Articles", "icon-briefcase", "articles", JustGeneral)
                        , createMenuItem(App.localize("Providers"), "Pages.Providers", "icon-briefcase", "providers", JustGeneral)
                        , createMenuItem(App.localize("CourrierPersons"), "Pages.CourrierPersons", "icon-briefcase", "courrierPersons", JustGeneral)
                        , createMenuItem(App.localize("ArticleShops"), "Pages.ArticleShops", "icon-briefcase", "articleShops", JustTenants)
                        , createMenuItem(App.localize("ProviderShops"), "Pages.ProviderShops", "icon-briefcase", "providerShops", JustTenants)
                        , createMenuItem(App.localize("ClientShops"), "Pages.ClientShops", "icon-briefcase", "clientShops", JustTenants)
                        , createMenuItem(App.localize("Inventory"), "Pages.Inventory", "icon-briefcase", "inventory", JustGeneral)
                        , createMenuItem(App.localize("InventoryShops"), "Pages.InventoryShops", "icon-briefcase", "inventoryShops", JustTenants)

                    ]),
                    createMenuItem(App.localize("Administrative"), "", "icon-notebook", "", GeneralAndTenants, [
                        createMenuItem(App.localize("Tenants"), "Pages.Tenants", "icon-globe", "tenants", JustGeneral)
                        , createMenuItem(App.localize("Users"), "Pages.Users", "icon-user", "users", GeneralAndTenants)
                        , createMenuItem(App.localize("Roles"), "Pages.Roles", "icon-link", "roles", GeneralAndTenants)
                        , createMenuItem(App.localize("PrinterTypes"), "Pages.PrinterTypes", "icon-printer", "printerTypes", JustGeneral)
                        , createMenuItem(App.localize("Reports"), "Pages.ReportsMain", "icon-layers", "reportsmain", GeneralAndTenants)
                        , createMenuItem(App.localize("MasterTenants"), "Pages.MasterTenants", "icon-wallet", "masterTenants", JustGeneral)
                        , createMenuItem(App.localize("Positions"), "Pages.Positions", "icon-wallet", "positions", JustGeneral)
                    ])
                    //createMenuItem(App.localize("Dimension"), "", "icon-notebook", "", GeneralAndTenants, [

                    //    , createMenuItem(App.localize("T3Dimensions"), "Pages.T3Dimensions", "", "t3Dimensions", JustGeneral)
                    //    ,createMenuItem(App.localize("T10Dimensions"), "Pages.T10Dimensions", "", "t10Dimensions", JustGeneral)
                    //    ,createMenuItem(App.localize("T9Dimensions"), "Pages.T9Dimensions", "", "t9Dimensions", JustGeneral)
                    //    ,createMenuItem(App.localize("T8Dimensions"), "Pages.T8Dimensions", "", "t8Dimensions", JustGeneral)
                    //    ,createMenuItem(App.localize("T7Dimensions"), "Pages.T7Dimensions", "", "t7Dimensions", JustGeneral)
                    //    ,createMenuItem(App.localize("T6Dimensions"), "Pages.T6Dimensions", "", "t6Dimensions", JustGeneral)
                    //    ,createMenuItem(App.localize("T5Dimensions"), "Pages.T5Dimensions", "", "t5Dimensions", JustGeneral)
                    //    ,createMenuItem(App.localize("T4Dimensions"), "Pages.T4Dimensions", "", "t4Dimensions", JustGeneral)
                    //    , createMenuItem(App.localize("T1Dimensions"), "Pages.T1Dimensions", "", "t1Dimensions", JustGeneral)
                    //    ,createMenuItem(App.localize("T2Dimensions"), "Pages.T2Dimensions", "", "t2Dimensions", JustGeneral)
                    //    ])


                    //  ,createMenuItem(App.localize("HistoryCancelReceipts"), "Pages.HistoryCancelReceipts", "", "historyCancelReceipts", JustGeneral)

                    //    ,createMenuItem(App.localize("SubjectSessions"), "Pages.SubjectSessions", "", "subjectSessions", JustGeneral)


                    //    ,createMenuItem(App.localize("SubjectSessionHighs"), "Pages.SubjectSessionHighs", "", "subjectSessionHighs", JustGeneral)


                    //    ,createMenuItem(App.localize("ConceptTenantRegistrations"), "Pages.ConceptTenantRegistrations", "", "conceptTenantRegistrations", JustGeneral)




                    //XXXInsertMenuItemXXX


                    //-----------------------------//
                ]),
                createMenuItem(App.localize("Process"), "", "icon-puzzle", "", GeneralAndTenants, [
                    createMenuItem(App.localize("Enrollments"), "Pages.Enrollments", "icon-puzzle", "enrollments", JustTenants)
                    , createMenuItem(App.localize("PreviousEnrollments"), "Pages.PreviousEnrollments", "icon-puzzle", "previousEnrollments", JustTenants)
                    , createMenuItem(App.localize("NextEnrollments"), "Pages.NextEnrollments", "icon-puzzle", "nextEnrollments", JustTenants)
                    , createMenuItem(App.localize("Receipts"), "Pages.Receipts", "icon-puzzle", "receipts", JustTenants)
                    , createMenuItem(App.localize("Transactions"), "Pages.Transactions", "icon-puzzle", "transactions", JustTenants)
                    , createMenuItem(App.localize("Deposits"), "Pages.Deposits", "icon-puzzle", "deposits", JustTenants)
                    , createMenuItem(App.localize("GenerateAllPaymentProjections"), "Pages.GenerateAllPaymentProjections", "icon-wallet", "generateAllPaymentProjections", JustTenants)
                    , createMenuItem(App.localize("Trades"), "Pages.Trades", "icon-puzzle", "trades", JustGeneral)
                    , createMenuItem(App.localize("Purchases"), "Pages.Purchases", "icon-puzzle", "purchases", JustGeneral)
                    , createMenuItem(App.localize("PurchaseShops"), "Pages.PurchaseShops", "icon-briefcase", "purchaseShops", JustTenants)
                    , createMenuItem(App.localize("TransactionShops"), "Pages.TransactionShops", "icon-briefcase", "transactionShops", JustTenants)
                    , createMenuItem(App.localize("SessionStudentSelections"), "Pages.SessionStudentSelections", "icon-briefcase", "sessionStudentSelections", JustTenants)
                    , createMenuItem(App.localize("SessionStudentSelectionPrevious"), "Pages.SessionStudentSelectionPrevious", "icon-briefcase", "sessionStudentSelectionPrevious", JustTenants)
                    , createMenuItem(App.localize("EvaluationHighs"), "Pages.EvaluationHighs", "icon-briefcase", "evaluationHighs", JustTenants)
                    , createMenuItem(App.localize("EvaluationHighsByPos"), "Pages.EvaluationHighsByPos", "icon-briefcase", "evaluationHighsByPos", JustTenants)
                    , createMenuItem(App.localize("EvaluationHighsByPosPrevious"), "Pages.EvaluationHighsByPosPrevious", "icon-briefcase", "evaluationHighsByPosPrevious", JustTenants)
                    , createMenuItem(App.localize("Evaluations"), "Pages.Evaluations", "icon-briefcase", "evaluations", JustTenants)
                    , createMenuItem(App.localize("TransactionQueues"), "Pages.TransactionQueues", "icon-briefcase", "TransactionQueues", JustGeneral)
                    , createMenuItem(App.localize("VerifoneBreaks"), "Pages.VerifoneBreaks", "icon-briefcase", "verifoneBreaks", JustTenants)
                    , createMenuItem(App.localize("Discounts"), "Pages.Discounts", "icon-wallet", "discounts", JustTenants)
                ]),
                createMenuItem(App.localize("Consult"), "", "icon-puzzle", "", GeneralAndTenants, [
                      createMenuItem(App.localize("AccountBalances"), "Pages.AccountBalances", "icon-wallet", "accountBalances", JustTenants)
                    , createMenuItem(App.localize("ViewPaymentProjection"), "Pages.ViewPaymentProjections", "icon-wallet", "viewPaymentProjections", JustTenants)
                ]),
                createMenuItem(App.localize("Cafeteria"), "", "icon-puzzle", "", GeneralAndTenants, [
                     createMenuItem(App.localize("AccountBalances"), "Pages.AccountBalances", "icon-coffee", "accountBalances", JustTenants)
                    , createMenuItem(App.localize("ViewPaymentProjection"), "Pages.ViewPaymentProjections", "icon-wallet", "viewPaymentProjections", JustTenants)
                ])
            ];

            vm.showMenuItem = function (menuItem) {
                if (menuItem.permissionName) {
                    return (abp.auth.isGranted(menuItem.permissionName) &&
                        (menuItem.isForTenant == GeneralAndTenants ||
                            (menuItem.isForTenant == JustTenants && $scope.currentTenantId != null) ||
                            (menuItem.isForTenant == JustGeneral && $scope.currentTenantId == null)
                        )
                    )
                }
                return true;
            };
            var tempCols0 = [];
            var tempCols1 = [];

            reportService.getAllReportsForMenu({}).then(function (result) {

                vm.reportTypes = result.data;
                var reportsMenu = [];
                if (vm.reportTypes.length > 0) {

                    for (var i in vm.reportTypes) {
                        var _reportType = vm.reportTypes[i];
                        var _reports = [];

                        for (var j in _reportType.reports) {
                            var _report = _reportType.reports[j];
                            _reports.push(createMenuItem(App.localize(_report.reportName), _report.permissionName || "Pages.Reports", "icon-layers", 'reports({id: ' + _report.id + '})', _report.isForTenant));
                        }

                        var reportTypeMenu = createMenuItem(App.localize(_reportType.nombre), "Pages.Reports", "icon-layers", "", GeneralAndTenants, _reports);
                        if (_reports.length > 0)
                            reportsMenu.push(reportTypeMenu);
                    }

                    vm.menuItems.push(createMenuItem(App.localize("Reports"), "Pages.Reports", "icon-layers", "reports", GeneralAndTenants, reportsMenu));

                    vm.menuItems = vm.deleteWithoutPerms(vm.menuItems);

                    sortByProperty(vm.menuItems, "name");

                    vm.menuItems.push(createMenuItem(App.localize("Dashboard"), "", "icon-home", "home", GeneralAndTenants));
                }

            });


            vm.deleteWithoutPerms = function (itemList) {
                var tempRetVal = [];
                for (var i = 0; i < itemList.length; i++) {
                    if (vm.showMenuItem(itemList[i])) {
                        if (itemList[i].items) {
                            itemList[i].items = vm.deleteWithoutPerms(itemList[i].items.slice());
                            if (itemList[i].items.length > 0) {
                                tempRetVal.push(itemList[i]);
                            }
                        } else {
                            tempRetVal.push(itemList[i]);
                        }
                    }
                }
                return tempRetVal;
            };

            vm.showMenuItem = function (menuItem) {
                if (menuItem == undefined || menuItem == null)
                    return false;
                try {

                    if (menuItem.permissionName) {
                        return (abp.auth.isGranted(menuItem.permissionName) &&
                            (menuItem.isForTenant == GeneralAndTenants ||
                                (menuItem.isForTenant == JustTenants && $scope.currentTenantId != null) ||
                                (menuItem.isForTenant == JustGeneral && $scope.currentTenantId == null)
                            )
                        )
                    }
                } catch (e) {
                    return false;
                }
                return true;
            }

            function createMenuItem(name, permissionName, icon, route, isForTenant, childItems) {
                return {
                    name: name,
                    permissionName: permissionName,
                    icon: icon,
                    route: route,
                    items: childItems,
                    isForTenant: isForTenant
                };
            }

            function sortByProperty(objArray, prop) {
                if (arguments.length < 2) throw new Error("ARRAY, AND OBJECT PROPERTY MINIMUM ARGUMENTS, OPTIONAL DIRECTION");
                if (!Array.isArray(objArray)) throw new Error("FIRST ARGUMENT NOT AN ARRAY");
                const clone = objArray.slice(0);
                const direct = arguments.length > 2 ? arguments[2] : 1; //Default to ascending
                const propPath = (prop.constructor === Array) ? prop : prop.split(".");
                clone.sort(function (a, b) {
                    for (let p in propPath) {
                        if (a[propPath[p]] && b[propPath[p]]) {
                            a = a[propPath[p]];
                            b = b[propPath[p]];
                        }
                    }
                    // convert numeric strings to integers
                    a = a.match(/^\d+$/) ? +a : a;
                    b = b.match(/^\d+$/) ? +b : b;
                    return ((a < b) ? -1 * direct : ((a > b) ? 1 * direct : 0));
                });
                return clone;
            }

        }
    ]);
})();
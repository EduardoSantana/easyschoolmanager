(function () {
    angular.module('MetronicApp').controller('app.views.regions.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.region', 
        function ($scope, $uibModalInstance, regionService ) {
            var vm = this;

            vm.region = {
                isActive: true
            };

            vm.save = function () {
                vm.saving = true;
                try {
                     regionService.create(vm.region)
                        .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			
			//XXXInsertCallRelatedEntitiesXXX


			
			

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };
			
			
			
			
		    App.initAjax();
			
			setTimeout(function () { $("#regionName").focus(); }, 100);
        }
    ]);
})();

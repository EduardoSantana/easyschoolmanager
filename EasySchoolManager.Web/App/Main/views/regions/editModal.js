(function () {
    angular.module('MetronicApp').controller('app.views.regions.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.region', 'id', 
        function ($scope, $uibModalInstance, regionService, id ) {
            var vm = this;

            vm.region = {
                isActive: true
            };

            var init = function () {
                regionService.get({ id: id })
                    .then(function (result) {
                        vm.region = result.data;
						
						
						
						App.initAjax();
						setTimeout(function () { $("#regionName").focus(); }, 100);
                    });
            }

            vm.save = function () {
                regionService.update(vm.region)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });
            };
			
			
			//XXXInsertCallRelatedEntitiesXXX


			
			

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

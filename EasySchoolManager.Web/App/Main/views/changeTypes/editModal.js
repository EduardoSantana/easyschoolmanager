(function () {
    angular.module('MetronicApp').controller('app.views.changeTypes.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.changeType', 'id', 
        function ($scope, $uibModalInstance, changeTypeService, id ) {
            var vm = this;
			vm.saving = false;

            vm.changeType = {
                isActive: true
            };
            var init = function () {
                changeTypeService.get({ id: id })
                    .then(function (result) {
                        vm.changeType = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#changeTypeName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						changeTypeService.update(vm.changeType)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

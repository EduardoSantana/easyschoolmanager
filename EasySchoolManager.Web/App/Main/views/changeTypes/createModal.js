(function () {
    angular.module('MetronicApp').controller('app.views.changeTypes.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.changeType', 
        function ($scope, $uibModalInstance, changeTypeService ) {
            var vm = this;
            vm.saving = false;

            vm.changeType = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     changeTypeService.create(vm.changeType)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#changeTypeName").focus(); }, 100);
        }
    ]);
})();

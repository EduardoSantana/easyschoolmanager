(function () {
    angular.module('MetronicApp').controller('app.views.providerShops.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.providerShop', 'id', 
        function ($scope, $uibModalInstance, providerShopService, id ) {
            var vm = this;
			vm.saving = false;

            vm.providerShop = {
                isActive: true
            };
            var init = function () {
                providerShopService.get({ id: id })
                    .then(function (result) {
                        vm.providerShop = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#providerShopName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						providerShopService.update(vm.providerShop)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

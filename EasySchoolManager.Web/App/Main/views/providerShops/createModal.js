(function () {
    angular.module('MetronicApp').controller('app.views.providerShops.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.providerShop', 
        function ($scope, $uibModalInstance, providerShopService ) {
            var vm = this;
            vm.saving = false;

            vm.providerShop = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     providerShopService.create(vm.providerShop)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#providerShopName").focus(); }, 100);
        }
    ]);
})();

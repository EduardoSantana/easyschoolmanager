(function () {
    angular.module('MetronicApp').controller('app.views.genders.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.gender', 
        function ($scope, $uibModalInstance, genderService ) {
            var vm = this;
            vm.saving = false;

            vm.gender = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     genderService.create(vm.gender)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#genderName").focus(); }, 100);
        }
    ]);
})();

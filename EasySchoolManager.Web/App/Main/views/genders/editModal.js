(function () {
    angular.module('MetronicApp').controller('app.views.genders.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.gender', 'id', 
        function ($scope, $uibModalInstance, genderService, id ) {
            var vm = this;
			vm.saving = false;

            vm.gender = {
                isActive: true
            };
            var init = function () {
                genderService.get({ id: id })
                    .then(function (result) {
                        vm.gender = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#genderName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						genderService.update(vm.gender)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

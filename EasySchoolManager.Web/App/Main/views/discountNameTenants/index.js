(function () {
    angular.module('MetronicApp').controller('app.views.discountNameTenants.index', [
        '$scope', '$timeout', '$uibModalInstance', 'abp.services.app.discountNameTenant', 'settings', 'periodDiscountId',
        function ($scope, $timeout, $uibModalInstance, discountNameTenantService, settings, periodDiscountId) {
            var vm = this;
            vm.textFilter = "";
            vm.periodDiscountId = periodDiscountId;

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getDiscountNameTenants(false);
            }
            vm.discountNameTenants = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '    <ul uib-dropdown-menu>' +
                         '      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '</div>'
                    },

                    {
                        name: App.localize('Tenant'),
                        field: 'tenant.name',
                        minWidth: 125
                    },

                ]
            };


            function getDiscountNameTenants(showTheLastPage, periodDiscountId) {

                //Utilize la variable period para buscar los discountNames para no tener que crear otro objeto.
                $scope.pagination.periodId = periodDiscountId;

                discountNameTenantService.getAllDiscountNameTenants($scope.pagination).then(function (result) {
                    vm.discountNameTenants = result.data.items;

                    $scope.gridOptions.data = vm.discountNameTenants;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

         

            vm.delete = function (discountNameTenant) {
                abp.message.confirm(
                    App.localize("AyouYouSureYouWantToDeleteThisTenant"),
                    function (result) {
                        if (result) {
                            discountNameTenantService.delete({ id: discountNameTenant.id })
                                .then(function (result) {
                                    getDiscountNameTenants(false, vm.periodDiscountId);
                                    abp.notify.info(App.localize("TenantRemovedSuccessfully"));

                                });
                        }
                    });
            }



            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.refresh = function () {
                getDiscountNameTenants(false, vm.periodDiscountId);
            };

            getDiscountNameTenants(false, vm.periodDiscountId);
        }
    ]);
})();

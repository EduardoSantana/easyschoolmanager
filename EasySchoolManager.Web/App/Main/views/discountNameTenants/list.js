(function () {
    angular.module('MetronicApp').controller('app.views.discountNameTenants.list', [
        '$scope', '$uibModalInstance', 'abp.services.app.discountNameTenant', 'abp.services.app.discountName', 'abp.services.app.tenant', 'abp.services.app.periodDiscount' , 'periodDiscountId',
        function ($scope, $uibModalInstance, discountNameTenantService, discountNameService, tenantService, periodDiscountService, periodDiscountId) {
            var vm = this;
            vm.saving = false;

            vm.discountNameTenant = {
                periodDiscountId: periodDiscountId,
                isActive: true
            };
          

            //XXXInsertCallRelatedEntitiesXXX

            vm.discountNames = [];
            vm.getDiscountNames = function () {
                discountNameService.getAllDiscountNamesForCombo().then(function (result) {
                    vm.discountNames = result.data;
                    App.initAjax();
                });
            }

            vm.periodDiscounts = [];
            vm.gePeriodDiscounts = function () {    
                periodDiscountService.getAllPeriodDiscountsForCombo().then(function (result) {
                    vm.periodDiscounts = result.data;
                    App.initAjax();
                });
            }

            vm.tenants = [];
            vm.getTenants = function () {
                tenantService.getAllTenantsForCombo().then(function (result) {
                    vm.tenants = result.data;
                    App.initAjax();
                });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.gePeriodDiscounts();
            vm.getTenants();

            App.initAjax();
            setTimeout(function () { $("#tradeTenantReceiveId").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.discountNameTenants.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.discountNameTenant', 'id', 'abp.services.app.discountName',
        function ($scope, $uibModalInstance, discountNameTenantService, id , discountNameService) {
            var vm = this;
			vm.saving = false;

            vm.discountNameTenant = {
                isActive: true
            };
            var init = function () {
                discountNameTenantService.get({ id: id })
                    .then(function (result) {
                        vm.discountNameTenant = result.data;
						                vm.getDiscountNames();

						App.initAjax();
						setTimeout(function () { $("#discountNameTenantDiscountNameId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						discountNameTenantService.update(vm.discountNameTenant)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.discountNames = [];
            vm.getDiscountNames	 = function()
            {
                discountNameService.getAllDiscountNamesForCombo().then(function (result) {
                    vm.discountNames = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

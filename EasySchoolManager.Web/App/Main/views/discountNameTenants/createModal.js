(function () {
    angular.module('MetronicApp').controller('app.views.discountNameTenants.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.discountNameTenant', 'abp.services.app.discountName', 'abp.services.app.tenant', 'abp.services.app.periodDiscount' , 'periodDiscountId',
        function ($scope, $uibModalInstance, discountNameTenantService, discountNameService, tenantService, periodDiscountService, periodDiscountId) {
            var vm = this;
            vm.saving = false;

            vm.discountNameTenant = {
                periodDiscountId: periodDiscountId,
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    discountNameTenantService.create(vm.discountNameTenant)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            //XXXInsertCallRelatedEntitiesXXX

            vm.discountNames = [];
            vm.getDiscountNames = function () {
                discountNameService.getAllDiscountNamesForCombo().then(function (result) {
                    vm.discountNames = result.data;
                    App.initAjax();
                });
            }

            vm.periodDiscounts = [];
            vm.gePeriodDiscounts = function () {    
                periodDiscountService.getAllPeriodDiscountsForCombo().then(function (result) {
                    vm.periodDiscounts = result.data;
                    App.initAjax();
                });
            }

            vm.tenants = [];
            vm.getTenants = function () {
                tenantService.getAllTenantsForCombo().then(function (result) {
                    vm.tenants = result.data;
                    App.initAjax();
                });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.gePeriodDiscounts();
            vm.getTenants();

            App.initAjax();
            setTimeout(function () { $("#tradeTenantReceiveId").focus(); }, 100);
        }
    ]);
})();

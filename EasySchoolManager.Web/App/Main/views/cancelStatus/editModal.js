(function () {
    angular.module('MetronicApp').controller('app.views.cancelStatus.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.cancelStatu', 'id', 
        function ($scope, $uibModalInstance, cancelStatuService, id ) {
            var vm = this;
			vm.saving = false;

            vm.cancelStatu = {
                isActive: true
            };
            var init = function () {
                cancelStatuService.get({ id: id })
                    .then(function (result) {
                        vm.cancelStatu = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#cancelStatuDescription").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						cancelStatuService.update(vm.cancelStatu)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

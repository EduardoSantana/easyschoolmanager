(function () {
    angular.module('MetronicApp').controller('app.views.cancelStatus.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.cancelStatu', 
        function ($scope, $uibModalInstance, cancelStatuService ) {
            var vm = this;
            vm.saving = false;

            vm.cancelStatu = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     cancelStatuService.create(vm.cancelStatu)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#cancelStatuDescription").focus(); }, 100);
        }
    ]);
})();

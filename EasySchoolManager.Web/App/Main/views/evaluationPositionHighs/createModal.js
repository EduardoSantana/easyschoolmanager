(function () {
    angular.module('MetronicApp').controller('app.views.evaluationPositionHighs.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.evaluationPositionHigh', 
        function ($scope, $uibModalInstance, evaluationPositionHighService ) {
            var vm = this;
            vm.saving = false;

            vm.evaluationPositionHigh = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     evaluationPositionHighService.create(vm.evaluationPositionHigh)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#evaluationPositionHighDescription").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.evaluationPositionHighs.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.evaluationPositionHigh', 'id', 
        function ($scope, $uibModalInstance, evaluationPositionHighService, id ) {
            var vm = this;
			vm.saving = false;

            vm.evaluationPositionHigh = {
                isActive: true
            };
            var init = function () {
                evaluationPositionHighService.get({ id: id })
                    .then(function (result) {
                        vm.evaluationPositionHigh = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#evaluationPositionHighDescription").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						evaluationPositionHighService.update(vm.evaluationPositionHigh)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

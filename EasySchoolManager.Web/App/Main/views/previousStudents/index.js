(function () {
    angular.module('MetronicApp').controller('app.views.previousStudents.index', [
        '$scope', '$timeout', '$uibModal', '$uibModalInstance', 'abp.services.app.student', 'settings', 'report',
        function ($scope, $timeout, $uibModal, $uibModalInstance, studentService, settings, report) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.enrollmentId = $scope.$resolve.enrollment.id;
            vm.enrollment = $scope.$resolve.enrollment;
            $scope.pagination.reload = function () {
                getStudents(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.students = [];

            vm.permissions = {
                delet: abp.auth.hasPermission('Pages.Students.PreviousDelete')
            };

            $scope.gridOptions = {
                rowHeight: 39,
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 150,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '    <button  title="' + App.localize("Edit") + '"class="btn btn-xs btn-default gray fa-grid-icon1" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.openStudentEditModal(row.entity)"><i class="fa fa-pencil"></i></button>' +
                        '    <button  title="' + App.localize("PassStudentToCurrentPeriod") + '"class="btn btn-xs btn-default gray fa-grid-icon1" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.passStudentToCurrentPeriod(row.entity)"><i class="fa fa-share"></i></button>' +
                        '    <button title="' + App.localize("ViewCalification") + '" data-content="Ver Calificaciones" class="buttonss btn btn-xs btn-default gray fa-grid-icon1" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.evaluations(row.entity)"><i class="fa fa-clipboard"></i></button>' +
                        '    <button  title="' + App.localize("PutStudentInactive") + '" class="btn btn-xs btn-default gray fa-grid-icon1" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.putStudentInactive(row.entity)"><i class="fa fa-user-times" style="color:red;"></i></button>' +
                        //'    <button  title="' + App.localize("Delete") + '"ng-if="grid.appScope.permissions.delete"  class="btn btn-xs btn-default gray fa-grid-icon1 red" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.delete(row.entity)"><i class="fa fa-close"></i></button>' +
                        '</div>'
                    },
                    {
                        name: App.localize('StudentFirstName'),
                        field: 'firstName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('StudentLastName'),
                        field: 'lastName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('StudentDateOfBirth'),
                        field: 'dateOfBirth',
                        minWidth: 125,
                        cellFilter: 'date:"dd-MM-yyyy\"'
                    },

                    {
                        name: App.localize('StudentGender'),
                        field: 'genders_Name',
                        minWidth: 125
                    },

                    {
                        name: App.localize('Course'),
                        field: 'enrollmentStudents[0].courseEnrollmentStudents[0].courses.name',
                        minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getStudents(showTheLastPage) {
                studentService.getAllStudentForEnrollment($scope.enrollmentId,1).then(function (result) {
                    vm.students = result.data.items;

                    $scope.gridOptions.data = vm.students;

                     $scope.pagination.assignTotalRecords(result.data.totalCount);
                    // if (showTheLastPage)
                    //   $scope.pagination.last();
                });
            }

            vm.openStudentCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/previouStudents/createModal.cshtml',
                    controller: 'app.views.previousStudents.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getStudents(true);
                });
            };

            vm.passStudentToCurrentPeriod = function (student) {
                abp.message.confirm(
                    App.localize('AreYouSureWantPassToCurrent') + " '" + student.firstName + " " + student.lastName + "'?",
                    function (result) {
                        if (result) {
                            studentService.passStudentToCurrentPeriod(student.enrollmentStudents[0].id)
                                .then(function () {
                                    abp.notify.info(App.localize('CloseSuccessfully'));
                                    $uibModalInstance.close();
                                });

                        }
                    });
            };

            vm.putStudentInactive = function (student) {
                abp.message.confirm(
                    App.localize('AreYouSurePutStudentInactive') + " '" + student.firstName + " " + student.lastName + "'?",
                    function (result) {
                        if (result) {
                            studentService.putStudentInactive(student.enrollmentStudents[0].id)
                                .then(function () {
                                    abp.notify.info(App.localize('InactiveSuccessfully'));
                                    $uibModalInstance.close();
                                });

                        }
                    });
            };



            vm.evaluations = function (student) {
                var config = {
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                };

                var objectReport = {};
                var reportsFilters = [];

                objectReport.reportCode = "67";

                objectReport.reportExport = "PDF";
                reportsFilters.push({ name: "EnrollmentStudentId", value: student.enrollmentStudents[0].id });

                objectReport.reportsFilters = reportsFilters;
                report.print(objectReport);
                abp.message.success("");
                swal.close();


            };




            vm.openStudentEditModal = function (student) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/previousStudents/editModal.cshtml',
                    controller: 'app.views.previousStudents.editModal as vm',
                    backdrop: 'static',
                    size:'lg',
                    resolve: {
                        id: function () {
                            return student.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getStudents(false);
                });
            };

            vm.delete = function (student) {
                abp.message.confirm(
                    "Delete student '" + student.firstName + " " + student.lastName + "'?",
                    function (result) {
                        if (result) {
                            studentService.delete({ id: student.id })
                                .then(function (result) {
                                    getStudents(false);
                                    abp.notify.info("Deleted student: " + student.firtName + " " + student.lastName);

                                });
                        }
                    });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.refresh = function () {
                getStudents(false);
            };

            getStudents(false);
        }
    ]);
})();

(function () {
    //Service Prefix
    var sp = 'abp.services.app.';
    angular.module('MetronicApp').controller('app.views.previousStudents.editModal', [
        '$scope', '$uibModalInstance', sp + 'student', 'id', sp + 'gender', sp + 'blood', sp + 'relationship', sp + 'allergy', sp + 'illness', sp + 'course', sp + 'level',
        function ($scope, $uibModalInstance, studentService, id, genderService, bloodService, relationshipService, allergyService, illnessService, courseService, levelService) {
            var vm = this;
            vm.saving = false;

            vm.student = {
                isActive: true
            };
            var init = function () {
                studentService.get({ id: id })
                    .then(function (result) {
                        vm.student = result.data;

                        var alergies = [];
                        for (var i = 0; i < vm.student.studentAllergies.length; i++) {
                            var al = vm.student.studentAllergies[i];
                            alergies.push(al.allergies)
                        }
                        vm.student.studentAllergies = alergies;
                        vm.student.dateOfBirthTemp = convertDateFormat_YMD_to_DMY(vm.student.dateOfBirth,4);


                        var illnesses = [];
                        for (var i = 0; i < vm.student.studentIllnesses.length; i++) {
                            var il = vm.student.studentIllnesses[i];
                            illnesses.push(il.illnesses)
                        }
                        vm.student.studentIllnesses = illnesses;

                        if (vm.student.enrollmentStudents.length > 0) {
                            vm.student.relationshipId = vm.student.enrollmentStudents[0].relationshipId;

                            if (vm.student.enrollmentStudents[0].courseEnrollmentStudents.length > 0) {
                                vm.student.courseId = vm.student.enrollmentStudents[0].courseEnrollmentStudents[0].courseId;
                            }
                        }

                        vm.student.dateOfBirth = getJustDate(vm.student.dateOfBirth);

                        vm.getGenders();
                        vm.getBloods();
                        vm.getAllergies();
                        vm.getIllnesses();
                        vm.getLevels();
                        vm.getCourses();
                        getCoursesByLevel(vm.student.levelId);
                        vm.getRelationships();
                        App.initAjax();
                        setTimeout(function () { $("#studentFirstName").focus(); }, 100);
                        $('.datepickerme').datepicker({
                            format: "yyyy/mm/dd",
                            language: "es"
                        });
                    });
            }


            $scope.$watch("vm.student.dateOfBirthTemp", function (newValue, oldValue) {
                if (newValue != undefined && newValue != oldValue) {
                    vm.student.dateOfBirth = convertDateFormat_DMY_to_YMD(newValue, 4);
                }
            });

            vm.save = function () {
                if (vm.saving === true)
                    return;
                
                if (!validateForm()) {
                    vm.saving = false;
                    return;
                }
                vm.saving = true;
                try {
                    studentService.update(vm.student)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            $scope.$watch("vm.student.levelId", function (newValue, oldValue) {
                if (newValue != undefined && newValue != null && newValue != oldValue) {
                    getCoursesByLevel(newValue);
                }
            });

            function getCoursesByLevel(levelId)
            {
                if (levelId == undefined || levelId === null) {
                    vm.courses = [];
                    return;
                }
                courseService.getAllCoursesByLevelsForCombo(levelId).then(function (result) {
                    vm.courses = result.data;
                });
            }


            vm.genders = [];
            vm.getGenders = function () {
                genderService.getAllGendersForCombo().then(function (result) {
                    vm.genders = result.data;
                    App.initAjax();
                });
            }

            vm.bloods = [];
            vm.getBloods = function () {
                bloodService.getAllBloodsForCombo().then(function (result) {
                    vm.bloods = result.data;
                    App.initAjax();
                });
            }

            vm.illnesses = [];
            vm.getIllnesses = function () {
                illnessService.getAllIllnessesForCombo().then(function (result) {
                    vm.illnesses = result.data;
                    //App.initAjax();
                });
            }

            vm.allergies = [];
            vm.getAllergies = function () {
                allergyService.getAllAllergiesForCombo().then(function (result) {
                    vm.allergies = result.data;
                    // App.initAjax();
                });
            }

            vm.levels = [];
            vm.getLevels = function () {
                levelService.getAllLevelsForCombo().then(function (result) {
                    vm.levels = result.data;
                    App.initAjax();
                });
            }

            vm.relationships = [];
            vm.getRelationships = function () {
                relationshipService.getAllRelationshipsForCombo().then(function (result) {
                    vm.relationships = result.data;
                    App.initAjax();
                });
            }

            vm.courses = [];
            vm.getCourses = function () {
                courseService.getAllCoursesForCombo().then(function (result) {
                    vm.courses = result.data;
                    App.initAjax();
                });
            }

            function validateForm() {
                var valid = false;
                try {
                    if (getObjectOrString(vm.student.emailAddress).length > 0 && !isValidEmail(vm.student.emailAddress)) {
                        abp.message.error(App.localize("EmailIsInvalid"));
                    }
                    else if (!validatePersonalId(vm.student.fatherPersonalId, true)) {
                        abp.message.error(App.localize("FathersPersonalIdIsInvalid"));
                    }
                    else if (!validatePersonalId(vm.student.motherPersonalId, true)) {
                        abp.message.error(App.localize("MothersPersonalIdIsInvalid"));
                    }
                    else if (getObjectOrString(vm.student.fatherPersonalId).length > 0
                        && getObjectOrString(vm.student.fatherName).length === 0) {
                        abp.message.error(App.localize("YouTypedTheFatherPersonalIdYouMustTypetheFatherName"));
                    }
                    else if (getObjectOrString(vm.student.motherPersonalId).length > 0
                        && getObjectOrString(vm.student.motherName).length === 0) {
                        abp.message.error(App.localize("YouTypedTheMotherPersonalIdYouMustTypetheMotherName"));
                    }
                    else if (vm.student.dateOfBirth == null) {
                        abp.message.error(App.localize("TheDateOfBirthIsInvalid"));
                    }
                    else if (!validateDateDMY(vm.student.dateOfBirthTemp)) {
                        abp.message.error(App.localize("TheDateOfBirthIsInvalid"));
                    }
                    else valid = true;

                } catch (e) {
                    console.log(e);
                    abp.message.error(JSON.stringify(e));
                }
                return valid;
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

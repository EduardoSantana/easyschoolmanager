(function () {
    angular.module('MetronicApp').controller('app.views.documentTypes.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.documentType', 'settings',
        function ($scope, $timeout, $uibModal, documentTypeService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getDocumentTypes(false);
            }
            vm.documentTypes = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openDocumentTypeEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                    {
                        name: App.localize('DocumentTypeName'),
                        field: 'name',
                        minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getDocumentTypes(showTheLastPage) {
                documentTypeService.getAllDocumentTypes($scope.pagination).then(function (result) {
                    vm.documentTypes = result.data.items;

                    $scope.gridOptions.data = vm.documentTypes;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openDocumentTypeCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/documentTypes/createModal.cshtml',
                    controller: 'app.views.documentTypes.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getDocumentTypes(true);
                });
            };

            vm.openDocumentTypeEditModal = function (documentType) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/documentTypes/editModal.cshtml',
                    controller: 'app.views.documentTypes.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return documentType.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getDocumentTypes(false);
                });
            };

            vm.delete = function (documentType) {
                abp.message.confirm(
                    "Delete documentType '" + documentType.name + "'?",
                    function (result) {
                        if (result) {
                            documentTypeService.delete({ id: documentType.id })
                                .then(function (result) {
                                    getDocumentTypes(false);
                                    abp.notify.info("Deleted documentType: " + documentType.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getDocumentTypes(false);
            };

            getDocumentTypes(false);
        }
    ]);
})();

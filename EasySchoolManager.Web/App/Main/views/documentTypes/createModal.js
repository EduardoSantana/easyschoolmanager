(function () {
    angular.module('MetronicApp').controller('app.views.documentTypes.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.documentType', 
        function ($scope, $uibModalInstance, documentTypeService ) {
            var vm = this;
            vm.saving = false;

            vm.documentType = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     documentTypeService.create(vm.documentType)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#documentTypeName").focus(); }, 100);
        }
    ]);
})();

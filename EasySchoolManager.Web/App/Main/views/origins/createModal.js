(function () {
    angular.module('MetronicApp').controller('app.views.origins.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.origin', 
        function ($scope, $uibModalInstance, originService ) {
            var vm = this;
            vm.saving = false;

            vm.origin = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     originService.create(vm.origin)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#originName").focus(); }, 100);
        }
    ]);
})();

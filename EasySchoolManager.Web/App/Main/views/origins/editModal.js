(function () {
    angular.module('MetronicApp').controller('app.views.origins.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.origin', 'id', 
        function ($scope, $uibModalInstance, originService, id ) {
            var vm = this;
			vm.saving = false;

            vm.origin = {
                isActive: true
            };
            var init = function () {
                originService.get({ id: id })
                    .then(function (result) {
                        vm.origin = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#originName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						originService.update(vm.origin)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

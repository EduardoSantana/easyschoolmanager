(function () {
    angular.module('MetronicApp').controller('app.views.religions.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.religion', 'id', 
        function ($scope, $uibModalInstance, religionService, id ) {
            var vm = this;
			vm.saving = false;

            vm.religion = {
                isActive: true
            };
            var init = function () {
                religionService.get({ id: id })
                    .then(function (result) {
                        vm.religion = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#religionShortName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						religionService.update(vm.religion)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

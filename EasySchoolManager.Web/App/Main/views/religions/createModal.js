(function () {
    angular.module('MetronicApp').controller('app.views.religions.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.religion', 
        function ($scope, $uibModalInstance, religionService ) {
            var vm = this;
            vm.saving = false;

            vm.religion = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     religionService.create(vm.religion)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#religionShortName").focus(); }, 100);
        }
    ]);
})();

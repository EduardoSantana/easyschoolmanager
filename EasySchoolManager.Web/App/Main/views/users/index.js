(function () {
    angular.module('MetronicApp').controller('app.views.users.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.user', 'abp.services.app.tenant', 'settings', 'appSession',
        function ($scope, $timeout, $uibModal, userService, tenantService, settings, appSession) {
            var vm = this;
            vm.textFilter = "";

            vm.appSession = appSession;

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getUsers(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }


            vm.users = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openUserEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('UserName'),
                        field: 'userName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('FullName'),
                        field: 'fullName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('tenant_Name'),
                        field: 'tenant_Name',
                        minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };

            if (appSession.tenant != null) {
                $scope.gridOptions.columnDefs.splice(4, 1);
            }

            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            vm.getEditionId = function (record) {
                return parseInt(record.id);
            };
            vm.currentTenantId = null;
            vm.tenants = [];
            vm.getTenants = function () {
                tenantService.getAllTenantsForCombo().then(function (result) {
                    vm.tenants = result.data;
                    App.initAjax();
                });
            }

            function getUsers(showTheLastPage) {
                $scope.pagination.id = vm.currentTenantId;
                userService.getAllUserByTenant($scope.pagination).then(function (result) {
                    vm.users = result.data.items;

                    $scope.gridOptions.data = vm.users;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openUserCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/users/createModal.cshtml',
                    controller: 'app.views.users.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getUsers(true);
                });
            };

            vm.openUserEditModal = function (user) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/users/editModal.cshtml',
                    controller: 'app.views.users.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return user.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getUsers(false);
                });
            };

            vm.delete = function (user) {
                abp.message.confirm(
                    "Delete user '" + user.name + "'?",
                    function (result) {
                        if (result) {
                            userService.delete({ id: user.id })
                                .then(function (result) {
                                    getUsers(false);
                                    abp.notify.info("Deleted user: " + user.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getUsers(false);
            };

            getUsers(false);

            if (appSession.tenant == null) {
                vm.getTenants();
            }
            
        }
    ]);
})();

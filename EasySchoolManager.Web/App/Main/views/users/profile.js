﻿(function () {
    angular.module('MetronicApp').controller('app.views.users.profile', [
        '$scope', '$rootScope', 'abp.services.app.user', 'appSession','settings', 
        function ($scope, $rootScope, userService, appSession, settings) {
            var vm = this;
            var id = appSession.user.id;
            vm.appSession = appSession;
            vm.defaultPicture = settings.settings.assetsPath + "/pages/media/users/avatar3.png";
            vm.user = {
                isActive: true
            };

            vm.roles = [];

            $scope.$watch("pc.name", function (newValue, oldValue) {
                vm.user.pictureChanged = newValue != oldValue;
            });

            
            var setAssignedRoles = function (user, roles) {
                vm.user.ProfileName = '';
                for (var i = 0; i < roles.length; i++) {
                    var role = roles[i];
                    role.isAssigned = $.inArray(role.name, user.roles) >= 0;
                    
                    if (role.isAssigned ) {
                        vm.user.ProfileName = vm.user.ProfileName + role.name + ', ';
                    }
                    
                }
                vm.user.ProfileName = vm.user.ProfileName.substring(0, vm.user.ProfileName.length - 2);
            }

            var init = function () {
                
                userService.getRoles()
                    .then(function (result) {
                        vm.roles = result.data.items;
                        userService.get({ id: id })
                            .then(function (result) {
                                vm.user = result.data;
                                setAssignedRoles(vm.user, vm.roles);
                                setTimeout(function () { $("#userUserName").focus(); App.initAjax(); vm.user.pictureChanged = false; }, 500);
                                
                            });
                    });

                // set default layout mode
                $rootScope.$settings.settings.layout.pageContentWhite = true;
                $rootScope.$settings.settings.layout.pageBodySolid = true;
                $rootScope.$settings.settings.layout.pageSidebarClosed = true;
   
            }

            vm.save = function () {
                var assingnedRoles = [];

                for (var i = 0; i < vm.roles.length; i++) {
                    var role = vm.roles[i];
                    if (!role.isAssigned) {
                        continue;
                    }

                    assingnedRoles.push(role.name);
                }

                vm.user.roleNames = assingnedRoles;

                userService.update(vm.user)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                     vm.user.pictureChanged = false; 
                    });
            };
            
            init();

        }
    ]);
})();

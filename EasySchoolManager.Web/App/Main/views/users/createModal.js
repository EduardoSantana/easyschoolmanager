(function () {
    angular.module('MetronicApp').controller('app.views.users.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.user', 'abp.services.app.tenant', 'appSession', 'abp.services.app.position', 
        function ($scope, $uibModalInstance, userService, tenantService, appSession,positionService) {
            var vm = this;
            vm.appSession = appSession;
            vm.user = {
                isActive: true
            };

            vm.roles = [];

            function getRoles() {
                userService.getRoles()
                    .then(function (result) {
                        vm.roles = result.data.items;
                    });
            }

            vm.save = function () {

                vm.saving = true;
                
                try {
                    var assingnedRoles = [];

                    for (var i = 0; i < vm.roles.length; i++) {
                        var role = vm.roles[i];
                        if (!role.isAssigned) {
                            continue;
                        }

                        assingnedRoles.push(role.name);
                    }

                    vm.user.roleNames = assingnedRoles;

                    userService.create(vm.user)
                        .then(function () {
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            vm.getEditionId = function (record) {
                return parseInt(record.id);
            };

            vm.tenants = [];
            vm.getTenants = function () {
                tenantService.getAllTenantsForCombo().then(function (result) {
                    vm.tenants = result.data;
                    App.initAjax();
                });
            }

            vm.positions = [];
            vm.getPositions = function () {
                positionService.getAllPositionsForCombo().then(function (result) {
                    vm.positions = result.data;
                    App.initAjax();
                });
            }


            //XXXInsertCallRelatedEntitiesXXX

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            getRoles();

            if (appSession.tenant == null) {
                vm.getTenants();
                vm.getPositions();
            }


            setTimeout(function () { $("#userUserName").focus(); }, 100);

        }
    ]);
})();

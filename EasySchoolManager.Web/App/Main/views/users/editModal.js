(function () {
    angular.module('MetronicApp').controller('app.views.users.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.user', 'id', 'abp.services.app.tenant', 'appSession', 'abp.services.app.position',
        function ($scope, $uibModalInstance, userService, id, tenantService, appSession,positionService) {
            var vm = this;
            vm.appSession = appSession;
            vm.user = {
                isActive: true
            };

            vm.roles = [];
            vm.tenants = [];

            var setAssignedRoles = function (user, roles) {
                for (var i = 0; i < roles.length; i++) {
                    var role = roles[i];
                    role.isAssigned = $.inArray(role.name, user.roles) >= 0;
                }
            }

            var init = function () {

                if (appSession.tenant == null) {

                    tenantService.getAllTenantsForCombo().then(function (result) {
                        vm.tenants = result.data;
                        vm.operatedTenans = $.extend({}, result.data);
                        userService.getRoles()
                            .then(function (result) {
                                vm.roles = result.data.items;
                                userService.get({ id: id })
                                    .then(function (result) {
                                        vm.user = result.data;
                                        setAssignedRoles(vm.user, vm.roles);

                                        var tenans = [];
                                        for (var i = 0; i < vm.user.userTenants.length; i++) {
                                            var al = vm.user.userTenants[i];
                                            if (al.tenant != null) {
                                                tenans.push(al.tenant);
                                            }
                                        }
                                        vm.user.tenants = tenans;

                                        setTimeout(function () { $("#userUserName").focus(); App.initAjax(); }, 500);
                                    });
                            });

                    });

                }
                else {
                    userService.getRoles()
                        .then(function (result) {
                            vm.roles = result.data.items;
                            userService.get({ id: id })
                                .then(function (result) {
                                    vm.user = result.data;
                                    setAssignedRoles(vm.user, vm.roles);
                                    setTimeout(function () { $("#userUserName").focus(); App.initAjax(); }, 500);
                                });
                        });
                }

                vm.getPositions();

            }

            vm.save = function () {
                var assingnedRoles = [];

                for (var i = 0; i < vm.roles.length; i++) {
                    var role = vm.roles[i];
                    if (!role.isAssigned) {
                        continue;
                    }

                    assingnedRoles.push(role.name);
                }

                vm.user.roleNames = assingnedRoles;
       
 //               vm.user.password = "AA7OZCk+qVj7eJcCC+2igsGg5pWJXbniW0f5VwkjUPaI+s/t6PgNrdRLO0Ybqz3Edw==";
                vm.user.password = vm.user.password;
                userService.update(vm.user)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });
            };


            vm.resetPassword = function () {
                var assingnedRoles = [];

                for (var i = 0; i < vm.roles.length; i++) {
                    var role = vm.roles[i];
                    if (!role.isAssigned) {
                        continue;
                    }

                    assingnedRoles.push(role.name);
                }

                vm.user.roleNames = assingnedRoles;


                if (vm.user.id != 1) {
                vm.user.password = "AA7OZCk+qVj7eJcCC+2igsGg5pWJXbniW0f5VwkjUPaI+s/t6PgNrdRLO0Ybqz3Edw==";
                            }
                    userService.update(vm.user)
                        .then(function () {
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        });
            };


            vm.getEditionId = function (record) {
                return parseInt(record.id);
            };

            //XXXInsertCallRelatedEntitiesXXX

            vm.positions = [];
            vm.getPositions = function () {
                positionService.getAllPositionsForCombo().then(function (result) {
                    vm.positions = result.data;
                    App.initAjax();
                });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();

        }
    ]);
})();

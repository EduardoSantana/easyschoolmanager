(function () {
    angular.module('MetronicApp').controller('app.views.evaluationOrderHighs.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.evaluationOrderHigh', 'id', 'abp.services.app.evaluationPositionHigh',
        function ($scope, $uibModalInstance, evaluationOrderHighService, id , evaluationPositionHighService) {
            var vm = this;
			vm.saving = false;

            vm.evaluationOrderHigh = {
                isActive: true
            };
            var init = function () {
                evaluationOrderHighService.get({ id: id })
                    .then(function (result) {
                        vm.evaluationOrderHigh = result.data;
						                vm.getEvaluationPositionHighs();

						App.initAjax();
						setTimeout(function () { $("#evaluationOrderHighEvaluationPositionHighId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						evaluationOrderHighService.update(vm.evaluationOrderHigh)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.evaluationPositionHighs = [];
            vm.getEvaluationPositionHighs	 = function()
            {
                evaluationPositionHighService.getAllEvaluationPositionHighsForCombo().then(function (result) {
                    vm.evaluationPositionHighs = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.evaluationOrderHighs.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.evaluationOrderHigh', 'abp.services.app.evaluationPositionHigh',
        function ($scope, $uibModalInstance, evaluationOrderHighService , evaluationPositionHighService) {
            var vm = this;
            vm.saving = false;

            vm.evaluationOrderHigh = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     evaluationOrderHighService.create(vm.evaluationOrderHigh)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.evaluationPositionHighs = [];
            vm.getEvaluationPositionHighs	 = function()
            {
                evaluationPositionHighService.getAllEvaluationPositionHighsForCombo().then(function (result) {
                    vm.evaluationPositionHighs = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			                vm.getEvaluationPositionHighs();

		    App.initAjax();
			setTimeout(function () { $("#evaluationOrderHighEvaluationPositionHighId").focus(); }, 100);
        }
    ]);
})();

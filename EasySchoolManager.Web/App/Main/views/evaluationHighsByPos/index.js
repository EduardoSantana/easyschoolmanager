(function () {
	angular.module('MetronicApp').controller('app.views.evaluationHighsByPos.index', [
		'$scope', '$timeout', '$uibModal', 'abp.services.app.evaluationHigh','settings',
		function ($scope, $timeout, $uibModal, evaluationHighService, settings) {
			var vm = this;
			vm.textFilter = "";

			vm.openEvaluationHighCreationModal = function () {
				var modalInstance = $uibModal.open({
					templateUrl: settings.settings.appPathNotHTTP + '/views/evaluationHighsByPos/createModal.cshtml',
					controller: 'app.views.evaluationHighsByPos.createModal as vm',
					size: "lg",
					backdrop: 'static'
				});

				modalInstance.rendered.then(function () {
				   App.initAjax();
				});

				modalInstance.result.then(function () {
					getEvaluationHighs(false);
				});
			};
		
		}
	]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.tenants.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.tenant', 'abp.services.app.city', 'abp.services.app.period', 'abp.services.app.province', 'abp.services.app.district',
        function ($scope, $uibModalInstance, tenantService, cityService, periodService, provinceService, districtService) {
            var vm = this;

            vm.tenant = {
                isActive: true
            };

            vm.cities = [];
            vm.periods = [];
            vm.provinces = [];
            vm.districts = [];

            $scope.$watch("vm.tenant.provinceId", function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    var paramx = {
                        provinceId: newValue
                    };
                    cityService.getAllCitiesForComboByProvinceId(paramx).then(function (result) {
                        vm.cities = result.data;
                        App.initAjax();
                    })
                }
            });

            vm.save = function () {
                vm.saving = true;
                try {
                    tenantService.create(vm.tenant)
                        .then(function () {
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            vm.getProvinces = function () {
                provinceService.getAllProvincesForCombo().then(function (result) {
                    vm.provinces = result.data;
                    periodService.getAllPeriodsForCombo().then(function (result) {
                        vm.periods = result.data;
                        App.initAjax();
                    });
                });
            }
            

            vm.getDistricts = function () {
                districtService.getAllDistrictsForCombo().then(function (result) {
                    vm.districts = result.data;
                    districtService.getAllDistrictsForCombo().then(function (result) {
                        vm.districts = result.data;
                        App.initAjax();
                    });
                });
            }

            //XXXInsertCallRelatedEntitiesXXX

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getProvinces();
            vm.getDistricts();
        }
    ]);
})();

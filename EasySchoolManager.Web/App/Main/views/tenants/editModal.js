(function () {
    angular.module('MetronicApp').controller('app.views.tenants.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.tenant', 'id', 'abp.services.app.city', 'abp.services.app.period', 'abp.services.app.province', 'abp.services.app.district', 'abp.services.app.concept', 'abp.services.app.printerType', 'abp.services.app.teacher',
        function ($scope, $uibModalInstance, tenantService, id, cityService, periodService, provinceService, districtService, conceptService, printerTypeService, teachersService) {
            var vm = this;
            vm.teacherTenantsId = null;
            vm.teachers = [];

            vm.tenant = {
                isActive: true
            };
            vm.tenant.teacherTenants = [];
            vm.cities = [];
            vm.periods = [];

            var init = function () {
                periodService.getAllPeriodsForCombo().then(function (result) {
                    vm.periods = result.data;
                    cityService.getAllCitiesForCombo().then(function (result) {
                        vm.cities = result.data;
                        tenantService.get({ id: id })
                            .then(function (result) {
                                vm.tenant = result.data;
                                var tempTeachers = [];
                                for (var i = 0; i < vm.tenant.teacherTenants.length; i++) {
                                    var t = vm.tenant.teacherTenants[i];
                                    tempTeachers.push({
                                        id: t.teacherId,
                                        name: t.teachers_Name
                                    });
                                }
                                vm.tenant.teacherTenants = tempTeachers;
                                var tempConcepts = [];
                                for (var i = 0; i < vm.tenant.conceptTenantRegistrations.length; i++) {
                                    var t = vm.tenant.conceptTenantRegistrations[i];
                                    tempConcepts.push({
                                        id: t.conceptId,
                                        shortDescription: t.concepts_ShortDescription
                                    });
                                }
                                vm.tenant.conceptTenantRegistrations = tempConcepts;

                                vm.getProvinces();
                                vm.getDistricts();
                                vm.getConcepts();
                                vm.getPrinterTypes();
                                vm.getTeachers();
                                vm.tenant.provinceId = vm.tenant.cities_ProvinceId;
                                App.initAjax();
                                setTimeout(function () { $("#tenantTenancyName").focus(); }, 100);
                            });
                    });
                });

            }

            vm.cierreEscolar = function () {
                
                abp.message.confirm(
                    App.localize('AreYouSureWantToClosePeriod') + "?",
                    function (result) {
                        if (result) {
                            tenantService.closePeriod({ tenantId: id })
                                .then(function () {
                                    abp.message.success(App.localize('CloseSuccessfullyMessage'), App.localize('CloseSuccessfullyTitle'));
                                    $uibModalInstance.close();
                                });
                        }
                    });
            }

            vm.save = function () {

                var tempTeachers = [];

                for (var i = 0; i < vm.tenant.teacherTenants.length; i++) {
                    var t = vm.tenant.teacherTenants[i];
                    tempTeachers.push({
                        teacherId: t.id,
                        tenantId: id
                    });
                }

                vm.tenant.teacherTenants = tempTeachers;


                var tempConceptTenantRegistrations = [];

                for (var i = 0; i < vm.tenant.conceptTenantRegistrations.length; i++) {
                    var t = vm.tenant.conceptTenantRegistrations[i];
                    tempConceptTenantRegistrations.push({
                        conceptId: t.id,
                        tenantId: id
                    });
                }

                vm.tenant.conceptTenantRegistrations = tempConceptTenantRegistrations;


                tenantService.update(vm.tenant)
                    .then(function () {
                        abp.notify.success(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });
            };

            $scope.$watch("vm.tenant.provinceId", function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    var paramx = {
                        provinceId: newValue
                    };
                    cityService.getAllCitiesForComboByProvinceId(paramx).then(function (result) {
                        vm.cities = result.data;
                        App.initAjax();
                    })
                }
            });

            vm.getCities = function () {
                reportService.getAllCitiesForCombo().then(function (result) {
                    vm.cities = result.data;
                    App.initAjax();
                    setTimeout(function () { $("#tenantTenancyName").focus(); }, 100);
                });
            }

            vm.provinces = [];
            vm.getProvinces = function () {
                provinceService.getAllProvincesForCombo().then(function (result) {
                    vm.provinces = result.data;
                });
            }

            vm.districts = [];
            vm.getDistricts = function () {
                districtService.getAllDistrictsForCombo().then(function (result) {
                    vm.districts = result.data;
                });
            }

            vm.concepts = [];
            vm.getConcepts = function () {
                conceptService.getAllConceptsForReceipts1Combo().then(function (result) {
                    vm.concepts = result.data;
                });
            }

            vm.printerTypes = [];
            vm.getPrinterTypes = function () {
                printerTypeService.getAllPrinterTypesForCombo().then(function (result) {
                    vm.printerTypes = result.data;
                });
            }

            vm.teachers = [];
            vm.getTeachers = function () {
                teachersService.getAllTeachersForCombo().then(function (result) {
                    vm.teachers = result.data;
                });
            }

            //XXXInsertCallRelatedEntitiesXXX

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

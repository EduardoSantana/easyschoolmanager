﻿(function () {
    // Handle global LINK click
    angular.module('MetronicApp').directive('dropdownMenuHover', function () {
        return {
            link: function (scope, elem) {
                elem.dropdownHover();
            }
        };
    });
})();
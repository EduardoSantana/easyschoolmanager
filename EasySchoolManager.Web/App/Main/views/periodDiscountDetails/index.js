(function () {
    angular.module('MetronicApp').controller('app.views.periodDiscountDetails.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.periodDiscountDetail','settings',
        function ($scope, $timeout, $uibModal, periodDiscountDetailService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getPeriodDiscountDetails(false);
            }
            vm.periodDiscountDetails = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openPeriodDiscountDetailEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        '      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('PeriodDiscountDetailPeriodDiscountId'),
                    field: 'periodDiscountId',
                    minWidth: 125
                    },
                    {
                    name: App.localize('PeriodDiscountDetailDiscountPercent'),
                    field: 'discountPercent',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getPeriodDiscountDetails(showTheLastPage) {
                periodDiscountDetailService.getAllPeriodDiscountDetails($scope.pagination).then(function (result) {
                    vm.periodDiscountDetails = result.data.items;

                    $scope.gridOptions.data = vm.periodDiscountDetails;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openPeriodDiscountDetailCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/periodDiscountDetails/createModal.cshtml',
                    controller: 'app.views.periodDiscountDetails.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getPeriodDiscountDetails(false);
                });
            };

            vm.openPeriodDiscountDetailEditModal = function (periodDiscountDetail) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/periodDiscountDetails/editModal.cshtml',
                    controller: 'app.views.periodDiscountDetails.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return periodDiscountDetail.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getPeriodDiscountDetails(false);
                });
            };

            vm.delete = function (periodDiscountDetail) {
                abp.message.confirm(
                    "Delete periodDiscountDetail '" + periodDiscountDetail.name + "'?",
                    function (result) {
                        if (result) {
                            periodDiscountDetailService.delete({ id: periodDiscountDetail.id })
                                .then(function (result) {
                                    getPeriodDiscountDetails(false);
                                    abp.notify.info("Deleted periodDiscountDetail: " + periodDiscountDetail.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getPeriodDiscountDetails(false);
            };

            getPeriodDiscountDetails(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.periodDiscountDetails.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.periodDiscountDetail', 'id', 
        function ($scope, $uibModalInstance, periodDiscountDetailService, id ) {
            var vm = this;
			vm.saving = false;

            vm.periodDiscountDetail = {
                isActive: true
            };
            var init = function () {
                periodDiscountDetailService.get({ id: id })
                    .then(function (result) {
                        vm.periodDiscountDetail = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#periodDiscountDetailPeriodDiscountId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						periodDiscountDetailService.update(vm.periodDiscountDetail)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.periodDiscountDetails.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.periodDiscountDetail', 
        function ($scope, $uibModalInstance, periodDiscountDetailService ) {
            var vm = this;
            vm.saving = false;

            vm.periodDiscountDetail = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     periodDiscountDetailService.create(vm.periodDiscountDetail)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#periodDiscountDetailPeriodDiscountId").focus(); }, 100);
        }
    ]);
})();

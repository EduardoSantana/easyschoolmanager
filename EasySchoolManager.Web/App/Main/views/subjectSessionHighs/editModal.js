(function () {
    angular.module('MetronicApp').controller('app.views.subjectSessionHighs.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.subjectSessionHigh', 'id', 'abp.services.app.subjectHigh', 'abp.services.app.teacherTenant', 'abp.services.app.courseSession', 'abp.services.app.period',
        function ($scope, $uibModalInstance, subjectSessionHighService, id, subjectHighService, teacherTenantService, courseSessionService, periodService) {
            var vm = this;
            vm.saving = false;

            vm.subjectSessionHigh = {
                isActive: true
            };
            var init = function () {
                subjectSessionHighService.get({ id: id })
                    .then(function (result) {
                        vm.subjectSessionHigh = result.data;
                       
                        vm.getTeacherTenants();
                        vm.getCourseSessions();
                        vm.getPeriods();

                        App.initAjax();
                        setTimeout(function () { $("#subjectSessionHighSubjectHighId").focus(); }, 100);
                    });
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    subjectSessionHighService.update(vm.subjectSessionHigh)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };
            vm.loading = true;
            $scope.$watch("vm.subjectSessionHigh.courseSessionId", function (newValue, oldValue) {
                if (newValue != null && newValue > 0 && !vm.loading) {
                    var courseId = 0;
                    for (var i = 0; i < vm.courseSessions.length; i++) {
                        if (vm.courseSessions[i].id == newValue) {
                            courseId = vm.courseSessions[i].courseId;
                            break;
                        }
                    }
                    vm.getSubjectHighs(courseId);
                }
            });

            //XXXInsertCallRelatedEntitiesXXX

            vm.subjectHighs = [];
            vm.getSubjectHighs = function (courseId) {
                subjectHighService.getAllSubjectHighsByCourseForCombo(courseId).then(function (result) {
                    vm.subjectSessionHigh.subjectHighId = null;
                    vm.subjectHighs = result.data;
                    App.initAjax();
                });
            };

            vm.teacherTenants = [];
            vm.getTeacherTenants = function () {
                teacherTenantService.getAllTeacherTenantsForCombo().then(function (result) {
                    vm.teacherTenants = result.data;
                    App.initAjax();
                });
            };

            vm.courseSessions = [];
            vm.getCourseSessions = function () {
                courseSessionService.getAllCourseSessionsHighsForCombo().then(function (result) {
                    vm.courseSessions = result.data;
                    var courseId = 0;
                    for (var i = 0; i < vm.courseSessions.length; i++) {
                        if (vm.courseSessions[i].id == vm.subjectSessionHigh.courseSessionId) {
                            courseId = vm.courseSessions[i].courseId;
                            break;
                        }
                    }

                    subjectHighService.getAllSubjectHighsByCourseForCombo(courseId).then(function (result2) {
                        vm.subjectHighs = result2.data;
                        vm.loading = false;
                    });

                    App.initAjax();
                });
            };

            vm.periods = [];
            vm.getPeriods = function () {
                periodService.getAllPeriodsForCombo().then(function (result) {
                    vm.periods = result.data;
                    App.initAjax();
                });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

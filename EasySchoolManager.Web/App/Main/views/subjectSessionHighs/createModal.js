(function () {
    angular.module('MetronicApp').controller('app.views.subjectSessionHighs.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.subjectSessionHigh', 'abp.services.app.subjectHigh', 'abp.services.app.teacherTenant', 'abp.services.app.courseSession', 'abp.services.app.period',
        function ($scope, $uibModalInstance, subjectSessionHighService, subjectHighService, teacherTenantService, courseSessionService, periodService) {
            var vm = this;
            vm.saving = false;

            vm.subjectSessionHigh = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    subjectSessionHighService.create(vm.subjectSessionHigh)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            $scope.$watch("vm.subjectSessionHigh.courseSessionId", function (newValue, oldValue) {
                if (newValue != null) {
                    var courseId = 0;
                    for (var i = 0; i < vm.courseSessions.length; i++) {
                        if (vm.courseSessions[i].id == newValue) {
                            courseId = vm.courseSessions[i].courseId;
                        }
                    }
                    vm.getSubjectHighs(courseId);
                }
            });

            //XXXInsertCallRelatedEntitiesXXX

            vm.subjectHighs = [];
            vm.getSubjectHighs = function (courseId) {
                subjectHighService.getAllSubjectHighsByCourseForCombo(courseId).then(function (result) {
                    vm.subjectSessionHigh.subjectHighId = null;
                    vm.subjectHighs = result.data;
                    App.initAjax();
                });
            };

            vm.teacherTenants = [];
            vm.getTeacherTenants = function () {
                teacherTenantService.getAllTeacherTenantsForCombo().then(function (result) {
                    vm.teacherTenants = result.data;
                    App.initAjax();
                });
            };

            vm.courseSessions = [];
            vm.getCourseSessions = function () {
                courseSessionService.getAllCourseSessionsHighsForCombo().then(function (result) {
                    vm.courseSessions = result.data;
                    App.initAjax();
                });
            };

            vm.periods = [];
            vm.getPeriods = function () {
                periodService.getAllPeriodsForCombo().then(function (result) {
                    vm.periods = result.data;
                    App.initAjax();
                });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getTeacherTenants();
            vm.getCourseSessions();
            vm.getPeriods();

            App.initAjax();
            setTimeout(function () { $("#subjectSessionHighSubjectHighId").focus(); }, 100);
        }
    ]);
})();

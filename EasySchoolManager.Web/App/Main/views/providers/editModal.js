(function () {
    angular.module('MetronicApp').controller('app.views.providers.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.provider', 'id', 
        function ($scope, $uibModalInstance, providerService, id ) {
            var vm = this;
			vm.saving = false;

            vm.provider = {
                isActive: true
            };
            var init = function () {
                providerService.get({ id: id })
                    .then(function (result) {
                        vm.provider = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#providerName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						providerService.update(vm.provider)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

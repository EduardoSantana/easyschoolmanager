(function () {
    angular.module('MetronicApp').controller('app.views.providers.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.provider','settings',
        function ($scope, $timeout, $uibModal, providerService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getProviders(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.providers = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openProviderEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                    {
                    name: App.localize('ProviderName'),
                    field: 'name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ProviderReference'),
                    field: 'reference',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ProviderRnc'),
                    field: 'rnc',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ProviderPhone1'),
                    field: 'phone1',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ProviderPhone2'),
                    field: 'phone2',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ProviderAddress'),
                    field: 'address',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getProviders(showTheLastPage) {
                providerService.getAllProviders($scope.pagination).then(function (result) {
                    vm.providers = result.data.items;

                    $scope.gridOptions.data = vm.providers;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openProviderCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/providers/createModal.cshtml',
                    controller: 'app.views.providers.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getProviders(false);
                });
            };

            vm.openProviderEditModal = function (provider) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/providers/editModal.cshtml',
                    controller: 'app.views.providers.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return provider.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getProviders(false);
                });
            };

            vm.delete = function (provider) {
                abp.message.confirm(
                    "Delete provider '" + provider.name + "'?",
                    function (result) {
                        if (result) {
                            providerService.delete({ id: provider.id })
                                .then(function (result) {
                                    getProviders(false);
                                    abp.notify.info("Deleted provider: " + provider.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getProviders(false);
            };

            getProviders(false);
        }
    ]);
})();

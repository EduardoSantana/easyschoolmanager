(function () {
    angular.module('MetronicApp').controller('app.views.providers.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.provider', 
        function ($scope, $uibModalInstance, providerService ) {
            var vm = this;
            vm.saving = false;

            vm.provider = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     providerService.create(vm.provider)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#providerName").focus(); }, 100);
        }
    ]);
})();

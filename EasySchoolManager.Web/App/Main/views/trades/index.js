(function () {
    angular.module('MetronicApp').controller('app.views.trades.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.trade','settings',
        function ($scope, $timeout, $uibModal, tradeService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getTrades(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.trades = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openTradeEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('TradeComment'),
                    field: 'comment',
                    minWidth: 125
                    },
                    {
                    name: App.localize('TradeCourrierPerson'),
                    field: 'courrierPersons_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('TradeDate'),
                    field: 'date',
                    minWidth: 125,
                    cellFilter: 'date:"dd-MM-yyyy\"'
                    },
                    {
                    name: App.localize('TradeTenantReceiveId'),
                    field: 'tenantReceive_Name',
                    minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getTrades(showTheLastPage) {
                tradeService.getAllTrades($scope.pagination).then(function (result) {
                    vm.trades = result.data.items;

                    $scope.gridOptions.data = vm.trades;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openTradeCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/trades/createModal.cshtml',
                    controller: 'app.views.trades.createModal as vm',
                    size: 'lg',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getTrades(false);
                });
            };

            vm.openTradeEditModal = function (trade) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/trades/editModal.cshtml',
                    controller: 'app.views.trades.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return trade.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getTrades(false);
                });
            };

            vm.delete = function (trade) {
                abp.message.confirm(
                    "Delete trade '" + trade.name + "'?",
                    function (result) {
                        if (result) {
                            tradeService.delete({ id: trade.id })
                                .then(function (result) {
                                    getTrades(false);
                                    abp.notify.info("Deleted trade: " + trade.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getTrades(false);
            };

            getTrades(false);
        }
    ]);
})();

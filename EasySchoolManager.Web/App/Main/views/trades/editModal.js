(function () {
    angular.module('MetronicApp').controller('app.views.trades.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.trade', 'id', 'abp.services.app.courrierPerson',
        function ($scope, $uibModalInstance, tradeService, id , courrierPersonService) {
            var vm = this;
			vm.saving = false;

            vm.trade = {
                isActive: true
            };
            var init = function () {
                tradeService.get({ id: id })
                    .then(function (result) {
                        vm.trade = result.data;
						                vm.getCourrierPersons();

						App.initAjax();
						setTimeout(function () { $("#tradeComment").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						tradeService.update(vm.trade)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.courrierPersons = [];
            vm.getCourrierPersons	 = function()
            {
                courrierPersonService.getAllCourrierPersonsForCombo().then(function (result) {
                    vm.courrierPersons = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.trades.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.trade', 'abp.services.app.courrierPerson', 'abp.services.app.article', 'abp.services.app.tenant',
        function ($scope, $uibModalInstance, tradeService , courrierPersonService,articleService,tenantService) {
            var vm = this;
            vm.saving = false;

            var todayDate = getDateYMD(new Date());

            vm.article = {};

            vm.articleIdIsFocused = false;

            vm.trade = {
                isActive: true,
                date: todayDate,
                dateSend: todayDate
            };

            vm.trade.dateTemp = convertDateFormat_YMD_to_DMY(todayDate, 4);
            vm.trade.date = todayDate;

            vm.trade.articles = [];

            $scope.getArticlesByName = function (userInputString) {
                $scope.pagination.maxResultCount = 10;
                $scope.pagination.textFilter = userInputString;
                return articleService.getAllArticles($scope.pagination);
            }

            $scope.returnedArticle = function (result) {
                if (!result) return;
                vm.article = result.originalObject;
                vm.article.quantity = 1;
            }

            $scope.$watch("vm.article.id", function (newValue, oldValue) {
                if (newValue == null) {
                    vm.article = {};
                }
                else {
                    if (vm.articleIdIsFocused == true)
                        vm.searArticleById();
                }
            });

            vm.searArticleById = function () {
                articleService.get({ id: vm.article.id }).then(function (result) {

                    if (result.data != null) {
                        var resultado = { originalObject: result.data };
                        $scope.returnedArticle(resultado);
                        $scope.$broadcast('angucomplete-alt:changeInput', 'ArticleAngu', vm.article.description);
                    }

                });
            }


            vm.removeArticle = function (article) {
                abp.message.confirm(App.localize("AreYouSureYouWantToRemoveTheArticle"), function (result) {
                    if (result == true) {
                        var index = vm.getIndex(article.id, vm.trade.articles);
                        vm.trade.articles.splice(index, 1);
                        calculateTotal();
                        refresArticles();
                        try {
                            $scope.$apply();
                        } catch (e) { }
                    }
                });
            }

            vm.getIndex = function (id, array) {
                var index = -1;

                for (var i = 0; i < array.length; i++) {
                    var item = array[i];
                    if (item.id == id) {
                        index = i;
                        break;
                    }
                }
                return index;
            }

            vm.getTempArticleFromLocalList = function (articleId) {
                var currentArticle = null;

                var articles = vm.trade.articles.filter(function (x) { return x.id == articleId });
                if (articles.length > 0)
                    currentArticle = articles[0];
                return currentArticle;
            }

            vm.appendArticle = function () {
                
                //Validamos si el articulo puede ser insertado o no en la lista de articulos
                if (!validateArticle())
                    return;

                vm.article.total = getDoubleOrCero(vm.article.price * vm.article.quantity);

                //de aqui
                var existingArticle = vm.getTempArticleFromLocalList(vm.article.id);
                if (existingArticle != null) {
                    existingArticle.quantity = parseInt(existingArticle.quantity) + parseInt(vm.article.quantity);
                    existingArticle.total = getDoubleOrCero(existingArticle.price * existingArticle.quantity);
                } else {
                  vm.trade.articles.push(vm.article);  
                }   
                //hasta aqui

                vm.article = {};

                $scope.$broadcast('angucomplete-alt:clearInput', 'ArticleAngu');
                refresArticles();
                calculateTotal();
                $("#tradeArticleId").focus();
            }

            function validateArticle() {
                var validated = false;
                if (getDoubleOrCero(vm.article.price) == 0.00) {
                    abp.message.info(App.localize("YouCanNotSellAnArticleWithThatPrice"));
                }
                else if (getIntOrCero(vm.article.quantity) == 0) {

                    abp.message.info(App.localize("YouCanNotSellAnArticleWithQuantityInZero"));
                }
                else if (getIntOrCero(vm.article.id) == 0) {

                    abp.message.info(App.localize("YouMustSelectAnArticleBeforeAddToTheArticleList"));
                }
                else
                    validated = true;

                return validated;
            }

            function refresArticles() {
                $scope.gridArticleOptions.data = vm.trade.articles;
            }

            function getCalculatedTotalWithArticles() {
                var total = 0.00;

                for (var i = 0; i < vm.trade.articles.length; i++) {
                    var ar = vm.trade.articles[i];
                    total += getDoubleOrCero(ar.total);
                }

                return total;
            }

            $scope.calculateTotal = function () {
                    vm.trade.totalCalculed = getCalculatedTotalWithArticles();
                    vm.trade.amount = vm.trade.totalCalculed;
            }


            calculateTotal = function () {

                    vm.trade.totalCalculed = getCalculatedTotalWithArticles();
                    vm.trade.amount = vm.trade.totalCalculed;

                App.initAjax();
                $scope.$apply();
            }

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    vm.trade.date = convertDateFormat_DMY_to_YMD(vm.trade.dateTemp)
                     tradeService.create(vm.trade)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };




			//XXXInsertCallRelatedEntitiesXXX


            vm.courrierPersons = [];
            vm.getCourrierPersons	 = function()
            {
                courrierPersonService.getAllCourrierPersonsForCombo().then(function (result) {
                    vm.courrierPersons = result.data;
					App.initAjax();
                });
            }

            vm.tenants = [];
            vm.getTenants = function () {
                tenantService.getAllTenantsForCombo().then(function (result) {
                    vm.tenants = result.data;
                    App.initAjax();
                });
            }

			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            $scope.gridArticleOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        minWidth: 30
                    },
                    {
                        name: App.localize('Article'),
                        field: 'description',
                    },
                    {
                        name: App.localize('Qty'),
                        field: 'quantity',
                        minWidth: 35
                    },
                    {
                        name: App.localize('Price'),
                        field: 'price',
                        minWidth: 100
                    },
                    {
                        name: App.localize('Total'),
                        field: 'total',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '      <button ng-click="grid.appScope.removeArticle(row.entity)"><i class=\"fa fa-close\"></i> ' + App.localize('remove') + '</a></button>' +
                        '  </div>' +
                        '</div>'
                    }
                ]
            };            

            vm.getCourrierPersons();
            vm.getTenants();

		    App.initAjax();
			setTimeout(function () { $("#tradeComment").focus(); }, 100);
        }
    ]);
})();

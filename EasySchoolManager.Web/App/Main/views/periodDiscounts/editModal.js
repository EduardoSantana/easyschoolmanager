(function () {
    angular.module('MetronicApp').controller('app.views.periodDiscounts.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.periodDiscount', 'id', 'abp.services.app.discountName', 'abp.services.app.period', 'abp.services.app.concept',
        function ($scope, $uibModalInstance, periodDiscountService, id, discountNameService, periodService, conceptService) {
            var vm = this;
            vm.saving = false;

            vm.periodDiscount = {
                isActive: true
            };
            var init = function () {
                periodDiscountService.get({ id: id })
                    .then(function (result) {
                        vm.periodDiscount = result.data;
                        vm.getDiscountNames();
                        vm.getPeriods();
                        vm.getConcepts(vm.periodDiscount.periodId);

                        if (vm.periodDiscount.periodDiscountDetails.length > 0)
                            for (var i = 0; i < vm.periodDiscount.periodDiscountDetails.length; i++) {
                                vm.periodDiscount.periodDiscountDetails[i].deleted = false;
                            }
                        

                        App.initAjax();
                        setTimeout(function () { $("#periodDiscountDiscountNameId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    periodDiscountService.update(vm.periodDiscount)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            //XXXInsertCallRelatedEntitiesXXX

            vm.discountNames = [];
            vm.getDiscountNames = function () {
                discountNameService.getAllDiscountNamesForCombo().then(function (result) {
                    vm.discountNames = result.data;
                    App.initAjax();
                });
            }





            vm.removeInEdit = function (item) {
                abp.message.confirm(App.localize("AreYouSureYouWantToRemoveThePercent"), function (result) {
                    if (result == true) {
                        item.deleted = true;
                        try {
                            setTimeout(function () { $scope.$apply(); }, 400);
                        } catch (e) { }
                    }
                });
            }

            vm.insertItem = function () {
                if (vm.periodDiscount.percent != null & vm.periodDiscount.percent != undefined && vm.periodDiscount.percent > 0) {
                    var det = { id: 0, deleted: false };
                    det.discountPercent = vm.periodDiscount.percent;
                    det.periodDiscountId = vm.periodDiscount.id;

                    if (det.discountPercent <= 0 || det.discountPercent > 100) {
                        abp.message.error(App.localize("TheValueMustBeBetween1And100"));
                        return;
                    }

                    if (vm.periodDiscount.periodDiscountDetails == null || vm.periodDiscount.periodDiscountDetails == undefined)
                        vm.periodDiscount.periodDiscountDetails = [];

                    vm.periodDiscount.periodDiscountDetails.push(det);
                    vm.periodDiscount.percent = null;
                }
            }

            $scope.$watch("vm.periodDiscount.periodId", function (newValue, oldValue) {
                if (newValue != null && newValue != undefined)
                    vm.getConcepts(newValue);
            });

            vm.periods = [];
            vm.getPeriods = function () {
                periodService.getAllPeriodsForCombo().then(function (result) {
                    vm.periods = result.data;
                    App.initAjax();
                });
            }

            vm.concepts = [];
            vm.getConcepts = function (periodId) {
                conceptService.getAllConceptsForDiscountsByPeriodCombo(periodId).then(function (result) {
                    vm.concepts = result.data;
                    App.initAjax();
                });
            }



            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.periodDiscounts.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.periodDiscount', 'settings',
        function ($scope, $timeout, $uibModal, periodDiscountService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getPeriodDiscounts(false);
            }
            vm.periodDiscounts = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openPeriodDiscountEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '      <li><a ng-click="grid.appScope.assignToTenant(row.entity)"><i class=\"fa fa-university\"></i>' + App.localize('AssignToTenant') + '</a></li>' +
                        '      <li><a ng-click="grid.appScope.viewAssignedTenants(row.entity)"><i class=\"fa fa-university\"></i>' + App.localize('ViewAssignedTenants') + '</a></li>' +

                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },


                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                    {
                        name: App.localize('DiscountName'),
                        field: 'discountNames.name',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Period'),
                        field: 'periods.period',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Concept'),
                        field: 'concepts.description',
                        minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getPeriodDiscounts(showTheLastPage) {
                periodDiscountService.getAllPeriodDiscounts($scope.pagination).then(function (result) {
                    vm.periodDiscounts = result.data.items;

                    $scope.gridOptions.data = vm.periodDiscounts;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openPeriodDiscountCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/periodDiscounts/createModal.cshtml',
                    controller: 'app.views.periodDiscounts.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getPeriodDiscounts(false);
                });
            };

            vm.openPeriodDiscountEditModal = function (periodDiscount) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/periodDiscounts/editModal.cshtml',
                    controller: 'app.views.periodDiscounts.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return periodDiscount.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getPeriodDiscounts(false);
                });
            };

            vm.delete = function (periodDiscount) {
                abp.message.confirm(
                    "Delete periodDiscount '" + periodDiscount.name + "'?",
                    function (result) {
                        if (result) {
                            periodDiscountService.delete({ id: periodDiscount.id })
                                .then(function (result) {
                                    getPeriodDiscounts(false);
                                    abp.notify.info("Deleted periodDiscount: " + periodDiscount.name);

                                });
                        }
                    });
            }


            vm.assignToTenant = function (periodDiscount) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/discountNameTenants/createModal.cshtml',
                    controller: 'app.views.discountNameTenants.createModal as vm',
                    backdrop: 'static',
                    resolve: {
                        periodDiscountId: function () {
                            return periodDiscount.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getPeriodDiscounts(false);
                });
            };

            vm.viewAssignedTenants = function (periodDiscount) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/discountNameTenants/index.cshtml',
                    controller: 'app.views.discountNameTenants.index as vm',
                    backdrop: 'static',
                    resolve: {
                        periodDiscountId: function () {
                            return periodDiscount.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getPeriodDiscounts(false);
                });
            };




            vm.refresh = function () {
                getPeriodDiscounts(false);
            };

            getPeriodDiscounts(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.provinces.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.province', 'id', 'abp.services.app.region',
        function ($scope, $uibModalInstance, provinceService, id , regionService) {
            var vm = this;
			vm.saving = false;

            vm.province = {
                isActive: true
            };
            var init = function () {
                provinceService.get({ id: id })
                    .then(function (result) {
                        vm.province = result.data;
						                vm.getRegions();

						App.initAjax();
						setTimeout(function () { $("#provinceName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						provinceService.update(vm.province)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.regions = [];
            vm.getRegions	 = function()
            {
                regionService.getAllRegionsForCombo().then(function (result) {
                    vm.regions = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

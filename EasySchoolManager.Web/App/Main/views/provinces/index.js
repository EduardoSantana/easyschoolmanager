(function () {
    angular.module('MetronicApp').controller('app.views.provinces.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.province','settings',
        function ($scope, $timeout, $uibModal, provinceService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getProvinces(false);
            }
            vm.provinces = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
                    
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openProvinceEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
{
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('ProvinceName'),
                    field: 'name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ProvinceRegion'),
                    field: 'regions_Name',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getProvinces(showTheLastPage) {
                provinceService.getAllProvinces($scope.pagination).then(function (result) {
                    vm.provinces = result.data.items;

                    $scope.gridOptions.data = vm.provinces;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openProvinceCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/provinces/createModal.cshtml',
                    controller: 'app.views.provinces.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getProvinces(true);
                });
            };

            vm.openProvinceEditModal = function (province) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/provinces/editModal.cshtml',
                    controller: 'app.views.provinces.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return province.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getProvinces(false);
                });
            };

            vm.delete = function (province) {
                abp.message.confirm(
                    "Delete province '" + province.name + "'?",
                    function (result) {
                        if (result) {
                            provinceService.delete({ id: province.id })
                                .then(function (result) {
                                    getProvinces(false);
                                    abp.notify.info("Deleted province: " + province.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getProvinces(false);
            };

            getProvinces(false);
        }
    ]);
})();

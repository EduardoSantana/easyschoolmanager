(function () {
    angular.module('MetronicApp').controller('app.views.purchaseShops.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.purchaseShop', 'id', 'abp.services.app.providerShop',
        function ($scope, $uibModalInstance, purchaseShopService, id , providerShopService) {
            var vm = this;
			vm.saving = false;

            vm.purchaseShop = {
                isActive: true
            };
            var init = function () {
                purchaseShopService.get({ id: id })
                    .then(function (result) {
                        vm.purchaseShop = result.data;
						                vm.getProviderShops();

						App.initAjax();
						setTimeout(function () { $("#purchaseShopDocument").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						purchaseShopService.update(vm.purchaseShop)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.providerShops = [];
            vm.getProviderShops	 = function()
            {
                providerShopService.getAllProviderShopsForCombo().then(function (result) {
                    vm.providerShops = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.purchaseShops.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.purchaseShop', 'abp.services.app.providerShop','abp.services.app.articleShop',
        function ($scope, $uibModalInstance, purchaseShopService , providerShopService,articleShopService) {
            var vm = this;
            vm.saving = false;

            var todayDate = getDateYMD(new Date());
            vm.articleShop = {};
            vm.articleShopIdIsFocused = false;

            vm.purchaseShop = {
                isActive: true,
                date: todayDate
            };

            vm.purchaseShop.articleShops = [];

            $scope.getArticleShopsByName = function (userInputString) {
                $scope.pagination.maxResultCount = 10;
                $scope.pagination.textFilter = userInputString;
                return articleShopService.getAllArticleShops($scope.pagination);
            }

            $scope.returnedArticleShop = function (result) {
                if (!result) return;
                vm.articleShop = result.originalObject;
                vm.articleShop.quantity = 1;
            }

            $scope.$watch("vm.articleShop.sequence", function (newValue, oldValue) {
                if (newValue == null) {
                    vm.articleShop = {};
                }
                else {
                    if (vm.articleShopIdIsFocused == true)
                        vm.searArticleShopById();
                }
            });

            vm.searArticleShopById = function () {
                articleShopService.getSpecific(vm.articleShop.sequence).then(function (result) {
                    
                    if (result.data != null) {
                        var resultado = { originalObject: result.data };
                        $scope.returnedArticleShop(resultado);
                        $scope.$broadcast('angucomplete-alt:changeInput', 'ArticleShopAngu', vm.articleShop.description);
                    }

                });
            }


            vm.removeArticleShop = function (articleShop) {
                abp.message.confirm(App.localize("AreYouSureYouWantToRemoveTheArticle"), function (result) {
                    if (result == true) {
                        var index = vm.getIndex(articleShop.id, vm.purchaseShop.articleShops);
                        vm.purchaseShop.articleShops.splice(index, 1);
                        calculateTotal();
                      //  refresArticleShops();
                        try {
                            $scope.$apply();
                        } catch (e) { }
                    }
                });
            }

            vm.getIndex = function (id, array) {
                var index = -1;

                for (var i = 0; i < array.length; i++) {
                    var item = array[i];
                    if (item.id == id) {
                        index = i;
                        break;
                    }
                }
                return index;
            }

            vm.getTempArticleFromLocalList = function (articleId) {
                var currentArticle = null;
                debugger;
                var articles = vm.purchaseShop.articleShops.filter(function (x) { return x.id == articleId });
                if (articles.length > 0)
                    currentArticle = articles[0];
                return currentArticle;
            }


            vm.appendArticleShop = function () {
                
                //Validamos si el articulo puede ser insertado o no en la lista de articulos
                if (!validateArticleShop())
                    return;
                
                vm.articleShop.total = getDoubleOrCero(vm.articleShop.price * vm.articleShop.quantity);

                //entre aqui
                var existingArticle = vm.getTempArticleFromLocalList(vm.articleShop.id);
                if (existingArticle != null) {
                    existingArticle.quantity = parseInt(existingArticle.quantity) + parseInt(vm.articleShop.quantity);
                    existingArticle.total = getDoubleOrCero(existingArticle.price * existingArticle.quantity);
                } else {
                    vm.purchaseShop.articleShops.push(vm.articleShop);
                }
               //y aqui
               
                vm.articleShop = {};
                
                $scope.$broadcast('angucomplete-alt:clearInput', 'ArticleShopAngu');
                
                calculateTotal();
                $("#purchaseArticleShopId").focus();
            }

            function validateArticleShop() {
                
                var validated = false;
                if (getDoubleOrCero(vm.articleShop.price) == 0.00) {
                    abp.message.info(App.localize("YouCanNotSellAnArticleWithThatPrice"));
                }
                else if (getIntOrCero(vm.articleShop.quantity) == 0) {

                    abp.message.info(App.localize("YouCanNotSellAnArticleWithQuantityInZero"));
                }
                else if (getIntOrCero(vm.articleShop.id) == 0) {

                    abp.message.info(App.localize("YouMustSelectAnArticleBeforeAddToTheArticleList"));
                }
                else
                    validated = true;


                return validated;
            }


            function getCalculatedTotalWithArticleShops() {
                var total = 0.00;

                for (var i = 0; i < vm.purchaseShop.articleShops.length; i++) {
                    var ar = vm.purchaseShop.articleShops[i];
                    total += getDoubleOrCero(ar.total);
                }

                return total;
            }

            $scope.calculateTotal = function () {
                
                vm.purchaseShop.totalCalculed = getCalculatedTotalWithArticleShops();
                vm.purchaseShop.amount = vm.purchaseShop.totalCalculed;
            }


            calculateTotal = function () {
                
                vm.purchaseShop.totalCalculed = getCalculatedTotalWithArticleShops();
                vm.purchaseShop.amount = vm.purchaseShop.totalCalculed;
                App.initAjax();
                $scope.$apply();
            }

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     purchaseShopService.create(vm.purchaseShop)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.providerShops = [];
            vm.getProviderShops	 = function()
            {
                providerShopService.getAllProviderShopsForCombo().then(function (result) {
                    vm.providerShops = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			                vm.getProviderShops();

		    App.initAjax();
			setTimeout(function () { $("#purchaseShopDocument").focus(); }, 100);
        }
    ]);
})();

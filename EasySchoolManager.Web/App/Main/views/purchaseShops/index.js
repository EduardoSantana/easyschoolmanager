(function () {
    angular.module('MetronicApp').controller('app.views.purchaseShops.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.purchaseShop','settings',
        function ($scope, $timeout, $uibModal, purchaseShopService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getPurchaseShops(false);
            }
            vm.purchaseShops = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                      //  '      <li><a ng-click="grid.appScope.openPurchaseShopEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'sequence',
                        width: 75
                    },

                                        {
                    name: App.localize('Document'),
                    field: 'document',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Provider'),
                    field: 'providerShops_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Date'),
                    field: 'date',
                    cellFilter: 'date:"dd-MM-yyyy\"',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Description'),
                    field: 'comnent1',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Comment'),
                    field: 'comment2',
                    minWidth: 125
                    },
                    {
                        name: App.localize('Amount'),
                        field: 'amount',
                        minWidth: 125,
                        cellFilter: 'number:2'
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getPurchaseShops(showTheLastPage) {
                purchaseShopService.getAllPurchaseShops($scope.pagination).then(function (result) {
                    vm.purchaseShops = result.data.items;

                    $scope.gridOptions.data = vm.purchaseShops;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openPurchaseShopCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/purchaseShops/createModal.cshtml',
                    controller: 'app.views.purchaseShops.createModal as vm',
                    size: 'lg',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getPurchaseShops(false);
                });
            };

            vm.openPurchaseShopEditModal = function (purchaseShop) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/purchaseShops/editModal.cshtml',
                    controller: 'app.views.purchaseShops.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return purchaseShop.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getPurchaseShops(false);
                });
            };

            vm.delete = function (purchaseShop) {
                abp.message.confirm(
                    "Delete purchaseShop '" + purchaseShop.name + "'?",
                    function (result) {
                        if (result) {
                            purchaseShopService.delete({ id: purchaseShop.id })
                                .then(function (result) {
                                    getPurchaseShops(false);
                                    abp.notify.info("Deleted purchaseShop: " + purchaseShop.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getPurchaseShops(false);
            };

            getPurchaseShops(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.bloods.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.blood', 
        function ($scope, $uibModalInstance, bloodService ) {
            var vm = this;
            vm.saving = false;

            vm.blood = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     bloodService.create(vm.blood)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#bloodName").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.bloods.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.blood', 'id', 
        function ($scope, $uibModalInstance, bloodService, id ) {
            var vm = this;
			vm.saving = false;

            vm.blood = {
                isActive: true
            };
            var init = function () {
                bloodService.get({ id: id })
                    .then(function (result) {
                        vm.blood = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#bloodName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						bloodService.update(vm.blood)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

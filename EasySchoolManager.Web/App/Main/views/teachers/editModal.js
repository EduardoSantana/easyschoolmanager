(function () {
    angular.module('MetronicApp').controller('app.views.teachers.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.teacher', 'id', 'abp.services.app.gender',
        function ($scope, $uibModalInstance, teacherService, id , genderService) {
            var vm = this;
			vm.saving = false;

            vm.teacher = {
                isActive: true,

                dateOfBirth: null
            };
            var init = function () {
                teacherService.get({ id: id })
                    .then(function (result) {
                        vm.teacher = result.data;
                        vm.getGenders();
                        vm.teacher.dateOfBirthTemp = convertDateFormat_YMD_to_DMY(vm.teacher.dateOfBirth, 4);

						App.initAjax();
						setTimeout(function () { $("#teacherName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;

                if (!validateForm()) {
                    vm.saving = false;
                    return;
                }
                try {
						teacherService.update(vm.teacher)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.genders = [];
            vm.getGenders	 = function()
            {
                genderService.getAllGendersForCombo().then(function (result) {
                    vm.genders = result.data;
					App.initAjax();
                });
            }

            $scope.$watch("vm.teacher.dateOfBirthTemp", function (newValue, oldValue) {
                vm.teacher.dateOfBirth = convertDateFormat_DMY_to_YMD(newValue, 4);
            });

            function validateForm() {
                var valid = false;
                try {
                    if (!validatePersonalId(vm.teacher.personalId, true)) {
                        abp.message.error(App.localize("PersonalIdIsInvalid"));
                    }

                    else if (vm.teacher.dateOfBirth == null) {
                        abp.message.error(App.localize("TheDateOfBirthIsInvalid"));
                    }
                    else if (!validateDateDMY(vm.teacher.dateOfBirthTemp)) {
                        abp.message.error(App.localize("TheDateOfBirthIsInvalid"));
                    }

                    else if (vm.teacher.genderId == null) {
                        abp.message.error(App.localize("YouMustSelectTheGender"));
                    }

                    else valid = true;

                } catch (e) {
                    console.log(e);
                    abp.message.error(JSON.stringify(e));
                }
                return valid;
            }
			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

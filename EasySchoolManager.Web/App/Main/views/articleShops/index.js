(function () {
	angular.module('MetronicApp').controller('app.views.articleShops.index', [
		'$scope', '$timeout', '$uibModal', 'abp.services.app.articleShop','settings',
		function ($scope, $timeout, $uibModal, articleShopService, settings) {
			var vm = this;
			vm.textFilter = "";

			$scope.pagination = new Pagination(0, 10);
			$scope.pagination.reload = function () {
				getArticleShops(false);
			}

			$scope.enterToGoToPage = function (e) {
				if (e.which == 13) {
					$scope.pagination.goToPage($scope.pagination.currentPage);
				}
			}

			vm.articleShops = [];

			$scope.gridOptions = {
				appScopeProvider: vm,
				columnDefs: [
					{
						name: App.localize('Actions'),
						enableSorting: false,
						width: 120,
						cellTemplate:
						'<div class=\"ui-grid-cell-contents\">' +
						'  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
						'    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
						'    <ul uib-dropdown-menu>' +
						'      <li><a ng-click="grid.appScope.openArticleShopEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
						//'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
						'    </ul>' +
						'  </div>' +
						'</div>'
					},
					
					
					{
						name: App.localize('Id'),
						field: 'sequence',
						width: 75
					},

										{
					name: App.localize('Description'),
					field: 'description',
					minWidth: 125
					},
					{
					name: App.localize('Reference'),
					field: 'reference',
					minWidth: 125
					},
					{
					name: App.localize('Price'),
					field: 'price',
					minWidth: 125
					},
					{
					name: App.localize('Unit'),
					field: 'units_Description',
					minWidth: 125
					},
					{
						name: App.localize('ButtonPosition'),
						field: 'buttonPositions_Number',
						minWidth: 125
					},
					 {
						name: App.localize('IsActive'),
						field: 'isActive',
						cellTemplate:
						' <div style=\"text-align:center;\"> ' +
						'<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
						'<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
						'<label for="\checkbox16\">' +
						'  <span class="\inc\"></span>' +
						'   <span class="\check\"></span>' +
						'  <span class=\"box\" ></span> </label>' +
						'   </div>'
						,
						width: 120
					}
					]
			};


			$scope.$watch("vm.textFilter", function (newValue, oldValue) {
				$scope.pagination.textFilter = newValue;
				if ($scope.pagination.currentPage > 1)
					$scope.pagination.first();
				$scope.pagination.reload();
			});

			function getArticleShops(showTheLastPage) {
				articleShopService.getAllArticleShops($scope.pagination).then(function (result) {
					vm.articleShops = result.data.items;

					$scope.gridOptions.data = vm.articleShops;

					$scope.pagination.assignTotalRecords(result.data.totalCount);
					if (showTheLastPage)
						$scope.pagination.last();
				});
			}

			vm.openArticleShopCreationModal = function () {
				var modalInstance = $uibModal.open({
					templateUrl: settings.settings.appPathNotHTTP + '/views/articleShops/createModal.cshtml',
					controller: 'app.views.articleShops.createModal as vm',
					backdrop: 'static',
					resolve: {
						listUsedPlace: function () {
							var retVal = [];
							for (var i = 0; i < vm.articleShops.length ; i++) {
								retVal.push(vm.articleShops[i].buttonPositions_Number);
							}
							
							return retVal;
						}
					}
				});

				modalInstance.rendered.then(function () {
				   App.initAjax();
				});

				modalInstance.result.then(function () {
					getArticleShops(false);
				});
			};

			vm.openArticleShopEditModal = function (articleShop) {
				var modalInstance = $uibModal.open({
					templateUrl: settings.settings.appPathNotHTTP + '/views/articleShops/editModal.cshtml',
					controller: 'app.views.articleShops.editModal as vm',
					backdrop: 'static',
					resolve: {
						id: function () {
							return articleShop.id;
						}
					}
				});

				modalInstance.rendered.then(function () {
					$timeout(function () {
					   App.initAjax();
					}, 0);
				});

				modalInstance.result.then(function () {
					getArticleShops(false);
				});
			};

			vm.delete = function (articleShop) {
				abp.message.confirm(
					"Delete articleShop '" + articleShop.name + "'?",
					function (result) {
						if (result) {
							articleShopService.delete({ id: articleShop.id })
								.then(function (result) {
									getArticleShops(false);
									abp.notify.info("Deleted articleShop: " + articleShop.name);

								});
						}
					});
			}

			vm.refresh = function () {
				getArticleShops(false);
			};

			getArticleShops(false);
		}
	]);
})();

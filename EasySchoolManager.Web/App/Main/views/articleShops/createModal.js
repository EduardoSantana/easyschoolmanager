(function () {
    angular.module('MetronicApp').controller('app.views.articleShops.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.articleShop', 'abp.services.app.articlesType', 'abp.services.app.unit', 'abp.services.app.brand', 'abp.services.app.buttonPosition', 'listUsedPlace',
        function ($scope, $uibModalInstance, articleShopService, articlesTypeService, unitService, brandService, buttonPositionService, listUsedPlace) {
            var vm = this;
            debugger;
            vm.saving = false;

            vm.articleShop = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     articleShopService.create(vm.articleShop)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function(e){
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                    } 
                    catch (e)
                    {
                       vm.saving = false;
                    }
            };

            //XXXInsertCallRelatedEntitiesXXX

            vm.units = [];
            vm.getUnits	 = function()
            {
                unitService.getAllUnitsForCombo().then(function (result) {
                    vm.units = result.data;
                    App.initAjax();
                });
            }

            vm.brands = [];
            vm.getBrands	 = function()
            {
                brandService.getAllBrandsForCombo().then(function (result) {
                    vm.brands = result.data;
                    App.initAjax();
                });
            }

            vm.buttonPositions = [];
            vm.getButtonPositions = function () {
                buttonPositionService.getAllButtonPositionsForCombo().then(function (result) {
                    var retVal = [];
                    for (var i1 = 0; i1 < result.data.length; i1++) {
                        var addItem = true;
                        for (var i2 = 0; i2 < listUsedPlace.length; i2++) {
                            if (result.data[i1].number == listUsedPlace[i2]) {
                                addItem = false;
                                break;
                            }
                        }
                        if (addItem) {
                            retVal.push(result.data[i1]);
                        }
                    }
                    vm.buttonPositions = retVal;
                    App.initAjax();
                });
            }



            
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

                vm.getUnits();
                vm.getBrands();
                vm.getButtonPositions();

            App.initAjax();
            setTimeout(function () { $("#articleShopDescription").focus(); }, 100);
        }
    ]);
})();

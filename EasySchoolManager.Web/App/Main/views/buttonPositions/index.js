(function () {
    angular.module('MetronicApp').controller('app.views.buttonPositions.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.buttonPosition','settings',
        function ($scope, $timeout, $uibModal, buttonPositionService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getButtonPositions(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.buttonPositions = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openButtonPositionEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                    name: App.localize('ButtonPositionNumber'),
                    field: 'number',
                    minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getButtonPositions(showTheLastPage) {
                buttonPositionService.getAllButtonPositions($scope.pagination).then(function (result) {
                    vm.buttonPositions = result.data.items;

                    $scope.gridOptions.data = vm.buttonPositions;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openButtonPositionCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/buttonPositions/createModal.cshtml',
                    controller: 'app.views.buttonPositions.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getButtonPositions(false);
                });
            };

            vm.openButtonPositionEditModal = function (buttonPosition) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/buttonPositions/editModal.cshtml',
                    controller: 'app.views.buttonPositions.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return buttonPosition.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getButtonPositions(false);
                });
            };

            vm.delete = function (buttonPosition) {
                abp.message.confirm(
                    "Delete buttonPosition '" + buttonPosition.name + "'?",
                    function (result) {
                        if (result) {
                            buttonPositionService.delete({ id: buttonPosition.id })
                                .then(function (result) {
                                    getButtonPositions(false);
                                    abp.notify.info("Deleted buttonPosition: " + buttonPosition.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getButtonPositions(false);
            };

            getButtonPositions(false);
        }
    ]);
})();

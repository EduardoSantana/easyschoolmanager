(function () {
    angular.module('MetronicApp').controller('app.views.buttonPositions.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.buttonPosition', 
        function ($scope, $uibModalInstance, buttonPositionService ) {
            var vm = this;
            vm.saving = false;

            vm.buttonPosition = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     buttonPositionService.create(vm.buttonPosition)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#buttonPositionNumber").focus(); }, 100);
        }
    ]);
})();

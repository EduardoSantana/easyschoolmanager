(function () {
    angular.module('MetronicApp').controller('app.views.buttonPositions.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.buttonPosition', 'id', 
        function ($scope, $uibModalInstance, buttonPositionService, id ) {
            var vm = this;
			vm.saving = false;

            vm.buttonPosition = {
                isActive: true
            };
            var init = function () {
                buttonPositionService.get({ id: id })
                    .then(function (result) {
                        vm.buttonPosition = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#buttonPositionNumber").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						buttonPositionService.update(vm.buttonPosition)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

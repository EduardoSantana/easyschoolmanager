(function () {
    angular.module('MetronicApp').controller('app.views.reportsFilters.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.reportsFilter', 'settings','$uibModalInstance',
        function ($scope, $timeout, $uibModal, reportsFilterService, settings, $uibModalInstance) {
            var vm = this;
            vm.textFilter = "";
            vm.title = $scope.$resolve.title;

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getReportsFilters(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.reportsFilters = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 100,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu class="show-top">' +
                        '      <li><a ng-click="grid.appScope.edit(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Name'),
                        field: 'name',
                        width: 160
                    },
                    {
                        name: App.localize('Type'),
                        field: 'dataType.description',
                        width: 100
                    },
                    {
                        name: App.localize('Range'),
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '   <span ng-if="row.entity.range">' + App.localize('Yes')+ '</span>' +
                        '   <span ng-if="!row.entity.range">' + App.localize('No') +'</span>' +
                        '   </div>'
                        ,
                        width: 80
                    },
                    {
                        name: App.localize('Service'),
                        field: 'urlService',
                        minWidth: 125
                    },
                    {
                        name: App.localize('OnlyParameter'),
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '   <span ng-if="row.entity.onlyParameter">' + App.localize('Yes') + '</span>' +
                        '   <span ng-if="!row.entity.onlyParameter">' + App.localize('No') + '</span>' +
                        '   </div>'
                        ,
                        width: 120
                    },
                    {
                        name: App.localize('Required'),
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '   <span ng-if="row.entity.required">' + App.localize('Yes') + '</span>' +
                        '   <span ng-if="!row.entity.required">' + App.localize('No') + '</span>' +
                        '   </div>'
                        ,
                        width: 100
                    },
                    {
                        name: App.localize('Operator'),
                        field : 'operator',
                        width: 100
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '   <span ng-if="row.entity.isActive">' + App.localize('Yes') + '</span>' +
                        '   <span ng-if="!row.entity.isActive">' + App.localize('No') + '</span>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };

            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getReportsFilters(showTheLastPage) {
                reportsFilterService.getAllReportsFilterByReportId({id: $scope.$resolve.id,textFilter: vm.textFilter }).then(function (result) {
                    vm.reportsFilters = result.data;
                    $scope.gridOptions.data = vm.reportsFilters;
                });
            }

            vm.edit = function (reportFilter) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/reportsFilters/createModal.cshtml',
                    controller: 'app.views.reportsFilters.createModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return reportFilter.reportsId;
                        },
                        reportFilterId: function () {
                            return reportFilter.id;
                        },
                        done: function () {
                            return vm.refresh;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    getReports(true);
                });
            };

            vm.openReportsFilterEditModal = function (reportsFilter) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/reportsFilters/editModal.cshtml',
                    controller: 'app.views.reportsFilters.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return reportsFilter.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getReportsFilters(false);
                });
            };

            vm.delete = function (reportsFilter) {
                abp.message.confirm(
                    "Delete reportsFilter '" + reportsFilter.name + "'?",
                    function (result) {
                        if (result) {
                            reportsFilterService.delete({ id: reportsFilter.id })
                                .then(function (result) {
                                    getReportsFilters(false);
                                    abp.notify.info("Deleted reportsFilter: " + reportsFilter.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getReportsFilters(false);
            };

            getReportsFilters(false);

            vm.close = function () {
                $uibModalInstance.close();
            }
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.reportsFilters.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.reportsFilter', 'id', '$timeout', '$uibModal', 'abp.services.app.reportsQuery', 'settings',
        function ($scope, $uibModalInstance, reportsFilterService, id, $timeout, $uibModal, reportsQueryService, settings) {
            var vm = this;
            vm.saving = false;
            vm.reportsFilter = {
                isActive: true
            };

            var init = function () {
                reportsFilterService.get({ id: id })
                    .then(function (result) {
                        vm.reportsFilter = result.data;

                        App.initAjax();
                        setTimeout(function () { $("#reportsFilterTipo").focus(); getReportsQueries(false); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    reportsFilterService.update(vm.reportsFilter)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();


            // Comienza parte de tabla


            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                init();
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<a ng-click="grid.appScope.openReportsQueryEditModal(row.entity)" class=\"btn btn-circle btn-icon-only btn-default\" title="' + App.localize('Edit') + '"><i class=\"fa fa-pencil\"></i></a> ' +
                        '<a ng-click="grid.appScope.deleteReportsQuery(row.entity)" class=\"btn btn-circle btn-icon-only btn-default\" title="' + App.localize('Delete') + '"><i class=\"fa fa-close\"></i></a> '
                    },
                    {
                        name: App.localize('ReportsQueryValueId'),
                        field: 'valueId',
                        minWidth: 125
                    },
                    {
                        name: App.localize('ReportsQueryValueDisplay'),
                        field: 'valueDisplay',
                        minWidth: 125
                    }
                ]
            };

            function getReportsQueries(showTheLastPage) {
                $scope.gridOptions.data = vm.reportsFilter.reportsQuerys;
                $scope.pagination.assignTotalRecords(vm.reportsFilter.reportsQuerys.length);
                if (showTheLastPage)
                    $scope.pagination.last();

            }

            vm.openReportsQueryCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/reportsQueries/createModal.cshtml',
                    controller: 'app.views.reportsQueries.createModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: id
                    }
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    init();
                });
            };

            vm.openReportsQueryEditModal = function (reportsQuery) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/reportsQueries/editModal.cshtml',
                    controller: 'app.views.reportsQueries.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return reportsQuery.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    init();
                });
            };

            vm.deleteReportsQuery = function (reportsQuery) {
                abp.message.confirm(
                    "Delete reportsQuery '" + reportsQuery.name + "'?",
                    function (result) {
                        if (result) {
                            reportsQueryService.delete({ id: reportsQuery.id })
                                .then(function (result) {
                                    init();
                                    abp.notify.info("Deleted reportsQuery: " + reportsQuery.name);

                                });
                        }
                    });
            }


        }
    ]);
})();

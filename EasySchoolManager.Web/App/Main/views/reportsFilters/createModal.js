(function () {
    angular.module('MetronicApp').controller('app.views.reportsFilters.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.reportsFilter', 'abp.services.app.report',
        function ($scope, $uibModalInstance, reportsFilterService,reportService) {
            var vm = this;
            vm.saving = false;



            function init() {
                vm.reportsFilter = {
                    isActive: true,
                    reportsId: $scope.$resolve.id,
                    reportId: $scope.$resolve.id,
                    id: $scope.$resolve.reportFilterId
                };

                if (vm.reportsFilter.id) {
                    reportsFilterService.get({ id: vm.reportsFilter.id }).then(function (result) {
                        vm.reportsFilter = result.data;

                    });

                }

            }

            init();

            vm.dataTypes = [];
            vm.services = [];

            getServices();

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    var method = "create";
                    if (vm.reportsFilter.id) method = "update";

                    reportsFilterService[method](vm.reportsFilter)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            init();
                            $scope.$resolve.done();
                        }, function (e) {
                           vm.saving = false;
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            App.initAjax();

            reportService.getDataTypes().then(function (result) {

                vm.dataTypes = result.data;
            });

            setTimeout(function () { $("#reportsFilterReportsId").focus(); }, 100);

            function getServices() {
                for (var service in abp.services.app) {
                    for (var method in abp.services.app[service])
                        vm.services.push('api/services/app/' + service + '/' + method);
                }
            }
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.inventory.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.inventory', 
        function ($scope, $uibModalInstance, inventoryService ) {
            var vm = this;
            vm.saving = false;

            vm.inventory = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     inventoryService.create(vm.inventory)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#inventoryArticleId").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.inventory.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.inventory', 'id', 
        function ($scope, $uibModalInstance, inventoryService, id ) {
            var vm = this;
			vm.saving = false;

            vm.inventory = {
                isActive: true
            };
            var init = function () {
                inventoryService.get({ id: id })
                    .then(function (result) {
                        vm.inventory = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#inventoryArticleId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						inventoryService.update(vm.inventory)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

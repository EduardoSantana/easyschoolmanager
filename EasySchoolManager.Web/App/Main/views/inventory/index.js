(function () {
    angular.module('MetronicApp').controller('app.views.inventory.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.inventory', 'settings', 'abp.services.app.tenant', 'appSession',
        function ($scope, $timeout, $uibModal, inventoryService, settings, tenantService, appSession) {
         
            var vm = this;
            vm.textFilter = "";

            vm.appSession = appSession;

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getInventory(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.inventory = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openInventoryEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                      //  '      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                    name: App.localize('Tenant'),
                    field: 'tenantInventories_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Article'),
                    field: 'articles_Description',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Existence'),
                    field: 'existence',
                    minWidth: 125
                    },

                    {
                    name: App.localize('Price'),
                    field: 'price',
                    minWidth: 125,
                    cellFilter: 'number:2'
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };

            if (appSession.tenant != null) {
                $scope.gridOptions.columnDefs.splice(4, 1);
            }

            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });


            $scope.$watch("vm.currentTenantId", function (newValue, oldValue) {
                
                vm.refresh();
 
            });

            function getInventory(showTheLastPage) {
               
                if (vm.currentTenantId != null)
                $scope.pagination.tenantId = vm.currentTenantId.id;
                inventoryService.getAllInventory($scope.pagination).then(function (result) {
                    vm.inventory = result.data.items;

                    $scope.gridOptions.data = vm.inventory;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openInventoryCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/inventory/createModal.cshtml',
                    controller: 'app.views.inventory.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getInventory(false);
                });
            };

            vm.openInventoryEditModal = function (inventory) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/inventory/editModal.cshtml',
                    controller: 'app.views.inventory.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return inventory.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getInventory(false);
                });
            };


            vm.tenants = [];
            vm.getTenants = function () {

                tenantService.getAllTenantsForCombo().then(function (result) {
                    vm.tenants = result.data;
                    App.initAjax();
                });
            }

            vm.delete = function (inventory) {
                abp.message.confirm(
                    "Delete inventory '" + inventory.name + "'?",
                    function (result) {
                        if (result) {
                            inventoryService.delete({ id: inventory.id })
                                .then(function (result) {
                                    getInventory(false);
                                    abp.notify.info("Deleted inventory: " + inventory.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getInventory(false);
            };

            getInventory(false);

            if (appSession.tenant == null) {
                               vm.getTenants();
            }

        }
    ]);
})();

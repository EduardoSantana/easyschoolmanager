(function () {
    angular.module('MetronicApp').controller('app.views.grantEnterprises.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.grantEnterprise', 
        function ($scope, $uibModalInstance, grantEnterpriseService ) {
            var vm = this;
            vm.saving = false;

            vm.grantEnterprise = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     grantEnterpriseService.create(vm.grantEnterprise)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#grantEnterpriseSequence").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.grantEnterprises.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.grantEnterprise', 'id', 
        function ($scope, $uibModalInstance, grantEnterpriseService, id ) {
            var vm = this;
			vm.saving = false;

            vm.grantEnterprise = {
                isActive: true
            };
            var init = function () {
                grantEnterpriseService.get({ id: id })
                    .then(function (result) {
                        vm.grantEnterprise = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#grantEnterpriseSequence").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						grantEnterpriseService.update(vm.grantEnterprise)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

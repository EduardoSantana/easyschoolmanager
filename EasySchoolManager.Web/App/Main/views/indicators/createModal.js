(function () {
    angular.module('MetronicApp').controller('app.views.indicators.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.indicator', 'abp.services.app.indicatorGroup',
        function ($scope, $uibModalInstance, indicatorService , indicatorGroupService) {
            var vm = this;
            vm.saving = false;

            vm.indicator = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     indicatorService.create(vm.indicator)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.indicatorGroups = [];
            vm.getIndicatorGroups	 = function()
            {
                indicatorGroupService.getAllIndicatorGroupsForComboWitoutChildrens().then(function (result) {
                    vm.indicatorGroups = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			                vm.getIndicatorGroups();

		    App.initAjax();
			setTimeout(function () { $("#indicatorName").focus(); }, 100);
        }
    ]);
})();

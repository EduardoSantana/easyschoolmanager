(function () {
    angular.module('MetronicApp').controller('app.views.indicators.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.indicator','settings',
        function ($scope, $timeout, $uibModal, indicatorService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getIndicators(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.indicators = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Acc'),
                        enableSorting: false,
                        width: 50,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openIndicatorEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                    name: App.localize('Group'),
                    field: 'indicatorGroups.name',
                    width: 180
                    },								
                    {
                        name: App.localize('Sequence'),
                        field: 'sequence',
                        width: 80
                    },
                    {
                    name: App.localize('Description'),
                    field: 'name',
                    minWidth: 300
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 80
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getIndicators(showTheLastPage) {
                indicatorService.getAllIndicators($scope.pagination).then(function (result) {
                    vm.indicators = result.data.items;

                    $scope.gridOptions.data = vm.indicators;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openIndicatorCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/indicators/createModal.cshtml',
                    controller: 'app.views.indicators.createModal as vm',
                    size: 'lg',                 
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getIndicators(false);
                });
            };

            vm.openIndicatorEditModal = function (indicator) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/indicators/editModal.cshtml',
                    controller: 'app.views.indicators.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return indicator.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getIndicators(false);
                });
            };

            vm.delete = function (indicator) {
                abp.message.confirm(
                    "Delete indicator '" + indicator.name + "'?",
                    function (result) {
                        if (result) {
                            indicatorService.delete({ id: indicator.id })
                                .then(function (result) {
                                    getIndicators(false);
                                    abp.notify.info("Deleted indicator: " + indicator.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getIndicators(false);
            };

            getIndicators(false);
        }
    ]);
})();

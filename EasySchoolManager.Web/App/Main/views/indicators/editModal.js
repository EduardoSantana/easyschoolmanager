(function () {
    angular.module('MetronicApp').controller('app.views.indicators.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.indicator', 'id', 'abp.services.app.indicatorGroup',
        function ($scope, $uibModalInstance, indicatorService, id , indicatorGroupService) {
            var vm = this;
			vm.saving = false;

            vm.indicator = {
                isActive: true
            };
            var init = function () {
                indicatorService.get({ id: id })
                    .then(function (result) {
                        vm.indicator = result.data;
						                vm.getIndicatorGroups();

						App.initAjax();
						setTimeout(function () { $("#indicatorName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						indicatorService.update(vm.indicator)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.indicatorGroups = [];
            vm.getIndicatorGroups	 = function()
            {
                indicatorGroupService.getAllIndicatorGroupsForCombo().then(function (result) {
                    vm.indicatorGroups = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

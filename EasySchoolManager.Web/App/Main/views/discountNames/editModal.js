(function () {
    angular.module('MetronicApp').controller('app.views.discountNames.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.discountName', 'id', 
        function ($scope, $uibModalInstance, discountNameService, id ) {
            var vm = this;
			vm.saving = false;

            vm.discountName = {
                isActive: true
            };
            var init = function () {
                discountNameService.get({ id: id })
                    .then(function (result) {
                        vm.discountName = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#discountNameName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						discountNameService.update(vm.discountName)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

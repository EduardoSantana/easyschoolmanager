(function () {
    angular.module('MetronicApp').controller('app.views.discountNames.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.discountName', 
        function ($scope, $uibModalInstance, discountNameService ) {
            var vm = this;
            vm.saving = false;

            vm.discountName = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     discountNameService.create(vm.discountName)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#discountNameName").focus(); }, 100);
        }
    ]);
})();

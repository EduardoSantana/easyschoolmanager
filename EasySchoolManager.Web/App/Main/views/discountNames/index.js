(function () {
    angular.module('MetronicApp').controller('app.views.discountNames.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.discountName', 'settings',
        function ($scope, $timeout, $uibModal, discountNameService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getDiscountNames(false);
            }
            vm.discountNames = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openDiscountNameEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },


                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                    {
                        name: App.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: App.localize('HasGrandEnterprise'),
                        field: 'hasGrantEnterprise',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.hasGrantEnterprise\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 200
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getDiscountNames(showTheLastPage) {
                discountNameService.getAllDiscountNames($scope.pagination).then(function (result) {
                    vm.discountNames = result.data.items;

                    $scope.gridOptions.data = vm.discountNames;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openDiscountNameCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/discountNames/createModal.cshtml',
                    controller: 'app.views.discountNames.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getDiscountNames(false);
                });
            };

            vm.openDiscountNameEditModal = function (discountName) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/discountNames/editModal.cshtml',
                    controller: 'app.views.discountNames.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return discountName.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getDiscountNames(false);
                });
            };


            vm.delete = function (discountName) {
                abp.message.confirm(
                    "Delete discountName '" + discountName.name + "'?",
                    function (result) {
                        if (result) {
                            discountNameService.delete({ id: discountName.id })
                                .then(function (result) {
                                    getDiscountNames(false);
                                    abp.notify.info("Deleted discountName: " + discountName.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getDiscountNames(false);
            };

            getDiscountNames(false);
        }
    ]);
})();

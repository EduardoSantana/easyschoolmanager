(function () {
    angular.module('MetronicApp').controller('app.views.subjects.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.subject', 'abp.services.app.course',
        function ($scope, $uibModalInstance, subjectService , courseService) {
            var vm = this;
            vm.saving = false;

            vm.subject = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     subjectService.create(vm.subject)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.courses = [];
            vm.getCourses	 = function()
            {
                courseService.getAllCoursesForCombo().then(function (result) {
                    vm.courses = result.data;
					App.initAjax();
                });
            }

			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

	        vm.getCourses();


		    App.initAjax();
			setTimeout(function () { $("#subjectName").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.subjects.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.subject','settings',
        function ($scope, $timeout, $uibModal, subjectService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getSubjects(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.subjects = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 80,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openSubjectEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                    name: App.localize('Code'),
                    field: 'code',
                    minWidth: 50
                    },
                    {
                    name: App.localize('Name'),
                    field: 'name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('LongName'),
                    field: 'longName',
                    minWidth: 125
                    },

                    {
                    name: App.localize('Course'),
                    field: 'courses.name',
                    minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 100
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getSubjects(showTheLastPage) {
                subjectService.getAllSubjects($scope.pagination).then(function (result) {
                    vm.subjects = result.data.items;

                    $scope.gridOptions.data = vm.subjects;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openSubjectCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/subjects/createModal.cshtml',
                    controller: 'app.views.subjects.createModal as vm',
                    backdrop: 'static',
                    size: 'lg'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getSubjects(false);
                });
            };

            vm.openSubjectEditModal = function (subject) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/subjects/editModal.cshtml',
                    controller: 'app.views.subjects.editModal as vm',
                    backdrop: 'static',
                    size:'lg',
                    resolve: {
                        id: function () {
                            return subject.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getSubjects(false);
                });
            };

            vm.delete = function (subject) {
                abp.message.confirm(
                    "Delete subject '" + subject.name + "'?",
                    function (result) {
                        if (result) {
                            subjectService.delete({ id: subject.id })
                                .then(function (result) {
                                    getSubjects(false);
                                    abp.notify.info("Deleted subject: " + subject.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getSubjects(false);
            };

            getSubjects(false);
        }
    ]);
})();

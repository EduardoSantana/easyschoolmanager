(function () {
    angular.module('MetronicApp').controller('app.views.companies.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.company','settings',
        function ($scope, $timeout, $uibModal, companyService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getCompanies(false);
            }
            vm.companies = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openCompanyEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('CompanyRNC'),
                    field: 'rnc',
                    minWidth: 125
                    },
                    {
                    name: App.localize('CompanyName'),
                    field: 'name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('CompanyCommercialName'),
                    field: 'commercialName',
                    minWidth: 125
                    },
                    {
                    name: App.localize('CompanyCommercialActivity'),
                    field: 'commercialActivity',
                    minWidth: 125
                    },
                    {
                    name: App.localize('CompanyCompanyDate'),
                    field: 'companyDate',
                    minWidth: 125
                    },
                    {
                    name: App.localize('CompanyStatus'),
                    field: 'status',
                    minWidth: 125
                    },
                    {
                    name: App.localize('CompanyPaymentSystem'),
                    field: 'paymentSystem',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getCompanies(showTheLastPage) {
                companyService.getAllCompanies($scope.pagination).then(function (result) {
                    vm.companies = result.data.items;

                    $scope.gridOptions.data = vm.companies;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openCompanyCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/companies/createModal.cshtml',
                    controller: 'app.views.companies.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getCompanies(false);
                });
            };

            vm.openCompanyEditModal = function (company) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/companies/editModal.cshtml',
                    controller: 'app.views.companies.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return company.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getCompanies(false);
                });
            };

            vm.delete = function (company) {
                abp.message.confirm(
                    "Delete company '" + company.name + "'?",
                    function (result) {
                        if (result) {
                            companyService.delete({ id: company.id })
                                .then(function (result) {
                                    getCompanies(false);
                                    abp.notify.info("Deleted company: " + company.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getCompanies(false);
            };

            getCompanies(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.companies.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.company', 'id', 
        function ($scope, $uibModalInstance, companyService, id ) {
            var vm = this;
			vm.saving = false;

            vm.company = {
                isActive: true
            };
            var init = function () {
                companyService.get({ id: id })
                    .then(function (result) {
                        vm.company = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#companyRNC").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						companyService.update(vm.company)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

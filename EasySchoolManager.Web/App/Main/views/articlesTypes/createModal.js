(function () {
    angular.module('MetronicApp').controller('app.views.articlesTypes.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.articlesType', 
        function ($scope, $uibModalInstance, articlesTypeService ) {
            var vm = this;
            vm.saving = false;

            vm.articlesType = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     articlesTypeService.create(vm.articlesType)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#articlesTypeDescription").focus(); }, 100);
        }
    ]);
})();

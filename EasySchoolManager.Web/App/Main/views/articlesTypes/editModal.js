(function () {
    angular.module('MetronicApp').controller('app.views.articlesTypes.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.articlesType', 'id', 
        function ($scope, $uibModalInstance, articlesTypeService, id ) {
            var vm = this;
			vm.saving = false;

            vm.articlesType = {
                isActive: true
            };
            var init = function () {
                articlesTypeService.get({ id: id })
                    .then(function (result) {
                        vm.articlesType = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#articlesTypeDescription").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						articlesTypeService.update(vm.articlesType)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

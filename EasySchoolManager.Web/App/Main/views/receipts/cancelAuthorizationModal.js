(function () {
    angular.module('MetronicApp').controller('app.views.receipts.cancelAuthorizationModal', [
        '$scope', '$uibModal', '$uibModalInstance', 'abp.services.app.transaction', 'id', 'abp.services.app.bank', 'abp.services.app.grantEnterprise', 'abp.services.app.paymentMethod', 'abp.services.app.concept', 'abp.services.app.enrollment',  'settings',
        function ($scope, $uibModal, $uibModalInstance, transactionService, id, bankService, grantEnterpriseService, paymentMethodService, conceptService, enrollmentService, settings) {
            var vm = this;
            vm.saving = false;


            vm.loadingEditingValues = true;

            $scope.pagination = new Pagination(0, 10);
            vm.amountIsFocused = false;

            vm.transaction = {
                isActive: true,
                indicator : 1
            };
            var init = function () {
                transactionService.get({ id: id })
                    .then(function (result) {
                        vm.transaction = result.data;
                        vm.transaction.articles = vm.transaction.transactionArticles;
                        vm.transaction.currentEnrollment = result.data.enrollments;
                        if (vm.transaction.enrollment > 0)
                            vm.transaction.enrollment = vm.transaction.enrollment;
                        else
                            vm.transaction.enrollment = '';
                        vm.transaction.dateTemp = convertDateFormat_YMD_to_DMY(vm.transaction.date, 4);

                        if (vm.transaction.currentEnrollment)
                            $scope.gridOptions.data = vm.transaction.currentEnrollment.enrollmentStudents;

                        vm.getBanks();
                        vm.getGrantEnterprises();
                        vm.getPaymentMethods();
                        vm.getConcepts1();
                        vm.getConcepts2();
                        vm.getConcepts3();

                        if (vm.transaction.currentEnrollment)
                            calculateTotal();
                        else
                            vm.transaction.totalCalculed = vm.transaction.amount;

                        $scope.applyInitAjax();
                        setTimeout(function () { $("#transactionSequence").focus(); }, 100);

                        $scope.$broadcast('angucomplete-alt:changeInput', 'enrollmentsAngu', vm.transaction.name);
                        $scope.$broadcast('angucomplete-alt:changeInput', 'enrollmentsAnguw', vm.transaction.name);
                    });
            }
            $scope.getEnrollmentsByName = function (userInputString) {
                $scope.pagination.maxResultCount = 10;
                $scope.pagination.textFilter = userInputString;
                return enrollmentService.getAllEnrollments($scope.pagination);
            }

            $scope.returnedValue = function (result) {
                if (!result) return;
                vm.transaction.enrollment = result.originalObject.enrollment;
                vm.transaction.name = result.originalObject.firstName + " " + result.originalObject.lastName;
            }
            function cleanTransaction() {
                vm.transaction.name = null;
                vm.transaction.enrolmentId = null;
                vm.transaction.currentEnrollment = null;
                $scope.$broadcast('angucomplete-alt:clearInput', 'enrollmentsAngu');

                $scope.gridOptions.data = [];
            }

            //Function para asignar los montos distribuidos a los diferentes estudiantes
            function assignDistributedValuesToStudents(distAmount) {
                if (vm.transaction.currentEnrollment.enrollmentStudents == null ||
                    vm.transaction.currentEnrollment.enrollmentStudents.length == 0)
                    return;

                for (var i = 0; i < distAmount.length; i++) {
                    var ele = distAmount[i];
                    vm.transaction.currentEnrollment.enrollmentStudents[i].students.amount = ele;
                }

                vm.transaction.totalCalculed = getCalculatedTotalByEnrollment();
            }


            $scope.$watch("vm.transaction.enrollment", function (newValue, oldValue) {
                if (newValue != undefined) {
                    if (newValue == null || newValue.length === 0) {
                        if (oldValue != undefined)
                            cleanTransaction();
                        return;
                    }


                    enrollmentService.getEnrollmentSequenceByEnrollment(newValue).then(function (result) {

                        if (result.data == null) {
                            cleanTransaction();
                        }
                        else {
                            if (vm.loadingEditingValues) {
                                vm.loadingEditingValues = false;
                                return;
                            }
                            else
                            {
                                vm.enrollment = result.data;
                                vm.transaction.enrolmentId = vm.enrollment.enrollmentId;
                                vm.transaction.currentEnrollment = vm.enrollment.enrollments;
                                if (vm.transaction.currentEnrollment != null) {
                                    vm.transaction.currentEnrollment.fullName = vm.transaction.currentEnrollment.firstName + ' ' + vm.transaction.currentEnrollment.lastName;
                                    vm.transaction.name = vm.transaction.currentEnrollment.fullName;
                                    //    calculateChildrenAmounts(vm.transaction.amount);
                                    $scope.gridOptions.data = vm.transaction.currentEnrollment.enrollmentStudents;
                                    $scope.$broadcast('angucomplete-alt:changeInput', 'enrollmentsAngu', vm.transaction.name);
                                }
                            }
                        }
                    });
                }
                else
                { cleanTransaction(); }

            });


            function calculateChildrenAmounts(newValue) {
                var childrenNumber = vm.transaction.currentEnrollment.enrollmentStudents.length;
                if (childrenNumber == 0) {
                    vm.transaction.totalCalculed = newValue;
                    return;
                }
                var distributedAmount = distributeAmount_SimpleMethod(newValue, childrenNumber)
                assignDistributedValuesToStudents(distributedAmount);
            }

       //     $scope.$watch("vm.transaction.amount", function (newValue, oldValue) {
       //         if (newValue == undefined || newValue == null || newValue.length == 0) {
       //             vm.transaction.letterAmount = null;
       //             return;
       //         }
       //         try {
       //             vm.transaction.letterAmount = NumeroALetras(newValue);
       //             if (vm.amountIsFocused) {
       //                 if (vm.transaction.currentEnrollment == null) {
       //                     vm.transaction.totalCalculed = newValue;
       //                     return;
       //                 }
       //                 calculateChildrenAmounts(newValue);
       //             }
       //         } catch (e) {
       //             console.log(JSON.stringify(e))
       //         }
       //     });


            function getCalculatedTotalByEnrollment() {
                var total = 0.00;

                for (var i = 0; i < vm.transaction.currentEnrollment.enrollmentStudents.length; i++) {
                    var es = vm.transaction.currentEnrollment.enrollmentStudents[i];
                    total += getDoubleOrCero(es.students.amount);
                }

                return total;
            }

            vm.anulate = function () {

                if (vm.transaction.statusId == 2)
                {
                    abp.message.error(App.localize("ThisReceiptIsAlreadyAnulated"));
                    return;
                }

                abp.message.confirm(
                    "AreYouSureYouWantToAnulateThisRecceipt",
                    function (result) {
                        if (result) {
                            transactionService.anulateInventoryReceipt({ transactionId: vm.transaction.id })
                                .then(function (result) {

                                    abp.notify.info(App.localize("ReceiptAnulated"));
                                    vm.cancel();
                                });
                        }
                    });
            }

            $scope.calculateTotal = function () {
                vm.transaction.totalCalculed = getCalculatedTotalByEnrollment();
            }


            calculateTotal = function () {
                vm.transaction.totalCalculed = getCalculatedTotalByEnrollment();
                vm.transaction.amount = vm.transaction.totalCalculed;
                try {
                    $scope.applyInitAjax();
                    $scope.$apply();
                } catch (e) { }
            }

            $scope.applyInitAjax = function ()
            {
                try {
                    App.initAjax();
                } catch (e) { }
            }

            validatePayment = function () {
                var validated = false;
                if (getDoubleOrCero(vm.transaction.amount) != getDoubleOrCero(vm.transaction.totalCalculed)) {
                    abp.message.error(App.localize("TheTransactionAmountDoesNotCorrespondToTheirDistribution"));
                }
                else if (vm.transaction.enrollment == null && getObjectValueOrEmptyString(vm.transaction.name).length === 0) {
                    abp.message.error(App.localize("YouMustSpecifyANameToApplyTheTransaction"));
                }
                else if (vm.transaction.enrollment != null && getObjectValueOrEmptyString(vm.transaction.name).length === 0) {
                    abp.message.error(App.localize("YouMustSpecifyAnExistingEnrollmentToPallyTheTransaction"));
                }
                else validated = true;


                return validated;
            }

            validatePayment = function () {
                var validated = false;
                if (getDoubleOrCero(vm.transaction.amount) != getDoubleOrCero(vm.transaction.totalCalculed)) {
                    abp.message.error(App.localize("TheTransactionAmountDoesNotCorrespondToTheirDistribution"));
                }
                else if (vm.transaction.enrollment == null && getObjectValueOrEmptyString(vm.transaction.name).length === 0) {
                    abp.message.error(App.localize("YouMustSpecifyANameToApplyTheTransaction"));
                }
                else if (vm.transaction.enrollment != null && getObjectValueOrEmptyString(vm.transaction.name).length === 0) {
                    abp.message.error(App.localize("YouMustSpecifyAnExistingEnrollmentToPallyTheTransaction"));
                }
                else validated = true;


                return validated;
            }

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    vm.transaction.date = convertDateFormat_DMY_to_YMD(vm.transaction.dateTemp)
                    vm.transaction.students = [];
                   // vm.transaction.amount = 0;
                   // vm.transaction.indicator = 1;
                    if (vm.transaction.currentEnrollment != null) {
                        vm.transaction.enrollmentId = vm.transaction.currentEnrollment.id;

                        for (var i = 0; i < vm.transaction.currentEnrollment.enrollmentStudents.length; i++) {
                            var es = vm.transaction.currentEnrollment.enrollmentStudents[i];
                            es.students.enrollmentStudentId = es.id;
                            es.isActive = true;
                            vm.transaction.students.push(es.students);

                        }
                    }

                    transactionService.update(vm.transaction)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            //XXXInsertCallRelatedEntitiesXXX


            vm.transaction.dateTemp = convertDateFormat_YMD_to_DMY(vm.transaction.date, 4);


            vm.banks = [];
            vm.getBanks = function () {
                bankService.getAllBanksForCombo().then(function (result) {
                    vm.banks = result.data;
                    $scope.applyInitAjax();
                });
            }

            vm.grantEnterprises = [];
            vm.getGrantEnterprises = function () {
                grantEnterpriseService.getAllGrantEnterprisesForCombo().then(function (result) {
                    vm.grantEnterprises = result.data;
                    $scope.applyInitAjax();
                });
            }

            vm.paymentMethods = [];
            vm.getPaymentMethods = function () {
                paymentMethodService.getAllPaymentMethodsForCombo().then(function (result) {
                    vm.paymentMethods = result.data;
                    $scope.applyInitAjax();
                });
            }


            vm.concepts1 = [];
            vm.getConcepts1 = function () {
                conceptService.getAllConceptsForReceipts1Combo().then(function (result) {
                    vm.concepts1 = result.data;
                    $scope.applyInitAjax();
                });
            }

            vm.concepts2 = [];
            vm.getConcepts2 = function () {
                conceptService.getAllConceptsForReceipts2Combo().then(function (result) {
                    vm.concepts2 = result.data;
                    $scope.applyInitAjax();
                });
            }

            vm.concepts3 = [];
            vm.getConcepts3 = function () {
                conceptService.getAllConceptsForReceipts3Combo().then(function (result) {
                    vm.concepts3 = result.data;
                    $scope.applyInitAjax();
                });
            }


            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };


            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('student'),
                        field: 'students.fullName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Course'),
                        field: 'students.course.abbreviation',
                       },
                    {
                        name: App.localize('Amount'),
                        field: 'amount',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class=\"form-group\" style=\"text-align:right; width:100%; margin-left:2px;\">' +
                        '<input type=\"number\" class="paymentBox form-control" min=\"0.00\" step=\"0.01\" id=\"txtAmount\" class="\md-check\" ng-model=\"row.entity.students.amount\" onchange=\"calculateTotal();\" onkeypress=\"return enterToTab(event,this);\" disabled readonly>' +
                        '   </div>'
                        ,
                        width: 150
                    }
                ]
            };

            vm.searchEnrollmentByStudentName = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/enrollments/queryByStudent/index.cshtml',
                    controller: 'app.views.enrollments.querybyStudent.index as vm',
                    backdrop: 'static',
                    size: 'lg'
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        $scope.applyInitAjax();
                    }, 0);
                });

                modalInstance.result.then(function (result) {
                    if (result == null)
                        return;
                    vm.transaction.enrollment = result.matricula;
                });
            }

            function getStudents() {
                $scope.gridOptions.data = [{ students_name: '', Course: '' }];
            }


            getStudents();

            init();
        }
    ]);
})();

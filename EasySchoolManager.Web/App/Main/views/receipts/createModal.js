(function () {
    var s = 'abp.services.app.';
    angular.module('MetronicApp').controller('app.views.receipts.createModal', [
        '$scope', '$uibModal', '$uibModalInstance', s + 'transaction', s + 'bank',
        'abp.services.app.grantEnterprise', s + 'paymentMethod', s + 'concept',
        s + 'enrollment', 'report', '$http', s + 'article', 'appSession', 'settings', s + 'taxReceipt', s + 'company',
        function ($scope, $uibModal, $uibModalInstance, transactionService, bankService, grantEnterpriseService, paymentMethodService, conceptService, enrollmentService, report, $http, articleService, appSession, settings, taxReceiptService, companyService) {
            var vm = this;
            vm.saving = false;
            vm.enrollment = {};

            $scope.pagination = new Pagination(0, 10);
            vm.amountIsFocused = false;

            vm.article = {};
            vm.articleIdIsFocused = false;

            var todayDate = getDateYMD(new Date());
            
            var ff = new Date(transactionService.freeDateReceiptTenant());

            vm.permissions = {
                enroll: abp.auth.hasPermission('Pages.Receipts.Enrollment'),
                gener: abp.auth.hasPermission('Pages.Receipts.General'),
                inven: abp.auth.hasPermission('Pages.Receipts.Inventory'),
                datem: abp.auth.hasPermission('Pages.Receipts.Date'),
                daterest: abp.auth.hasPermission('Pages.Receipts.DateRest')
            };

            vm.transaction = {
                isActive: true,
                paymentMethodId: null,
                transactionTypeId: 1, //Recibo
                letterAmount: null,
                originId: 2,  // Credito
                statusId: 1,  //Creado
                receiptTypeId: 1,
                date: todayDate,
                isMultiConcept: false,
                restday: 0
            };

            vm.transaction.transactionConcepts = []

            vm.transaction.articles = [];

            $scope.getEnrollmentsByName = function (userInputString) {
                $scope.pagination.maxResultCount = 10;
                $scope.pagination.textFilter = userInputString;
                vm.transaction.name = userInputString;
                return enrollmentService.getAllEnrollments($scope.pagination);
            }

            $scope.returnedValue = function (result) {
                if (!result) return;
                vm.transaction.enrollment = result.originalObject.enrollment;
                vm.transaction.name = result.originalObject.firstName + " " + result.originalObject.lastName;
            }

            $scope.getArticlesByName = function (userInputString) {
                $scope.pagination.maxResultCount = 10;
                $scope.pagination.textFilter = userInputString;
                return articleService.getAllArticles($scope.pagination);
            }

            $scope.returnedArticle = function (result) {
                if (!result) return;
                vm.article = result.originalObject;
                vm.article.quantity = 1;
            }

            $scope.$watch("vm.article.id", function (newValue, oldValue) {

                if (newValue == null) {
                    vm.article = {};
                }
                else {
                    if (vm.articleIdIsFocused == true)
                        vm.searArticleById();
                }
            });

            $scope.$watch("vm.transaction.rncNumber", function (newValue, oldValue) {

                if (newValue == undefined || newValue == null || newValue.length == 0) {
                    vm.transaction.rncName = null;
                    vm.transaction.taxReceiptId = null;
                    setTimeout(function () { $scope.$apply(); App.initAjax(); }, 400);

                    return;

                }

                     getCompanyByRNC();

            });

            function getCompanyByRNC() {
                
                companyService.getCompanyByRNC({ rnc: vm.transaction.rncNumber }).then(function (result) {
                    var company = result.data;
                    if (company.rnc != null) {
                        if (company.status.toUpperCase() != "ACTIVO")
                            abp.message.info("La empresa " + company.commercialName + " (" + company.name + " ) no se encuentra activa, tiene el estatus " + company.status);
                    }
                    vm.transaction.rncName = company.commercialName || company.name;
                });
            }

            vm.searArticleById = function () {
                articleService.get({ id: vm.article.id }).then(function (result) {

                    if (result.data != null) {
                        var resultado = { originalObject: result.data };
                        var prices = result.data.inventories.filter(function (x) { return x.tenantInventoryId == appSession.tenant.id });
                        if (prices.length == 0) {
                            abp.message.error(App.localize("ThisArticleDoesNotHavePriceRegisteredForthisSchool"));
                            return;
                        }

                        resultado.originalObject.price = prices[0].price;

                        $scope.returnedArticle(resultado);
                        $scope.$broadcast('angucomplete-alt:changeInput', 'ArticleAngu', vm.article.description);
                    }

                });
            }

            vm.removeArticle = function (article) {
                abp.message.confirm(App.localize("AreYouSureYouWantToRemoveTheArticle"), function (result) {
                    if (result == true) {
                        var index = vm.getIndex(article.id, vm.transaction.articles);
                        vm.transaction.articles.splice(index, 1);
                        calculateTotal();
                        refresArticles();
                        try {
                            setTimeout(function () { $scope.$apply(); }, 400);
                        } catch (e) { }
                    }
                });
            }

            vm.removeConcept = function (row) {
                var index = vm.getIndex(row.id, vm.transaction.transactionConcepts);
                vm.transaction.transactionConcepts.splice(index, 1);
                calculateTotal();
            }

            vm.getIndex = function (id, array) {
                var index = -1;

                for (var i = 0; i < array.length; i++) {
                    var item = array[i];
                    if (item.id == id) {
                        index = i;
                        break;
                    }
                }
                return index;
            }

            vm.getTempArticleFromLocalList = function (articleId) {
                var currentArticle = null;

                var articles = vm.transaction.articles.filter(function (x) { return x.id == articleId });
                if (articles.length > 0)
                    currentArticle = articles[0];
                return currentArticle;
            }

            vm.appendArticle = function () {

                //Validamos si el articulo puede ser insertado o no en la lista de articulos
                if (!validateArticle())
                    return;

                vm.article.total = getDoubleOrCero(vm.article.price * vm.article.quantity);

                var existingArticle = vm.getTempArticleFromLocalList(vm.article.id);
                if (existingArticle != null) {
                    existingArticle.quantity = parseInt(existingArticle.quantity) + parseInt(vm.article.quantity);
                    existingArticle.total = getDoubleOrCero(existingArticle.price * existingArticle.quantity);
                } else {
                    vm.transaction.articles.push(vm.article);
                }
                vm.article = {};

                $scope.$broadcast('angucomplete-alt:clearInput', 'ArticleAngu');
                refresArticles();
                calculateTotal();
                $("#transactionArticleId").focus();
            }

            function validateArticle() {
                var validated = false;
                if (getDoubleOrCero(vm.article.price) == 0.00) {
                    abp.message.info(App.localize("YouCanNotSellAnArticleWithThatPrice"));
                }
                else if (getIntOrCero(vm.article.quantity) == 0) {

                    abp.message.info(App.localize("YouCanNotSellAnArticleWithQuantityInZero"));
                }
                else if (getIntOrCero(vm.article.id) == 0) {

                    abp.message.info(App.localize("YouMustSelectAnArticleBeforeAddToTheArticleList"));
                }
                else
                    validated = true;

                return validated;
            }

            vm.transaction.dateTemp = convertDateFormat_YMD_to_DMY(todayDate, 4);

            vm.transaction.date = todayDate;



            vm.restdate = function (da)
            {  
                var f = new Date();
                var mes = parseInt(f.getMonth()) + 1;              
                f= new Date( f.setDate(f.getDate() - da));
                var mes2 = parseInt(f.getMonth()) + 1;
                var f2 = getDateYMD(f); 
              
                if (mes == mes2) {
                    vm.transaction.dateTemp = convertDateFormat_YMD_to_DMY(f2, 4);
                }
                else { abp.message.info(App.localize("MonthChange"));}
            }

            vm.freedate = function ()
            {
                ff = new Date(ff.setDate(ff.getDate()));
                var f2 = getDateYMD(ff); 
                vm.transaction.dateTemp = convertDateFormat_YMD_to_DMY(f2, 4);
            }


            function cleanTransaction() {
                vm.transaction.name = null;
                //vm.transaction.enrolmentId = null;
                //vm.transaction.currentEnrollment = null;
                vm.totalBalance = 0.00;
                vm.absTotalBalance = 0.00;
                vm.transaction.amount = null;
                $scope.$broadcast('angucomplete-alt:clearInput', 'enrollmentsAngu');
                $scope.$broadcast('angucomplete-alt:clearInput', 'ArticleAngu');
                $scope.gridOptions.data = [];
                $scope.gridArticleOptions.data = [];
                vm.transaction.articles = [];
                vm.article = {};
                setTimeout(function () { $scope.$apply(); }, 400);
            }

            validatePayment = function () {
                var validated = false;
                if (getDoubleOrCero(vm.transaction.amount) != getDoubleOrCero(vm.transaction.totalCalculed)) {
                    abp.message.error(App.localize("TheReceiptAmountDoesNotCorrespondToTheirDistribution"));
                }
                else if (vm.transaction.enrollment == null && getObjectValueOrEmptyString(vm.transaction.name).length === 0) {
                    abp.message.error(App.localize("YouMustSpecifyANameToApplyTheReceipt"));
                }
                else if (vm.transaction.enrollment != null && getObjectValueOrEmptyString(vm.transaction.name).length === 0) {
                    abp.message.error(App.localize("YouMustSpecifyAnExistingEnrollmentToPallyTheReceipt"));
                }
                else if (vm.transaction.receiptTypeId == 3 && vm.transaction.articles.length == 0) {
                    abp.message.error(App.localize("YouMustAppendTheArticlesThatWillBePayedInThisReceipt"));
                }
                else if (vm.transaction.receiptTypeId == 2 && vm.transaction.isMultiConcept && vm.transaction.transactionConcepts.length == 0) {
                    abp.message.error(App.localize("YouMustAppendTheConceptsThatWillBePayedInThisReceipt"));
                }
                else validated = true;

                return validated;
            }

            $scope.distributeAmount_SimpleMethod = function (amount, numberOfItems) {
                var distribution = [];
                var amountSplited = getDoubleOrCero(amount / numberOfItems);

                for (var i = 0; i < numberOfItems; i++) {
                    distribution[i] = amountSplited;
                }

                return distribution;
            }

            vm.lastEnrollmentId = 0;

            $scope.$watch("vm.transaction.enrollment", function (newValue, oldValue) {

                if (newValue != undefined) {
                    if (newValue == null || newValue.length === 0) {
                        vm.lastEnrollmentId = 0;
                        cleanTransaction();
                        return;
                    }

                    vm.lastEnrollmentId = newValue;

                    setTimeout(function () {
                        if (newValue == vm.lastEnrollmentId) {

                            enrollmentService.getEnrollmentSequenceByEnrollment(newValue).then(function (result) {

                                if (result.data == null) {
                                    vm.lastEnrollmentId = 0;
                                    cleanTransaction();
                                }
                                else {
                                    vm.enrollment = result.data;
                                    vm.transaction.amount = 0;
                                    vm.transaction.totalCalculed = 0;
                                    vm.transaction.enrolmentId = vm.enrollment.enrollmentId;
                                    vm.transaction.currentEnrollment = vm.enrollment.enrollments;
                                    if (vm.transaction.currentEnrollment != null) {
                                        vm.transaction.currentEnrollment.fullName = vm.transaction.currentEnrollment.firstName + ' ' + vm.transaction.currentEnrollment.lastName;
                                        vm.transaction.name = vm.transaction.currentEnrollment.fullName;
                                        if (vm.transaction.receiptTypeId == 3) {
                                            vm.transaction.enrollmentStudents = [];
                                        }
                                        $scope.gridOptions.data = vm.transaction.currentEnrollment.enrollmentStudents;
                                        $scope.$broadcast('angucomplete-alt:changeInput', 'enrollmentsAngu', vm.transaction.name);
                                    }

                                    transactionService.getEnrollmentBalance({ sequence: newValue }).then(function (result) {
                                        if (result.data == null) {
                                            vm.totalBalance = 0.00;
                                            vm.absTotalBalance = 0.00;
                                            return;
                                        }
                                        vm.totalBalance = result.data.totalBalance;
                                        vm.absTotalBalance = result.data.absTotalBalance;
                                    });

                                }
                            });

                        }
                    }, 900);

                }
                else { vm.lastEnrollmentId = 0; cleanTransaction(); }

            });

            vm.isMultiConceptChanged = function isMultiConceptChanged() {
                vm.transaction.transactionConcepts = [];
                vm.transaction.insertAmount = null;
                vm.transaction.conceptId = null;
                setTimeout(function () {
                    $("#transactionConcept2").trigger('change');
                }, 200);
                calculateTotal();
            }

            vm.conceptIdChange = function conceptIdChange() {
                if (vm.transaction.receiptTypeId == 2 && vm.transaction.conceptId && !vm.transaction.isMultiConcept) {
                    for (var i = 0; i < vm.concepts2.length; i++) {
                        if (vm.concepts2[i].id == vm.transaction.conceptId && vm.concepts2[i].amount) {
                            vm.transaction.amount = vm.concepts2[i].amount;
                        }
                    }
                }
                if (vm.transaction.receiptTypeId == 2 && vm.transaction.conceptId && vm.transaction.isMultiConcept) {
                    for (var i = 0; i < vm.concepts2.length; i++) {
                        if (vm.concepts2[i].id == vm.transaction.conceptId && vm.concepts2[i].amount) {
                            vm.transaction.insertAmount = vm.concepts2[i].amount;
                        }
                    }
                    setTimeout(function () {
                        $("#transactionInsertAmount2").focus();
                    }, 400);
                }
            }

            vm.isValidToSubmit = function isValidToSubmit() {
                if (vm.transaction.receiptTypeId != 2 || !vm.transaction.isMultiConcept) {
                    return true;
                }
                if (vm.transaction.transactionConcepts.length > 0) {
                    return true;
                }
                return false;
            }

            vm.enterToInsert = function enterToInsert(e) {

                var keyCode = e.which || e.keyCode;

                if (keyCode === 13) {

                    if (vm.transaction.insertAmount > 0 &&
                        vm.transaction.conceptId > 0 &&
                        vm.transaction.insertAmount != null &&
                        vm.transaction.conceptId != null) {

                        var index = vm.getIndex(vm.transaction.conceptId, vm.transaction.transactionConcepts);
                        var obj = vm.transaction.transactionConcepts[index];

                        if (obj) {
                            vm.transaction.insertAmount += obj.amount;
                            vm.transaction.transactionConcepts.splice(index, 1);
                        }

                        vm.transaction.transactionConcepts.push({
                            id: vm.transaction.conceptId,
                            amount: vm.transaction.insertAmount,
                            conceptId: vm.transaction.conceptId,
                            concept_name: $("#transactionConcept2").select2('data')[0].text
                        });

                        vm.transaction.insertAmount = null;
                        vm.transaction.conceptId = null;
                        setTimeout(function () {
                            $("#transactionConcept2").trigger('change');
                        }, 200);
                        calculateTotal();

                    }
                }
            }

            function refresArticles() {
                $scope.gridArticleOptions.data = vm.transaction.articles;
            }

            //Function para asignar los montos distribuidos a los diferentes estudiantes
            function assignDistributedValuesToStudents(distAmount) {
                if (vm.transaction.currentEnrollment.enrollmentStudents == null ||
                    vm.transaction.currentEnrollment.enrollmentStudents.length == 0)
                    return;

                for (var i = 0; i < distAmount.length; i++) {
                    var ele = distAmount[i];
                    vm.transaction.currentEnrollment.enrollmentStudents[i].students.amount = ele;
                }

                vm.transaction.totalCalculed = getCalculatedTotalByEnrollment();
                vm.transaction.amount = totalCalculed;
            }

            $scope.$watch("vm.transaction.amount", function (newValue, oldValue) {
                if (newValue == undefined || newValue == null || newValue.length == 0) {
                    vm.transaction.letterAmount = null;
                    return;
                }
                try {
                    vm.transaction.letterAmount = NumeroALetras(newValue);

                    if (vm.amountIsFocused) {

                        if (vm.transaction.currentEnrollment == null) {
                            vm.transaction.totalCalculed = newValue;
                            return;
                        }


                        var childrenNumber = vm.transaction.currentEnrollment.enrollmentStudents.length;
                        if (childrenNumber == 0) {
                            vm.transaction.totalCalculed = newValue;
                            return;
                        }

                        var distributedAmount = distributeAmount_SimpleMethod(newValue, childrenNumber)

                        assignDistributedValuesToStudents(distributedAmount);
                    }

                } catch (e) {
                    console.log(JSON.stringify(e))
                }
            });

            function getCalculatedTotalWithArticles() {
                var total = 0.00;

                for (var i = 0; i < vm.transaction.articles.length; i++) {
                    var ar = vm.transaction.articles[i];
                    total += getDoubleOrCero(ar.total);
                }

                return total;
            }

            function getCalculatedTotalWithStudents() {
                var total = 0.00;

                for (var i = 0; i < vm.transaction.articles.length; i++) {
                    var es = vm.transaction.students[i];
                    total += getDoubleOrCero(es.amount);
                }

                return total;
            }

            function getCalculatedTotalByEnrollment() {
                var total = 0.00;

                for (var i = 0; i < vm.transaction.currentEnrollment.enrollmentStudents.length; i++) {
                    var es = vm.transaction.currentEnrollment.enrollmentStudents[i];
                    total += getDoubleOrCero(es.students.amount);
                }

                return total;
            }

            function getCalculatedTotalWithConcepts() {
                var total = 0.00;

                for (var i = 0; i < vm.transaction.transactionConcepts.length; i++) {
                    var ar = vm.transaction.transactionConcepts[i];
                    total += getDoubleOrCero(ar.amount);
                }

                return total;
            }

            $scope.calculateTotal = function () {
                if (vm.transaction.receiptTypeId == 1)
                    vm.transaction.totalCalculed = getCalculatedTotalByEnrollment();
                else if (vm.transaction.receiptTypeId == 2 && vm.transaction.isMultiConcept) {
                    vm.transaction.totalCalculed = getCalculatedTotalWithConcepts();
                    vm.transaction.amount = vm.transaction.totalCalculed;
                }
                else if (vm.transaction.receiptTypeId == 3) {//Cuando el tipo sea de Inventario
                    vm.transaction.totalCalculed = getCalculatedTotalWithArticles();
                    vm.transaction.amount = vm.transaction.totalCalculed;
                }
            }

            calculateTotal = function () {
                if (vm.transaction.receiptTypeId == 1) {
                    vm.transaction.totalCalculed = getCalculatedTotalByEnrollment();
                    vm.transaction.amount = vm.transaction.totalCalculed;
                }
                else if (vm.transaction.receiptTypeId == 2 && vm.transaction.isMultiConcept) {
                    vm.transaction.totalCalculed = getCalculatedTotalWithConcepts();
                    vm.transaction.amount = vm.transaction.totalCalculed;
                }
                else if (vm.transaction.receiptTypeId == 3) {  //Cuando el tipo sea de Inventario
                    vm.transaction.totalCalculed = getCalculatedTotalWithArticles();
                    vm.transaction.amount = vm.transaction.totalCalculed;
                }
                App.initAjax();
                setTimeout(function () {
                    $scope.$apply();
                }, 400);
            }




            vm.save = function () {

                if (!validatePayment())
                    return;

                if (vm.saving === true)
                    return;

                if (vm.transaction.receiptTypeId === 1 && (!vm.transaction.currentEnrollment.enrollmentStudents || vm.transaction.currentEnrollment.enrollmentStudents.length === 0)) {
                    abp.message.error(App.localize('Tutor sin estudiantes, imposible salvar recibo'));
                    return;
                }

                if (vm.transaction.amount === 0) {
                    abp.message.error('No pueder guardar un recibo con monto en cero');
                    return;
                }


                vm.saving = true;


                try {

                    vm.transaction.date = convertDateFormat_DMY_to_YMD(vm.transaction.dateTemp);

                    var transaction = $.extend({}, vm.transaction);
                    transaction.students = [];


                    if (transaction.currentEnrollment != null) {
                        transaction.enrollmentId = transaction.currentEnrollment.id;

                        for (var i = 0; i < transaction.currentEnrollment.enrollmentStudents.length; i++) {
                            var es = transaction.currentEnrollment.enrollmentStudents[i];
                            es.students.enrollmentStudentId = es.id;
                            transaction.students.push(es.students);

                        }
                    }


                    if (vm.transaction.restday > 0)
                    {
                        transaction.isActive = 0;
                    }

                    //if (!transaction.taxReceiptId)
                    //    transaction.taxReceiptId = 0;

                    transactionService.createAndPrint(transaction)
                        .then(function (result) {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                            printReceipt(result.data);
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            function printReceipt(result) {

                var objectReport = {};
                var reportsFilters = [];

                if (vm.transaction.receiptTypeId == 1)
                    objectReport.reportCode = "04";
                else if (vm.transaction.receiptTypeId == 2)
                    objectReport.reportCode = "25";
                else if (vm.transaction.receiptTypeId == 3)
                    objectReport.reportCode = "26";

                objectReport.reportExport = "PDF";
                reportsFilters.push({ name: "TransactionId", value: result.id.toString() });
                reportsFilters.push({ name: "AmountText", value: NumeroALetras(vm.transaction.amount) });

                objectReport.reportsFilters = reportsFilters;
                report.printDirect(objectReport);

                //TODO : hacer que bolier plate no maneje el error de ajax

            }

            //XXXInsertCallRelatedEntitiesXXX

            vm.banks = [];

            vm.getBanks = function () {
                bankService.getAllBanksForCombo().then(function (result) {
                    vm.banks = result.data;
                    App.initAjax();
                });
            }

            vm.grantEnterprises = [];

            vm.getGrantEnterprises = function () {
                grantEnterpriseService.getAllGrantEnterprisesForCombo().then(function (result) {
                    vm.grantEnterprises = result.data;
                    App.initAjax();
                });
            }

            vm.concepts1 = [];

            vm.getConcepts1 = function () {
                conceptService.getAllConceptsForReceipts1Combo().then(function (result) {
                    vm.concepts1 = result.data;
                    App.initAjax();
                });
            }

            vm.concepts2 = [];

            vm.getConcepts2 = function () {
                conceptService.getAllConceptsForReceipts2Combo().then(function (result) {
                    vm.concepts2 = result.data;
                    App.initAjax();
                });
            }

            vm.concepts3 = [];

            vm.getConcepts3 = function () {
                conceptService.getAllConceptsForReceipts3Combo().then(function (result) {
                    vm.concepts3 = result.data;
                    App.initAjax();
                });
            }

            vm.paymentMethods = [];

            vm.getPaymentMethods = function () {
                paymentMethodService.getAllPaymentMethodsForCombo().then(function (result) {
                    vm.paymentMethods = result.data;
                    App.initAjax();
                });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getBanks();
            vm.getGrantEnterprises();
            vm.getPaymentMethods();
            vm.getConcepts1();
            vm.getConcepts2();
            vm.getConcepts3();

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Students'),
                        field: 'students.fullName'
                    },
                    {
                        name: App.localize('Course'),
                        field: 'students.course.abbreviation',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Balance'),
                        field: 'studentBalance',
                        minWidth: 125,
                        cellFilter: 'number:2',
                        cellClass: "font-uigrid-right"
                    },
                    {
                        name: App.localize('Amount'),
                        field: 'amount',
                        cellTemplate:
                            ' <div style=\"text-align:center;\"> ' +
                            '<div class=\"form-group\" style=\"text-align:right; width:100%; margin-left:2px;\">' +
                            '<input type=\"number\" class="paymentBox form-control" min=\"0.00\" step=\"0.01\" id=\"txtAmount\" class="\md-check\" ng-model=\"row.entity.students.amount\" onchange=\"calculateTotal();\" autocomplete=off onkeyup=\"calculateTotal();\"   onmousewheel=\"return false\"    onkeypress=\"return enterToTab(event,this);\">' +
                            '   </div>'
                        ,
                        width: 150
                    }
                ]
            };


            function getStudents() {
                $scope.gridOptions.data = [{ students_name: '', Course: '' }];
            }

            vm.taxReceipts = [];
            function getTaxReceipt() {
                taxReceiptService.getAllTaxReceiptsByRegion().then(function (result) {
                    vm.taxReceipts = result.data;
                    App.initAjax();
                });
            }

            vm.cleanNonRelatedDate = function () {
                vm.transaction.name = null;
                vm.transaction.enrollment = null;
                vm.transaction.enrollmentStudents = [];
                vm.transaction.currentEnrollment = null;
                App.initAjax();
                //$scope.$apply();
            }

            $scope.gridArticleOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        minWidth: 30
                    },
                    {
                        name: App.localize('Article'),
                        field: 'description',
                    },
                    {
                        name: App.localize('Qty'),
                        field: 'quantity',
                        minWidth: 35
                    },
                    {
                        name: App.localize('Price'),
                        field: 'price',
                        minWidth: 100
                    },
                    {
                        name: App.localize('Total'),
                        field: 'total',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '      <button ng-click="grid.appScope.removeArticle(row.entity)"><i class=\"fa fa-close\"></i> ' + App.localize('remove') + '</a></button>' +
                            '  </div>' +
                            '</div>'
                    }
                    //},
                    //{
                    //    name: App.localize('Total'),
                    //    field: 'amount',
                    //    cellTemplate:
                    //    ' <div style=\"text-align:center;\"> ' +
                    //    '<div class=\"form-group\" style=\"text-align:right; width:100%; margin-left:2px;\">' +
                    //    '<input type=\"number\" class="paymentBox form-control" min=\"0.00\" step=\"0.01\" id=\"txtAmount\" class="\md-check\" ng-model=\"row.entity.students.amount\" onchange=\"calculateTotal();\" onkeypress=\"return enterToTab(event,this);\" readonly disabled>' +
                    //    '   </div>'
                    //    ,
                    //    width: 150
                    //}
                ]
            };

            vm.searchEnrollmentByStudentName = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/enrollments/queryByStudent/index.cshtml',
                    controller: 'app.views.enrollments.querybyStudent.index as vm',
                    backdrop: 'static',
                    size: 'lg'
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function (result) {
                    if (result == null)
                        return;
                    vm.transaction.enrollment = result.matricula;
                });
            }


            getStudents();
            getTaxReceipt();

            App.initAjax();

            setTimeout(function () { $("#transactionSequence").focus(); }, 100);
        }
    ]);
})();

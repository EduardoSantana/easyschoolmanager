(function () {
    angular.module('MetronicApp').controller('app.views.receipts.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.transaction', 'settings', '$stateParams', 'report',
        function ($scope, $timeout, $uibModal, receiptService, settings, $stateParams, report) {
            var vm = this;
            vm.textFilter = "";
            vm.reportCode = "";


            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getReceipts(false);
            }


            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.receipts = [];

            vm.permissions = {
                edit: abp.auth.hasPermission('Pages.Receipts.Edit'),
                cancel: abp.auth.hasPermission('Pages.Receipts.Cancel'),
                request: abp.auth.hasPermission('Pages.Receipts.Request'),
                cancelAuthorization: abp.auth.hasPermission('Pages.Receipts.CancelAuthorization')
            };

           
            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [

                    {

                        //ng-if="grid.appScope.permissions.edit"
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 100,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.openReceiptEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.cancel && row.entity.cancelAuthorization!=null && row.entity.comment==null" ng-click="grid.appScope.openReceiptCancelModal(row.entity)" ><i class=\"fa fa-pencil\"></i>' + App.localize('Cancel') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.cancelAuthorization && row.entity.request!=null && row.entity.cancelAuthorization==null" ng-click="grid.appScope.openReceiptCancelAuthorizationModal(row.entity)" ><i class=\"fa fa-pencil\"></i>' + App.localize('CancelAuthorization') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.request && row.entity.request==null" ng-click="grid.appScope.openReceiptRequestModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Request') + '</a></li>' +
                        '      <li><a ng-click="grid.appScope.rePrintReceipt(row.entity)"><i class=\"fa fa-print\"></i> ' + App.localize('RePrint') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('ReceiptNumber'),
                        field: 'sequence',
                        width: 100
                    },
                    {
                        name: App.localize('ReceiptDate'),
                        field: 'date',
                        width: 110,
                        cellFilter: 'date:"dd-MM-yyyy\"'
                    },
                    {
                        name: App.localize('ReceiptName'),
                        field: 'name',
                        minWidth: 380
                    },
                    {
                        name: App.localize('ReceiptEnrollment'),
                        field: 'enrollment',
                        width: 100
                    },
                    {
                        name: App.localize('ReceiptConcept'),
                        field: 'concepts_Description',
                        minWidth: 250
                    },
                    {
                        name: App.localize('TransactionAmount'),
                        field: 'amount',
                        width: 120,
                        cellFilter: 'number:2',
                        cellClass: "font-uigrid-right",
                        headerClass: "font-uigrid-right"
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 100
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getReceipts(showTheLastPage) {
                receiptService.getAllReceipts($scope.pagination).then(function (result) {
                    vm.receipts = result.data.items;

                    $scope.gridOptions.data = vm.receipts;
                    receiptService.getAdditionalValues().then(function (result) {
                        vm.reportCode = result.data.receipReportCodeForTenant;
                    });


                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openReceiptCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/receipts/createModal.cshtml',
                    controller: 'app.views.receipts.createModal as vm',
                    size: 'lg',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getReceipts(true);
                    vm.openReceiptCreationModal();
                });
            };

            vm.openReceiptEditModal = function (receipt) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/receipts/editModal.cshtml',
                    controller: 'app.views.receipts.editModal as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        id: function () {
                            return receipt.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getReceipts(false);
                });
            };

            vm.openReceiptCancelModal = function (receipt) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/receipts/cancelModal.cshtml',
                    controller: 'app.views.receipts.cancelModal as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        id: function () {
                            return receipt.id;
                        }
                    }
                });


                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getReceipts(false);
                });
            };


            vm.openReceiptCancelAuthorizationModal = function (receipt) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/receipts/cancelAuthorizationModal.cshtml',
                    controller: 'app.views.receipts.cancelAuthorizationModal as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        id: function () {
                            return receipt.id;
                        }
                    }
                });


                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getReceipts(false);
                });
            };

            vm.openReceiptRequestModal = function (receipt) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/receipts/requestModal.cshtml',
                    controller: 'app.views.receipts.requestModal as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        id: function () {
                            return receipt.id;
                        }
                    }
                });


                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getReceipts(false);
                });
            };

            vm.delete = function (receipt) {
                abp.message.confirm(
                    "Delete receipt '" + receipt.name + "'?",
                    function (result) {
                        if (result) {
                            receiptService.delete({ id: receipt.id })
                                .then(function (result) {
                                    getReceipts(false);
                                    abp.notify.info("Deleted receipt: " + receipt.name);

                                });
                        }
                    });
            };

            vm.refresh = function () {
                getReceipts(false);
            };

            vm.printReportReceipt = function () {
                abp.message.info(App.localize("NotAvailable"));
            };

            vm.rePrintReceipt = function (receipt) {
                var objectReport = {};
                var reportsFilters = [];
                if (receipt.receiptTypeId == 1) {
                    objectReport.reportCode = "04";
                    if (vm.reportCode.length > 0)
                        objectReport.reportCode = vm.reportCode;
                }
                else if (receipt.receiptTypeId == 2)
                    objectReport.reportCode = "25";
                else if (receipt.receiptTypeId == 3)
                    objectReport.reportCode = "26";

                objectReport.reportExport = "PDF";
                reportsFilters.push({ name: "TransactionId", value: receipt.id.toString() });
                reportsFilters.push({ name: "AmountText", value: NumeroALetras(receipt.amount) });

                objectReport.reportsFilters = reportsFilters;
                report.print(objectReport);

            };

            getReceipts(false);


            try {
                if ($stateParams.origin == 'new')
                    vm.openReceiptCreationModal();
            } catch (e) { }
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.cities.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.city', 'settings',
        function ($scope, $timeout, $uibModal, cityService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getCities(false);
            }



            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.cities = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openCityEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    }
                    ,
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                    {
                        name: App.localize('CityName'),
                        field: 'name',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Province'),
                        field: 'provinces_Name',
                        minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getCities(showTheLastPage) {
                cityService.getAllCities($scope.pagination).then(function (result) {
                    vm.cities = result.data.items;

                    $scope.gridOptions.data = vm.cities;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openCityCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/cities/createModal.cshtml',
                    controller: 'app.views.cities.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getCities(true);
                });
            };

            vm.openCityEditModal = function (city) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/cities/editModal.cshtml',
                    controller: 'app.views.cities.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return city.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getCities(false);
                });
            };

            vm.delete = function (city) {
                abp.message.confirm(
                    "Delete city '" + city.name + "'?",
                    function (result) {
                        if (result) {
                            cityService.delete({ id: city.id })
                                .then(function (result) {
                                    getCities(false);
                                    abp.notify.info("Deleted city: " + city.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getCities(false);
            };

            getCities(false);
        }
    ]);
})();

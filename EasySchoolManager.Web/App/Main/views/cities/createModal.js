(function () {
    angular.module('MetronicApp').controller('app.views.cities.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.city', 'abp.services.app.province',
        function ($scope, $uibModalInstance, cityService , provinceService) {
            var vm = this;
            vm.saving = false;

            vm.city = {
                isActive: true
            };

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     cityService.create(vm.city)
                        .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.provinces = [];
            vm.getProvinces	 = function()
            {
                provinceService.getAllProvincesForCombo().then(function (result) {
                    vm.provinces = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };
			
			
			                vm.getProvinces();

		    App.initAjax();
			
			setTimeout(function () { $("#cityProvinceId").focus(); }, 100);
        }
    ]);
})();

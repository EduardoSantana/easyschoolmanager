(function () {
    angular.module('MetronicApp').controller('app.views.cities.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.city', 'id', 'abp.services.app.province',
        function ($scope, $uibModalInstance, cityService, id , provinceService) {
            var vm = this;
			vm.saving = false;

            vm.city = {
                isActive: true
            };

            var init = function () {
                cityService.get({ id: id })
                    .then(function (result) {
                        vm.city = result.data;
						
						
						                vm.getProvinces();

						App.initAjax();
						setTimeout(function () { $("#cityProvinceId").focus(); }, 100);
                    });
            }

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						cityService.update(vm.city)
						.then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.provinces = [];
            vm.getProvinces	 = function()
            {
                provinceService.getAllProvincesForCombo().then(function (result) {
                    vm.provinces = result.data;
					App.initAjax();
                });
            }


			
			

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

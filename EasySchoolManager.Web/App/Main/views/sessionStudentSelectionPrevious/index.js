(function () {
    angular.module('MetronicApp').controller('app.views.sessionStudentSelectionPrevious.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.sessionStudentSelection', 'settings', 'appSession', 'report',
        function ($scope, $timeout, $uibModal, sessionStudentSelectionService, settings, appSession, report) {
            var vm = this;
            vm.textFilter = "";


            $scope.$watch("vm.courseId", function (newValue, oldValue) {
                if (newValue != null) {
                    sessionStudentSelectionService.getSessionsByCourse({ courseId: vm.courseId }).then(function (result) {
                        vm.sessions = result.data.sessions;
                        vm.sessionId = null;
                        LoadStudents();
                    });
                }
            });

            $scope.$watch("vm.sessionId", function (newValue, oldValue) {
                LoadStudents();
            });

            vm.generateStudentSequences = function () {
                if (vm.courseId == null) {
                    abp.message.error(App.localize("YouMustSelectACourseBeforeGenerateTheSequences"));
                    return;
                }
                if (vm.sessionId == null) {
                    abp.message.error(App.localize("YouMustSelectASessionBeforeGenerateTheSequences"));
                    return;
                }

                if (vm.studentSession.length == 0) {
                    abp.message.error(App.localize("YouCanNotGenerateSequencesForASessionThatNotHaveStudents"));
                    return;
                }

                try {
                    if (vm.studentSession.filter(function (x) { return x.sequence != null && parseInt(sequence) > 0 }).length > 0) {
                        abp.message.error(App.localize("YouCanNotGenerateSequencesForASessionThatHaveAnyStudentsWithSequences"));
                        return;
                    }
                } catch (e) { }

                abp.message.confirm(
                    App.localize("AreYouShureYouWantToGenerateTheSequencesForSessionXOfTheCourseX", getNameById(vm.sessions, vm.sessionId), getNameById(vm.courses, vm.courseId)),
                    function (result) {
                        if (result) {

                            sessionStudentSelectionService.setOrderNumberForSessionInCourse({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentId: null }).then(function (result) {
                                abp.notify.info(App.localize("TheStudentsHaveBeenOrdenedSuccessfully"));
                                vm.studentCourse = result.data.studentCourse;
                                vm.studentSession = result.data.studentSession;
                                vm.studentCount = result.data.studentCount;

                            })
                        }
                    });
            }

            vm.printStudentByCourseBySession = function () {
                if (vm.courseId == null) {
                    abp.message.error(App.localize("YouMustSelectACourseBeforeGenerateTheSequences"));
                    return;
                }
                if (vm.sessionId == null) {
                    abp.message.error(App.localize("YouMustSelectASessionBeforeGenerateTheSequences"));
                    return;
                }

                if (vm.studentSession.length == 0) {
                    abp.message.error(App.localize("YouCanNotGenerateSequencesForASessionThatNotHaveStudents"));
                    return;
                }

                try {

                    var config = { headers: { 'Content-Type': 'text/plain' } };

                    var objectReport = {};
                    var reportsFilters = [];

                    objectReport.reportCode = "66";

                    objectReport.reportExport = "PDF";
                    reportsFilters.push({ name: "CourseId", value: vm.courseId });
                    reportsFilters.push({ name: "SessionId", value: vm.sessionId });
                    reportsFilters.push({ name: "TenantId", value: (appSession.tenant ? appSession.tenant.id : -1) });

                    objectReport.reportsFilters = reportsFilters;
                    report.print(objectReport);
                    abp.notify.info("Success");

                } catch (e) { }


            }



            vm.printStudentQualificationByCourseBySession = function () {

                if (vm.courseId == null) {
                    abp.message.error(App.localize("YouMustSelectACourseBefore"));
                    return;
                }
                if (vm.sessionId == null) {
                    abp.message.error(App.localize("YouMustSelectASessionBefore"));
                    return;
                }

                if (vm.studentSession.length == 0) {
                    abp.message.error(App.localize("SessionThatNotHaveStudents"));
                    return;
                }

                try {

                    var i = 0;
                    do {

                        var config = { headers: { 'Content-Type': 'text/plain' } };

                        var objectReport = {};
                        var reportsFilters = [];

                        objectReport.reportCode = "67";

                        objectReport.reportExport = "PDF";
                        reportsFilters.push({ name: "EnrollmentStudentId", value: vm.studentSession[i].id });

                        objectReport.reportsFilters = reportsFilters;
                        report.print(objectReport);
                        // abp.message.success("");
                        i++;
                    } while (i < vm.studentSession.length)
                    //  swal.close();


                } catch (e) { }


            }
            vm.printActQualificationFinal = function () {

                if (vm.courseId == null) {
                    abp.message.error(App.localize("YouMustSelectACourseBefore"));
                    return;
                }
                if (vm.sessionId == null) {
                    abp.message.error(App.localize("YouMustSelectASessionBefore"));
                    return;
                }

                if (vm.studentSession.length == 0) {
                    abp.message.error(App.localize("SessionThatNotHaveStudents"));
                    return;
                }

                try {


                    var config = { headers: { 'Content-Type': 'text/plain' } };

                    var objectReport = {};
                    var reportsFilters = [];

                    objectReport.reportCode = "111";
                    if (vm.courseId == 14 || vm.courseId == 15)
                        objectReport.reportCode = "112";

                    objectReport.reportExport = "PDF";
                    reportsFilters.push({ name: "CourseId", value: vm.courseId });
                    reportsFilters.push({ name: "SessionId", value: vm.sessionId });
                    reportsFilters.push({ name: "TenantId", value: appSession.tenant.id });

                    objectReport.reportsFilters = reportsFilters;
                    report.print(objectReport);
                    // abp.message.success("");

                    //  swal.close();


                } catch (e) { }


            }

            function LoadStudents() {
             
                if (vm.courseId == undefined) {
                    return;
                }

                if (vm.sessionId == undefined)
                    vm.sessionId = null;

                abp.ui.setBusy(null,

                    sessionStudentSelectionService.getAllStudentByCoursePrevious({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentId: null }).then(function (result) {
                        vm.studentCourse = result.data.studentCourse;
                        vm.studentSession = result.data.studentSession;
                        vm.studentCount = result.data.studentCount;
                    })
                );

            }

            function Init() {
                sessionStudentSelectionService.getInitialValues().then(function (result) {
                    vm.courses = result.data.courses;
                    vm.sessions = result.data.sessions;
                });
            }

            vm.assignStudent = function (student) {
                sessionStudentSelectionService.insertStudentInSessionAndGetAllStudentByCourse({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentId: student.id }).then(function (result) {
                    vm.studentCourse = result.data.studentCourse;
                    vm.studentSession = result.data.studentSession;
                });
            }

            vm.assignAllStudent = function () {


                abp.message.confirm(
                    App.localize("AreYouShureYouWantToInsertAlltheStudentForTheSessionXOfTheCourseX", getNameById(vm.sessions, vm.sessionId), getNameById(vm.courses, vm.courseId)),
                    function (result) {
                        if (result) {

                            vm.students = [];
                            debugger;
                            vm.studentCourse.forEach(function (student, t) {
                                vm.students.push(student.id);
                            });

                            sessionStudentSelectionService.insertAllStudentInSessionAndGetAllStudentByCourse({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentIds: vm.students }).then(function (result) {
                                vm.studentCourse = result.data.studentCourse;
                                vm.studentSession = result.data.studentSession;
                            });
                        }
                    });




            }

            vm.removeStudent = function (student) {
                debugger;
                abp.message.confirm(
                    App.localize("AreYouShureYouWantToRemoveTheStudentXFromTheSessionX", student.students.firstName + ' ' + student.students.lastName, getNameById(vm.sessions, vm.sessionId)),
                    function (result) {
                        if (result) {
                            debugger;
                            sessionStudentSelectionService.removeStudentFromSessionAndGetAllStudentByCourse({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentId: student.id }).then(function (result) {
                                vm.studentCourse = result.data.studentCourse;
                                vm.studentSession = result.data.studentSession;
                            });
                        }
                    });
            }

            vm.removeAllStudent = function () {
                debugger;
                abp.message.confirm(
                    App.localize("AreYouShureYouWantToRemoveAllTheStudentFromTheSessionX", getNameById(vm.sessions, vm.sessionId)),
                    function (result) {
                        if (result) {
                            vm.students = [];
                            debugger;
                            vm.studentSession.forEach(function (student, t) {
                                vm.students.push(student.id);
                            });

                            sessionStudentSelectionService.removeAllStudentFromSessionAndGetAllStudentByCourse({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentIds: vm.students }).then(function (result) {
                                vm.studentCourse = result.data.studentCourse;
                                vm.studentSession = result.data.studentSession;
                            });
                        }
                    });
            }

            Init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.districts.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.district','settings',
        function ($scope, $timeout, $uibModal, districtService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getDistricts(false);
            }


            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.districts = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openDistrictEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('DistrictName'),
                    field: 'name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('DistrictAbbreviation'),
                    field: 'abbreviation',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getDistricts(showTheLastPage) {
                districtService.getAllDistricts($scope.pagination).then(function (result) {
                    vm.districts = result.data.items;

                    $scope.gridOptions.data = vm.districts;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openDistrictCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/districts/createModal.cshtml',
                    controller: 'app.views.districts.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getDistricts(false);
                });
            };

            vm.openDistrictEditModal = function (district) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/districts/editModal.cshtml',
                    controller: 'app.views.districts.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return district.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getDistricts(false);
                });
            };

            vm.delete = function (district) {
                abp.message.confirm(
                    "Delete district '" + district.name + "'?",
                    function (result) {
                        if (result) {
                            districtService.delete({ id: district.id })
                                .then(function (result) {
                                    getDistricts(false);
                                    abp.notify.info("Deleted district: " + district.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getDistricts(false);
            };

            getDistricts(false);
        }
    ]);
})();

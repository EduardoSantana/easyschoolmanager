(function () {
    angular.module('MetronicApp').controller('app.views.districts.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.district', 'id', 
        function ($scope, $uibModalInstance, districtService, id ) {
            var vm = this;
			vm.saving = false;

            vm.district = {
                isActive: true
            };
            var init = function () {
                districtService.get({ id: id })
                    .then(function (result) {
                        vm.district = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#districtName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						districtService.update(vm.district)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

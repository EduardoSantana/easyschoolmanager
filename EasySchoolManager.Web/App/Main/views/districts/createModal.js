(function () {
    angular.module('MetronicApp').controller('app.views.districts.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.district', 
        function ($scope, $uibModalInstance, districtService ) {
            var vm = this;
            vm.saving = false;

            vm.district = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     districtService.create(vm.district)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#districtName").focus(); }, 100);
        }
    ]);
})();

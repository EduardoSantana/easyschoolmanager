﻿(function () {
    /* This is a generic modal that can be used to select an entity.
     * It can work with a remote service method that gets
     * PagedAndFilteredInputDto as input and returns PagedResultDto<NameValueDto> as output.
     * Example: CommonLookupAppService.FindUsers
     */
    angular.module('MetronicApp').controller('common.views.common.genericModal', [
        '$scope', '$uibModalInstance', 'uiGridConstants', 'lookupOptions',
        function ($scope, $uibModalInstance, uiGridConstants, lookupOptions) {
            var vm = this;

            vm.loading = false;
            vm.init = true;

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.currentPage = 1;
            $scope.pagination.reload = function () {
                if (!vm.init || lookupOptions.textFilter)
                vm.refreshGrid();
            }

            //Options
            vm.options = angular.extend({
                serviceMethod: null, //Required
                title: App.localize('SelectAnItem'),
                loadOnStartup: true,
                showFilter: true,
                textFilter: '',
                skipCount: 0,
                periodId: lookupOptions.periodId,
                pageSize: 10,
                callback: function (selectedItem) {
                    //This method is used to get selected item
                },
                canSelect: function (item) {
                    /* This method can return boolean or a promise which returns boolean.
                     * A false value is used to prevent selection.
                     */
                    return true;
                },
                gridOptions: {
                    enableSorting: false,
                    enableFiltering: false
                }
            }, lookupOptions);

            var colDef = [
                {
                    name: App.localize('Select'),
                    width: 110,
                    cellTemplate:
                    '<div class=\"ui-grid-cell-contents\">' +
                    '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.selectItem(row.entity)"><i class="fa fa-check"></i> ' + App.localize('Select') + '</button>' +
                    '</div>'
                }

            ];

            if (vm.options.columnDefs) {
                for (var i in vm.options.columnDefs)
                    colDef.push(vm.options.columnDefs[i]);
            } else {

                colDef.push({
                    name: App.localize('Name'),
                    field: 'name'
                });
            }

            //Check required parameters
            if (!vm.options.serviceMethod) {
                $uibModalInstance.dismiss();
                return;
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: 10,
                paginationPageSize: vm.options.pageSize,
                useExternalPagination: true,
                enableFiltering: vm.options.gridOptions.enableFiltering,
                enableSorting: vm.options.gridOptions.enableSorting,
                appScopeProvider: vm,
                columnDefs: colDef,
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                   
                },
                data: []
            };

            vm.selectItem = function (item) {
                var boolOrPromise = vm.options.canSelect(item);
                if (!boolOrPromise) {
                    return;
                }

                if (boolOrPromise === true) {
                    vm.options.callback(item);
                    $uibModalInstance.close(item);
                    return;
                }

                //assume as promise
                boolOrPromise.then(function (result) {
                    if (result) {
                        vm.options.callback(item);
                        $uibModalInstance.close(item);
                    }
                });
            };

            vm.refreshGrid = function () {
                var prms = angular.extend({
                    skipCount: ($scope.pagination.currentPage || 1) -1,
                    maxResultCount: vm.options.pageSize,
                    textFilter: vm.options.textFilter,
                    periodId: vm.options.periodId
                }, lookupOptions.extraFilters);

                vm.loading = true;
                vm.options.serviceMethod(prms).then(function (result) {
                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    vm.gridOptions.data = result.data.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }
            setTimeout(100, function () {vm.init = false; });
            
        }
    ]);

    //genericModal service
    angular.module('MetronicApp').factory('genericModal', [
        '$uibModal',
        function ($uibModal) {
            function open(lookupOptions) {
                $uibModal.open({
                    templateUrl: '~/App/Main/views/common/genericModal.cshtml',
                    controller: 'common.views.common.genericModal as vm',
                    backdrop: 'static',
                    size: lookupOptions.size || "md",
                    resolve: {
                        lookupOptions: function () {
                            return lookupOptions;
                        }
                    }
                });
            }

            return {
                open: open
            };
        }
    ]);
})();
(function () {
    angular.module('MetronicApp').controller('app.views.scoreTypes.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.scoreType', 'id', 
        function ($scope, $uibModalInstance, scoreTypeService, id ) {
            var vm = this;
			vm.saving = false;

            vm.scoreType = {
                isActive: true
            };
            var init = function () {
                scoreTypeService.get({ id: id })
                    .then(function (result) {
                        vm.scoreType = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#scoreTypeName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						scoreTypeService.update(vm.scoreType)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

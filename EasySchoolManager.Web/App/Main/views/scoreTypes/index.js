(function () {
    angular.module('MetronicApp').controller('app.views.scoreTypes.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.scoreType','settings',
        function ($scope, $timeout, $uibModal, scoreTypeService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getScoreTypes(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.scoreTypes = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openScoreTypeEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('ScoreTypeName'),
                    field: 'name',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getScoreTypes(showTheLastPage) {
                scoreTypeService.getAllScoreTypes($scope.pagination).then(function (result) {
                    vm.scoreTypes = result.data.items;

                    $scope.gridOptions.data = vm.scoreTypes;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openScoreTypeCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/scoreTypes/createModal.cshtml',
                    controller: 'app.views.scoreTypes.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getScoreTypes(false);
                });
            };

            vm.openScoreTypeEditModal = function (scoreType) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/scoreTypes/editModal.cshtml',
                    controller: 'app.views.scoreTypes.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return scoreType.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getScoreTypes(false);
                });
            };

            vm.delete = function (scoreType) {
                abp.message.confirm(
                    "Delete scoreType '" + scoreType.name + "'?",
                    function (result) {
                        if (result) {
                            scoreTypeService.delete({ id: scoreType.id })
                                .then(function (result) {
                                    getScoreTypes(false);
                                    abp.notify.info("Deleted scoreType: " + scoreType.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getScoreTypes(false);
            };

            getScoreTypes(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.verifoneBreaks.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.verifoneBreak', 'abp.services.app.bank','abp.services.app.bankAccount',
        function ($scope, $uibModalInstance, verifoneBreakService , bankService, bankAccountService) {
            var vm = this;
            vm.saving = false;

            var todayDate = getDateYMD(new Date());

            vm.verifoneBreak = {
                isActive: true,
                date: todayDate
            };


            vm.verifoneBreak.dateTemp = convertDateFormat_YMD_to_DMY(todayDate, 4);
            vm.verifoneBreak.date = todayDate;

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {

                    vm.verifoneBreak.date = convertDateFormat_DMY_to_YMD(vm.verifoneBreak.dateTemp)

                     verifoneBreakService.create(vm.verifoneBreak)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.banks = [];
            vm.getBanks	 = function()
            {
                bankService.getAllBanksForCombo().then(function (result) {
                    vm.banks = result.data;
					App.initAjax();
                });
            }

            vm.bankAccounts = [];
            vm.getBankAccounts	 = function()
            {
                bankAccountService.getAllBankAccountsForCombo().then(function (result) {
                    vm.bankAccounts = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			                vm.getBanks();
                vm.getBankAccounts();

		    App.initAjax();
			setTimeout(function () { $("#verifoneBreakSequence").focus(); }, 100);
        }
    ]);
})();

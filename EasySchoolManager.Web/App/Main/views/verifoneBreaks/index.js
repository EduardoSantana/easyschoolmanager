(function () {
    angular.module('MetronicApp').controller('app.views.verifoneBreaks.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.verifoneBreak','settings',
        function ($scope, $timeout, $uibModal, verifoneBreakService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getVerifoneBreaks(false);
            }
            vm.verifoneBreaks = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openVerifoneBreakEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        '      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                    name: App.localize('Sequence'),
                    field: 'sequence',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Date'),
                    field: 'date',
                    cellFilter: 'date:"dd-MM-yyyy\"',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Amount'),
                    field: 'amount',
                    cellFilter: 'number:2',
                    minWidth: 125
                    },
                    {
                    name: App.localize('CreditCardPercent'),
                    field: 'creditCardPercent',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Comment'),
                    field: 'comments',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getVerifoneBreaks(showTheLastPage) {
                verifoneBreakService.getAllVerifoneBreaks($scope.pagination).then(function (result) {
                    vm.verifoneBreaks = result.data.items;

                    $scope.gridOptions.data = vm.verifoneBreaks;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openVerifoneBreakCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/verifoneBreaks/createModal.cshtml',
                    controller: 'app.views.verifoneBreaks.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getVerifoneBreaks(false);
                });
            };

            vm.openVerifoneBreakEditModal = function (verifoneBreak) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/verifoneBreaks/editModal.cshtml',
                    controller: 'app.views.verifoneBreaks.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return verifoneBreak.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getVerifoneBreaks(false);
                });
            };

            vm.delete = function (verifoneBreak) {
                abp.message.confirm(
                    "Delete verifoneBreak '" + verifoneBreak.name + "'?",
                    function (result) {
                        if (result) {
                            verifoneBreakService.delete({ id: verifoneBreak.id })
                                .then(function (result) {
                                    getVerifoneBreaks(false);
                                    abp.notify.info("Deleted verifoneBreak: " + verifoneBreak.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getVerifoneBreaks(false);
            };

            getVerifoneBreaks(false);
        }
    ]);
})();

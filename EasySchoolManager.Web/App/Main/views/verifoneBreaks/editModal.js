(function () {
    angular.module('MetronicApp').controller('app.views.verifoneBreaks.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.verifoneBreak', 'id', 'abp.services.app.bank','abp.services.app.bankAccount',
        function ($scope, $uibModalInstance, verifoneBreakService, id , bankService, bankAccountService) {
            var vm = this;
			vm.saving = false;

            vm.verifoneBreak = {
                isActive: true
            };
            var init = function () {
                verifoneBreakService.get({ id: id })
                    .then(function (result) {
                        vm.verifoneBreak = result.data;

                        vm.verifoneBreak.dateTemp = convertDateFormat_YMD_to_DMY(vm.verifoneBreak.date, 4);
                        vm.getBanks();
                        vm.getBankAccounts();

						App.initAjax();
						setTimeout(function () { $("#verifoneBreakSequence").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    vm.verifoneBreak.date = convertDateFormat_DMY_to_YMD(vm.verifoneBreak.dateTemp)
						verifoneBreakService.update(vm.verifoneBreak)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.verifoneBreak.dateTemp = convertDateFormat_YMD_to_DMY(vm.verifoneBreak.date, 4);

            vm.banks = [];
            vm.getBanks	 = function()
            {
                bankService.getAllBanksForCombo().then(function (result) {
                    vm.banks = result.data;
					App.initAjax();
                });
            }

            vm.bankAccounts = [];
            vm.getBankAccounts	 = function()
            {
                bankAccountService.getAllBankAccountsForCombo().then(function (result) {
                    vm.BankAccounts = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

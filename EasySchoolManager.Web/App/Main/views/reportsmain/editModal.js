(function () {
    angular.module('MetronicApp').controller('app.views.reportsmain.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.report', 'id', '$timeout', 'abp.services.app.reportsFilter', '$uibModal', 'settings',
        function ($scope, $uibModalInstance, reportService, id, $timeout, reportsFilterService, $uibModal, settings) {
            var vm = this;
            vm.saving = false;

            vm.report = {
                isActive: true
            };
            vm.reportsTypes = [];

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                init();
            }


            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                   
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<a ng-click="grid.appScope.openReportsFilterEditModal(row.entity)" class=\"btn btn-circle btn-icon-only btn-default\" title="' + App.localize('Edit') + '"><i class=\"fa fa-pencil\"></i></a> ' +
                        '<a ng-click="grid.appScope.deleteReportsFilter(row.entity)" class=\"btn btn-circle btn-icon-only btn-default\" title="' + App.localize('Delete') + '"><i class=\"fa fa-close\"></i></a> '
                    },
                    {
                        name: App.localize('ReportsFilterTipo'),
                        field: 'tipo',
                        minWidth: 125
                    },
                    {
                        name: App.localize('ReportsFilterNombreCampo'),
                        field: 'nombreCampo',
                        minWidth: 125
                    },
                    {
                        name: App.localize('ReportsFilterNombreVisual'),
                        field: 'nombreVisual',
                        minWidth: 125
                    },
                    {
                        name: App.localize('ReportsFilterPlaceholder'),
                        field: 'placeholder',
                        minWidth: 125
                    },
                    {
                        name: App.localize('ReportsFilterOrden'),
                        field: 'orden',
                        minWidth: 125
                    }
                ]
            };

            vm.openReportsFilterCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/reportsFilters/createModal.cshtml',
                    controller: 'app.views.reportsFilters.createModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: id,
                        defatulOrden: function () {
                            return (vm.report.reportsFilters.length ? vm.report.reportsFilters.length + 10 : 10);
                        } 
                    }
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    init();
                });
            };

            vm.openReportsFilterEditModal = function (reportsFilter) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/reportsFilters/editModal.cshtml',
                    controller: 'app.views.reportsFilters.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return reportsFilter.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    init();
                });
            };

            vm.deleteReportsFilter = function (reportsFilter) {
                abp.message.confirm(
                    "Delete reportsFilter '" + reportsFilter.name + "'?",
                    function (result) {
                        if (result) {
                            reportsFilterService.delete({ id: reportsFilter.id })
                                .then(function (result) {
                                    init();
                                    abp.notify.info("Deleted reportsFilter: " + reportsFilter.name);
                                });
                        }
                    });
            }

            var init = function () {
                reportService.getAllReportsTypesForCombo().then(function (result) {
                    vm.reportsTypes = result.data;
                    reportService.get({ id: id })
                        .then(function (result) {
                            vm.report = result.data;
                            App.initAjax();
                            setTimeout(function () { $("#reportDescripcion").focus(); }, 100);
                            initGetReportsFilters(false);
                        });
                });

            }

            var initGetReportsFilters = function (showTheLastPage) {
                $scope.gridOptions.data = vm.report.reportsFilters;
                $scope.pagination.assignTotalRecords(vm.report.reportsFilters.length);
                if (showTheLastPage)
                    $scope.pagination.last();
            }

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    vm.report.nombre = vm.report.descripcion.replace('_', '');
                    reportService.update(vm.report)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

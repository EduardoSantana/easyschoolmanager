(function () {
    angular.module('MetronicApp').controller('app.views.reportsmain.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.report',
        function ($scope, $uibModalInstance, reportService) {
            var vm = this;
            vm.saving = false;
            vm.report = {
                isActive: true,
                reportRoot: "Reports\\"
            };
            vm.hostAndTenants = [
                { id: 0, name: "Just General" },
                { id: 1, name: "Just Tenants" },
                { id: 2, name: "General And Tenants" }
            ];

            if ($scope.$resolve.id) {
                reportService.get({ id: $scope.$resolve.id })
                    .then(function (result) {
                        vm.report = result.data;

                        vm.report.reportsTypes = {
                            id: vm.report.reportsTypesId,
                            nombre : vm.report.reportsTypes_Descripcion
                        };

                        vm.report.reportDataSource = {
                            id: vm.report.reportDataSourceId
                        }
                        

                    });
            }

            vm.reportDataSources = [];

            reportService.getReportDataSources().then(function (result) {
                vm.reportDataSources = result.data;
            });



            vm.reportDataSourceChange = function () {
                vm.report.reportDataSourceId = vm.report.reportDataSource.id

                if (vm.report.reportDataSource.code == "01" || vm.report.reportDataSource.code == "02") {
                    vm.getReportSource(vm.report.reportDataSource.code);
                    vm.showComboReportSource = true;
                }
                else
                    vm.showComboReportSource = false;

            }

            vm.reportsTypes = [];
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;

                var method = "create";
                if ($scope.$resolve.id)
                    method = "update";

                try {
                    reportService[method](vm.report)
                        .then(function () {
                            vm.saving = true;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            vm.getReportSource = function (code) {
                reportService.getReportSources(code).then(function (result) {
                    vm.reportSources = result.data;
                });
            }

            vm.getReportsTypes = function () {
                reportService.getAllReportsTypesForCombo().then(function (result) {
                    vm.reportsTypes = result.data;
                    App.initAjax();
                    setTimeout(function () { $("#reportDescripcion").focus(); }, 100);
                });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getReportsTypes();

            vm.nameChanged = function () {
                if (!vm.report.reportName || !vm.report.reportsTypes || vm.report.id) return;
                vm.report.reportPath = "RdlcFiles\\" + (vm.report.reportsTypes.folder || vm.report.reportsTypes.nombre ) + "\\" + vm.report.reportName +".rdlc";
            }

        }
    ]);
})();

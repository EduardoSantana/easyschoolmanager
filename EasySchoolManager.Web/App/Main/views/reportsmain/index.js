(function () {
    angular.module('MetronicApp').controller('app.views.reportsmain.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.report', 'settings',
        function ($scope, $timeout, $uibModal, reportService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getReports(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.reports = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openReportEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '      <li><a ng-click="grid.appScope.addFilters(row.entity)"><i class=\"fa fa-plus\"></i>' + App.localize('AddFilters') + '</a></li>' +
                        '      <li><a ng-click="grid.appScope.viewFilters(row.entity)"><i class=\"fa fa-eye\"></i>' + App.localize('ViewFilters') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Code'),
                        field: 'reportCode',
                        width: 75
                    },
                    {
                        name: App.localize('Name'),
                        field: 'reportName',
                        minWidth: 125
                    },

                    {
                        name: App.localize('reportsTypes_Descripcion'),
                        field: 'reportsTypes_Descripcion',
                        minWidth: 125
                    },
                  
                    {
                        name: App.localize('File'),
                        field: 'reportPath',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Query'),
                        field: 'view',
                        minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };

            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getReports(showTheLastPage) {
                reportService.getAllReports($scope.pagination).then(function (result) {
                    vm.reports = result.data.items;

                    $scope.gridOptions.data = vm.reports;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openReportCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/reportsmain/createModal.cshtml',
                    controller: 'app.views.reportsmain.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getReports(true);
                });
            };

            vm.openReportEditModal = function (report) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/reportsmain/createModal.cshtml',
                    controller: 'app.views.reportsmain.createModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return report.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getReports(false);
                });
            };

            vm.delete = function (report) {
                abp.message.confirm(
                    "Delete report '" + report.name + "'?",
                    function (result) {
                        if (result) {
                            reportService.delete({ id: report.id })
                                .then(function (result) {
                                    getReports(false);
                                    abp.notify.info("Deleted report: " + report.name);

                                });
                        }
                    });
            }

            vm.addFilters = function (report) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/reportsFilters/createModal.cshtml',
                    controller: 'app.views.reportsFilters.createModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return report.id;
                        },
                        done: function () {
                            return vm.refresh;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    getReports(true);
                });
            }

            vm.viewFilters = function(report) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/reportsFilters/index.cshtml',
                    controller: 'app.views.reportsFilters.index as vm',
                    backdrop: 'static',
                    size : 'xlg',
                    resolve: {
                        id: function () {
                            return report.id;
                        },
                        title: function () {
                            return report.reportName;
                        },
                        done: vm.refresh
                    }
                });

                modalInstance.result.then(function () {
                    getReports(true);
                });
            }

            vm.refresh = function () {
                getReports(false);
            };

            getReports(false);
        }
    ]);
})();

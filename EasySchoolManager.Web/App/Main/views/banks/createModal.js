(function () {
    angular.module('MetronicApp').controller('app.views.banks.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.bank', 
        function ($scope, $uibModalInstance, bankService ) {
            var vm = this;
            vm.saving = false;

            vm.bank = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     bankService.create(vm.bank)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#bankName").focus(); }, 100);
        }
    ]);
})();

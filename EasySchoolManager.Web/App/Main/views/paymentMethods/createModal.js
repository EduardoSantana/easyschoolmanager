(function () {
    angular.module('MetronicApp').controller('app.views.paymentMethods.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.paymentMethod', 
        function ($scope, $uibModalInstance, paymentMethodService ) {
            var vm = this;
            vm.saving = false;

            vm.paymentMethod = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     paymentMethodService.create(vm.paymentMethod)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#paymentMethodName").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.paymentMethods.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.paymentMethod', 'id', 
        function ($scope, $uibModalInstance, paymentMethodService, id ) {
            var vm = this;
			vm.saving = false;

            vm.paymentMethod = {
                isActive: true
            };
            var init = function () {
                paymentMethodService.get({ id: id })
                    .then(function (result) {
                        vm.paymentMethod = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#paymentMethodName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						paymentMethodService.update(vm.paymentMethod)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.courseValues.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.courseValue', 'id', 'abp.services.app.course','period',
        function ($scope, $uibModalInstance, courseValueService, id , courseService, period) {
            var vm = this;
			vm.saving = false;

            vm.courseValue = {
                periodId: period.id,
                period: { description: period.description },
                isActive: true
            };
            var init = function () {
                courseValueService.get({ id: id })
                    .then(function (result) {
                        vm.courseValue = result.data;
						                vm.getCourses();

						App.initAjax();
						setTimeout(function () { $("#courseValuePeriodId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						courseValueService.update(vm.courseValue)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.courses = [];
            vm.getCourses	 = function()
            {
                courseService.getAllCoursesForCombo().then(function (result) {
                    vm.courses = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

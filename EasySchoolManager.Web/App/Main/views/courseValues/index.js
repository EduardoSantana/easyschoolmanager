(function () {
    angular.module('MetronicApp').controller('app.views.courseValues.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.courseValue', 'settings', 'abp.services.app.period',
        function ($scope, $timeout, $uibModal, courseValueService, settings, periodService) {
            var vm = this;
            vm.textFilter = "";
            vm.currentPeriod = { id: 0, description: App.localize("AllPeriods") };

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getCourseValues(false);
            }


            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.courseValues = [];



            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openCourseValueEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },

                    {
                        name: App.localize('CourseValuePeriod'),
                        field: 'periods_Description',
                        minWidth: 125
                    },
                    {
                        name: App.localize('CourseValueCourse'),
                        field: 'courses_Name',
                        minWidth: 125
                    },
                    {
                        name: App.localize('CourseValueLevel'),
                        field: 'courses_Levels_Name',
                        minWidth: 125
                    },
                    {
                        name: App.localize('CourseValueInscriptionAmount'),
                        field: 'inscriptionAmount',
                        minWidth: 125,
                        cellFilter: 'number:2'
                    },
                    {
                        name: App.localize('CourseValueTotalAmount'),
                        field: 'totalAmount',
                        minWidth: 125,
                        cellFilter: 'number:2'
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };

            $scope.$watch("vm.currentPeriod", function (newValue, oldValue) {
                $scope.pagination.periodId = newValue.id;
                getCourseValues(false);
            });

            vm.periods = [];
            vm.getPeriods = function () {
                periodService.getAllPeriodsForCombo().then(function (result) {
                    vm.periods = result.data;
                    vm.periods.unshift({ id: 0, description: App.localize("AllPeriods") });
                    App.initAjax();
                });
            }

            function getCurrentPeriodById(periodId)
            {
                var resultedValue = null;
                if (vm.periods.length == 0)
                    return resultedValue;

                for (var i = 0; i < vm.periods.length; i++) {
                    var x = vm.periods[i];
                    if (x.id === periodId)
                    {
                        resultedValue = x;
                        break;
                    }
                }

                return resultedValue;
                //return vm.periods.filter(function (x) { x.id == periodId });
            }



            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getCourseValues(showTheLastPage) {
                courseValueService.getAllCourseValues($scope.pagination).then(function (result) {
                    vm.courseValues = result.data.items;

                    $scope.gridOptions.data = vm.courseValues;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }



            vm.openCourseValueCreationModal = function () {
                if (vm.currentPeriod == undefined || vm.currentPeriod == null || vm.currentPeriod.id === 0) {
                    abp.message.error(App.localize("YouMustSelectAPeriodBeforeAssignValuesToACourse"))
                    return;
                }

                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/courseValues/createModal.cshtml',
                    controller: 'app.views.courseValues.createModal as vm',
                    backdrop: 'static',
                    resolve: {
                        period: function () {
                            return vm.currentPeriod;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getCourseValues(true);
                });
            };

            vm.openCourseValueEditModal = function (courseValue) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/courseValues/editModal.cshtml',
                    controller: 'app.views.courseValues.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return courseValue.id;
                        },
                        period: function () {
                            return vm.currentPeriod;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getCourseValues(false);
                });
            };

            vm.delete = function (courseValue) {
                abp.message.confirm(
                    "Delete courseValue '" + courseValue.name + "'?",
                    function (result) {
                        if (result) {
                            courseValueService.delete({ id: courseValue.id })
                                .then(function (result) {
                                    getCourseValues(false);
                                    abp.notify.info("Deleted courseValue: " + courseValue.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getCourseValues(false);
            };

            getCourseValues(false);
            vm.getPeriods();
        }
    ]);
})();

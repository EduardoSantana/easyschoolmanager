(function () {
    angular.module('MetronicApp').controller('app.views.clientShops.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.clientShop', 
        function ($scope, $uibModalInstance, clientShopService ) {
            var vm = this;
            vm.saving = false;

            vm.clientShop = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     clientShopService.create(vm.clientShop)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#clientShopName").focus(); }, 100);
        }
    ]);
})();

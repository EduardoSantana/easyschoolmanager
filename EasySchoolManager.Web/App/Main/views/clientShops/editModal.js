(function () {
    angular.module('MetronicApp').controller('app.views.clientShops.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.clientShop', 'id', 
        function ($scope, $uibModalInstance, clientShopService, id ) {
            var vm = this;
			vm.saving = false;

            vm.clientShop = {
                isActive: true
            };
            var init = function () {
                clientShopService.get({ id: id })
                    .then(function (result) {
                        vm.clientShop = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#clientShopName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						clientShopService.update(vm.clientShop)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.catalogs.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.catalog','settings',
        function ($scope, $timeout, $uibModal, catalogService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getCatalogs(false);
            }


            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.catalogs = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openCatalogEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                    {
                    name: App.localize('CatalogAccountNumber'),
                    field: 'accountNumber',
                    minWidth: 125
                    },
                    {
                    name: App.localize('CatalogName'),
                    field: 'name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('CatalogDescsription'),
                    field: 'descsription',
                    minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getCatalogs(showTheLastPage) {
                catalogService.getAllCatalogs($scope.pagination).then(function (result) {
                    vm.catalogs = result.data.items;

                    $scope.gridOptions.data = vm.catalogs;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openCatalogCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/catalogs/createModal.cshtml',
                    controller: 'app.views.catalogs.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getCatalogs(true);
                });
            };

            vm.openCatalogEditModal = function (catalog) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/catalogs/editModal.cshtml',
                    controller: 'app.views.catalogs.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return catalog.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getCatalogs(false);
                });
            };

            vm.delete = function (catalog) {
                abp.message.confirm(
                    "Delete catalog '" + catalog.name + "'?",
                    function (result) {
                        if (result) {
                            catalogService.delete({ id: catalog.id })
                                .then(function (result) {
                                    getCatalogs(false);
                                    abp.notify.info("Deleted catalog: " + catalog.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getCatalogs(false);
            };

            getCatalogs(false);
        }
    ]);
})();

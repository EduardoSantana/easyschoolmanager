(function () {
    angular.module('MetronicApp').controller('app.views.catalogs.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.catalog', 'id', 
        function ($scope, $uibModalInstance, catalogService, id ) {
            var vm = this;
			vm.saving = false;

            vm.catalog = {
                isActive: true
            };
            var init = function () {
                catalogService.get({ id: id })
                    .then(function (result) {
                        vm.catalog = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#catalogAccountNumber").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						catalogService.update(vm.catalog)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

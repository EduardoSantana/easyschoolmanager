(function () {
    angular.module('MetronicApp').controller('app.views.catalogs.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.catalog', 
        function ($scope, $uibModalInstance, catalogService ) {
            var vm = this;
            vm.saving = false;

            vm.catalog = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     catalogService.create(vm.catalog)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#catalogAccountNumber").focus(); }, 100);
        }
    ]);
})();

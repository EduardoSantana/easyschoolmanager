(function () {
    angular.module('MetronicApp').controller('app.views.evaluationHighEnters.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.sessionStudentSelection', 'settings',
        function ($scope, $timeout, $uibModal, sessionStudentSelectionService, settings) {
            var vm = this;
            vm.textFilter = "";


      //      $scope.$watch("vm.courseId", function (newValue, oldValue) {
     //           if (newValue != null) {
     //               sessionStudentSelectionService.getSessionsByCourse({ courseId: vm.courseId }).then(function (result) {
    //                    vm.sessions = result.data.sessions;
    //                    vm.sessionId = null;
   //                     LoadStudents();
   //                 });
  //              }
  //         });

            $scope.$watch("vm.sessionId", function (newValue, oldValue) {
                LoadStudents();
            });

            vm.generateStudentSequences = function () {
                
                if (vm.courseId == null) {
                    abp.message.error(App.localize("YouMustSelectACourseBeforeGenerateTheSequences"));
                    return;
                }
                if (vm.sessionId == null) {
                    abp.message.error(App.localize("YouMustSelectASessionBeforeGenerateTheSequences"));
                    return;
                }

                if (vm.studentSession.length == 0) {
                    abp.message.error(App.localize("YouCanNotGenerateSequencesForASessionThatNotHaveStudents"));
                    return;
                }

                try {
                    if (vm.studentSession.filter(function (x) { return x.sequence != null && parseInt(sequence) > 0 }).length > 0) {
                        abp.message.error(App.localize("YouCanNotGenerateSequencesForASessionThatHaveAnyStudentsWithSequences"));
                        return;
                    }
                } catch (e) { }

                abp.message.confirm(
                    App.localize("AreYouShureYouWantToGenerateTheSequencesForSessionXOfTheCourseX", getNameById(vm.sessions, vm.sessionId), getNameById(vm.courses, vm.courseId)),
                    function (result) {
                        if (result) {

                            sessionStudentSelectionService.setOrderNumberForSessionInCourse({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentId: null }).then(function (result) {
                                abp.notify.info(App.localize("TheStudentsHaveBeenOrdenedSuccessfully"));
                                vm.studentCourse = result.data.studentCourse;
                                vm.studentSession = result.data.studentSession;
                                vm.studentCount = result.data.studentCount;

                            })
                        }
                    });
            }

            function LoadStudents() {

                if (vm.courseId == undefined) {
                    return;
                }

                if (vm.sessionId == undefined)
                    vm.sessionId = null;

                abp.ui.setBusy(null,

                    sessionStudentSelectionService.getAllStudentByCourse({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentId: null }).then(function (result) {
                        vm.studentCourse = result.data.studentCourse;
                        vm.studentSession = result.data.studentSession;
                        vm.studentCount = result.data.studentCount;
                    })
                );

            }

            function Init() {
                sessionStudentSelectionService.getInitialValues().then(function (result) {
                    vm.courses = result.data.courses;
                    vm.sessions = result.data.sessions;
                });
            }

            vm.assignStudent = function (student) {
                sessionStudentSelectionService.insertStudentInSessionAndGetAllStudentByCourse({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentId: student.id }).then(function (result) {
                    vm.studentCourse = result.data.studentCourse;
                    vm.studentSession = result.data.studentSession;
                });
            }


            vm.removeStudent = function (student) {
                abp.message.confirm(
                    App.localize("AreYouShureYouWantToRemoveTheStudentXFromTheSessionX", student.students.firstName + ' ' + student.students.lastName, getNameById(vm.sessions, vm.sessionId)),
                    function (result) {
                        if (result) {
                                sessionStudentSelectionService.removeStudentFromSessionAndGetAllStudentByCourse({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentId: student.id }).then(function (result) {
                                vm.studentCourse = result.data.studentCourse;
                                vm.studentSession = result.data.studentSession;
                            });
                        }
                    });
            }

            Init();
        }
    ]);
})();

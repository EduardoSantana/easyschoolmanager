(function () {
    angular.module('MetronicApp').controller('app.views.roles.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.role', 'id', 
        function ($scope, $uibModalInstance, roleService, id ) {
            var vm = this;

            vm.role = {
                isActive: true
            };
            vm.permissions = [];


            /**
             * Asigna un valor logico de verdadero a todos los permisos que han sido asignados.
             * @param {any} role
             * @param {any} permissions
             */
            var setAssignedPermissions = function (role, permissions) {
                for (var i = 0; i < permissions.length; i++) {
                    var permission = permissions[i];
                    permission.isAssigned = $.inArray(permission.name, role.permissions) >= 0;
                }
            }

            vm.getParentName = function (optionUsed) {
                var returnedValue = "";
                var itemindex = optionUsed.indexOf(".", 6);
                if (itemindex > 5)
                    returnedValue = optionUsed.substring(0, itemindex);
                return returnedValue;
            }


            /**
             * Asigna los permisos hijos a todos los permisos que lo poseean
             * @param {any} permissions Listado de permisos de la base de datos.
             */
            var setChildrenPermissions = function (permissions) {
                var sec = 1;
                permissions.forEach(function (ele) {
                    ele.children = getChildrenPermission(ele, permissions);
                    ele.hasChildren = false;
                    ele.visible = false;
                    ele.index = sec++;
                    if (ele.children.length > 0)
                        ele.hasChildren = true;

                });
            }


            /**
             * Asigna el padre a todos los permisos que sean hijos de algun permiso en particular
             * @param {any} permissions Listado de permisos de la base de datos.
             */
            var setParentPermissions = function (permissions) {
                permissions.forEach(function (ele) {
                    ele.parent = getParentPermission(ele, permissions);
                });
            }

            /**
             * Obtiene todos los permisos hijos de un permiso en particular
             * @param {any} permission Permiso al cual se le pretenden buscar los hijos.
             * @param {any} permissions Listado de permisos de la base de datos.
             */
            var getChildrenPermission = function (permission, permissions) {
                var childrenPermissions = [];

                for (var i = 0; i < permissions.length; i++) {
                    var perm = permissions[i];
                    if (perm.name.indexOf(permission.name + ".") > - 1 && perm.name != permission.name)
                        childrenPermissions.push(perm);
                }
                return childrenPermissions;
            }

            /**
             * Obtiene el nombre del permiso padre de un permiso en particular
             * @param {any} permission Permiso al cual se le quiere buscar su padre
             * @param {any} permissions Listado de permisos de la base de datos.
             */
            var getParentPermission = function (permission, permissions) {
                var parentPermission = null;

                for (var i = 0; i < permissions.length; i++) {
                    var perm = permissions[i];
                    if (permission.name.indexOf(perm.name + ".") > - 1 && perm.name != permission.name) {
                        parentPermission = perm.name;
                        break;
                    }
                }
                return parentPermission;
            }
            

            /**
             * Obtiene solo los permisos principales, con la intencion de crear el albor de permisos a partir de los padres
             * @param {any} permissions Listado de permisos de la base de datos
             */
            var getRootPermissions = function (permissions)
            {
                var rootPermissions = [];

                for (var i = 0; i < permissions.length; i++) {
                    var perm = permissions[i];
                    if (perm.parent == null)
                        rootPermissions.push(perm);
                }
                return rootPermissions; 
            }



            var init = function () {
                roleService.getAllPermissions()
                    .then(function (result) {
                        vm.permissions = result.data.items;
                        
                    }).then(function () {
                        roleService.get({ id: id })
                            .then(function (result) {
                                vm.role = result.data;

                                setAssignedPermissions(vm.role, vm.permissions);

                                setChildrenPermissions(vm.permissions);
                                setParentPermissions(vm.permissions);
                                vm.rootPermissions = getRootPermissions(vm.permissions);

                                vm.rootPermissions = sortArrayByKey(vm.rootPermissions, "name");

                                App.initAjax();
                                setTimeout(function () { $("#rolename").focus(); }, 100);
                            });
                    });
            }


            vm.save = function () {

                var assignedPermissions = [];

                for (var i = 0; i < vm.permissions.length; i++) {
                    var permission = vm.permissions[i];
                    if (!permission.isAssigned) {
                        continue;
                    }

                    assignedPermissions.push(permission.name);
                }

                vm.role.permissions = assignedPermissions;

                roleService.update(vm.role)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

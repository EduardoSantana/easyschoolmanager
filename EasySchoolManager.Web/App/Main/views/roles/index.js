(function () {
    angular.module('MetronicApp').controller('app.views.roles.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.role', 'settings',
        function ($scope, $timeout, $uibModal, roleService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getRoles(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.roles = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openRoleEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Name'),
                        field: 'name',
                        minWidth: 125
                    },
                    {
                        name: App.localize('DisplayName'),
                        field: 'displayName',
                        minWidth: 125
                    }
                ]
            };

            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getRoles(showTheLastPage) {
                roleService.getAll($scope.pagination).then(function (result) {
                    vm.roles = result.data.items;

                    $scope.gridOptions.data = vm.roles;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openRoleCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/roles/createModal.cshtml',
                    controller: 'app.views.roles.createModal as vm',
                    backdrop: 'static',
                    size: 'lg'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getRoles(true);
                });
            };

            vm.openRoleEditModal = function (role) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/roles/editModal.cshtml',
                    controller: 'app.views.roles.editModal as vm',
                    backdrop: 'static',
                    size:'lg',
                    resolve: {
                        id: function () {
                            return role.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getRoles(false);
                });
            };

            vm.delete = function (role) {
                abp.message.confirm(
                    "Delete role '" + role.name + "'?",
                    function (result) {
                        if (result) {
                            roleService.delete({ id: role.id })
                                .then(function (result) {
                                    getRoles(false);
                                    abp.notify.info("Deleted role: " + role.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getRoles(false);
            };

            getRoles(false);
        }
    ]);
})();

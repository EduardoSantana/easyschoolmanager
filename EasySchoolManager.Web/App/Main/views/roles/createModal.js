(function () {
    angular.module('MetronicApp').controller('app.views.roles.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.role',
        function ($scope, $uibModalInstance, roleService) {
            var vm = this;

            vm.role = {
                isActive: true
            };
            vm.permissions = [];

            function getPermissions() {
                roleService.getAllPermissions()
                    .then(function (result) {
                        vm.permissions = result.data.items;


                        setChildrenPermissions(vm.permissions);
                        setParentPermissions(vm.permissions);
                        vm.rootPermissions = getRootPermissions(vm.permissions);

                        vm.rootPermissions = sortArrayByKey(vm.rootPermissions, "name");

                        var valor = vm.getParentName("Pages.Enrollments.ChangeDate");

                    });
            }

            /**
            * Asigna los permisos hijos a todos los permisos que lo poseean
            * @param {any} permissions Listado de permisos de la base de datos.
            */
            var setChildrenPermissions = function (permissions) {
                var sec = 1;
                permissions.forEach(function (ele) {
                    ele.children = getChildrenPermission(ele, permissions);
                    ele.hasChildren = false;
                    ele.visible = false;
                    ele.index = sec++;
                    if (ele.children.length > 0)
                        ele.hasChildren = true;

                });
            }


            /**
             * Asigna el padre a todos los permisos que sean hijos de algun permiso en particular
             * @param {any} permissions Listado de permisos de la base de datos.
             */
            var setParentPermissions = function (permissions) {
                permissions.forEach(function (ele) {
                    ele.parent = getParentPermission(ele, permissions);
                });
            }

            /**
             * Obtiene todos los permisos hijos de un permiso en particular
             * @param {any} permission Permiso al cual se le pretenden buscar los hijos.
             * @param {any} permissions Listado de permisos de la base de datos.
             */
            var getChildrenPermission = function (permission, permissions) {
                var childrenPermissions = [];

                for (var i = 0; i < permissions.length; i++) {
                    var perm = permissions[i];
                    if (perm.name.indexOf(permission.name + ".") > - 1 && perm.name != permission.name)
                        childrenPermissions.push(perm);
                }
                return childrenPermissions;
            }

            /**
             * Obtiene el nombre del permiso padre de un permiso en particular
             * @param {any} permission Permiso al cual se le quiere buscar su padre
             * @param {any} permissions Listado de permisos de la base de datos.
             */
            var getParentPermission = function (permission, permissions) {
                var parentPermission = null;

                for (var i = 0; i < permissions.length; i++) {
                    var perm = permissions[i];
                    if (permission.name.indexOf(perm.name + ".") > - 1 && perm.name != permission.name) {
                        parentPermission = perm.name;
                        break;
                    }
                }
                return parentPermission;
            }


            /**
             * Obtiene solo los permisos principales, con la intencion de crear el albor de permisos a partir de los padres
             * @param {any} permissions Listado de permisos de la base de datos
             */
            var getRootPermissions = function (permissions) {
                var rootPermissions = [];

                for (var i = 0; i < permissions.length; i++) {
                    var perm = permissions[i];
                    if (perm.parent == null)
                        rootPermissions.push(perm);
                }
                return rootPermissions;
            }


            vm.getParentName = function (optionUsed) {
                var returnedValue = "";
                var itemindex = optionUsed.indexOf(".", 6);
                if (itemindex > 5)
                    returnedValue = optionUsed.substring(0, itemindex);
                return returnedValue;
            }


            vm.save = function () {
                vm.saving = true;
                var assignedPermissions = [];
                for (var i = 0; i < vm.permissions.length; i++) {
                    var permission = vm.permissions[i];
                    if (!permission.isAssigned) {
                        continue;
                    }

                    assignedPermissions.push(permission.name);
                }

                vm.role.permissions = assignedPermissions;

                try {
                    roleService.create(vm.role)
                        .then(function () {
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            //XXXInsertCallRelatedEntitiesXXX

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            getPermissions();

            App.initAjax();

            setTimeout(function () { $("#rolename").focus(); }, 100);
        }
    ]);
})();

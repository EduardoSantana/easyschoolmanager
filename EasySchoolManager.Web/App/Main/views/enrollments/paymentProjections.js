(function () {
    angular.module('MetronicApp').controller('app.views.enrollments.paymentProjections', [
        '$scope', '$uibModal', '$uibModalInstance', 'abp.services.app.enrollment', 'enrollment', 'appSession', 'settings', 'periodId', 'report',
        function ($scope, $uibModal, $uibModalInstance, enrollmentService, enrollment, appSession, settings, periodId, report) {
          
            var vm = this;
            vm.saving = false;

            vm.enrollment = enrollment;
            vm.paymentProjectionAlreadyCreated = false;
            vm.paymentProjectionAlreadyCharged = false;

            vm.enrollment.numberOfPayment = 10;
            vm.numberOfPaymentFocused = false;

            vm.totalInscriptions = 0.00;
            vm.totalPayments = 0.00;
            vm.totalAmount = 0.00;

            function calculateTotals() {
                vm.totalInscriptions = 0.00;
                vm.totalPayments = 0.00;
                vm.totalAmount = 0.00;

                for (var i = 0; i < vm.enrollment.enrollmentStudents.length; i++) {
                    var currentES = vm.enrollment.enrollmentStudents[i];
                    vm.totalInscriptions += currentES.students.inscriptionAmount;
                    vm.totalPayments += currentES.students.totalAmount;
                }
                vm.totalAmount = vm.totalInscriptions + vm.totalPayments;
            }


            vm.save = function () {
                debugger;
                if (vm.saving === true)
                    return;
                vm.saving = true;

                if (!validateForm()) {
                    vm.saving = false;
                    return;
                }

                var paymentProjectionsTmp = [];

                for (var i = 0; i < vm.enrollment.enrollmentStudents.length; i++) {
                    var pptmp = vm.enrollment.enrollmentStudents[i];

                    for (var j = 0; j < pptmp.paymentProjections.length; j++) {
                        var ppStudent = pptmp.paymentProjections[j];
                        paymentProjectionsTmp.push(ppStudent);

                    }
                }


                try {
                    debugger;
                    var dataToSave = {
                        enrollmentId: vm.enrollment.id,
                        paymentProjections: paymentProjectionsTmp
                    };

                    enrollmentService.savePaymentProyection(dataToSave)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            vm.students = [];


            $scope.gridOptions = {
                rowHeight: 39,
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '    <button title="' + App.localize("StudentProyections") + '" class="btn btn-xs btn-default gray fa-grid-icon1" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.openStudentPaymentsModal(row.entity)"><i class="fa fa-industry" style=\"color:blue\"></i></button>' +
                        '    <button title="' + App.localize("PrintStudentProyections") + '" class="btn btn-xs btn-default gray fa-grid-icon1" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.printStudentPayment(row.entity)"><i class="fa fa-print" style=\"color:blue\"></i></button>' +
                        '</div>'
                    },

                    {
                        name: App.localize('Student'),
                        field: 'students.fullName',
                        minWidth: 300
                    },
                    {
                        name: App.localize('Course'),
                        field: 'students.courseDescription',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Inscription'),
                        field: 'students.inscriptionAmount',
                        minWidth: 125,
                        cellFilter: 'number'
                    },
                    {
                        name: App.localize('TotalAmount'),
                        field: 'students.totalAmount',
                        minWidth: 125,
                        cellFilter: 'number'
                    }

                ]
            };


            function validateForm() {
                var valid = false;
                try {
                    if ((!vm.enrollment.numberOfPayment) || vm.enrollment.numberOfPayment < 1) {
                        abp.message.error(App.localize("YouMustSetAValidNumberOfPayment"));
                    }
                    else valid = true;

                } catch (e) {
                    console.log(e);
                    abp.message.error(JSON.stringify(e));
                }

                return valid;
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            App.initAjax();

            setTimeout(function () { $("#enrollmentNewEnrollmentId").focus(); }, 100);

            var init = function () {
                    enrollmentService.getEnrollmentPaymentProyection({
                    enrollmentId: enrollment.id,
                    tenantId: appSession.tenant.id,
                    periodId: null

                }).then(function (result) {
                    vm.enrollment = $.extend(vm.enrollment, result.data.enrollment);
                    if (vm.enrollment.tutorPaymentProjection && vm.enrollment.tutorPaymentProjection.length > 0)
                        vm.enrollment.numberOfPayment = vm.enrollment.tutorPaymentProjection[vm.enrollment.tutorPaymentProjection.length - 1].sequence;
               
                    vm.paymentProjectionAlreadyCreated = result.data.hasAlreadyPaymentProjections;

                    getStudents();

                    vm.getChargedjb();

                    calculateTotals();

                 
                });


                App.initAjax();

                setTimeout(function () { $("input").popover({ trigger: "hover", placement: "top" }); }, 1000);

            }

            function getStudents() {
                $scope.gridOptions.data = vm.enrollment.enrollmentStudents;

            }


            
            vm.getChargedjb = function () {
                enrollmentService.getCharged().then(function (result) {
                    vm.paymentProjectionAlreadyCharged = result.data;
                    App.initAjax();
                });
            }



            reemplazeDateToSend = function (enrollment) {
                if (enrollment.tutorPaymentProjection)
                    for (var i = 0; i < enrollment.tutorPaymentProjection.length; i++) {
                        var rec = enrollment.tutorPaymentProjection[i];
                        if (rec.date && rec.date.toString().indexOf("GMT") < 0)
                            break;
                        else if (!rec.date) {
                            rec.date = new Date();
                        }
    
                        rec.date = getDateYMD(rec.date);
                    }
            }

            vm.configuratePayments = function (enrollment) {

                reemplazeDateToSend(enrollment);

                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/enrollments/configuratePayments.cshtml',
                    controller: 'app.views.enrollments.configuratePayments as vm',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        enrollment: function () {
                            return enrollment;
                        }
                    }
                })

                modalInstance.result.then(function (result) {
                    vm.enrollment = result;
                    getStudents();
                    calculateTotals();
                });
            };

            vm.openStudentPaymentsModal = function (student) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/enrollments/configurateStudentPayments.cshtml',
                    controller: 'app.views.enrollments.configurateStudentPayments as vm',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        enrollment: function () {
                            return vm.enrollment;
                        },
                        student: function () {
                            return student;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.enrollment = result;
                    calculateTotals();
                });
            }

            vm.printStudentPayment = function (student) {
               // debugger;
                var objectReport = {};
                var reportsFilters = [];
                objectReport.reportCode = "64";
                objectReport.reportExport = "PDF";
                reportsFilters.push({ name: "StudentId", value: student.studentId });
                reportsFilters.push({ name: "PeriodId", value: student.periodId });
                objectReport.reportsFilters = reportsFilters;
                report.print(objectReport);
                abp.message.success("");
            }

            init();
        }

    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.enrollments.querybyStudent.index', [
        '$scope', '$timeout', '$uibModal', '$uibModalInstance', 'abp.services.app.enrollment', 'settings',
        function ($scope, $timeout, $uibModal, $uibModalInstance, enrollmentService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getStudents(false);
            }


            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.students = [];

            $scope.gridOptions = {
                rowHeight: 39,
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 90,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '    <button class="btn btn-xs btn-default gray fa-grid-icon1" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.selectEnrollment(row.entity)"><i class="fa fa-angle-down"></button>' +
                     //   '    <button class="btn btn-xs btn-default gray fa-grid-icon1 red" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.delete(row.entity)"><i class="fa fa-close"></i></button>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Enrollment'),
                        field: 'matricula',
                        width: 95
                    },
                    {
                        name: App.localize('EnrollmentName'),
                        field: 'enrollmentName',
                        minWidth: 200
                    },
                    {
                        name: App.localize('StudentName'),
                        field: 'studentName',
                        minWidth: 200
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getStudents(showTheLastPage) {
                enrollmentService.getEnrollmentsByStudentName($scope.pagination).then(function (result) {
                    vm.students = result.data.enrollmentStudents;

                    $scope.gridOptions.data = vm.students;

                     $scope.pagination.assignTotalRecords(result.data.totalCount);
                });
            }


            vm.selectEnrollment = function (enrollment) {
               
                $uibModalInstance.close(enrollment);
            };
            
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.refresh = function () {
                getStudents(false);
            };

            getStudents(false);

            setTimeout(function () {
                document.getElementById("txtSearch").focus();
            }, 1000)
        }
    ]);
})();

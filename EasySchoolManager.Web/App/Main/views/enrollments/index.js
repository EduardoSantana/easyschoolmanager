(function () {
    angular.module('MetronicApp').controller('app.views.enrollments.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.enrollment', 'settings', '$stateParams',
        function ($scope, $timeout, $uibModal, enrollmentService, settings, $stateParams) {
            var vm = this;
            vm.textFilter = "";
            vm.prevPeriod = false;
            vm.nePeriod = false;


            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.previousPeriod = false;


            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            $scope.pagination.nextPeriod = false;
            $scope.pagination.reload = function () {
                getEnrollments(false);
            }
            vm.enrollments = [];

            vm.permissions = {
                change: abp.auth.hasPermission('Pages.Enrollments.ChangePersonalId'),
                create: abp.auth.hasPermission('Pages.Enrollments.Create'),
                edit: abp.auth.hasPermission('Pages.Enrollments.Edit'),
                addStudent: abp.auth.hasPermission('Pages.Enrollments.AddStudent'),
                assignPaymentProyection: abp.auth.hasPermission('Pages.Enrollments.AssignPaymentProyection'),
                viewStudents: abp.auth.hasPermission('Pages.Enrollments.ViewStudents'),
                delete: abp.auth.hasPermission('Pages.Enrollments.Delete')
            };

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.openEnrollmentEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-user-times\" style=\"color:red;\"></i>' + App.localize('Delete') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.addStudent" ng-click="grid.appScope.addStudent(row.entity)"><i class=\"fa fa-child\" style=\"color:blue;\"></i>' + App.localize('AddStudent') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.viewStudents" ng-click="grid.appScope.viewStudents(row.entity)" ng-if="row.entity.numberOfStudents > 0"><i class=\"fa fa-users\" style=\"color:blue;\"></i>' + App.localize('ViewStudentList') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.change" ng-click="grid.appScope.changeEnrollmentId(row.entity)" ><i class=\"fa fa-exchange\" style=\"color:orange;\"></i>' + App.localize('ChangeEnrollmentPersonalId') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.assignPaymentProyection" ng-click="grid.appScope.generatePaymentProjection(row.entity)" ><i class=\"fa fa-calculator\" style=\"color:blue;\"></i>' + App.localize('AssignPaymentProyection') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('PersonalId'),
                        field: 'id',
                        width: 130
                    },
                    {
                        name: App.localize('EnrollmentEnrollment'),
                        field: 'enrollment',
                        width: 120
                    },
                    {
                        name: App.localize('EnrollmentFirstName'),
                        field: 'fullName',
                        minWidth: 160
                    },
                    {
                        name: App.localize('Students'),
                        field: 'studentsNames',
                        minWidth: 160
                    },
                    {
                        name: App.localize('Qty'),
                        field: 'numberOfStudents',
                        width: 120
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getEnrollments(showTheLastPage) {
                enrollmentService.getAllEnrollments($scope.pagination).then(function (result) {
                    vm.enrollments = result.data.items;
                    replacePersonalIdWithStringFormat();

                    $scope.gridOptions.data = vm.enrollments;


                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();

                    setTimeout(function () { $scope.$apply(); }, 2000)
                });
            }


            vm.openEnrollmentCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/enrollments/createModal.cshtml',
                    controller: 'app.views.enrollments.createModal as vm',
                    size: 'lg',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getEnrollments(true);
                    vm.openEnrollmentCreationModal();
                });
            };

            vm.openEnrollmentEditModal = function (enrollment) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/enrollments/editModal.cshtml',
                    controller: 'app.views.enrollments.editModal as vm',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return enrollment.id;
                        }
                    }
                });


                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getEnrollments(false);
                });
            };





            vm.addStudent = function (enrollment) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/students/createModal.cshtml',
                    controller: 'app.views.students.createModal as vm',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        id: 0,
                        enrollment: function () {
                            return enrollment;
                        }
                    }
                })

                modalInstance.result.then(function () {
                    getEnrollments(false);
                });
            };


            /*
                        vm.generatePaymentProyection = function (enrollment) {
                            var modalInstance = $uibModal.open({
                                templateUrl: settings.settings.appPathNotHTTP + '/views/enrollments/paymentProjections.cshtml',
                                controller: 'app.views.enrollments.paymentProyections as vm',
                                size: 'lg',
                                backdrop: 'static',
                                resolve: {
                                    enrollment: function () {
                                        return $.extend({}, enrollment);
                                    }
                                }
                            })
            
                            modalInstance.result.then(function () {
                                getEnrollments(false);
                            });
                        };
            */

            vm.generatePaymentProjection = function (enrollment) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/enrollments/paymentProjections.cshtml',
                    controller: 'app.views.enrollments.paymentProjections as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        enrollment: function () {
                            return $.extend({}, enrollment);
                        },
                        periodId: function () {
                            return null;
                        }
                    }
                })

                modalInstance.result.then(function () {
                    getEnrollments(false);
                });
            }

            vm.changeEnrollmentId = function (enrollment) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/enrollments/changeEnrollmentId.cshtml',
                    controller: 'app.views.enrollments.changeEnrollmentId as vm',
                    backdrop: 'static',
                    resolve: {
                        enrollment: function () {
                            return enrollment;
                        }
                    }
                })

                modalInstance.result.then(function () {
                    getEnrollments(false);
                });
            };


            vm.viewStudents = function (enrollment) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/students/index.cshtml',
                    controller: 'app.views.students.index as vm',
                    size: 'lg2',
                    backdrop: 'static',
                    resolve: {
                        enrollment: function () {
                            return enrollment;
                        }
                    }
                })

                modalInstance.result.then(function () {
                    getEnrollments(false);
                }, function (result) {
                    getEnrollments(false);
                });


            };

            //vm.editStudent = function (enrollment, studentId) {
            //    var modalInstance = $uibModal.open({
            //        templateUrl: settings.settings.appPathNotHTTP + '/views/students/createOrEditModal.cshtml',
            //        controller: 'app.views.enrollments.createOrEditModal as vm',
            //        size: 'lg',
            //        backdrop: 'static',
            //        resolve: {
            //            id: function () {
            //                return studentId;
            //            },
            //            enrollment: function () {
            //                return enrollment;
            //            }

            //        }
            //    });



            function replacePersonalIdWithStringFormat() {
                for (var i = 0; i < vm.enrollments.length; i++) {
                    var enrol = vm.enrollments[i];
                    enrol.id = completeWithCeros(enrol.id, 11);
                }
            }



            vm.delete = function (enrollment) {
                abp.message.confirm(
                    "Delete enrollment '" + enrollment.name + "'?",
                    function (result) {
                        if (result) {
                            enrollmentService.delete({ id: enrollment.id })
                                .then(function (result) {
                                    getEnrollments(false);
                                    abp.notify.info("Deleted enrollment: " + enrollment.name);

                                });
                        }
                    }).result.then(function () {

                        getEnrollments(false);
                    });
            }

            vm.refresh = function () {
                getEnrollments(false);
            };

            vm.printEnrollmentList = function () {
                abp.message.info(App.localize("NotAvailable"));
            };

            getEnrollments(false);


            try {
                if ($stateParams.origin == 'new')
                    vm.openEnrollmentCreationModal();
            } catch (e) { }
        }
    ]);
})();

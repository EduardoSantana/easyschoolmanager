(function () {
    angular.module('MetronicApp').controller('app.views.enrollments.configuratePayments', [
        '$scope', '$uibModal', '$uibModalInstance', 'abp.services.app.enrollment', 'enrollment', 'appSession', 'settings',
        function ($scope, $uibModal, $uibModalInstance, enrollmentService, enrollment, appSession, settings) {
            var vm = this;
            vm.saving = false;

            vm.enrollment = enrollment;
            transformDateFormatReceived();

            var LONGITUD_CEDULA = 11;
           
            vm.students = [];

            $scope.$watch("vm.enrollment.numberOfPayment", function (newValue, oldValue) {

                if (vm.numberOfPaymentFocused == true && newValue != oldValue && getObjectValueOrEmptyString(newValue).length > 0 ) {
                    transformDateFormatToSend();
                    enrollmentService.setPaymentProjectionToNumber({
                        numberOfPayments: newValue,
                        paymentProjections: vm.enrollment.tutorPaymentProjection,
                        enrollmentStudents: vm.enrollment.enrollmentStudents
                    }).then(function (result) {

                        vm.enrollment.tutorPaymentProjection = result.data.paymentProjections;
                        vm.enrollment.enrollmentStudents = result.data.enrollmentStudents;
                        transformDateFormatReceived();
                    });
                }
            });
            

            function transformDateFormatReceived()
            {
                for (var i = 0; i < vm.enrollment.tutorPaymentProjection.length; i++) {
                    var current = vm.enrollment.tutorPaymentProjection[i];
                    current.date = convertDateFormat_YMD_to_DMY(current.date, 4);
                }
            }
            function transformDateFormatToSend()
            {
                for (var i = 0; i < vm.enrollment.tutorPaymentProjection.length; i++) {
                    var current = vm.enrollment.tutorPaymentProjection[i];

                    if (current.date && current.date.toString().indexOf("GMT") > 0)
                        current.date = new Date(current.date);

                    current.date = convertDateFormat_DMY_to_YMD(current.date, 4);
                }
            }

            vm.cancel = function () {
                transformDateFormatToSend();
                $uibModalInstance.dismiss({});
            };

            App.initAjax();

            setTimeout(function () { $("#enrollmentNewEnrollmentId").focus(); }, 100);

            var init = function () {

                App.initAjax();
            }


            function AssignTutorProjectionDatesToStudentProjections(enrollment)
            {
                for (var i = 0; i < vm.enrollment.tutorPaymentProjection.length; i++) {
                    var tp = vm.enrollment.tutorPaymentProjection[i];
                    for (var j = 0; j < vm.enrollment.enrollmentStudents.length; j++) {
                        var enrollmentStudent = vm.enrollment.enrollmentStudents[j];
                        if (enrollmentStudent) {
                            var paymentDateX = enrollmentStudent.paymentProjections.filter(function (x) { return x.sequence == tp.sequence });
                            if (paymentDateX.length > 0)
                            {
                                paymentDateX[0].paymentDate = tp.date;
                            }
                        }
                    }
                }
            }

            vm.save = function () {
                transformDateFormatToSend();
                AssignTutorProjectionDatesToStudentProjections(vm.enrollment);
                $uibModalInstance.close(vm.enrollment);
            }

            init();
        }

    ]);
})();

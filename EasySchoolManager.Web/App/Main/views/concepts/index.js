(function () {
    angular.module('MetronicApp').controller('app.views.concepts.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.concept','settings',
        function ($scope, $timeout, $uibModal, conceptService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getConcepts(false);
            }


            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.concepts = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 100,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openConceptEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('ConceptDescription'),
                    field: 'description',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ConceptShortDescription'),
                    field: 'shortDescription',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ConceptCatalogName'),
                    field: 'catalogs_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Period'),
                    field: 'periods_Period',
                     width: 75
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getConcepts(showTheLastPage) {
                conceptService.getAllConcepts($scope.pagination).then(function (result) {
                    vm.concepts = result.data.items;

                    $scope.gridOptions.data = vm.concepts;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openConceptCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/concepts/createModal.cshtml',
                    controller: 'app.views.concepts.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getConcepts(true);
                });
            };

            vm.openConceptEditModal = function (concept) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/concepts/editModal.cshtml',
                    controller: 'app.views.concepts.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return concept.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getConcepts(false);
                });
            };

            vm.delete = function (concept) {
                abp.message.confirm(
                    "Delete concept '" + concept.name + "'?",
                    function (result) {
                        if (result) {
                            conceptService.delete({ id: concept.id })
                                .then(function (result) {
                                    getConcepts(false);
                                    abp.notify.info("Deleted concept: " + concept.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getConcepts(false);
            };

            getConcepts(false);
        }
    ]);
})();

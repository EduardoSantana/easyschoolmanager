(function () {
    angular.module('MetronicApp').controller('app.views.concepts.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.concept', 'abp.services.app.catalog', 'abp.services.app.transactionType', 'abp.services.app.t1Dimension', 'abp.services.app.t2Dimension',
        'abp.services.app.t3Dimension', 'abp.services.app.t4Dimension', 'abp.services.app.t5Dimension', 'abp.services.app.t6Dimension', 'abp.services.app.t7Dimension', 'abp.services.app.t8Dimension',
        'abp.services.app.t9Dimension', 'abp.services.app.t10Dimension', 'abp.services.app.receiptType', 'abp.services.app.period',
        function ($scope, $uibModalInstance, conceptService, catalogService, transactionTypeService, t1DimensionService, t2DimensionService,
            t3DimensionService, t4DimensionService, t5DimensionService, t6DimensionService, t7DimensionService, t8DimensionService, t9DimensionService,
            t10DimensionService, receiptTypeService, periodService) {
            var vm = this;
            vm.saving = false;

            vm.concept = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     conceptService.create(vm.concept)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.catalogs = [];
            vm.getCatalogs	 = function()
            {
                catalogService.getAllCatalogsForCombo().then(function (result) {
                    vm.catalogs = result.data;
					App.initAjax();
                });
            }

            vm.t1Dimensions = [];
            vm.getT1Dimensions = function () {
                t1DimensionService.getAllT1DimensionsForCombo().then(function (result) {
                    vm.t1Dimensions = result.data;
                    App.initAjax();
                });
            }

            vm.t2Dimensions = [];
            vm.getT2Dimensions = function () {
                t2DimensionService.getAllT2DimensionsForCombo().then(function (result) {
                    vm.t2Dimensions = result.data;
                    App.initAjax();
                });
            }

            vm.t3Dimensions = [];
            vm.getT3Dimensions = function () {
                t3DimensionService.getAllT3DimensionsForCombo().then(function (result) {
                    vm.t3Dimensions = result.data;
                    App.initAjax();
                });
            }

            vm.t4Dimensions = [];
            vm.getT4Dimensions = function () {
                t4DimensionService.getAllT4DimensionsForCombo().then(function (result) {
                    vm.t4Dimensions = result.data;
                    App.initAjax();
                });
            }

            vm.t5Dimensions = [];
            vm.getT5Dimensions = function () {
                t5DimensionService.getAllT5DimensionsForCombo().then(function (result) {
                    vm.t5Dimensions = result.data;
                    App.initAjax();
                });
            }

            vm.t6Dimensions = [];
            vm.getT6Dimensions = function () {
                t6DimensionService.getAllT6DimensionsForCombo().then(function (result) {
                    vm.t6Dimensions = result.data;
                    App.initAjax();
                });
            }

            vm.t7Dimensions = [];
            vm.getT7Dimensions = function () {
                t7DimensionService.getAllT7DimensionsForCombo().then(function (result) {
                    vm.t7Dimensions = result.data;
                    App.initAjax();
                });
            }

            vm.t8Dimensions = [];
            vm.getT8Dimensions = function () {
                t8DimensionService.getAllT8DimensionsForCombo().then(function (result) {
                    vm.t8Dimensions = result.data;
                    App.initAjax();
                });
            }

            vm.t9Dimensions = [];
            vm.getT9Dimensions = function () {
                t9DimensionService.getAllT9DimensionsForCombo().then(function (result) {
                    vm.t9Dimensions = result.data;
                    App.initAjax();
                });
            }

            vm.t10Dimensions = [];
            vm.getT10Dimensions = function () {
                t10DimensionService.getAllT10DimensionsForCombo().then(function (result) {
                    vm.t10Dimensions = result.data;
                    App.initAjax();
                });
            }

            vm.receiptTypes = [];
            vm.getTransactionTypes = function () {
                transactionTypeService.getAllTransactionTypesForCombo().then(function (result) {
                    vm.transactionTypes = result.data;
                    App.initAjax();
                });
            }

            vm.receiptTypes = [];
            vm.getReceiptTypes = function () {
                receiptTypeService.getAllReceiptTypesForCombo().then(function (result) {
                    vm.receiptTypes = result.data;
                    App.initAjax();
                });
            }

            vm.periods = [];
            vm.getPeriods = function () {
                periodService.getAllPeriodsForCombo().then(function (result) {
                    vm.periods = result.data;
                    App.initAjax();
                });
            }
			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getCatalogs();
            vm.getT1Dimensions();
            vm.getT2Dimensions();
            vm.getT3Dimensions();
            vm.getT4Dimensions();
            vm.getT5Dimensions();
            vm.getT6Dimensions();
            vm.getT7Dimensions();
            vm.getT8Dimensions();
            vm.getT9Dimensions();
            vm.getT10Dimensions();
            vm.getTransactionTypes();
            vm.getReceiptTypes();
            vm.getPeriods();

		    App.initAjax();
			setTimeout(function () { $("#conceptDescription").focus(); }, 100);
        }
    ]);
})();

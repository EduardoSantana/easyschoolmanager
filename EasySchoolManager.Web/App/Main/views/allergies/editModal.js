(function () {
    angular.module('MetronicApp').controller('app.views.allergies.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.allergy', 'id', 
        function ($scope, $uibModalInstance, allergyService, id ) {
            var vm = this;  vm.saving = false;

            vm.allergy = {
                isActive: true
            };

            var init = function () {
                allergyService.get({ id: id })
                    .then(function (result) {
                        vm.allergy = result.data;

						App.initAjax();
						setTimeout(function () { $("#allergyName").focus(); }, 100);
                    });
            }

            vm.save = function () {
                if (vm.saving == true)
                    return;
                vm.saving = true;

                allergyService.update(vm.allergy)
                    .then(function () {
                        vm.saving = false;
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    }, function (e) {

                        vm.saving = false;
                        console.log(e.data.message);
                    });
            };
			
			
			//XXXInsertCallRelatedEntitiesXXX


			
			

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

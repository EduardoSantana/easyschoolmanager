(function () {
    angular.module('MetronicApp').controller('app.views.allergies.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.allergy', 
        function ($scope, $uibModalInstance, allergyService ) {
            var vm = this;

            vm.allergy = {
                isActive: true
            };

            vm.saving = false;
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     allergyService.create(vm.allergy)
                         .then(function () {
                             vm.saving = false;
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                         }, function (e)
                         {
                             vm.saving = false;
                             console.log(e.data.message);
                         });
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };
			
		    App.initAjax();
			
			setTimeout(function () { $("#allergyName").focus(); }, 100);
        }
    ]);
})();

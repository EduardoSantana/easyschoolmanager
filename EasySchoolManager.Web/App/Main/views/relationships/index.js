(function () {
    angular.module('MetronicApp').controller('app.views.relationships.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.relationship','settings',
        function ($scope, $timeout, $uibModal, relationshipService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getRelationships(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.relationships = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
                    
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openRelationshipEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('RelationshipName'),
                    field: 'name',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getRelationships(showTheLastPage) {
                relationshipService.getAllRelationships($scope.pagination).then(function (result) {
                    vm.relationships = result.data.items;

                    $scope.gridOptions.data = vm.relationships;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openRelationshipCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/relationships/createModal.cshtml',
                    controller: 'app.views.relationships.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getRelationships(false);
                });
            };

            vm.openRelationshipEditModal = function (relationship) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/relationships/editModal.cshtml',
                    controller: 'app.views.relationships.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return relationship.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getRelationships(false);
                });
            };

            vm.delete = function (relationship) {
                abp.message.confirm(
                    "Delete relationship '" + relationship.name + "'?",
                    function (result) {
                        if (result) {
                            relationshipService.delete({ id: relationship.id })
                                .then(function (result) {
                                    getRelationships(false);
                                    abp.notify.info("Deleted relationship: " + relationship.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getRelationships(false);
            };

            getRelationships(false);
        }
    ]);
})();

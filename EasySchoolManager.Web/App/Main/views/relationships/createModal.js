(function () {
    angular.module('MetronicApp').controller('app.views.relationships.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.relationship', 
        function ($scope, $uibModalInstance, relationshipService ) {
            var vm = this;
            vm.saving = false;

            vm.relationship = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     relationshipService.create(vm.relationship)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#relationshipName").focus(); }, 100);
        }
    ]);
})();

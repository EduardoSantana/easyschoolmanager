(function () {
    angular.module('MetronicApp').controller('app.views.relationships.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.relationship', 'id', 
        function ($scope, $uibModalInstance, relationshipService, id ) {
            var vm = this;
			vm.saving = false;

            vm.relationship = {
                isActive: true
            };
            var init = function () {
                relationshipService.get({ id: id })
                    .then(function (result) {
                        vm.relationship = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#relationshipName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						relationshipService.update(vm.relationship)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.illnesses.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.illness', 
        function ($scope, $uibModalInstance, illnessService ) {
            var vm = this;
            vm.saving = false;

            vm.illness = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     illnessService.create(vm.illness)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#illnessName").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.illnesses.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.illness', 'id', 
        function ($scope, $uibModalInstance, illnessService, id ) {
            var vm = this;
			vm.saving = false;

            vm.illness = {
                isActive: true
            };
            var init = function () {
                illnessService.get({ id: id })
                    .then(function (result) {
                        vm.illness = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#illnessName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						illnessService.update(vm.illness)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

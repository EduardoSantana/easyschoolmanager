(function () {
    angular.module('MetronicApp').controller('app.views.inventoryShops.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.inventoryShop', 'id', 'abp.services.app.articleShop',
        function ($scope, $uibModalInstance, inventoryShopService, id , articleShopService) {
            var vm = this;
			vm.saving = false;

            vm.inventoryShop = {
                isActive: true
            };
            var init = function () {
                inventoryShopService.get({ id: id })
                    .then(function (result) {
                        vm.inventoryShop = result.data;
						                vm.getArticleShops();

						App.initAjax();
						setTimeout(function () { $("#inventoryShopArticleShopId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						inventoryShopService.update(vm.inventoryShop)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.articleShops = [];
            vm.getArticleShops	 = function()
            {
                articleShopService.getAllArticleShopsForCombo().then(function (result) {
                    vm.articleShops = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

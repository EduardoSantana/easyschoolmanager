(function () {
    angular.module('MetronicApp').controller('app.views.inventoryShops.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.inventoryShop','settings',
        function ($scope, $timeout, $uibModal, inventoryShopService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getInventoryShops(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.inventoryShops = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openInventoryShopEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                       // '      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'articleShops_Sequence',
                        width: 75
                    },

                                        {
                    name: App.localize('Article'),
                    field: 'articleShops_Description',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Existence'),
                    field: 'existence',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Price'),
                    field: 'price',
                    minWidth: 125,
                    cellFilter: 'number:2'
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getInventoryShops(showTheLastPage) {
                inventoryShopService.getAllInventoryShops($scope.pagination).then(function (result) {
                    vm.inventoryShops = result.data.items;

                    $scope.gridOptions.data = vm.inventoryShops;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openInventoryShopCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/inventoryShops/createModal.cshtml',
                    controller: 'app.views.inventoryShops.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getInventoryShops(false);
                });
            };

            vm.openInventoryShopEditModal = function (inventoryShop) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/inventoryShops/editModal.cshtml',
                    controller: 'app.views.inventoryShops.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return inventoryShop.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getInventoryShops(false);
                });
            };

            vm.delete = function (inventoryShop) {
                abp.message.confirm(
                    "Delete inventoryShop '" + inventoryShop.name + "'?",
                    function (result) {
                        if (result) {
                            inventoryShopService.delete({ id: inventoryShop.id })
                                .then(function (result) {
                                    getInventoryShops(false);
                                    abp.notify.info("Deleted inventoryShop: " + inventoryShop.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getInventoryShops(false);
            };

            getInventoryShops(false);
        }
    ]);
})();

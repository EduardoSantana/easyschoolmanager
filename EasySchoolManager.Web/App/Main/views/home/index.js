﻿(function () {
    var controllerId = 'MetronicApp.views.home.index';
    angular.module('MetronicApp').controller(controllerId, [
        '$rootScope', '$scope', function ($rootScope, $scope) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                // initialize core components
                App.initAjax();
            });

            // set sidebar closed and body solid layout mode
            $rootScope.$settings.settings.layout.pageContentWhite = true;
            $rootScope.$settings.settings.layout.pageBodySolid = false;
            $rootScope.$settings.settings.layout.pageSidebarClosed = false;
         
        }
    ]);
})();
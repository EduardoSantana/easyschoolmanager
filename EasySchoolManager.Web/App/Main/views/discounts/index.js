(function () {
    angular.module('MetronicApp').controller('app.views.discounts.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.discount', 'settings',
        function ($scope, $timeout, $uibModal, discountService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getDiscounts(false);
            }
            vm.discounts = [];

            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openDiscountEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Period'),
                        field: 'periods_Period',
                        width: 100
                    },
                    {
                        name: App.localize('Enrollment'),
                        field: 'enrollmentSequence',
                        width: 100
                    },
                    {
                        name: App.localize('Student'),
                        field: 'studentName',
                        width: 200
                    },
                    {
                        name: App.localize('Tutor'),
                        field: 'tutorName',
                        width: 200
                    },
                    {
                        name: App.localize('Description'),
                        field: 'concepts_Description',
                        minWidth: 100
                    },
                    {
                        name: App.localize('DiscountNames'),
                        field: 'periodDiscountDetails_PeriodDiscounts_DiscountNames_Name',
                        width: 150
                    },                  

                    {
                        name: App.localize('DiscountPercent'),
                        field: 'percentString',
                        width: 100
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getDiscounts(showTheLastPage) {
                discountService.getAllDiscounts($scope.pagination).then(function (result) {
                    vm.discounts = result.data.items;

                    $scope.gridOptions.data = vm.discounts;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openDiscountCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/discounts/createModal.cshtml',
                    controller: 'app.views.discounts.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getDiscounts(true);
                });
            };

            vm.openDiscountEditModal = function (discount) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/discounts/editModal.cshtml',
                    controller: 'app.views.discounts.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return discount.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getDiscounts(false);
                });
            };

            vm.delete = function (discount) {
                abp.message.confirm(
                    "Delete discount '" + discount.name + "'?",
                    function (result) {
                        if (result) {
                            discountService.delete({ id: discount.id })
                                .then(function (result) {
                                    getDiscounts(false);
                                    abp.notify.info("Deleted discount: " + discount.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getDiscounts(false);
            };

            getDiscounts(false);
        }
    ]);
})();

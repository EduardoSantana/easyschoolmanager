(function () {
    angular.module('MetronicApp').controller('app.views.discounts.createModal', [
        '$scope', '$uibModal', '$uibModalInstance', 'abp.services.app.discount', 'abp.services.app.student', 'abp.services.app.period', 'genericModal',
        '$location', 'abp.services.app.concept', 'abp.services.app.enrollment', 'settings', 'abp.services.app.periodDiscountDetail', 'abp.services.app.discountName',
        function ($scope, $uibModal, $uibModalInstance, discountService, studentService, periodService, genericModal, $location, conceptService, enrollmentService, settings, periodDiscountDetailService, discountNameService) {
            var vm = this;
            vm.saving = false;

            vm.discount = {
                isActive: true
            };
            vm.save = function () {

                if (!ValidateDiscount())
                    return;

                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    discountService.create(vm.discount)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            vm.searchEnrollments = function () {
                enrollmentService.getAllEnrollmentsForQueries({ textFilter: vm.discount.name, maxResultCount: 15, periodId: vm.discount.periodId })
                    .then(function (result) {
                        if (result.data.items && result.data.items.length == 1)
                            vm.selectedEnrollment(result.data.items[0]);
                        else if (result.data.items && result.data.items.length > 1)
                            vm.showModal(vm.discount.name);
                    });
            }

            vm.searchEnrollmentByNumber = function () {
                vm.discount.periodDiscountDetailId = null;
                App.initAjax();

                enrollmentService.getAllEnrollmentsForQueries({ textFilter: vm.discount.enrollment, maxResultCount: 1, periodId: vm.discount.periodId })
                    .then(function (result) {
                        if (result.data.items && result.data.items.length > 0) {
                            vm.selectedEnrollment(result.data.items[0]);
                            if (result.data.items[0].enrollmentStudents.length > 1)
                                vm.viewAllEnrollmentStudent();
                            else {
                                var studx = result.data.items[0].enrollmentStudents[0].students;
                                vm.discount.studentName = studx.fullName;
                                vm.discount.students = studx;
                                vm.discount.studentId = studx.id;
                            }
                        };
                    });
            }


            vm.showModal = function (textFilter) {
                genericModal.open({
                    textFilter: textFilter,
                    size: "lg",
                    periodId: vm.discount.periodId,
                    title: App.localize('SelectEnrollment'),
                    serviceMethod: enrollmentService.getAllEnrollmentsForQueries,
                    gridOptions: {
                        enableSorting: false,
                        enableFiltering: false
                    },
                    callback: vm.selectedEnrollment,
                    columnDefs: [
                        {
                            name: App.localize('Enrollment'),
                            field: 'enrollment',
                            width: 150
                        },
                        {
                            name: App.localize('Tutor'),
                            field: 'fullName'
                        },
                        {
                            name: App.localize('Students'),
                            field: 'numberOfStudents',
                            width: 150
                        }

                    ]
                });
            }

            vm.selectedEnrollment = function (newValue) {

                if (newValue) {
                    vm.discount.enrollment = newValue.enrollment;
                    vm.discount.enrollmentId = newValue.id;
                    vm.discount.enrollmentName = newValue.fullName;
                    vm.discount.enrollmentStudents = newValue.enrollmentStudents;
                    vm.discount.studentName = null;
                    vm.discount.studentId = null;


                    if (newValue.enrollmentStudents.length > 1)
                        vm.viewAllEnrollmentStudent();
                    else {
                        var studx = newValue.enrollmentStudents[0].students;
                        vm.discount.studentName = studx.fullName;
                        vm.discount.students = studx;
                        vm.discount.studentId = studx.id;
                    }

                }
                else {
                    vm.discount.enrollment = null;
                    vm.discount.enrollmentId = null;
                    vm.discount.enrollmentName = null;
                    vm.discount.enrollmentStudents = null;
                    vm.discount.studentName = null;
                    vm.discount.studentId = null;
                }
            }

            $scope.getEnrollmentsByName = function (userInputString) {
                $scope.pagination.maxResultCount = 10;
                $scope.pagination.textFilter = userInputString;
                return mcStudentService.getAllMcStudents($scope.pagination);
            }

            $scope.returnedValue = function (result) {
                if (!result) return;
                vm.discount.enrollment = result.originalObject.enrollment;
                vm.discount.enrollmentId = result.originalObject.id;
                vm.discount.enrollmentName = result.originalObject.fullName;
            }

            vm.cleanDiscountStudent = function () {
                vm.discount.studentName = null;
                vm.discount.students = null;
                vm.discount.studentId = null;
            }

            vm.cleanDiscountEnrollment = function () {
                vm.discount.enrollmentId = null;
                vm.discount.enrollment = null;
                vm.discount.enrollmentName = null;
                $("#EnrollmentName").focus();
            }
            vm.concepts = [];

            vm.getConcepts = function () {
                conceptService.getAllConceptsForDiscountsCombo().then(function (result) {
                    vm.concepts = result.data;
                    App.initAjax();
                });
            }
            vm.discountNames = [];

            vm.getDiscountNames = function () {
                discountNameService.getAllDiscountNamesForCombo().then(function (result) {
                    vm.discountNames = result.data;
                    App.initAjax();
                });
            }


            $scope.$watch("vm.discount.periodId", function (newValue, oldValue) {
                if (newValue != null && newValue != undefined)
                    vm.getConceptsByPeriod(newValue);
            });


            $scope.$watch("vm.discount.conceptId", function (newValue, oldValue) {
                if (newValue != null && newValue != undefined)
                    if (vm.discount.conceptId != null && vm.discount.periodId != null) {
                        vm.getPeriodDiscountDetails(vm.discount.periodId, vm.discount.conceptId, vm.discount.discountNameId);
                    }
            });


            $scope.$watch("vm.discount.discountNameId", function (newValue, oldValue) {
                if (newValue != null && newValue != undefined)
                    if (vm.discount.conceptId != null && vm.discount.periodId != null) {
                        vm.getPeriodDiscountDetails(vm.discount.periodId, vm.discount.conceptId, vm.discount.discountNameId);
                    }
            });

            vm.concepts = [];
            vm.getConceptsByPeriod = function (periodId) {
                conceptService.getAllConceptsForDiscountsByPeriodCombo(periodId).then(function (result) {
                    vm.concepts = result.data;
                    App.initAjax();
                });
            }

            vm.periodDiscountDetails = [];
            vm.getPeriodDiscountDetails = function (periodId, conceptId, discountNameId) {
                periodDiscountDetailService.getAllPeriodDiscountDetailsByPeriodAndConceptForCombo({ periodId: periodId, conceptId: conceptId, discountNameId: discountNameId })
                    .then(function (result) {
                        vm.periodDiscountDetails = result.data;
                        App.initAjax();
                    });
            }

            //XXXInsertCallRelatedEntitiesXXX

            //vm.students = [];
            //vm.getStudents = function () {
            //    studentService.getAllStudentsForCombo().then(function (result) {
            //        vm.students = result.data;
            //        App.initAjax();
            //    });
            //}

            vm.periods = [];
            vm.getPeriods = function () {
                periodService.getAllPeriodsForDiscountForCombo().then(function (result) {
                    vm.periods = result.data;
                    App.initAjax();
                });
            }

            vm.viewAllEnrollmentStudent = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/discounts/studentList.cshtml',
                    controller: 'app.views.discount.studentList as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        enrollmentStudents: function () {
                            return vm.discount.enrollmentStudents;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function (result) {
                    vm.discount.studentName = result.students.fullName;
                    vm.discount.students = result.students;
                    vm.discount.studentId = result.students.id;
                });
            };

            function ValidateDiscount() {
                var valido = true;
                if (vm.discount.studentName == null || vm.discount.studentName.length == 0) {
                    abp.message.error("YouMustSelectOneStudentBeforeSaveTheDiscount");
                    valido = false;
                }
                else if (vm.discount.enrollmentName == null || vm.discount.enrollmentName.length == 0) {
                    abp.message.error("YouMustSelectOneEnrollmentBeforeSaveTheDiscount");
                    valido = false;
                }

                return valido;
            }



            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getPeriods();
            vm.getConcepts();
            vm.getDiscountNames();

            App.initAjax();
            setTimeout(function () { $("#discountStudentId").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.discount.studentList', [
        '$scope', '$timeout', '$uibModal', '$uibModalInstance', 'abp.services.app.student', 'settings', 'report','enrollmentStudents',
        function ($scope, $timeout, $uibModal, $uibModalInstance, studentService, settings, report, enrollmentStudents) {
            var vm = this;
            vm.textFilter = "";
            vm.students = enrollmentStudents;
            
            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }
            
            $scope.gridOptions = {
                rowHeight: 39,
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 180,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '    <button title="' + App.localize("SelectStudent") +
                        '" class="btn btn-xs btn-default gray fa-grid-icon1" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.selectStudent(row.entity)"><i class="fa fa-reply"></i></button>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Id'),
                        field: 'students.id',
                        minWidth: 125
                    },
                    {
                        name: App.localize('StudentFirstName'),
                        field: 'students.firstName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('StudentLastName'),
                        field: 'students.lastName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('StudentGender'),
                        field: 'students.genders_Name',
                        minWidth: 125
                    }
                   
                ]
            };

            function getStudents() {
                    $scope.gridOptions.data = vm.students;
            }

            vm.selectStudent = function(row)
            {
                $uibModalInstance.close(row);
            }
            
            
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };


            getStudents();

           
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.subjectSessions.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.subjectSession','settings',
        function ($scope, $timeout, $uibModal, subjectSessionService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getSubjectSessions(false);
            }
            vm.subjectSessions = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openSubjectSessionEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                    name: App.localize('Periods'),
                    field: 'periods_Description',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Courses'),
                    field: 'courseSessions_Courses_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Subjects'),
                    field: 'subjects_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Teachers'),
                    field: 'teacherTenants_Teachers_Name',
                    minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getSubjectSessions(showTheLastPage) {
                subjectSessionService.getAllSubjectSessions($scope.pagination).then(function (result) {
                    vm.subjectSessions = result.data.items;

                    $scope.gridOptions.data = vm.subjectSessions;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openSubjectSessionCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/subjectSessions/createModal.cshtml',
                    controller: 'app.views.subjectSessions.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getSubjectSessions(false);
                });
            };

            vm.openSubjectSessionEditModal = function (subjectSession) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/subjectSessions/editModal.cshtml',
                    controller: 'app.views.subjectSessions.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return subjectSession.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getSubjectSessions(false);
                });
            };

            vm.delete = function (subjectSession) {
                abp.message.confirm(
                    "Delete subjectSession '" + subjectSession.name + "'?",
                    function (result) {
                        if (result) {
                            subjectSessionService.delete({ id: subjectSession.id })
                                .then(function (result) {
                                    getSubjectSessions(false);
                                    abp.notify.info("Deleted subjectSession: " + subjectSession.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getSubjectSessions(false);
            };

            getSubjectSessions(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.subjectSessions.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.subjectSession', 'abp.services.app.subject', 'abp.services.app.teacherTenant', 'abp.services.app.courseSession', 'abp.services.app.period',
        function ($scope, $uibModalInstance, subjectSessionService, subjectService, teacherTenantService, courseSessionService, periodService) {
            var vm = this;
            vm.saving = false;

            vm.subjectSession = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    subjectSessionService.create(vm.subjectSession)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            $scope.$watch("vm.subjectSession.courseSessionId", function (newValue, oldValue) {
                if (newValue != null) {
                    var courseId = 0;
                    for (var i = 0; i < vm.courseSessions.length; i++) {
                        if (vm.courseSessions[i].id == newValue) {
                            courseId = vm.courseSessions[i].courseId;
                        }
                    }
                    vm.getSubjects(courseId);
                }
            });

            //XXXInsertCallRelatedEntitiesXXX

            vm.subjects = [];
            vm.getSubjects = function (courseId) {
                subjectService.getAllSubjectsByCourseForCombo(courseId).then(function (result) {
                    vm.subjects = result.data;
                    vm.subjectSession.subjectsId = null;
                    App.initAjax();
                });
            };

            vm.teacherTenants = [];
            vm.getTeacherTenants = function () {
                teacherTenantService.getAllTeacherTenantsForCombo().then(function (result) {
                    vm.teacherTenants = result.data;
                    App.initAjax();
                });
            };

            vm.courseSessions = [];
            vm.getCourseSessions = function () {
                courseSessionService.getAllCourseSessionsPrimaryForCombo().then(function (result) {
                    vm.courseSessions = result.data;
                    App.initAjax();
                });
            };

            vm.periods = [];
            vm.getPeriods = function () {
                periodService.getAllPeriodsForCombo().then(function (result) {
                    vm.periods = result.data;
                    App.initAjax();
                });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getTeacherTenants();
            vm.getCourseSessions();
            vm.getPeriods();

            App.initAjax();
            setTimeout(function () { $("#subjectSessionSubjectId").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.deposits.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.transaction','settings', '$stateParams',
        function ($scope, $timeout, $uibModal, depositService, settings, $stateParams) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getDeposits(false);
            }


            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.deposits = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openDepositEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.appendImage(row.entity)"><i class=\"fa fa-picture-o\"></i> ' + App.localize('Image') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                    name: App.localize('DepositSequence'),
                    field: 'sequence',
                    width: 110
                    },
                    {
                        name: App.localize('DepositDate'),
                        field: 'date',
                        width: 110,
                        cellFilter: 'date:"dd-MM-yyyy\"'
                    },
                    {
                        name: App.localize('DepositBank'),
                        field: 'banks_Name',
                        minWidth: 250
                    },
                    {
                        name: App.localize('DepositBankAccountNumber'),
                        field: 'bankAccounts_Number',
                        minWidth: 250
                    },
                    {
                    name: App.localize('DepositNumber'),
                    field: 'number',
                    minWidth: 250
                    },
                    {
                    name: App.localize('DepositAmount'),
                    field: 'amount',
                    width: 130,
                    cellFilter: 'number:2',
                    cellClass: "font-uigrid-right",
                    headerClass: "font-uigrid-right"
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 100
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getDeposits(showTheLastPage) {
                depositService.getAllDeposits($scope.pagination).then(function (result) {
                    vm.deposits = result.data.items;

                    $scope.gridOptions.data = vm.deposits;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openDepositCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/deposits/createModal.cshtml',
                    controller: 'app.views.deposits.createModal as vm',
                    size: 'lg',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getDeposits(true);
                    vm.openDepositCreationModal();
                });
            };

            vm.openDepositEditModal = function (deposit) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/deposits/editModal.cshtml',
                    controller: 'app.views.deposits.editModal as vm',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return deposit.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getDeposits(false);
                });
            };

            vm.delete = function (deposit) {
                abp.message.confirm(
                    "Delete deposit '" + deposit.name + "'?",
                    function (result) {
                        if (result) {
                            depositService.delete({ id: deposit.id })
                                .then(function (result) {
                                    getDeposits(false);
                                    abp.notify.info("Deleted deposit: " + deposit.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getDeposits(false);
            };

            vm.printReportDeposit = function () {
                abp.message.info(App.localize("NotAvailable"));
            };

            vm.appendImage = function () {
                abp.message.info(App.localize("NotAvailable"));
            };

            getDeposits(false);
            try {
                if ($stateParams.origin == 'new')
                    vm.openDepositCreationModal();
            } catch (e) { }
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.deposits.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.transaction', 'abp.services.app.bank', 'abp.services.app.paymentMethod', 'abp.services.app.concept','abp.services.app.enrollment','abp.services.app.bankAccount',
        function ($scope, $uibModalInstance, transactionService, bankService, paymentMethodService, conceptService, enrollmentService,bankAccountService) {
            var vm = this;
            vm.saving = false;
            vm.enrollment = {};

            
            var todayDate = getDateYMD(new Date());

            vm.transaction = {
                isActive: true,
                transactionTypeId: 2, //Deposito
                letterAmount: null,
                originId: 1,   //Debito
                statusId: 1,   //Creado
                date: todayDate //Fecha del d�a
            };

            vm.transaction.dateTemp = convertDateFormat_YMD_to_DMY(todayDate, 4);
            vm.transaction.date = todayDate;

            function cleanTransaction()
            {
                vm.transaction.name = null;
                vm.transaction.enrolmentId = null;
                vm.transaction.currentEnrollment = null;

                $scope.gridOptions.data = [];
            }

            $scope.$watch("vm.transaction.enrollment", function (newValue, oldValue) {
                if (newValue != undefined) {
                    if (newValue == null || newValue.length === 0) {
                        cleanTransaction();
                        return;
                    }


                    enrollmentService.getEnrollmentSequenceByEnrollment(newValue).then(function (result) {

                        if (result.data == null) {
                            cleanTransaction();
                        }
                        else {
                            vm.enrollment = result.data;
                            vm.transaction.enrolmentId = vm.enrollment.enrollmentId;
                            vm.transaction.currentEnrollment = vm.enrollment.enrollments;
                            if (vm.transaction.currentEnrollment != null) {
                                vm.transaction.currentEnrollment.fullName = vm.transaction.currentEnrollment.firstName + ' ' + vm.transaction.currentEnrollment.lastName;
                                vm.transaction.name = vm.transaction.currentEnrollment.fullName;

                                $scope.gridOptions.data = vm.transaction.currentEnrollment.enrollmentStudents;
                            }
                        }
                    });
                }
                else
                { cleanTransaction();}

            });


            $scope.$watch("vm.transaction.amount", function (newValue, oldValue) {
                if (newValue == undefined || newValue == null || newValue.length == 0) {
                    vm.transaction.letterAmount = null;
                    return;
                }
                try {
                    vm.transaction.letterAmount = NumeroALetras(newValue);
                } catch (e) {
                    console.log(JSON.stringify(e))
                }
            });


            $scope.$watch("vm.transaction.bankId", function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    var paramx = {
                        bankId: newValue
                    };

                    bankAccountService.getAllBankAccountsForComboByBankIdAndRegionId(paramx).then(function (result) {
                        vm.bankAccounts = result.data;
                        App.initAjax();
                    })
    
                }
            });


            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    vm.transaction.date = convertDateFormat_DMY_to_YMD(vm.transaction.dateTemp);
                    transactionService.create(vm.transaction)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            //XXXInsertCallRelatedEntitiesXXX

            vm.banks = [];
            vm.getBanks = function () {
                bankService.getAllBanksForCombo().then(function (result) {
                    vm.banks = result.data;
                    App.initAjax();
                });
            }

    
            vm.concepts = [];
            vm.getConcepts = function () {
                conceptService.getAllConceptsForDepositsCombo().then(function (result) {
                    vm.concepts = result.data;
                    App.initAjax();
                });
            }

            vm.paymentMethods = [];
            vm.getPaymentMethods = function () {
                paymentMethodService.getAllPaymentMethodsForCombo().then(function (result) {
                    vm.paymentMethods = result.data;
                    App.initAjax();
                });
            }

            vm.bankAccounts = [];
            vm.getBankAccounts = function () {
                bankAccountService.getAllBankAccountsForComboByRegionId().then(function (result) {
                    vm.bankAccounts = result.data;
                    App.initAjax();
                });
            }


            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getBanks();
            vm.getPaymentMethods();
            vm.getConcepts();
            vm.getBankAccounts();


            $scope.gridOptions = {
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openDepositEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        '      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('student'),
                        field: 'students.fullName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('Course'),
                        field: 'students.course.abbreviation',
                        minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };

            function getStudents() {


               $scope.gridOptions.data = [{ students_name: '', Course:'' }];
               

                  
            }


            getStudents();

            

            App.initAjax();
            setTimeout(function () { $("#transactionSequence").focus(); }, 100);
        }
    ]);
})();

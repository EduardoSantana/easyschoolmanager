(function () {
    angular.module('MetronicApp').controller('app.views.deposits.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.transaction', 'id', 'abp.services.app.bank', 'abp.services.app.paymentMethod', 'abp.services.app.concept', 'abp.services.app.bankAccount', 'abp.services.app.enrollment',
        function ($scope, $uibModalInstance, transactionService, id , bankService, paymentMethodService,conceptService,bankAccountService,enrollmentService) {
            var vm = this;
			vm.saving = false;

            vm.transaction = {
                isActive: true
            };
            var init = function () {
                transactionService.get({ id: id })
                    .then(function (result) {
                        vm.transaction = result.data;
                        vm.getBanks();
                        vm.getConcepts();
                        vm.getBankAccounts();
                        vm.transaction.dateTemp = convertDateFormat_YMD_to_DMY(vm.transaction.date, 4);

						App.initAjax();
						setTimeout(function () { $("#transactionSequence").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    vm.transaction.date = convertDateFormat_DMY_to_YMD(vm.transaction.dateTemp);
                    transactionService.update(vm.transaction)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };


            $scope.$watch("vm.transaction.amount", function (newValue, oldValue) {
                if (newValue == undefined || newValue == null || newValue.length == 0) {
                    vm.transaction.letterAmount = null;
                    return;
                }
                try {
                    vm.transaction.letterAmount = NumeroALetras(newValue);
                } catch (e) {
                    console.log(JSON.stringify(e))
                }
            });
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.banks = [];
            vm.getBanks	 = function()
            {
                bankService.getAllBanksForCombo().then(function (result) {
                    vm.banks = result.data;
					App.initAjax();
                });
            }

            vm.concepts = [];
            vm.getConcepts = function () {
                conceptService.getAllConceptsForDepositsCombo().then(function (result) {
                    vm.concepts = result.data;
                    App.initAjax();
                });
            }


            vm.bankAccounts = [];
            vm.getBankAccounts = function () {
                bankAccountService.getAllBankAccountsForCombo().then(function (result) {
                    vm.bankAccounts = result.data;
                    App.initAjax();
                });
            }
			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.periods.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.period', 
        function ($scope, $uibModalInstance, periodService ) {
            var vm = this;
            vm.saving = false;

            vm.period = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     periodService.create(vm.period)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
            setTimeout(function () { $("#periodPeriod").focus(); }, 100);
        }
    ]);
})();

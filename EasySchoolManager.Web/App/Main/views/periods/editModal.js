(function () {
    angular.module('MetronicApp').controller('app.views.periods.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.period', 'id', 
        function ($scope, $uibModalInstance, periodService, id ) {
            var vm = this;
			vm.saving = false;

            vm.period = {
                isActive: true
            };
            var init = function () {
                periodService.get({ id: id })
                    .then(function (result) {
                        vm.period = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#periodTenantId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						periodService.update(vm.period)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

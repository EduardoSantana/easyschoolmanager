(function () {
    angular.module('MetronicApp').controller('app.views.transactionTypes.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.transactionType', 
        function ($scope, $uibModalInstance, transactionTypeService ) {
            var vm = this;
            vm.saving = false;

            vm.transactionType = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     transactionTypeService.create(vm.transactionType)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#transactionTypeName").focus(); }, 100);
        }
    ]);
})();

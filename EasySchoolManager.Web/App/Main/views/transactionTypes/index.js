(function () {
    angular.module('MetronicApp').controller('app.views.transactionTypes.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.transactionType','settings',
        function ($scope, $timeout, $uibModal, transactionTypeService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getTransactionTypes(false);
            }
            vm.transactionTypes = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openTransactionTypeEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('TransactionTypeName'),
                    field: 'name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('TransactionTypeAbbreviation'),
                    field: 'abbreviation',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getTransactionTypes(showTheLastPage) {
                transactionTypeService.getAllTransactionTypes($scope.pagination).then(function (result) {
                    vm.transactionTypes = result.data.items;

                    $scope.gridOptions.data = vm.transactionTypes;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openTransactionTypeCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/transactionTypes/createModal.cshtml',
                    controller: 'app.views.transactionTypes.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getTransactionTypes(false);
                });
            };

            vm.openTransactionTypeEditModal = function (transactionType) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/transactionTypes/editModal.cshtml',
                    controller: 'app.views.transactionTypes.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return transactionType.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getTransactionTypes(false);
                });
            };

            vm.delete = function (transactionType) {
                abp.message.confirm(
                    "Delete transactionType '" + transactionType.name + "'?",
                    function (result) {
                        if (result) {
                            transactionTypeService.delete({ id: transactionType.id })
                                .then(function (result) {
                                    getTransactionTypes(false);
                                    abp.notify.info("Deleted transactionType: " + transactionType.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getTransactionTypes(false);
            };

            getTransactionTypes(false);
        }
    ]);
})();

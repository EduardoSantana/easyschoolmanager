(function () {
    angular.module('MetronicApp').controller('app.views.transactionTypes.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.transactionType', 'id', 
        function ($scope, $uibModalInstance, transactionTypeService, id ) {
            var vm = this;
			vm.saving = false;

            vm.transactionType = {
                isActive: true
            };
            var init = function () {
                transactionTypeService.get({ id: id })
                    .then(function (result) {
                        vm.transactionType = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#transactionTypeName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						transactionTypeService.update(vm.transactionType)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

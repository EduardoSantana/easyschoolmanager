(function () {
    angular.module('MetronicApp').controller('app.views.nextEnrollments.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.enrollment', 'abp.services.app.city', 'abp.services.app.occupation', 'abp.services.app.religion', 'abp.services.app.gender', 'abp.services.app.province',
        function ($scope, $uibModalInstance, enrollmentService, cityService, occupationService, religionService, genderService, provinceService) {
            var vm = this;
            vm.saving = false;
            vm.id = $scope.$resolve.id;

            vm.enrollment = {
                isActive: true
            };

            var LONGITUD_CEDULA = 11;
            var CADENA_INICIAL_GENERAR_SECUENCIA = 999;


            $scope.$watch("vm.enrollment.id", function (newValue, oldValue) {
                if (newValue !== undefined && newValue !== null && newValue.toString().length === LONGITUD_CEDULA) {
                    enrollmentService.getEnrollmentByPersonalId(newValue).then(function (result) {

                        var oldEnrollment = result.data;
                        if (oldEnrollment != null) {
                            abp.message.confirm(
                                App.localize("ThePersonalId'") + completeWithCeros(oldEnrollment.id, 11) + App.localize("IsAlreadyRegisteredInOurDatabase") + "." +
                                App.localize("DoYouWishGetThisInformationInThisForm") + "?",
                                function (result) {
                                    if (result) {
                                        vm.enrollment = $.extend({}, oldEnrollment);
                                        vm.enrollment.id = completeWithCeros(vm.enrollment.id, 11);
                                        vm.enrollment.firstName = oldEnrollment.firstName;
                                        App.initAjax();
                                        $('.select2me').select2();
                                        $("#enrollmentId").attr("disabled", "disabled");
                                        setTimeout(function () { $("#enrollmentId").focus(); }, 100);
                                        setTimeout(function () { $("#enrollmentFirstName").focus(); }, 200);

                                        $scope.$apply();
                                    }
                                });
                        }
                    });
                }
            });

            $scope.$watch("vm.enrollment.provinceId", function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    var paramx = {
                        provinceId: newValue
                    };
                    cityService.getAllCitiesForComboByProvinceId(paramx).then(function (result) {
                        vm.cities = result.data;
                        App.initAjax();
                        createFullAddress();
                    })
                }
            });

            $scope.$watch("vm.enrollment.address", function (newValue, oldValue) {
                createFullAddress();
            });

            $scope.$watch("vm.enrollment.cityId", function (newValue, oldValue) {
                createFullAddress();
            });

            function createFullAddress() {
                try {
                    vm.enrollment.fullAddress = getObjectOrString(vm.enrollment.address) + ", " + getCityNameById(vm.enrollment.cityId, vm.cities)
                        + ", " + getProvinceNameById(vm.enrollment.provinceId, vm.provinces);
                } catch (e) {
                    vm.enrollment.fullAddress = "";
                }
            }



            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;

                if (!validateForm()) {
                    vm.saving = false;
                    return;
                }


                try {
                    enrollmentService.createEnrollment(vm.enrollment)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            function validateForm() {
                var valid = false;
                try {
                    if (vm.enrollment.id.length < LONGITUD_CEDULA && vm.enrollment.id != CADENA_INICIAL_GENERAR_SECUENCIA) {
                        abp.message.error(App.localize("PersonalIdIsInvalid"));
                    }
                    else if (getObjectOrString(vm.enrollment.emailAddress).length > 0 && !isValidEmail(vm.enrollment.emailAddress)) {
                        abp.message.error(App.localize("EmailIsInvalid"));
                    }
                    else valid = true;

                } catch (e) {
                    console.log(e);
                    abp.message.error(JSON.stringify(e));
                }

                return valid;
            }

            //XXXInsertCallRelatedEntitiesXXX

            vm.cities = [];
            vm.getCities = function () {
                cityService.getAllCitiesForCombo().then(function (result) {
                    vm.cities = result.data;
                    App.initAjax();
                });
            }

            vm.occupations = [];
            vm.getOccupations = function () {
                occupationService.getAllOccupationsForCombo().then(function (result) {
                    vm.occupations = result.data;
                    App.initAjax();
                });
            }

            vm.religions = [];
            vm.getReligions = function () {
                religionService.getAllReligionsForCombo().then(function (result) {
                    vm.religions = result.data;
                    App.initAjax();
                });
            }

            vm.genders = [];
            vm.getGenders = function () {
                genderService.getAllGendersForCombo().then(function (result) {
                    vm.genders = result.data;
                    App.initAjax();
                });
            }

            vm.provinces = [];
            vm.getProvinces = function () {
                provinceService.getAllProvincesForCombo().then(function (result) {
                    vm.provinces = result.data;
                    App.initAjax();
                });
            }



            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };



            App.initAjax();

            setTimeout(function () { $("#enrollmentEnrollment").focus(); }, 100);

            var init = function () {

                //vm.enrollment = result.data;

                //vm.enrollment.id = completeWithCeros(vm.enrollment.id, 11);


                vm.getCities();
                vm.getOccupations();
                vm.getReligions();
                vm.getGenders();
                vm.getProvinces();

                App.initAjax();
                setTimeout(function () { $("#enrollmentId").focus(); }, 100);

                setTimeout(function () { $("input").popover({ trigger: "hover", placement: "top" }); }, 1000);

            }

            init();
        }



    ]);
})();


function getCityNameById(cityId, arrayOfCities) {
    var o = arrayOfCities.filter(function (x) { return x.id == cityId });
    if (o != null)
        return o[0].name;
    else
        return "";
}
function getProvinceNameById(provinceId, arrayOfProvinces) {
    var o = arrayOfProvinces.filter(function (x) { return x.id == provinceId });
    if (o != null)
        return o[0].name;
    else
        return "";
}
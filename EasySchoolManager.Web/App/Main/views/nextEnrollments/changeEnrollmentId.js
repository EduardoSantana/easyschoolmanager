(function () {
    angular.module('MetronicApp').controller('app.views.nextEnrollments.changeEnrollmentId', [
        '$scope', '$uibModalInstance', 'abp.services.app.enrollment', 'enrollment',
        function ($scope, $uibModalInstance, enrollmentService, enrollment) {
            var vm = this;
            vm.saving = false;

            vm.enrollment = enrollment;

            var LONGITUD_CEDULA = 11;

            vm.save = function () {

                if (vm.saving === true)
                    return;
                vm.saving = true;

                if (!validateForm()) {
                    vm.saving = false;
                    return;
                }

                try {
                    var dataToChange = {
                        currentEnrollmentId: vm.enrollment.id,
                        newEnrollmentId: vm.enrollment.newEnrollmentId
                    };

                    enrollmentService.changeEnrollmentId(dataToChange)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            function validateForm() {
                var valid = false;
                try {
                    if (vm.enrollment.id.length < LONGITUD_CEDULA) {
                        abp.message.error(App.localize("PersonalIdIsInvalid"));
                    }
                    else if (vm.enrollment.newEnrollmentId.length < LONGITUD_CEDULA) {
                            abp.message.error(App.localize("PersonalIdIsInvalid"));
                        }
                    else valid = true;

                } catch (e) {
                    console.log(e);
                    abp.message.error(JSON.stringify(e));
                }

                return valid;
            }
          
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };
            
            App.initAjax();

            setTimeout(function () { $("#enrollmentNewEnrollmentId").focus(); }, 100);

            var init = function () {
               

                App.initAjax();

                setTimeout(function () { $("input").popover({ trigger: "hover", placement: "top" }); }, 1000);

            }

            init();
        }

    ]);
})();

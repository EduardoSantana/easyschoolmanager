(function () {
    angular.module('MetronicApp').controller('app.views.nextEnrollments.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.enrollment', 'id', 'abp.services.app.city', 'abp.services.app.occupation', 'abp.services.app.religion', 'abp.services.app.gender', 'abp.services.app.province',
        function ($scope, $uibModalInstance, enrollmentService, id, cityService, occupationService, religionService, genderService, provinceService) {
            var vm = this;
            vm.saving = false;
            vm.enrollmentId = $scope.$resolve.id;

            vm.enrollment = {
                isActive: true
            };


            var init = function () {
                enrollmentService.get({ id: vm.enrollmentId })
                    .then(function (result) {
                        vm.getCities();
                        vm.getProvinces();
                        vm.getOccupations();
                        vm.getReligions();
                        vm.getGenders();

                        vm.enrollment = result.data;

                        vm.enrollment.id = completeWithCeros(vm.enrollment.id, 11);
                        vm.enrollment.provinceId = vm.enrollment.cities_ProvinceId;


                        //App.initAjax();
                        setTimeout(function () { $("#enrollmentEnrollment").focus(); }, 100);
                    });
            }

            $scope.$watch("vm.student.relationshipId", function (newValue, oldValue) {
                if (newValue == 1) {

                    if (getObjectOrString(vm.student.fatherPersonalId).length == 0 && vm.enrollment.genderId == 1) {
                        vm.student.fatherPersonalId = completeWithCeros(vm.enrollment.id, 11);
                        vm.student.fatherName = vm.enrollment.firstName + ' ' + vm.enrollment.lastName;
                    }
                }
                if (newValue == 2) {

                    if (getObjectOrString(vm.student.motherPersonalId).length == 0 && vm.enrollment.genderId == 2) {
                        vm.student.motherPersonalId = completeWithCeros(vm.enrollment.id, 11);
                        vm.student.motherName = vm.enrollment.firstName + ' ' + vm.enrollment.lastName;
                    }
                }

            });

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {

                    createFullAddress();
                    enrollmentService.updateEnrollment(vm.enrollment)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            $scope.$watch("vm.enrollment.provinceId", function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    var paramx = {
                        provinceId: newValue
                    };
                    cityService.getAllCitiesForComboByProvinceId(paramx).then(function (result) {
                        vm.cities = result.data;
                        App.initAjax();
                        createFullAddress();
                    })
                }
            });

            $scope.$watch("vm.enrollment.address", function (newValue, oldValue) {
                createFullAddress();
            });

            $scope.$watch("vm.enrollment.cityId", function (newValue, oldValue) {
                createFullAddress();
            });

            function createFullAddress() {
                try {
                    vm.enrollment.fullAddress = vm.enrollment.address + ", " + getCityNameById(vm.enrollment.cityId, vm.cities)
                        + ", " + getProvinceNameById(vm.enrollment.provinceId, vm.provinces);
                } catch (e) {
                    vm.enrollment.fullAddress = "";
                }
            }


            vm.cities = [];
            vm.getCities = function () {
                cityService.getAllCitiesForCombo().then(function (result) {
                    vm.cities = result.data;
                });
            }

            vm.occupations = [];
            vm.getOccupations = function () {
                occupationService.getAllOccupationsForCombo().then(function (result) {
                    vm.occupations = result.data;
                });
            }

            vm.religions = [];
            vm.getReligions = function () {
                religionService.getAllReligionsForCombo().then(function (result) {
                    vm.religions = result.data;
                });
            }

            vm.genders = [];
            vm.getGenders = function () {
                genderService.getAllGendersForCombo().then(function (result) {
                    vm.genders = result.data;
                });
            }

            vm.provinces = [];
            vm.getProvinces = function () {
                provinceService.getAllProvincesForCombo().then(function (result) {
                    vm.provinces = result.data;
                });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

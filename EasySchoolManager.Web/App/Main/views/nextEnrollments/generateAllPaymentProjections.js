(function () {
    angular.module('MetronicApp').controller('app.views.enrollments.generateAllPaymentProyection', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.enrollment', 'settings', '$stateParams',
        function ($scope, $timeout, $uibModal, enrollmentService, settings, $stateParams) {
            var vm = this;

            vm.generateAllPaymentProyection = function () {

                abp.message.confirm(
                    App.localize("AreYouSureYouWantToGenerateAllPaymentProjectionForThisSchool"),
                    function (result) {
                        if (result == true) {
                            abp.ui.setBusy(null,
                                enrollmentService.generateAllPaymentProyectionForASchool().then(function (result) {
                                    abp.notify.info(App.localize("PaymentProjectionsGeneratedSuccessfully"));
                                }));
                        }
                    }).
                    result.then(function (result) {
                        if (result == true) {
                            enrollmentService.generateAllPaymentProyectionForASchool.then(function (result) {
                                abp.notify.info(App.localize("PaymentProjectionsGeneratedSuccessfully"));
                            });
                        }
                    });
            }


            function replacePersonalIdWithStringFormat() {
                for (var i = 0; i < vm.enrollments.length; i++) {
                    var enrol = vm.enrollments[i];
                    enrol.id = completeWithCeros(enrol.id, 11);
                }
            }



        }
    ]);
})();

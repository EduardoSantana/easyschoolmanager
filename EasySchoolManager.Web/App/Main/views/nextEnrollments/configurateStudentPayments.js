(function () {
    angular.module('MetronicApp').controller('app.views.nextEnrollments.configurateStudentPayments', [
        '$scope', '$uibModal', '$uibModalInstance', 'abp.services.app.enrollment', 'enrollment', 'appSession', 'settings',
        function ($scope, $uibModal, $uibModalInstance, enrollmentService, enrollment, appSession, settings) {
            var vm = this;
            vm.saving = false;

            vm.enrollment = $scope.$resolve.enrollment;
            vm.student = $scope.$resolve.student;

            //transformDateFormatReceived();


            //function transformDateFormatReceived()
            //{
            //    for (var i = 0; i < vm.enrollment.tutorPaymentProjection.length; i++) {
            //        var current = vm.enrollment.tutorPaymentProjection[i];
            //        current.date = convertDateFormat_YMD_to_DMY(current.date, 4);
            //    }
            //}
            //function transformDateFormatToSend()
            //{
            //    for (var i = 0; i < vm.enrollment.tutorPaymentProjection.length; i++) {
            //        var current = vm.enrollment.tutorPaymentProjection[i];
            //        current.date = convertDateFormat_DMY_to_YMD(current.date, 4);
            //    }
            //}

            $scope.calculateTotalOfStudents = function ()
            {
                var total = 0.00;
                for (var i = 0; i < vm.enrollment.enrollmentStudents.length; i++) {
                    var currentStudent = vm.enrollment.enrollmentStudents[i];
                    currentStudent.students.totalAmount = 0.00;

                    for (var j = 0; j < currentStudent.paymentProjections.length; j++) {
                        var currentPayment = currentStudent.paymentProjections[j];
                        if (currentPayment.sequence > 0)
                            currentStudent.students.totalAmount += Number(currentPayment.amount)
                    }
                }
            }



            $scope.calculateTotalOfTutor = function () {
                var total = 0.00;

                for (var k = 0; k < vm.enrollment.tutorPaymentProjection.length; k++) {
                    var x = vm.enrollment.tutorPaymentProjection[k];
                    x.amount = 0.00;
                }

                for (var i = 0; i < vm.enrollment.enrollmentStudents.length; i++) {
                    var currentStudent = vm.enrollment.enrollmentStudents[i];

                    for (var j = 0; j < currentStudent.paymentProjections.length; j++) {
                        var currentPayment = currentStudent.paymentProjections[j];
                        if (currentPayment.sequence > 0) {
                            vm.enrollment.tutorPaymentProjection[j].amount += Number(currentPayment.amount)
                        }
                        else
                            vm.enrollment.tutorPaymentProjection[j].amount = Number(currentPayment.amount);
                    }
                }
            }




            $scope.getTotalOfStudentPayment = function() {
                vm.student.totalPayment = 0.00;
                for (var i = 0; i < vm.student.paymentProjections.length; i++) {
                    var pp = vm.student.paymentProjections[i];
                    if (pp.sequence > 0)
                        vm.student.totalPayment += parseFloat(pp.amount);
                    else
                        vm.student.inscription = parseFloat(pp.amount);
                }
            }

            function setNewStudentPaymentValues() {
                var index = getEnrollMentStudentIndex(vm.student.id);
                vm.enrollment.enrollmentStudents.splice(index, 1, vm.student);
            }

            function getEnrollMentStudentIndex(Id) {
                var index = -1;
                for (var i = 0; i < vm.enrollment.enrollmentStudents.length; i++) {
                    var es = vm.enrollment.enrollmentStudents[i];
                    if (es.id == Id) {
                        index = i;
                        break
                    }
                }
                return index;
            }


            vm.cancel = function () {
                //transformDateFormatToSend();
                $uibModalInstance.dismiss({});
            };

            App.initAjax();

            setTimeout(function () { $("#enrollmentNewEnrollmentId").focus(); }, 100);

            var init = function () {
                $scope.getTotalOfStudentPayment();
                App.initAjax();
            }

            vm.save = function () {
                //transformDateFormatToSend();
                setNewStudentPaymentValues();
                $scope.calculateTotalOfStudents();
                $scope.calculateTotalOfTutor();
                $uibModalInstance.close(vm.enrollment);
            }

            init();
        }

    ]);
})();

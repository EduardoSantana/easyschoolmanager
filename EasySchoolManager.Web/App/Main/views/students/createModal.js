(function () {
    //Service Prefix
    var sp = 'abp.services.app.';
    angular.module('MetronicApp').controller('app.views.students.createModal', [
        '$scope', '$uibModalInstance', sp + 'student', sp + 'gender', sp + 'blood', sp + 'illness', sp + 'allergy',
        sp + 'relationship', sp + 'course', sp + 'level',

        function ($scope, $uibModalInstance, studentService, genderService, bloodService, illnessService, allergyService,
            relationshipService, courseService, levelService) {
            var vm = this;
            vm.saving = false;
            vm.enrollment = $scope.$resolve.enrollment;

            vm.student = {
                isActive: true,
                position: 1,
                fatherAttendedHere: false,
                motherAttendedHere: false,
                dateOfBirth: null,
                enrollmentId: vm.enrollment.id,
                studentAllergies: [],
                studentIllnesses: [],
                levelId: 0
            };

            var LONGITUD_CEDULA = 11;


            $scope.$watch("vm.student.relationshipId", function (newValue, oldValue) {
                if (newValue == 1) {

                    if (getObjectOrString(vm.student.fatherPersonalId).length == 0 && vm.enrollment.genderId == 1) {
                        vm.student.fatherPersonalId = completeWithCeros(vm.enrollment.id, 11);
                        vm.student.fatherName = vm.enrollment.firstName + ' ' + vm.enrollment.lastName;
                    }
                }
                if (newValue == 2) {

                    if (getObjectOrString(vm.student.motherPersonalId).length == 0 && vm.enrollment.genderId == 2) {
                        vm.student.motherPersonalId = completeWithCeros(vm.enrollment.id, 11);
                        vm.student.motherName = vm.enrollment.firstName + ' ' + vm.enrollment.lastName;
                    }
                }

            });


            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;

                if (!validateForm()) {
                    vm.saving = false;
                    return;
                }

                try {
                    studentService.create(vm.student)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (error) {
                            vm.saving = false;
                            // abp.message.error(JSON.stringify(error));
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };


            $scope.$watch("vm.student.studentPersonalId", function (newValue, oldValue) {
                if (newValue !== undefined && newValue !== null && newValue.toString().length === LONGITUD_CEDULA) {
                    studentService.getStudentByPersonalId(newValue).then(function (result) {

                        var oldStudent = result.data;
                        if (oldStudent != null) {
                            abp.message.confirm(
                                App.localize("ThePersonalId'") +oldStudent.studentPersonalId+ App.localize("IsAlreadyRegisteredInOurDatabase") + "." +
                                App.localize("DoYouWishGetThisInformationInThisForm") + "?",
                                function (result) {
                                    if (result) {
                                        vm.student = $.extend({}, oldStudent);
                                        vm.student.studentPersonalId = vm.student.studentPersonalId;  //revisar debe ser  no vm.enrollment.id sino oldEnrollment.id
                                        vm.student.firstName = oldStudent.firstName;
                                        App.initAjax();
                                        setTimeout(function () { $("#studentPersonalId").focus(); }, 100);
                                        setTimeout(function () { $("#studentFirstName").focus(); }, 200);
                                        $scope.$apply();
                                    }
                                });
                        }
                    });
                }
            });


            $scope.$watch("vm.student.levelId", function (newValue, oldValue) {
                if (newValue != undefined && newValue != null && newValue != oldValue) {
                    courseService.getAllCoursesByLevelsForCombo(newValue).then(function (result) {
                        vm.courses = result.data;
                    });
                }
            });

            $scope.$watch("vm.student.dateOfBirthTemp", function (newValue, oldValue) {
                vm.student.dateOfBirth = convertDateFormat_DMY_to_YMD(newValue, 4);
            });


            vm.genders = [];
            vm.getGenders = function () {
                genderService.getAllGendersForCombo().then(function (result) {
                    vm.genders = result.data;
                    App.initAjax();
                });
            }

            vm.bloods = [];
            vm.getBloods = function () {
                bloodService.getAllBloodsForCombo().then(function (result) {
                    vm.bloods = result.data;
                    App.initAjax();
                });
            }

            $scope.test = function () {
                var dato = $scope;
            }

            vm.illnesses = [];
            vm.getIllnesses = function () {
                illnessService.getAllIllnessesForCombo().then(function (result) {
                    vm.illnesses = result.data;
                    App.initAjax();
                });
            }

            vm.allergies = [];
            vm.getAllergies = function () {
                allergyService.getAllAllergiesForCombo().then(function (result) {
                    vm.allergies = result.data;
                    App.initAjax();
                });
            }

            vm.relationships = [];
            vm.getRelationships = function () {
                relationshipService.getAllRelationshipsForCombo().then(function (result) {
                    vm.relationships = result.data;
                    App.initAjax();
                });
            }


            vm.levels = [];
            vm.getLevels = function () {
                levelService.getAllLevelsForCombo().then(function (result) {
                    vm.levels = result.data;
                    App.initAjax();
                });
            }

            vm.courses = [];
            vm.getCourses = function () {
                courseService.getAllCoursesForCombo().then(function (result) {
                    vm.courses = result.data;
                    App.initAjax();
                });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getIllnesses();
            vm.getAllergies();
            vm.getGenders();
            vm.getBloods();
            vm.getRelationships();
            vm.getLevels();
            vm.getCourses();


            App.initAjax();
            setTimeout(function () { $("#studentFirstName").focus(); }, 100);

            $('.datepickerme').datepicker({
                format: "dd/mm/yyyy",
                appendText: "(dd/mm/yyyy)",
                language: "es"
            });

            function validateForm() {
                var valid = false;
                try {
                    if (getObjectOrString(vm.student.emailAddress).length > 0 && !isValidEmail(vm.student.emailAddress)) {
                        abp.message.error(App.localize("EmailIsInvalid"));
                    }
                    else if (!validatePersonalId(vm.student.fatherPersonalId, true)) {
                        abp.message.error(App.localize("FathersPersonalIdIsInvalid"));
                    }
                    else if (!validatePersonalId(vm.student.motherPersonalId, true)) {
                        abp.message.error(App.localize("MothersPersonalIdIsInvalid"));
                    }
                    else if (getObjectOrString(vm.student.fatherPersonalId).length > 0
                        && getObjectOrString(vm.student.fatherName).length === 0) {
                        abp.message.error(App.localize("YouTypedTheFatherPersonalIdYouMustTypetheFatherName"));
                    }
                    else if (getObjectOrString(vm.student.motherPersonalId).length > 0
                        && getObjectOrString(vm.student.motherName).length === 0) {
                        abp.message.error(App.localize("YouTypedTheMotherPersonalIdYouMustTypetheMotherName"));
                    }
                    else if (vm.student.dateOfBirth == null) {
                        abp.message.error(App.localize("TheDateOfBirthIsInvalid"));
                    }
                    else if (!isDateDMY(vm.student.dateOfBirthTemp)) {
                        abp.message.error(App.localize("TheDateOfBirthIsInvalid"));
                    }
                    else valid = true;

                } catch (e) {
                    console.log(e);
                    abp.message.error(JSON.stringify(e));
                }
                return valid;
            }
        }
    ]);
})();

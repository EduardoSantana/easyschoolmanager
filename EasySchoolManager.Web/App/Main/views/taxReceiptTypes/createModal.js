(function () {
    angular.module('MetronicApp').controller('app.views.taxReceiptTypes.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.taxReceiptType', 
        function ($scope, $uibModalInstance, taxReceiptTypeService ) {
            var vm = this;
            vm.saving = false;

            vm.taxReceiptType = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     taxReceiptTypeService.create(vm.taxReceiptType)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#taxReceiptTypeName").focus(); }, 100);
        }
    ]);
})();

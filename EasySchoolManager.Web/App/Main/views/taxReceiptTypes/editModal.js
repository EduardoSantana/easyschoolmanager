(function () {
    angular.module('MetronicApp').controller('app.views.taxReceiptTypes.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.taxReceiptType', 'id', 
        function ($scope, $uibModalInstance, taxReceiptTypeService, id ) {
            var vm = this;
			vm.saving = false;

            vm.taxReceiptType = {
                isActive: true
            };
            var init = function () {
                taxReceiptTypeService.get({ id: id })
                    .then(function (result) {
                        vm.taxReceiptType = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#taxReceiptTypeName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						taxReceiptTypeService.update(vm.taxReceiptType)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.evaluationPeriods.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.evaluationPeriod','settings',
        function ($scope, $timeout, $uibModal, evaluationPeriodService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getEvaluationPeriods(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.evaluationPeriods = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 90,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openEvaluationPeriodEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 60
                    },

                                        {
                    name: App.localize('EvaluationPeriodName'),
                    field: 'name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('EvaluationPeriodPosition'),
                    field: 'position',
                    minWidth: 125
                    },
                    {
                    name: App.localize('EvaluationPeriodInitialMonth'),
                    field: 'initialMonth',
                    minWidth: 125
                    },
                    {
                    name: App.localize('EvaluationPeriodFinalMonth'),
                    field: 'finalMonth',
                    minWidth: 125
                    },
                    {
                    name: App.localize('EvaluationPeriodPeriod'),
                    field: 'periods.period',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 90
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getEvaluationPeriods(showTheLastPage) {
                evaluationPeriodService.getAllEvaluationPeriods($scope.pagination).then(function (result) {
                    vm.evaluationPeriods = result.data.items;

                    $scope.gridOptions.data = vm.evaluationPeriods;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openEvaluationPeriodCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/evaluationPeriods/createModal.cshtml',
                    controller: 'app.views.evaluationPeriods.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getEvaluationPeriods(false);
                });
            };

            vm.openEvaluationPeriodEditModal = function (evaluationPeriod) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/evaluationPeriods/editModal.cshtml',
                    controller: 'app.views.evaluationPeriods.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return evaluationPeriod.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getEvaluationPeriods(false);
                });
            };

            vm.delete = function (evaluationPeriod) {
                abp.message.confirm(
                    "Delete evaluationPeriod '" + evaluationPeriod.name + "'?",
                    function (result) {
                        if (result) {
                            evaluationPeriodService.delete({ id: evaluationPeriod.id })
                                .then(function (result) {
                                    getEvaluationPeriods(false);
                                    abp.notify.info("Deleted evaluationPeriod: " + evaluationPeriod.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getEvaluationPeriods(false);
            };

            getEvaluationPeriods(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.evaluationPeriods.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.evaluationPeriod', 'abp.services.app.period',
        function ($scope, $uibModalInstance, evaluationPeriodService , periodService) {
            var vm = this;
            vm.saving = false;

            vm.evaluationPeriod = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     evaluationPeriodService.create(vm.evaluationPeriod)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.periods = [];
            vm.getPeriods	 = function()
            {
                periodService.getAllPeriodsForCombo().then(function (result) {
                    vm.periods = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			                vm.getPeriods();

		    App.initAjax();
			setTimeout(function () { $("#evaluationPeriodName").focus(); }, 100);
        }
    ]);
})();

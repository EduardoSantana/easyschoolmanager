(function () {
    angular.module('MetronicApp').controller('app.views.subjectHighs.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.subjectHigh', 'id', 'abp.services.app.course',
        function ($scope, $uibModalInstance, subjectHighService, id , courseService) {
            var vm = this;
			vm.saving = false;

            vm.subjectHigh = {
                isActive: true
            };
            var init = function () {
                subjectHighService.get({ id: id })
                    .then(function (result) {
                        vm.subjectHigh = result.data;
						                vm.getCourses();

						App.initAjax();
						setTimeout(function () { $("#subjectHighName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						subjectHighService.update(vm.subjectHigh)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.courses = [];
            vm.getCourses	 = function()
            {
                courseService.getAllCoursesByLevelsForCombo(3).then(function (result) {
                    vm.courses = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

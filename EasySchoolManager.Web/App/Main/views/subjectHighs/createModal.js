(function () {
	angular.module('MetronicApp').controller('app.views.subjectHighs.createModal', [
		'$scope', '$uibModalInstance', 'abp.services.app.subjectHigh', 'abp.services.app.course',
		function ($scope, $uibModalInstance, subjectHighService , courseService) {
			var vm = this;
			vm.saving = false;

			vm.subjectHigh = {
				isActive: true
			};
			vm.save = function () {
				if (vm.saving === true)
					return;
				vm.saving = true;
				try {
					 subjectHighService.create(vm.subjectHigh)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							console.log(e.data.message);
						});
					} 
					catch (e)
					{
					   vm.saving = false;
					}
			};

			//XXXInsertCallRelatedEntitiesXXX

			vm.courses = [];
			vm.getCourses	 = function()
			{
				   courseService.getAllCoursesByLevelsForCombo(3).then(function (result) {
					vm.courses = result.data;
					App.initAjax();
				});
			}


			
			vm.cancel = function () {
				$uibModalInstance.dismiss({});
			};

			vm.getCourses();

			App.initAjax();
			setTimeout(function () { $("#subjectHighName").focus(); }, 100);
		}
	]);
})();

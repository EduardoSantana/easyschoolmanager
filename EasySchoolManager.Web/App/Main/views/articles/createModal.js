(function () {
    angular.module('MetronicApp').controller('app.views.articles.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.article', 'abp.services.app.articlesType','abp.services.app.unit','abp.services.app.brand',
        function ($scope, $uibModalInstance, articleService , articlesTypeService, unitService, brandService) {
            var vm = this;
            vm.saving = false;

            vm.article = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     articleService.create(vm.article)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.articlesTypes = [];
            vm.getArticlesTypes	 = function()
            {
                articlesTypeService.getAllArticlesTypesForCombo().then(function (result) {
                    vm.articlesTypes = result.data;
					App.initAjax();
                });
            }

            vm.units = [];
            vm.getUnits	 = function()
            {
                unitService.getAllUnitsForCombo().then(function (result) {
                    vm.units = result.data;
					App.initAjax();
                });
            }

            vm.brands = [];
            vm.getBrands	 = function()
            {
                brandService.getAllBrandsForCombo().then(function (result) {
                    vm.brands = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			                vm.getArticlesTypes();
                vm.getUnits();
                vm.getBrands();

		    App.initAjax();
			setTimeout(function () { $("#articleDescription").focus(); }, 100);
        }
    ]);
})();

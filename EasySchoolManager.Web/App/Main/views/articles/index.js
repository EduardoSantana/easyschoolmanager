(function () {
    angular.module('MetronicApp').controller('app.views.articles.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.article','settings',
        function ($scope, $timeout, $uibModal, articleService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getArticles(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.articles = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openArticleEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        '      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('ArticleDescription'),
                    field: 'description',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ArticleReference'),
                    field: 'reference',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ArticlePrice'),
                    field: 'price',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ArticleArticleType'),
                    field: 'articlesTypes_Description',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ArticleUnit'),
                    field: 'units_Description',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ArticleBrand'),
                    field: 'brands_Name',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getArticles(showTheLastPage) {
                articleService.getAllArticles($scope.pagination).then(function (result) {
                    vm.articles = result.data.items;

                    $scope.gridOptions.data = vm.articles;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openArticleCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/articles/createModal.cshtml',
                    controller: 'app.views.articles.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getArticles(false);
                });
            };

            vm.openArticleEditModal = function (article) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/articles/editModal.cshtml',
                    controller: 'app.views.articles.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return article.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getArticles(false);
                });
            };

            vm.delete = function (article) {
                abp.message.confirm(
                    "Delete article '" + article.name + "'?",
                    function (result) {
                        if (result) {
                            articleService.delete({ id: article.id })
                                .then(function (result) {
                                    getArticles(false);
                                    abp.notify.info("Deleted article: " + article.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getArticles(false);
            };

            getArticles(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.articles.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.article', 'id', 'abp.services.app.articlesType','abp.services.app.unit','abp.services.app.brand',
        function ($scope, $uibModalInstance, articleService, id , articlesTypeService, unitService, brandService) {
            var vm = this;
			vm.saving = false;

            vm.article = {
                isActive: true
            };
            var init = function () {
                articleService.get({ id: id })
                    .then(function (result) {
                        vm.article = result.data;
						                vm.getArticlesTypes();
                vm.getUnits();
                vm.getBrands();

						App.initAjax();
						setTimeout(function () { $("#articleDescription").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						articleService.update(vm.article)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.articlesTypes = [];
            vm.getArticlesTypes	 = function()
            {
                articlesTypeService.getAllArticlesTypesForCombo().then(function (result) {
                    vm.articlesTypes = result.data;
					App.initAjax();
                });
            }

            vm.units = [];
            vm.getUnits	 = function()
            {
                unitService.getAllUnitsForCombo().then(function (result) {
                    vm.units = result.data;
					App.initAjax();
                });
            }

            vm.brands = [];
            vm.getBrands	 = function()
            {
                brandService.getAllBrandsForCombo().then(function (result) {
                    vm.brands = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.commentTypes.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.commentType', 'id', 
        function ($scope, $uibModalInstance, commentTypeService, id ) {
            var vm = this;
			vm.saving = false;

            vm.commentType = {
                isActive: true
            };
            var init = function () {
                commentTypeService.get({ id: id })
                    .then(function (result) {
                        vm.commentType = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#commentTypeName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						commentTypeService.update(vm.commentType)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

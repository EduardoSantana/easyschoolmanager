(function () {
    angular.module('MetronicApp').controller('app.views.commentTypes.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.commentType', 
        function ($scope, $uibModalInstance, commentTypeService ) {
            var vm = this;
            vm.saving = false;

            vm.commentType = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     commentTypeService.create(vm.commentType)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#commentTypeName").focus(); }, 100);
        }
    ]);
})();

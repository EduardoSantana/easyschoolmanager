(function () {
    angular.module('MetronicApp').controller('app.views.transactionShops.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.transactionShop', 'id', 'abp.services.app.clientShop',
        function ($scope, $uibModalInstance, transactionShopService, id , clientShopService) {
            var vm = this;
			vm.saving = false;

            vm.transactionShop = {
                isActive: true
            };
            var init = function () {
                transactionShopService.get({ id: id })
                    .then(function (result) {
                        vm.transactionShop = result.data;
						                vm.getClientShops();

						App.initAjax();
						setTimeout(function () { $("#transactionShopSequence").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						transactionShopService.update(vm.transactionShop)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.clientShops = [];
            vm.getClientShops	 = function()
            {
                clientShopService.getAllClientShopsForCombo().then(function (result) {
                    vm.clientShops = result.data;
					App.initAjax();
                });
            }
            			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();

         
        }
    ]);
})();

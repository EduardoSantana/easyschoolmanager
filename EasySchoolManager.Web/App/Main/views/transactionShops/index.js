(function () {
    angular.module('MetronicApp').controller('app.views.transactionShops.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.transactionShop','settings',
        function ($scope, $timeout, $uibModal, transactionShopService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getTransactionShops(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.transactionShops = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                      //  '      <li><a ng-click="grid.appScope.openTransactionShopEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        '      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                    name: App.localize('Id'),
                    field: 'sequence',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Date'),
                    field: 'date',
                    minWidth: 125,
                    cellFilter: 'date:"dd-MM-yyyy\"'
                    },
                    {
                    name: App.localize('Client'),
                    field: 'clientShops_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Amount'),
                    field: 'amount',
                    minWidth: 125,
                    cellFilter: 'number:2'
                    },                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getTransactionShops(showTheLastPage) {
                transactionShopService.getAllTransactionShops($scope.pagination).then(function (result) {
                    vm.transactionShops = result.data.items;

                    $scope.gridOptions.data = vm.transactionShops;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openTransactionShopCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/transactionShops/createModal.cshtml',
                    controller: 'app.views.transactionShops.createModal as vm',
                    size: 'lg',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getTransactionShops(false);
                });
            };

            vm.openTransactionShopEditModal = function (transactionShop) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/transactionShops/editModal.cshtml',
                    controller: 'app.views.transactionShops.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return transactionShop.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getTransactionShops(false);
                });
            };

            vm.delete = function (transactionShop) {
                abp.message.confirm(
                    "Delete transactionShop '" + transactionShop.name + "'?",
                    function (result) {
                        if (result) {
                            transactionShopService.delete({ id: transactionShop.id })
                                .then(function (result) {
                                    getTransactionShops(false);
                                    abp.notify.info("Deleted transactionShop: " + transactionShop.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getTransactionShops(false);
            };

            getTransactionShops(false);
        }
    ]);
})();

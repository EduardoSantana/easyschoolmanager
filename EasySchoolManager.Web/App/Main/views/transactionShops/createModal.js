(function () {
    angular.module('MetronicApp').controller('app.views.transactionShops.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.transactionShop', 'abp.services.app.clientShop', 'abp.services.app.articleShop', 'abp.services.app.inventoryShop', 'report', '$http',
        function ($scope, $uibModalInstance, transactionShopService, clientShopService, articleShopService, inventoryShopService, report, $http) {
            var vm = this;
            vm.saving = false;

            var todayDate = getDateYMD(new Date());

            vm.transactionShop = {
                isActive: true,
                transactionTypeId: 1, //Recibo
                statusId: 1,  //Creado
                originId: 1,
                paymentMethodId: 1,
                date: todayDate

            };

            vm.articlesShopsLines = [[]];

            vm.transactionShop.dateTemp = convertDateFormat_YMD_to_DMY(todayDate, 4);
            vm.transactionShop.date = todayDate;

            vm.transactionShop.articleShops = [];

            $scope.getArticleShopsByName = function (userInputString) {
                $scope.pagination.maxResultCount = 10;
                $scope.pagination.textFilter = userInputString;
                return articleShopService.getAllArticleShops($scope.pagination);
            }

            $scope.returnedArticleShop = function (result) {
                if (!result) return;
                vm.articleShop = result.originalObject;
                vm.articleShop.quantity = 1;
            }

            $scope.$watch("vm.articleShop.sequence", function (newValue, oldValue) {
                if (newValue == null) {
                    vm.articleShop = {};
                }
                else {
                    if (vm.articleShopIdIsFocused == true)
                        vm.searArticleShopById();
                }
            });

            vm.searArticleShopById = function () {
                articleShopService.getSpecific(vm.articleShop.sequence).then(function (result) {

                    if (result.data != null) {
                        var resultado = { originalObject: result.data };
                        inventoryShopService.getPrice(result.data.id).then(function (result) {
                            
                            var prices = result.data;
                            if (prices == null) {
                                abp.message.error(App.localize("ThisArticleDoesNotHavePriceRegisteredForthisSchool"));
                                return;
                            }

                            resultado.originalObject.price = prices.price;

                            $scope.returnedArticleShop(resultado);
                            $scope.$broadcast('angucomplete-alt:changeInput', 'ArticleShopAngu', vm.articleShop.description);
                        });


                    }

                });
            }
            
            vm.removeArticleShop = function (articleShop) {
                abp.message.confirm(App.localize("AreYouSureYouWantToRemoveTheArticle"), function (result) {
                    if (result == true) {
                        var index = vm.getIndex(articleShop.id, vm.transactionShop.articleShops);
                        vm.transactionShop.articleShops.splice(index, 1);
                        calculateTotal();
                        refresArticleShops();
                        try {
                            $scope.$apply();
                        } catch (e) { }
                    }
                });
            }

            vm.getIndex = function (id, array) {
                var index = -1;

                for (var i = 0; i < array.length; i++) {
                    var item = array[i];
                    if (item.id == id) {
                        index = i;
                        break;
                    }
                }
                return index;
            }
            
            vm.getTempArticleFromLocalList = function (articleId) {
                var currentArticle = null;

                var articles = vm.transactionShop.articleShops.filter(function (x) { return x.id == articleId });
                if (articles.length > 0)
                    currentArticle = articles[0];
                return currentArticle;
            }

            vm.appendArticleShop = function () {

                //Validamos si el articulo puede ser insertado o no en la lista de articulos
                if (!validateArticleShop())
                    return;

                vm.articleShop.total = getDoubleOrCero(vm.articleShop.price * vm.articleShop.quantity);

                //entre aqui
                var existingArticle = vm.getTempArticleFromLocalList(vm.articleShop.id);
                if (existingArticle != null) {
                    existingArticle.quantity = parseInt(existingArticle.quantity) + parseInt(vm.articleShop.quantity);
                    existingArticle.total = getDoubleOrCero(existingArticle.price * existingArticle.quantity);
                } else {
                    vm.transactionShop.articleShops.push(vm.articleShop);
                }
                //y aqui

                vm.articleShop = {};

                $scope.$broadcast('angucomplete-alt:clearInput', 'ArticleShopAngu');
                refresArticleShops();
                calculateTotal();
                setTimeout(function () { document.getElementById("transactionArticleId").focus(); }, 500);
            }

            vm.getArticleIndex = function (articleId) {
                var indice = -1;
                for (var i = 0; i < vm.transactionShop.articleShops.length; i++) {
                    var ca = vm.transactionShop.articleShops[i];
                    if (ca.id == articleId) {
                        indice = i;
                        break
                    }
                }
                return indice;
            }

            vm.appendArticleShopFromButtonPosition = function (article) {
                vm.articleShop = $.extend({}, article);

                //Los precios de los articulos son cargados cuando se cargan los button positions
                // y cuando se hace click en uno de los buton positions, solo se busca el arreglo de 
                // inventory shop y se toma el primero, ( que solo debe haber uno) para adquirir su precio
                if (!article.inventoryShops || article.inventoryShops.length == 0) {
                    abp.message.error(App.localize("ThisArticleDoesNotHavePriceRegisteredForthisSchool"));
                    return;
                }
                else {
                    vm.articleShop.price = article.inventoryShops[0].price;
                }

                //aqui buscar precio para los botones *** PENDIENTE HACER**** 
               // inventoryShopService.getPriceSeq(article.sequence).then(function (result) {
               // var prices = result.data;
               // vm.articleShop.price = prices.price;});            
                //hasta aqui buscar precio

                vm.articleShop.quantity = 1;
                vm.articleShop.total = getDoubleOrCero(vm.articleShop.price * vm.articleShop.quantity);

                //entre aqui
                var existingArticle = vm.getTempArticleFromLocalList(vm.articleShop.id);
                if (existingArticle != null) {
                    existingArticle.quantity = parseInt(existingArticle.quantity) + parseInt(vm.articleShop.quantity);
                    existingArticle.total = getDoubleOrCero(existingArticle.price * existingArticle.quantity);
                    var index = vm.getArticleIndex(article.id);
                    vm.transactionShop.articleShops.splice(index, 1, $.extend({}, existingArticle));
                } else {
                    vm.transactionShop.articleShops.push($.extend({}, vm.articleShop));
                }
                //y aqui

                vm.articleShop = {};

                $scope.$broadcast('angucomplete-alt:clearInput', 'ArticleShopAngu');
                refresArticleShops();
                calculateTotal();
                $("#transactionArticleId").focus();              
            }

            function validateArticleShop() {

                var validated = false;
                if (getDoubleOrCero(vm.articleShop.price) == 0.00) {
                    abp.message.info(App.localize("YouCanNotSellAnArticleWithThatPrice"));
                }
                else if (getIntOrCero(vm.articleShop.quantity) == 0) {

                    abp.message.info(App.localize("YouCanNotSellAnArticleWithQuantityInZero"));
                }
                else if (getIntOrCero(vm.articleShop.id) == 0) {

                    abp.message.info(App.localize("YouMustSelectAnArticleBeforeAddToTheArticleList"));
                }
                else
                    validated = true;


                return validated;
            }

            function refresArticleShops() {

                //   $scope.gridArticleOptions.data = vm.purchaseShop.articleShops;

            }

            function getButtonPositions() {
                for (var i = 0; i < 46; i++) {
                    var res = articleShopService.getButtonPositions(i);
                    if (i == 1) { }
                }

            }

            function getCalculatedTotalWithArticleShops() {
                var total = 0.00;

                for (var i = 0; i < vm.transactionShop.articleShops.length; i++) {
                    var ar = vm.transactionShop.articleShops[i];
                    total += getDoubleOrCero(ar.total);
                }

                return total;
            }
            
            $scope.calculateTotal = function () {

                vm.transactionShop.totalCalculed = getCalculatedTotalWithArticleShops();
                vm.transactionShop.amount = vm.transactionShop.totalCalculed;
            }
            
            calculateTotal = function () {

                vm.transactionShop.totalCalculed = getCalculatedTotalWithArticleShops();
                vm.transactionShop.amount = vm.transactionShop.totalCalculed;
                App.initAjax();
                // $scope.$apply();
            }

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    transactionShopService.createAndPrint(vm.transactionShop)
                        .then(function (result) {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                            debugger;
                            printReceipt(result.data);
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            //XXXInsertCallRelatedEntitiesXXX

            vm.clientShops = [];
            vm.getClientShops = function () {
                clientShopService.getAllClientShopsForCombo().then(function (result) {
                    vm.clientShops = result.data;
                    App.initAjax();
                });
            }

            vm.enterNewArticle = function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    vm.appendArticleShop();

                    setTimeout(function () { document.getElementById("transactionArticleId").focus(); }, 500);
                }
            }

            $scope.enterNewArticle = function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    vm.appendArticleShop();
                }
            }

            vm.getAllArticlesForButtonPositions = function () {
                transactionShopService.getAllArticlesShops().then(function (result) {
                    vm.articlesShopsLines = result.data;
                });
            };

            vm.art2 = function () {
                abp.message.info(App.localize("Selecci�n art�culo 2"));
            };

            vm.art3 = function () {
                abp.message.info(App.localize("Selecci�n art�culo 3"));
            };

            vm.art4 = function () {
                abp.message.info(App.localize("Selecci�n art�culo 4"));
            };

            vm.art5 = function () {
                abp.message.info(App.localize("Selecci�n art�culo 5"));
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getClientShops();

            App.initAjax();

            setTimeout(function () { $("#transactionShopSequence").focus(); }, 100);

            function printReceipt(result) {
                var config = {
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                };
           
                $http.post('http://localhost:8081', "||" + result.reportString, config)
                    .then(function () {

                    },
                    function () {

                        var objectReport = {};
                        var reportsFilters = [];

                        objectReport.reportCode = "62";

                        objectReport.reportExport = "PDF";
                        reportsFilters.push({ name: "TransactionShopId", value: result.id.toString() });

                        objectReport.reportsFilters = reportsFilters;
                        report.print(objectReport);
                        abp.message.success("");
                        swal.close();

                        //TODO : hacer que bolier plate no maneje el error de ajax
                    });
            }

            vm.getAllArticlesForButtonPositions();
        }
    ]);
})();

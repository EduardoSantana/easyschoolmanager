﻿(function () {
    angular.module('MetronicApp').controller('app.views.courses.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.course', 'abp.services.app.level', 'id',
        function ($scope, $uibModalInstance, courseService, levelService, id) {
            var vm = this;

            vm.course = {
                isActive: true
            };

            vm.levels = [];

            var init = function () {
                vm.getLevels();
                courseService.get({ id: id })
                    .then(function (result) {
                        vm.course = result.data;

                        App.initAjax();
                        setTimeout(function () { $("#courseName").focus(); }, 100);
                    });
            }

            vm.save = function () {
                courseService.update(vm.course)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });
            };

            vm.getLevels = function () {
                levelService.getAllLevelsForCombo().then(function (result) {
                    vm.levels = result.data;
                    App.initAjax();
                });

            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();
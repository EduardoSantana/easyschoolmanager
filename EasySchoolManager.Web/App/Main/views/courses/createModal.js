﻿(function () {
    angular.module('MetronicApp').controller('app.views.courses.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.course', 'abp.services.app.level',
        function ($scope, $uibModalInstance, courseService, levelService) {
            var vm = this;
            vm.saving = false;

            vm.course = {
                isActive: true,
                name: null
            };


            vm.save = function () {
                vm.saving = true;
                try {
                    courseService.create(vm.course)
                        .then(function () {
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            vm.saving = false;
                            $uibModalInstance.close();
                        });
                } catch (e) {
                    vm.saving = false;
                }
            };

            vm.levels = [];
            vm.getLevels = function () {
                levelService.getAllLevelsForCombo().then(function (result) {
                    vm.levels = result.data;
                    App.initAjax();

                });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getLevels();

            setTimeout(function () { $("#courseName").focus(); }, 100);
        }
    ]);
})();
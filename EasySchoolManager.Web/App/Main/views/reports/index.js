(function () {
    angular.module('MetronicApp').controller('app.views.reports.index', [
        '$scope', '$uibModal', 'abp.services.app.report', '$interval',
        function ($scope, $uibModal, reportService, $interval) {
            var vm = this;

            vm.reports = [];

            vm.report;

            vm.getEditionId = function (record) {
                return parseInt(record.valueId);
            };

            function getreports() {
                reportService.getAllReports({}).then(function (result) {
                    vm.reports = result.data.items;
                });
            }

            vm.delete = function (report) {
                abp.message.confirm(
                    "Delete report '" + report.name + "'?",
                    function (result) {
                        if (result) {
                            reportService.delete({ id: report.id })
                                .then(function () {
                                    abp.notify.info("Deleted report: " + report.name);
                                    getreports();
                                });
                        }
                    });
            }

            vm.refresh = function () {
                getreports();
            };

            getreports();
        }
    ]);
})();
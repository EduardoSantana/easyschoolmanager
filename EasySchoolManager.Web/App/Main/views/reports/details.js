﻿(function () {
    angular.module('MetronicApp').controller('app.views.reports.details', [
        '$scope', '$uibModal', 'abp.services.app.report', '$interval', '$stateParams', 'appSession',
        function ($scope, $uibModal, reportService, $interval, $stateParams, appSession) {
            var vm = this;
            vm.exports = [{ name: "PDF", value: "PDF" }, { name: "Excel", value: "Excel" }];

            vm.TenantId = 0;
            if (appSession.tenant != null) {
                vm.TenantId = appSession.tenant.id;
            }

            vm.report;

            vm.getEditionId = function (record) {
                return parseInt(record.valueId);
            };

            function getreports() {
                reportService.get({ id: $stateParams.id })
                    .then(function (result) {
                        vm.report = result.data;
                        vm.report.reportExport = vm.exports[0];

                    });

            };

            vm.translate = function (name) {
                return App.localize(name);
            };

            vm.delete = function (report) {
                abp.message.confirm(
                    "Delete report '" + report.name + "'?",
                    function (result) {
                        if (result) {
                            reportService.delete({ id: report.id })
                                .then(function () {
                                    abp.notify.info("Deleted report: " + report.name);
                                    getreports();
                                });
                        }
                    });
            };

            vm.refresh = function () {
                getreports();
            };

            vm.showReports = function showReports(e) {

                if (!$("#frmReportesV1").valid()) {
                    return;
                }
            };
            getreports();
        }
    ]);
})();
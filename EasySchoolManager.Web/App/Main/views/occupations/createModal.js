(function () {
    angular.module('MetronicApp').controller('app.views.occupations.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.occupation', 
        function ($scope, $uibModalInstance, occupationService ) {
            var vm = this;
            vm.saving = false;

            vm.occupation = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     occupationService.create(vm.occupation)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#occupationName").focus(); }, 100);
        }
    ]);
})();

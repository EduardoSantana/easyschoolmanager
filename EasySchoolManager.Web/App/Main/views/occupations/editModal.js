(function () {
    angular.module('MetronicApp').controller('app.views.occupations.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.occupation', 'id', 
        function ($scope, $uibModalInstance, occupationService, id ) {
            var vm = this;
			vm.saving = false;

            vm.occupation = {
                isActive: true
            };
            var init = function () {
                occupationService.get({ id: id })
                    .then(function (result) {
                        vm.occupation = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#occupationName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						occupationService.update(vm.occupation)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

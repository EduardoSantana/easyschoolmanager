(function () {
    angular.module('MetronicApp').controller('app.views.occupations.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.occupation','settings',
        function ($scope, $timeout, $uibModal, occupationService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getOccupations(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.occupations = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
                   
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openOccupationEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('OccupationName'),
                    field: 'name',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getOccupations(showTheLastPage) {
                occupationService.getAllOccupations($scope.pagination).then(function (result) {
                    vm.occupations = result.data.items;

                    $scope.gridOptions.data = vm.occupations;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openOccupationCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/occupations/createModal.cshtml',
                    controller: 'app.views.occupations.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getOccupations(true);
                });
            };

            vm.openOccupationEditModal = function (occupation) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/occupations/editModal.cshtml',
                    controller: 'app.views.occupations.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return occupation.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getOccupations(false);
                });
            };

            vm.delete = function (occupation) {
                abp.message.confirm(
                    "Delete occupation '" + occupation.name + "'?",
                    function (result) {
                        if (result) {
                            occupationService.delete({ id: occupation.id })
                                .then(function (result) {
                                    getOccupations(false);
                                    abp.notify.info("Deleted occupation: " + occupation.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getOccupations(false);
            };

            getOccupations(false);
        }
    ]);
})();

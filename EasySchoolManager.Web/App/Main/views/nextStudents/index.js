(function () {
    angular.module('MetronicApp').controller('app.views.nextStudents.index', [
        '$scope', '$timeout', '$uibModal', '$uibModalInstance', 'abp.services.app.student', 'settings',
        function ($scope, $timeout, $uibModal, $uibModalInstance, studentService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.enrollmentId = $scope.$resolve.enrollment.id;
            vm.enrollment = $scope.$resolve.enrollment;
            $scope.pagination.reload = function () {
                getStudents(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.students = [];

            vm.permissions = {
                delet: abp.auth.hasPermission('Pages.Students.NextDelete')
            };

            $scope.gridOptions = {
                rowHeight: 39,
                appScopeProvider: vm,
                columnDefs: [

                    {
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '    <button class="btn btn-xs btn-default gray fa-grid-icon1" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.openStudentEditModal(row.entity)"><i class="fa fa-pencil"></i></button>' +
                        '    <button ng-if="grid.appScope.permissions.delet" class="btn btn-xs btn-default gray fa-grid-icon1 red" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false" ng-click="grid.appScope.delete(row.entity)"><i class="fa fa-close"></i></button>' +
                        '</div>'
                    },
                    {
                        name: App.localize('StudentFirstName'),
                        field: 'firstName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('StudentLastName'),
                        field: 'lastName',
                        minWidth: 125
                    },
                    {
                        name: App.localize('StudentDateOfBirth'),
                        field: 'dateOfBirth',
                        minWidth: 125,
                        cellFilter: 'date:"dd-MM-yyyy\"'
                    },

                    {
                        name: App.localize('StudentGender'),
                        field: 'genders_Name',
                        minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
                ]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getStudents(showTheLastPage) {
                
                studentService.getAllStudentForEnrollment($scope.enrollmentId,2).then(function (result) {
                    vm.students = result.data.items;

                    $scope.gridOptions.data = vm.students;

                     $scope.pagination.assignTotalRecords(result.data.totalCount);
                    // if (showTheLastPage)
                    //   $scope.pagination.last();
               });
                
            }

            vm.openStudentCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/nextStudents/createModal.cshtml',
                    controller: 'app.views.nextStudents.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getStudents(true);
                });
            };

            vm.openStudentEditModal = function (student) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/nextStudents/editModal.cshtml',
                    controller: 'app.views.nextStudents.editModal as vm',
                    backdrop: 'static',
                    size:'lg',
                    resolve: {
                        id: function () {
                            return student.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getStudents(false);
                });
            };

            vm.delete = function (student) {
                abp.message.confirm(
                    "Delete student '" + student.firstName + " " + student.lastName + "'?",
                    function (result) {
                        if (result) {
                            studentService.delete({ id: student.id })
                                .then(function (result) {
                                    getStudents(false);
                                    abp.notify.info("Deleted student: " + student.firtName + " " + student.lastName);

                                });
                        }
                    });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.refresh = function () {
                getStudents(false);
            };

            getStudents(false);
        }
    ]);
})();

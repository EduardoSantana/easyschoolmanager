(function () {
    angular.module('MetronicApp').controller('app.views.paymentDates.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.paymentDate', 'id', 
        function ($scope, $uibModalInstance, paymentDateService, id ) {
            var vm = this;
			vm.saving = false;

            vm.paymentDate = {
                isActive: true
            };
            var init = function () {
                paymentDateService.get({ id: id })
                    .then(function (result) {
                        vm.paymentDate = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#paymentDateSequence").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						paymentDateService.update(vm.paymentDate)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

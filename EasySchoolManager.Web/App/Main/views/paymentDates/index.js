(function () {
    angular.module('MetronicApp').controller('app.views.paymentDates.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.paymentDate','settings',
        function ($scope, $timeout, $uibModal, paymentDateService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getPaymentDates(false);
            }
            vm.paymentDates = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openPaymentDateEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },

                                        {
                    name: App.localize('PaymentDateSequence'),
                    field: 'sequence',
                    minWidth: 125
                    },
                    {
                    name: App.localize('PaymentDatePaymentDay'),
                    field: 'paymentDay',
                    minWidth: 125
                    },
                    {
                    name: App.localize('PaymentDatePaymentMonth'),
                    field: 'monthName',
                    minWidth: 125
                    },
                    {
                    name: App.localize('PaymentDatePaymentYear'),
                    field: 'paymentYear',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getPaymentDates(showTheLastPage) {
                paymentDateService.getAllPaymentDates($scope.pagination).then(function (result) {
                    vm.paymentDates = result.data.items;


                    for (var i = 0; i < vm.paymentDates.length; i++) {
                        var m = vm.paymentDates[i];
                        m.monthName = getMonthByNumber(m.paymentMonth);
                    }


                    $scope.gridOptions.data = vm.paymentDates;

                    


                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openPaymentDateCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/paymentDates/createModal.cshtml',
                    controller: 'app.views.paymentDates.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getPaymentDates(true);
                });
            };

            vm.openPaymentDateEditModal = function (paymentDate) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/paymentDates/editModal.cshtml',
                    controller: 'app.views.paymentDates.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return paymentDate.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getPaymentDates(false);
                });
            };

            vm.delete = function (paymentDate) {
                abp.message.confirm(
                    "Delete paymentDate '" + paymentDate.name + "'?",
                    function (result) {
                        if (result) {
                            paymentDateService.delete({ id: paymentDate.id })
                                .then(function (result) {
                                    getPaymentDates(false);
                                    abp.notify.info("Deleted paymentDate: " + paymentDate.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getPaymentDates(false);
            };

            getPaymentDates(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.paymentDates.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.paymentDate', 
        function ($scope, $uibModalInstance, paymentDateService ) {
            var vm = this;
            vm.saving = false;

            vm.paymentDate = {
                isActive: true,
                initialYear: new Date().getFullYear()
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    paymentDateService.generatePaymentDateList(vm.paymentDate)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
            setTimeout(function () { $("#paymentDatePaymentDay").focus(); }, 100);
        }
    ]);
})();

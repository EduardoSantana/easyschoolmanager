(function () {
    angular.module('MetronicApp').controller('app.views.courseSessions.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.courseSession','settings',
        function ($scope, $timeout, $uibModal, courseSessionService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getCourseSessions(false);
            }


            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.courseSessions = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
                    
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openCourseSessionEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('Course'),
                    field: 'courses_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('CourseSessionName'),
                    field: 'name',
                    minWidth: 125
                    },
                    
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getCourseSessions(showTheLastPage) {
                courseSessionService.getAllCourseSessions($scope.pagination).then(function (result) {
                    vm.courseSessions = result.data.items;

                    $scope.gridOptions.data = vm.courseSessions;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openCourseSessionCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/courseSessions/createModal.cshtml',
                    controller: 'app.views.courseSessions.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getCourseSessions(true);
                });
            };

            vm.openCourseSessionEditModal = function (courseSession) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/courseSessions/editModal.cshtml',
                    controller: 'app.views.courseSessions.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return courseSession.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getCourseSessions(false);
                });
            };

            vm.delete = function (courseSession) {
                abp.message.confirm(
                    "Delete courseSession '" + courseSession.name + "'?",
                    function (result) {
                        if (result) {
                            courseSessionService.delete({ id: courseSession.id })
                                .then(function (result) {
                                    getCourseSessions(false);
                                    abp.notify.info("Deleted courseSession: " + courseSession.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getCourseSessions(false);
            };

            getCourseSessions(false);
        }
    ]);
})();

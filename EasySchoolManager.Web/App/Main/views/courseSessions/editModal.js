(function () {
    angular.module('MetronicApp').controller('app.views.courseSessions.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.courseSession', 'id', 'abp.services.app.course',
        function ($scope, $uibModalInstance, courseSessionService, id, courseService) {
            var vm = this; vm.saving = false;

            vm.courseSession = {
                isActive: true
            };

            var init = function () {
                courseSessionService.get({ id: id })
                    .then(function (result) {
                        vm.courseSession = result.data;


                        vm.getCourses();

                        App.initAjax();
                        setTimeout(function () { $("#courseSessionCourseId").focus(); }, 100);
                        setTimeout(function () { $("#courseSessionCourse2").focus(); }, 100);
                    });
            }

            vm.save = function () {
                courseSessionService.update(vm.courseSession)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });
            };


            //XXXInsertCallRelatedEntitiesXXX

            vm.courses = [];
            vm.getCourses = function () {
                courseService.getAllCoursesForCombo().then(function (result) {
                    vm.courses = result.data;
                    App.initAjax();
                });
            }





            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

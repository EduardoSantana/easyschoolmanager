(function () {
    angular.module('MetronicApp').controller('app.views.courseSessions.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.courseSession', 'abp.services.app.course',
        function ($scope, $uibModalInstance, courseSessionService , courseService) {
            var vm = this;

            vm.courseSession = {
                isActive: true
            };

            vm.save = function () {
                vm.saving = true;
                try {
                     courseSessionService.create(vm.courseSession)
                        .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			
			//XXXInsertCallRelatedEntitiesXXX

            vm.courses = [];
            vm.getCourses	 = function()
            {
                courseService.getAllCoursesForCombo().then(function (result) {
                    vm.courses = result.data;
					App.initAjax();
                });
            }


			
			

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };
			
			
			
			                vm.getCourses();

		    App.initAjax();
			
			setTimeout(function () { $("#courseSessionCourseId").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.TransactionQueues.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.TransactionQueue', 'id', 
        function ($scope, $uibModalInstance, TransactionQueueService, id ) {
            var vm = this;
			vm.saving = false;

            vm.TransactionQueue = {
                isActive: true
            };
            var init = function () {
                TransactionQueueService.get({ id: id })
                    .then(function (result) {
                        vm.TransactionQueue = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#TransactionQueueTransactionId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						TransactionQueueService.update(vm.TransactionQueue)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

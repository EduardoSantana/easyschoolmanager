(function () {
    angular.module('MetronicApp').controller('app.views.TransactionQueues.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.TransactionQueue', 
        function ($scope, $uibModalInstance, TransactionQueueService ) {
            var vm = this;
            vm.saving = false;

            vm.TransactionQueue = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     TransactionQueueService.create(vm.TransactionQueue)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#TransactionQueueTransactionId").focus(); }, 100);
        }
    ]);
})();

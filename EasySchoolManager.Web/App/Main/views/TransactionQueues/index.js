(function () {
    angular.module('MetronicApp').controller('app.views.TransactionQueues.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.transactionQueue', 'settings', '$http',
        function ($scope, $timeout, $uibModal, transactionQueueService, settings, $http) {
            var vm = this;
            vm.textFilter = "";
            vm.fromDate = new Date();
            vm.toDate = new Date();

            vm.download = function () {
                debugger;
                abp.ui.setBusy(null, transactionQueueService.downloadFileXml({ fromDate: vm.fromDate, toDate: vm.toDate, tenantId: (vm.tenantId ? vm.tenantId.id : null) }).success(function (result) {
                    // Get XML file token
                    var token = result.file.token;

                    // Get XML file name
                    var filename = result.file.filename;

                    // Do HTTP request to download file
                    $http.post('/file/DownloadXmlFile', { token }, { responseType: 'arraybuffer' }).then(
                        function successCallback(response) {
                            // This callback will be executed asynchronously when the request will be available.
                            var file = new Blob([response.data], { type: 'application/xml' });
                            var fileURL = window.URL.createObjectURL(file);

                            // Create an element to execute the download.
                            var element = document.createElement("a");
                            document.body.appendChild(element);
                            element.style = "display: none";
                            element.href = fileURL;
                            element.download = filename;
                            element.click();

                            abp.notify.success('XML file generated successfully.');
                        },
                        function errorCallback(response) {
                            // This callback will be executed asynchronously if there is an error or if server return an error code.
                            abp.notify.error('Error generating the XML file.');
                        });
                }));
            }

        }
    ]);
})();

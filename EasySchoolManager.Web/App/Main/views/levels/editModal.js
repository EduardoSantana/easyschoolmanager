(function () {
    angular.module('MetronicApp').controller('app.views.levels.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.level', 'id', 
        function ($scope, $uibModalInstance, levelService, id ) {
            var vm = this;

            vm.level = {
                isActive: true
            };

            var init = function () {
                levelService.get({ id: id })
                    .then(function (result) {
                        vm.level = result.data;
						
						
						
						App.initAjax();
						setTimeout(function () { $("#levelName").focus(); }, 100);
                    });
            }

            vm.save = function () {
                levelService.update(vm.level)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });
            };
			
			
			//XXXInsertCallRelatedEntitiesXXX


			
			

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

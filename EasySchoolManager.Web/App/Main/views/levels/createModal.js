(function () {
    angular.module('MetronicApp').controller('app.views.levels.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.level', 
        function ($scope, $uibModalInstance, levelService ) {
            var vm = this;

            vm.level = {
                isActive: true
            };

            vm.save = function () {
                vm.saving = true;
                try {
                     levelService.create(vm.level)
                        .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			
			//XXXInsertCallRelatedEntitiesXXX


			
			

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };
			
			
			
			
		    App.initAjax();
			
			setTimeout(function () { $("#levelName").focus(); }, 100);
        }
    ]);
})();

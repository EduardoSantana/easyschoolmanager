(function () {
    angular.module('MetronicApp').controller('app.views.masterTenants.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.masterTenant', 'id', 'abp.services.app.city', 'abp.services.app.concept', 'abp.services.app.catalog',
        function ($scope, $uibModalInstance, masterTenantService, id , citieService, conceptService, catalogService) {
            var vm = this;
			vm.saving = false;

            vm.masterTenant = {
                isActive: true
            };
            var init = function () {
                masterTenantService.get({ id: id })
                    .then(function (result) {
                        vm.masterTenant = result.data;
                        vm.getCities();
                        vm.getConcepts();
                        vm.getCatalogs();

						App.initAjax();
						setTimeout(function () { $("#masterTenantName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						masterTenantService.update(vm.masterTenant)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.cities = [];
            vm.getCities	 = function()
            {
                citieService.getAllCitiesForCombo().then(function (result) {
                    vm.cities = result.data;
					App.initAjax();
                });
            }

            vm.conceptForPayments = [];
            vm.conceptForInscriptions = [];
            vm.getConcepts = function () {
                conceptService.getAllConceptsForTransactionsCombo().then(function (result) {
                    vm.conceptForPayments = result.data;
                    vm.conceptForInscriptions = result.data;
                    App.initAjax();
                });
            }



            vm.catalogs = [];
            vm.getCatalogs = function () {
                catalogService.getAllCatalogsForCombo().then(function (result) {
                    vm.catalogs = result.data;
                    App.initAjax();
                });
            }

			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

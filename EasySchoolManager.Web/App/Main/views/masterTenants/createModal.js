(function () {
    angular.module('MetronicApp').controller('app.views.masterTenants.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.masterTenant', 'abp.services.app.city', 'abp.services.app.concept',
        function ($scope, $uibModalInstance, masterTenantService , citieService, conceptService) {
            var vm = this;
            vm.saving = false;

            vm.masterTenant = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     masterTenantService.create(vm.masterTenant)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.cities = [];
            vm.getCities	 = function()
            {
                citieService.getAllCitiesForCombo().then(function (result) {
                    vm.cities = result.data;
					App.initAjax();
                });
            }


            vm.conceptForPayments = [];
            vm.conceptForInscriptions = [];
            vm.getConcepts	 = function()
            {
                conceptService.getAllConceptsForTransactionsCombo().then(function (result) {
                    vm.conceptForPayments = result.data;
                    vm.conceptForInscriptions = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getCities();
            vm.getConcepts();

		    App.initAjax();
			setTimeout(function () { $("#masterTenantName").focus(); }, 100);
        }
    ]);
})();

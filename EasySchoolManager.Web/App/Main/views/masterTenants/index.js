(function () {
    angular.module('MetronicApp').controller('app.views.masterTenants.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.masterTenant','settings','abp.services.app.city',
        function ($scope, $timeout, $uibModal, masterTenantService, settings,cityServices) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getMasterTenants(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.masterTenants = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openMasterTenantEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('MasterTenantName'),
                    field: 'name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('MasterTenantShortName'),
                    field: 'shortName',
                    minWidth: 125
                    },
                    {
                    name: App.localize('MasterTenantAddress'),
                    field: 'address',
                    minWidth: 125
                    },
                    {
                    name: App.localize('MasterTenantCity'),
                    field: 'cities_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('MasterTenantPhone1'),
                    field: 'phone1',
                    minWidth: 125
                    },
                    {
                    name: App.localize('MasterTenantPhone2'),
                    field: 'phone2',
                    minWidth: 125
                    },
                    {
                    name: App.localize('MasterTenantPhone3'),
                    field: 'phone3',
                    minWidth: 125
                    },
               
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getMasterTenants(showTheLastPage) {
                masterTenantService.getAllMasterTenants($scope.pagination).then(function (result) {
                    vm.masterTenants = result.data.items;

                    $scope.gridOptions.data = vm.masterTenants;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openMasterTenantCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/masterTenants/createModal.cshtml',
                    controller: 'app.views.masterTenants.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getMasterTenants(true);
                });
            };

            vm.openMasterTenantEditModal = function (masterTenant) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/masterTenants/editModal.cshtml',
                    controller: 'app.views.masterTenants.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return masterTenant.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getMasterTenants(false);
                });
            };

            vm.delete = function (masterTenant) {
                abp.message.confirm(
                    "Delete masterTenant '" + masterTenant.name + "'?",
                    function (result) {
                        if (result) {
                            masterTenantService.delete({ id: masterTenant.id })
                                .then(function (result) {
                                    getMasterTenants(false);
                                    abp.notify.info("Deleted masterTenant: " + masterTenant.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getMasterTenants(false);
            };

            getMasterTenants(false);
        }
    ]);
})();

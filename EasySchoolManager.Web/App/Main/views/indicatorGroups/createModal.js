(function () {
    angular.module('MetronicApp').controller('app.views.indicatorGroups.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.indicatorGroup', 'abp.services.app.subject','abp.services.app.indicatorGroup',
        function ($scope, $uibModalInstance, indicatorGroupService , subjectService, indicatorGroupService) {
            var vm = this;
            vm.saving = false;

            vm.indicatorGroup = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     indicatorGroupService.create(vm.indicatorGroup)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.subjects = [];
            vm.getSubjects	 = function()
            {
                subjectService.getAllSubjectsForCombo().then(function (result) {
                    vm.subjects = result.data;
					App.initAjax();
                });
            }

            vm.indicatorGroups = [];
            vm.getIndicatorGroups	 = function()
            {
                indicatorGroupService.getAllIndicatorGroupsForCombo().then(function (result) {
                    vm.indicatorGroups = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			                vm.getSubjects();
                vm.getIndicatorGroups();

		    App.initAjax();
			setTimeout(function () { $("#indicatorGroupName").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.indicatorGroups.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.indicatorGroup','settings',
        function ($scope, $timeout, $uibModal, indicatorGroupService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getIndicatorGroups(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.indicatorGroups = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openIndicatorGroupEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },
                    {
                    name: App.localize('Name'),
                    field: 'name'
                    },
                    {
                    name: App.localize('Subject'),
                    field: 'subjects_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('ParentIndicatorGroup'),
                    field: 'parentIndicatorGroups_Name',
                    minWidth: 125
                    },
                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getIndicatorGroups(showTheLastPage) {
                indicatorGroupService.getAllIndicatorGroups($scope.pagination).then(function (result) {
                    vm.indicatorGroups = result.data.items;

                    $scope.gridOptions.data = vm.indicatorGroups;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openIndicatorGroupCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/indicatorGroups/createModal.cshtml',
                    controller: 'app.views.indicatorGroups.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getIndicatorGroups(false);
                });
            };

            vm.openIndicatorGroupEditModal = function (indicatorGroup) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/indicatorGroups/editModal.cshtml',
                    controller: 'app.views.indicatorGroups.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return indicatorGroup.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getIndicatorGroups(false);
                });
            };

            vm.delete = function (indicatorGroup) {
                abp.message.confirm(
                    "Delete indicatorGroup '" + indicatorGroup.name + "'?",
                    function (result) {
                        if (result) {
                            indicatorGroupService.delete({ id: indicatorGroup.id })
                                .then(function (result) {
                                    getIndicatorGroups(false);
                                    abp.notify.info("Deleted indicatorGroup: " + indicatorGroup.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getIndicatorGroups(false);
            };

            getIndicatorGroups(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.evaluations.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.evaluation', 'subjectId', 'report',
        function ($scope, $uibModalInstance, evaluationService, subjectId, report) {
            var vm = this;
            vm.saving = false;
            vm.student = $scope.$resolve.student;
            vm.sessionId = $scope.$resolve.sessionId;
            vm.courseId = $scope.$resolve.courseId;
            vm.subjectId = $scope.$resolve.subjectId;
            vm.subject = {};

            vm.isDisabled = function (ind, eva, $ig, $i, $e) {
                var disabled = true;
                if ($e == 0)
                    disabled = false;
                else {
                    var evaAnterior = vm.subject.indicatorGroups[$ig].indicators[$i].evaluations[$e - 1];
                    var evaCurrent = vm.subject.indicatorGroups[$ig].indicators[$i].evaluations[$e];

                    var maxEvaluation = $scope.getMax(vm.evaluationLegends, "sequence");


                    if (evaAnterior.evaluationLegendId && evaAnterior.evaluationLegendId > -1) {
                        disabled = false;
                    }

                    if (evaAnterior.evaluationLegends && evaAnterior.evaluationLegends.sequence === maxEvaluation.sequence)
                        disabled = true;

                }
                return disabled;
            }



            vm.getInitialValues = function () {
                var dataToSend = {
                    subjectId: vm.subjectId,
                    sessionId: vm.sessionId,
                    courseId: vm.courseId,
                    enrollmentStudentId: vm.student.id
                };

                evaluationService.getSubjectFull(dataToSend).then(function (result) {
                    vm.subject = result.data.subjects;
                    vm.evaluationPeriods = result.data.evaluationPeriods;
                    vm.evaluationLegends = result.data.evaluationLegends;
                    vm.evaluationLegends.splice(0, 0, { id: -1, name: '...', sequence: -1 });


                    assignTempValues();



                    App.initAjax();
                    setTimeout(function () { $("#evaluationIndicatorId").focus(); }, 100);
                });
            }

            function assignTempValues() {
                vm.subject.indicatorGroups[0].indicators.forEach(function (ind, i) {
                    ind.evaluations.forEach(function (ev, ex) {
                        ev.evaluationLegendId_Tmp = ev.evaluationLegendId;

                    });
                });
            }

            /**
            * Funcion usada para buscar el evaluation period anterior al que se esta modificando. Esto
            * es realizado para comparar el EvaluationLegend que tiene el anterior, para no poner uno
            * que sea inferior al anterior, este debe ser almenos igual, pero nunca inferior.
            * @param evaluation es la evaluacion actual.
            *
            * @param ind Es el indicador que contiene todos los evaluations.
            */
            function getEvaluationPriorEvaluationPeriod(evaluation, ind) {
                var priorEvaluation = null;
                for (var i = evaluation.evaluationPerios.position; i > 0; i--) {
                    var searchedEvaluation = ind.evaluations[i - 1];
                    if (searchedEvaluation.evaluationPerios.position < evaluation.evaluationPerios.position) {
                        priorEvaluation = searchedEvaluation;
                        break;
                    }
                }
                return priorEvaluation;
            }

            /**
             * Funcion usada para buscar el indicador por el indicator id
             * @param {any} indicatorId id de identificador a bucar
             */
            function getIndicator(indicatorId) {
                var returnedValue = null;
                for (var i = 0; i < vm.subject.indicatorGroups.length; i++) {
                    var ig = vm.subject.indicatorGroups[i];
                    var indicator = getElementByKeyField(ig.indicators, "id", indicatorId);
                    if (indicator != null) {
                        returnedValue = indicator;
                        break;
                    }
                }
                return returnedValue;
            }





            vm.setLegendToStudent = function (evaluation) {

                //Buscamos el indicador al cual pertenece la evaluacion que esta siendo modificada.
                var indicator = getIndicator(evaluation.indicatorId);

                //Buscamos el evaluation legend que tiene asignado el evaluation que se esta modificando
                var currentEvaLeg = getElementByKeyField(vm.evaluationLegends, "id", evaluation.evaluationLegendId);
                evaluation.evaluationLegends = currentEvaLeg;

                //Buscamos el evaluation anterior al evaluation que esta siendo modificado en el momento
                var priorEva = getEvaluationPriorEvaluationPeriod(evaluation, indicator);
                //var priorEva = null;

                //Comparamos a ver si el evaluation legend es inferior al evaluation legen usado en el periodo anterior.
                if (priorEva != null && priorEva.evaluationLegends != null
                    && currentEvaLeg.sequence < priorEva.evaluationLegends.sequence && currentEvaLeg.sequence > -1) {
                    evaluation.evaluationLegendId = null;
                    abp.message.error(App.localize("YouSelTheLegendXXXIntheLasPerAndSelectTheLegXXXInTheCurrenPeriod_IsNotAllowed",
                        priorEva.evaluationLegends.description, currentEvaLeg.description));
                    evaluation.evaluationLegendId = evaluation.evaluationLegendId_Tmp;
                    return;
                }

                var dataToSend = evaluation;
                dataToSend.enrollmentStudentId = vm.student.id;

                evaluationService.create(dataToSend).then(function (result) {
                    abp.notify.info(App.localize("EvaluationWasUpdatedSuccessfully"));
                    if (result.data != null)
                        evaluation.id = result.data.id;
                    assignTempValues();
                }, function (error) {
                    evaluation.evaluationLegendId = evaluation.evaluationLegendId_Tmp;
                });
            }


            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    evaluationService.create(vm.evaluation)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };


            //TODO: CONFIGURAR ESTO PARA IMPRIMIR EL STUDENT EVALUATION
            vm.printEvaluation = function (student) {
                var config = { headers: { 'Content-Type': 'text/plain' } };

                var objectReport = {};
                var reportsFilters = [];

                objectReport.reportCode = "52";

                objectReport.reportExport = "PDF";
                reportsFilters.push({ name: "EnrollmentStudentId", value: vm.student.id });
                reportsFilters.push({ name: "SubjectId", value: vm.subjectId });

                objectReport.reportsFilters = reportsFilters;
                report.print(objectReport);
                abp.message.success("");
                swal.close();

            }



            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.getInitialValues();



        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.evaluations.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.evaluation', 'settings',
        function ($scope, $timeout, $uibModal, evaluationService, settings) {
            var vm = this;
            vm.textFilter = "";


            $scope.$watch("vm.courseId", function (newValue, oldValue) {
                if (newValue != null) {
                    evaluationService.getSessionsByCourse({ courseId: vm.courseId }).then(function (result) {
                        vm.sessions = result.data.sessions;
                        vm.sessionId = null;
                        LoadStudents();
                    });
                }
            });

            $scope.$watch("vm.sessionId", function (newValue, oldValue) {
                LoadStudents();
            });

            function LoadStudents() {

                if (vm.courseId == undefined) {
                    return;
                }

                if (vm.sessionId == undefined)
                    vm.sessionId = null;

                abp.ui.setBusy(null,

                    evaluationService.getAllStudentByCourse({ courseId: vm.courseId, sessionId: vm.sessionId, enrollmentStudentId: null }).then(function (result) {
                        vm.studentSession = result.data.studentSession;
                        vm.studentCount = result.data.studentCount;
                    })
                );
            }

            vm.qualifyStudent = function (student) {
                //abp.message.info(App.localize("ThisFuncitonHasNotBeenFishishedYet"));
                //return;

                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/evaluations/createModal.cshtml',
                    controller: 'app.views.evaluations.createModal as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        student: function () {
                            return student;
                        },
                        sessionId: function () {
                            return vm.sessionId;
                        },
                        courseId: function () {
                            return vm.courseId;
                        },
                        subjectId: function () {
                            return vm.subjectId;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();

                    LoadStudents();
                });

                modalInstance.result.then(function () {
                    LoadStudents();
                });
            };

            function Init() {
                evaluationService.getInitialValues().then(function (result) {
                    vm.courses = result.data.courses;
                    vm.sessions = result.data.sessions;
                    vm.subjects = result.data.subjects;
                });
            }

            Init();
        }
    ]);
})();

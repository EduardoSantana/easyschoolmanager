(function () {
    angular.module('MetronicApp').controller('app.views.purchases.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.purchase', 'id', 'abp.services.app.provider',
        function ($scope, $uibModalInstance, purchaseService, id , providerService) {
            var vm = this;
			vm.saving = false;

            vm.purchase = {
                isActive: true
            };
            var init = function () {
                purchaseService.get({ id: id })
                    .then(function (result) {
                        vm.purchase = result.data;
						                vm.getProviders();

						App.initAjax();
						setTimeout(function () { $("#purchaseDocument").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						purchaseService.update(vm.purchase)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.providers = [];
            vm.getProviders	 = function()
            {
                providerService.getAllProvidersForCombo().then(function (result) {
                    vm.providers = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

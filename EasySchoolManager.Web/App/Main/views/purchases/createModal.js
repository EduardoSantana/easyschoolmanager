(function () {
    angular.module('MetronicApp').controller('app.views.purchases.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.purchase', 'abp.services.app.provider', 'abp.services.app.article',
        function ($scope, $uibModalInstance, purchaseService , providerService,articleService) {
            var vm = this;
            vm.saving = false;

            var todayDate = getDateYMD(new Date());
            vm.article = {};
            vm.articleIdIsFocused = false;

            vm.purchase = {
                isActive: true,
                date: todayDate
            };


            vm.purchase.dateTemp = convertDateFormat_YMD_to_DMY(todayDate, 4);
            vm.purchase.date = todayDate;

            vm.purchase.articles = [];

            $scope.getArticlesByName = function (userInputString) {
                $scope.pagination.maxResultCount = 10;
                $scope.pagination.textFilter = userInputString;
                return articleService.getAllArticles($scope.pagination);
            }

            $scope.returnedArticle = function (result) {
                if (!result) return;
                vm.article = result.originalObject;
                vm.article.quantity = 1;
            }

            $scope.$watch("vm.article.id", function (newValue, oldValue) {
                if (newValue == null) {
                    vm.article = {};
                }
                else {
                    if (vm.articleIdIsFocused == true)
                        vm.searArticleById();
                }
            });

            vm.searArticleById = function () {
                articleService.get({ id: vm.article.id }).then(function (result) {

                    if (result.data != null) {
                        var resultado = { originalObject: result.data };
                        $scope.returnedArticle(resultado);
                        $scope.$broadcast('angucomplete-alt:changeInput', 'ArticleAngu', vm.article.description);
                    }

                });
            }


            vm.removeArticle = function (article) {
                abp.message.confirm(App.localize("AreYouSureYouWantToRemoveTheArticle"), function (result) {
                    if (result == true) {
                        var index = vm.getIndex(article.id, vm.purchase.articles);
                        vm.purchase.articles.splice(index, 1);
                        calculateTotal();
                        refresArticles();
                        try {
                            $scope.$apply();
                        } catch (e) { }
                    }
                });
            }

            vm.getIndex = function (id, array) {
                var index = -1;

                for (var i = 0; i < array.length; i++) {
                    var item = array[i];
                    if (item.id == id) {
                        index = i;
                        break;
                    }
                }
                return index;
            }


            vm.getTempArticleFromLocalList = function (articleId) {
                var currentArticle = null;

                var articles = vm.purchase.articles.filter(function (x) { return x.id == articleId });
                if (articles.length > 0)
                    currentArticle = articles[0];
                return currentArticle;
            }

            vm.appendArticle = function () {

                //Validamos si el articulo puede ser insertado o no en la lista de articulos
                if (!validateArticle())
                    return;

                vm.article.total = getDoubleOrCero(vm.article.price * vm.article.quantity);

                //entre aqui
                var existingArticle = vm.getTempArticleFromLocalList(vm.article.id);
                if (existingArticle != null) {
                    existingArticle.quantity = parseInt(existingArticle.quantity) + parseInt(vm.article.quantity);
                    existingArticle.total = getDoubleOrCero(existingArticle.price * existingArticle.quantity);
                } else {
                    vm.purchase.articles.push(vm.article);
                }    
               //y aqui

                vm.article = {};
              
                $scope.$broadcast('angucomplete-alt:clearInput', 'ArticleAngu');
              //  refresArticles();
                calculateTotal();
                $("#purchaseArticleId").focus();
            }

            function validateArticle() {
                var validated = false;
                if (getDoubleOrCero(vm.article.price) == 0.00) {
                    abp.message.info(App.localize("YouCanNotSellAnArticleWithThatPrice"));
                }
                else if (getIntOrCero(vm.article.quantity) == 0) {

                    abp.message.info(App.localize("YouCanNotSellAnArticleWithQuantityInZero"));
                }
                else if (getIntOrCero(vm.article.id) == 0) {

                    abp.message.info(App.localize("YouMustSelectAnArticleBeforeAddToTheArticleList"));
                }
                else
                    validated = true;

                return validated;
            }

            function refresArticles() {
                $scope.gridArticleOptions.data = vm.purchase.articles;
            }

            function getCalculatedTotalWithArticles() {
                var total = 0.00;

                for (var i = 0; i < vm.purchase.articles.length; i++) {
                    var ar = vm.purchase.articles[i];
                    total += getDoubleOrCero(ar.total);
                }

                return total;
            }

            $scope.calculateTotal = function () {
                vm.purchase.totalCalculed = getCalculatedTotalWithArticles();
                vm.purchase.amount = vm.purchase.totalCalculed;
            }


            calculateTotal = function () {

                vm.purchase.totalCalculed = getCalculatedTotalWithArticles();
                vm.purchase.amount = vm.purchase.totalCalculed;

                App.initAjax();
                $scope.$apply();
            }

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    vm.purchase.date = convertDateFormat_DMY_to_YMD(vm.purchase.dateTemp)
                     purchaseService.create(vm.purchase)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX

            vm.providers = [];
            vm.getProviders	 = function()
            {
                providerService.getAllProvidersForCombo().then(function (result) {
                    vm.providers = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

		    vm.getProviders();

		    App.initAjax();
			setTimeout(function () { $("#purchaseDocument").focus(); }, 100);
        }
    ]);
})();

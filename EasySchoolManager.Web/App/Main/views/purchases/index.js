(function () {
    angular.module('MetronicApp').controller('app.views.purchases.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.purchase','settings',
        function ($scope, $timeout, $uibModal, purchaseService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getPurchases(false);
            }
            vm.purchases = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openPurchaseEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('PurchaseDocument'),
                    field: 'document',
                    minWidth: 125
                    },
                    {
                    name: App.localize('PurchaseProviderId'),
                    field: 'providers_Name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('PurchaseDate'),
                    field: 'date',
                    cellFilter: 'date:"dd-MM-yyyy\"',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Description'),
                    field: 'comnent1',
                    minWidth: 125
                    },
                    {
                    name: App.localize('Amount'),
                    field: 'amount',
                    cellFilter: 'number:2',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getPurchases(showTheLastPage) {
                purchaseService.getAllPurchases($scope.pagination).then(function (result) {
                    vm.purchases = result.data.items;

                    $scope.gridOptions.data = vm.purchases;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openPurchaseCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/purchases/createModal.cshtml',
                    controller: 'app.views.purchases.createModal as vm',
                    size : 'lg',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getPurchases(false);
                });
            };

            vm.openPurchaseEditModal = function (purchase) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/purchases/editModal.cshtml',
                    controller: 'app.views.purchases.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return purchase.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getPurchases(false);
                });
            };

            vm.delete = function (purchase) {
                abp.message.confirm(
                    "Delete purchase '" + purchase.name + "'?",
                    function (result) {
                        if (result) {
                            purchaseService.delete({ id: purchase.id })
                                .then(function (result) {
                                    getPurchases(false);
                                    abp.notify.info("Deleted purchase: " + purchase.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getPurchases(false);
            };

            getPurchases(false);
        }
    ]);
})();

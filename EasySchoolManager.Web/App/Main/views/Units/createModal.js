(function () {
    angular.module('MetronicApp').controller('app.views.units.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.unit', 
        function ($scope, $uibModalInstance, UnitService ) {
            var vm = this;
            vm.saving = false;

            vm.Unit = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     UnitService.create(vm.Unit)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#UnitDescription").focus(); }, 100);
        }
    ]);
})();

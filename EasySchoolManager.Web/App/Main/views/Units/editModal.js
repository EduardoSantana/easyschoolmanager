(function () {
    angular.module('MetronicApp').controller('app.views.units.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.unit', 'id', 
        function ($scope, $uibModalInstance, UnitService, id ) {
            var vm = this;
			vm.saving = false;

            vm.Unit = {
                isActive: true
            };
            var init = function () {
                UnitService.get({ id: id })
                    .then(function (result) {
                        vm.Unit = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#UnitDescription").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						UnitService.update(vm.Unit)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.evaluationHighs.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.evaluationHigh','settings',
        function ($scope, $timeout, $uibModal, evaluationHighService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getEvaluationHighs(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.evaluationHighs = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openEvaluationHighEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                    name: App.localize('Subject'),
                    field: 'subjectHighId',
                    minWidth: 125
                    },
                    {
                    name: App.localize('EvaluationHighEnrollmentStudentId'),
                    field: 'enrollmentStudentId',
                    minWidth: 125
                    },
                    {
                    name: App.localize('EvaluationHighEvaluationOrderHighId'),
                    field: 'evaluationOrderHighId',
                    minWidth: 125
                    },
                    {
                    name: App.localize('EvaluationHighExpression'),
                    field: 'expression',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getEvaluationHighs(showTheLastPage) {
                evaluationHighService.getAllEvaluationHighs($scope.pagination).then(function (result) {
                    vm.evaluationHighs = result.data.items;

                    $scope.gridOptions.data = vm.evaluationHighs;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openEvaluationHighCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/evaluationHighs/createModal.cshtml',
                    controller: 'app.views.evaluationHighs.createModal as vm',
                    size: "lg",
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getEvaluationHighs(false);
                });
            };

            vm.openEvaluationHighEditModal = function (evaluationHigh) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/evaluationHighs/editModal.cshtml',
                    controller: 'app.views.evaluationHighs.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return evaluationHigh.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getEvaluationHighs(false);
                });
            };

            vm.delete = function (evaluationHigh) {
                abp.message.confirm(
                    "Delete evaluationHigh '" + evaluationHigh.name + "'?",
                    function (result) {
                        if (result) {
                            evaluationHighService.delete({ id: evaluationHigh.id })
                                .then(function (result) {
                                    getEvaluationHighs(false);
                                    abp.notify.info("Deleted evaluationHigh: " + evaluationHigh.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getEvaluationHighs(false);
            };

            getEvaluationHighs(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.evaluationHighs.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.evaluationHigh', 'id', 'abp.services.app.subjectHigh','abp.services.app.evaluationOrderHigh',
        function ($scope, $uibModalInstance, evaluationHighService, id , subjectHighService, evaluationOrderHighService) {
            var vm = this;
			vm.saving = false;

            vm.evaluationHigh = {
                isActive: true
            };
            var init = function () {
                evaluationHighService.get({ id: id })
                    .then(function (result) {
                        vm.evaluationHigh = result.data;
						                vm.getSubjectHighs();
                vm.getEvaluationOrderHighs();

						App.initAjax();
						setTimeout(function () { $("#evaluationHighSubjectHighId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						evaluationHighService.update(vm.evaluationHigh)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.subjectHighs = [];
            vm.getSubjectHighs	 = function()
            {
                subjectHighService.getAllSubjectHighsForCombo().then(function (result) {
                    vm.subjectHighs = result.data;
					App.initAjax();
                });
            }

            vm.evaluationOrderHighs = [];
            vm.getEvaluationOrderHighs	 = function()
            {
                evaluationOrderHighService.getAllEvaluationOrderHighsForCombo().then(function (result) {
                    vm.evaluationOrderHighs = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

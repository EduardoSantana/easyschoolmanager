(function () {
    angular.module('MetronicApp').controller('app.views.evaluationHighs.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.evaluationHigh', 'abp.services.app.subjectHigh', 'abp.services.app.evaluationOrderHigh',
        'abp.services.app.course', 'abp.services.app.sessionStudentSelection', 'report', 
        function ($scope, $uibModalInstance, evaluationHighService, subjectHighService, evaluationOrderHighService, courseService, sessionStudentSelectionService, report) {
            var vm = this;
            vm.saving = false;

            vm.evaluationHigh = {
                isActive: true,
                expression: 0,
                anexpression: 0,
                enrollmentStudentId: 0,
                studentId: 0,
                id: 0
            };

            vm.save = function () {
                if (vm.saving === true)
                    return;
                if (vm.evaluationHigh.expression == undefined) {
                    return;
                }

                vm.saving = true;
                try {
                    evaluationHighService.create(vm.evaluationHigh)
                        .then(function () {
                            vm.saving = false;
                            //abp.notify.info(App.localize('SavedSuccessfully'));
                            //$uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            vm.printEvaluation = function () {

                if (vm.evaluationHigh.courseId == undefined || vm.evaluationHigh.courseId == null)
                    return;

                if (vm.evaluationHigh.courseSessionId == undefined || vm.evaluationHigh.courseSessionId == null)
                    return;

                if (vm.evaluationHigh.subjectHighId == undefined || vm.evaluationHigh.subjectHighId == null)
                    return;

                if (vm.evaluationHigh.evaluationOrderHighId == undefined || vm.evaluationHigh.evaluationOrderHighId == null)
                    return;

                var config = { headers: { 'Content-Type': 'text/plain' } };

                var objectReport = {};
                var reportsFilters = [];

                objectReport.reportCode = "53";

                objectReport.reportExport = "PDF";
                reportsFilters.push({ name: "SubjectHighId", value: vm.evaluationHigh.subjectHighId });
                reportsFilters.push({ name: "EvaluationOrderHighId", value: vm.evaluationHigh.evaluationOrderHighId });
                reportsFilters.push({ name: "CourseId", value: vm.evaluationHigh.courseId });
                reportsFilters.push({ name: "SessionId", value: vm.evaluationHigh.courseSessionId });

                objectReport.reportsFilters = reportsFilters;
                report.print(objectReport);
                abp.message.success("");
                swal.close();

            }

            vm.printEvaluationByCourse = function () {

                if (vm.evaluationHigh.courseId == undefined || vm.evaluationHigh.courseId == null)
                    return;

                if (vm.evaluationHigh.courseSessionId == undefined || vm.evaluationHigh.courseSessionId == null)
                    return;

                if (vm.evaluationHigh.subjectHighId == undefined || vm.evaluationHigh.subjectHighId == null)
                    return;

                var config = { headers: { 'Content-Type': 'text/plain' } };

                var objectReport = {};
                var reportsFilters = [];

                objectReport.reportCode = "55";

                objectReport.reportExport = "PDF";
                reportsFilters.push({ name: "SubjectHighId", value: vm.evaluationHigh.subjectHighId });
                reportsFilters.push({ name: "CourseId", value: vm.evaluationHigh.courseId });
                reportsFilters.push({ name: "SessionId", value: vm.evaluationHigh.courseSessionId });

                objectReport.reportsFilters = reportsFilters;
                report.print(objectReport);
                abp.message.success("");
                swal.close();

            };

            $scope.$watch("vm.evaluationHigh.courseId", function (newValue, oldValue) {
                if (newValue != null) {
                    sessionStudentSelectionService.getSessionsByCourse({ courseId: vm.evaluationHigh.courseId }).then(function (result) {
                        vm.sessions = result.data.sessions;
                        vm.sessionId = null;
                        //  LoadStudents();
                    });
                }
            });

            $scope.$watch("vm.evaluationHigh.courseId", function (newValue, oldValue) {
                if (newValue != null) {
                    subjectHighService.getAllSubjectHighsByCourseForCombo(vm.evaluationHigh.courseId).then(function (result) {
                        vm.subjectHighs = result.data;
                        //  vm.subjectHighId = null;
                        //  LoadStudents();
                    });
                }
            });


            $scope.$watch("vm.evaluationHigh.courseSessionId", function (newValue, oldValue) {
                if (newValue != null) {
                    LoadStudents();
                }
            });

            $scope.$watch("vm.evaluationHigh.evaluationOrderHighId", function (newValue, oldValue) {
                if (newValue != null) {
                    LoadStudents();
                }
            });

            $scope.$watch("vm.evaluationHigh.subjectHighId", function (newValue, oldValue) {
                if (newValue != null) {
                    LoadStudents();
                }
            });

            function LoadStudents() {

                if (vm.evaluationHigh.courseId == undefined) {
                    return;
                }

                if (vm.evaluationHigh.courseSessionId == undefined)
                    vm.evaluationHigh.courseSessionId = null;

                abp.ui.setBusy(null,

                    sessionStudentSelectionService.getAllStudentScoreByCourse({
                        courseId: vm.evaluationHigh.courseId,
                        sessionId: vm.evaluationHigh.courseSessionId,
                        enrollmentStudentId: null,
                        evaluationOrderHighId: vm.evaluationHigh.evaluationOrderHighId,
                        subjectHighId: vm.evaluationHigh.subjectHighId
                    })
                    .then(function (result) {
                        vm.studentCourse = result.data.studentCourse;
                        vm.studentSession = result.data.studentEvaluationHigh;
                        vm.studentCount = result.data.studentCount;
                    })
                );

            }

            //XXXInsertCallRelatedEntitiesXXX

            vm.courses = [];
            vm.getCourses = function () {
                courseService.getAllCoursesByLevelsForCombo(3).then(function (result) {
                    vm.courses = result.data;
                    App.initAjax();
                });
            }

            vm.sessions = [];

            vm.subjectHighs = [];

            //        vm.getSubjectHighs	 = function()
            //        {
            //            subjectHighService.getAllSubjectHighsByCourseForCombo().then(function (result) {
            //               vm.subjectHighs = result.data;
            //				App.initAjax();
            //            });
            //        }

            vm.evaluationOrderHighs = [];
            vm.getEvaluationOrderHighs = function () {
                evaluationOrderHighService.getAllEvaluationOrderHighsForCombo().then(function (result) {
                    vm.evaluationOrderHighs = result.data;
                    App.initAjax();
                });
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            //  vm.getSubjectHighs();
            vm.getEvaluationOrderHighs();
            vm.getCourses();
            App.initAjax();
            setTimeout(function () { $("#evaluationHighSubjectHighId").focus(); }, 100);
        }
    ]);
})();

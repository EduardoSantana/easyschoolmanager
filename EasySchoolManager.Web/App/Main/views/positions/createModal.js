(function () {
    angular.module('MetronicApp').controller('app.views.positions.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.position', 
        function ($scope, $uibModalInstance, positionService ) {
            var vm = this;
            vm.saving = false;

            vm.position = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     positionService.create(vm.position)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#positionName").focus(); }, 100);
        }
    ]);
})();

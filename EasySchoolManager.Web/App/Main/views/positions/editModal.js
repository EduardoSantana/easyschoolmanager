(function () {
    angular.module('MetronicApp').controller('app.views.positions.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.position', 'id', 
        function ($scope, $uibModalInstance, positionService, id ) {
            var vm = this;
			vm.saving = false;

            vm.position = {
                isActive: true
            };
            var init = function () {
                positionService.get({ id: id })
                    .then(function (result) {
                        vm.position = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#positionName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						positionService.update(vm.position)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

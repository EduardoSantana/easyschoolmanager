(function () {
    angular.module('MetronicApp').controller('app.views.brands.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.brand', 'id', 
        function ($scope, $uibModalInstance, brandService, id ) {
            var vm = this;
			vm.saving = false;

            vm.brand = {
                isActive: true
            };
            var init = function () {
                brandService.get({ id: id })
                    .then(function (result) {
                        vm.brand = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#brandName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						brandService.update(vm.brand)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

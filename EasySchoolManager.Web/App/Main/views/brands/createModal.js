(function () {
    angular.module('MetronicApp').controller('app.views.brands.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.brand', 
        function ($scope, $uibModalInstance, brandService ) {
            var vm = this;
            vm.saving = false;

            vm.brand = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     brandService.create(vm.brand)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#brandName").focus(); }, 100);
        }
    ]);
})();

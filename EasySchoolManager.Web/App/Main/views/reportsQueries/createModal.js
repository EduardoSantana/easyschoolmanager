(function () {
    angular.module('MetronicApp').controller('app.views.reportsQueries.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.reportsQuery', 'id',
        function ($scope, $uibModalInstance, reportsQueryService, id ) {
            var vm = this;
            vm.saving = false;

            vm.reportsQuery = {
                isActive: true,
                reportsFiltersId: id
            };

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     reportsQueryService.create(vm.reportsQuery)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#reportsQueryValueId").focus(); }, 100);
        }
    ]);
})();

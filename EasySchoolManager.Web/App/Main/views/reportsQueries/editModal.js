(function () {
    angular.module('MetronicApp').controller('app.views.reportsQueries.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.reportsQuery', 'id', 
        function ($scope, $uibModalInstance, reportsQueryService, id ) {
            var vm = this;
			vm.saving = false;

            vm.reportsQuery = {
                isActive: true
            };
            var init = function () {
                reportsQueryService.get({ id: id })
                    .then(function (result) {
                        vm.reportsQuery = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#reportsQueryValueId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						reportsQueryService.update(vm.reportsQuery)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

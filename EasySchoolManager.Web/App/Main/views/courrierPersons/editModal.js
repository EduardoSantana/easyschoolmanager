(function () {
    angular.module('MetronicApp').controller('app.views.courrierPersons.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.courrierPerson', 'id', 
        function ($scope, $uibModalInstance, courrierPersonService, id ) {
            var vm = this;
			vm.saving = false;

            vm.courrierPerson = {
                isActive: true
            };
            var init = function () {
                courrierPersonService.get({ id: id })
                    .then(function (result) {
                        vm.courrierPerson = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#courrierPersonName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						courrierPersonService.update(vm.courrierPerson)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

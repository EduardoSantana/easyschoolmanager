(function () {
    angular.module('MetronicApp').controller('app.views.courrierPersons.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.courrierPerson', 
        function ($scope, $uibModalInstance, courrierPersonService ) {
            var vm = this;
            vm.saving = false;

            vm.courrierPerson = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     courrierPersonService.create(vm.courrierPerson)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#courrierPersonName").focus(); }, 100);
        }
    ]);
})();

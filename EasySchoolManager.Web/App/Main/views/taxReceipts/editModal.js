(function () {
    angular.module('MetronicApp').controller('app.views.taxReceipts.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.taxReceipt', 'id', 'abp.services.app.taxReceiptType', 'abp.services.app.region',
        function ($scope, $uibModalInstance, taxReceiptService, id, taxReceiptTypeService, regionService) {
            var vm = this;
            vm.saving = false;

            vm.taxReceipt = {
                isActive: true
            };
            var init = function () {
                taxReceiptService.get({ id: id })
                    .then(function (result) {
                        vm.taxReceipt = result.data;
                        vm.getTaxReceiptTypes();

                        vm.taxReceipt.expirationDateTemp = convertDateFormat_YMD_to_DMY(vm.taxReceipt.expirationDate, 4);

                        vm.getRegions();

                        App.initAjax();
                        setTimeout(function () { $("#taxReceiptTaxReceiptTypeId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                    taxReceiptService.update(vm.taxReceipt)
                        .then(function () {
                            vm.saving = false;
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }, function (e) {
                            vm.saving = false;
                            console.log(e.data.message);
                        });
                }
                catch (e) {
                    vm.saving = false;
                }
            };

            $scope.$watch("vm.taxReceipt.expirationDateTemp", function (newValue, oldValue) {
                if (newValue != undefined && newValue != oldValue) {
                    vm.taxReceipt.ExpirationDate = convertDateFormat_DMY_to_YMD(newValue, 4);
                }
            });
            //XXXInsertCallRelatedEntitiesXXX

            vm.taxReceiptTypes = [];
            vm.getTaxReceiptTypes = function () {
                taxReceiptTypeService.getAllTaxReceiptTypesForCombo().then(function (result) {
                    vm.taxReceiptTypes = result.data;
                    App.initAjax();
                });
            }

            vm.regions = [];
            vm.getRegions = function () {
                regionService.getAllRegionsForCombo().then(function (result) {
                    vm.regions = result.data;
                    App.initAjax();
                });
            }



            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

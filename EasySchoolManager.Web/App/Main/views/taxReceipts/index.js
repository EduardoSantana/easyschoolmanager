(function () {
    angular.module('MetronicApp').controller('app.views.taxReceipts.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.taxReceipt','settings',
        function ($scope, $timeout, $uibModal, taxReceiptService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getTaxReceipts(false);
            }
            vm.taxReceipts = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openTaxReceiptEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
					
					
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('TaxReceiptTaxReceiptType'),
                    field: 'taxReceiptTypes.name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('TaxReceiptName'),
                    field: 'name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('TaxReceiptInitialNumber'),
                    field: 'initialNumber',
                    minWidth: 125
                    },
                    {
                    name: App.localize('TaxReceiptFinalNumber'),
                    field: 'finalNumber',
                    minWidth: 125
                    },
                    {
                    name: App.localize('TaxReceiptCurrentSequence'),
                    field: 'currentSequence',
                    minWidth: 125
                    },
                    {
                    name: App.localize('TaxReceiptRegion'),
                    field: 'regions.name',
                    minWidth: 125
                    },
                    {
                    name: App.localize('TaxReceiptSerie'),
                    field: 'serie',
                    minWidth: 125
                    },
                    {
                    name: App.localize('TaxReceiptExpirationDate'),
                    field: 'expirationDate',
                    minWidth: 125,
                    cellFilter: 'date:"dd-MM-yyyy\"'
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getTaxReceipts(showTheLastPage) {
                taxReceiptService.getAllTaxReceipts($scope.pagination).then(function (result) {
                    vm.taxReceipts = result.data.items;

                    $scope.gridOptions.data = vm.taxReceipts;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openTaxReceiptCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/taxReceipts/createModal.cshtml',
                    controller: 'app.views.taxReceipts.createModal as vm',
                    size: 'lg',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getTaxReceipts(false);
                });
            };

            vm.openTaxReceiptEditModal = function (taxReceipt) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/taxReceipts/editModal.cshtml',
                    controller: 'app.views.taxReceipts.editModal as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        id: function () {
                            return taxReceipt.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getTaxReceipts(false);
                });
            };

            vm.delete = function (taxReceipt) {
                abp.message.confirm(
                    "Delete taxReceipt '" + taxReceipt.name + "'?",
                    function (result) {
                        if (result) {
                            taxReceiptService.delete({ id: taxReceipt.id })
                                .then(function (result) {
                                    getTaxReceipts(false);
                                    abp.notify.info("Deleted taxReceipt: " + taxReceipt.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getTaxReceipts(false);
            };

            getTaxReceipts(false);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.taxReceipts.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.taxReceipt', 'abp.services.app.taxReceiptType','abp.services.app.region',
        function ($scope, $uibModalInstance, taxReceiptService , taxReceiptTypeService, regionService) {
            var vm = this;
            vm.saving = false;

            vm.taxReceipt = {
                isActive: true,
                expirationDateTemp : new Date()
            };

            vm.taxReceipt.expirationDateTemp = convertDateFormat_YMD_to_DMY(vm.taxReceipt.expirationDateTemp, 4);

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     taxReceiptService.create(vm.taxReceipt)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

            $scope.$watch("vm.taxReceipt.expirationDateTemp", function (newValue, oldValue) {
                if (newValue != undefined && newValue != oldValue) {
                    vm.taxReceipt.ExpirationDate = convertDateFormat_DMY_to_YMD(newValue, 4);
                }
            });

			//XXXInsertCallRelatedEntitiesXXX

            vm.taxReceiptTypes = [];
            vm.getTaxReceiptTypes	 = function()
            {
                taxReceiptTypeService.getAllTaxReceiptTypesForCombo().then(function (result) {
                    vm.taxReceiptTypes = result.data;
					App.initAjax();
                });
            }

            vm.regions = [];
            vm.getRegions	 = function()
            {
                regionService.getAllRegionsForCombo().then(function (result) {
                    vm.regions = result.data;
					App.initAjax();
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			                vm.getTaxReceiptTypes();
                vm.getRegions();

		    App.initAjax();
			setTimeout(function () { $("#taxReceiptTaxReceiptTypeId").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.historyCancelReceipts.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.historyCancelReceipt', 'id', 
        function ($scope, $uibModalInstance, historyCancelReceiptService, id ) {
            var vm = this;
			vm.saving = false;

            vm.historyCancelReceipt = {
                isActive: true
            };
            var init = function () {
                historyCancelReceiptService.get({ id: id })
                    .then(function (result) {
                        vm.historyCancelReceipt = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#historyCancelReceiptTransactionId").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						historyCancelReceiptService.update(vm.historyCancelReceipt)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

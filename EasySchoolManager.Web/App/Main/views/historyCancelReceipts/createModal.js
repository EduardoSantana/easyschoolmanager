(function () {
    angular.module('MetronicApp').controller('app.views.historyCancelReceipts.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.historyCancelReceipt', 
        function ($scope, $uibModalInstance, historyCancelReceiptService ) {
            var vm = this;
            vm.saving = false;

            vm.historyCancelReceipt = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     historyCancelReceiptService.create(vm.historyCancelReceipt)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#historyCancelReceiptTransactionId").focus(); }, 100);
        }
    ]);
})();

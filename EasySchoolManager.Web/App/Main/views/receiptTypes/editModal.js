(function () {
    angular.module('MetronicApp').controller('app.views.receiptTypes.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.receiptType', 'id', 
        function ($scope, $uibModalInstance, receiptTypeService, id ) {
            var vm = this;
			vm.saving = false;

            vm.receiptType = {
                isActive: true
            };
            var init = function () {
                receiptTypeService.get({ id: id })
                    .then(function (result) {
                        vm.receiptType = result.data;
						
						App.initAjax();
						setTimeout(function () { $("#receiptTypeName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						receiptTypeService.update(vm.receiptType)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
                           console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.receiptTypes.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.receiptType', 
        function ($scope, $uibModalInstance, receiptTypeService ) {
            var vm = this;
            vm.saving = false;

            vm.receiptType = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     receiptTypeService.create(vm.receiptType)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

			
		    App.initAjax();
			setTimeout(function () { $("#receiptTypeName").focus(); }, 100);
        }
    ]);
})();

(function () {
    angular.module('MetronicApp').controller('app.views.printerTypes.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.printerType', 'id',   'abp.services.app.report', 
        function ($scope, $uibModalInstance, printerTypeService, id, reportService ) {
            var vm = this;
			vm.saving = false;

            vm.printerType = {
                isActive: true
            };
            var init = function () {
                printerTypeService.get({ id: id })
                    .then(function (result) {
                        vm.printerType = result.data;
                        vm.getReports();
						App.initAjax();
						setTimeout(function () { $("#printerTypeName").focus(); }, 100);
                    });
            }
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
						printerTypeService.update(vm.printerType)
						.then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
						   vm.saving = false;
						   console.log(e.data.message);
						});
					} 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };
			
			//XXXInsertCallRelatedEntitiesXXX

            vm.reports = [];
            vm.getReports = function () {
                reportService.getAllReportsCombo2().then(function (result) {
                    vm.reports = result.data;
                    App.initAjax();
                });
            }



			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();

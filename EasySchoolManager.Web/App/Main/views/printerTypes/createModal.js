(function () {
    angular.module('MetronicApp').controller('app.views.printerTypes.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.printerType',  'abp.services.app.report', 
        function ($scope, $uibModalInstance, printerTypeService, reportService ) {
            var vm = this;
            vm.saving = false;

            vm.printerType = {
                isActive: true
            };
            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                try {
                     printerTypeService.create(vm.printerType)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
							vm.saving = false;
							console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            vm.reports = [];
            vm.getReports = function () {
                reportService.getAllReportsForCombo2().then(function (result) {
                    vm.reports = result.data;
                    App.initAjax();
                });
            }


            vm.getReports();
			
		    App.initAjax();
			setTimeout(function () { $("#printerTypeName").focus(); }, 100);
        }
    ]);
})();

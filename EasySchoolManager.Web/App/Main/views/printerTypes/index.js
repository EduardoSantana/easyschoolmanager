(function () {
    angular.module('MetronicApp').controller('app.views.printerTypes.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.printerType','settings',
        function ($scope, $timeout, $uibModal, printerTypeService, settings) {
            var vm = this;
            vm.textFilter = "";

            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getPrinterTypes(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.printerTypes = [];

            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
                    
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-default gray" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>     <span class="menu"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-click="grid.appScope.openPrinterTypeEditModal(row.entity)"><i class=\"fa fa-pencil\"></i>' + App.localize('Edit') + '</a></li>' +
                        //'      <li><a ng-click="grid.appScope.delete(row.entity)"><i class=\"fa fa-close\"></i>' + App.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('Id'),
                        field: 'id',
                        width: 75
                    },

                                        {
                    name: App.localize('PrinterTypeName'),
                    field: 'name',
                    minWidth: 125
                    },

                                        {
                    name: App.localize('Report'),
                    field: 'reports.reportName',
                    minWidth: 125
                    },


                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 120
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getPrinterTypes(showTheLastPage) {
                printerTypeService.getAllPrinterTypes($scope.pagination).then(function (result) {
                    vm.printerTypes = result.data.items;

                    $scope.gridOptions.data = vm.printerTypes;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();
                });
            }

            vm.openPrinterTypeCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/printerTypes/createModal.cshtml',
                    controller: 'app.views.printerTypes.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getPrinterTypes(true);
                });
            };

            vm.openPrinterTypeEditModal = function (printerType) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/printerTypes/editModal.cshtml',
                    controller: 'app.views.printerTypes.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return printerType.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getPrinterTypes(false);
                });
            };

            vm.delete = function (printerType) {
                abp.message.confirm(
                    "Delete printerType '" + printerType.name + "'?",
                    function (result) {
                        if (result) {
                            printerTypeService.delete({ id: printerType.id })
                                .then(function (result) {
                                    getPrinterTypes(false);
                                    abp.notify.info("Deleted printerType: " + printerType.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getPrinterTypes(false);
            };

            getPrinterTypes(false);
        }
    ]);
})();

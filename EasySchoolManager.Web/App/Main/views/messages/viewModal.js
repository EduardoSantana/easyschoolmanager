(function () {
    angular.module('MetronicApp').controller('app.views.messages.viewModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.message', 'abp.services.app.user', 'abp.services.app.tenant',
        function ($scope, $uibModalInstance, messageService, userService,tenantService) {
            var vm = this;
            vm.saving = false;

            vm.message = {
                isActive: true,
                allTenant: true,
                allUser: true
            };
            vm.tenants = [];

            if ($scope.$resolve.id) {

                messageService.get({ id: $scope.$resolve.id}).then(function (result) {
                    vm.message = result.data;
                });
            }


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };
            
		    
        }

    ]);
})();

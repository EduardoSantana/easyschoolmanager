(function () {
    angular.module('MetronicApp').controller('app.views.messages.index', [
        '$scope', '$timeout', '$uibModal', 'abp.services.app.message','settings','$stateParams',
        function ($scope, $timeout, $uibModal, messageService, settings, $stateParams) {
            var vm = this;
            vm.textFilter = "";

            vm.permission = {
                create: abp.auth.hasPermission('Pages.Messages.Create')
            };
            vm.userId = abp.session.userId;
            $scope.pagination = new Pagination(0, 10);
            $scope.pagination.reload = function () {
                getMessages(false);
            }

            $scope.enterToGoToPage = function (e) {
                if (e.which == 13) {
                    $scope.pagination.goToPage($scope.pagination.currentPage);
                }
            }

            vm.messages = [];


            $scope.gridOptions = {
			    appScopeProvider: vm,
                columnDefs: [
					{
                        name: App.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '    <a ng-click="grid.appScope.openMessageViewModal(row.entity)"><i class=\"fa fa-eye\"></i> ' + App.localize('View') + '</a>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: App.localize('FromUser'),
                        field: 'fromUser_FullName',
                        width: 160,
                        cellTemplate:
                        ' <span class="photo" ng-if="row.entity.creatorUserId!= grid.appScope.userId"  title="'
                        + App.localize('FromUser') + '" data-content="{{row.entity.fromUser_FullName}}" data-placement="bottom" >' +
                        '<img src="Account/GetUserPicture/{{row.entity.creatorUserId}}" class="img-circle  img-min" alt="" >' +
                        '<span>{{row.entity.fromUser_FullName}}</span>' +
                        '</span>' 
                        +
                        ' <span class="text" ng-if="row.entity.creatorUserId== grid.appScope.userId">' +
                        App.localize('You')   +
                        '</span>'
                        ,
                    },
                    {
                    name: App.localize('Subject'),
                    field: 'subject',
                    width: 160
                    },
                    {
                    name: App.localize('Body'),
                    field: 'body',
                    minWidth: 150,
                    cellTemplate:
                    '<div> ' +
                    '<span class="trucate-text"> {{row.entity.body}}</span>' +
                    '</div>'
                        
                    },
                    {
                    name: App.localize('AllTenant'),
                    field: 'allTenant',
                    cellTemplate:
                    '  <span ng-if="row.entity.allTenant">' + App.localize('Yes') + '</span>' +
                    '  <span ng-if="!row.entity.allTenant">' + App.localize('No') + '</span>' 
                        ,
                    width: 100
                    },

                    {
                        name: App.localize('School'),
                        field: 'tenant_Name',
                        cellTemplate:
                        '<div title="' + App.localize('School') +'" data-content="{{row.entity.tenant_Name}}" data-placement="bottom" > ' +
                        '<span class="trucate-text" ng-if="!row.entity.allTenant"> {{row.entity.tenant_Name}}</span>' +
                        '  <span ng-if="row.entity.allTenant">' + App.localize('AllSchools') + '</span>' +
                        '</div>'
                        ,
                        width: 260
                    },

                    {
                    name: App.localize('AllUser'),
                    field: 'allUser',
                    cellTemplate:
                    '  <span ng-if="row.entity.allUser">' + App.localize('Yes') + '</span>' +
                    '  <span ng-if="!row.entity.allUser">' + App.localize('No') + '</span>' ,
                    width: 100
                    },


                    {
                        name: App.localize('User'),
                        field: 'destinationUser_FullName',
                        cellTemplate:
                        '  <span ng-if="row.entity.allUser">' + App.localize('AllUsers') + '</span>'+

                        ' <span class="photo" ng-if="!row.entity.allUser && row.entity.destinationUserId!= grid.appScope.userId"  title="'
                        + App.localize('ToUser') + '" data-content="{{row.entity.destinationUser_FullName}}" data-placement="bottom" >' +
                        '<img src="Account/GetUserPicture/{{row.entity.destinationUserId}}" class="img-circle img-min" alt="" >' +
                        '<span>{{row.entity.destinationUser_FullName}}</span>' +
                        '</span>'
                        +
                        ' <span class="text" ng-if="row.entity.destinationUserId== grid.appScope.userId">' +
                        App.localize('You') +
                        '</span>'
                        ,
                        width: 160
                    },

                    {
                        name: App.localize('IsActive'),
                        field: 'isActive',
                        cellTemplate:
                        ' <div style=\"text-align:center;\"> ' +
                        '<div class="\md-checkbox default\" style=\"text-align:center; width:100%; margin-left:20px;\">' +
                        '<input type="\checkbox\" id="\checkbox16\" class="\md-check\" ng-model=\"row.entity.isActive\" disabled>' +
                        '<label for="\checkbox16\">' +
                        '  <span class="\inc\"></span>' +
                        '   <span class="\check\"></span>' +
                        '  <span class=\"box\" ></span> </label>' +
                        '   </div>'
                        ,
                        width: 80
                    }
					]
            };


            $scope.$watch("vm.textFilter", function (newValue, oldValue) {
                $scope.pagination.textFilter = newValue;
                if ($scope.pagination.currentPage > 1)
                    $scope.pagination.first();
                $scope.pagination.reload();
            });

            function getMessages(showTheLastPage) {
                messageService.getAllMessages($scope.pagination).then(function (result) {
                    vm.messages = result.data.items;

                    $scope.gridOptions.data = vm.messages;

                    $scope.pagination.assignTotalRecords(result.data.totalCount);
                    if (showTheLastPage)
                        $scope.pagination.last();

                    setTimeout(function () { $("div,span").popover({ trigger: "hover" }); }, 1000);
                });
            }

            vm.openMessageCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/messages/createModal.cshtml',
                    controller: 'app.views.messages.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                   App.initAjax();
                });

                modalInstance.result.then(function () {
                    getMessages(false);
                });
            };

            vm.openMessageEditModal = function (message) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/messages/createModal.cshtml',
                    controller: 'app.views.messages.createModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return message.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                       App.initAjax();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getMessages(false);
                });
            };

            vm.openMessageViewModal = function (message) {
                var modalInstance = $uibModal.open({
                    templateUrl: settings.settings.appPathNotHTTP + '/views/messages/viewModal.cshtml',
                    controller: 'app.views.messages.viewModal as vm',
                    backdrop: 'static',
                    size : "lg",
                    resolve: {
                        id: function () {
                            return message.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    App.initAjax();
                });

                modalInstance.result.then(function () {
                    getMessages(false);
                });
            };

            vm.delete = function (message) {
                abp.message.confirm(
                    "Delete message '" + message.name + "'?",
                    function (result) {
                        if (result) {
                            messageService.delete({ id: message.id })
                                .then(function (result) {
                                    getMessages(false);
                                    abp.notify.info("Deleted message: " + message.name);

                                });
                        }
                    });
            }

            vm.refresh = function () {
                getMessages(false);
            };

            getMessages(false);


            if ($stateParams.id)
                vm.openMessageViewModal({ id: $stateParams.id });
        }
    ]);
})();

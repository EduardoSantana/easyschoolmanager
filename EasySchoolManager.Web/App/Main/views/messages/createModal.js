(function () {
    angular.module('MetronicApp').controller('app.views.messages.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.message', 'abp.services.app.user', 'abp.services.app.tenant',
        function ($scope, $uibModalInstance, messageService, userService,tenantService) {
            var vm = this;
            vm.saving = false;

            vm.permission = {
                allTenant: abp.auth.hasPermission('Pages.Messages.SendAllTenant') ,
                allUser: abp.auth.hasPermission('Pages.Messages.SendAllUser') 
            };
            vm.message = {
                isActive: true,
                allTenant: vm.permission.allTenant,
                allUser: vm.permission.allUser
            };
            vm.tenants = [];

            if ($scope.$resolve.id) {

                messageService.get({ id: $scope.$resolve.id}).then(function (result) {
                    vm.message = result.data;
                });
            }

            vm.save = function () {
                if (vm.saving === true)
                    return;
                vm.saving = true;
                var method = "create";

                if (vm.message.id)
                    method = "update";

                try {
                    messageService[method](vm.message)
                        .then(function () {
							vm.saving = false;
							abp.notify.info(App.localize('SavedSuccessfully'));
							$uibModalInstance.close();
						}, function(e){
							vm.saving = false;
                            console.log(e.data.message);
						});
				    } 
					catch (e)
                    {
                       vm.saving = false;
                    }
            };

			//XXXInsertCallRelatedEntitiesXXX


			
            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            tenantService.getAllTenantsForCombo().then(function (result) {
                vm.tenants = result.data;
            });

            $scope.getUsersForMessage = function (textFilter) {
                return userService.getAllUserForMessage({ textFilter: textFilter, tenantId: vm.message.tenantId });
            }

            $scope.selectedUser = function (newValue, oldValue) {

                if (newValue)
                    vm.message.destinationUserId = newValue.originalObject.id;
            }

            vm.destinationUserChange = function () {
                vm.destinationUserId = null;
                $scope.$broadcast('angucomplete-alt:clearInput');
            }

		    App.initAjax();
			setTimeout(function () { $("#messageDestinationUserId").focus(); }, 100);
        }
    ]);
})();

﻿angular.module('MetronicApp').service('report', ['abp.services.app.report', '$http', function (reportService, $http) {

    return {

        print: function (objReport) {
            
            var frm = "<form id='form_1_' action='../ReportsV2/GetReport' method='post' target='_blank'>";
            frm += '<input type="number" name="reportCode" value="' + objReport.reportCode + '" />';
            frm += '<input type="number" name="id" value="' + (objReport.id || 0)+ '" />';
            frm += '<input type="text" name="reportExport" value="' + (objReport.reportExport || "PDF") + '" />';

            for (var i in objReport.reportsFilters) {
                var item = objReport.reportsFilters[i];
                frm += '<input type="text" name="reportsFilters[' + i.toString() + '].name" value="' + item.name + '" />';
                frm += '<input type="text" name="reportsFilters[' + i.toString() + '].value" value="' + (item.value instanceof Date ? $filter("date")(item.value, "yyyy-mm-dd") + "T00:00:00.000Z" : item.value) + '" />';
                frm += '<input type="text" name="reportsFilters[' + i.toString() + '].rangeValue" value="' + item.rangeValue+ '" />';
                frm += '<input type="text" name="reportsFilters[' + i.toString() + '].valueText" value="' + item.valueText+ '" />';
            }
                
            frm += "</form >";

            var _frm = $(frm).appendTo("body");
            _frm.submit();
            _frm.remove();

            
        },

        printDirect: function (objReport) {

            var config = {
                headers: {
                    'Content-Type': 'text/plain' 
                },
                disabledHandleError: true
            };

            objReport.reportsFilters = objReport.filters || objReport.reportsFilters;

            var obj = this;
            $http.post("http://localhost:" + agentPrint.port + "/print", JSON.stringify(objReport), config).then(function (result) {

            },
                function (error) {
                    obj.print(objReport);
                }
            );
        }



    };


}]);
﻿angular.module('MetronicApp').directive('myFileUpload', function () {

    return {
        scope: {
            model: '=',
            name: '=',
            fileType: '=',
            fileSize: '=',
            data: '=',
            add: '@',
            aliasName: '@',
            aliasFileType: '@',
            aliasFileSize: '@',
            aliasData: '@',
        },
        link: function (scope, element, attrs) {

            scope.add = scope.add || false;
            element.on("change", function () {

                if (element[0].files.length <= 0) return;

                if (!scope.add || !scope.model)
                    scope.model = [];

                scope.aliasName = scope.aliasName || "name";
                scope.aliasFileType = scope.aliasFileType || "fileType";
                scope.aliasFileSize = scope.aliasFileSize || "fileSize";
                scope.aliasData = scope.aliasData || "data";
                var cargados = 0;

                for (var i = 0; i < element[0].files.length; i++) {
                    var f = element[0].files[i];

                    var img = {};
                    img[scope.aliasFileType] = f.type;
                    img[scope.aliasName] = f.name;
                    img[scope.aliasFileSize] = f.size;
                    var _index = scope.model.push(img) - 1;
                    var reader = new FileReader();
                    reader.onload = (function (theFile, index) {
                        return function (e) {
                            scope.model[index][scope.aliasData] = e.target.result;
                            cargados++;
                            if (cargados == element[0].files.length) {
                                if (!element.attr("multiple")) {
                                    scope.model = scope.model[0];
                                    try {
                                        scope.name = scope.model[scope.aliasName];
                                        scope.fileType = scope.model[scope.aliasFileType];
                                        scope.fileSize = scope.model[scope.aliasFileSize];
                                        scope.data = scope.model[scope.aliasData];
                                    } catch (e) {

                                    }

                                }
                                scope.$apply();
                            }
                        };
                    })(f, _index);
                    reader.readAsDataURL(f);

                }

            });
        },
        restric: "E"
    };

});
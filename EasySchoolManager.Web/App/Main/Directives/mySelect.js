﻿angular.module('MetronicApp').directive('mySelect',["$timeout", function ( $timeout) {

    return {
        scope: {
            url: '@',
            titleField: '@',
            model: '=',
            urlDataField: '@',
            dependency: '='
        },
        controller: ["$scope","$http","$attrs", function ($scope, $http, $attrs) {

            var init = function () {
                var request = {
                    method: "POST",
                    url: $scope.url
                };
                $http(request).then(
                    function (result) {

                        $scope.data = $scope.urlDataField ? result.data[$scope.urlDataField] : result.data;
                    });
            };

            $scope.$watch("url", function () {
                if ($attrs.dependency) {
                    if ($scope.dependency)
                        init();
                    else {
                        $scope.data = null;
                        $scope.model = null;
                    }

                }
                else
                    init();
            });

            $scope.$watchCollection("data", function () {
                $timeout(function () {
                    $('.select-choosen').selectpicker('refresh');
                }, 200);

            });
        }],
        template: function (element, attrs) {

            var attrList = "";
            var trackBy = attrs.trackBy ? " track by item." + attrs.trackBy : "";
            var titleFieldAs = attrs.titleFieldAs ? " as item." + attrs.titleFieldAs : "";
            for (var i in attrs) {
                if (typeof attrs[i] != 'string' || i == 'model') continue;
                attrList += (i + "='" + attrs[i] + "'");
            }

            return '<select ' + attrList + ' ng-model="model"  ng-options="item.' + attrs.titleField + titleFieldAs + ' for item in data' + trackBy + '"></select>';
        },
        restric: "A",
        replace: true
    };

}]);
﻿angular.module('MetronicApp').directive('filterElement', ["$compile", "$timeout", function ($compile, $timeout) {

    return {
        scope: {
            filtro: '=',
            range: '=',
            scope: '='
        },

        link: function (scope, element, attrs) {

            function getDefaultValue(obj) {
                if (obj.defaultValueComputed != undefined && obj.defaultValueComputed != null && obj.defaultValueComputed != "") {
                    return eval(obj.defaultValueComputed);
                }
                return obj.defaultValue || "";
            }

            var html = '';
            var value = 'value';
            var select = false;
            if (scope.filtro.urlService && scope.filtro.urlService != "") {
                html = '<my-select class="select-choosen form-control" ' +
                    'url="' + abp.appPath + scope.filtro.urlService + '" ' +
                    'title-field="' + scope.filtro.displayNameService + '" ' +
                    'data-live-search="true" ' +
                    'model="' + value + '" ' +
                    (scope.filtro.dependencyField ? ('dependency="scope.' + scope.filtro.dependencyField +'" ') : '' )+
                    'track-by="' + scope.filtro.fieldService + '"' +
                    'ng-required="' + scope.filtro.required + '"></my-select>';
                select = true;
            }
            else {
                html = '<input type="' + scope.filtro.dataType.typeHTML + '" name="' + scope.filtro.name + '" ng-model="' + value + '" class="form-control" ng-required="' + scope.filtro.required + '"/>';
            }

            scope.value = getDefaultValue(scope.filtro);

            scope.$watch(value, function () {
                if (scope.range)
                    scope.filtro.rangeValue = scope.value;
                else {

                    if (select) {
                        scope.filtro.value = scope.value[scope.filtro.fieldService];
                        scope.filtro.valueText = scope.value[scope.filtro.displayNameService];
                        scope.scope[scope.filtro.dataField] = scope.value[scope.filtro.fieldService];
                    }
                    else {
                        scope.filtro.value = scope.value;
                        scope.scope[scope.filtro.dataField] = scope.value;
                        scope[scope.filtro.dataField] = scope.value;
                    }

                }

            });

            var el = $compile(html)(scope);
            element.append(el);
        },
        restric: "A"
    };

}]);
angular.module('MetronicApp').directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {

                if (attrs.myEnter) {
                    scope.$apply(function () {
                        scope.$eval(attrs.myEnter, { 'event': event });
                    });
                }

                if (attrs.nextControl) {
                    var ctrl = document.getElementById(attrs.nextControl)
                    if (ctrl) ctrl.focus();
                }

                event.preventDefault();
            }


        });
    };
});
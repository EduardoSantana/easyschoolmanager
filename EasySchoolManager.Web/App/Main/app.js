/***
Metronic AngularJS App Main Script
***/
// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        value: function (predicate) {
            // 1. Let O be ? ToObject(this value).
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }

            var o = Object(this);

            // 2. Let len be ? ToLength(? Get(O, "length")).
            var len = o.length >>> 0;

            // 3. If IsCallable(predicate) is false, throw a TypeError exception.
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }

            // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
            var thisArg = arguments[1];

            // 5. Let k be 0.
            var k = 0;

            // 6. Repeat, while k < len
            while (k < len) {
                // a. Let Pk be ! ToString(k).
                // b. Let kValue be ? Get(O, Pk).
                // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
                // d. If testResult is true, return kValue.
                var kValue = o[k];
                if (predicate.call(thisArg, kValue, k, o)) {
                    return kValue;
                }
                // e. Increase k by 1.
                k++;
            }

            // 7. Return undefined.
            return undefined;
        }
    });
}

/* Metronic App */
var MetronicApp = angular.module("MetronicApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize",
    "ui.jq",
    "abp",
    "ui.grid",
    "angular.chosen",
    "720kb.datepicker",
    "angucomplete-alt"
]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MetronicApp.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
MetronicApp.config(['$controllerProvider', function ($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    // $controllerProvider.allowGlobals();
}]);

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

MetronicApp.factory('appSession', [
    function () {

        var _session = {
            user: null,
            tenant: null
        };

        abp.services.app.session.getCurrentLoginInformations({ async: false }).done(function (result) {
            _session.user = result.user;
            _session.tenant = result.tenant;
        });

        return _session;
    }
]);

/* Init global settings and run the app */
MetronicApp.run(["$rootScope", "settings", "$state", "helpers", "$http", function ($rootScope, settings, $state, helpers, $http) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
    $rootScope.$helpers = helpers;
    $rootScope.concessionLoadingScreen = true;

    $rootScope.$on('$viewContentLoaded', function () {
        var breadcrumb = [];
        breadcrumb.push({ desc: App.localize("Home"), name: "home", index: 1 });
        $rootScope.$state.$current.url.source.split('/').forEach(function (item, index) {
            if (item != '') {
                $rootScope.$settings.settings.layout.viewName = item;
                var objItem = $rootScope.$state.get().find(function (x) { return x.name === item; });
                if (objItem) {
                    breadcrumb.push(
                        {
                            desc: objItem.data.stateDesc,
                            name: objItem.name,
                            iconClass: objItem.data.iconClass,
                            index: breadcrumb.length + 1
                        });
                }

            } else {
                $rootScope.$settings.settings.layout.viewName = 'home';
            }
        });
        $rootScope.breadcrumb = breadcrumb;

        $rootScope.getMax = function (array, keyField) {
            if (array == null)
                return null;
            var returnedValued = array[0];
            for (var i = 0; i < array.length; i++) {
                var ele = array[i];
                if (ele[keyField] > returnedValued[keyField]) {
                    returnedValued = ele;
                }
            }
            return returnedValued;
        }

        $rootScope.getElementByKeyField = function (array, keyField, value) {
            var returnedValued = null;
            for (var i = 0; i < array.length; i++) {
                var ele = array[i];
                if (ele[keyField] == value) {
                    returnedValued = ele;
                    break;
                }
            }
            return returnedValued;
        }

    });

    $rootScope.left = function (text, length) {

        if (text.length > length)
            return text.substring(0, length) + "...";
        else
            return text;

    }

    $rootScope.sum = function (arreglo, propiedad, condicion) {
        if (!arreglo || arreglo.length <= 0) return 0;

        var total = 0;

        for (var i in arreglo) {
            if (condicion == null || condicion(arreglo[i]) == true) {
                if (typeof propiedad == 'string' && arreglo[i][propiedad])
                    total += arreglo[i][propiedad];
                else if (typeof propiedad == 'function')
                    total += propiedad(arreglo[i]);
            }

        }
        return total;
    }

    $http.get(abp.appPath + 'Home/SessionToken').then(function (result) {
        var config = {
            headers: {
                'Content-Type': 'text/plain'
            },
            disabledHandleError: true
        }

        var nothing = function () { };
        // comented to avoid the error on the loading application. 
        //$http.post("http://localhost:" + agentPrint.port + "/config", JSON.stringify(result.data), config).then(nothing, function (error) {
        //});
    });
}]);

MetronicApp.provider('settings', function () {

    var $this = this;

    // supported languages 
    this.settings = {
        layout: {
            pageSidebarClosed: true, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000, // auto scroll to top on page load
            viewName: '',
            showmenu: true
        },
        assetsPath: getRootUrl() + '/Content/assets',
        globalPath: getRootUrl() + '/Content/assets/global',
        layoutPath: getRootUrl() + '/Content/assets/layouts/layout2',
        appPath: getRootUrl() + '/App/Main',
        appPathNotHTTP: '/App/Main'
    };

    this.$get = function () {
        return $this;
    }

});


MetronicApp.provider('helpers', function () {

    var $this = this;
    this.getEditionId = function (record) {
        return parseInt(record.id);
    };

    this.getEditionIdOfTenant = function (record) {
        return parseInt(record.tenantId);
    };

    this.$get = function () {
        return $this;
    }

});

MetronicApp.config(['$stateProvider', '$urlRouterProvider', 'settingsProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, settingsProvider, $locationProvider) {

    var settings = settingsProvider.settings;

    $locationProvider.hashPrefix('');
    $urlRouterProvider.otherwise('/'); // Redirect any unmatched url {{settings.appPath}} + '/views/

    $stateProvider.state('home', {
        url: "/",
        templateUrl: settings.appPathNotHTTP + "/views/home/index.cshtml",
        data: { pageTitle: App.localize("Dashboard"), pageTitleSub: App.localize("DashboardPageTitleSub"), stateDesc: App.localize("DashboardStateDesc"), iconClass: 'fa-home' },
        controller: "MetronicApp.views.home.index",
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'MetronicApp',
                    insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                    files: [
                        getRootUrl() + '/Content/assets/global/plugins/morris/morris.css',
                        getRootUrl() + '/Content/assets/global/plugins/morris/morris.min.js',
                        getRootUrl() + '/Content/assets/global/plugins/morris/raphael-min.js',
                        getRootUrl() + '/Content/assets/global/plugins/jquery.sparkline.min.js',
                        getRootUrl() + '/Content/assets/pages/scripts/dashboard.min.js',
                    ]
                });
            }]
        }
    });

    //$stateProvider.state('blank', {
    //    url: "/blank",
    //    templateUrl: settings.appPathNotHTTP + "/views/home/blank.cshtml",
    //    data: { pageTitle: 'Account User Profile', pageTitleSub: 'Blank Page Template', stateDesc: 'Blank Page' , iconClass: 'fa-home'},
    //    controller: "MetronicApp.views.home.blank"
    //});

    if (abp.auth.hasPermission('Pages.Profile')) {
        $stateProvider
            .state('profile', {
                url: '/users/profile',
                templateUrl: settings.appPathNotHTTP + '/views/users/profile.cshtml',
                data: { pageTitle: App.localize("AbpusersProfilePageTitle"), pageTitleSub: App.localize("AbpusersProfilePageTitleSub"), stateDesc: App.localize("AbpusersProfileStateDesc"), iconClass: 'fa-user' },
            });
        $urlRouterProvider.otherwise('/users/profile');
    }

    $stateProvider
        .state('reports', {
            url: '/reports/details/:id',
            templateUrl: settings.appPathNotHTTP + '/views/reports/details.cshtml',
            data: { pageTitle: App.localize("ReportsPageTitle"), pageTitleSub: App.localize("ReportsPageTitleSub"), stateDesc: App.localize("ReportsStateDesc"), iconClass: 'fa-home' }
        });

    if (abp.auth.hasPermission('Pages.Users')) {
        $stateProvider
            .state('users', {
                url: '/users',
                templateUrl: settings.appPathNotHTTP + '/views/users/index.cshtml',
                data: { pageTitle: App.localize("AbpusersPageTitle"), pageTitleSub: App.localize("AbpusersPageTitleSub"), stateDesc: App.localize("AbpusersStateDesc"), iconClass: 'fa-user' },
                menu: 'Users' //Matches to name of 'Users' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/users');
    }

    if (abp.auth.hasPermission('Pages.Roles')) {
        $stateProvider
            .state('roles', {
                url: '/roles',
                templateUrl: settings.appPathNotHTTP + '/views/roles/index.cshtml',
                data: {
                    pageTitle: App.localize("AbprolesPageTitle"),
                    pageTitleSub: App.localize("AbprolesPageTitleSub"),
                    stateDesc: App.localize("AbprolesStateDesc")
                },
                menu: 'Roles' //Matches to name of 'Tenants' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/roles');
    }

    if (abp.auth.hasPermission('Pages.Tenants')) {
        $stateProvider
            .state('tenants', {
                url: '/tenants',
                templateUrl: settings.appPathNotHTTP + '/views/tenants/index.cshtml',
                data: { pageTitle: App.localize("AbptenantsPageTitle"), pageTitleSub: App.localize("AbptenantsPageTitleSub"), stateDesc: App.localize("AbptenantsStateDesc"), iconClass: 'fa-tenants' },
                menu: 'Tenants' //Matches to name of 'Tenants' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/tenants');
    }

    //No borrar esta linea, ya que se usar� para insertar los menus
    if (abp.auth.hasPermission('Pages.EvaluationPeriods')) {
        $stateProvider
            .state('evaluationPeriods', {
                url: '/evaluationPeriods',
                templateUrl: settings.appPathNotHTTP + '/views/evaluationPeriods/index.cshtml',
                data: { pageTitle: App.localize("EvaluationPeriodsPageTitle"), pageTitleSub: App.localize("EvaluationPeriodsPageTitleSub"), stateDesc: App.localize("EvaluationPeriodsStateDesc"), iconClass: 'fa-home' },
                menu: 'EvaluationPeriods' //Matches to name of 'EvaluationPeriods' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/evaluationPeriods');
    }

    if (abp.auth.hasPermission('Pages.Evaluations')) {
        $stateProvider
            .state('evaluations', {
                url: '/evaluations',
                templateUrl: settings.appPathNotHTTP + '/views/evaluations/index.cshtml',
                data: { pageTitle: App.localize("EvaluationsPageTitle"), pageTitleSub: App.localize("EvaluationsPageTitleSub"), stateDesc: App.localize("EvaluationsStateDesc"), iconClass: 'fa-home' },
                menu: 'Evaluations' //Matches to name of 'Evaluations' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/evaluations');
    }

    if (abp.auth.hasPermission('Pages.GrantEnterprises')) {
        $stateProvider
            .state('grantEnterprises', {
                url: '/grantEnterprises',
                templateUrl: settings.appPathNotHTTP + '/views/grantEnterprises/index.cshtml',
                data: { pageTitle: App.localize("GrantEnterprisesPageTitle"), pageTitleSub: App.localize("GrantEnterprisesPageTitleSub"), stateDesc: App.localize("GrantEnterprisesStateDesc"), iconClass: 'fa-home' },
                menu: 'GrantEnterprises' //Matches to name of 'GrantEnterprises' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/grantEnterprises');
    }



    //      if (abp.auth.hasPermission('Pages.HistoryCancelReceipts')) {
    //          $stateProvider
    //              .state('historyCancelReceipts', {
    //                  url: '/historyCancelReceipts',
    //                  templateUrl: settings.appPathNotHTTP + '/views/historyCancelReceipts/index.cshtml',
    //data: { pageTitle: App.localize("HistoryCancelReceiptsPageTitle"), pageTitleSub: App.localize("HistoryCancelReceiptsPageTitleSub"), stateDesc: App.localize("HistoryCancelReceiptsStateDesc"),  iconClass: 'fa-home' },
    //                  menu: 'HistoryCancelReceipts' //Matches to name of 'HistoryCancelReceipts' menu in EasySchoolManagerNavigationProvider
    //              });
    //          $urlRouterProvider.otherwise('/historyCancelReceipts');
    //      }


    if (abp.auth.hasPermission('Pages.SubjectHighs')) {
        $stateProvider
            .state('subjectHighs', {
                url: '/subjectHighs',
                templateUrl: settings.appPathNotHTTP + '/views/subjectHighs/index.cshtml',
                data: { pageTitle: App.localize("SubjectHighsPageTitle"), pageTitleSub: App.localize("SubjectHighsPageTitleSub"), stateDesc: App.localize("SubjectHighsStateDesc"), iconClass: 'fa-home' },
                menu: 'SubjectHighs' //Matches to name of 'SubjectHighs' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/subjectHighs');
    }

    if (abp.auth.hasPermission('Pages.EvaluationPositionHighs')) {
        $stateProvider
            .state('evaluationPositionHighs', {
                url: '/evaluationPositionHighs',
                templateUrl: settings.appPathNotHTTP + '/views/evaluationPositionHighs/index.cshtml',
                data: { pageTitle: App.localize("EvaluationPositionHighsPageTitle"), pageTitleSub: App.localize("EvaluationPositionHighsPageTitleSub"), stateDesc: App.localize("EvaluationPositionHighsStateDesc"), iconClass: 'fa-home' },
                menu: 'EvaluationPositionHighs' //Matches to name of 'EvaluationPositionHighs' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/evaluationPositionHighs');
    }



    if (abp.auth.hasPermission('Pages.EvaluationOrderHighs')) {
        $stateProvider
            .state('evaluationOrderHighs', {
                url: '/evaluationOrderHighs',
                templateUrl: settings.appPathNotHTTP + '/views/evaluationOrderHighs/index.cshtml',
                data: { pageTitle: App.localize("EvaluationOrderHighsPageTitle"), pageTitleSub: App.localize("EvaluationOrderHighsPageTitleSub"), stateDesc: App.localize("EvaluationOrderHighsStateDesc"), iconClass: 'fa-home' },
                menu: 'EvaluationOrderHighs' //Matches to name of 'EvaluationOrderHighs' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/evaluationOrderHighs');
    }

    if (abp.auth.hasPermission('Pages.EvaluationHighsByPos')) {
        $stateProvider
            .state('evaluationHighsByPos', {
                url: '/evaluationHighsByPos',
                templateUrl: settings.appPathNotHTTP + '/views/evaluationHighsByPos/index.cshtml',
                data: { pageTitle: App.localize("EvaluationHighsByPosPageTitle"), pageTitleSub: App.localize("EvaluationHighsByPosPageTitleSub"), stateDesc: App.localize("EvaluationHighsByPosStateDesc"), iconClass: 'fa-home' },
                menu: 'EvaluationHighsByPos' //Matches to name of 'EvaluationHighs' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/evaluationHighsByPos');
    }

    if (abp.auth.hasPermission('Pages.EvaluationHighsByPosPrevious')) {
        $stateProvider
            .state('evaluationHighsByPosPrevious', {
                url: '/evaluationHighsByPosPrevious',
                templateUrl: settings.appPathNotHTTP + '/views/evaluationHighsByPosPrevious/index.cshtml',
                data: { pageTitle: App.localize("EvaluationHighsByPosPreviousPageTitle"), pageTitleSub: App.localize("EvaluationHighsByPosPreviousPageTitleSub"), stateDesc: App.localize("EvaluationHighsByPosPreviousStateDesc"), iconClass: 'fa-home' },
                menu: 'EvaluationHighsByPosPrevious' //Matches to name of 'EvaluationHighs' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/evaluationHighsByPosPrevious');
    }

    if (abp.auth.hasPermission('Pages.EvaluationHighs')) {
        $stateProvider
            .state('evaluationHighs', {
                url: '/evaluationHighs',
                templateUrl: settings.appPathNotHTTP + '/views/evaluationHighs/index.cshtml',
                data: { pageTitle: App.localize("EvaluationHighsPageTitle"), pageTitleSub: App.localize("EvaluationHighsPageTitleSub"), stateDesc: App.localize("EvaluationHighsStateDesc"), iconClass: 'fa-home' },
                menu: 'EvaluationHighs' //Matches to name of 'EvaluationHighs' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/evaluationHighs');
    }


    if (abp.auth.hasPermission('Pages.TransactionQueues')) {
        $stateProvider
            .state('TransactionQueues', {
                url: '/TransactionQueues',
                templateUrl: settings.appPathNotHTTP + '/views/TransactionQueues/index.cshtml',
                data: { pageTitle: App.localize("TransactionQueuesPageTitle"), pageTitleSub: App.localize("TransactionQueuesPageTitleSub"), stateDesc: App.localize("TransactionQueuesStateDesc"), iconClass: 'fa-home' },
                menu: 'TransactionQueues' //Matches to name of 'TransactionQueues' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/TransactionQueues');
    }

    if (abp.auth.hasPermission('Pages.TaxReceiptTypes')) {
        $stateProvider
            .state('taxReceiptTypes', {
                url: '/taxReceiptTypes',
                templateUrl: settings.appPathNotHTTP + '/views/taxReceiptTypes/index.cshtml',
                data: { pageTitle: App.localize("TaxReceiptTypesPageTitle"), pageTitleSub: App.localize("TaxReceiptTypesPageTitleSub"), stateDesc: App.localize("TaxReceiptTypesStateDesc"), iconClass: 'fa-home' },
                menu: 'TaxReceiptTypes' //Matches to name of 'TaxReceiptTypes' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/taxReceiptTypes');
    }

    if (abp.auth.hasPermission('Pages.TaxReceipts')) {
        $stateProvider
            .state('taxReceipts', {
                url: '/taxReceipts',
                templateUrl: settings.appPathNotHTTP + '/views/taxReceipts/index.cshtml',
                data: { pageTitle: App.localize("TaxReceiptsPageTitle"), pageTitleSub: App.localize("TaxReceiptsPageTitleSub"), stateDesc: App.localize("TaxReceiptsStateDesc"), iconClass: 'fa-home' },
                menu: 'TaxReceipts' //Matches to name of 'TaxReceipts' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/taxReceipts');
    }

    if (abp.auth.hasPermission('Pages.Companies')) {
        $stateProvider
            .state('companies', {
                url: '/companies',
                templateUrl: settings.appPathNotHTTP + '/views/companies/index.cshtml',
                data: { pageTitle: App.localize("CompaniesPageTitle"), pageTitleSub: App.localize("CompaniesPageTitleSub"), stateDesc: App.localize("CompaniesStateDesc"), iconClass: 'fa-home' },
                menu: 'Companies' //Matches to name of 'Companies' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/companies');
    }

    if (abp.auth.hasPermission('Pages.CancelStatus')) {
        $stateProvider
            .state('cancelStatus', {
                url: '/cancelStatus',
                templateUrl: settings.appPathNotHTTP + '/views/cancelStatus/index.cshtml',
                data: { pageTitle: App.localize("CancelStatusPageTitle"), pageTitleSub: App.localize("CancelStatusPageTitleSub"), stateDesc: App.localize("CancelStatusStateDesc"), iconClass: 'fa-home' },
                menu: 'CancelStatus' //Matches to name of 'CancelStatus' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/cancelStatus');
    }

    if (abp.auth.hasPermission('Pages.SubjectSessions')) {
        $stateProvider
            .state('subjectSessions', {
                url: '/subjectSessions',
                templateUrl: settings.appPathNotHTTP + '/views/subjectSessions/index.cshtml',
                data: { pageTitle: App.localize("SubjectSessionsPageTitle"), pageTitleSub: App.localize("SubjectSessionsPageTitleSub"), stateDesc: App.localize("SubjectSessionsStateDesc"), iconClass: 'fa-home' },
                menu: 'SubjectSessions' //Matches to name of 'SubjectSessions' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/subjectSessions');
    }

    if (abp.auth.hasPermission('Pages.SubjectSessionHighs')) {
        $stateProvider
            .state('subjectSessionHighs', {
                url: '/subjectSessionHighs',
                templateUrl: settings.appPathNotHTTP + '/views/subjectSessionHighs/index.cshtml',
                data: { pageTitle: App.localize("SubjectSessionHighsPageTitle"), pageTitleSub: App.localize("SubjectSessionHighsPageTitleSub"), stateDesc: App.localize("SubjectSessionHighsStateDesc"), iconClass: 'fa-home' },
                menu: 'SubjectSessionHighs' //Matches to name of 'SubjectSessionHighs' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/subjectSessionHighs');
    }

    if (abp.auth.hasPermission('Pages.ConceptTenantRegistrations')) {
        $stateProvider
            .state('conceptTenantRegistrations', {
                url: '/conceptTenantRegistrations',
                templateUrl: settings.appPathNotHTTP + '/views/conceptTenantRegistrations/index.cshtml',
                data: { pageTitle: App.localize("ConceptTenantRegistrationsPageTitle"), pageTitleSub: App.localize("ConceptTenantRegistrationsPageTitleSub"), stateDesc: App.localize("ConceptTenantRegistrationsStateDesc"), iconClass: 'fa-home' },
                menu: 'ConceptTenantRegistrations' //Matches to name of 'ConceptTenantRegistrations' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/conceptTenantRegistrations');
    }

    if (abp.auth.hasPermission('Pages.VerifoneBreaks')) {
        $stateProvider
            .state('verifoneBreaks', {
                url: '/verifoneBreaks',
                templateUrl: settings.appPathNotHTTP + '/views/verifoneBreaks/index.cshtml',
                data: { pageTitle: App.localize("VerifoneBreaksPageTitle"), pageTitleSub: App.localize("VerifoneBreaksPageTitleSub"), stateDesc: App.localize("VerifoneBreaksStateDesc"), iconClass: 'fa-home' },
                menu: 'VerifoneBreaks' //Matches to name of 'VerifoneBreaks' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/verifoneBreaks');
    }

    if (abp.auth.hasPermission('Pages.Discounts')) {
        $stateProvider
            .state('discounts', {
                url: '/discounts',
                templateUrl: settings.appPathNotHTTP + '/views/discounts/index.cshtml',
                data: { pageTitle: App.localize("DiscountsPageTitle"), pageTitleSub: App.localize("DiscountsPageTitleSub"), stateDesc: App.localize("DiscountsStateDesc"), iconClass: 'fa-home' },
                menu: 'Discounts' //Matches to name of 'Discounts' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/discounts');
    }

    if (abp.auth.hasPermission('Pages.DiscountNames')) {
        $stateProvider
            .state('discountNames', {
                url: '/discountNames',
                templateUrl: settings.appPathNotHTTP + '/views/discountNames/index.cshtml',
                data: { pageTitle: App.localize("DiscountNamesPageTitle"), pageTitleSub: App.localize("DiscountNamesPageTitleSub"), stateDesc: App.localize("DiscountNamesStateDesc"), iconClass: 'fa-home' },
                menu: 'DiscountNames' //Matches to name of 'DiscountNames' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/discountNames');
    }

    if (abp.auth.hasPermission('Pages.PeriodDiscounts')) {
        $stateProvider
            .state('periodDiscounts', {
                url: '/periodDiscounts',
                templateUrl: settings.appPathNotHTTP + '/views/periodDiscounts/index.cshtml',
                data: { pageTitle: App.localize("PeriodDiscountsPageTitle"), pageTitleSub: App.localize("PeriodDiscountsPageTitleSub"), stateDesc: App.localize("PeriodDiscountsStateDesc"), iconClass: 'fa-home' },
                menu: 'PeriodDiscounts' //Matches to name of 'PeriodDiscounts' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/periodDiscounts');
    }

    //XXXInsertAppJsMenuXXX

    if (abp.auth.hasPermission('Pages.Indicators')) {
        $stateProvider
            .state('indicators', {
                url: '/indicators',
                templateUrl: settings.appPathNotHTTP + '/views/indicators/index.cshtml',
                data: { pageTitle: App.localize("IndicatorsPageTitle"), pageTitleSub: App.localize("IndicatorsPageTitleSub"), stateDesc: App.localize("IndicatorsStateDesc"), iconClass: 'fa-home' },
                menu: 'Indicators' //Matches to name of 'Indicators' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/indicators');
    }

    if (abp.auth.hasPermission('Pages.EvaluationLegends')) {
        $stateProvider
            .state('evaluationLegends', {
                url: '/evaluationLegends',
                templateUrl: settings.appPathNotHTTP + '/views/evaluationLegends/index.cshtml',
                data: { pageTitle: App.localize("EvaluationLegendsPageTitle"), pageTitleSub: App.localize("EvaluationLegendsPageTitleSub"), stateDesc: App.localize("EvaluationLegendsStateDesc"), iconClass: 'fa-home' },
                menu: 'EvaluationLegends' //Matches to name of 'EvaluationLegends' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/evaluationLegends');
    }

    if (abp.auth.hasPermission('Pages.IndicatorGroups')) {
        $stateProvider
            .state('indicatorGroups', {
                url: '/indicatorGroups',
                templateUrl: settings.appPathNotHTTP + '/views/indicatorGroups/index.cshtml',
                data: { pageTitle: App.localize("IndicatorGroupsPageTitle"), pageTitleSub: App.localize("IndicatorGroupsPageTitleSub"), stateDesc: App.localize("IndicatorGroupsStateDesc"), iconClass: 'fa-home' },
                menu: 'IndicatorGroups' //Matches to name of 'IndicatorGroups' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/indicatorGroups');
    }

    if (abp.auth.hasPermission('Pages.Subjects')) {
        $stateProvider
            .state('subjects', {
                url: '/subjects',
                templateUrl: settings.appPathNotHTTP + '/views/subjects/index.cshtml',
                data: { pageTitle: App.localize("SubjectsPageTitle"), pageTitleSub: App.localize("SubjectsPageTitleSub"), stateDesc: App.localize("SubjectsStateDesc"), iconClass: 'fa-home' },
                menu: 'Subjects' //Matches to name of 'Subjects' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/subjects');
    }

    if (abp.auth.hasPermission('Pages.Teachers')) {
        $stateProvider
            .state('teachers', {
                url: '/teachers',
                templateUrl: settings.appPathNotHTTP + '/views/teachers/index.cshtml',
                data: { pageTitle: App.localize("TeachersPageTitle"), pageTitleSub: App.localize("TeachersPageTitleSub"), stateDesc: App.localize("TeachersStateDesc"), iconClass: 'fa-home' },
                menu: 'Teachers' //Matches to name of 'Teachers' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/teachers');
    }


    //if (abp.auth.hasPermission('Pages.T2Dimensions')) {
    //    $stateProvider
    //        .state('t2Dimensions', {
    //            url: '/t2Dimensions',
    //            templateUrl: settings.appPathNotHTTP + '/views/t2Dimensions/index.cshtml',
    //            data: { pageTitle: App.localize("T2DimensionsPageTitle"), pageTitleSub: App.localize("T2DimensionsPageTitleSub"), stateDesc: App.localize("T2DimensionsStateDesc"), iconClass: 'fa-home' },
    //            menu: 'T2Dimensions' //Matches to name of 'T2Dimensions' menu in EasySchoolManagerNavigationProvider
    //        });
    //    $urlRouterProvider.otherwise('/t2Dimensions');
    //}

    //if (abp.auth.hasPermission('Pages.T10Dimensions')) {
    //    $stateProvider
    //        .state('t10Dimensions', {
    //            url: '/t10Dimensions',
    //            templateUrl: settings.appPathNotHTTP + '/views/t10Dimensions/index.cshtml',
    //            data: { pageTitle: App.localize("T10DimensionsPageTitle"), pageTitleSub: App.localize("T10DimensionsPageTitleSub"), stateDesc: App.localize("T10DimensionsStateDesc"), iconClass: 'fa-home' },
    //            menu: 'T10Dimensions' //Matches to name of 'T10Dimensions' menu in EasySchoolManagerNavigationProvider
    //        });
    //    $urlRouterProvider.otherwise('/t10Dimensions');
    //}

    //if (abp.auth.hasPermission('Pages.T9Dimensions')) {
    //    $stateProvider
    //        .state('t9Dimensions', {
    //            url: '/t9Dimensions',
    //            templateUrl: settings.appPathNotHTTP + '/views/t9Dimensions/index.cshtml',
    //            data: { pageTitle: App.localize("T9DimensionsPageTitle"), pageTitleSub: App.localize("T9DimensionsPageTitleSub"), stateDesc: App.localize("T9DimensionsStateDesc"), iconClass: 'fa-home' },
    //            menu: 'T9Dimensions' //Matches to name of 'T9Dimensions' menu in EasySchoolManagerNavigationProvider
    //        });
    //    $urlRouterProvider.otherwise('/t9Dimensions');
    //}

    //if (abp.auth.hasPermission('Pages.T8Dimensions')) {
    //    $stateProvider
    //        .state('t8Dimensions', {
    //            url: '/t8Dimensions',
    //            templateUrl: settings.appPathNotHTTP + '/views/t8Dimensions/index.cshtml',
    //            data: { pageTitle: App.localize("T8DimensionsPageTitle"), pageTitleSub: App.localize("T8DimensionsPageTitleSub"), stateDesc: App.localize("T8DimensionsStateDesc"), iconClass: 'fa-home' },
    //            menu: 'T8Dimensions' //Matches to name of 'T8Dimensions' menu in EasySchoolManagerNavigationProvider
    //        });
    //    $urlRouterProvider.otherwise('/t8Dimensions');
    //}

    //if (abp.auth.hasPermission('Pages.T7Dimensions')) {
    //    $stateProvider
    //        .state('t7Dimensions', {
    //            url: '/t7Dimensions',
    //            templateUrl: settings.appPathNotHTTP + '/views/t7Dimensions/index.cshtml',
    //            data: { pageTitle: App.localize("T7DimensionsPageTitle"), pageTitleSub: App.localize("T7DimensionsPageTitleSub"), stateDesc: App.localize("T7DimensionsStateDesc"), iconClass: 'fa-home' },
    //            menu: 'T7Dimensions' //Matches to name of 'T7Dimensions' menu in EasySchoolManagerNavigationProvider
    //        });
    //    $urlRouterProvider.otherwise('/t7Dimensions');
    //}

    //if (abp.auth.hasPermission('Pages.T6Dimensions')) {
    //    $stateProvider
    //        .state('t6Dimensions', {
    //            url: '/t6Dimensions',
    //            templateUrl: settings.appPathNotHTTP + '/views/t6Dimensions/index.cshtml',
    //            data: { pageTitle: App.localize("T6DimensionsPageTitle"), pageTitleSub: App.localize("T6DimensionsPageTitleSub"), stateDesc: App.localize("T6DimensionsStateDesc"), iconClass: 'fa-home' },
    //            menu: 'T6Dimensions' //Matches to name of 'T6Dimensions' menu in EasySchoolManagerNavigationProvider
    //        });
    //    $urlRouterProvider.otherwise('/t6Dimensions');
    //}

    //if (abp.auth.hasPermission('Pages.T5Dimensions')) {
    //    $stateProvider
    //        .state('t5Dimensions', {
    //            url: '/t5Dimensions',
    //            templateUrl: settings.appPathNotHTTP + '/views/t5Dimensions/index.cshtml',
    //            data: { pageTitle: App.localize("T5DimensionsPageTitle"), pageTitleSub: App.localize("T5DimensionsPageTitleSub"), stateDesc: App.localize("T5DimensionsStateDesc"), iconClass: 'fa-home' },
    //            menu: 'T5Dimensions' //Matches to name of 'T5Dimensions' menu in EasySchoolManagerNavigationProvider
    //        });
    //    $urlRouterProvider.otherwise('/t5Dimensions');
    //}

    //if (abp.auth.hasPermission('Pages.T4Dimensions')) {
    //    $stateProvider
    //        .state('t4Dimensions', {
    //            url: '/t4Dimensions',
    //            templateUrl: settings.appPathNotHTTP + '/views/t4Dimensions/index.cshtml',
    //            data: { pageTitle: App.localize("T4DimensionsPageTitle"), pageTitleSub: App.localize("T4DimensionsPageTitleSub"), stateDesc: App.localize("T4DimensionsStateDesc"), iconClass: 'fa-home' },
    //            menu: 'T4Dimensions' //Matches to name of 'T4Dimensions' menu in EasySchoolManagerNavigationProvider
    //        });
    //    $urlRouterProvider.otherwise('/t4Dimensions');
    //}

    //if (abp.auth.hasPermission('Pages.T1Dimensions')) {
    //    $stateProvider
    //        .state('t1Dimensions', {
    //            url: '/t1Dimensions',
    //            templateUrl: settings.appPathNotHTTP + '/views/t1Dimensions/index.cshtml',
    //            data: { pageTitle: App.localize("T1DimensionsPageTitle"), pageTitleSub: App.localize("T1DimensionsPageTitleSub"), stateDesc: App.localize("T1DimensionsStateDesc"), iconClass: 'fa-home' },
    //            menu: 'T1Dimensions' //Matches to name of 'T1Dimensions' menu in EasySchoolManagerNavigationProvider
    //        });
    //    $urlRouterProvider.otherwise('/t1Dimensions');
    //}

    //if (abp.auth.hasPermission('Pages.T3Dimensions')) {
    //    $stateProvider
    //        .state('t3Dimensions', {
    //            url: '/t3Dimensions',
    //            templateUrl: settings.appPathNotHTTP + '/views/t3Dimensions/index.cshtml',
    //            data: { pageTitle: App.localize("T3DimensionsPageTitle"), pageTitleSub: App.localize("T3DimensionsPageTitleSub"), stateDesc: App.localize("T3DimensionsStateDesc"), iconClass: 'fa-home' },
    //            menu: 'T3Dimensions' //Matches to name of 'T3Dimensions' menu in EasySchoolManagerNavigationProvider
    //        });
    //    $urlRouterProvider.otherwise('/t3Dimensions');
    //}


    if (abp.auth.hasPermission('Pages.Positions')) {
        $stateProvider
            .state('positions', {
                url: '/positions',
                templateUrl: settings.appPathNotHTTP + '/views/positions/index.cshtml',
                data: { pageTitle: App.localize("PositionsPageTitle"), pageTitleSub: App.localize("PositionsPageTitleSub"), stateDesc: App.localize("PositionsStateDesc"), iconClass: 'fa-home' },
                menu: 'Positions' //Matches to name of 'Positions' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/positions');
    }

    if (abp.auth.hasPermission('Pages.InventoryShops')) {
        $stateProvider
            .state('inventoryShops', {
                url: '/inventoryShops',
                templateUrl: settings.appPathNotHTTP + '/views/inventoryShops/index.cshtml',
                data: { pageTitle: App.localize("InventoryShopsPageTitle"), pageTitleSub: App.localize("InventoryShopsPageTitleSub"), stateDesc: App.localize("InventoryShopsStateDesc"), iconClass: 'fa-home' },
                menu: 'InventoryShops' //Matches to name of 'InventoryShops' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/inventoryShops');
    }

    if (abp.auth.hasPermission('Pages.Inventory')) {
        $stateProvider
            .state('inventory', {
                url: '/inventory',
                templateUrl: settings.appPathNotHTTP + '/views/inventory/index.cshtml',
                data: { pageTitle: App.localize("InventoryPageTitle"), pageTitleSub: App.localize("InventoryPageTitleSub"), stateDesc: App.localize("InventoryStateDesc"), iconClass: 'fa-home' },
                menu: 'Inventory' //Matches to name of 'Inventories' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/inventory');
    }

    if (abp.auth.hasPermission('Pages.ButtonPositions')) {
        $stateProvider
            .state('buttonPositions', {
                url: '/buttonPositions',
                templateUrl: settings.appPathNotHTTP + '/views/buttonPositions/index.cshtml',
                data: { pageTitle: App.localize("ButtonPositionsPageTitle"), pageTitleSub: App.localize("ButtonPositionsPageTitleSub"), stateDesc: App.localize("ButtonPositionsStateDesc"), iconClass: 'fa-home' },
                menu: 'ButtonPositions' //Matches to name of 'ButtonPositions' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/buttonPositions');
    }

    if (abp.auth.hasPermission('Pages.TransactionShops')) {
        $stateProvider
            .state('transactionShops', {
                url: '/transactionShops',
                templateUrl: settings.appPathNotHTTP + '/views/transactionShops/index.cshtml',
                data: { pageTitle: App.localize("TransactionShopsPageTitle"), pageTitleSub: App.localize("TransactionShopsPageTitleSub"), stateDesc: App.localize("TransactionShopsStateDesc"), iconClass: 'fa-home' },
                menu: 'TransactionShops' //Matches to name of 'TransactionShops' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/transactionShops');
    }

    if (abp.auth.hasPermission('Pages.ClientShops')) {
        $stateProvider
            .state('clientShops', {
                url: '/clientShops',
                templateUrl: settings.appPathNotHTTP + '/views/clientShops/index.cshtml',
                data: { pageTitle: App.localize("ClientShopsPageTitle"), pageTitleSub: App.localize("ClientShopsPageTitleSub"), stateDesc: App.localize("ClientShopsStateDesc"), iconClass: 'fa-home' },
                menu: 'ClientShops' //Matches to name of 'ClientShops' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/clientShops');
    }

    if (abp.auth.hasPermission('Pages.PurchaseShops')) {
        $stateProvider
            .state('purchaseShops', {
                url: '/purchaseShops',
                templateUrl: settings.appPathNotHTTP + '/views/purchaseShops/index.cshtml',
                data: { pageTitle: App.localize("PurchaseShopsPageTitle"), pageTitleSub: App.localize("PurchaseShopsPageTitleSub"), stateDesc: App.localize("PurchaseShopsStateDesc"), iconClass: 'fa-home' },
                menu: 'PurchaseShops' //Matches to name of 'PurchaseShops' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/purchaseShops');
    }

    if (abp.auth.hasPermission('Pages.ProviderShops')) {
        $stateProvider
            .state('providerShops', {
                url: '/providerShops',
                templateUrl: settings.appPathNotHTTP + '/views/providerShops/index.cshtml',
                data: { pageTitle: App.localize("ProviderShopsPageTitle"), pageTitleSub: App.localize("ProviderShopsPageTitleSub"), stateDesc: App.localize("ProviderShopsStateDesc"), iconClass: 'fa-home' },
                menu: 'ProviderShops' //Matches to name of 'ProviderShops' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/providerShops');
    }

    if (abp.auth.hasPermission('Pages.ArticleShops')) {
        $stateProvider
            .state('articleShops', {
                url: '/articleShops',
                templateUrl: settings.appPathNotHTTP + '/views/articleShops/index.cshtml',
                data: { pageTitle: App.localize("ArticleShopsPageTitle"), pageTitleSub: App.localize("ArticleShopsPageTitleSub"), stateDesc: App.localize("ArticleShopsStateDesc"), iconClass: 'fa-home' },
                menu: 'ArticleShops' //Matches to name of 'ArticleShops' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/articleShops');
    }

    if (abp.auth.hasPermission('Pages.Messages')) {
        $stateProvider
            .state('messages', {
                url: '/messages',
                templateUrl: settings.appPathNotHTTP + '/views/messages/index.cshtml',
                data: { pageTitle: App.localize("MessagesPageTitle"), pageTitleSub: App.localize("MessagesPageTitleSub"), stateDesc: App.localize("MessagesStateDesc"), iconClass: 'fa-home' },
                menu: 'Messages' //Matches to name of 'Messages' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/messages');

        $stateProvider
            .state('messagesId', {
                url: '/messages/:id',
                templateUrl: settings.appPathNotHTTP + '/views/messages/index.cshtml',
                data: { pageTitle: App.localize("MessagesPageTitle"), pageTitleSub: App.localize("MessagesPageTitleSub"), stateDesc: App.localize("MessagesStateDesc"), iconClass: 'fa-home' },
                menu: 'Messages' //Matches to name of 'Messages' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/messages');
    }

    if (abp.auth.hasPermission('Pages.Trades')) {
        $stateProvider
            .state('trades', {
                url: '/trades',
                templateUrl: settings.appPathNotHTTP + '/views/trades/index.cshtml',
                data: { pageTitle: App.localize("TradesPageTitle"), pageTitleSub: App.localize("TradesPageTitleSub"), stateDesc: App.localize("TradesStateDesc"), iconClass: 'fa-home' },
                menu: 'Trades' //Matches to name of 'Trades' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/trades');
    }

    if (abp.auth.hasPermission('Pages.CourrierPersons')) {
        $stateProvider
            .state('courrierPersons', {
                url: '/courrierPersons',
                templateUrl: settings.appPathNotHTTP + '/views/courrierPersons/index.cshtml',
                data: { pageTitle: App.localize("CourrierPersonsPageTitle"), pageTitleSub: App.localize("CourrierPersonsPageTitleSub"), stateDesc: App.localize("CourrierPersonsStateDesc"), iconClass: 'fa-home' },
                menu: 'CourrierPersons' //Matches to name of 'CourrierPersons' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/courrierPersons');
    }

    if (abp.auth.hasPermission('Pages.Purchases')) {
        $stateProvider
            .state('purchases', {
                url: '/purchases',
                templateUrl: settings.appPathNotHTTP + '/views/purchases/index.cshtml',
                data: { pageTitle: App.localize("PurchasesPageTitle"), pageTitleSub: App.localize("PurchasesPageTitleSub"), stateDesc: App.localize("PurchasesStateDesc"), iconClass: 'fa-home' },
                menu: 'Purchases' //Matches to name of 'Purchases' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/purchases');
    }

    if (abp.auth.hasPermission('Pages.ReceiptTypes')) {
        $stateProvider
            .state('receiptTypes', {
                url: '/receiptTypes',
                templateUrl: settings.appPathNotHTTP + '/views/receiptTypes/index.cshtml',
                data: { pageTitle: App.localize("ReceiptTypesPageTitle"), pageTitleSub: App.localize("ReceiptTypesPageTitleSub"), stateDesc: App.localize("ReceiptTypesStateDesc"), iconClass: 'fa-home' },
                menu: 'ReceiptTypes' //Matches to name of 'ReceiptTypes' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/receiptTypes');
    }

    if (abp.auth.hasPermission('Pages.TransactionTypes')) {
        $stateProvider
            .state('transactionTypes', {
                url: '/transactionTypes',
                templateUrl: settings.appPathNotHTTP + '/views/transactionTypes/index.cshtml',
                data: { pageTitle: App.localize("TransactionTypesPageTitle"), pageTitleSub: App.localize("TransactionTypesPageTitleSub"), stateDesc: App.localize("TransactionTypesStateDesc"), iconClass: 'fa-home' },
                menu: 'TransactionTypes' //Matches to name of 'TransactionTypes' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/transactionTypes');
    }

    if (abp.auth.hasPermission('Pages.Providers')) {
        $stateProvider
            .state('providers', {
                url: '/providers',
                templateUrl: settings.appPathNotHTTP + '/views/providers/index.cshtml',
                data: { pageTitle: App.localize("ProvidersPageTitle"), pageTitleSub: App.localize("ProvidersPageTitleSub"), stateDesc: App.localize("ProvidersStateDesc"), iconClass: 'fa-home' },
                menu: 'Providers' //Matches to name of 'Providers' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/providers');
    }

    if (abp.auth.hasPermission('Pages.Articles')) {
        $stateProvider
            .state('articles', {
                url: '/articles',
                templateUrl: settings.appPathNotHTTP + '/views/articles/index.cshtml',
                data: { pageTitle: App.localize("ArticlesPageTitle"), pageTitleSub: App.localize("ArticlesPageTitleSub"), stateDesc: App.localize("ArticlesStateDesc"), iconClass: 'fa-home' },
                menu: 'Articles' //Matches to name of 'Articles' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/articles');
    }

    if (abp.auth.hasPermission('Pages.ArticlesTypes')) {
        $stateProvider
            .state('articlesTypes', {
                url: '/articlesTypes',
                templateUrl: settings.appPathNotHTTP + '/views/articlesTypes/index.cshtml',
                data: { pageTitle: App.localize("ArticlesTypesPageTitle"), pageTitleSub: App.localize("ArticlesTypesPageTitleSub"), stateDesc: App.localize("ArticlesTypesStateDesc"), iconClass: 'fa-home' },
                menu: 'ArticlesTypes' //Matches to name of 'ArticlesTypes' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/articlesTypes');
    }

    if (abp.auth.hasPermission('Pages.Brands')) {
        $stateProvider
            .state('brands', {
                url: '/brands',
                templateUrl: settings.appPathNotHTTP + '/views/brands/index.cshtml',
                data: { pageTitle: App.localize("BrandsPageTitle"), pageTitleSub: App.localize("BrandsPageTitleSub"), stateDesc: App.localize("BrandsStateDesc"), iconClass: 'fa-home' },
                menu: 'Brands' //Matches to name of 'Brands' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/brands');
    }

    if (abp.auth.hasPermission('Pages.Units')) {
        $stateProvider
            .state('Units', {
                url: '/Units',
                templateUrl: settings.appPathNotHTTP + '/views/Units/index.cshtml',
                data: { pageTitle: App.localize("UnitsPageTitle"), pageTitleSub: App.localize("UnitsPageTitleSub"), stateDesc: App.localize("UnitsStateDesc"), iconClass: 'fa-home' },
                menu: 'Units' //Matches to name of 'Units' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/Units');
    }


    if (abp.auth.hasPermission('Pages.AccountBalances')) {
        $stateProvider
            .state('accountBalances', {
                url: '/accountBalances',
                templateUrl: settings.appPathNotHTTP + '/views/transactions/accountBalance.cshtml',
                data: { pageTitle: App.localize("AccountBalancePageTitle"), pageTitleSub: App.localize("AccountBalancesPageTitleSub"), stateDesc: App.localize("AccountBalancesStateDesc"), iconClass: 'fa-home' },
                menu: 'AccountBalances' //Matches to name of 'Districts' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/accountBalances');
    }

    if (abp.auth.hasPermission('Pages.ViewPaymentProjections')) {
        $stateProvider
            .state('viewPaymentProjections', {
                url: '/viewPaymentProjections',
                templateUrl: settings.appPathNotHTTP + '/views/enrollments/viewPaymentProjections.cshtml',
                data: { pageTitle: App.localize("ViewPaymentProjectionPageTitle"), pageTitleSub: App.localize("ViewPaymentProjectionsPageTitleSub"), stateDesc: App.localize("ViewPaymentProjectionsStateDesc"), iconClass: 'fa-home' },
                menu: 'ViewPaymentProjections' //Matches to name of 'Districts' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/viewPaymentProjections');
    }


    if (abp.auth.hasPermission('Pages.GenerateAllPaymentProjections')) {
        $stateProvider
            .state('generateAllPaymentProjections', {
                url: '/generateAllPaymentProjections',
                templateUrl: settings.appPathNotHTTP + '/views/enrollments/generateAllPaymentProjections.cshtml',
                data: { pageTitle: App.localize("GenerateAllPaymentProjectionPageTitle"), pageTitleSub: App.localize("GenerateAllPaymentProjectionPageTitleSub"), stateDesc: App.localize("GenerateAllPaymentProjectionStateDesc"), iconClass: 'fa-home' },
                menu: 'GenerateAllPaymentProjections'
            });
        $urlRouterProvider.otherwise('/generateAllPaymentProjections');
    }
    if (abp.auth.hasPermission('Pages.Districts')) {
        $stateProvider
            .state('districts', {
                url: '/districts',
                templateUrl: settings.appPathNotHTTP + '/views/districts/index.cshtml',
                data: { pageTitle: App.localize("DistrictsPageTitle"), pageTitleSub: App.localize("DistrictsPageTitleSub"), stateDesc: App.localize("DistrictsStateDesc"), iconClass: 'fa-home' },
                menu: 'Districts' //Matches to name of 'Districts' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/districts');
    }

    if (abp.auth.hasPermission('Pages.MasterTenants')) {
        $stateProvider
            .state('masterTenants', {
                url: '/masterTenants',
                templateUrl: settings.appPathNotHTTP + '/views/masterTenants/index.cshtml',
                data: { pageTitle: App.localize("MasterTenantsPageTitle"), pageTitleSub: App.localize("MasterTenantsPageTitleSub"), stateDesc: App.localize("MasterTenantsStateDesc"), iconClass: 'fa-home' },
                menu: 'MasterTenants' //Matches to name of 'MasterTenants' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/masterTenants');
    }

    if (abp.auth.hasPermission('Pages.BankAccounts')) {
        $stateProvider
            .state('bankAccounts', {
                url: '/bankAccounts',
                templateUrl: settings.appPathNotHTTP + '/views/bankAccounts/index.cshtml',
                data: { pageTitle: App.localize("BankAccountsPageTitle"), pageTitleSub: App.localize("BankAccountsPageTitleSub"), stateDesc: App.localize("BankAccountsStateDesc"), iconClass: 'fa-home' },
                menu: 'BankAccounts' //Matches to name of 'BankAccounts' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/bankAccounts');
    }

    if (abp.auth.hasPermission('Pages.PaymentDates')) {
        $stateProvider
            .state('paymentDates', {
                url: '/paymentDates',
                templateUrl: settings.appPathNotHTTP + '/views/paymentDates/index.cshtml',
                data: { pageTitle: App.localize("PaymentDatesPageTitle"), pageTitleSub: App.localize("PaymentDatesPageTitleSub"), stateDesc: App.localize("PaymentDatesStateDesc"), iconClass: 'fa-home' },
                menu: 'PaymentDates' //Matches to name of 'PaymentDates' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/paymentDates');
    }

    if (abp.auth.hasPermission('Pages.CourseValues')) {
        $stateProvider
            .state('courseValues', {
                url: '/courseValues',
                templateUrl: settings.appPathNotHTTP + '/views/courseValues/index.cshtml',
                data: { pageTitle: App.localize("CourseValuesPageTitle"), pageTitleSub: App.localize("CourseValuesPageTitleSub"), stateDesc: App.localize("CourseValuesStateDesc"), iconClass: 'fa-home' },
                menu: 'CourseValues' //Matches to name of 'CourseValues' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/courseValues');
    }

    if (abp.auth.hasPermission('Pages.Periods')) {
        $stateProvider
            .state('periods', {
                url: '/periods',
                templateUrl: settings.appPathNotHTTP + '/views/periods/index.cshtml',
                data: { pageTitle: App.localize("PeriodsPageTitle"), pageTitleSub: App.localize("PeriodsPageTitleSub"), stateDesc: App.localize("PeriodsStateDesc"), iconClass: 'fa-home' },
                menu: 'Periods' //Matches to name of 'Periods' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/periods');
    }

    if (abp.auth.hasPermission('Pages.Concepts')) {
        $stateProvider
            .state('concepts', {
                url: '/concepts',
                templateUrl: settings.appPathNotHTTP + '/views/concepts/index.cshtml',
                data: { pageTitle: App.localize("ConceptsPageTitle"), pageTitleSub: App.localize("ConceptsPageTitleSub"), stateDesc: App.localize("ConceptsStateDesc"), iconClass: 'fa-home' },
                menu: 'Concepts' //Matches to name of 'Concepts' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/concepts');
    }

    if (abp.auth.hasPermission('Pages.PaymentMethods')) {
        $stateProvider
            .state('paymentMethods', {
                url: '/paymentMethods',
                templateUrl: settings.appPathNotHTTP + '/views/paymentMethods/index.cshtml',
                data: { pageTitle: App.localize("PaymentMethodsPageTitle"), pageTitleSub: App.localize("PaymentMethodsPageTitleSub"), stateDesc: App.localize("PaymentMethodsStateDesc"), iconClass: 'fa-home' },
                menu: 'PaymentMethods' //Matches to name of 'PaymentMethods' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/paymentMethods');
    }

    if (abp.auth.hasPermission('Pages.Receipts')) {
        $stateProvider
            .state('receipts', {
                url: '/receipts?origin=25',
                templateUrl: settings.appPathNotHTTP + '/views/receipts/index.cshtml',
                data: { pageTitle: App.localize("ReceiptsPageTitle"), pageTitleSub: App.localize("ReceiptsPageTitleSub"), stateDesc: App.localize("ReceiptsStateDesc"), iconClass: 'fa-home' },
                menu: 'Receipts' //Matches to name of 'receipts' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/receipts');
    }

    if (abp.auth.hasPermission('Pages.Transactions')) {
        $stateProvider
            .state('transactions', {
                url: '/transactions?origin=25',
                templateUrl: settings.appPathNotHTTP + '/views/transactions/index.cshtml',
                data: { pageTitle: App.localize("TransactionsPageTitle"), pageTitleSub: App.localize("TransactionsPageTitleSub"), stateDesc: App.localize("TransactionsStateDesc"), iconClass: 'fa-home' },
                menu: 'Transactions' //Matches to name of 'transactions' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/transactions');
    }


    if (abp.auth.hasPermission('Pages.Deposits')) {
        $stateProvider
            .state('deposits', {
                url: '/deposits?origin=25',
                templateUrl: settings.appPathNotHTTP + '/views/deposits/index.cshtml',
                data: { pageTitle: App.localize("DepositsPageTitle"), pageTitleSub: App.localize("DepositsPageTitleSub"), stateDesc: App.localize("DepositsStateDesc"), iconClass: 'fa-home' },
                menu: 'Deposits' //Matches to name of 'deposits' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/deposits');
    }

    if (abp.auth.hasPermission('Pages.Catalogs')) {
        $stateProvider
            .state('catalogs', {
                url: '/catalogs',
                templateUrl: settings.appPathNotHTTP + '/views/catalogs/index.cshtml',
                data: { pageTitle: App.localize("CatalogsPageTitle"), pageTitleSub: App.localize("CatalogsPageTitleSub"), stateDesc: App.localize("CatalogsStateDesc"), iconClass: 'fa-home' },
                menu: 'Catalogs' //Matches to name of 'Catalogs' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/catalogs');
    }

    if (abp.auth.hasPermission('Pages.ReportsMain')) {
        $stateProvider
            .state('reportsmain', {
                url: '/reportsmain',
                templateUrl: settings.appPathNotHTTP + '/views/reportsmain/index.cshtml',
                data: { pageTitle: App.localize("ReportsPageTitle"), pageTitleSub: App.localize("ReportsPageTitleSub"), stateDesc: App.localize("ReportsStateDesc"), iconClass: 'fa-home' },
                menu: 'ReportsMain' //Matches to name of 'Reports' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/reportsmain');
    }

    if (abp.auth.hasPermission('Pages.Relationships')) {
        $stateProvider
            .state('relationships', {
                url: '/relationships',
                templateUrl: settings.appPathNotHTTP + '/views/relationships/index.cshtml',
                data: { pageTitle: App.localize("RelationshipsPageTitle"), pageTitleSub: App.localize("RelationshipsPageTitleSub"), stateDesc: App.localize("RelationshipsStateDesc"), iconClass: 'fa-home' },
                menu: 'Relationships' //Matches to name of 'Relationships' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/relationships');
    }

    if (abp.auth.hasPermission('Pages.Cities')) {
        $stateProvider
            .state('cities', {
                url: '/cities',
                templateUrl: settings.appPathNotHTTP + '/views/cities/index.cshtml',
                data: { pageTitle: App.localize("CitiesPageTitle"), pageTitleSub: App.localize("CitiesPageTitleSub"), stateDesc: App.localize("CitiesStateDesc"), iconClass: 'fa-home' },
                menu: 'Cities' //Matches to name of 'Cities' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/cities');
    }

    if (abp.auth.hasPermission('Pages.Enrollments')) {
        $stateProvider
            .state('enrollments', {
                url: '/enrollments?origin=25',
                templateUrl: settings.appPathNotHTTP + '/views/enrollments/index.cshtml',
                data: { pageTitle: App.localize("EnrollmentsPageTitle"), pageTitleSub: App.localize("EnrollmentsPageTitleSub"), stateDesc: App.localize("EnrollmentsStateDesc"), iconClass: 'fa-home' },
                menu: 'Enrollments' //Matches to name of 'Enrollments' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/enrollments');
    }


    if (abp.auth.hasPermission('Pages.PreviousEnrollments')) {
        $stateProvider
            .state('previousEnrollments', {
                url: '/previousEnrollments?origin=25',
                templateUrl: settings.appPathNotHTTP + '/views/previousEnrollments/index.cshtml',
                data: { pageTitle: App.localize("PreviousEnrollmentsPageTitle"), pageTitleSub: App.localize("PreviousEnrollmentsPageTitleSub"), stateDesc: App.localize("PreviousEnrollmentsStateDesc"), iconClass: 'fa-home' },
                menu: 'PreviousEnrollments' //Matches to name of 'Enrollments' menu in EasySchoolManagerNavigationProvider

            });
        $urlRouterProvider.otherwise('/previousEnrollments');
    }


    if (abp.auth.hasPermission('Pages.NextEnrollments')) {
        $stateProvider
            .state('nextEnrollments', {
                url: '/nextEnrollments?origin=25',
                templateUrl: settings.appPathNotHTTP + '/views/nextEnrollments/index.cshtml',
                data: { pageTitle: App.localize("NextEnrollmentsPageTitle"), pageTitleSub: App.localize("NextEnrollmentsPageTitleSub"), stateDesc: App.localize("NextEnrollmentsStateDesc"), iconClass: 'fa-home' },
                menu: 'NextEnrollments' //Matches to name of 'Enrollments' menu in EasySchoolManagerNavigationProvider

            });
        $urlRouterProvider.otherwise('/nextEnrollments');
    }

    if (abp.auth.hasPermission('Pages.Bloods')) {
        $stateProvider
            .state('bloods', {
                url: '/bloods',
                templateUrl: settings.appPathNotHTTP + '/views/bloods/index.cshtml',
                data: { pageTitle: App.localize("BloodsPageTitle"), pageTitleSub: App.localize("BloodsPageTitleSub"), stateDesc: App.localize("BloodsStateDesc"), iconClass: 'fa-home' },
                menu: 'Bloods' //Matches to name of 'Bloods' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/bloods');
    }

    if (abp.auth.hasPermission('Pages.Provinces')) {
        $stateProvider
            .state('provinces', {
                url: '/provinces',
                templateUrl: settings.appPathNotHTTP + '/views/provinces/index.cshtml',
                data: { pageTitle: App.localize("ProvincesPageTitle"), pageTitleSub: App.localize("ProvincesPageTitleSub"), stateDesc: App.localize("ProvincesStateDesc"), iconClass: 'fa-home' },
                menu: 'Provinces' //Matches to name of 'Provinces' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/provinces');
    }

    if (abp.auth.hasPermission('Pages.CourseSessions')) {
        $stateProvider
            .state('courseSessions', {
                url: '/courseSessions',
                templateUrl: settings.appPathNotHTTP + '/views/courseSessions/index.cshtml',
                data: { pageTitle: App.localize("CourseSessionsPageTitle"), pageTitleSub: App.localize("CourseSessionsPageTitleSub"), stateDesc: App.localize("CourseSessionsStateDesc"), iconClass: 'fa-home' },
                menu: 'CourseSessions' //Matches to name of 'CourseSessions' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/courseSessions');
    }

    if (abp.auth.hasPermission('Pages.Religions')) {
        $stateProvider
            .state('religions', {
                url: '/religions',
                templateUrl: settings.appPathNotHTTP + '/views/religions/index.cshtml',
                data: { pageTitle: App.localize("ReligionsPageTitle"), pageTitleSub: App.localize("ReligionsPageTitleSub"), stateDesc: App.localize("ReligionsStateDesc"), iconClass: 'fa-home' },
                menu: 'Religions' //Matches to name of 'Religions' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/religions');
    }

    if (abp.auth.hasPermission('Pages.Regions')) {
        $stateProvider
            .state('regions', {
                url: '/regions',
                templateUrl: settings.appPathNotHTTP + '/views/regions/index.cshtml',
                data: { pageTitle: App.localize("RegionsPageTitle"), pageTitleSub: App.localize("RegionsPageTitleSub"), stateDesc: App.localize("RegionsStateDesc"), iconClass: 'fa-home' },
                menu: 'Regions' //Matches to name of 'Regions' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/regions');
    }

    if (abp.auth.hasPermission('Pages.PrinterTypes')) {
        $stateProvider
            .state('printerTypes', {
                url: '/printerTypes',
                templateUrl: settings.appPathNotHTTP + '/views/printerTypes/index.cshtml',
                data: { pageTitle: App.localize("PrinterTypesPageTitle"), pageTitleSub: App.localize("PrinterTypesPageTitleSub"), stateDesc: App.localize("PrinterTypesStateDesc"), iconClass: 'fa-home' },
                menu: 'PrinterTypes' //Matches to name of 'PrinterTypes' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/printerTypes');
    }

    if (abp.auth.hasPermission('Pages.Origins')) {
        $stateProvider
            .state('origins', {
                url: '/origins',
                templateUrl: settings.appPathNotHTTP + '/views/origins/index.cshtml',
                data: { pageTitle: App.localize("OriginsPageTitle"), pageTitleSub: App.localize("OriginsPageTitleSub"), stateDesc: App.localize("OriginsStateDesc"), iconClass: 'fa-home' },
                menu: 'Origins' //Matches to name of 'Origins' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/origins');
    }

    if (abp.auth.hasPermission('Pages.Illnesses')) {
        $stateProvider
            .state('illnesses', {
                url: '/illnesses',
                templateUrl: settings.appPathNotHTTP + '/views/illnesses/index.cshtml',
                data: { pageTitle: App.localize("IllnessesPageTitle"), pageTitleSub: App.localize("IllnessesPageTitleSub"), stateDesc: App.localize("IllnessesStateDesc"), iconClass: 'fa-home' },
                menu: 'Illnesses' //Matches to name of 'Illnesses' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/illnesses');
    }

    if (abp.auth.hasPermission('Pages.Genders')) {
        $stateProvider
            .state('genders', {
                url: '/genders',
                templateUrl: settings.appPathNotHTTP + '/views/genders/index.cshtml',
                data: { pageTitle: App.localize("GendersPageTitle"), pageTitleSub: App.localize("GendersPageTitleSub"), stateDesc: App.localize("GendersStateDesc"), iconClass: 'fa-home' },
                menu: 'Genders' //Matches to name of 'Genders' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/genders');
    }

    if (abp.auth.hasPermission('Pages.DocumentTypes')) {
        $stateProvider
            .state('documentTypes', {
                url: '/documentTypes',
                templateUrl: settings.appPathNotHTTP + '/views/documentTypes/index.cshtml',
                data: { pageTitle: App.localize("DocumentTypesPageTitle"), pageTitleSub: App.localize("DocumentTypesPageTitleSub"), stateDesc: App.localize("DocumentTypesStateDesc"), iconClass: 'fa-home' },
                menu: 'DocumentTypes' //Matches to name of 'DocumentTypes' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/documentTypes');
    }

    if (abp.auth.hasPermission('Pages.CommentTypes')) {
        $stateProvider
            .state('commentTypes', {
                url: '/commentTypes',
                templateUrl: settings.appPathNotHTTP + '/views/commentTypes/index.cshtml',
                data: { pageTitle: App.localize("CommentTypesPageTitle"), pageTitleSub: App.localize("CommentTypesPageTitleSub"), stateDesc: App.localize("CommentTypesStateDesc"), iconClass: 'fa-home' },
                menu: 'CommentTypes' //Matches to name of 'CommentTypes' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/commentTypes');
    }

    if (abp.auth.hasPermission('Pages.ChangeTypes')) {
        $stateProvider
            .state('changeTypes', {
                url: '/changeTypes',
                templateUrl: settings.appPathNotHTTP + '/views/changeTypes/index.cshtml',
                data: { pageTitle: App.localize("ChangeTypesPageTitle"), pageTitleSub: App.localize("ChangeTypesPageTitleSub"), stateDesc: App.localize("ChangeTypesStateDesc"), iconClass: 'fa-home' },
                menu: 'ChangeTypes' //Matches to name of 'ChangeTypes' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/changeTypes');
    }



    if (abp.auth.hasPermission('Pages.Banks')) {
        $stateProvider
            .state('banks', {
                url: '/banks',
                templateUrl: settings.appPathNotHTTP + '/views/banks/index.cshtml',
                data: { pageTitle: App.localize("BanksPageTitle"), pageTitleSub: App.localize("BanksPageTitleSub"), stateDesc: App.localize("BanksStateDesc"), iconClass: 'fa-home' },
                menu: 'Banks' //Matches to name of 'Banks' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/banks');
    }

    if (abp.auth.hasPermission('Pages.Allergies')) {
        $stateProvider
            .state('allergies', {
                url: '/allergies',
                templateUrl: settings.appPathNotHTTP + '/views/allergies/index.cshtml',
                data: { pageTitle: App.localize("AllergiesPageTitle"), pageTitleSub: App.localize("AllergiesPageTitleSub"), stateDesc: App.localize("AllergiesStateDesc"), iconClass: 'fa-home' },
                menu: 'Allergies' //Matches to name of 'Allergies' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/allergies');
    }

    if (abp.auth.hasPermission('Pages.Occupations')) {
        $stateProvider
            .state('occupations', {
                url: '/occupations',
                templateUrl: settings.appPathNotHTTP + '/views/occupations/index.cshtml',
                data: { pageTitle: App.localize("OccupationsPageTitle"), pageTitleSub: App.localize("OccupationsPageTitleSub"), stateDesc: App.localize("OccupationsStateDesc"), iconClass: 'fa-home' },
                menu: 'Occupations' //Matches to name of 'Occupations' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/occupations');
    }

    if (abp.auth.hasPermission('Pages.Levels')) {
        $stateProvider
            .state('levels', {
                url: '/levels',
                templateUrl: settings.appPathNotHTTP + '/views/levels/index.cshtml',
                data: { pageTitle: App.localize("LevelsPageTitle"), pageTitleSub: App.localize("LevelsPageTitleSub"), stateDesc: App.localize("LevelsStateDesc"), iconClass: 'fa-home' },
                menu: 'Levels' //Matches to name of 'Levels' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/levels');
    }

    if (abp.auth.hasPermission('Pages.SessionStudentSelections')) {
        $stateProvider
            .state('sessionStudentSelections', {
                url: '/sessionStudentSelections',
                templateUrl: settings.appPathNotHTTP + '/views/sessionStudentSelections/index.cshtml',
                data: { pageTitle: App.localize("SessionStudentSelectionsPageTitle"), pageTitleSub: App.localize("SessionStudentSelectionsPageTitleSub"), stateDesc: App.localize("SessionStudentSelectionsStateDesc"), iconClass: 'fa-home' },
                menu: 'SessionStudentSelections' //Matches to name of 'sessionStudentSelections' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/levels');
    }


    if (abp.auth.hasPermission('Pages.SessionStudentSelectionPrevious')) {
        $stateProvider
            .state('sessionStudentSelectionPrevious', {
                url: '/sessionStudentSelectionPrevious',
                templateUrl: settings.appPathNotHTTP + '/views/sessionStudentSelectionPrevious/index.cshtml',
                data: { pageTitle: App.localize("SessionStudentSelectionPreviousPageTitle"), pageTitleSub: App.localize("SessionStudentSelectionPreviousPageTitleSub"), stateDesc: App.localize("SessionStudentSelectionPreviousStateDesc"), iconClass: 'fa-home' },
                menu: 'SessionStudentSelectionPrevious' //Matches to name of 'sessionStudentSelections' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/levels');
    }

    //-----------------------------------------------/     

    if (abp.auth.hasPermission('Pages.Courses')) {
        $stateProvider
            .state('courses', {
                url: '/courses',
                templateUrl: settings.appPathNotHTTP + '/views/courses/index.cshtml',
                data: { pageTitle: App.localize("CoursesPageTitle"), pageTitleSub: App.localize("CoursesPageTitleSub"), stateDesc: App.localize("CoursesStateDesc") },
                menu: 'Courses' //Matches to name of 'Courses' menu in EasySchoolManagerNavigationProvider
            });
        $urlRouterProvider.otherwise('/');
    }
    $urlRouterProvider.otherwise('/');

}]);
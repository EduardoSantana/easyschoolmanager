drop procedure GeneratePaymentProyectionsForOneStudent
go

create procedure GeneratePaymentProyectionsForOneStudent @TenantId int,  @EnrollmentStudentId int 
as
--Declaracion de Variables
declare @periodId int = (select PeriodId from EnrollmentStudents where Id = @EnrollmentStudentId)
declare @sequence int, @paymentDay int, @paymentMonth int, @paymentYear int
declare @IncriptionAmount decimal, @AmountMontly decimal, @TotalYearAmount decimal, @TotalPayments int 
declare @SpecificDate datetime

--Ponemos a que realice el proceso solo si la tabla de payment proyection no tiene valores para el estudiante indicado.
if((select count(*) from PaymentProjections where EnrollmentStudentId = @EnrollmentStudentId and IsDeleted = 0 and IsActive = 1 ) = 0)
begin

	--Buscamos la cantidad de pagos que hay en las fechas de pagos preprogramadas
	set @TotalPayments = ( select count(*) from PaymentDates 
		where  TenantId = @TenantId and IsDeleted = 0 and IsActive = 1 and Sequence > 0)
 
	 --Buscamos el costo de inscripcion y el total  a pagar para un curso determinado
	 select @IncriptionAmount = cv.InscriptionAmount, @TotalYearAmount = cv.TotalAmount from CourseEnrollmentStudents ces inner join CourseValues cv on cv.CourseId = ces.CourseId
	 where ces.EnrollmentStudentId = @EnrollmentStudentId

	 if(@TotalPayments = 0)
	 begin
	 print('No hay fechas de pagos registrados en el periodo indicado para el tenant ' + cast(@tenantId as varchar(3)))
	 return;
	 end


	 --Calculamos la cantidad mensual que deben pagar en cada periodo de pago que le corresponda ( mensual por ejemplo)
	set @AmountMontly = Round((@TotalYearAmount / @TotalPayments),2)


	--Buscamos todas las fechas en que seran programados pagos
	 DECLARE payment_dates CURSOR FOR 
		select Sequence, PaymentDay, PaymentMonth, PaymentYear from PaymentDates 
		where  TenantId = @TenantId and IsDeleted = 0 and IsActive = 1
		order by Sequence


	 open payment_dates

	 fetch next from payment_dates
	 into @sequence, @paymentDay , @paymentMonth , @paymentYear 
	 WHILE @@FETCH_STATUS = 0  
		BEGIN  
		 print @sequence

		 --Deteminamos la fecha de cada pago
		 if(@paymentMonth = 2 and @paymentDay > 28) 
			set @SpecificDate = CAST( cast(@paymentYear as varchar(4)) + '/' + CAST(@paymentMonth as varchar(2)) + '/28' as DateTime) 
		 else if(@paymentMonth IN (4,6,9,11))
			set @SpecificDate = CAST( cast(@paymentYear as varchar(4)) + '/' + CAST(@paymentMonth as varchar(2)) + '/30' as DateTime) 
		 else
			set @SpecificDate = CAST( cast(@paymentYear as varchar(4)) + '/' + CAST(@paymentMonth as varchar(2)) + '/'  + cast(@paymentDay as varchar(2)) as DateTime) 
		 
		 print(@SpecificDate)
		 print(@IncriptionAmount)
		 print(@AmountMontly)
		 print(@TotalYearAmount)

		 --Insertamos las mensualidaddes 
		 if(@sequence > 0)
			 begin
			  insert into PaymentProjections ( EnrollmentStudentId, Amount, PaymentDate, TenantId, IsDeleted, IsActive, CreatorUserId, Sequence, Applied, CreationTime) values
			  (@EnrollmentStudentId,@AmountMontly, @SpecificDate, @TenantId, 0, 1, 1, @sequence, 0 , GETDATE())
			 end

		 --Insertamos el monto de inscripcion
		 if(@sequence = 0)
			 begin
			  insert into PaymentProjections ( EnrollmentStudentId, Amount, PaymentDate, TenantId, IsDeleted, IsActive, CreatorUserId, Sequence, Applied, CreationTime) values
			  (@EnrollmentStudentId,@IncriptionAmount, @SpecificDate, @TenantId, 0, 1, 1, @sequence, 0 , GETDATE())
			 end
		  fetch next from payment_dates
		  into @sequence, @paymentDay , @paymentMonth , @paymentYear 
		end

	 deallocate payment_dates
end
go

drop procedure SearchAllStudentForPendingPaymentProjectionAndCreateProjections
go


create procedure SearchAllStudentForPendingPaymentProjectionAndCreateProjections @PeriodId int
as

declare @enrollmentStudentId int, @tenantId int

declare Students_Cursor Cursor for 
	select Id, TenantId from EnrollmentStudents where IsActive = 1 and IsDeleted = 0 and PeriodId = @PeriodId
	and Id not in (select EnrollmentStudentId from PaymentProjections where PeriodId = @PeriodId)

open Students_Cursor 

fetch next from Students_Cursor
into @enrollmentStudentId, @tenantId
 WHILE @@FETCH_STATUS = 0  
	BEGIN  
		exec GeneratePaymentProyectionsForOneStudent @tenantId, @enrollmentStudentId
		
		fetch next from Students_Cursor
		into @enrollmentStudentId, @tenantId
	end

deallocate Students_Cursor

go


/*
 esta parte que se esta comentado aqui abajo es para realizar pruebas con el procedimiento.

 begin tran tt
 select count(*) from PaymentProjections, EnrollmentStudents  where EnrollmentStudents.id = PaymentProjections.EnrollmentStudentId and EnrollmentStudents.PeriodId = 3

 exec SearchAllStudentForPendingPaymentProjectionAndCreateProjections 3
 select count(*) from PaymentProjections, EnrollmentStudents  where EnrollmentStudents.id = PaymentProjections.EnrollmentStudentId and EnrollmentStudents.PeriodId = 3

-- select * from PaymentProjections, EnrollmentStudents  where EnrollmentStudents.id = PaymentProjections.EnrollmentStudentId and EnrollmentStudents.PeriodId = 3
 --delete PaymentProjections from PaymentProjections, EnrollmentStudents  where EnrollmentStudents.id = PaymentProjections.EnrollmentStudentId and EnrollmentStudents.PeriodId = 3

--delete PaymentProjections from PaymentProjections, EnrollmentStudents  where EnrollmentStudents.id = PaymentProjections.EnrollmentStudentId and EnrollmentStudents.PeriodId = 3 and PaymentProjections.EnrollmentStudentId = 13615
select * from PaymentProjections, EnrollmentStudents  where EnrollmentStudents.id = PaymentProjections.EnrollmentStudentId and EnrollmentStudents.PeriodId = 3 and PaymentProjections.EnrollmentStudentId = 13615
 
 rollback tran tt 

 */
 /*


 select * from PaymentProjections
 select * from CourseValues
 select * from PaymentDates where PeriodId = 3

 select cv.InscriptionAmount, cv.TotalAmount from CourseEnrollmentStudents ces inner join CourseValues cv on cv.CourseId = ces.CourseId
 where ces.EnrollmentStudentId = Enroll


 select * from Periods
 */


 --select * from EnrollmentStudents where PeriodId = 3 and IsDeleted = 0 and IsActive = 1 and TenantId = 3
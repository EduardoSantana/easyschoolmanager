USE [EasySchoolManager_]
GO

/****** Object:  View [dbo].[rep_view_EvaluationFinalReport]    Script Date: 07/24/2019 18:50:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


			-- =============================================
			-- Author:		<Eduardo Santana>
			-- Create date: <2018 - 10 - 07>
			-- Description:	<Se busca el listado de evaluaciones finales para un estudiante seleccionado>
			-- =============================================
			ALTER  VIEW [dbo].[rep_view_EvaluationFinalReport]
			AS 
			SELECT
				isnull(t.Name,'') as EducationalCenterName, --NombreDelCentro,
				isnull(t.AccreditationNumber,'') as AccreditationNumber, --CodigoCentroEducativo,
				isnull(t.[Address],'') as EducationalDistrictAddress, --DireccionCentroEducativo,
				isnull(t.EducationalDistrict,'') as EducationalDistrict, --DistritoEducativo,
				isnull(t.Regional,'') as DirectionOfRegionalEducation, --DireccionRegionalEducacion,
				StartYear = substring(CAST( year(p.startDate) as varchar(4)),3,2), 
				EndYear = SUBSTRING( CAST( year(p.Enddate) as varchar(4)),3,2),
				'x' as Aproved, --PromovidoA,
				'' as PendingsSubjects, --AsignaturasPendientes,
				'' as NotAproved, --Reprobado,
				'' as Observations, --Observaciones,
				p.[Description] as SchoolYear, --AnoEscolar
				'' as AvgAA,

					cast(round((isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0),0)+
						isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0),0)+
						isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0),0)+
						isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0),0))/4.0,0) as integer)  Average,
						
					(isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0),0)+
						isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0),0)+
						isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0),0)+
						isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0),0))/4.0  AverageTmp,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) > 0 then 

					cast(round((dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0))/8.0,0) as integer) else -1 end as PCP50,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) > 0 then
					cast(	round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0),2) as integer)  else  -1 end as CPC,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) > 0 then
					cast(	round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) / 2.0,0) as integer)  else -1 end as CPC50,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) > 0 then 

					cast(round((dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0))/8.0,0) + 
						
						round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) / 2.0,0) as integer)
						 else -1 end as CC,

						 ---------------------------------------------

					case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) > 0 then 

					cast(round(((dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0))/4.0)*0.3,0) as integer)  else -1 end as PCP30,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) > 0 then
					cast(	round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0),0) as integer)  else  -1 end as CPEX,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) > 0 then
					cast(	round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) * 0.70,0) as integer)  else -1 end as CPEX70,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) > 0 then 

					cast(
					
					round(((dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0))/4.0)*0.30,0) + 
						
					round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) * 0.70 ,0)  as integer)
						 
						  else -1 end as CEX,

				'' AS FinalA,
				'' AS FinalR,
				'' as CAP1,
				'' as CAP2,
				es.Id as EnrollmentStudentId,
				es.StudentId,
				es.PeriodId,
				s.FirstName,
				s.LastName,
				ces.Sequence, --NumeroOrden,
				sh.Id as SubjectHighId,
				ces.CourseId,
				ces.SessionId,
				es.TenantId,
				c.Name as CourseName,
				c.Abbreviation,
				cs.Name as SessionName,
				sh.Name as SubjectHighsName
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0) as Evaluation1
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0) as Evaluation2
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0) as Evaluation3
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0) as Evaluation4




				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) as Evaluation6


				,dbo.[rep_fn_getOneEvaluationHighByPositionName](1) as Evaluation1Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](2) as Evaluation2Name	
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](3) as Evaluation3Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](4) as Evaluation4Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](5) as Evaluation5Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](6) as Evaluation6Name
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,0,1) as Average2
				,s.FirstName + ' ' + s.LastName as StudentFullName
				,(CASE c.Id
					WHEN 11 THEN 'Reports/Images/Finals/R67_IMAGEN11GRADO1.jpg' --	Primero de Secundaria
					WHEN 12 THEN 'Reports/Images/Finals/R67_IMAGEN12GRADO1.jpg' --	Segundo de Secundaria
					WHEN 13 THEN 'Reports/Images/Finals/R67_IMAGEN13GRADO1.jpg' --	Tercero de Secundaria
					WHEN 14 THEN 'Reports/Images/Finals/R67_IMAGEN14GRADO1.jpg' --	Cuarto de Secundaria
					WHEN 15 THEN 'Reports/Images/Finals/R67_IMAGEN15GRADO1.jpg' --	Quinto de Secundaria
					WHEN 18 THEN 'Reports/Images/Finals/R67_IMAGEN18GRADO1.jpg' --	Sexto de Secundaria
					WHEN 19 THEN 'Reports/Images/Finals/R67_IMAGEN19GRADO1.jpg' --	Cuarto de Secundario T�cnico
					WHEN 20 THEN 'Reports/Images/Finals/R67_IMAGEN20GRADO1.jpg' --	Quinto de Secundario T�cnico
					WHEN 21 THEN 'Reports/Images/Finals/R67_IMAGEN21GRADO1.jpg' --	Sexto de Secundario T�cnico
				END) AS Imagen1
				,'Reports/Images/Finals/R67_IMAGEN11GRADO2.jpg' as Imagen2
				,(CASE c.Id
					WHEN 11 THEN 'Reports/Images/Finals/ACTA_FINAL_CALIFICACIONES_1G.jpg' --	Primero de Secundaria
					WHEN 12 THEN 'Reports/Images/Finals/ACTA_FINAL_CALIFICACIONES_2G.jpg' --	Segundo de Secundaria
					WHEN 13 THEN 'Reports/Images/Finals/ACTA_FINAL_CALIFICACIONES_3G.jpg' --	Tercero de Secundaria
					WHEN 14 THEN 'Reports/Images/Finals/ACTA_FINAL_CALIFICACIONES_4G.jpg' --	Cuarto de Secundaria
					WHEN 15 THEN 'Reports/Images/Finals/ACTA_FINAL_CALIFICACIONES_5G.jpg' --	Quinto de Secundaria
					WHEN 18 THEN '' --	Sexto de Secundaria
					WHEN 19 THEN '' --	Cuarto de Secundario T�cnico
					WHEN 20 THEN '' --	Quinto de Secundario T�cnico
					WHEN 21 THEN '' --	Sexto de Secundario T�cnico
				    ELSE ''
				END) AS Imagen3
				,'' as Imagen4
				,'' as Imagen5
			FROM 
				EnrollmentStudents es 
			INNER JOIN 
				CourseEnrollmentStudents ces ON es.Id = ces.EnrollmentStudentId and es.TenantId = ces.TenantId
			INNER JOIN 
				Students s ON es.StudentId = s.id
			INNER JOIN 
				Courses c ON ces.CourseId = c.id
			INNER JOIN 
				CourseSessions cs ON ces.SessionId = cs.id
			INNER JOIN 
				SubjectHighs sh ON ces.CourseId = sh.CourseId
			INNER JOIN 
				Periods  p ON p.Id = es.PeriodId 
			INNER JOIN 
				AbpTenants t ON t.Id = es.TenantId

			WHERE es.IsActive = 1 AND es.IsDeleted = 0
			
GO

drop function getSubjetHighIdFromPosition
go

create function getSubjetHighIdFromPosition(@CourseId int, @Position int)
returns int
begin
 return isnull(( select SubjectHighId from CourseSubjectPositions where courseId = @CourseId and Position = @Position),0)
end
go
drop function getAvg 
go
create function getAvg(@enrollmentStudentId int, @Position int, @CourseId int, @SessionId int)
returns integer
begin 
declare @SubjectHighId integer 
select @SubjectHighId = dbo.getSubjetHighIdFromPosition(@CourseId, @Position)
return isnull((select top 1 Average from rep_view_EvaluationFinalReport where EnrollmentStudentId = @enrollmentStudentId
	and SubjectHighId = @SubjectHighId
	and CourseId = @CourseId
	and SessionId = @SessionId),-1)
end
go
drop function getCompl
go
create function getCompl(@enrollmentStudentId int, @Position int, @CourseId int, @SessionId int)
returns integer
begin 
declare @SubjectHighId integer 
select @SubjectHighId = dbo.getSubjetHighIdFromPosition(@CourseId, @Position)
return isnull((select top 1 CC from rep_view_EvaluationFinalReport where EnrollmentStudentId = @enrollmentStudentId
	and SubjectHighId = @SubjectHighId
	and CourseId = @CourseId
	and SessionId = @SessionId),-1)
end
go
go
drop function getExt
go
create function getExt(@enrollmentStudentId int, @Position int, @CourseId int, @SessionId int)
returns integer
begin 
declare @SubjectHighId integer 
select @SubjectHighId = dbo.getSubjetHighIdFromPosition(@CourseId, @Position)
return isnull((select top 1 CEX from rep_view_EvaluationFinalReport where EnrollmentStudentId = @enrollmentStudentId
	and SubjectHighId = @SubjectHighId
	and CourseId = @CourseId
	and SessionId = @SessionId),-1)
end
go
go
drop function getSubjectHightName
go
create function getSubjectHightName(@CourseId int, @Position int)
returns varchar(500)
begin 
return isnull((select top 1 sh.name from CourseSubjectPositions csp, SubjectHighs sh where sh.id = csp.SubjectHighId and csp.Position = @Position and csp.CourseId = @CourseId ),'')
end
go


--select dbo.getSubjetHighIdFromPosition(15, 1), dbo.getAvg(149,1,15,31), dbo.getCompl(149,1,15,31), dbo.getSubjectHightName(11,1); 

--select * from rep_view_EvaluationFinalReport where firstName like '%enger%' or firstName like '%nic%'and courseid = 15

go
drop view Rep_View_ActaFinalCalification
go

Create view Rep_View_ActaFinalCalification as

select distinct  EnrollmentStudentId, FirstName, LastName, CourseId, CourseName, sessionId, TenantId,Sequence, PendingsSubjects, Aproved, NotAproved,
    StartYear, EndYear,

	dbo.getSubjectHightName( CourseId,1) SubjectHightName1,
	dbo.getAvg(EnrollmentStudentId, 1, CourseId, SessionId) Avg1,
	dbo.getCompl(EnrollmentStudentId, 1, CourseId, SessionId) Comp1,
	dbo.getExt(EnrollmentStudentId, 1, CourseId, SessionId) Ext1, 

	dbo.getSubjectHightName( CourseId,2) SubjectHightName2,
	dbo.getAvg(EnrollmentStudentId, 2, CourseId, SessionId) Avg2,
	dbo.getCompl(EnrollmentStudentId, 2, CourseId, SessionId) Comp2,
	dbo.getExt(EnrollmentStudentId, 2, CourseId, SessionId) Ext2, 

	dbo.getSubjectHightName( CourseId,3) SubjectHightName3,
	dbo.getAvg(EnrollmentStudentId, 3, CourseId, SessionId) Avg3,
	dbo.getCompl(EnrollmentStudentId, 3, CourseId, SessionId) Comp3,
	dbo.getExt(EnrollmentStudentId, 3, CourseId, SessionId) Ext3, 

	dbo.getSubjectHightName( CourseId,4) SubjectHightName4,
	dbo.getAvg(EnrollmentStudentId, 4, CourseId, SessionId) Avg4,
	dbo.getCompl(EnrollmentStudentId, 4, CourseId, SessionId) Comp4,
	dbo.getExt(EnrollmentStudentId, 4, CourseId, SessionId) Ext4, 

	dbo.getSubjectHightName( CourseId,5) SubjectHightName5,
	dbo.getAvg(EnrollmentStudentId, 5, CourseId, SessionId) Avg5,
	dbo.getCompl(EnrollmentStudentId, 5, CourseId, SessionId) Comp5,
	dbo.getExt(EnrollmentStudentId, 5, CourseId, SessionId) Ext5, 

	dbo.getSubjectHightName( CourseId,6) SubjectHightName6,
	dbo.getAvg(EnrollmentStudentId, 6, CourseId, SessionId) Avg6,
	dbo.getCompl(EnrollmentStudentId, 6, CourseId, SessionId) Comp6,
	dbo.getExt(EnrollmentStudentId, 6, CourseId, SessionId) Ext6, 

	dbo.getSubjectHightName( CourseId,7) SubjectHightName7,
	dbo.getAvg(EnrollmentStudentId, 7, CourseId, SessionId) Avg7,
	dbo.getCompl(EnrollmentStudentId, 7, CourseId, SessionId) Comp7,
	dbo.getExt(EnrollmentStudentId, 7, CourseId, SessionId) Ext7, 

	dbo.getSubjectHightName( CourseId,8) SubjectHightName8,
	dbo.getAvg(EnrollmentStudentId, 8, CourseId, SessionId) Avg8,
	dbo.getCompl(EnrollmentStudentId, 8, CourseId, SessionId) Comp8,
	dbo.getExt(EnrollmentStudentId, 8, CourseId, SessionId) Ext8, 

	dbo.getSubjectHightName( CourseId,9) SubjectHightName9,
	dbo.getAvg(EnrollmentStudentId, 9, CourseId, SessionId) Avg9,
	dbo.getCompl(EnrollmentStudentId, 9, CourseId, SessionId) Comp9,
	dbo.getExt(EnrollmentStudentId, 9, CourseId, SessionId) Ext9, 

	dbo.getSubjectHightName( CourseId,10) SubjectHightName10,
	dbo.getAvg(EnrollmentStudentId, 10, CourseId, SessionId) Avg10,
	dbo.getCompl(EnrollmentStudentId, 10, CourseId, SessionId) Comp10,
	dbo.getExt(EnrollmentStudentId, 10, CourseId, SessionId) Ext10, 

	dbo.getSubjectHightName( CourseId,11) SubjectHightName11,
	dbo.getAvg(EnrollmentStudentId, 11, CourseId, SessionId) Avg11,
	dbo.getCompl(EnrollmentStudentId, 11, CourseId, SessionId) Comp11,
	dbo.getExt(EnrollmentStudentId, 11, CourseId, SessionId) Ext11,

	 
	Abbreviation, SessionName,  StudentId, 
	PeriodId, StudentFullName, Imagen1, Imagen2, 
	Imagen3, Imagen4, Imagen5, EducationalCenterName, 
	EducationalDistrictAddress, EducationalDistrict, DirectionOfRegionalEducation,
	schoolYear, Observations ,  AccreditationNumber
  
from rep_view_EvaluationFinalReport  
 
 go
 
 select * from Rep_View_ActaFinalCalification where
 courseid = 12 AND tenantid = 1 AND SESSIONID = 12
 order by sequence

/*


select * from levels

select * from courses where levelid = 3

select * from [SubjectHighs] where courseId = 11
select * from [SubjectHighs] where courseId = 12
select * from [SubjectHighs] where courseId = 13
select * from [SubjectHighs] where courseId = 14
select * from [SubjectHighs] where courseId = 15
select * from CourseSubjectPositions


*/



				DECLARE @ReportCode varchar(3) = '111';
				if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
				begin
					INSERT INTO [dbo].[Reports]
									([Tabla]
									,[ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
								VALUES
									(null -- <Tabla, nvarchar(100),>
									,  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  @ReportCode -- <ReportCode, nvarchar(80),>
									,  'Acta de Calificacion Final' -- <ReportName, nvarchar(100),>
									,  'Rep_View_ActaFinalCalification' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Finals\ActaCalificacionFinalCuerpo.rdlc' -- <ReportPath, nvarchar(500),>
									,  1 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									);

				  INSERT INTO [dbo].[ReportsFilters]
						([ReportsId]
						,[IsDeleted]
						,[IsActive]
						,[CreatorUserId]
						,[CreationTime]
						,[LastModifierUserId]
						,[LastModificationTime]
						,[DeleterUserId]
						,[DeletionTime]
						,[ReportId]
						,[Order]
						,[Name]
						,[DisplayName]
						,[DataField]
						,[DataTypeId]
						,[Range]
						,[OnlyParameter]
						,[UrlService]
						,[FieldService]
						,[DisplayNameService]
						,[DependencyField]
						,[Operator]
						,[DisplayNameRange]
						,[Required])
					VALUES
						((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
						,  0  -- <IsDeleted, bit,>
						,  1  -- <IsActive, bit,>
						,  1  -- <CreatorUserId, bigint,>
						,  GETDATE() -- <CreationTime, datetime,>
						,  null -- <LastModifierUserId, bigint,>
						,  null -- <LastModificationTime, datetime,>
						,  null -- <DeleterUserId, bigint,>
						,  null -- <DeletionTime, datetime,>
						,  0 -- <ReportId, int,>
						,  1 -- <Order, int,>
						,  'CourseId' -- <Name, nvarchar(max),>
						,  'Curso' -- <DisplayName, nvarchar(max),>
						,  'CourseId' -- <DataField, nvarchar(max),>
						,  1 -- <DataTypeId, int,>
						,  0 -- <Range, bit,>
						,  0 -- <OnlyParameter, bit,>
						,  null -- <UrlService, nvarchar(max),>
						,  null   -- <FieldService, nvarchar(max),>
						,  null -- <DisplayNameService, nvarchar(max),>
						,  null -- <DependencyField, nvarchar(max),>
						,  null -- <Operator, nvarchar(max),>
						,  null -- <DisplayNameRange, nvarchar(max),>
						,  1 -- <Required, bit,>
						);

				  INSERT INTO [dbo].[ReportsFilters]
						([ReportsId]
						,[IsDeleted]
						,[IsActive]
						,[CreatorUserId]
						,[CreationTime]
						,[LastModifierUserId]
						,[LastModificationTime]
						,[DeleterUserId]
						,[DeletionTime]
						,[ReportId]
						,[Order]
						,[Name]
						,[DisplayName]
						,[DataField]
						,[DataTypeId]
						,[Range]
						,[OnlyParameter]
						,[UrlService]
						,[FieldService]
						,[DisplayNameService]
						,[DependencyField]
						,[Operator]
						,[DisplayNameRange]
						,[Required])
					VALUES
						((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
						,  0  -- <IsDeleted, bit,>
						,  1  -- <IsActive, bit,>
						,  1  -- <CreatorUserId, bigint,>
						,  GETDATE() -- <CreationTime, datetime,>
						,  null -- <LastModifierUserId, bigint,>
						,  null -- <LastModificationTime, datetime,>
						,  null -- <DeleterUserId, bigint,>
						,  null -- <DeletionTime, datetime,>
						,  0 -- <ReportId, int,>
						,  1 -- <Order, int,>
						,  'SessionId' -- <Name, nvarchar(max),>
						,  'Session' -- <DisplayName, nvarchar(max),>
						,  'SessionId' -- <DataField, nvarchar(max),>
						,  1 -- <DataTypeId, int,>
						,  0 -- <Range, bit,>
						,  0 -- <OnlyParameter, bit,>
						,  null -- <UrlService, nvarchar(max),>
						,  null   -- <FieldService, nvarchar(max),>
						,  null -- <DisplayNameService, nvarchar(max),>
						,  null -- <DependencyField, nvarchar(max),>
						,  null -- <Operator, nvarchar(max),>
						,  null -- <DisplayNameRange, nvarchar(max),>
						,  1 -- <Required, bit,>
						);

				end
				
				set @ReportCode = '112';
				if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
				begin
					INSERT INTO [dbo].[Reports]
									([Tabla]
									,[ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
								VALUES
									(null -- <Tabla, nvarchar(100),>
									,  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  @ReportCode -- <ReportCode, nvarchar(80),>
									,  'Acta de Calificacion Final ( con optativas)' -- <ReportName, nvarchar(100),>
									,  'Rep_View_ActaFinalCalification' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Finals\ActaCalificacionFinalOptativas.rdlc' -- <ReportPath, nvarchar(500),>
									,  1 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									);

				  INSERT INTO [dbo].[ReportsFilters]
						([ReportsId]
						,[IsDeleted]
						,[IsActive]
						,[CreatorUserId]
						,[CreationTime]
						,[LastModifierUserId]
						,[LastModificationTime]
						,[DeleterUserId]
						,[DeletionTime]
						,[ReportId]
						,[Order]
						,[Name]
						,[DisplayName]
						,[DataField]
						,[DataTypeId]
						,[Range]
						,[OnlyParameter]
						,[UrlService]
						,[FieldService]
						,[DisplayNameService]
						,[DependencyField]
						,[Operator]
						,[DisplayNameRange]
						,[Required])
					VALUES
						((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
						,  0  -- <IsDeleted, bit,>
						,  1  -- <IsActive, bit,>
						,  1  -- <CreatorUserId, bigint,>
						,  GETDATE() -- <CreationTime, datetime,>
						,  null -- <LastModifierUserId, bigint,>
						,  null -- <LastModificationTime, datetime,>
						,  null -- <DeleterUserId, bigint,>
						,  null -- <DeletionTime, datetime,>
						,  0 -- <ReportId, int,>
						,  1 -- <Order, int,>
						,  'CourseId' -- <Name, nvarchar(max),>
						,  'Curso' -- <DisplayName, nvarchar(max),>
						,  'CourseId' -- <DataField, nvarchar(max),>
						,  1 -- <DataTypeId, int,>
						,  0 -- <Range, bit,>
						,  0 -- <OnlyParameter, bit,>
						,  null -- <UrlService, nvarchar(max),>
						,  null   -- <FieldService, nvarchar(max),>
						,  null -- <DisplayNameService, nvarchar(max),>
						,  null -- <DependencyField, nvarchar(max),>
						,  null -- <Operator, nvarchar(max),>
						,  null -- <DisplayNameRange, nvarchar(max),>
						,  1 -- <Required, bit,>
						);

				  INSERT INTO [dbo].[ReportsFilters]
						([ReportsId]
						,[IsDeleted]
						,[IsActive]
						,[CreatorUserId]
						,[CreationTime]
						,[LastModifierUserId]
						,[LastModificationTime]
						,[DeleterUserId]
						,[DeletionTime]
						,[ReportId]
						,[Order]
						,[Name]
						,[DisplayName]
						,[DataField]
						,[DataTypeId]
						,[Range]
						,[OnlyParameter]
						,[UrlService]
						,[FieldService]
						,[DisplayNameService]
						,[DependencyField]
						,[Operator]
						,[DisplayNameRange]
						,[Required])
					VALUES
						((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
						,  0  -- <IsDeleted, bit,>
						,  1  -- <IsActive, bit,>
						,  1  -- <CreatorUserId, bigint,>
						,  GETDATE() -- <CreationTime, datetime,>
						,  null -- <LastModifierUserId, bigint,>
						,  null -- <LastModificationTime, datetime,>
						,  null -- <DeleterUserId, bigint,>
						,  null -- <DeletionTime, datetime,>
						,  0 -- <ReportId, int,>
						,  1 -- <Order, int,>
						,  'SessionId' -- <Name, nvarchar(max),>
						,  'Session' -- <DisplayName, nvarchar(max),>
						,  'SessionId' -- <DataField, nvarchar(max),>
						,  1 -- <DataTypeId, int,>
						,  0 -- <Range, bit,>
						,  0 -- <OnlyParameter, bit,>
						,  null -- <UrlService, nvarchar(max),>
						,  null   -- <FieldService, nvarchar(max),>
						,  null -- <DisplayNameService, nvarchar(max),>
						,  null -- <DependencyField, nvarchar(max),>
						,  null -- <Operator, nvarchar(max),>
						,  null -- <DisplayNameRange, nvarchar(max),>
						,  1 -- <Required, bit,>
						);

				end
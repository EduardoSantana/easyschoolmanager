﻿using EasySchoolManager.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Tests.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var connString = ConfigurationManager.ConnectionStrings["Default"].ToString();
            var context = new EasySchoolManagerDbContext(connString);
            var c = new Migrations.Configuration();
            c.RunSeed(context);
        }
    }
}

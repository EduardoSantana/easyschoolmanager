﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using EasySchoolManager.EntityFramework;

namespace EasySchoolManager
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(EasySchoolManagerCoreModule))]
    public class EasySchoolManagerDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<EasySchoolManagerDbContext>());

            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}

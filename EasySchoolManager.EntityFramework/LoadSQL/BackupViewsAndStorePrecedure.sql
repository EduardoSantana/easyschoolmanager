USE [GEDUCON_UNACA]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Campos]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE FUNCTION [dbo].[fn_Campos](@tabla varchar(200))  RETURNS varchar(5000)  AS  BEGIN  	DECLARE @val VARCHAR(5000) ='', @select VARCHAR(10) = 'select '  	SELECT  @val = '[' + sc.NAME +'],' + @val   	FROM sys.objects so INNER JOIN sys.columns sc   	ON so.[object_id]=sc.[object_id]   	WHERE so.name=@tabla   	ORDER BY sc.name    	IF LEN( @val) > 0  		set  @val = SUBSTRING(@val,0,LEN( @val))   	ELSE  		set @val = '*'  	RETURN @select +  @val + ' from ' +@tabla   END 
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetTieneConsultas]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE FUNCTION [dbo].[fn_GetTieneConsultas]( @filtroID INT )  RETURNS BIT  AS  BEGIN  	DECLARE @retValue BIT = 0  	SELECT TOP 1 @retValue = 1   	FROM dbo.ReportsQueries  	WHERE ReportsFiltersId = @filtroID  	RETURN @retValue  END 
GO
/****** Object:  UserDefinedFunction [dbo].[rep_fn_getOneEvaluationHigh]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				-- =============================================
				-- Author:		<Eduardo Santana>
				-- Create date: <2018-08-13>
				-- Description:	<Buscar la calificacion de un alunno, para un curso, para unsa session>
				-- =============================================
				CREATE FUNCTION [dbo].[rep_fn_getOneEvaluationHigh]
				(
					-- Add the parameters for the function here
					@EnrollmentStudentId INT,
					@SubjectHighId INT,
					@TenantId INT,
					@Orden INT,
					@IsTotal BIT
				)
				RETURNS INT
				AS
				BEGIN
					-- Declare the return variable here
					DECLARE @ResultVar INT = 0;
					DECLARE @RowsTotal INT = 0;
					IF (@IsTotal = 1)
					BEGIN
						-- Add the T-SQL statements to compute the return value here
						SELECT @ResultVar = SUM([Expression]), @RowsTotal = COUNT(1)
						FROM [dbo].[EvaluationHighs]
						WHERE [SubjectHighId] = @SubjectHighId
							AND [EnrollmentStudentId] = @EnrollmentStudentId
							AND [TenantId] = @TenantId;

						IF (@ResultVar > 0)
						BEGIN 
							SET @ResultVar = @ResultVar / @RowsTotal;
						END 
					END 
					ELSE 
					BEGIN
						-- Add the T-SQL statements to compute the return value here
						SELECT @ResultVar = [Expression]
						FROM [dbo].[EvaluationHighs]
						WHERE [SubjectHighId] = @SubjectHighId
							AND [EnrollmentStudentId] = @EnrollmentStudentId
							AND [TenantId] = @TenantId
							AND EvaluationOrderHighId = @Orden
						;

					END

					IF (@ResultVar = 0)
					BEGIN 
						SET @ResultVar = NULL;
					END 
	
					-- Return the result of the function
					RETURN @ResultVar;

				END
			
GO
/****** Object:  UserDefinedFunction [dbo].[rep_fn_getOneEvaluationHighByPosition]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				-- =============================================
				-- Author:		<Eduardo Santana>
				-- Create date: <2018-10-07>
				-- Description:	<Buscar la calificacion de un alunno, para una posicion>
				-- =============================================
				CREATE FUNCTION [dbo].[rep_fn_getOneEvaluationHighByPosition]
				(
					-- Add the parameters for the function here
					@EnrollmentStudentId INT,
					@SubjectHighId INT,
					@TenantId INT,
					@PositionId INT,
					@IsTotal BIT
				)
				RETURNS INT
				AS
				BEGIN
					-- Declare the return variable here
					DECLARE @ResultVar INT = 0;
					DECLARE @RowsTotal INT = 0;
					DECLARE @TempVal INT = 0;

					IF (@IsTotal = 1)
					BEGIN
						
						SET @TempVal = ISNULL(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,@SubjectHighId,@TenantId,1,0),0);
						IF(@TempVal > 0)
						BEGIN 
							SET @ResultVar += @TempVal;
							SET @RowsTotal += 1;
						END 
						
						SET @TempVal = ISNULL(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,@SubjectHighId,@TenantId,2,0),0);
						IF(@TempVal > 0)
						BEGIN 
							SET @ResultVar += @TempVal;
							SET @RowsTotal += 1;
						END 

						SET @TempVal = ISNULL(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,@SubjectHighId,@TenantId,3,0),0);
						IF(@TempVal > 0)
						BEGIN 
							SET @ResultVar += @TempVal;
							SET @RowsTotal += 1;
						END 

						SET @TempVal = ISNULL(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,@SubjectHighId,@TenantId,4,0),0);
						IF(@TempVal > 0)
						BEGIN 
							SET @ResultVar += @TempVal;
							SET @RowsTotal += 1;
						END 

						IF (@ResultVar > 0)
						BEGIN 
							SET @ResultVar = @ResultVar / @RowsTotal;
						END 

					END 
					ELSE 
					BEGIN
						-- Add the T-SQL statements to compute the return value here
						SELECT @ResultVar = AVG(ISNULL([Expression],0)) 
						FROM [dbo].[EvaluationHighs] eh
						inner join EvaluationOrderHighs eoh on eh.EvaluationOrderHighId = eoh.Id
						WHERE eh.[SubjectHighId] = @SubjectHighId
							AND eh.[EnrollmentStudentId] = @EnrollmentStudentId
							AND eh.[TenantId] = @TenantId
							AND  eoh.EvaluationPositionHighId = @PositionId
						;
						
					END
					
					IF(@ResultVar = 0)
					BEGIN
						SET @ResultVar = NULL;
					END

					-- Return the result of the function
					RETURN @ResultVar;

				END
			
			
GO
/****** Object:  UserDefinedFunction [dbo].[rep_fn_getOneEvaluationHighByPositionName]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				-- =============================================
				-- Author:		<Eduardo Santana>
				-- Create date: <2018-10-07>
				-- Description:	<Buscar el nombre deL position>
				-- =============================================
				CREATE FUNCTION [dbo].[rep_fn_getOneEvaluationHighByPositionName]
				(
					-- Add the parameters for the function here
					@PositionId int 
				)
				RETURNS VARCHAR(256)
				AS
				BEGIN
					-- Declare the return variable here
					DECLARE @ResultVar VARCHAR(256) ='';

					-- Add the T-SQL statements to compute the return value here
					SELECT TOP 1 @ResultVar = eph.Description
					FROM EvaluationPositionHighs eph
					WHERE eph.Id = @PositionId
					
					-- Return the result of the function
					RETURN @ResultVar;

				END
			
			
GO
/****** Object:  UserDefinedFunction [dbo].[rep_fn_getOneEvaluationHighName]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				-- =============================================
				-- Author:		<Eduardo Santana>
				-- Create date: <2018-08-13>
				-- Description:	<Buscar el nombre deL mes>
				-- =============================================
				CREATE FUNCTION [dbo].[rep_fn_getOneEvaluationHighName]
				(
					-- Add the parameters for the function here
					@EnrollmentStudentId INT,
					@SubjectHighId INT,
					@TenantId INT,
					@Orden INT
				)
				RETURNS VARCHAR(256)
				AS
				BEGIN
					-- Declare the return variable here
					DECLARE @ResultVar VARCHAR(256) ='';

					-- Add the T-SQL statements to compute the return value here
					SELECT TOP 1 @ResultVar = eoh.Description
					FROM EvaluationOrderHighs eoh
					WHERE eoh.Id = @Orden
					
					-- Return the result of the function
					RETURN @ResultVar;

				END
			
			
GO
/****** Object:  UserDefinedFunction [dbo].[rep_fn_getRolesName]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE FUNCTION [dbo].[rep_fn_getRolesName] ( @USERID INT )  RETURNS VARCHAR(4000)  AS  BEGIN 	 DECLARE @RESULTVAR VARCHAR(4000) = ''; 	 SELECT @RESULTVAR = @RESULTVAR + R.NAME  + ', ' 	 FROM ABPUSERROLES UXR 	 INNER JOIN ABPROLES R ON UXR.ROLEID = R.ID 	 WHERE USERID = @USERID AND R.ISDELETED = 0 	 IF(@RESULTVAR = '') BEGIN RETURN '' END 	 RETURN SUBSTRING(@RESULTVAR, 0, LEN(@RESULTVAR) - 1)  END 
GO
/****** Object:  Table [dbo].[Courses]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Courses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Abbreviation] [nvarchar](5) NOT NULL,
	[LevelId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
	[Sequence] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Courses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[rep_view_EnrollmentInscriptionStudent]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				CREATE view [dbo].[rep_view_EnrollmentInscriptionStudent] as
				Select
				a.CreationTime,
				e.Description as Period,
				a.TenantId,
				a.StudentId,
				a.Id as EnrollmentStudentId,
				b.LastName as StudentLastName,
				b.FirstName as StudentFirstName,
				b.DateOfBirth,
				f.Name as StudentGender,
				c.Sequence as EnrollmentSequencesId,
				d.Id as Cedula,
				d.FirstName,
				d.LastName,
				g.Name as Gender,
				d.Address,
				d.Phone1,
				d.Phone2,
				d.EmailAddress,
				i.Abbreviation as CourseAbbreviation,
				j.Amount as PagoMatricula
				from EnrollmentStudents a
				inner join Students b on b.Id = a.StudentId
				inner join EnrollmentSequences c on c.EnrollmentId = a.EnrollmentId and c.TenantId = a.TenantId
				inner join Enrollments d on d.Id = a.EnrollmentId
				inner join Periods e on e.Id = a.PeriodId
				inner join Genders f on f.Id = b.GenderId
				inner join Genders g on g.Id = d.GenderId
				inner join CourseEnrollmentStudents h on h.EnrollmentStudentId = a.Id
				inner join Courses i on i.Id = h.CourseId
				inner join TransactionStudents j on j.EnrollmentStudentId = a.Id
				inner join AbpTenants k on k.Id = a.TenantId
				inner join Transactions l on l.Id = j.TransactionId
				where l.ConceptId in (select ConceptId from ConceptTenantRegistrations where TenantId = a.TenantId and IsDeleted = 0 and IsActive = 1)
			
GO
/****** Object:  View [dbo].[vw_evaluations]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				
				
			create view [dbo].[vw_evaluations] as

			select ig.Name IndicatorGroup,

			i.Name Indicator, i.Id IndicatorId, cs.CourseId, s.FirstName, s.LastName, e.EvaluationPeriodId, ep.Position , el.Name Legend ,
			ces.EnrollmentStudentId
			from CourseEnrollmentStudents ces  
			inner join Evaluations e on e.EnrollmentStudentId = ces.EnrollmentStudentId
			inner join CourseSessions cs on cs.id = ces.SessionId
			inner join Courses c on c.id = cs.CourseId
			inner join EvaluationPeriods ep on ep.Id = e.EvaluationPeriodId
			inner join EvaluationLegends el  on el.Id = e.EvaluationLegendId
			inner join Indicators i on i.id = e.IndicatorId
			inner join IndicatorGroups ig on ig.Id = i.IndicatorGroupId
			inner join EnrollmentStudents es on es.Id = ces.EnrollmentStudentId
			inner join Students s on s.Id = es.StudentId 
			
			
GO
/****** Object:  View [dbo].[vw_evaluations_report]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				
				
			create view [dbo].[vw_evaluations_report] as
			select distinct  s.CourseId, c.Name Course, c.Abbreviation CourseAbbreviation, cs.Name Session, cs.Id SessionId, ig.SubjectId, s.Name Subject,  i.IndicatorGroupId, 
			StudentFirstName =  stu.FirstName, StudentLastName = stu.LastName, StudentFullName = stu.FirstName + ' ' + stu.LastName, EnrollmentFullName = enr.FirstName + ' ' + enr.LastName, EnrollmentFirstName = enr.FirstName,
			EnrollmentLastName = enr.LastName,
 
			ig.Name IndicatorGroup,
			i.id IndicatorId, 
			i.sequence IndicatorSequence,   
			i.name Idicator ,  
			ces.EnrollmentStudentId, ces.TenantId,
			isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 1 and  e.enrollmentStudentId = es.Id  ),'') E1 ,
			isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 2 and  e.enrollmentStudentId = es.Id  ),'')  E2,
			isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 3 and  e.enrollmentStudentId = es.Id ),'') E3 ,
			isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 4 and  e.enrollmentStudentId = es.Id  ),'') E4 ,
			isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 5 and  e.enrollmentStudentId = es.Id  ),'')  RF
			from Courses c, Indicators i, IndicatorGroups ig, 
			Subjects s, CourseSessions cs, Evaluations e, CourseEnrollmentStudents ces,
			EnrollmentStudents es, Students stu, Enrollments enr
			where	ig.Id = i.IndicatorGroupId  and 
			c.Id = s.CourseId and cs.CourseId = c.Id and 
			e.IndicatorId = i.Id and 
			ces.SessionId = cs.Id and 
			ig.SubjectId = s.Id and
			es.Id = ces.EnrollmentStudentId and
			stu.Id = es.StudentId and 
			enr.Id = es.EnrollmentId and ces.TenantId = e.TenantId and ces.TenantId = es.TenantId 
			and isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 1 and  e.enrollmentStudentId = es.Id  ),'') != ''		
		
			
			
GO
/****** Object:  View [dbo].[rep_view_EvaluationHighs]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					-- =============================================
					-- Author:		<Eduardo Santana>
					-- Create date: <2018 - 08 - 11>
					-- Description:	<Se busca el listado de evaluaciones de secundaria>
					-- =============================================
					CREATE VIEW [dbo].[rep_view_EvaluationHighs]
					AS 
					SELECT
						eh.Id AS EvaluationHighsId,
						eh.EnrollmentStudentId,
						es.StudentId,
						es.PeriodId,
						s.FirstName,
						s.LastName,
						eh.Expression,
						ces.Sequence,
						eh.SubjectHighId,
						eh.EvaluationOrderHighId,
						ces.CourseId,
						ces.SessionId,
						es.TenantId,
						c.Name as CourseName,
						c.Abbreviation,
						cs.Name as SessionName,
						sh.Name as SubjectHighsName,
						eoh.[Description] as EvaluationOrderHighsName
					FROM
						EvaluationHighs eh
					INNER JOIN 
						EnrollmentStudents es ON eh.EnrollmentStudentId = es.Id
					INNER JOIN 
						CourseEnrollmentStudents ces ON eh.EnrollmentStudentId = ces.EnrollmentStudentId
					INNER JOIN 
						Students s ON es.StudentId = s.id
					INNER JOIN 
						Courses c ON ces.CourseId = c.id
					INNER JOIN 
						CourseSessions cs ON ces.SessionId = cs.id
					INNER JOIN 
						SubjectHighs sh ON eh.SubjectHighId = sh.id
					INNER JOIN 
						EvaluationOrderHighs eoh ON eh.EvaluationOrderHighId = eoh.id
			
GO
/****** Object:  View [dbo].[rep_vw_StudentWithNotProyect]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 09>
			-- Description:   <Se busca el listado de estsudiantes WithNotProyect>
			-- =============================================
			CREATE VIEW [dbo].[rep_vw_StudentWithNotProyect]
			as
			Select 
			'' as StudentWithNotProyect,
				stu.LastName,
				stu.FirstName,
				enrxsec.Sequence,
				enr.FirstName + ' ' + enr.LastName as FullName,
				cou.Name CourseName,
				enrxstu.TenantId,
				stu.Id as StudentId,
				cou.Id as CourseId,
				enr.Id as EnrollmentId,
				enrxstu.PeriodId ,
				pr.Period,
				tn.Name Tenant,
				cou.LevelId,
				lv.Name AS LevelName
			From Students stu
			inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
			inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId
			inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id
			inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
			inner join Courses cou on cou.Id = couxenrxstu.CourseId
			INNER JOIN dbo.Periods pr ON pr.Id = enrxstu.PeriodId
			LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId
			INNER JOIN dbo.Levels lv ON lv.Id = cou.LevelId
			LEFT JOIN dbo.PaymentProjections pp2
			ON pp2.enrollmentStudentId = enrxstu.Id
			and pp2.TenantId = enrxstu.TenantId
			WHERE stu.IsDeleted = 0 AND
			stu.IsActive = 1 and
			enrxsec.IsDeleted = 0 and
			enrxstu.IsDeleted = 0 and
			tn.PeriodId = enrxstu.PeriodId
			and couxenrxstu.CourseId != 22
			AND pp2.Id IS NULL
			;
			
GO
/****** Object:  View [dbo].[rep_view_EvaluationFinalReport]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

			-- =============================================
			-- Author:		<Eduardo Santana>
			-- Create date: <2018 - 10 - 07>
			-- Description:	<Se busca el listado de evaluaciones finales para un estudiante seleccionado>
			-- =============================================
			CREATE  VIEW [dbo].[rep_view_EvaluationFinalReport]
			AS 
			SELECT
				t.Name as EducationalCenterName, --NombreDelCentro,
				t.AccreditationNumber as AccreditationNumber, --CodigoCentroEducativo,
				t.[Address] as EducationalDistrictAddress, --DireccionCentroEducativo,
				t.EducationalDistrict as EducationalDistrict, --DistritoEducativo,
				t.Regional as DirectionOfRegionalEducation, --DireccionRegionalEducacion,
				'' as Aproved, --PromovidoA,
				'' as PendingsSubjects, --AsignaturasPendientes,
				'' as NotAproved, --Reprobado,
				'' as Observations, --Observaciones,
				p.[Description] as SchoolYear, --AnoEscolar
				'' as AvgAA,
				'' as PCP50,
				'' AS CPC,
				'' AS CPC50,
				'' AS CC,
				'' AS PCP30,
				'' AS CPEX,
				'' AS CPEX70,
				'' AS CEX,
				'' AS FinalA,
				'' AS FinalR,
				'' as CAP1,
				'' as CAP2,
				es.Id as EnrollmentStudentId,
				es.StudentId,
				es.PeriodId,
				s.FirstName,
				s.LastName,
				ces.Sequence, --NumeroOrden,
				sh.Id as SubjectHighId,
				ces.CourseId,
				ces.SessionId,
				es.TenantId,
				c.Name as CourseName,
				c.Abbreviation,
				cs.Name as SessionName,
				sh.Name as SubjectHighsName
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0) as Evaluation1
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0) as Evaluation2
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0) as Evaluation3
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0) as Evaluation4
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](1) as Evaluation1Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](2) as Evaluation2Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](3) as Evaluation3Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](4) as Evaluation4Name
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,0,1) as Average
				,s.FirstName + ' ' + s.LastName as StudentFullName
				,(CASE c.Id
					WHEN 11 THEN 'Reports/Images/Finals/R67_IMAGEN11GRADO1.jpg' --	Primero de Secundaria
					WHEN 12 THEN 'Reports/Images/Finals/R67_IMAGEN12GRADO1.jpg' --	Segundo de Secundaria
					WHEN 13 THEN 'Reports/Images/Finals/R67_IMAGEN13GRADO1.jpg' --	Tercero de Secundaria
					WHEN 14 THEN 'Reports/Images/Finals/R67_IMAGEN14GRADO1.jpg' --	Cuarto de Secundaria
					WHEN 15 THEN 'Reports/Images/Finals/R67_IMAGEN15GRADO1.jpg' --	Quinto de Secundaria
					WHEN 18 THEN 'Reports/Images/Finals/R67_IMAGEN18GRADO1.jpg' --	Sexto de Secundaria
					WHEN 19 THEN 'Reports/Images/Finals/R67_IMAGEN19GRADO1.jpg' --	Cuarto de Secundario Técnico
					WHEN 20 THEN 'Reports/Images/Finals/R67_IMAGEN20GRADO1.jpg' --	Quinto de Secundario Técnico
					WHEN 21 THEN 'Reports/Images/Finals/R67_IMAGEN21GRADO1.jpg' --	Sexto de Secundario Técnico
				END) AS Imagen1
				,'Reports/Images/Finals/R67_IMAGEN11GRADO2.jpg' as Imagen2
				,'' as Imagen3
				,'' as Imagen4
				,'' as Imagen5
			FROM 
				EnrollmentStudents es 
			INNER JOIN 
				CourseEnrollmentStudents ces ON es.Id = ces.EnrollmentStudentId and es.TenantId = ces.TenantId
			INNER JOIN 
				Students s ON es.StudentId = s.id
			INNER JOIN 
				Courses c ON ces.CourseId = c.id
			INNER JOIN 
				CourseSessions cs ON ces.SessionId = cs.id
			INNER JOIN 
				SubjectHighs sh ON ces.CourseId = sh.CourseId
			INNER JOIN 
				Periods  p ON p.Id = es.PeriodId 
			INNER JOIN 
				AbpTenants t ON t.Id = es.TenantId

			WHERE es.IsActive = 1 AND es.IsDeleted = 0

			
GO
/****** Object:  View [dbo].[rep_view_EvaluationHighsByStudent]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				-- =============================================
				-- Author:		<Eduardo Santana>
				-- Create date: <2018 - 08 - 13>
				-- Description:	<Se busca el listado de evaluaciones para un estudiante seleccionado>
				-- =============================================
				CREATE  VIEW [dbo].[rep_view_EvaluationHighsByStudent]
				AS 
				SELECT
					es.Id as EnrollmentStudentId,
					es.StudentId,
					es.PeriodId,
					s.FirstName,
					s.LastName,
					ces.Sequence,
					sh.Id as SubjectHighId,
					ces.CourseId,
					ces.SessionId,
					es.TenantId,
					c.Name as CourseName,
					c.Abbreviation,
					cs.Name as SessionName,
					sh.Name as SubjectHighsName
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,1,0) as Evaluation1
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,2,0) as Evaluation2
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,3,0) as Evaluation3
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,4,0) as Evaluation4
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,5,0) as Evaluation5
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,6,0) as Evaluation6
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,7,0) as Evaluation7
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,8,0) as Evaluation8
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,9,0) as Evaluation9
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,10,0) as Evaluation10
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,11,0) as Evaluation11
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,1) as Evaluation1Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,2) as Evaluation2Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,3) as Evaluation3Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,4) as Evaluation4Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,5) as Evaluation5Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,6) as Evaluation6Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,7) as Evaluation7Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,8) as Evaluation8Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,9) as Evaluation9Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,10) as Evaluation10Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,11) as Evaluation11Name
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,0,1) as Average
					,s.FirstName + ' ' + s.LastName as StudentFullName
				FROM 
					EnrollmentStudents es 
				INNER JOIN 
					CourseEnrollmentStudents ces ON es.Id = ces.EnrollmentStudentId AND ces.TenantId = es.TenantId
				INNER JOIN 
					Students s ON es.StudentId = s.id
				INNER JOIN 
					Courses c ON ces.CourseId = c.id
				INNER JOIN 
					CourseSessions cs ON ces.SessionId = cs.id
				INNER JOIN 
					SubjectHighs sh ON ces.CourseId = sh.CourseId
				WHERE es.IsDeleted = 0 AND es.IsActive = 1 

			
GO
/****** Object:  View [dbo].[rep_vw_StudentByCourse]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 09>
			-- Description:   <Se busca el listado de estsudiantes>
			-- =============================================
			CREATE VIEW [dbo].[rep_vw_StudentByCourse]
			as
			Select 
				'' as StudentByCourse,
				stu.LastName,
				stu.FirstName,
				enrxsec.Sequence,
				enr.FirstName + ' ' + enr.LastName as FullName,
				cou.Name CourseName,
				enrxstu.TenantId,
				stu.Id as StudentId,
				cou.Id as CourseId,
				enr.Id as EnrollmentId,
				enrxstu.PeriodId ,
				pr.Period,
				tn.Name Tenant,
				cou.LevelId,
				lv.Name AS LevelName
			From Students stu 
			inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
			inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId
			inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
			inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
			inner join Courses cou on cou.Id = couxenrxstu.CourseId
			INNER JOIN dbo.Periods pr ON pr.Id = enrxstu.PeriodId 
			LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId 
			INNER JOIN dbo.Levels lv ON lv.Id = cou.LevelId 
			WHERE stu.IsDeleted = 0 AND stu.IsActive = 1 and enrxsec.IsDeleted=0 and enrxstu.IsDeleted=0
			and tn.PeriodId = enrxstu.PeriodId
			;
			
GO
/****** Object:  View [dbo].[rep_view_EnrollmentPaymentStudent]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 23>
			-- Description:   <Se busca el listado de proyecciones de pago de estudiantes>
			-- =============================================
			CREATE VIEW [dbo].[rep_view_EnrollmentPaymentStudent]
			 AS
			Select
					a.CreationTime,
					a.PeriodId,
					e.Description as PeriodDescription,
					e.Period as PeriodPeriod,
					a.TenantId,
					a.StudentId,
					a.Id as EnrollmentStudentId,
					b.LastName as StudentLastName,
					b.FirstName as StudentFirstName,
					b.DateOfBirth,
					f.Name as StudentGender,
					c.Sequence as EnrollmentSequencesId,
					d.Id as Cedula,
					d.FirstName,
					d.LastName,
					g.Name as Gender,
					d.Address,
					d.Phone1,
					d.Phone2,
					d.EmailAddress,
					i.Abbreviation as CourseAbbreviation,
					a.EnrollmentId,
					payr.FirstQuota,
					payr.QuantityQuota,
					payr.MonthlyQuota,
					payr.YearlyQuota,
					DATEDIFF(YEAR,b.DateOfBirth,GETDATE()) as Age,
					j.Code as NivelCode,
					j.Name as NivelName,
					b.MotherPersonalId,
					b.MotherName,
					b.FatherPersonalId,
					b.FatherName
				from EnrollmentStudents a
					inner join Students b on b.Id = a.StudentId
					inner join EnrollmentSequences c on c.EnrollmentId = a.EnrollmentId and c.TenantId = a.TenantId
					inner join Enrollments d on d.Id = a.EnrollmentId
					inner join Periods e on e.Id = a.PeriodId
					inner join Genders f on f.Id = b.GenderId
					inner join Genders g on g.Id = d.GenderId
					inner join CourseEnrollmentStudents h on h.EnrollmentStudentId = a.Id
					inner join Courses i on i.Id = h.CourseId
					inner join AbpTenants k on k.Id = a.TenantId
					inner join Levels j on j.Id = i.LevelId
					inner join (
						SELECT 
							ee.StudentId, ee.PeriodId,
							sum (case t.Sequence when 0 then t.Amount else 0 end) as FirstQuota,
							max (t.Sequence) as QuantityQuota ,
							sum (case t.Sequence when 1 then t.Amount else 0 end) as MonthlyQuota,
							sum (t.Amount) as YearlyQuota
						FROM  dbo.PaymentProjections t 
							inner join EnrollmentStudents ee on ee.id = t.enrollmentStudentId
							INNER JOIN dbo.Enrollments er ON er.Id = ee.EnrollmentId
							INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId  
							inner join AbpTenants ten on ten.Id = t.TenantId
						WHERE t.IsDeleted = 0 AND ers.IsDeleted = 0 AND t.IsActive = 1 
					GROUP BY ee.StudentId, ee.PeriodId) payr 
					ON payr.StudentId = a.StudentId AND payr.PeriodId = a.PeriodId
			
GO
/****** Object:  View [dbo].[rep_view_ReporteEnrolmentsDet]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[rep_view_ReporteEnrolmentsDet]
as
Select 
	 enrxsec.Sequence as MatriculaId,
	 enr.Id as Cedula,
	 enr.FirstName,
	 enr.LastName,
	 enr.Address,
	 gen.Name as Gender,
	 enr.Phone1,
	 enr.Phone2,
	 enr.EmailAddress,
	 per.Period,
	 enrxstu.CreationTime,
	 stu.LastName as StudentLastName,
	 stu.FirstName as StudentFirstName,
	 stu.DateOfBirth,
	 genstu.Name as StudentGender,
	 cou.Abbreviation CourseAbbreviation,
	 0 PagoMatricula,
	 enrxsec.TenantId,
	 enr.Id as EnrollmentsId,
	 enrxsec.Id as EnrollmentSequencesId,
	 stu.Id as StudentId
From Enrollments enr
inner join EnrollmentSequences enrxsec on enr.Id = enrxsec.EnrollmentId
inner join EnrollmentStudents enrxstu on enr.Id = enrxstu.EnrollmentId
inner join AbpTenants aten on aten.Id = enrxsec.TenantId 
inner join Periods per on enrxstu.PeriodId = per.Id and aten.PeriodId = per.Id
inner join Students stu on enrxstu.StudentId = stu.Id
inner join CourseEnrollmentStudents ces on enrxstu.Id = ces.EnrollmentStudentId
inner join Courses cou on cou.Id = ces.CourseId
inner join Genders gen on gen.Id = enr.GenderId
inner join Genders genstu on genstu.Id = stu.GenderId
where stu.IsDeleted=0 and enr.IsDeleted=0
--where enrxsec.Sequence = @matricula and enrxsec.TenantId = 1

GO
/****** Object:  View [dbo].[rep_view_ListadoEstudianteXCursos]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				
			CREATE view [dbo].[rep_view_ListadoEstudianteXCursos]
			as
			Select '' ReporteDePrueba,
			ROW_NUMBER () over(order by stu.id asc) as Numero, 
			stu.LastName,
			stu.FirstName,
			enrxsec.Sequence,
			enr.FirstName + ' ' + enr.LastName as FullName,
			cou.Name,
			enrxstu.TenantId,
			stu.Id as StudentId,
			cou.Id as CourseId,
			enr.Id as EnrollmentId
			From Students stu 
			inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
			inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId
			inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
			inner join CourseEnrollmentStudents couxenrxstu 
				on enrxstu.Id = couxenrxstu.EnrollmentStudentId
				inner join Courses cou on cou.Id = couxenrxstu.CourseId
			
			
GO
/****** Object:  View [dbo].[rep_vw_Receipt]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				CREATE VIEW [dbo].[rep_vw_Receipt]
				AS
				SELECT
				tn.Name AS TenatnName,
				tn.Phone1,
				t.Date ,
				t.TransactionTypeId,
				t.ReceiptTypeId,
				NCF = isnull(t.TaxReceiptSequence,''),
				Number = CAST(t.Sequence AS VARCHAR),
				t.Name ,
				t.Amount ,
				isnull(cp.Description,(Select top 1 c2.Description + ' ...' from TransactionConcepts tc2 inner join Concepts c2 on c2.Id = tc2.ConceptId where tc2.TransactionId = t.Id )) AS Concept,
				t.Id AS TransactionId,
				t.TenantId,
				t.ConceptId,
				ers.Sequence as EnrollmentSequence,
				t.PaymentMethodId,
				pm.Name PaymentMethod,
				st.FirstName + ' ' + st.LastName AS StudentName,
				ts.EnrollmentStudentId ,
				c.Abbreviation CourseName,
				c.Id CourseId,
				sec.Name SeccionName,
				sec.Id SeccionId,
				ts.Amount AS StudentAmount,
				t.RncName, 
				t.RncNumber,
				t.TaxReceiptSequence,
				tarety.Name as TaxReceiptName,
				case when t.GrantEnterpriseId is null then '' else '<strong>Subvencionado Por:</strong> ' + gren.Name end as Subvencionador,
				case when t.Comments is null or t.Comments = '' then '' else '<strong>Comentario:</strong> ' + t.Comments end as Comments, 
				tare.ExpirationDate
				FROM dbo.Transactions t
				INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
				INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
				INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
				LEFT JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
				INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
				INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
				INNER JOIN dbo.TransactionStudents ts ON ts.TransactionId = t.Id
				INNER JOIN dbo.EnrollmentStudents erst ON erst.Id = ts.EnrollmentStudentId 
				INNER JOIN dbo.Students st ON st.Id = erst.StudentId 
				INNER JOIN dbo.CourseEnrollmentStudents crs ON crs.EnrollmentStudentId = ts.EnrollmentStudentId AND crs.TenantId = ts.TenantId
				INNER JOIN dbo.Courses c ON c.Id = crs.CourseId 
				LEFT JOIN dbo.CourseSessions sec ON sec.Id = crs.SessionId 
				LEFT JOIN TaxReceipts tare on tare.Id = t.TaxReceiptId
				LEFT JOIN TaxReceiptTypes tarety on tarety.Id = tare.TaxReceiptTypeId
				LEFT JOIN GrantEnterprises gren on gren.Id = t.GrantEnterpriseId
				WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1 and t.receiptTypeId = 1

			
			
GO
/****** Object:  View [dbo].[rep_view_AccountBalanceList]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
               create view  [dbo].[rep_view_AccountBalanceList] as
			    select es.Sequence, Tenant = at.Name, e.id, e.FirstName, e.LastName, EnrollmentName = e.FirstName + ' ' + e.LastName, e.Phone1, e.Phone2, t.TenantId
			    ,sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) Balance
			    from Enrollments e inner join EnrollmentSequences es on e.id = es.EnrollmentId
			    inner join Transactions t on t.EnrollmentId = e.Id and es.EnrollmentId = t.enrollmentId
			    and t.TenantId = es.TenantId 
				inner join AbpTenants at on at.Id = t.TenantId
			    where t.TransactionTypeId in (1,3) and isnull(t.ReceiptTypeId,0) not in (2,3) 
			    group by e.id, at.Name,e.FirstName, e.LastName, e.Phone1, e.Phone2, es.Sequence, t.TenantId
GO
/****** Object:  View [dbo].[rep_view_CanceledTrasactions]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 27>
			-- Description:   <Se busca el listado de recibos para cancelar, sin autorizacion>
			-- =============================================
			CREATE VIEW [dbo].[rep_view_CanceledTrasactions]
			AS
			Select 
				a.Sequence as ReceipNumber,
				a.TenantId,
				b.Name as TenantName,
				a.EnrollmentId,
				a.Date as TransactionDate,
				a.TransactionTypeId,
				a.ReceiptTypeId,
				a.Amount
			from Transactions a
			inner join AbpTenants b on b.Id = a.TenantId
			where a.CancelStatusId = 1
			
GO
/****** Object:  View [dbo].[rep_view_Deposits]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

			CREATE view [dbo].[rep_view_Deposits] as 
			Select b.Name as Bank,c.Number as BankAccount,a.Amount,a.[Date],
			a.Sequence ,d.[Description] as Concept ,a.Number as NumberDep,
			a.TenantId,a.BankAccountId,a.ConceptId,a.BankId,e.Name as TenantName from Transactions a
			inner join Banks b on b.Id = a.BankId
			inner join BankAccounts c on c.Id=a.BankAccountId
			inner join Concepts d on d.Id = a.ConceptId
			inner join AbpTenants e on e.Id = a.TenantId
			where a.TransactionTypeId=2 and a.StatusId=1 
			
GO
/****** Object:  View [dbo].[rep_view_ListadoDepositos]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[rep_view_ListadoDepositos]
as
Select 
	tra.Sequence as Numero, 
	tra.BankId EnrollmentId,
	0 as EnrollmentSec
	,baa.Description as Name
	,ban.Name ShortDescription
	,tra.Amount
	,tra.[Date] as TransDate
	,baa.Id as BankAccountId
From Transactions tra
inner join Banks ban on ban.Id = tra.BankId
inner join BankAccounts baa on baa.BanktId = ban.Id
where TransactionTypeId = 2
 and tra.StatusId = 1;
GO
/****** Object:  View [dbo].[rep_view_ListadoMontosPorConceptos]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					create view [dbo].[rep_view_ListadoMontosPorConceptos] as
					select 
						' ' ResumenRecibos, 
						t.TenantId, 
						te.Name TenantName,
						t.ConceptId, 
						ISNULL(ca.AccountNumber, ca2.AccountNumber) + ' - ' +  isnull(c.Description,c2.Description) Description,  
						t.Date,
						t.CreationTime,
						isnull(tc.Amount ,t.Amount) as Amount,
						o.Name,
						AmountDebit = case when t.OriginId = 1 then isnull(tc.Amount ,t.Amount) else 0.00 end,  --Charges
						AmountCredit = case when t.OriginId = 2 then isnull(tc.Amount ,t.Amount) else 0.00 end  --Recibos
					from Transactions t 
						left join Concepts c on c.id = t.ConceptId
						inner join AbpTenants te on te.id = t.TenantId
						inner join Origins o on o.Id = t.OriginId
						left join Catalogs ca on ca.Id = c.CatalogId
						left join TransactionConcepts tc on tc.TransactionId = t.Id
						left join Concepts c2 on c2.id = tc.ConceptId
						left join Catalogs ca2 on ca2.Id = c2.CatalogId
					where t.IsDeleted = 0  and t.IsActive = 1 and  t.TransactionTypeId = 1
			
GO
/****** Object:  View [dbo].[rep_view_ListadoMontosPorConceptosTrans]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				Create view [dbo].[rep_view_ListadoMontosPorConceptosTrans] as
					select ' ' TransactionsResume, t.TenantId, 
					te.Name TenantName,
					t.ConceptId, 
					isnull(c.Description,(Select top 1 c2.Description + ' ...' from TransactionConcepts tc2 inner join Concepts c2 on c2.Id = tc2.ConceptId where tc2.TransactionId = t.Id )) as Description,  
					t.Date,
					t.CreationTime,
					t.Amount ,
					AmountDebit = case when t.OriginId = 1 then t.amount else 0.00 end,  --Charges
					AmountCredit = case when t.OriginId = 2 then t.amount else 0.00 end  --Recibos
					from Transactions t 
					left join Concepts c on c.id = t.ConceptId
					inner join AbpTenants te on te.id = t.TenantId
					where t.IsDeleted = 0 and t.TransactionTypeId = 3
			
GO
/****** Object:  View [dbo].[rep_view_ListadoMontosPorCuentas]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					create view [dbo].[rep_view_ListadoMontosPorCuentas] as
					select 
					t.TenantId, 
					te.Name TenantName,
					isnull(cg2.Id, cg.Id) CatalogId,
					isnull(cg2.AccountNumber, cg.AccountNumber) AccountNumber,
					isnull(cg2.Descsription, cg.Descsription)  Descsription,
					isnull(cg2.Name, cg.Name) as Name,
					t.Date,
					t.CreationTime,
					isnull(tc.Amount, t.Amount) as Amount 
					from Transactions t 
					inner join AbpTenants te on te.id = t.TenantId
					left join Concepts c on c.id = t.ConceptId
					inner join Catalogs cg on cg.id = c.CatalogId
					left join TransactionConcepts tc on tc.TransactionId = t.Id
					left join Concepts c2 on c2.Id = tc.ConceptId
					left join Catalogs cg2 on cg2.Id = c2.CatalogId
					where t.IsDeleted = 0 and t.TransactionTypeId=1
			
GO
/****** Object:  View [dbo].[rep_view_ListadoRecibos]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[rep_view_ListadoRecibos]
as
Select 
	tra.Sequence as Numero, 
	isnull(tra.EnrollmentId,0) as EnrolllmentId,
	enrxsec.Sequence as EnrollmentSec
	,tra.Name
	,con.ShortDescription
	,tra.Amount
	,tra.[Date] as TransDate
From Transactions tra
left join EnrollmentSequences enrxsec on tra.EnrollmentId = enrxsec.EnrollmentId and tra.TenantId = enrxsec.TenantId
inner join Concepts con on con.Id = tra.ConceptId
where TransactionTypeId = 1
 and tra.StatusId = 1;
GO
/****** Object:  View [dbo].[rep_view_listadoUsuarios]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[rep_view_listadoUsuarios]
as
select 
	u.Id UserId, u.Name as StudenName, u.Surname StudenSurname, u.UserName, u.TenantId , t.Name as TenantName , dbo.rep_fn_getRolesName(u.Id) AS RolesName
from AbpUsers u 
inner join AbpTenants t on u.TenantId = t.Id
where u.IsDeleted=0

GO
/****** Object:  View [dbo].[rep_view_ListReciptShops]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 08>
			-- Description:   <Se busca el listado de recibos de tienda>
			-- =============================================
			CREATE VIEW [dbo].[rep_view_ListReciptShops]
			AS 
			select 
				  ts.Id as TransactionShopId,
				  ts.Sequence,
				  ts.Amount,
				  ts.ClientShopId,
				  ts.TenantId,
				  tas.ArticleShopId,
				  tas.Amount as AmountArticle,
				  tas.Total as TotalArticle,
				  tas.Quantity as QuantityArticle,
				  arsh.Description as DescriptionArticle,
				  arsh.Reference as ReferenceArticle,
				  arsh.Price as PriceArticle, 
				  cs.Name as ClientName,
				  cs.Reference as ClientReference,
				  pm.Name as PaymentMethodsName,
				  at.Name as TenantName,
				  ts.CreationTime as Date,
				  ts.CreationTime
			from TransactionShops ts
			inner join TransactionArticleShops tas on tas.TransactionShopId = ts.Id and ts.TenantId = tas.TenantId
			inner join ArticleShops arsh on arsh.Id = tas.ArticleShopId and arsh.TenantId = ts.TenantId
			inner join ClientShops cs on cs.Id = ts.ClientShopId and cs.TenantId = ts.TenantId
			inner join PaymentMethods pm on pm.Id = ts.PaymentMethodId
			inner join AbpTenants at on at.Id = ts.TenantId
	  
GO
/****** Object:  View [dbo].[rep_view_ListReciptShopsList]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 09>
			-- Description:   <Se busca el listado de recibos de tienda listado>
			-- =============================================
			CREATE VIEW [dbo].[rep_view_ListReciptShopsList]
			AS 
			SELECT 
				at.Name AS TenantName,
				ts.CreationTime as Date ,
				NCF = '',
				Number = CAST(ts.Sequence AS VARCHAR),
				cs.Name as Name ,
				ts.Amount as Amount ,
				'Venta Tienda' AS Concept,
				ts.Id AS TransactionId,
				ts.TenantId,
				1 as ConceptId,
				cs.Name as ClientName,
				cs.Reference as ClientReference,
				pm.Name as PaymentMethodsName,
				ts.PaymentMethodId,
				pm.Name PaymentMethod
			from TransactionShops ts
			inner join ClientShops cs on cs.Id = ts.ClientShopId and cs.TenantId = ts.TenantId
			inner join PaymentMethods pm on pm.Id = ts.PaymentMethodId
			inner join AbpTenants at on at.Id = ts.TenantId
			;
			
GO
/****** Object:  View [dbo].[rep_view_PaymentProjections]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

						Create View  [dbo].[rep_view_PaymentProjections] as
						SELECT 
						' ' PaymentProjectionReport,
						ten.Name ,
						er.Id AS EnrollmentId,
						er.FirstName,
						er.LastName,
						EnrollmentName = er.FirstName + ' ' + er.Lastname,
						t.PaymentDate ,
						t.Sequence,
						ers.Sequence EnrollmentSequence,
						case t.sequence when 0 then 'Matricula' else 'Mensualidad' end ConceptDescription,
						t.Amount,
						t.TenantId 
						FROM  dbo.PaymentProjections t 
						inner join EnrollmentStudents ee on ee.id = t.enrollmentStudentId
						INNER JOIN dbo.Enrollments er ON er.Id = ee.EnrollmentId
						INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId  
						inner join AbpTenants ten on ten.Id = t.TenantId
						WHERE t.IsDeleted = 0 AND ers.IsDeleted = 0 AND t.IsActive = 1 
			
GO
/****** Object:  View [dbo].[rep_viewListadoUsuarios]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[rep_viewListadoUsuarios]
as
select 
	u.Id UserId, u.Name as StudenName, u.Surname StudenSurname, u.UserName, u.TenantId , t.Name as TenantName , dbo.rep_fn_getRolesName(u.Id) AS RolesName
from AbpUsers u 
inner join AbpTenants t on u.TenantId = t.Id
where u.IsDeleted=0
GO
/****** Object:  View [dbo].[rep_vw_AccountBalance]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

			CREATE VIEW [dbo].[rep_vw_AccountBalance]
			AS
			SELECT 
			 er.Id AS EnrollmentId,
			 er.FirstName,
			 er.LastName,
			 t.Date ,
			 Number = tt.Abbreviation + '-' + CAST(t.Sequence AS VARCHAR),
			 tt.Abbreviation,
			 t.Sequence,
			 ers.Sequence EnrollmentSequence,
			 cp.Description AS ConceptDescription,
			  t.OriginId,
			  Debit = CASE t.OriginId WHEN 1 THEN t.Amount ELSE 0 END ,
			  Credit = CASE t.OriginId WHEN 2 THEN t.Amount ELSE 0 END ,
			  ts.TenantId 
			  FROM TransactionStudents ts
			  INNER JOIN dbo.Transactions t ON t.Id = ts.TransactionId 
			  INNER JOIN dbo.TransactionTypes tt ON tt.Id = ts.TransactionTypeId 
			  INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
			  INNER JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
			  INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = ts.TenantId  
			  WHERE ts.IsDeleted = 0 AND t.IsDeleted = 0 AND t.IsActive = 1;
			
GO
/****** Object:  View [dbo].[rep_vw_DebitCredit]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[rep_vw_DebitCredit]
  AS
SELECT TOP 2000000
tn.Name AS TenantName,
tn.Phone1,
t.Date ,
NCF = '',
Number = CAST(t.Sequence AS VARCHAR),
t.Name ,
t.Amount ,
cp.Description AS Concept,
t.Id AS TransactionId,
t.TenantId,
t.ConceptId,
ers.Sequence as EnrollmentSequence,
Debit = CASE t.OriginId WHEN 1 THEN t.Amount ELSE 0 end,
Credit = CASE t.OriginId WHEN 2 THEN t.Amount ELSE 0 end
FROM dbo.Transactions t
INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
INNER JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
WHERE  t.IsDeleted = 0 AND t.IsActive = 1
ORDER BY t.Sequence
GO
/****** Object:  View [dbo].[rep_vw_EnrollmentStudentCount]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  CREATE VIEW [dbo].[rep_vw_EnrollmentStudentCount]
  AS

SELECT 
sq.TenantId,
Tenant = tn.Name ,
sq.Sequence,
EnrollmentName = CONCAT(enr.FirstName,' ', enr.LastName),
StudentId = ers.Id
FROM dbo.Enrollments enr
LEFT JOIN dbo.EnrollmentSequences sq ON sq.EnrollmentId = enr.Id 
LEFT JOIN dbo.EnrollmentStudents ers ON ers.EnrollmentId = enr.Id 
LEFT JOIN dbo.AbpTenants tn ON tn.Id = sq.TenantId
WHERE ers.IsDeleted = 0 and sq.IsDeleted=0


GO
/****** Object:  View [dbo].[rep_vw_ReceiptArticles]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

						CREATE VIEW [dbo].[rep_vw_ReceiptArticles]
							AS
						SELECT isnull(ers.Sequence,'') EnrollmentSequence,
						tn.Name AS TenatnName,
						tn.Phone1,
						u.Description Unit,
						a.Description Article,
						t.Date ,
						NCF = isnull(t.TaxReceiptSequence,''),
						Number = CAST(t.Sequence AS VARCHAR),
						t.Name ,
						t.TransactionTypeId,
						t.ReceiptTypeId,
						t.Amount ,
						isnull(cp.Description,(Select top 1 c2.Description + ' ...' from TransactionConcepts tc2 inner join Concepts c2 on c2.Id = tc2.ConceptId where tc2.TransactionId = t.Id )) AS Concept,
						t.Id AS TransactionId,
						t.TenantId,
						t.ConceptId, 
						ta.Quantity,
						ta.Amount Price,
						ta.Total,
						t.PaymentMethodId,
						pm.Name PaymentMethod,
						t.RncName, 
						t.RncNumber,
						t.TaxReceiptSequence,
						tarety.Name as TaxReceiptName,
						tare.ExpirationDate,
						case when t.Comments is null or t.Comments = '' then '' else '<strong>Comentario:</strong> ' + t.Comments end as Comments
						FROM dbo.Transactions t
						INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
						INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
						LEFT JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
						LEFT JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
						LEFT JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
						INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
						INNER JOIN TransactionArticles ta on ta.TransactionId = t.Id
						inner join Articles a on a.Id = ta.ArticleId
						inner join Units u on u.Id = a.UnitId
						LEFT JOIN TaxReceipts tare on tare.Id = t.TaxReceiptId
						LEFT JOIN TaxReceiptTypes tarety on tarety.Id = tare.TaxReceiptTypeId
						WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1 AND t.ReceiptTypeId = 3               
			
GO
/****** Object:  View [dbo].[rep_vw_ReceiptDeposit]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

				
				create VIEW [dbo].[rep_vw_ReceiptDeposit]
							AS
								SELECT
						tn.Name AS TenatnName,
						tn.Phone1,
						t.Date ,
						t.TransactionTypeId,
						t.ReceiptTypeId,
						t.CreditCardPercent,
						NCF = isnull(t.TaxReceiptSequence,''),
						Number = CAST(t.Sequence AS VARCHAR),
						t.Name ,
						CASe when t.TransactionTypeId = 1 then t.Amount 
							 when t.TransactionTypeId = 2 then t.Amount * -1 end Amount,
						CASe when t.TransactionTypeId = 1 and t.PaymentMethodId = 2 then t.Amount  - round( t.creditCardPercent * t.Amount,2)
							 when t.TransactionTypeId = 1 and t.PaymentMethodId != 2 then t.Amount 
							 when t.TransactionTypeId = 2 then t.Amount * -1 end AmountMinusRetention,
						case when isnull(t.PaymentMethodId,0) = 2 and t.TransactionTypeId = 1 then round( t.creditCardPercent * t.Amount,2) else 0 end Retention,
						case when isnull(t.PaymentMethodId,0) = 2 and t.TransactionTypeId = 1 then round( t.Amount - round( t.creditCardPercent * t.Amount,2) ,2) else 0 end CreditCardAmount,
						 isnull(t.PaymentMethodId,0) pidd,
						isnull(cp.Description,(Select top 1 c2.Description + ' ...' from TransactionConcepts tc2 inner join Concepts c2 on c2.Id = tc2.ConceptId where tc2.TransactionId = t.Id )) AS Concept,
						t.Id AS TransactionId,
						t.TenantId,
						t.ConceptId,
						ers.Sequence as EnrollmentSequence,
						t.PaymentMethodId,
						case when t.TransactionTypeId = 1 then pm.Name 
							when t.TransactionTypeId = 2 then 'Deposits' end PaymentMethod,
						 ' ' AS StudentName,
						0 AS EnrollmentStudentId ,
						'' AS CourseName,
						0 AS CourseId,
						'' AS SeccionName,
						0 AS SeccionId,
						0 AS StudentAmount,
						t.RncName, 
						t.RncNumber,
						t.TaxReceiptSequence,
						tarety.Name as TaxReceiptName
						FROM dbo.Transactions t
						INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
						INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
						left JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
						left JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
						left JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
						left JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
						LEFT JOIN TaxReceipts tare on tare.Id = t.TaxReceiptId
						LEFT JOIN TaxReceiptTypes tarety on tarety.Id = tare.TaxReceiptTypeId
						WHERE  t.IsDeleted = 0 AND t.IsActive = 1  
						AND (
								(t.TransactionTypeId = 1) or
								(t.TransactionTypeId = 2)
							)
			
			
			
GO
/****** Object:  View [dbo].[rep_vw_ReceiptGeneral]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

						CREATE VIEW [dbo].[rep_vw_ReceiptGeneral]
						AS
						SELECT
						tn.Name AS TenatnName,
						tn.Phone1,
						t.Date ,
						NCF = isnull(t.TaxReceiptSequence,''),
						Number = CAST(t.Sequence AS VARCHAR),
						t.Name ,
						t.TransactionTypeId,
						t.ReceiptTypeId,
						t.Amount as Amount,
						isNull(cp2.Description, cp.Description) AS Concept,
						t.Id AS TransactionId,
						t.TenantId,
						t.ConceptId,
						t.PaymentMethodId,
						pm.Name PaymentMethod,
						tc2.Amount as AmountConcept,
						t.RncName, 
						t.RncNumber,
						t.TaxReceiptSequence,
						tarety.Name as TaxReceiptName,
						tare.ExpirationDate,
						case when t.Comments is null or t.Comments = '' then '' else '<strong>Comentario:</strong> ' + t.Comments end as Comments
						FROM dbo.Transactions t
						INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
						INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
						left JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
						INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
						LEFT join dbo.TransactionConcepts tc2 on tc2.transactionId = t.Id
						left join  dbo.Concepts cp2 ON cp2.id = tc2.ConceptId
						LEFT JOIN TaxReceipts tare on tare.Id = t.TaxReceiptId
						LEFT JOIN TaxReceiptTypes tarety on tarety.Id = tare.TaxReceiptTypeId
						WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1
						and t.ReceiptTypeId = 2          
			
GO
/****** Object:  View [dbo].[rep_vw_Receipts]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 18>
			-- Description:   <Se busca el listado de recibos>
			-- =============================================
			CREATE VIEW [dbo].[rep_vw_Receipts] 
			 AS
			SELECT 
				tn.Name AS TenantName,
				tn.Phone1,
				t.Date ,
				NCF = '',
				Number = CAST(t.Sequence AS VARCHAR),
				t.Name ,
				t.Amount ,
				isnull(cp.Description,(Select top 1 c2.Description + ' ...' from TransactionConcepts tc2 inner join Concepts c2 on c2.Id = tc2.ConceptId where tc2.TransactionId = t.Id )) AS Concept,
				t.Id AS TransactionId,
				t.TenantId,
				t.ConceptId,
				isnull(ers.Sequence,0) as EnrollmentSequence,
				t.PaymentMethodId,
				pm.Name PaymentMethod,
				t.RncName, 
				t.RncNumber,
				t.TaxReceiptSequence,
				tarety.Name as TaxReceiptName
			FROM dbo.Transactions t
			INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
			INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
			LEFT JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
			LEFT JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
			LEFT JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
			INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
			LEFT JOIN TaxReceipts tare on tare.Id = t.TaxReceiptId
			LEFT JOIN TaxReceiptTypes tarety on tarety.Id = tare.TaxReceiptTypeId
			WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1
			
GO
/****** Object:  View [dbo].[Rep_vw_SchoolList]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Rep_vw_SchoolList]
AS
SELECT Id AS Code, Name AS Description, Id as TenantId FROM dbo.AbpTenants 
WHERE IsDeleted = 0

GO
/****** Object:  View [dbo].[rep_vw_Transactions]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[rep_vw_Transactions]
  AS
SELECT TOP 2000000
tn.Name AS TenantName,
tn.Phone1,
t.Date ,
NCF = '',
Number = CAST(t.Sequence AS VARCHAR),
t.Name ,
t.Amount ,
cp.Description AS Concept,
t.Id AS TransactionId,
t.TenantId,
t.ConceptId,
ers.Sequence as EnrollmentSequence,
Debit = CASE t.OriginId WHEN 1 THEN t.Amount ELSE 0 end,
Credit = CASE t.OriginId WHEN 2 THEN t.Amount ELSE 0 end
FROM dbo.Transactions t
INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
INNER JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 3
ORDER BY t.Sequence
GO
/****** Object:  View [dbo].[VW_Proyection]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					-- =============================================
					-- Author:		<Eduardo Santana>
					-- Create date: <2018 - 08 - 11>
					-- Description:	<Se buscan las proyecciones de pagos de los tutores>
					-- =============================================
					create view [dbo].[VW_Proyection] as
						SELECT
							  e.FirstName + ' ' + e.LastName EnrollmentFullName,
							  es.Sequence Enrollment
							  ,pp.Amount ProjectionAmount
							  ,t.Name Tenant
							  ,pp.PaymentDate 
							  ,pp.TenantId
							  ,pp.IsDeleted
							  ,pp.IsActive
						  FROM dbo.PaymentProjections pp inner join EnrollmentStudents est on est.Id = pp.EnrollmentStudentId 
						  inner join Enrollments e on  est.EnrollmentId = e.Id 
						  inner join EnrollmentSequences es on es.EnrollmentId = e.Id
						  inner join AbpTenants t on t.Id = pp.TenantId and t.Id = es.TenantId
						  where pp.IsActive = 1 and pp.IsDeleted = 0
			
GO
/****** Object:  Table [dbo].[DataTypes]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[TypeHTML] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
 CONSTRAINT [PK_dbo.DataTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DocumentTypes]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
 CONSTRAINT [PK_dbo.DocumentTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Occupations]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Occupations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
 CONSTRAINT [PK_dbo.Occupations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReportDataSources]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDataSources](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Code] [nvarchar](20) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
 CONSTRAINT [PK_dbo.ReportDataSources] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reports]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reports](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tabla] [nvarchar](100) NULL,
	[ReportsTypesId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
	[Consulta]  AS ([dbo].[fn_Campos]([Tabla])),
	[ReportCode] [nvarchar](80) NOT NULL,
	[ReportName] [nvarchar](100) NOT NULL,
	[View] [nvarchar](max) NOT NULL,
	[ReportRoot] [nvarchar](100) NOT NULL,
	[ReportPath] [nvarchar](500) NOT NULL,
	[ReportDataSourceId] [int] NOT NULL,
	[FilterByTenant] [bit] NOT NULL,
	[IsForTenant] [int] NOT NULL,
	[PermissionName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Reports] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReportsFilters]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportsFilters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReportsId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
	[ReportId] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[DisplayName] [nvarchar](max) NULL,
	[DataField] [nvarchar](max) NULL,
	[DataTypeId] [int] NOT NULL,
	[Range] [bit] NOT NULL,
	[OnlyParameter] [bit] NOT NULL,
	[UrlService] [nvarchar](max) NULL,
	[FieldService] [nvarchar](max) NULL,
	[DisplayNameService] [nvarchar](max) NULL,
	[DependencyField] [nvarchar](max) NULL,
	[Operator] [nvarchar](max) NULL,
	[DisplayNameRange] [nvarchar](max) NULL,
	[Required] [bit] NOT NULL,
	[DefaultValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.ReportsFilters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Courses] ADD  DEFAULT ((0)) FOR [Sequence]
GO
ALTER TABLE [dbo].[Reports] ADD  DEFAULT ('') FOR [ReportCode]
GO
ALTER TABLE [dbo].[Reports] ADD  DEFAULT ('') FOR [ReportName]
GO
ALTER TABLE [dbo].[Reports] ADD  DEFAULT ('') FOR [View]
GO
ALTER TABLE [dbo].[Reports] ADD  DEFAULT ('') FOR [ReportRoot]
GO
ALTER TABLE [dbo].[Reports] ADD  DEFAULT ('') FOR [ReportPath]
GO
ALTER TABLE [dbo].[Reports] ADD  DEFAULT ((0)) FOR [ReportDataSourceId]
GO
ALTER TABLE [dbo].[Reports] ADD  DEFAULT ((0)) FOR [FilterByTenant]
GO
ALTER TABLE [dbo].[Reports] ADD  DEFAULT ((0)) FOR [IsForTenant]
GO
ALTER TABLE [dbo].[ReportsFilters] ADD  DEFAULT ((0)) FOR [ReportId]
GO
ALTER TABLE [dbo].[ReportsFilters] ADD  DEFAULT ((0)) FOR [Order]
GO
ALTER TABLE [dbo].[ReportsFilters] ADD  DEFAULT ((0)) FOR [DataTypeId]
GO
ALTER TABLE [dbo].[ReportsFilters] ADD  DEFAULT ((0)) FOR [Range]
GO
ALTER TABLE [dbo].[ReportsFilters] ADD  DEFAULT ((0)) FOR [OnlyParameter]
GO
ALTER TABLE [dbo].[ReportsFilters] ADD  DEFAULT ((0)) FOR [Required]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Courses_dbo.Levels_LevelId] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Levels] ([Id])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_dbo.Courses_dbo.Levels_LevelId]
GO
ALTER TABLE [dbo].[Reports]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Reports_dbo.ReportDataSources_ReportDataSourceId] FOREIGN KEY([ReportDataSourceId])
REFERENCES [dbo].[ReportDataSources] ([Id])
GO
ALTER TABLE [dbo].[Reports] CHECK CONSTRAINT [FK_dbo.Reports_dbo.ReportDataSources_ReportDataSourceId]
GO
ALTER TABLE [dbo].[Reports]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Reports_dbo.ReportsTypes_ReportsTypesId] FOREIGN KEY([ReportsTypesId])
REFERENCES [dbo].[ReportsTypes] ([Id])
GO
ALTER TABLE [dbo].[Reports] CHECK CONSTRAINT [FK_dbo.Reports_dbo.ReportsTypes_ReportsTypesId]
GO
ALTER TABLE [dbo].[ReportsFilters]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReportsFilters_dbo.DataTypes_DataTypeId] FOREIGN KEY([DataTypeId])
REFERENCES [dbo].[DataTypes] ([Id])
GO
ALTER TABLE [dbo].[ReportsFilters] CHECK CONSTRAINT [FK_dbo.ReportsFilters_dbo.DataTypes_DataTypeId]
GO
ALTER TABLE [dbo].[ReportsFilters]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReportsFilters_dbo.Reports_ReportsId] FOREIGN KEY([ReportsId])
REFERENCES [dbo].[Reports] ([Id])
GO
ALTER TABLE [dbo].[ReportsFilters] CHECK CONSTRAINT [FK_dbo.ReportsFilters_dbo.Reports_ReportsId]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProjectionAmountByEnrollment]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
			
			-- =============================================
			-- Author:		<Amaury Nunez>
			-- Create date: <2018 - 08 - 30>
			-- Description:	<Buscar informacion agrupada por tutor y en un rango de fecha dada>
			-- =============================================
			
				create procedure [dbo].[sp_ProjectionAmountByEnrollment] (@TenantId int = 0, @dateFrom as datetime, @dateTo as datetime) as

				if(ISNULL(@TenantId,0) = 0)
				begin
					select sum(p.ProjectionAmount) Balance, 
						p.EnrollmentFullName EnrollmentName, p.Enrollment Sequence, p.TenantId,p.tenant, p.IsDeleted, p.IsActive  
					from VW_Proyection p
						where p.PaymentDate >= @dateFrom and p.PaymentDate <= @dateTo
					group by p.EnrollmentFullName, p.Enrollment, p.TenantId,p.tenant, p.IsDeleted, p.IsActive
				end
				else
				begin
					select sum(p.ProjectionAmount) Balance, 
						p.EnrollmentFullName EnrollmentName, p.Enrollment Sequence, p.TenantId,p.tenant, p.IsDeleted, p.IsActive  
					from VW_Proyection p
						where p.PaymentDate >= @dateFrom and p.PaymentDate <= @dateTo and p.TenantId = @TenantId
					group by p.EnrollmentFullName, p.Enrollment, p.TenantId,p.tenant, p.IsDeleted, p.IsActive
				end

			
GO
/****** Object:  StoredProcedure [dbo].[sp_rep_AccountBalanceListByStudent]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
			
			-- =============================================
			-- Author:		<Eduardo Santana>
			-- Create date: <2018 - 01 - 02>
			-- Description:	<Buscar informacion agrupada filtrada por fecha>
			-- =============================================
			CREATE PROCEDURE [dbo].[sp_rep_AccountBalanceListByStudent]
				-- Add the parameters for the stored procedure here
				@TenantId int = NULL,
				@ToDate datetime = NULL,
				@CourseId int = NULL
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				select 
					es.Sequence, 
					Tenant = at.Name, 
					e.id, 
					e.FirstName, 
					e.LastName, 
					EnrollmentName = e.FirstName + ' ' + e.LastName, 
					e.Phone1, 
					e.Phone2, 
					t.TenantId,
					sum(trst.Amount * (case t.OriginId when 2 then -1 else 1 end)) Balance,
					st.FirstName StudenFirstName, 
					st.LastName StudenLastName, 
					StudenName = st.FirstName + ' ' + st.LastName + ' / ' +  e.FirstName + ' ' + e.LastName,
					trst.EnrollmentStudentId,
					ces.CourseId,
					CourseName = co.Name 
				from 
					Enrollments e 
					inner join EnrollmentSequences es on e.id = es.EnrollmentId
					inner join Transactions t on 
						t.EnrollmentId = e.Id and 
						es.EnrollmentId = t.enrollmentId and 
						t.TenantId = es.TenantId 
					inner join AbpTenants at on at.Id = t.TenantId
					inner join TransactionStudents trst on trst.TransactionId = t.Id 
					inner join EnrollmentStudents enst on enst.Id = trst.EnrollmentStudentId and enst.TenantId = t.TenantId
					inner join Students st on st.Id = enst.StudentId 
					inner join CourseEnrollmentStudents ces on ces.EnrollmentStudentId = trst.EnrollmentStudentId and ces.TenantId = t.TenantId
					inner join Courses co on co.Id = ces.CourseId
				where 
					t.TransactionTypeId in (1, 3) and 
					isnull(t.ReceiptTypeId,0) not in (2, 3) and
					(t.TenantId = @TenantId or @TenantId is null) and 
					t.[Date] < convert(datetime, convert(varchar, @ToDate, 23) + ' 23:59:59:999', 21) and
					(@CourseId IS null or @CourseId = ces.CourseId)
				group by 
					e.id, at.Name,
					e.FirstName, 
					e.LastName, 
					e.Phone1, 
					e.Phone2, 
					es.Sequence, 
					t.TenantId,
					st.FirstName, 
					st.LastName,
					trst.EnrollmentStudentId,
					ces.CourseId,
					co.Name 
			END

			
GO
/****** Object:  StoredProcedure [dbo].[sp_rep_GetCxCReportTutor]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
			
			-- =============================================
			-- Author:		<Eduardo Santana>
			-- Create date: <2018 - 08 - 28>
			-- Description:	<Buscar informacion agrupada filtrada por fecha>
			-- =============================================
			CREATE PROCEDURE [dbo].[sp_rep_GetCxCReportTutor]
				-- Add the parameters for the stored procedure here
				@TenantId int = NULL,
				@ToDate datetime
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				-- Insert statements for procedure here
				select 
					es.Sequence, 
					Tenant = at.Name, 
					e.id, 
					e.FirstName, 
					e.LastName, 
					EnrollmentName = e.FirstName + ' ' + e.LastName, 
					e.Phone1, 
					e.Phone2, 
					t.TenantId
					,sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) Balance
					,sum( (case t.OriginId when  1 then t.Amount else 0 end)) Debit
					,sum( (case t.OriginId when  2 then t.Amount else 0 end)) Credit
				from 
					Enrollments e 
					inner join EnrollmentSequences es on e.id = es.EnrollmentId
					inner join Transactions t on t.EnrollmentId = e.Id and es.EnrollmentId = t.enrollmentId and t.TenantId = es.TenantId 
					inner join AbpTenants at on at.Id = t.TenantId
				where 
					t.TransactionTypeId in (1 , 3) and 
					isnull(t.ReceiptTypeId, 0) not in ( 2 , 3 ) and 
					(t.TenantId = @TenantId or @TenantId is null) and
					t.[Date] < convert(datetime, convert(varchar, @ToDate, 23) + ' 23:59:59:999', 21)
				group by 
					e.id, at.Name,e.FirstName, e.LastName, e.Phone1, e.Phone2, es.Sequence, t.TenantId
			END

			
GO
/****** Object:  StoredProcedure [dbo].[sp_rep_StudentByCourseBySession]    Script Date: 2019-09-18 19:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
			
			-- =============================================
			-- Author:		<Eduardo Santana>
			-- Create date: <2018 - 09 - 05>
			-- Description:	<Buscar informacion de estudiantes por curso por sessiones>
			-- =============================================
			CREATE PROCEDURE [dbo].[sp_rep_StudentByCourseBySession]
				-- Add the parameters for the stored procedure here
				@TenantId int = NULL,
				@CourseId int = NULL,
				@SessionId int = NULL
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				Select 
					stu.LastName,
					stu.FirstName,
					enrxsec.Sequence,
					enr.FirstName + ' ' + enr.LastName as FullName,
					cou.Name CourseName,
					enrxstu.TenantId,
					stu.Id as StudentId,
					stu.DateOfBirth,
					cou.Id as CourseId,
					enr.Id as EnrollmentId,
					enrxstu.PeriodId ,
					pr.Period,
					tn.Name Tenant,
					cou.LevelId,
					lv.Name AS LevelName,
					isnull(couxenrxstu.Sequence,  ROW_NUMBER() OVER(ORDER BY stu.LastName,stu.FirstName ASC)) as SequenceStudent,
					(CASE WHEN couxenrxstu.Sequence IS NULL THEN '**' ELSE '' END) as IsTemporalSequence,
					couxenrxstu.SessionId,
					corses.Name as SessionName,
					stu.LastName + ' ' + stu.FirstName as StudentFullName
				From Students stu 
					inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
					inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId and enrxsec.TenantId = enrxstu.TenantId 
					inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
					inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
					inner join Courses cou on cou.Id = couxenrxstu.CourseId
					INNER JOIN dbo.Periods pr ON pr.Id = enrxstu.PeriodId 
					LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId 
					INNER JOIN dbo.Levels lv ON lv.Id = cou.LevelId 
					LEFT JOIN dbo.CourseSessions corses on corses.CourseId = cou.Id and corses.Id = couxenrxstu.SessionId
				WHERE stu.IsDeleted = 0 AND stu.IsActive = 1 and enrxsec.IsDeleted = 0 and enrxstu.IsDeleted = 0
				and enrxstu.TenantId = @TenantId 
				and cou.Id  = @CourseId
				and couxenrxstu.SessionId = @SessionId
				and tn.PeriodId = enrxstu.PeriodId
			END

			
GO

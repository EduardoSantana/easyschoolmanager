
SET IDENTITY_INSERT [dbo].[ReportDataSources] ON 

GO
INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'View', N'01')
GO
INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (2, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'Procedure', N'02')
GO
INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (3, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'Query', N'03')
GO
INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (4, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'Report', N'04')
GO
SET IDENTITY_INSERT [dbo].[ReportDataSources] OFF
GO
SET IDENTITY_INSERT [dbo].[Reports] ON 

GO
INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant]) VALUES (9, 1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'01', N'Schools', N'Rep_VW_SchoolList', N'Reports\', N'RdlcFiles\Generales\CodeDescription.rdlc', 0)
GO
INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant]) VALUES (10, 1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'02', N'EnrollmentList', N'rep_vw_EnrollmentStudentCount', N'Reports\', N'RdlcFiles\Generales\EnrollmentList.rdlc', 1)
GO
SET IDENTITY_INSERT [dbo].[Reports] OFF
GO
SET IDENTITY_INSERT [dbo].[DataTypes] ON 

GO
INSERT [dbo].[DataTypes] ([Id], [Code], [Description], [TypeHTML], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (1, N'01', N'Numero', N'number', 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[DataTypes] ([Id], [Code], [Description], [TypeHTML], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (2, N'02', N'Texto', N'text', 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[DataTypes] ([Id], [Code], [Description], [TypeHTML], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (3, N'03', N'Fecha', N'date', 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DataTypes] OFF
GO

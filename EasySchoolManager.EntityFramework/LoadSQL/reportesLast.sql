USE [EasySchoolManager_]
GO
SET IDENTITY_INSERT [dbo].[ReportDataSources] ON 

GO
INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'View', N'01')
GO
INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (2, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'Procedure', N'02')
GO
INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (3, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'Query', N'03')
GO
INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (4, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'Report', N'04')
GO
SET IDENTITY_INSERT [dbo].[ReportDataSources] OFF
GO
SET IDENTITY_INSERT [dbo].[Reports] ON 

GO
INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant]) VALUES (9, 1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'01', N'Schools', N'Rep_VW_SchoolList', N'Reports\', N'RdlcFiles\Generales\CodeDescription.rdlc', 0)
GO
INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant]) VALUES (10, 1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'02', N'EnrollmentList', N'rep_vw_EnrollmentStudentCount', N'Reports\', N'RdlcFiles\Generales\EnrollmentList.rdlc', 1)
GO
INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant]) VALUES (11, 1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'02', N'StudentByCourse', N'rep_vw_StudentByCourse', N'Reports\', N'RdlcFiles\Generales\StudentByCourse.rdlc', 1)
GO
SET IDENTITY_INSERT [dbo].[Reports] OFF
GO
SET IDENTITY_INSERT [dbo].[DataTypes] ON 

GO
INSERT [dbo].[DataTypes] ([Id], [Code], [Description], [TypeHTML], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (1, N'01', N'Numero', N'number', 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[DataTypes] ([Id], [Code], [Description], [TypeHTML], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (2, N'02', N'Texto', N'text', 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[DataTypes] ([Id], [Code], [Description], [TypeHTML], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (3, N'03', N'Fecha', N'date', 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DataTypes] OFF
GO



  CREATE VIEW [dbo].[rep_vw_EnrollmentStudentCount]
  AS

SELECT 
sq.TenantId,
Tenant = tn.Name ,
sq.Sequence,
EnrollmentName = CONCAT(enr.FirstName,' ', enr.LastName),
StudentId = ers.Id
FROM dbo.Enrollments enr
LEFT JOIN dbo.EnrollmentSequences sq ON sq.EnrollmentId = enr.Id 
LEFT JOIN dbo.EnrollmentStudents ers ON ers.EnrollmentId = enr.Id 
LEFT JOIN dbo.AbpTenants tn ON tn.Id = sq.TenantId
WHERE ers.IsDeleted = 0 


GO

CREATE VIEW [dbo].[Rep_vw_SchoolList]
AS
SELECT Id AS Code, Name AS Description FROM dbo.AbpTenants 
WHERE IsDeleted = 0

GO

CREATE VIEW [dbo].[rep_vw_StudentByCourse]
as
Select 
	stu.LastName,
	stu.FirstName,
	enrxsec.Sequence,
	enr.FirstName + ' ' + enr.LastName as FullName,
	cou.Name CourseName,
	enrxstu.TenantId,
	stu.Id as StudentId,
	cou.Id as CourseId,
	enr.Id as EnrollmentId,
	enrxstu.PeriodId ,
	pr.Period,
	tn.Name Tenant 
From Students stu 
inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId
inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
inner join Courses cou on cou.Id = couxenrxstu.CourseId
INNER JOIN dbo.Periods pr ON pr.Id = enrxstu.PeriodId 
LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId 
WHERE stu.IsDeleted = 0 AND stu.IsActive = 1
GO


ALTER VIEW [dbo].[rep_vw_StudentByCourse]
as
Select 
	stu.LastName,
	stu.FirstName,
	enrxsec.Sequence,
	enr.FirstName + ' ' + enr.LastName as FullName,
	cou.Name CourseName,
	enrxstu.TenantId,
	stu.Id as StudentId,
	cou.Id as CourseId,
	enr.Id as EnrollmentId,
	enrxstu.PeriodId ,
	pr.Period,
	tn.Name Tenant,
	cou.LevelId,
	lv.Name AS LevelName
From Students stu 
inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId
inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
inner join Courses cou on cou.Id = couxenrxstu.CourseId
INNER JOIN dbo.Periods pr ON pr.Id = enrxstu.PeriodId 
LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId 
INNER JOIN dbo.Levels lv ON lv.Id = cou.LevelId 
WHERE stu.IsDeleted = 0 AND stu.IsActive = 1

GO

USE [EasySchoolManager_]
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] ON 

GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (16, 11, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 11, 1, N'School', N'School', N'TenantId', 1, 0, 0, N'api/services/app/tenant/GetAllTenantsForCombo', N'id', N'name', NULL, NULL, NULL, 1)
GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (17, 11, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 11, 2, N'Level', N'Level', N'levelId', 1, 0, 0, N'api/services/app/level/GetAllLevelsForCombo', N'id', N'name', NULL, NULL, NULL, 1)
GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (18, 11, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 11, 2, N'Course', N'Course', N'CourseId', 1, 0, 0, N'api/services/app/course/GetAllCoursesForCombo', N'id', N'name', NULL, NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] OFF
GO



----------------------------------------------------

USE [EasySchoolManager_]
GO
SET IDENTITY_INSERT [dbo].[Reports] ON 
GO
INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant], [IsForTenant]) VALUES (12, 3, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 4, N'02', N'Receipt', N'', N'Reports\', N'RdlcFiles\Financiero\Receipt.rdlc', 0, 0)
GO
SET IDENTITY_INSERT [dbo].[Reports] OFF
GO


-----------------------------------------------------
GO
SET IDENTITY_INSERT [dbo].[Reports] ON 

INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant], [IsForTenant]) VALUES (13, 3, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'05', N'AccountBalance', N'rep_vw_AccountBalance', N'Reports\', N'RdlcFiles\Financiero\AccountBalance.rdlc', 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Reports] OFF
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] ON 

GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (19, 13, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 13, 1, N'Enrollment', N'Enrollment', N'EnrollmentId', 1, 0, 0, NULL, NULL, NULL, NULL, NULL, N'To', 1)
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] OFF
GO

--------------------------------------------------

CREATE VIEW rep_vw_AccountBalance
AS
SELECT 
 er.Id AS EnrollmentId,
 er.FirstName,
 er.LastName,
 t.Date ,
 Number = tt.Abbreviation + '-' + CAST(t.Sequence AS VARCHAR),
 tt.Abbreviation,
 t.Sequence,
 ers.Sequence EnrollmentSequence,
 cp.Description AS ConceptDescription,
  t.OriginId,
  Debit = CASE t.OriginId WHEN 1 THEN t.Amount ELSE 0 END ,
  Credit = CASE t.OriginId WHEN 2 THEN t.Amount ELSE 0 END ,
  ts.TenantId 
  FROM TransactionStudents ts
  INNER JOIN dbo.Transactions t ON t.Id = ts.TransactionId 
  INNER JOIN dbo.TransactionTypes tt ON tt.Id = ts.TransactionTypeId 
  INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
  INNER JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
  INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = ts.TenantId  
  WHERE ts.IsDeleted = 0 AND t.IsDeleted = 0 AND t.IsActive = 1
  go

  -------------------------------------------------------------------------------------------
ALTER VIEW rep_vw_Receipt
  AS
SELECT
tn.Name AS TenatnName,
tn.Phone1,
t.Date ,
NCF = '',
Number = CAST(t.Sequence AS VARCHAR),
t.Name ,
t.Amount ,
cp.Description AS Concept,
t.Id AS TransactionId,
t.TenantId,
t.ConceptId,
ers.Sequence as EnrollmentSequence,
t.PaymentMethodId,
pm.Name PaymentMethod
FROM dbo.Transactions t
INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
INNER JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1
GO

SET IDENTITY_INSERT [dbo].[Reports] ON 


INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant], [IsForTenant]) VALUES (12, 3, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'04', N'Receipt', N'rep_vw_Receipt', N'Reports\', N'RdlcFiles\Financiero\Receipt.rdlc', 0, 0)
GO
INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant], [IsForTenant]) VALUES (13, 3, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'05', N'AccountBalance', N'rep_vw_AccountBalance', N'Reports\', N'RdlcFiles\Financiero\AccountBalance.rdlc', 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Reports] OFF
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] ON 


GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (19, 13, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 13, 1, N'Enrollment', N'Enrollment', N'EnrollmentSequence', 1, 0, 0, NULL, NULL, NULL, NULL, NULL, N'To', 1)
GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (21, 12, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 12, 1, N'TransactionId', N'Transaction', N'TransactionId', 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1)
GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (22, 12, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 12, 1, N'AmountText', N'AmountText', N'AmountText', 2, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] OFF
GO

------------------------------------------------------------------------------------

ALTER VIEW [dbo].[rep_vw_Receipt]
  AS
SELECT
tn.Name AS TenatnName,
tn.Phone1,
t.Date ,
NCF = 'A0018895154987984',
Number = CAST(t.Sequence AS VARCHAR),
t.Name ,
t.Amount ,
cp.Description AS Concept,
t.Id AS TransactionId,
t.TenantId,
t.ConceptId,
ers.Sequence as EnrollmentSequence,
t.PaymentMethodId,
pm.Name PaymentMethod,
st.FirstName + ' ' + st.LastName AS StudentName,
ts.EnrollmentStudentId ,
c.Abbreviation CourseName,
c.Id CourseId,
sec.Name SeccionName,
sec.Id SeccionId,
ts.Amount AS StudentAmount
FROM dbo.Transactions t
INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
INNER JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
INNER JOIN dbo.TransactionStudents ts ON ts.TransactionId = t.Id
INNER JOIN dbo.EnrollmentStudents erst ON erst.Id = ts.EnrollmentStudentId 
INNER JOIN dbo.Students st ON st.Id = erst.StudentId 
INNER JOIN dbo.CourseEnrollmentStudents crs ON crs.EnrollmentStudentId = ts.EnrollmentStudentId AND crs.TenantId = ts.TenantId
INNER JOIN dbo.Courses c ON c.Id = crs.CourseId 
LEFT JOIN dbo.CourseSessions sec ON sec.Id = crs.SessionId 
WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1

go

UPDATE dbo.Reports SET ReportPath = 'RdlcFiles\Financiero\ReceiptStudents.rdlc'
WHERE ReportCode = '04'


---------------------------2018/02/01--------------------------------------------------------------


CREATE VIEW [dbo].[rep_vw_Transactions]
  AS
SELECT
tn.Name AS TenantName,
tn.Phone1,
t.Date ,
NCF = '',
Number = CAST(t.Sequence AS VARCHAR),
t.Name ,
t.Amount ,
cp.Description AS Concept,
t.Id AS TransactionId,
t.TenantId,
t.ConceptId,
ers.Sequence as EnrollmentSequence,
Debit = CASE t.OriginId WHEN 1 THEN t.Amount ELSE 0 end,
Credit = CASE t.OriginId WHEN 2 THEN t.Amount ELSE 0 end
FROM dbo.Transactions t
INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
INNER JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 3
GO

SET IDENTITY_INSERT [dbo].[Reports] ON 

GO
INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant], [IsForTenant]) VALUES (15, 3, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'06', N'TransactionList', N'rep_vw_Transactions', N'Reports\', N'RdlcFiles\Financiero\TransactionList.rdlc', 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Reports] OFF
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] ON 

GO
UPDATE dbo.ReportsFilters SET UrlService = 'api/services/app/course/GetAllCoursesForCombo?levelId={{scope.levelId}}', DependencyField ='levelId' 
WHERE Id = 18
GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (24, 15, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 15, 1, N'Date', N'From', N'Date', 3, 1, 0, NULL, NULL, NULL, NULL, NULL, N'To', 1)
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] OFF
GO
----------------------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Reports] ON 

GO

INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant], [IsForTenant]) VALUES (14, 3, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'06', N'ReceiptsList', N'rep_vw_Receipts', N'Reports\', N'RdlcFiles\Financiero\ReceiptList.rdlc', 1, 1)
GO

SET IDENTITY_INSERT [dbo].[Reports] OFF
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] ON 

GO

GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (23, 14, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 14, 1, N'Date', N'From', N'Date', 3, 1, 0, NULL, NULL, NULL, NULL, NULL, N'To', 1)
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] OFF
GO


----------------------------------------------------------------------------------------------

CREATE VIEW [dbo].[rep_vw_Receipts]
  AS
SELECT
tn.Name AS TenantName,
tn.Phone1,
t.Date ,
NCF = '',
Number = CAST(t.Sequence AS VARCHAR),
t.Name ,
t.Amount ,
cp.Description AS Concept,
t.Id AS TransactionId,
t.TenantId,
t.ConceptId,
ers.Sequence as EnrollmentSequence,
t.PaymentMethodId,
pm.Name PaymentMethod
FROM dbo.Transactions t
INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
INNER JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1
GO


--------------2018-02-07--------------------------------------------------


CREATE VIEW [dbo].[rep_vw_Deposits]
  AS
SELECT
tn.Name AS TenantName,
tn.Phone1,
t.Date ,
NCF = '',
Number = CAST(t.Sequence AS VARCHAR),
t.Amount ,
cp.Description AS Concept,
t.Id AS TransactionId,
t.TenantId,
t.ConceptId,
t.BankAccountId ,
bck.BanktId ,
bck.Description AS AccountName ,
bck.Number AS AccountNumber,
bk.Name as BankName
FROM dbo.Transactions t
INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
INNER JOIN dbo.BankAccounts bck ON bck.Id = t.BankAccountId 
INNER JOIN dbo.Banks bk ON bk.Id = bck.BanktId 
INNER JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 2

GO

SET IDENTITY_INSERT [dbo].[Reports] ON
INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant], [IsForTenant]) VALUES (22, 3, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'08', N'DepositList', N'rep_vw_Deposits', N'Reports\', N'RdlcFiles\Financiero\DepositList.rdlc', 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Reports] OFF
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] ON 


INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (40, 14, 0, 1, 29, CAST(0x0000A8800159E511 AS DateTime), NULL, NULL, NULL, NULL, 0, 1, N'Concept', N'Concept', N'ConceptId', 1, 0, 0, N'api/services/app/concept/getAllConceptsForCombo', N'id', N'description', NULL, NULL, NULL, 0)
go
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (41, 15, 0, 1, 29, CAST(0x0000A8800159E511 AS DateTime), NULL, NULL, NULL, NULL, 0, 1, N'Concept', N'Concept', N'ConceptId', 1, 0, 0, N'api/services/app/concept/getAllConceptsForCombo', N'id', N'description', NULL, NULL, NULL, 0)


GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (31, 22, 0, 1, 29, CAST(0x0000A88001596527 AS DateTime), NULL, NULL, NULL, NULL, 0, 1, N'Date', N'From', N'Date', 3, 1, 0, NULL, NULL, NULL, NULL, NULL, N'To', 1)

GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (32, 22, 0, 1, 29, CAST(0x0000A8800159E511 AS DateTime), NULL, NULL, NULL, NULL, 0, 1, N'Concept', N'Concept', N'ConceptId', 1, 0, 0, N'api/services/app/concept/getAllConceptsForCombo', N'id', N'description', NULL, NULL, NULL, 0)
GO
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES (39, 22, 0, 1, 29, CAST(0x0000A880015BBC48 AS DateTime), NULL, NULL, NULL, NULL, 0, 3, N'Bank', N'Bank', N'BanktId', 1, 0, 0, N'api/services/app/bank/getAllBanksForCombo', N'id', N'name', NULL, NULL, NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[ReportsFilters] OFF
GO

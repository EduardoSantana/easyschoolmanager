

alter table EnrollmentStudents drop constraint "FK_dbo.EnrollmentStudents_dbo.Enrollments_EnrollmentId"
alter table EnrollmentStudents add constraint  "FK_dbo.EnrollmentStudents_dbo.Enrollments_EnrollmentId"  foreign key(EnrollmentId) references Enrollments(id) on update cascade 


alter table EnrollmentSequences drop constraint "FK_dbo.EnrollmentSequences_dbo.Enrollments_EnrollmentId"
alter table EnrollmentSequences add constraint  "FK_dbo.EnrollmentSequences_dbo.Enrollments_EnrollmentId"  foreign key(EnrollmentId) references Enrollments(id) on update cascade 


alter table Transactions drop constraint "FK_dbo.Transactions_dbo.Enrollments_EnrollmentId"
alter table Transactions add constraint  "FK_dbo.Transactions_dbo.Enrollments_EnrollmentId"  foreign key(EnrollmentId) references Enrollments(id) on update cascade 
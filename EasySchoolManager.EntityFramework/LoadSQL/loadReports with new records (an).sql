USE [EasySchoolManager_]
GO
/****** Object:  Table [dbo].[Reports]    Script Date: 1/3/2018 10:30:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Reports](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](80) NOT NULL,
	[Descripcion] [nvarchar](120) NULL,
	[Vista] [nvarchar](50) NULL,
	[Archivo] [nvarchar](50) NULL,
	[Url] [nvarchar](120) NULL,
	[ResourceFile] [nvarchar](50) NULL,
	[Tabla] [nvarchar](100) NULL,
	[ReportsTypesId] [int] NOT NULL,
	[appID] [int] NULL,
	[CustomConsulta] [nvarchar](max) NULL,
	[textoDetalleDescripcion] [nvarchar](2000) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
	[Consulta]  AS ([dbo].[fn_Campos]([Tabla])),
 CONSTRAINT [PK_dbo.Reports] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReportsFilters]    Script Date: 1/3/2018 10:30:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportsFilters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReportsId] [int] NOT NULL,
	[Tipo] [nvarchar](10) NOT NULL,
	[NombreCampo] [nvarchar](50) NOT NULL,
	[NombreVariable] [nvarchar](50) NOT NULL,
	[NombreVariableHasta] [nvarchar](50) NULL,
	[consulta] [nvarchar](1000) NULL,
	[NombreVisual] [nvarchar](50) NULL,
	[Orden] [int] NULL,
	[Placeholder] [nvarchar](50) NULL,
	[PlaceholderHasta] [nvarchar](50) NULL,
	[IsHidden] [bit] NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
	[tieneConsultas]  AS ([dbo].[fn_GetTieneConsultas]([Id])),
 CONSTRAINT [PK_dbo.ReportsFilters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportsQueries]    Script Date: 1/3/2018 10:30:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportsQueries](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ValueId] [nvarchar](20) NOT NULL,
	[ValueDisplay] [nvarchar](40) NOT NULL,
	[ReportsFiltersId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
 CONSTRAINT [PK_dbo.ReportsQueries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportsTypes]    Script Date: 1/3/2018 10:30:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportsTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](120) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatorUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModifierUserId] [bigint] NULL,
	[LastModificationTime] [datetime] NULL,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
 CONSTRAINT [PK_dbo.ReportsTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Reports] ON 

INSERT [dbo].[Reports] ([Id], [Nombre], [Descripcion], [Vista], [Archivo], [Url], [ResourceFile], [Tabla], [ReportsTypesId], [appID], [CustomConsulta], [textoDetalleDescripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (1, N'Reporte de Alergias', N'Reporte de Alergias', N'DataSet1', N'Report2Fields.rdlc', N'RdlcFiles/', N'Allergies', N'Allergies', 1, NULL, N'select id Col1, Name Col2 from Allergies', N'Reporte de alergias', 0, 1, 1, CAST(0x0000A846015AB47D AS DateTime), 1, CAST(0x0000A85D01684C3E AS DateTime), NULL, NULL)
INSERT [dbo].[Reports] ([Id], [Nombre], [Descripcion], [Vista], [Archivo], [Url], [ResourceFile], [Tabla], [ReportsTypesId], [appID], [CustomConsulta], [textoDetalleDescripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (2, N'Listado Estudiantes Por Curso', N'Listado Estudiantes Por Curso', N'DataSet1', N'Report2FieldsListStudent.rdlc', N'RdlcFiles/', N'EstXCur', N'rep_view_ListadoEstudianteXCursos', 2, NULL, NULL, N'Listado de Estuantes por Colegios', 0, 1, 1, CAST(0x0000A84F016A2273 AS DateTime), 1, CAST(0x0000A84F0177F04F AS DateTime), NULL, NULL)
INSERT [dbo].[Reports] ([Id], [Nombre], [Descripcion], [Vista], [Archivo], [Url], [ResourceFile], [Tabla], [ReportsTypesId], [appID], [CustomConsulta], [textoDetalleDescripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (3, N'Listado de Matriculas', N'Listado de Matriculas', N'DataSet1', N'Report2FieldsListEnrollment.rdlc', N'RdlcFiles/', N'ListaMaticula', N'rep_view_ListadoEstudianteXCursos', 2, NULL, NULL, N'Este es un listado de matriculas', 0, 1, 1, CAST(0x0000A84F017B5AC8 AS DateTime), 1, CAST(0x0000A84F017B9F95 AS DateTime), NULL, NULL)
INSERT [dbo].[Reports] ([Id], [Nombre], [Descripcion], [Vista], [Archivo], [Url], [ResourceFile], [Tabla], [ReportsTypesId], [appID], [CustomConsulta], [textoDetalleDescripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (4, N'Listado de Recibos', N'Listado de Recibos', N'DataSet1', N'Report2FieldsListTrans.rdlc', N'RdlcFiles/', N'ListaRecibo', N'rep_view_ListadoRecibos', 3, NULL, NULL, N'Listado de recibos, este es un listado.', 0, 1, 1, CAST(0x0000A84F01889BE0 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Reports] ([Id], [Nombre], [Descripcion], [Vista], [Archivo], [Url], [ResourceFile], [Tabla], [ReportsTypesId], [appID], [CustomConsulta], [textoDetalleDescripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (5, N'Listado Usuarios', N'Listado Usuarios', N'DataSet1', N'Report2FieldsListUsers.rdlc', N'RdlcFiles/', N'ListaUsuarios', N'rep_viewListadoUsuarios', 1, NULL, NULL, N'Descripcion del reporte', 0, 1, 1, CAST(0x0000A85401297BDE AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Reports] ([Id], [Nombre], [Descripcion], [Vista], [Archivo], [Url], [ResourceFile], [Tabla], [ReportsTypesId], [appID], [CustomConsulta], [textoDetalleDescripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (6, N'Usuarios de Colegio', N'Usuarios de Colegio', N'DataSet1', N'Report2FieldsListUsers.rdlc', N'RdlcFiles/', N'ListaUsuarios', N'rep_viewListadoUsuarios', 1, NULL, NULL, N'Descripcion del reporte', 0, 1, 1, CAST(0x0000A85401297BDE AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Reports] ([Id], [Nombre], [Descripcion], [Vista], [Archivo], [Url], [ResourceFile], [Tabla], [ReportsTypesId], [appID], [CustomConsulta], [textoDetalleDescripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (7, N'Listado de Depositos', N'Listado de Depositos', N'DataSet1', N'Report2FieldsListTransDeposit.rdlc', N'RdlcFiles/', N'ListaDeposito', N'rep_view_ListadoDepositos', 3, NULL, NULL, N'Listado de recibos, este es un listado.', 0, 1, 1, CAST(0x0000A84F01889BE0 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Reports] ([Id], [Nombre], [Descripcion], [Vista], [Archivo], [Url], [ResourceFile], [Tabla], [ReportsTypesId], [appID], [CustomConsulta], [textoDetalleDescripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (8, N'Detalle Matricula', N'Detalle Matricula', N'DataSet1', N'Report2FieldsEnrolmentDet.rdlc', N'RdlcFiles/', N'EnrolmentDet', N'rep_view_ReporteEnrolmentsDet', 2, NULL, NULL, N'Detalle o fiecha de matricula', 0, 1, 1, CAST(0x0000A84F01889BE0 AS DateTime), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Reports] OFF
SET IDENTITY_INSERT [dbo].[ReportsFilters] ON 

INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [Tipo], [NombreCampo], [NombreVariable], [NombreVariableHasta], [consulta], [NombreVisual], [Orden], [Placeholder], [PlaceholderHasta], [IsHidden], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (1, 2, N'int', N'CourseId', N'CourseId', N'', NULL, N'Curso', 10, N'Curso', NULL, NULL, 0, 1, 1, CAST(0x0000A84F0176154B AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [Tipo], [NombreCampo], [NombreVariable], [NombreVariableHasta], [consulta], [NombreVisual], [Orden], [Placeholder], [PlaceholderHasta], [IsHidden], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (2, 2, N'format', N'Formato', N'Formato', N'', NULL, N'Formato', 5, N'Formato', NULL, NULL, 0, 1, 1, CAST(0x0000A84F0176EC74 AS DateTime), 1, CAST(0x0000A84F0176FDF2 AS DateTime), NULL, NULL)
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [Tipo], [NombreCampo], [NombreVariable], [NombreVariableHasta], [consulta], [NombreVisual], [Orden], [Placeholder], [PlaceholderHasta], [IsHidden], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (3, 4, N'datetime', N'TransDate', N'TransDate', N'FechaHasta', NULL, N'Fecha', 10, N'Fecha', N'Hasta', NULL, 0, 1, 1, CAST(0x0000A84F0188F279 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [Tipo], [NombreCampo], [NombreVariable], [NombreVariableHasta], [consulta], [NombreVisual], [Orden], [Placeholder], [PlaceholderHasta], [IsHidden], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (4, 3, N'int', N'TenantId', N'TenantId', N'', NULL, N'TenantId', 10, N'TenantId', NULL, 1, 0, 1, 1, CAST(0x0000A85001568EF5 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [Tipo], [NombreCampo], [NombreVariable], [NombreVariableHasta], [consulta], [NombreVisual], [Orden], [Placeholder], [PlaceholderHasta], [IsHidden], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (6, 6, N'int', N'TenantId', N'TenantId', N'', NULL, N'Colegio', 10, N'Colegio', NULL, NULL, 0, 1, 1, CAST(0x0000A83C00000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [Tipo], [NombreCampo], [NombreVariable], [NombreVariableHasta], [consulta], [NombreVisual], [Orden], [Placeholder], [PlaceholderHasta], [IsHidden], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (7, 7, N'datetime', N'TransDate', N'TransDate', N'FechaHasta', NULL, N'Fecha', 10, N'Fecha', N'Hasta', NULL, 0, 1, 1, CAST(0x0000A84F0188F279 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [Tipo], [NombreCampo], [NombreVariable], [NombreVariableHasta], [consulta], [NombreVisual], [Orden], [Placeholder], [PlaceholderHasta], [IsHidden], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (8, 8, N'int', N'MatriculaId', N'MatriculaId', N'', NULL, N'Matricula', 10, N'Matricula', NULL, NULL, 0, 1, 1, CAST(0x0000A84F0176154B AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [Tipo], [NombreCampo], [NombreVariable], [NombreVariableHasta], [consulta], [NombreVisual], [Orden], [Placeholder], [PlaceholderHasta], [IsHidden], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (9, 8, N'int', N'TenantId', N'TenantId', N'', NULL, N'TenantId', 10, N'TenantId', NULL, 1, 0, 1, 1, CAST(0x0000A85001568EF5 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsFilters] ([Id], [ReportsId], [Tipo], [NombreCampo], [NombreVariable], [NombreVariableHasta], [consulta], [NombreVisual], [Orden], [Placeholder], [PlaceholderHasta], [IsHidden], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (10, 1, N'format', N'FORMATO', N'PDF', N'', NULL, N'FORMATO', 1, N'FORMATO', NULL, NULL, 0, 1, 1, CAST(0x0000A85D016AD7E7 AS DateTime), 1, CAST(0x0000A85D016B3031 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[ReportsFilters] OFF
SET IDENTITY_INSERT [dbo].[ReportsQueries] ON 

INSERT [dbo].[ReportsQueries] ([Id], [ValueId], [ValueDisplay], [ReportsFiltersId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (1, N'1', N'Primero de Primaria', 1, 0, 1, 1, CAST(0x0000A84F017654B0 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsQueries] ([Id], [ValueId], [ValueDisplay], [ReportsFiltersId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (2, N'2', N'Segundo de Primaria', 1, 0, 1, 1, CAST(0x0000A84F017660F7 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsQueries] ([Id], [ValueId], [ValueDisplay], [ReportsFiltersId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (3, N'pdf', N'PDF', 2, 0, 1, 1, CAST(0x0000A84F01771268 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsQueries] ([Id], [ValueId], [ValueDisplay], [ReportsFiltersId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (4, N'excel', N'EXCEL', 2, 0, 1, 1, CAST(0x0000A84F01771D70 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsQueries] ([Id], [ValueId], [ValueDisplay], [ReportsFiltersId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (7, N'1', N'Colegio 1', 6, 0, 1, 1, CAST(0x0000A84F00000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsQueries] ([Id], [ValueId], [ValueDisplay], [ReportsFiltersId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (8, N'2', N'Colegio 2', 6, 0, 1, 1, CAST(0x0000A84F00000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsQueries] ([Id], [ValueId], [ValueDisplay], [ReportsFiltersId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (9, N'pdf', N'PDF', 10, 0, 1, 1, CAST(0x0000A85D016FBE46 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsQueries] ([Id], [ValueId], [ValueDisplay], [ReportsFiltersId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (10, N'excel', N'EXCEL', 10, 0, 1, 1, CAST(0x0000A85D016FCDE0 AS DateTime), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ReportsQueries] OFF
SET IDENTITY_INSERT [dbo].[ReportsTypes] ON 

INSERT [dbo].[ReportsTypes] ([Id], [Nombre], [Descripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (1, N'Generales', NULL, 0, 1, 1, CAST(0x0000A8590175143D AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsTypes] ([Id], [Nombre], [Descripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (2, N'Académicos', NULL, 0, 1, 1, CAST(0x0000A859017514A4 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsTypes] ([Id], [Nombre], [Descripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (3, N'Financieros', NULL, 0, 1, 1, CAST(0x0000A859017514A9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[ReportsTypes] ([Id], [Nombre], [Descripcion], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (4, N'Administrativos', NULL, 0, 1, 1, CAST(0x0000A859017514AE AS DateTime), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ReportsTypes] OFF
ALTER TABLE [dbo].[Reports]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Reports_dbo.ReportsTypes_ReportsTypesId] FOREIGN KEY([ReportsTypesId])
REFERENCES [dbo].[ReportsTypes] ([Id])
GO
ALTER TABLE [dbo].[Reports] CHECK CONSTRAINT [FK_dbo.Reports_dbo.ReportsTypes_ReportsTypesId]
GO
ALTER TABLE [dbo].[ReportsFilters]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReportsFilters_dbo.Reports_ReportsId] FOREIGN KEY([ReportsId])
REFERENCES [dbo].[Reports] ([Id])
GO
ALTER TABLE [dbo].[ReportsFilters] CHECK CONSTRAINT [FK_dbo.ReportsFilters_dbo.Reports_ReportsId]
GO
ALTER TABLE [dbo].[ReportsQueries]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReportsQueries_dbo.ReportsFilters_ReportsFiltersId] FOREIGN KEY([ReportsFiltersId])
REFERENCES [dbo].[ReportsFilters] ([Id])
GO
ALTER TABLE [dbo].[ReportsQueries] CHECK CONSTRAINT [FK_dbo.ReportsQueries_dbo.ReportsFilters_ReportsFiltersId]
GO

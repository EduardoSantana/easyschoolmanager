namespace EasySchoolManager.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class DropTables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Concepts", "T10DimensionId", "dbo.T10Dimensions");
            DropForeignKey("dbo.Concepts", "T1DimensionId", "dbo.T1Dimensions");
            DropForeignKey("dbo.Concepts", "T2DimensionId", "dbo.T2Dimensions");
            DropForeignKey("dbo.Concepts", "T3DimensionId", "dbo.T3Dimensions");
            DropForeignKey("dbo.Concepts", "T4DimensionId", "dbo.T4Dimensions");
            DropForeignKey("dbo.Concepts", "T5DimensionId", "dbo.T5Dimensions");
            DropForeignKey("dbo.Concepts", "T6DimensionId", "dbo.T6Dimensions");
            DropForeignKey("dbo.Concepts", "T7DimensionId", "dbo.T7Dimensions");
            DropForeignKey("dbo.Concepts", "T8DimensionId", "dbo.T8Dimensions");
            DropForeignKey("dbo.Concepts", "T9DimensionId", "dbo.T9Dimensions");
            DropIndex("dbo.Concepts", new[] { "T1DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T2DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T3DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T4DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T5DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T6DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T7DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T8DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T9DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T10DimensionId" });
            DropColumn("dbo.Concepts", "T1DimensionId");
            DropColumn("dbo.Concepts", "T2DimensionId");
            DropColumn("dbo.Concepts", "T3DimensionId");
            DropColumn("dbo.Concepts", "T4DimensionId");
            DropColumn("dbo.Concepts", "T5DimensionId");
            DropColumn("dbo.Concepts", "T6DimensionId");
            DropColumn("dbo.Concepts", "T7DimensionId");
            DropColumn("dbo.Concepts", "T8DimensionId");
            DropColumn("dbo.Concepts", "T9DimensionId");
            DropColumn("dbo.Concepts", "T10DimensionId");
            DropTable("dbo.T10Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T10Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T1Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T1Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T2Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T2Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T3Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T3Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T4Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T4Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T5Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T5Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T6Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T6Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T7Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T7Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T8Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T8Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T9Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T9Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.T9Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T9Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T8Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T8Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T7Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T7Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T6Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T6Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T5Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T5Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T4Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T4Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T3Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T3Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T2Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T2Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T1Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T1Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T10Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T10Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Concepts", "T10DimensionId", c => c.Int());
            AddColumn("dbo.Concepts", "T9DimensionId", c => c.Int());
            AddColumn("dbo.Concepts", "T8DimensionId", c => c.Int());
            AddColumn("dbo.Concepts", "T7DimensionId", c => c.Int());
            AddColumn("dbo.Concepts", "T6DimensionId", c => c.Int());
            AddColumn("dbo.Concepts", "T5DimensionId", c => c.Int());
            AddColumn("dbo.Concepts", "T4DimensionId", c => c.Int());
            AddColumn("dbo.Concepts", "T3DimensionId", c => c.Int());
            AddColumn("dbo.Concepts", "T2DimensionId", c => c.Int());
            AddColumn("dbo.Concepts", "T1DimensionId", c => c.Int());
            CreateIndex("dbo.Concepts", "T10DimensionId");
            CreateIndex("dbo.Concepts", "T9DimensionId");
            CreateIndex("dbo.Concepts", "T8DimensionId");
            CreateIndex("dbo.Concepts", "T7DimensionId");
            CreateIndex("dbo.Concepts", "T6DimensionId");
            CreateIndex("dbo.Concepts", "T5DimensionId");
            CreateIndex("dbo.Concepts", "T4DimensionId");
            CreateIndex("dbo.Concepts", "T3DimensionId");
            CreateIndex("dbo.Concepts", "T2DimensionId");
            CreateIndex("dbo.Concepts", "T1DimensionId");
            AddForeignKey("dbo.Concepts", "T9DimensionId", "dbo.T9Dimensions", "Id");
            AddForeignKey("dbo.Concepts", "T8DimensionId", "dbo.T8Dimensions", "Id");
            AddForeignKey("dbo.Concepts", "T7DimensionId", "dbo.T7Dimensions", "Id");
            AddForeignKey("dbo.Concepts", "T6DimensionId", "dbo.T6Dimensions", "Id");
            AddForeignKey("dbo.Concepts", "T5DimensionId", "dbo.T5Dimensions", "Id");
            AddForeignKey("dbo.Concepts", "T4DimensionId", "dbo.T4Dimensions", "Id");
            AddForeignKey("dbo.Concepts", "T3DimensionId", "dbo.T3Dimensions", "Id");
            AddForeignKey("dbo.Concepts", "T2DimensionId", "dbo.T2Dimensions", "Id");
            AddForeignKey("dbo.Concepts", "T1DimensionId", "dbo.T1Dimensions", "Id");
            AddForeignKey("dbo.Concepts", "T10DimensionId", "dbo.T10Dimensions", "Id");
        }
    }
}

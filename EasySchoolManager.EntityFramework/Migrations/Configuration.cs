using System.Data.Entity.Migrations;
using Abp.MultiTenancy;
using Abp.Zero.EntityFramework;
using EasySchoolManager.Migrations.SeedData;
using EntityFramework.DynamicFilters;
using System.Linq;
using System;
using System.Linq.Expressions;
using EasySchoolManager.Models;
using EasySchoolManager.EntityFramework;
using EasySchoolManager.Migrations.Delta;

namespace EasySchoolManager.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<EasySchoolManager.EntityFramework.EasySchoolManagerDbContext>, IMultiTenantSeed
    {
        public AbpTenantBase Tenant { get; set; }

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "EasySchoolManager";
        }

        protected override void Seed(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context)
        {
            //if (System.Diagnostics.Debugger.IsAttached == false)
            //    System.Diagnostics.Debugger.Launch();

            MethodSeeds(context);
            context.DisableAllFilters();

            if (Tenant == null)
            {

                //Host seed
                new InitialHostDbBuilder(context).Create();

                //Default tenant seed (in host database).
                new DefaultTenantCreator(context).Create();
                //new TenantRoleAndUserBuilder(context, 1).Create();


                var GedDataGeneral = new DefaultGenerals(context);

                //Creando Tipos de usuarios
                GedDataGeneral.CreateUserTypeSeed();

                //Creating the Genders
                GedDataGeneral.CreateGenderSeed();

                //creando los metodos de pago
                GedDataGeneral.CreatePaymentMethodSeed();

                //creando los origenes, d�bito y cr�dito
                GedDataGeneral.CreateOriginSeed();


                //creando los tipos de sangre
                GedDataGeneral.CreateBloodSeed();

                //creando los status
                GedDataGeneral.CreateStatusSeed();

                //Creando los transaction types
                GedDataGeneral.CreateTransactionTypeSeed();

            }
            else
            {
                //You can add seed for tenant databases and use Tenant property...
            }

            new SeedTables(context).InsertOthers();
            context.SaveChanges();
            new CreateAllViews(context).Create_all_views();
            context.SaveChanges();
            new SeedTables(context).All();

            context.SaveChanges();

        }

        public void RunSeed(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context)
        {
            this.Seed(context);
        }

        public void MethodSeeds(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context)
        {
            ButtonPositionsSeed(context);

            // Comentarlos despues de subir correctamente
            CreatePeriodsFromEnum(context);
            EvaluationLegend(context);
            EvaluationPeriod(context);
            TiposComprobantesFiscales(context);
            CancelStatusInsert(context);
        }

        private void CreatePeriodsFromEnum(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context)
        {
            foreach (PeriodsEnum item in (PeriodsEnum[])Enum.GetValues(typeof(PeriodsEnum)))
            {
                if (!context.Periods.Any(p => p.Id == (int)item))
                {
                    try
                    {
                        InsertRecord(context, new Periods() { StartDate = DateTime.Now, EndDate = DateTime.Now.AddMonths(12), Id = (int)item, Description = $"{item}", Period = DateTime.Now.Year.ToString() });
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                    {
                        MigrationLog.Add("Enter001");
                        var i = 0;
                        foreach (var v in ex.EntityValidationErrors)
                        {
                            
                            MigrationLog.Add($"Line[{i}]: IsValid:{v.IsValid}, Entry: {v.Entry}, ");

                            foreach (var ve in v.ValidationErrors)
                            {
                                MigrationLog.Add($"PropertyName: {ve.PropertyName}, ErrorMessage: {ve.ErrorMessage}");

                            }
                            i++;
                        }
                      
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        
                        MigrationLog.Add(ex.Message);
                        throw ex;
                    }
                  
                }
            }

        }

        private void ButtonPositionsSeed(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context)
        {
            for (int i = 1; i < 46; i++)
            {
                InsertRecord(context, new ButtonPositions() { Id = i, Number = i.ToString() });
            }
        }

        private void CancelStatusInsert(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context)
        {
            InsertRecord(context, new CancelStatus() { Id = 1, Description = "Cancelled" });
            InsertRecord(context, new CancelStatus() { Id = 2, Description = "Authorized" });
        }

        private void TiposComprobantesFiscales(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context)
        {
            InsertRecord(context, new TaxReceiptTypes() { Id = 1, Name = "Tax Credit Invoice", Code = "01" });
            InsertRecord(context, new TaxReceiptTypes() { Id = 2, Name = "Consumer Invoice", Code = "02" });
            InsertRecord(context, new TaxReceiptTypes() { Id = 3, Name = "Debit Notes", Code = "03" });
            InsertRecord(context, new TaxReceiptTypes() { Id = 4, Name = "Credit notes", Code = "04" });
            InsertRecord(context, new TaxReceiptTypes() { Id = 5, Name = "Informal Suppliers Registry", Code = "11" });
            InsertRecord(context, new TaxReceiptTypes() { Id = 6, Name = "Single Income Record", Code = "12" });
            InsertRecord(context, new TaxReceiptTypes() { Id = 7, Name = "Minor Expense Record", Code = "13" });
            InsertRecord(context, new TaxReceiptTypes() { Id = 8, Name = "Special Tax Regimes", Code = "14" });
            InsertRecord(context, new TaxReceiptTypes() { Id = 9, Name = "Government Vouchers", Code = "15" });

        }

        private void EvaluationLegend(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context)
        {
            InsertRecord(context, new EvaluationLegends() { Id = 1, Name = "I", Description = "Initiated", Sequence = 2 });
            InsertRecord(context, new EvaluationLegends() { Id = 2, Name = "L", Description = "Accomplished", Sequence = 4 });
            InsertRecord(context, new EvaluationLegends() { Id = 3, Name = "P", Description = "In process", Sequence = 3 });
            InsertRecord(context, new EvaluationLegends() { Id = 4, Name = "/", Description = "Not worked", Sequence = 1 });
        }

        private void EvaluationPeriod(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context)
        {
            InsertRecord(context, new EvaluationPeriods() { Id = 1, Name = "1�", Position = 1, InitialMonth = 7, FinalMonth = 9, PeriodId = 1 });
            InsertRecord(context, new EvaluationPeriods() { Id = 2, Name = "2�", Position = 2, InitialMonth = 10, FinalMonth = 12, PeriodId = 1 });
            InsertRecord(context, new EvaluationPeriods() { Id = 3, Name = "3�", Position = 3, InitialMonth = 1, FinalMonth = 3, PeriodId = 1 });
            InsertRecord(context, new EvaluationPeriods() { Id = 4, Name = "4�", Position = 4, InitialMonth = 4, FinalMonth = 5, PeriodId = 1 });
            InsertRecord(context, new EvaluationPeriods() { Id = 5, Name = "RP", Position = 5, InitialMonth = 6, FinalMonth = 6, PeriodId = 1 });
        }

        /// <summary>
        /// This method es used to get a entity vaule from a antity searched by a Entity Field Name and Value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="ob"></param>
        /// <param name="EntityFieldName"></param>
        /// <param name="EntityFieldValue"></param>
        /// <returns></returns>
        public object GetPropertyValueFromEntityByEntityField<T>(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context, 
            T ob, String PropertyNameToGetValue, String EntityFieldName, String EntityFieldValue) where T : class
        {
            object record = null;
            
            try
            {
                var parameter = Expression.Parameter(typeof(T), "t");
                var _propertyName = typeof(T).GetProperty(EntityFieldName);
                var member = Expression.PropertyOrField(parameter, _propertyName.Name);
                var _value = Expression.Constant(_propertyName.GetValue(ob), _propertyName.PropertyType);
                var _filter = Expression.Lambda<Func<T, bool>>
                    (Expression.Equal(member, _value), new ParameterExpression[] { parameter });
                var filterSelect = Expression.Lambda<Func<T, int>>(Expression.PropertyOrField(parameter, PropertyNameToGetValue), new ParameterExpression[] { parameter });
                record = context.Set<T>().Where(_filter).FirstOrDefault();
            }
            catch (Exception err)
            {
            }

            return record;

        }

        /// <summary>
        /// This metod is used to insert or edit a recort by using the corresponding entity and its parameters
        /// </summary>
        /// <typeparam name="T">Entity Type</typeparam>
        /// <param name="context">Data context</param>
        /// <param name="ob"> Instance of Entity Type</param>
        /// <param name="CodeProperty"> Property used to find records by Code</param>
        public void InsertRecord<T>(EasySchoolManager.EntityFramework.EasySchoolManagerDbContext context, T ob, string CodeProperty = "Code") where T : class
        {
            var entry = context.Entry(ob);

            int TenantId = Tenant == null ? 2 : Tenant.Id;
            #region process
            var id = typeof(T).GetProperty("Id");
            var code = typeof(T).GetProperty(CodeProperty);
            var idx = code ?? id;
            var parameter = Expression.Parameter(typeof(T), "t");
            var member = Expression.PropertyOrField(parameter, idx.Name);
            var _value = Expression.Constant(idx.GetValue(ob), idx.PropertyType);
            var filter = Expression.Lambda<Func<T, bool>>(Expression.Equal(member, _value), new ParameterExpression[] { parameter });

            var tenantId = typeof(T).GetProperty("TenantId");
            var CreationTime = typeof(T).GetProperty("CreationTime");
            var CreatorUserId = typeof(T).GetProperty("CreatorUserId");
            var LastModificationTime = typeof(T).GetProperty("LastModificationTime");
            var LastModifierUserId = typeof(T).GetProperty("LastModifierUserId");
            var IsActive = typeof(T).GetProperty("IsActive");
            #endregion  
            //Hay casos donde devuelve cero el tenantId
            TenantId = TenantId == 0 ? 2 : TenantId;

            object record = 0;
            if (id.PropertyType == typeof(Int64))
            {
                var filterSelect = Expression.Lambda<Func<T, long>>(Expression.PropertyOrField(parameter, "Id"), new ParameterExpression[] { parameter });
                record = Convert.ToInt64(context.Set<T>().Where(filter).Select(filterSelect).DefaultIfEmpty(-1L).FirstOrDefault());
            }
            else
            {
                var filterSelect = Expression.Lambda<Func<T, int>>(Expression.PropertyOrField(parameter, "Id"), new ParameterExpression[] { parameter });
                record = Convert.ToInt32(context.Set<T>().Where(filter).Select(filterSelect).DefaultIfEmpty(-1).FirstOrDefault());
            }

            IsActive.SetValue(ob, true);
            CreatorUserId.SetValue(ob, 1L);

            if (tenantId != null)
                tenantId.SetValue(ob, TenantId);
            if (Convert.ToInt64(record) == -1)
            {
                CreationTime.SetValue(ob, DateTime.Now);

                entry.State = System.Data.Entity.EntityState.Added;
            }
            else
            {
                var selectDateFilter = Expression.Lambda<Func<T, DateTime>>(Expression.PropertyOrField(parameter, CreationTime.Name), new ParameterExpression[] { parameter });
                var currentCreationDate = (DateTime)context.Set<T>().Where(filter).Select(selectDateFilter).FirstOrDefault();
                CreationTime.SetValue(ob, currentCreationDate);
                LastModificationTime.SetValue(ob, DateTime.Now);
                LastModifierUserId.SetValue(ob, 1L);
                id.SetValue(ob, record);
                entry.State = System.Data.Entity.EntityState.Modified;
            }

            context.SaveChanges();
        }
    }
}

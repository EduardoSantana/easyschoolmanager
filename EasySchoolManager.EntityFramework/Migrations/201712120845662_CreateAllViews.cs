﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Migrations
{
	class CreateAllViews : DbMigration
	{
		private EntityFramework.EasySchoolManagerDbContext context;

		public CreateAllViews(EntityFramework.EasySchoolManagerDbContext context)
		{
			this.context = context;
		}

		public String DeleteViewString(String ViewName)
		{
			return string.Format(@" if (exists(select name from sys.objects where name = '{0}'))
				begin drop view {0} end  ", ViewName);
		}

		public String DeleteFunctionString(String FunctionName)
		{
			return string.Format(@" if (exists(select name from sys.objects where name = '{0}'))
				begin drop function {0} end  ", FunctionName);
		}

		public String DeleteProcedureString(String ProcedureName)
		{
			return string.Format(@" if (exists(select name from sys.objects where name = '{0}'))
				begin drop procedure {0} end  ", ProcedureName);
		}

		public override void Up()
		{

		}

		/// <summary>
		/// Method to call all the methods made to create the views
		/// </summary>
		public void Create_all_views()
		{
            InsertingReportStudentByCourseBySessionWB();
            ViewFor_ListadoEstudianteXCursos();
            ViewFor_rep_vw_Receipt();
            ViewFor_VW_Proyection();
            ViewFor_ListadoDepositos();
            ViewFor_ListadoRecibosPorConcepto();
            ViewFor_ListadoMontosoPorCuentas();
            InsertReportGeneralReceiptInDatabaseByCode();
            InsertReportArticlesReceiptInDatabaseByCode();
            ViewFor_rep_vw_ReceiptGeneral();
            ViewFor_rep_vw_ReceiptArticles();
            ViewFor_ListadoRecibosPorConceptoTrans();
            ViewFor_PaymentProjections();
            ViewFor_EnrollmentInscription();
            ViewFor_rep_vw_ReceiptDeposit();
            InsertingMenuForReceiptList();
            InsertingReportCalifications();
            ViewFor_Evaluations();
            ViewFor_Evaluations_Report();
            InsertingReportStudentEvaluationHighs();
            ViewFor_rep_view_EvaluationHighs();
            FunctionFor_rep_fn_getOneEvaluationHigh();
            FunctionFor_rep_fn_getOneEvaluationHighName();
            FunctionFor_rep_fn_getOneEvaluationHighByPosition();
            FunctionFor_rep_fn_getOneEvaluationHighByPositionName();
            InsertingReportReporteFinaldeCurso();
            ViewFor_rep_vw_AccountBalance();
            ViewFor_rep_vw_StudentWithNotProyect();
            InsertingReportStudentWithNotProyect();
            ViewFor_rep_view_EvaluationFinalReport();
            InsertingReportStudentEvaluationHighsByStudent();
            InsertingReportStudentEvaluationHighsBySubject();
            ViewFor_rep_view_EvaluationHighsByStudent();
            ProcedureFor_sp_rep_GetCxCReportTutor();
            ProcedureForProjectionAmountByEnrollment();
            InsertingReportEnrollmentBalanceListToDate();
            InsertingReportEnrollmentBalanceOne();
            InsertingProjectionReport();
            InsertingReceipForSecondImpresion();
            InsertingReportEnrollmentBalanceListByStudent();
            ProcedureFor_rep_AccountBalanceListByStudent();
            UpdateReportsPermissionName();
            InsertingReportStudentByCourseBySession();
            ProcedureFor_rep_StudentByCourseBySession();
            InsertingReportReceiptShops();
            ViewFor_rep_view_ListReciptShops();
            InsertingReportReceiptShopsList();
            ViewFor_rep_view_ListReciptShopsList();
            ViewFor_rep_vw_StudentByCourse();
            ViewFor_rep_vw_Receipts();
            ViewFor_rep_view_EnrollmentPaymentStudent();
            InsertingReportEnrollmentPaymentStudent();
            ViewFor_rep_view_CanceledTrasactions();
            InsertingReportCanceledTrasactions();
            InsertingReportStudentsTutores();
            InsertingReportEnrollmentBalanceListByEnrollment();
            ProcedureFor_rep_AccountBalanceListByEnrolment();
            InsertingReportEnrollmentBalanceListByEnrollmentLess();
            ProcedureFor_rep_AccountBalanceListByEnrolmentLess();
            ProceduraFor_sp_rep_ProjectionRate();
            InsertingReportEnrollmentBalanceListByEnrollmentMore();
            ProcedureFor_rep_AccountBalanceListByEnrolmentMore();
            InsertingReportVerifoneBreaksDetails();
            ViewFor_rep_view_VerifoneBreaksDetails();
            DefaultValueForDateInputs();
            CreateView_Projection_Rate();
            Create_Sp_Projection_Rate();
            ProceduraFor_sp_rep_GetCxCReportTutorActual();
        }

		/// <summary>
		/// Method to create a view for ListadoEstudianteXCursos
		/// </summary>
		public void ViewFor_ListadoEstudianteXCursos()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_ListadoEstudianteXCursos"));
			this.context.Database.ExecuteSqlCommand(@"
				
			CREATE view [dbo].[rep_view_ListadoEstudianteXCursos]
			as
			Select '' ReporteDePrueba,
			ROW_NUMBER () over(order by stu.id asc) as Numero, 
			stu.LastName,
			stu.FirstName,
			enrxsec.Sequence,
			enr.FirstName + ' ' + enr.LastName as FullName,
			cou.Name,
			enrxstu.TenantId,
			stu.Id as StudentId,
			cou.Id as CourseId,
			enr.Id as EnrollmentId
			From Students stu 
			inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
			inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId
			inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
			inner join CourseEnrollmentStudents couxenrxstu 
				on enrxstu.Id = couxenrxstu.EnrollmentStudentId
				inner join Courses cou on cou.Id = couxenrxstu.CourseId
			
			");
		}

		public void ViewFor_ListadoDepositos()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_Deposits"));
			this.context.Database.ExecuteSqlCommand(@"
			CREATE view [dbo].[rep_view_Deposits] as 
			Select b.Name as Bank,c.Number as BankAccount,a.Amount,a.[Date],
			a.Sequence ,d.[Description] as Concept ,a.Number as NumberDep,
			a.TenantId,a.BankAccountId,a.ConceptId,a.BankId,e.Name as TenantName from Transactions a
			inner join Banks b on b.Id = a.BankId
			inner join BankAccounts c on c.Id=a.BankAccountId
			inner join Concepts d on d.Id = a.ConceptId
			inner join AbpTenants e on e.Id = a.TenantId
			where a.TransactionTypeId=2 and a.StatusId=1 
			");
		}

		public void ViewFor_VW_Proyection()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("VW_Proyection"));
			this.context.Database.ExecuteSqlCommand(@"
					-- =============================================
					-- Author:		<Eduardo Santana>
					-- Create date: <2018 - 08 - 11>
					-- Description:	<Se buscan las proyecciones de pagos de los tutores>
					-- =============================================
					create view [dbo].[VW_Proyection] as
						SELECT
							  e.FirstName + ' ' + e.LastName EnrollmentFullName,
							  es.Sequence Enrollment
							  ,pp.Amount ProjectionAmount
							  ,t.Name Tenant
							  ,pp.PaymentDate 
							  ,pp.TenantId
							  ,pp.IsDeleted
							  ,pp.IsActive
						  FROM dbo.PaymentProjections pp inner join EnrollmentStudents est on est.Id = pp.EnrollmentStudentId 
						  inner join Enrollments e on  est.EnrollmentId = e.Id 
						  inner join EnrollmentSequences es on es.EnrollmentId = e.Id
						  inner join AbpTenants t on t.Id = pp.TenantId and t.Id = es.TenantId
						  where pp.IsActive = 1 and pp.IsDeleted = 0
			");
		}

		public void ViewFor_EnrollmentInscription()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_EnrollmentInscriptionStudent"));
			this.context.Database.ExecuteSqlCommand(@"
				CREATE view [dbo].[rep_view_EnrollmentInscriptionStudent] as
				Select
				a.CreationTime,
				e.Description as Period,
				a.TenantId,
				a.StudentId,
				a.Id as EnrollmentStudentId,
				b.LastName as StudentLastName,
				b.FirstName as StudentFirstName,
				b.DateOfBirth,
				f.Name as StudentGender,
				c.Sequence as EnrollmentSequencesId,
				d.Id as Cedula,
				d.FirstName,
				d.LastName,
				g.Name as Gender,
				d.Address,
				d.Phone1,
				d.Phone2,
				d.EmailAddress,
				i.Abbreviation as CourseAbbreviation,
				j.Amount as PagoMatricula
				from EnrollmentStudents a
				inner join Students b on b.Id = a.StudentId
				inner join EnrollmentSequences c on c.EnrollmentId = a.EnrollmentId and c.TenantId = a.TenantId
				inner join Enrollments d on d.Id = a.EnrollmentId
				inner join Periods e on e.Id = a.PeriodId
				inner join Genders f on f.Id = b.GenderId
				inner join Genders g on g.Id = d.GenderId
				inner join CourseEnrollmentStudents h on h.EnrollmentStudentId = a.Id
				inner join Courses i on i.Id = h.CourseId
				inner join TransactionStudents j on j.EnrollmentStudentId = a.Id
				inner join AbpTenants k on k.Id = a.TenantId
				inner join Transactions l on l.Id = j.TransactionId
				where l.ConceptId in (select ConceptId from ConceptTenantRegistrations where TenantId = a.TenantId and IsDeleted = 0 and IsActive = 1)
			");
		}

		public void ViewFor_PaymentProjections()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_PaymentProjections"));
			this.context.Database.ExecuteSqlCommand(@"
						Create View  rep_view_PaymentProjections as
						SELECT 
						' ' PaymentProjectionReport,
						ten.Name ,
						er.Id AS EnrollmentId,
						er.FirstName,
						er.LastName,
						EnrollmentName = er.FirstName + ' ' + er.Lastname,
						t.PaymentDate ,
						t.Sequence,
						ers.Sequence EnrollmentSequence,
						case t.sequence when 0 then 'Matricula' else 'Mensualidad' end ConceptDescription,
						t.Amount,
						t.TenantId 
						FROM  dbo.PaymentProjections t 
						inner join EnrollmentStudents ee on ee.id = t.enrollmentStudentId
						INNER JOIN dbo.Enrollments er ON er.Id = ee.EnrollmentId
						INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId  
						inner join AbpTenants ten on ten.Id = t.TenantId
						WHERE t.IsDeleted = 0 AND ers.IsDeleted = 0 AND t.IsActive = 1 
			");
		}

		public void InsertReportGeneralReceiptInDatabaseByCode()
		{
			this.context.Database.ExecuteSqlCommand(@"
		   
				   
						if( not( exists(select id from Reports where ReportCode = '25')))
						begin

								INSERT [dbo].[Reports] (  [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId],
									[CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime],
									[ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [ReportDataSourceId], 
									[FilterByTenant], [IsForTenant]) 
									VALUES ( 3, 0, 0, 1, CAST(N'2001-01-01T00:00:00.000' AS DateTime), 
									NULL, NULL, NULL, NULL, N'25', N'Receipt', N'rep_vw_ReceiptGeneral', 
									N'Reports\', N'RdlcFiles\Financiero\Receipt.rdlc', 1, 1, 1)

								INSERT [dbo].[ReportsFilters] 
									( [ReportsId], [IsDeleted], [IsActive],
									[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], 
									[DeleterUserId], [DeletionTime], [ReportId], [Order], 
									[Name], [DisplayName], [DataField], [DataTypeId], 
									[Range], [OnlyParameter], [UrlService], [FieldService], 
									[DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], 
									[Required]) 
									VALUES ((select id from Reports where ReportCode = '25'), 0, 1,
									1, CAST(N'2001-01-01T00:00:00.000' AS DateTime), NULL, NULL, 
									NULL, NULL, 12, 1, 
									N'TransactionId', N'Transaction', N'TransactionId', 1,
									0, 0, NULL, NULL, 
									NULL, NULL, NULL, NULL, 
									1)

								INSERT [dbo].[ReportsFilters] ( [ReportsId], [IsDeleted], [IsActive],
									[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], 
									[DeleterUserId], [DeletionTime], [ReportId], [Order],
									[Name], [DisplayName], [DataField], [DataTypeId], 
									[Range], [OnlyParameter], [UrlService], [FieldService], 
									[DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], 
									[Required]) 
									VALUES ((select id from Reports where ReportCode = '25'), 0, 1, 
									1, CAST(N'2001-01-01T00:00:00.000' AS DateTime), NULL, NULL,
									NULL, NULL, 12, 1, 
									N'AmountText', N'AmountText', N'AmountText', 2,
									0, 1, NULL, NULL, 
									NULL, NULL, NULL, NULL,
									1)
						end
		   
				");
		}

		public void InsertReportArticlesReceiptInDatabaseByCode()
		{
			this.context.Database.ExecuteSqlCommand(@"
		   
				   
						if( not( exists(select id from Reports where ReportCode = '26')))
						begin

								INSERT [dbo].[Reports] (  [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId],
									[CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime],
									[ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [ReportDataSourceId], 
									[FilterByTenant], [IsForTenant]) 
									VALUES ( 3, 0, 0, 1, CAST(N'2001-01-01T00:00:00.000' AS DateTime), 
									NULL, NULL, NULL, NULL, N'26', N'Receipt Articles', N'rep_vw_ReceiptArticles', 
									N'Reports\', N'RdlcFiles\Financiero\ReceiptArticles.rdlc', 1, 1, 1)

								INSERT [dbo].[ReportsFilters] 
									( [ReportsId], [IsDeleted], [IsActive],
									[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], 
									[DeleterUserId], [DeletionTime], [ReportId], [Order], 
									[Name], [DisplayName], [DataField], [DataTypeId], 
									[Range], [OnlyParameter], [UrlService], [FieldService], 
									[DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], 
									[Required]) 
									VALUES ((select id from Reports where ReportCode = '26'), 0, 1,
									1, CAST(N'2001-01-01T00:00:00.000' AS DateTime), NULL, NULL, 
									NULL, NULL, 12, 1, 
									N'TransactionId', N'Transaction', N'TransactionId', 1,
									0, 0, NULL, NULL, 
									NULL, NULL, NULL, NULL, 
									1)

								INSERT [dbo].[ReportsFilters] ( [ReportsId], [IsDeleted], [IsActive],
									[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], 
									[DeleterUserId], [DeletionTime], [ReportId], [Order],
									[Name], [DisplayName], [DataField], [DataTypeId], 
									[Range], [OnlyParameter], [UrlService], [FieldService], 
									[DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], 
									[Required]) 
									VALUES ((select id from Reports where ReportCode = '26'), 0, 1, 
									1, CAST(N'2001-01-01T00:00:00.000' AS DateTime), NULL, NULL,
									NULL, NULL, 12, 1, 
									N'AmountText', N'AmountText', N'AmountText', 2,
									0, 1, NULL, NULL, 
									NULL, NULL, NULL, NULL,
									1)
						end
		   
				");
		}

		public void ViewFor_ListadoRecibosPorConcepto()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_ListadoMontosPorConceptos"));
			this.context.Database.ExecuteSqlCommand(@"
					create view [dbo].[rep_view_ListadoMontosPorConceptos] as
					select 
						' ' ResumenRecibos, 
						t.TenantId, 
						te.Name TenantName,
						t.ConceptId, 
						ISNULL(ca.AccountNumber, ca2.AccountNumber) + ' - ' +  isnull(c.Description,c2.Description) Description,  
						t.Date,
						t.CreationTime,
						isnull(tc.Amount ,t.Amount) as Amount,
						o.Name,
						AmountDebit = case when t.OriginId = 1 then isnull(tc.Amount ,t.Amount) else 0.00 end,  --Charges
						AmountCredit = case when t.OriginId = 2 then isnull(tc.Amount ,t.Amount) else 0.00 end  --Recibos
					from Transactions t 
						left join Concepts c on c.id = t.ConceptId
						inner join AbpTenants te on te.id = t.TenantId
						inner join Origins o on o.Id = t.OriginId
						left join Catalogs ca on ca.Id = c.CatalogId
						left join TransactionConcepts tc on tc.TransactionId = t.Id
						left join Concepts c2 on c2.id = tc.ConceptId
						left join Catalogs ca2 on ca2.Id = c2.CatalogId
					where t.IsDeleted = 0  and t.IsActive = 1 and  t.TransactionTypeId = 1
			");
		}

		public void ViewFor_ListadoRecibosPorConceptoTrans()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_ListadoMontosPorConceptosTrans"));
			this.context.Database.ExecuteSqlCommand(@"
				Create view [dbo].[rep_view_ListadoMontosPorConceptosTrans] as
					select ' ' TransactionsResume, t.TenantId, 
					te.Name TenantName,
					t.ConceptId, 
					isnull(c.Description,(Select top 1 c2.Description + ' ...' from TransactionConcepts tc2 inner join Concepts c2 on c2.Id = tc2.ConceptId where tc2.TransactionId = t.Id )) as Description,  
					t.Date,
					t.CreationTime,
					t.Amount ,
					AmountDebit = case when t.OriginId = 1 then t.amount else 0.00 end,  --Charges
					AmountCredit = case when t.OriginId = 2 then t.amount else 0.00 end  --Recibos
					from Transactions t 
					left join Concepts c on c.id = t.ConceptId
					inner join AbpTenants te on te.id = t.TenantId
					where t.IsDeleted = 0 and t.TransactionTypeId = 3
			");
		}

		public void ViewFor_ListadoMontosoPorCuentas()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_ListadoMontosPorCuentas"));
			this.context.Database.ExecuteSqlCommand(@"
					create view [dbo].[rep_view_ListadoMontosPorCuentas] as
					select 
					t.TenantId, 
					te.Name TenantName,
					isnull(cg2.Id, cg.Id) CatalogId,
					isnull(cg2.AccountNumber, cg.AccountNumber) AccountNumber,
					isnull(cg2.Descsription, cg.Descsription)  Descsription,
					isnull(cg2.Name, cg.Name) as Name,
					t.Date,
					t.CreationTime,
					isnull(tc.Amount, t.Amount) as Amount 
					from Transactions t 
					inner join AbpTenants te on te.id = t.TenantId
					left join Concepts c on c.id = t.ConceptId
					left join Catalogs cg on cg.id = c.CatalogId
					left join TransactionConcepts tc on tc.TransactionId = t.Id
					left join Concepts c2 on c2.Id = tc.ConceptId
					left join Catalogs cg2 on cg2.Id = c2.CatalogId
					where t.IsDeleted = 0 and t.TransactionTypeId=1
			");
		}

		/// <summary>
		/// Method to create a view for Account Balance List
		/// </summary>
		public void ViewFor_AccountBalanceList()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_AccountBalanceList"));
			this.context.Database.ExecuteSqlCommand(@"
				
				create view  rep_view_AccountBalanceList as
				select es.Sequence, Tenant = at.Name, e.id, e.FirstName, e.LastName, EnrollmentName = e.FirstName + ' ' + e.LastName, e.Phone1, e.Phone2, t.TenantId
				,sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) Balance
				from Enrollments e inner join EnrollmentSequences es on e.id = es.EnrollmentId
				inner join Transactions t on t.EnrollmentId = e.Id and es.EnrollmentId = t.enrollmentId
				and t.TenantId = es.TenantId 
				inner join AbpTenants at on at.Id = t.TenantId
				where t.TransactionTypeId in (1,3) and isnull(t.ReceiptTypeId,0) not in (2,3) 
				group by e.id, at.Name,e.FirstName, e.LastName, e.Phone1, e.Phone2, es.Sequence, t.TenantId
			
			");
		}

		/// <summary>
		/// Method to create a view for Evaluations
		/// </summary>
		public void ViewFor_Evaluations()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("vw_evaluations"));
			this.context.Database.ExecuteSqlCommand(@"
				
				
			create view vw_evaluations as

			select ig.Name IndicatorGroup,

			i.Name Indicator, i.Id IndicatorId, cs.CourseId, s.FirstName, s.LastName, e.EvaluationPeriodId, ep.Position , el.Name Legend ,
			ces.EnrollmentStudentId
			from CourseEnrollmentStudents ces  
			inner join Evaluations e on e.EnrollmentStudentId = ces.EnrollmentStudentId
			inner join CourseSessions cs on cs.id = ces.SessionId
			inner join Courses c on c.id = cs.CourseId
			inner join EvaluationPeriods ep on ep.Id = e.EvaluationPeriodId
			inner join EvaluationLegends el  on el.Id = e.EvaluationLegendId
			inner join Indicators i on i.id = e.IndicatorId
			inner join IndicatorGroups ig on ig.Id = i.IndicatorGroupId
			inner join EnrollmentStudents es on es.Id = ces.EnrollmentStudentId
			inner join Students s on s.Id = es.StudentId 
			
			");
		}

		/// <summary>
		/// Method to create a view for Evaluations
		/// </summary>
		public void ViewFor_Evaluations_Report()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("vw_evaluations_report"));
			this.context.Database.ExecuteSqlCommand(@"
				
				
			create view [dbo].[vw_evaluations_report] as
			select distinct  s.CourseId, c.Name Course, c.Abbreviation CourseAbbreviation, cs.Name Session, cs.Id SessionId, ig.SubjectId, s.Name Subject,  i.IndicatorGroupId, 
			StudentFirstName =  stu.FirstName, StudentLastName = stu.LastName, StudentFullName = stu.FirstName + ' ' + stu.LastName, EnrollmentFullName = enr.FirstName + ' ' + enr.LastName, EnrollmentFirstName = enr.FirstName,
			EnrollmentLastName = enr.LastName,
 
			ig.Name IndicatorGroup,
			i.id IndicatorId, 
			i.sequence IndicatorSequence,   
			i.name Idicator ,  
			ces.EnrollmentStudentId, ces.TenantId,
			isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 1 and  e.enrollmentStudentId = es.Id  ),'') E1 ,
			isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 2 and  e.enrollmentStudentId = es.Id  ),'')  E2,
			isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 3 and  e.enrollmentStudentId = es.Id ),'') E3 ,
			isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 4 and  e.enrollmentStudentId = es.Id  ),'') E4 ,
			isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 5 and  e.enrollmentStudentId = es.Id  ),'')  RF
			from Courses c, Indicators i, IndicatorGroups ig, 
			Subjects s, CourseSessions cs, Evaluations e, CourseEnrollmentStudents ces,
			EnrollmentStudents es, Students stu, Enrollments enr
			where	ig.Id = i.IndicatorGroupId  and 
			c.Id = s.CourseId and cs.CourseId = c.Id and 
			e.IndicatorId = i.Id and 
			ces.SessionId = cs.Id and 
			ig.SubjectId = s.Id and
			es.Id = ces.EnrollmentStudentId and
			stu.Id = es.StudentId and 
			enr.Id = es.EnrollmentId and ces.TenantId = e.TenantId and ces.TenantId = es.TenantId 
			and isnull((select top 1 legend from vw_evaluations e where e.IndicatorId = i.id and e.Position = 1 and  e.enrollmentStudentId = es.Id  ),'') != ''		
		
			
			");
		}

		/// <summary>
		/// Method to create a view for ViewFor_rep_vw_Receipt
		/// </summary>
		public void ViewFor_rep_vw_Receipt()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_vw_Receipt"));
			this.context.Database.ExecuteSqlCommand(@"
				CREATE VIEW [dbo].[rep_vw_Receipt]
				AS
				SELECT
				tn.Name AS TenatnName,
				tn.Phone1,
				t.Date ,
				t.TransactionTypeId,
				t.ReceiptTypeId,
				NCF = isnull(t.TaxReceiptSequence,''),
				Number = CAST(t.Sequence AS VARCHAR),
				t.Name ,
				t.Amount ,
				isnull(cp.Description,(Select top 1 c2.Description + ' ...' from TransactionConcepts tc2 inner join Concepts c2 on c2.Id = tc2.ConceptId where tc2.TransactionId = t.Id )) AS Concept,
				t.Id AS TransactionId,
				t.TenantId,
				t.ConceptId,
				ers.Sequence as EnrollmentSequence,
				t.PaymentMethodId,
				pm.Name PaymentMethod,
				st.FirstName + ' ' + st.LastName AS StudentName,
				ts.EnrollmentStudentId ,
				c.Abbreviation CourseName,
				c.Id CourseId,
				sec.Name SeccionName,
				sec.Id SeccionId,
				ts.Amount AS StudentAmount,
				t.RncName, 
				t.RncNumber,
				t.TaxReceiptSequence,
				tarety.Name as TaxReceiptName,
				case when t.GrantEnterpriseId is null then '' else '<strong>Subvencionado Por:</strong> ' + gren.Name end as Subvencionador,
				case when t.Comments is null or t.Comments = '' then '' else '<strong>Comentario:</strong> ' + t.Comments end as Comments,
				ExpirationDate = case when t.TaxReceiptExpirationDate is null then  tare.ExpirationDate else t.TaxReceiptExpirationDate end,
                us.Name+' '+us.Surname as UserCreator
				FROM dbo.Transactions t
				INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
				INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
				INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
				LEFT JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
				INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
				INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
				INNER JOIN dbo.TransactionStudents ts ON ts.TransactionId = t.Id
				INNER JOIN dbo.EnrollmentStudents erst ON erst.Id = ts.EnrollmentStudentId 
				INNER JOIN dbo.Students st ON st.Id = erst.StudentId 
				INNER JOIN dbo.CourseEnrollmentStudents crs ON crs.EnrollmentStudentId = ts.EnrollmentStudentId AND crs.TenantId = ts.TenantId
				INNER JOIN dbo.Courses c ON c.Id = crs.CourseId 
				LEFT JOIN dbo.CourseSessions sec ON sec.Id = crs.SessionId 
				LEFT JOIN TaxReceipts tare on tare.Id = t.TaxReceiptId
				LEFT JOIN TaxReceiptTypes tarety on tarety.Id = tare.TaxReceiptTypeId
				LEFT JOIN GrantEnterprises gren on gren.Id = t.GrantEnterpriseId
                Left JOIN dbo.AbpUsers us on us.Id=t.CreatorUserId
				WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1 and t.receiptTypeId = 1

			
			");
		}

		/// <summary>
		/// Method to create a view for ViewFor_rep_vw_Receipt
		/// </summary>
		public void InsertingMenuForReceiptList()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '51')))
					begin
						INSERT [dbo].[Reports] (  [ReportsTypesId], [IsDeleted], [IsActive],
						[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], 
						[DeleterUserId], [DeletionTime], [ReportCode], [ReportName], [View], [ReportRoot], 
						[ReportPath], [ReportDataSourceId], [FilterByTenant], [IsForTenant]) 
						VALUES ( 3, 0, 1, 1, CAST(N'2001-01-01T00:00:00.000' AS DateTime), 1, 
						CAST(N'2018-03-16T11:06:48.377' AS DateTime), NULL, NULL, N'51', N'ReceiptsTotalDepositList',
						N'rep_vw_ReceiptDeposit', N'Reports\', N'RdlcFiles\Financiero\ReceiptTotalDepositList.rdlc', 1, 1, 2)

						INSERT [dbo].[ReportsFilters] ( [ReportsId], [IsDeleted], [IsActive],
						[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], 
						[DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], 
						[DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService],
						[DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) 
						VALUES ( (SELECT ID FROM Reports where ReportCode = '51'), 0, 1, 1, CAST(N'2001-01-01T00:00:00.000' AS DateTime),
						NULL, NULL, NULL, NULL, 14, 1, N'Date', N'From', N'Date', 3, 1, 0, NULL,
						NULL, NULL, NULL, NULL, N'To', 1)

						INSERT [dbo].[ReportsFilters] ( [ReportsId], [IsDeleted], [IsActive],
						[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], 
						[DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], [DataField], 
						[DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService],
						[DependencyField], [Operator], [DisplayNameRange], [Required]) VALUES
						( (SELECT ID FROM Reports where ReportCode = '51'), 0, 1, 1, CAST(N'2018-03-12T04:03:00.267' AS DateTime), 1, 
						CAST(N'2018-03-12T04:09:07.540' AS DateTime), NULL, NULL, 0, 3, 
						N'Concept', N'Concept', N'conceptId', 1, 0, 0, N'api/services/app/concept/getAllConceptsForReceiptsCombo',
						N'id', N'description', NULL, NULL, NULL, 0)

						INSERT [dbo].[ReportsFilters] ( [ReportsId], [IsDeleted], [IsActive], 
						[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime],
						[DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], 
						[DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], 
						[DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required])
						VALUES ( (SELECT ID FROM Reports where ReportCode = '51'), 0, 1, 1, CAST(N'2018-03-18T23:08:04.867' AS DateTime), 
						NULL, NULL, NULL, NULL, 0, 4, N'School', N'School', N'TenantId', 1, 0, 0, 
						N'api/services/app/tenant/getAllTenantsForCombo', N'id', N'name', NULL, NULL, NULL, 0)

					end
						");

		}

		public void InsertingReportCalifications()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '52')))
					begin

						INSERT [dbo].[Reports] (  [ReportsTypesId], [IsDeleted], 
						[IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], 
						[LastModificationTime], [DeleterUserId], [DeletionTime], [ReportCode], 
						[ReportName], [View], [ReportRoot], [ReportPath], [ReportDataSourceId], 
						[FilterByTenant], [IsForTenant]) 
						VALUES ( 1, 0, 1, 1, CAST(N'2018-07-12T23:03:58.927' AS DateTime), 
						NULL, NULL, NULL, NULL, N'52', N'Reporte de Calificaciones',
						N'vw_evaluations_report', N'Reports\', N'RdlcFiles\Generales\StudentEvaluation.rdlc', 1, 1, 1)

		   
						INSERT [dbo].[ReportsFilters] ( [ReportsId], [IsDeleted], [IsActive],
						[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime],
						[DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], 
						[DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], 
						[DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required]) 
						VALUES ( (SELECT ID FROM Reports where ReportCode = '52'), 0, 1, 1, CAST(N'2018-07-12T23:05:04.240' AS DateTime), 
						NULL, NULL, NULL, NULL, (SELECT ID FROM Reports where ReportCode = '52'), 1, N'enrollmentStudentId', N'EnrollmentStudent',
						N'enrollmentStudentId', 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1)

					
						INSERT [dbo].[ReportsFilters] ([ReportsId], [IsDeleted], [IsActive], 
						[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime],
						[DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], 
						[DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService],
						[DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required])
						VALUES ( (SELECT ID FROM Reports where ReportCode = '52'), 0, 1, 1, CAST(N'2018-07-12T23:06:06.527' AS DateTime), NULL, NULL, 
						NULL, NULL, (SELECT ID FROM Reports where ReportCode = '52'), 2, N'SubjectId', N'Subject', N'SubjectId', 1, 0, 0, NULL, 
						NULL, NULL, NULL, NULL, NULL, 1)


					end
						");

		}

		/// <summary>
		/// Method to create a view for ViewFor_rep_vw_Receipt
		/// </summary>
		public void ViewFor_rep_vw_ReceiptDeposit()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_vw_ReceiptDeposit"));
			this.context.Database.ExecuteSqlCommand(@"
				
				create VIEW [dbo].[rep_vw_ReceiptDeposit]
							AS
								SELECT
						tn.Name AS TenatnName,
						tn.Phone1,
						t.Date ,
						t.TransactionTypeId,
						t.ReceiptTypeId,
						t.CreditCardPercent,
						NCF = isnull(t.TaxReceiptSequence,''),
						Number = CAST(t.Sequence AS VARCHAR),
						t.Name ,
						CASe when t.TransactionTypeId = 1 then t.Amount 
							 when t.TransactionTypeId = 2 then t.Amount * -1 end Amount,
						CASe when t.TransactionTypeId = 1 and t.PaymentMethodId = 2 then t.Amount  - round( t.creditCardPercent * t.Amount,2)
							 when t.TransactionTypeId = 1 and t.PaymentMethodId != 2 then t.Amount 
							 when t.TransactionTypeId = 2 then t.Amount * -1 end AmountMinusRetention,
						case when isnull(t.PaymentMethodId,0) = 2 and t.TransactionTypeId = 1 then round( t.creditCardPercent * t.Amount,2) else 0 end Retention,
						case when isnull(t.PaymentMethodId,0) = 2 and t.TransactionTypeId = 1 then round( t.Amount - round( t.creditCardPercent * t.Amount,2) ,2) else 0 end CreditCardAmount,
						 isnull(t.PaymentMethodId,0) pidd,
						isnull(cp.Description,(Select top 1 c2.Description + ' ...' from TransactionConcepts tc2 inner join Concepts c2 on c2.Id = tc2.ConceptId where tc2.TransactionId = t.Id )) AS Concept,
						t.Id AS TransactionId,
						t.TenantId,
						t.ConceptId,
						ers.Sequence as EnrollmentSequence,
						t.PaymentMethodId,
						case when t.TransactionTypeId = 1 then pm.Name 
							when t.TransactionTypeId = 2 then 'Deposits' end PaymentMethod,
						 ' ' AS StudentName,
						0 AS EnrollmentStudentId ,
						'' AS CourseName,
						0 AS CourseId,
						'' AS SeccionName,
						0 AS SeccionId,
						0 AS StudentAmount,
						t.RncName, 
						t.RncNumber,
						t.TaxReceiptSequence,
						tarety.Name as TaxReceiptName
						FROM dbo.Transactions t
						INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
						INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
						left JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
						left JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
						left JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
						left JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
						LEFT JOIN TaxReceipts tare on tare.Id = t.TaxReceiptId
						LEFT JOIN TaxReceiptTypes tarety on tarety.Id = tare.TaxReceiptTypeId
						WHERE  t.IsDeleted = 0 AND t.IsActive = 1  
						AND (
								(t.TransactionTypeId = 1) or
								(t.TransactionTypeId = 2)
							)
			
			
			");
		}

        public void Create_Sp_Projection_Rate()
        {
            this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_rep_ProjectionRate"));
            this.context.Database.ExecuteSqlCommand(@"

	CREATE procedure [dbo].[sp_rep_ProjectionRate] 
				     @School int = NULL,
				     @PaymentDate datetime = NULL,
					 @PaymentDateRange datetime = NULL,
					 @Course int = NULL,
					 @Period int = NULL
                    
					as begin
              Select pp.Amount,
         pp.EnrollmentStudentId,
                 pp.PaymentDate,
                    pp.TenantId,
             enstu.EnrollmentId,
                enstu.StudentId,
                 enstu.PeriodId,
                 concat (enr.FirstName,' ',enr.LastName) as EnrollmentName,
                 ensec.Sequence as TutorEnrollment,
				 concat(stu.FirstName,' ',stu.LastName) as StudentName,
                   cee.CourseId,
                   cee.Sequence as StudentNumber,
                   cee.SessionId,
                   cur.Name as Curso,
				   Mensualidad =(Select TotalAmount/(Select count(*) from PaymentDates
				where TenantId=@School
				and IsDeleted=0
				and Sequence<>0) from CourseValues
				where CourseId=@Course
				and TenantId=@School
				and PeriodId=@Period)
			  
                 from PaymentProjections pp
                   inner join EnrollmentStudents enstu on enstu.id=pp.EnrollmentStudentId
                   inner join Enrollments enr on enr.id=enstu.EnrollmentId
                   inner join EnrollmentSequences ensec on ensec.EnrollmentId= enr.Id and ensec.TenantId=enstu.TenantId
                   inner join Students  stu on stu.id=enstu.StudentId
                   inner join CourseEnrollmentStudents cee on cee.EnrollmentStudentId = enstu.Id
                   inner join Courses cur on cur.Id=cee.CourseId
                   
                where pp.IsDeleted=0 
                   and pp.IsActive=1
				   and pp.TenantId =@School
				   and cee.CourseId=@Course
				   and enstu.PeriodId=@Period
				   and pp.PaymentDate between @PaymentDate and @PaymentDateRange


				select TotalAmount/(Select max(Sequence) as Total from PaymentDates
				                           where TenantId=@School) as Presupuestado
				from CourseValues
				where CourseId=@Course
					and TenantId = @School
					and PeriodId=@Period
			
				end


");


        }

        public void CreateView_Projection_Rate()
        {
            this.context.Database.ExecuteSqlCommand(DeleteViewString("vw_Projection_Rate"));
            this.context.Database.ExecuteSqlCommand(@"
           Create view vw_Projection_Rate as
               Select pp.Amount,
         pp.EnrollmentStudentId,
                 pp.PaymentDate,
                    pp.TenantId,
             enstu.EnrollmentId,
                enstu.StudentId,
                 enstu.PeriodId,
                 enr.FirstName+' '+enr.LastName as EnrollmentName,
                 ensec.Sequence as TutorEnrollment,
                 stu.FirstName+' '+stu.LastName as StudentName,
                   cee.CourseId,
                   cee.Sequence as StudentNumber,
                   cee.SessionId,
                   cur.Name as Curso
from PaymentProjections pp
inner join EnrollmentStudents enstu on enstu.id=pp.EnrollmentStudentId
inner join Enrollments enr on enr.id=enstu.EnrollmentId
inner join EnrollmentSequences ensec on ensec.EnrollmentId= enr.Id and ensec.TenantId=enstu.TenantId
inner join Students  stu on stu.id=enstu.StudentId
inner join CourseEnrollmentStudents cee on cee.EnrollmentStudentId = enstu.Id
inner join Courses cur on cur.Id=cee.CourseId
where pp.IsDeleted=0 
and pp.IsActive=1     




        ");


        }

		/// <summary>
		/// Method to create a view for ViewFor_rep_vw_Receipt
		/// </summary>
        /// 


		public void ViewFor_rep_vw_ReceiptGeneral()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_vw_ReceiptGeneral"));
			this.context.Database.ExecuteSqlCommand(@"
						CREATE VIEW [dbo].[rep_vw_ReceiptGeneral]
						AS
						SELECT
						tn.Name AS TenatnName,
						tn.Phone1,
						t.Date ,
						NCF = isnull(t.TaxReceiptSequence,''),
						Number = CAST(t.Sequence AS VARCHAR),
						t.Name ,
						t.TransactionTypeId,
						t.ReceiptTypeId,
						t.Amount as Amount,
						isNull(cp2.Description, cp.Description) AS Concept,
						t.Id AS TransactionId,
						t.TenantId,
						t.ConceptId,
						t.PaymentMethodId,
						pm.Name PaymentMethod,
						tc2.Amount as AmountConcept,
						t.RncName, 
						t.RncNumber,
						t.TaxReceiptSequence,
						tarety.Name as TaxReceiptName,
						ExpirationDate = case when t.TaxReceiptExpirationDate is null then  tare.ExpirationDate else t.TaxReceiptExpirationDate end,
                        us.Name+' '+us.Surname as UserCreator,
						case when t.Comments is null or t.Comments = '' then '' else '<strong>Comentario:</strong> ' + t.Comments end as Comments
						FROM dbo.Transactions t
						INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
						INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
						left JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
						INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
						LEFT join dbo.TransactionConcepts tc2 on tc2.transactionId = t.Id
						left join  dbo.Concepts cp2 ON cp2.id = tc2.ConceptId
						LEFT JOIN TaxReceipts tare on tare.Id = t.TaxReceiptId
						LEFT JOIN TaxReceiptTypes tarety on tarety.Id = tare.TaxReceiptTypeId
                        Left JOIN dbo.AbpUsers us on us.Id=t.CreatorUserId
						WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1
						and t.ReceiptTypeId = 2          
			");
		}

		/// <summary>
		/// Method to create a view for ViewFor_rep_vw_Receipt
		/// </summary>
		public void ViewFor_rep_vw_ReceiptArticles()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_vw_ReceiptArticles"));
			this.context.Database.ExecuteSqlCommand(@"
						CREATE VIEW [dbo].[rep_vw_ReceiptArticles]
							AS
						SELECT isnull(ers.Sequence,'') EnrollmentSequence,
						tn.Name AS TenatnName,
						tn.Phone1,
						u.Description Unit,
						a.Description Article,
						t.Date ,
						NCF = isnull(t.TaxReceiptSequence,''),
						Number = CAST(t.Sequence AS VARCHAR),
						t.Name ,
						t.TransactionTypeId,
						t.ReceiptTypeId,
						t.Amount ,
						isnull(cp.Description,(Select top 1 c2.Description + ' ...' from TransactionConcepts tc2 inner join Concepts c2 on c2.Id = tc2.ConceptId where tc2.TransactionId = t.Id )) AS Concept,
						t.Id AS TransactionId,
						t.TenantId,
						t.ConceptId, 
						ta.Quantity,
						ta.Amount Price,
						ta.Total,
						t.PaymentMethodId,
						pm.Name PaymentMethod,
						t.RncName, 
						t.RncNumber,
						t.TaxReceiptSequence,
						tarety.Name as TaxReceiptName,
						ExpirationDate = case when t.TaxReceiptExpirationDate is null then  tare.ExpirationDate else t.TaxReceiptExpirationDate end,
						case when t.Comments is null or t.Comments = '' then '' else '<strong>Comentario:</strong> ' + t.Comments end as Comments
						FROM dbo.Transactions t
						INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
						INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
						LEFT JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
						LEFT JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
						LEFT JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
						INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
						INNER JOIN TransactionArticles ta on ta.TransactionId = t.Id
						inner join Articles a on a.Id = ta.ArticleId
						inner join Units u on u.Id = a.UnitId
						LEFT JOIN TaxReceipts tare on tare.Id = t.TaxReceiptId
						LEFT JOIN TaxReceiptTypes tarety on tarety.Id = tare.TaxReceiptTypeId
						WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1 AND t.ReceiptTypeId = 3               
			");
		}

		public void InsertingReportStudentEvaluationHighs()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '53')))
					begin

						INSERT INTO [dbo].[Reports]
								   ([ReportsTypesId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportCode]
								   ,[ReportName]
								   ,[View]
								   ,[ReportRoot]
								   ,[ReportPath]
								   ,[ReportDataSourceId]
								   ,[FilterByTenant]
								   ,[IsForTenant])
							 VALUES
								   (1
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,'53'
								   ,'Reporte de Calificaciones Secundaria'
								   ,'rep_view_EvaluationHighs'
								   ,'Reports\'
								   ,'RdlcFiles\Generales\StudentEvaluationHighs.rdlc'
								   ,1
								   ,1
								   ,1);

						Declare @reportId int = @@IDENTITY;

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,1
								   ,'SubjectHighId'
								   ,'SubjectHigh'
								   ,'SubjectHighId'
								   ,1
								   ,0
								   ,0
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,1);

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,2
								   ,'EvaluationOrderHighId'
								   ,'EvaluationOrderHigh'
								   ,'EvaluationOrderHighId'
								   ,1
								   ,0
								   ,0
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,1);

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,3
								   ,'CourseId'
								   ,'Course'
								   ,'CourseId'
								   ,1
								   ,0
								   ,0
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,1);

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,4
								   ,'SessionId'
								   ,'Session'
								   ,'SessionId'
								   ,1
								   ,0
								   ,0
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,1);

					end
						");

		}

		/// <summary>
		/// Method to create a view for ViewFor_rep_view_EvaluationHighs
		/// </summary>
		public void ViewFor_rep_view_EvaluationHighs()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_EvaluationHighs"));
			this.context.Database.ExecuteSqlCommand(@"
					-- =============================================
					-- Author:		<Eduardo Santana>
					-- Create date: <2018 - 08 - 11>
					-- Description:	<Se busca el listado de evaluaciones de secundaria>
					-- =============================================
					CREATE VIEW [dbo].[rep_view_EvaluationHighs]
					AS 
					SELECT
						eh.Id AS EvaluationHighsId,
						eh.EnrollmentStudentId,
						es.StudentId,
						es.PeriodId,
						s.FirstName,
						s.LastName,
						eh.Expression,
						ces.Sequence,
						eh.SubjectHighId,
						eh.EvaluationOrderHighId,
						ces.CourseId,
						ces.SessionId,
						es.TenantId,
						c.Name as CourseName,
						c.Abbreviation,
						cs.Name as SessionName,
						sh.Name as SubjectHighsName,
						eoh.[Description] as EvaluationOrderHighsName
					FROM
						EvaluationHighs eh
					INNER JOIN 
						EnrollmentStudents es ON eh.EnrollmentStudentId = es.Id
					INNER JOIN 
						CourseEnrollmentStudents ces ON eh.EnrollmentStudentId = ces.EnrollmentStudentId
					INNER JOIN 
						Students s ON es.StudentId = s.id
					INNER JOIN 
						Courses c ON ces.CourseId = c.id
					INNER JOIN 
						CourseSessions cs ON ces.SessionId = cs.id
					INNER JOIN 
						SubjectHighs sh ON eh.SubjectHighId = sh.id
					INNER JOIN 
						EvaluationOrderHighs eoh ON eh.EvaluationOrderHighId = eoh.id
			");
		}

		public void InsertingReportStudentEvaluationHighsByStudent()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '54')))
					begin

						INSERT INTO [dbo].[Reports]
								   ([ReportsTypesId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportCode]
								   ,[ReportName]
								   ,[View]
								   ,[ReportRoot]
								   ,[ReportPath]
								   ,[ReportDataSourceId]
								   ,[FilterByTenant]
								   ,[IsForTenant])
							 VALUES
								   (1
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,'54'
								   ,'Reporte de Calificaciones Secundaria por Estudiante'
								   ,'rep_view_EvaluationHighsByStudent'
								   ,'Reports\'
								   ,'RdlcFiles\Generales\StudentEvaluationHighsByStudent.rdlc'
								   ,1
								   ,1
								   ,1);

						Declare @reportId int = @@IDENTITY;
					
						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,3
								   ,'EnrollmentStudentId'
								   ,'EnrollmentStudent'
								   ,'EnrollmentStudentId'
								   ,1
								   ,0
								   ,0
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,1);


						end
					

						");
		}

		public void InsertingReportStudentEvaluationHighsBySubject()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '55')))
					begin

						INSERT INTO [dbo].[Reports]
								   ([ReportsTypesId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportCode]
								   ,[ReportName]
								   ,[View]
								   ,[ReportRoot]
								   ,[ReportPath]
								   ,[ReportDataSourceId]
								   ,[FilterByTenant]
								   ,[IsForTenant])
							 VALUES
								   (1
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,'55'
								   ,'Reporte de Calificaciones Secundaria por Materia'
								   ,'rep_view_EvaluationHighsByStudent'
								   ,'Reports\'
								   ,'RdlcFiles\Generales\StudentEvaluationHighsBySubject.rdlc'
								   ,1
								   ,1
								   ,1);

						Declare @reportId int = @@IDENTITY;
					
						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,1
								   ,'SubjectHighId'
								   ,'SubjectHigh'
								   ,'SubjectHighId'
								   ,1
								   ,0
								   ,0
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,1);

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,3
								   ,'CourseId'
								   ,'Course'
								   ,'CourseId'
								   ,1
								   ,0
								   ,0
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,1);

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,4
								   ,'SessionId'
								   ,'Session'
								   ,'SessionId'
								   ,1
								   ,0
								   ,0
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,null
								   ,1);

						end
					
						");
		}

		/// <summary>
		/// Method to create a view for ViewFor_rep_view_EvaluationHighsByStudent
		/// </summary>
		public void ViewFor_rep_view_EvaluationHighsByStudent()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_EvaluationHighsByStudent"));
			this.context.Database.ExecuteSqlCommand(@"
				-- =============================================
				-- Author:		<Eduardo Santana>
				-- Create date: <2018 - 08 - 13>
				-- Description:	<Se busca el listado de evaluaciones para un estudiante seleccionado>
				-- =============================================
				CREATE  VIEW [dbo].[rep_view_EvaluationHighsByStudent]
				AS 
				SELECT
					es.Id as EnrollmentStudentId,
					es.StudentId,
					es.PeriodId,
					s.FirstName,
					s.LastName,
					ces.Sequence,
					sh.Id as SubjectHighId,
					ces.CourseId,
					ces.SessionId,
					es.TenantId,
					c.Name as CourseName,
					c.Abbreviation,
					cs.Name as SessionName,
					sh.Name as SubjectHighsName
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,1,0) as Evaluation1
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,2,0) as Evaluation2
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,3,0) as Evaluation3
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,4,0) as Evaluation4
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,5,0) as Evaluation5
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,6,0) as Evaluation6
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,7,0) as Evaluation7
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,8,0) as Evaluation8
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,9,0) as Evaluation9
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,10,0) as Evaluation10
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,11,0) as Evaluation11
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,1) as Evaluation1Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,2) as Evaluation2Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,3) as Evaluation3Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,4) as Evaluation4Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,5) as Evaluation5Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,6) as Evaluation6Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,7) as Evaluation7Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,8) as Evaluation8Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,9) as Evaluation9Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,10) as Evaluation10Name
					,dbo.rep_fn_getOneEvaluationHighName(es.Id,sh.Id,es.TenantId,11) as Evaluation11Name
					,dbo.rep_fn_getOneEvaluationHigh(es.Id,sh.Id,es.TenantId,0,1) as Average
					,s.FirstName + ' ' + s.LastName as StudentFullName
				FROM 
					EnrollmentStudents es 
				INNER JOIN 
					CourseEnrollmentStudents ces ON es.Id = ces.EnrollmentStudentId AND ces.TenantId = es.TenantId
				INNER JOIN 
					Students s ON es.StudentId = s.id
				INNER JOIN 
					Courses c ON ces.CourseId = c.id
				INNER JOIN 
					CourseSessions cs ON ces.SessionId = cs.id
				INNER JOIN 
					SubjectHighs sh ON ces.CourseId = sh.CourseId
				WHERE es.IsDeleted = 0 AND es.IsActive = 1 

			");
		}

		/// <summary>
		/// Method to create a view for FunctionFor_rep_view_EvaluationHighsByStudent
		/// </summary>
		public void FunctionFor_rep_fn_getOneEvaluationHigh()
		{
			this.context.Database.ExecuteSqlCommand(DeleteFunctionString("rep_fn_getOneEvaluationHigh"));
			this.context.Database.ExecuteSqlCommand(@"
				-- =============================================
				-- Author:		<Eduardo Santana>
				-- Create date: <2018-08-13>
				-- Description:	<Buscar la calificacion de un alunno, para un curso, para unsa session>
				-- =============================================
				CREATE FUNCTION [dbo].[rep_fn_getOneEvaluationHigh]
				(
					-- Add the parameters for the function here
					@EnrollmentStudentId INT,
					@SubjectHighId INT,
					@TenantId INT,
					@Orden INT,
					@IsTotal BIT
				)
				RETURNS INT
				AS
				BEGIN
					-- Declare the return variable here
					DECLARE @ResultVar INT = 0;
					DECLARE @RowsTotal INT = 0;
					IF (@IsTotal = 1)
					BEGIN
						-- Add the T-SQL statements to compute the return value here
						SELECT @ResultVar = SUM([Expression]), @RowsTotal = COUNT(1)
						FROM [dbo].[EvaluationHighs]
						WHERE [SubjectHighId] = @SubjectHighId
							AND [EnrollmentStudentId] = @EnrollmentStudentId
							AND [TenantId] = @TenantId;

						IF (@ResultVar > 0)
						BEGIN 
							SET @ResultVar = @ResultVar / @RowsTotal;
						END 
					END 
					ELSE 
					BEGIN
						-- Add the T-SQL statements to compute the return value here
						SELECT @ResultVar = [Expression]
						FROM [dbo].[EvaluationHighs]
						WHERE [SubjectHighId] = @SubjectHighId
							AND [EnrollmentStudentId] = @EnrollmentStudentId
							AND [TenantId] = @TenantId
							AND EvaluationOrderHighId = @Orden
						;

					END

					IF (@ResultVar = 0)
					BEGIN 
						SET @ResultVar = NULL;
					END 
	
					-- Return the result of the function
					RETURN @ResultVar;

				END
			");
		}

		public void InsertingReportEnrollmentBalanceOne()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '57')))
					begin

						INSERT INTO [dbo].[Reports]
								   ([ReportsTypesId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportCode]
								   ,[ReportName]
								   ,[View]
								   ,[ReportRoot]
								   ,[ReportPath]
								   ,[ReportDataSourceId]
								   ,[FilterByTenant]
								   ,[IsForTenant])
							 VALUES
								   (  3  -- <ReportsTypesId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  '57' -- <ReportCode, nvarchar(80),>
								   ,  'CxC de Tutor' -- <ReportName, nvarchar(100),>
								   ,  'rep_view_AccountBalanceList' -- <View, nvarchar(max),>
								   ,  'Reports\' -- <ReportRoot, nvarchar(100),>
								   ,  'RdlcFiles\Financiero\EnrollmentBalanceOne.rdlc' -- <ReportPath, nvarchar(500),>
								   ,  1 -- <ReportDataSourceId, int,>
								   ,  1 -- <FilterByTenant, bit,>
								   ,  2 -- <IsForTenant, int,>
								   )

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = '57') -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'School' -- <Name, nvarchar(max),>
								   ,  'School' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   )

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = '57') -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'Sequence' -- <Name, nvarchar(max),>
								   ,  'Matricula' -- <DisplayName, nvarchar(max),>
								   ,  'Sequence' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  null -- 'api/services/app/enrollment/getAllEnrollmentsForCombo' -- <UrlService, nvarchar(max),>
								   ,  null -- 'id'   -- <FieldService, nvarchar(max),>
								   ,  null -- 'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   )
					end
					
						");
		}

		public void InsertingReportEnrollmentBalanceListToDate()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '56')))
					begin

						INSERT INTO [dbo].[Reports]
								   ([ReportsTypesId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportCode]
								   ,[ReportName]
								   ,[View]
								   ,[ReportRoot]
								   ,[ReportPath]
								   ,[ReportDataSourceId]
								   ,[FilterByTenant]
								   ,[IsForTenant])
							 VALUES
								   (  3  -- <ReportsTypesId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  '56' -- <ReportCode, nvarchar(80),>
								   ,  'CxC Tutores Fecha Corte' -- <ReportName, nvarchar(100),>
								   ,  'sp_rep_GetCxCReportTutor' -- <View, nvarchar(max),>
								   ,  'Reports\' -- <ReportRoot, nvarchar(100),>
								   ,  'RdlcFiles\Financiero\EnrollmentBalanceListToDate.rdlc' -- <ReportPath, nvarchar(500),>
								   ,  2 -- <ReportDataSourceId, int,>
								   ,  1 -- <FilterByTenant, bit,>
								   ,  2 -- <IsForTenant, int,>
								   )

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = '56') -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'TenantId' -- <Name, nvarchar(max),>
								   ,  'School' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  0 -- <Required, bit,>
								   )
								   
						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = '56') -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  2 -- <Order, int,>
								   ,  'ToDate' -- <Name, nvarchar(max),>
								   ,  'Fecha Corte' -- <DisplayName, nvarchar(max),>
								   ,  'ToDate' -- <DataField, nvarchar(max),>
								   ,  3 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  null -- <UrlService, nvarchar(max),>
								   ,  null   -- <FieldService, nvarchar(max),>
								   ,  null -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   )

					end
					

						");
		}

		public void InsertingProjectionReport()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '58')))
					begin

						INSERT INTO [dbo].[Reports]
								   ([ReportsTypesId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportCode]
								   ,[ReportName]
								   ,[View]
								   ,[ReportRoot]
								   ,[ReportPath]
								   ,[ReportDataSourceId]
								   ,[FilterByTenant]
								   ,[IsForTenant])
							 VALUES
								   (  3  -- <ReportsTypesId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  '58' -- <ReportCode, nvarchar(80),>
								   ,  'Proyecciones de Pagos' -- <ReportName, nvarchar(100),>
								   ,  'sp_ProjectionAmountByEnrollment' -- <View, nvarchar(max),>
								   ,  'Reports\' -- <ReportRoot, nvarchar(100),>
								   ,  'RdlcFiles\Financiero\PaymentProjectionDateRange.rdlc' -- <ReportPath, nvarchar(500),>
								   ,  2 -- <ReportDataSourceId, int,>
								   ,  1 -- <FilterByTenant, bit,>
								   ,  2 -- <IsForTenant, int,>
								   )

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = '58') -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  2 -- <Order, int,>
								   ,  'dateFrom' -- <Name, nvarchar(max),>
								   ,  'Desde' -- <DisplayName, nvarchar(max),>
								   ,  'dateFrom' -- <DataField, nvarchar(max),>
								   ,  3 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  null -- <UrlService, nvarchar(max),>
								   ,  null   -- <FieldService, nvarchar(max),>
								   ,  null -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
							)

							INSERT INTO [dbo].[ReportsFilters]
									   ([ReportsId]
									   ,[IsDeleted]
									   ,[IsActive]
									   ,[CreatorUserId]
									   ,[CreationTime]
									   ,[LastModifierUserId]
									   ,[LastModificationTime]
									   ,[DeleterUserId]
									   ,[DeletionTime]
									   ,[ReportId]
									   ,[Order]
									   ,[Name]
									   ,[DisplayName]
									   ,[DataField]
									   ,[DataTypeId]
									   ,[Range]
									   ,[OnlyParameter]
									   ,[UrlService]
									   ,[FieldService]
									   ,[DisplayNameService]
									   ,[DependencyField]
									   ,[Operator]
									   ,[DisplayNameRange]
									   ,[Required])
								 VALUES
									   ((select Id from Reports where ReportCode = '58') -- <Report
									   ,  0  -- <IsDeleted, bit,>
									   ,  1  -- <IsActive, bit,>
									   ,  1  -- <CreatorUserId, bigint,>
									   ,  GETDATE() -- <CreationTime, datetime,>
									   ,  null -- <LastModifierUserId, bigint,>
									   ,  null -- <LastModificationTime, datetime,>
									   ,  null -- <DeleterUserId, bigint,>
									   ,  null -- <DeletionTime, datetime,>
									   ,  0 -- <ReportId, int,>
									   ,  2 -- <Order, int,>
									   ,  'dateTo' -- <Name, nvarchar(max),>
									   ,  'Desde' -- <DisplayName, nvarchar(max),>
									   ,  'dateTo' -- <DataField, nvarchar(max),>
									   ,  3 -- <DataTypeId, int,>
									   ,  0 -- <Range, bit,>
									   ,  0 -- <OnlyParameter, bit,>
									   ,  null -- <UrlService, nvarchar(max),>
									   ,  null   -- <FieldService, nvarchar(max),>
									   ,  null -- <DisplayNameService, nvarchar(max),>
									   ,  null -- <DependencyField, nvarchar(max),>
									   ,  null -- <Operator, nvarchar(max),>
									   ,  null -- <DisplayNameRange, nvarchar(max),>
									   ,  1 -- <Required, bit,>
									   )
					end

					if(not(exists(select r.ReportCode from Reports r inner join reportsFilters f on r.id = f.ReportsId
						 where r.ReportCode = '58' and f.Name ='TenantId')))
				   begin
							INSERT [dbo].[ReportsFilters] ( [ReportsId], [IsDeleted], [IsActive], 
							[CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime],
							[DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], [DisplayName], 
							[DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], 
							[DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required])
							VALUES ( (SELECT ID FROM Reports where ReportCode = '58'), 0, 1, 1, CAST(N'2018-03-18T23:08:04.867' AS DateTime), 
							NULL, NULL, NULL, NULL, 0, 4, N'TenantId', N'School', N'TenantId', 1, 0, 0, 
							N'api/services/app/tenant/getAllTenantsForCombo', N'id', N'name', NULL, NULL, NULL, 0)
					end
					

						");
		}

		public void InsertingReceipForSecondImpresion()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '60')))
					begin

						
							INSERT [dbo].[Reports] (  [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime],
							 [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportCode], [ReportName], 
							 [View], [ReportRoot], [ReportPath], [ReportDataSourceId], [FilterByTenant], [IsForTenant]) 
							 VALUES ( 3, 0, 0, 1, CAST(N'2001-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL,
							  N'60', N'Receipt Printer 2', N'rep_vw_Receipt', N'Reports\', N'RdlcFiles\Financiero\ReceiptStudents2.rdlc', 1, 1, 1)

							INSERT [dbo].[ReportsFilters] ( [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], 
							[LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order],
							 [Name], [DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService],
							  [DisplayNameService], [DependencyField], [Operator], [DisplayNameRange], [Required], [DefaultValue]) 
							  VALUES ( (select top 1 id from Reports where ReportCode = '60'), 0, 1, 1, CAST(N'2001-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, (select top 1 id from Reports where ReportCode = '60'), 1,
							   N'TransactionId', N'Transaction', N'TransactionId', 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)

							INSERT [dbo].[ReportsFilters] ( [ReportsId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], 
							[LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportId], [Order], [Name], 
							[DisplayName], [DataField], [DataTypeId], [Range], [OnlyParameter], [UrlService], [FieldService], [DisplayNameService],
							 [DependencyField], [Operator], [DisplayNameRange], [Required], [DefaultValue]) 
							 VALUES ((select top 1 id from Reports where ReportCode = '60'), 0, 1, 1, CAST(N'2001-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, (select top 1 id from Reports where ReportCode = '60'),
							  1, N'AmountText', N'AmountText', N'AmountText', 2, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
					end
					

						");
		}

        public void ProceduraFor_sp_rep_GetCxCReportTutorActual()
        {
            this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_rep_CxCReportTutorActualPeriod"));
            this.context.Database.ExecuteSqlCommand(@"
            Create Procedure sp_rep_CxCReportTutorActualPeriod
                   @TenantId int = null,
	  @ToDate datetime
     as 
	 begin

				select 
					es.Sequence, 
					Tenant = at.Name, 
					e.id, 
					e.FirstName, 
					e.LastName, 
					EnrollmentName = e.FirstName + ' ' + e.LastName, 
					e.Phone1, 
					e.Phone2, 
					t.TenantId
                    ,case when sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) > 0 then
					sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) else 0 end  BalanceDebit
					,case when sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) < 0 then
					sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) else 0 end  BalanceCredit
					,sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) Balance
					,sum( (case t.OriginId when  1 then t.Amount else 0 end)) Debit
					,sum( (case t.OriginId when  2 then t.Amount else 0 end)) Credit
				from 
					Enrollments e 
					inner join EnrollmentSequences es on e.id = es.EnrollmentId
					inner join Transactions t on t.EnrollmentId = e.Id and es.EnrollmentId = t.enrollmentId and t.TenantId = es.TenantId 
					inner join AbpTenants at on at.Id = t.TenantId
					inner join Periods pr on pr.Id = at.PeriodId
					
				where 
					t.TransactionTypeId in (1 , 3) and 
					isnull(t.ReceiptTypeId, 0) not in ( 2 , 3 ) and 
					(t.TenantId = @TenantId or @TenantId is null) and
					t.[Date] < convert(datetime, convert(varchar, @ToDate, 23) + ' 23:59:59:999', 21) and
					e.Id in ( Select enr.Id from PaymentProjections ppj
				inner join EnrollmentStudents es on es.Id = ppj.EnrollmentStudentId
				inner join Enrollments enr on enr.id=es.EnrollmentId
				where ppj.PaymentDate between pr.StartDate and pr.EndDate
				and (ppj.TenantId = @TenantId or @TenantId is null) and ppj.IsDeleted=0  
				group by enr.Id)

				group by 
					e.id, at.Name,e.FirstName, e.LastName, e.Phone1, e.Phone2, es.Sequence, t.TenantId

				end

      ");
        }

		public void ProcedureFor_sp_rep_GetCxCReportTutor()
		{
			this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_rep_GetCxCReportTutor"));
			this.context.Database.ExecuteSqlCommand(@"			
			-- =============================================
			-- Author:		<Eduardo Santana>
			-- Create date: <2018 - 08 - 28>
			-- Description:	<Buscar informacion agrupada filtrada por fecha>
			-- =============================================
			CREATE PROCEDURE [dbo].[sp_rep_GetCxCReportTutor]
				-- Add the parameters for the stored procedure here
				@TenantId int = NULL,
				@ToDate datetime
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				-- Insert statements for procedure here
				select 
					es.Sequence, 
					Tenant = at.Name, 
					e.id, 
					e.FirstName, 
					e.LastName, 
					EnrollmentName = e.FirstName + ' ' + e.LastName, 
					e.Phone1, 
					e.Phone2, 
					t.TenantId
					,case when sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) > 0 then
					sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) else 0 end  BalanceDebit
					,case when sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) < 0 then
					sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) else 0 end  BalanceCredit
					,sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) Balance
					,sum( (case t.OriginId when  1 then t.Amount else 0 end)) Debit
					,sum( (case t.OriginId when  2 then t.Amount else 0 end)) Credit
				from 
					Enrollments e 
					inner join EnrollmentSequences es on e.id = es.EnrollmentId
					inner join Transactions t on t.EnrollmentId = e.Id and es.EnrollmentId = t.enrollmentId and t.TenantId = es.TenantId 
					inner join AbpTenants at on at.Id = t.TenantId
				where 
					t.TransactionTypeId in (1 , 3) and 
					isnull(t.ReceiptTypeId, 0) not in ( 2 , 3 ) and 
					(t.TenantId = @TenantId or @TenantId is null) and
					t.[Date] < convert(datetime, convert(varchar, @ToDate, 23) + ' 23:59:59:999', 21)
				group by 
					e.id, at.Name,e.FirstName, e.LastName, e.Phone1, e.Phone2, es.Sequence, t.TenantId
			END
			");
		}

		public void ProcedureForProjectionAmountByEnrollment()
		{
			this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_ProjectionAmountByEnrollment"));
			this.context.Database.ExecuteSqlCommand(@"			
			-- =============================================
			-- Author:		<Amaury Nunez>
			-- Create date: <2018 - 08 - 30>
			-- Description:	<Buscar informacion agrupada por tutor y en un rango de fecha dada>
			-- =============================================
			
				create procedure [dbo].[sp_ProjectionAmountByEnrollment] (@TenantId int = 0, @dateFrom as datetime, @dateTo as datetime) as

				if(ISNULL(@TenantId,0) = 0)
				begin
					select sum(p.ProjectionAmount) Balance, 
						p.EnrollmentFullName EnrollmentName, p.Enrollment Sequence, p.TenantId,p.tenant, p.IsDeleted, p.IsActive  
					from VW_Proyection p
						where p.PaymentDate >= @dateFrom and p.PaymentDate <= @dateTo
					group by p.EnrollmentFullName, p.Enrollment, p.TenantId,p.tenant, p.IsDeleted, p.IsActive
				end
				else
				begin
					select sum(p.ProjectionAmount) Balance, 
						p.EnrollmentFullName EnrollmentName, p.Enrollment Sequence, p.TenantId,p.tenant, p.IsDeleted, p.IsActive  
					from VW_Proyection p
						where p.PaymentDate >= @dateFrom and p.PaymentDate <= @dateTo and p.TenantId = @TenantId
					group by p.EnrollmentFullName, p.Enrollment, p.TenantId,p.tenant, p.IsDeleted, p.IsActive
				end

			");
		}

		public void DefaultValueForDateInputs()
		{
			this.context.Database.ExecuteSqlCommand("update ReportsFilters set DefaultValue = '# new Date()' where DataTypeId = 3 and DefaultValue is null");
		}

		public void InsertingReportEnrollmentBalanceListByStudent()
		{
			this.context.Database.ExecuteSqlCommand(@" 
				if(not(exists(select ReportCode from Reports where ReportCode = '59')))
				begin

					INSERT INTO [dbo].[Reports]
								([ReportsTypesId]
								,[IsDeleted]
								,[IsActive]
								,[CreatorUserId]
								,[CreationTime]
								,[LastModifierUserId]
								,[LastModificationTime]
								,[DeleterUserId]
								,[DeletionTime]
								,[ReportCode]
								,[ReportName]
								,[View]
								,[ReportRoot]
								,[ReportPath]
								,[ReportDataSourceId]
								,[FilterByTenant]
								,[IsForTenant])
							VALUES
								(  3  -- <ReportsTypesId, int,>
								,  0  -- <IsDeleted, bit,>
								,  1  -- <IsActive, bit,>
								,  1  -- <CreatorUserId, bigint,>
								,  GETDATE() -- <CreationTime, datetime,>
								,  null -- <LastModifierUserId, bigint,>
								,  null -- <LastModificationTime, datetime,>
								,  null -- <DeleterUserId, bigint,>
								,  null -- <DeletionTime, datetime,>
								,  '59' -- <ReportCode, nvarchar(80),>
								,  'CxC de Tutor por Estudiante' -- <ReportName, nvarchar(100),>
								,  'sp_rep_AccountBalanceListByStudent' -- <View, nvarchar(max),>
								,  'Reports\' -- <ReportRoot, nvarchar(100),>
								,  'RdlcFiles\Financiero\EnrollmentBalanceListByStudent.rdlc' -- <ReportPath, nvarchar(500),>
								,  2 -- <ReportDataSourceId, int,>
								,  1 -- <FilterByTenant, bit,>
								,  2 -- <IsForTenant, int,>
								)

					INSERT INTO [dbo].[ReportsFilters]
								([ReportsId]
								,[IsDeleted]
								,[IsActive]
								,[CreatorUserId]
								,[CreationTime]
								,[LastModifierUserId]
								,[LastModificationTime]
								,[DeleterUserId]
								,[DeletionTime]
								,[ReportId]
								,[Order]
								,[Name]
								,[DisplayName]
								,[DataField]
								,[DataTypeId]
								,[Range]
								,[OnlyParameter]
								,[UrlService]
								,[FieldService]
								,[DisplayNameService]
								,[DependencyField]
								,[Operator]
								,[DisplayNameRange]
								,[Required])
							VALUES
								((select Id from Reports where ReportCode = '59') -- <ReportsId, int,>
								,  0  -- <IsDeleted, bit,>
								,  1  -- <IsActive, bit,>
								,  1  -- <CreatorUserId, bigint,>
								,  GETDATE() -- <CreationTime, datetime,>
								,  null -- <LastModifierUserId, bigint,>
								,  null -- <LastModificationTime, datetime,>
								,  null -- <DeleterUserId, bigint,>
								,  null -- <DeletionTime, datetime,>
								,  0 -- <ReportId, int,>
								,  1 -- <Order, int,>
								,  'TenantId' -- <Name, nvarchar(max),>
								,  'School' -- <DisplayName, nvarchar(max),>
								,  'TenantId' -- <DataField, nvarchar(max),>
								,  1 -- <DataTypeId, int,>
								,  0 -- <Range, bit,>
								,  0 -- <OnlyParameter, bit,>
								,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								,  'id'   -- <FieldService, nvarchar(max),>
								,  'name' -- <DisplayNameService, nvarchar(max),>
								,  null -- <DependencyField, nvarchar(max),>
								,  null -- <Operator, nvarchar(max),>
								,  null -- <DisplayNameRange, nvarchar(max),>
								,  0 -- <Required, bit,>
								)

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = '59') -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  2 -- <Order, int,>
								   ,  'ToDate' -- <Name, nvarchar(max),>
								   ,  'Fecha Corte' -- <DisplayName, nvarchar(max),>
								   ,  'ToDate' -- <DataField, nvarchar(max),>
								   ,  3 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  null -- <UrlService, nvarchar(max),>
								   ,  null   -- <FieldService, nvarchar(max),>
								   ,  null -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   )

								INSERT INTO [dbo].[ReportsFilters]
									([ReportsId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportId]
									,[Order]
									,[Name]
									,[DisplayName]
									,[DataField]
									,[DataTypeId]
									,[Range]
									,[OnlyParameter]
									,[UrlService]
									,[FieldService]
									,[DisplayNameService]
									,[DependencyField]
									,[Operator]
									,[DisplayNameRange]
									,[Required])
								VALUES
									((select Id from Reports where ReportCode = '59') -- <ReportsId, int,>
									,0
									,1
									,1
									,GETDATE()
									,null
									,null
									,null
									,null
									,0
									,3
									,'CourseId'
									,'Course'
									,'CourseId'
									,1
									,0
									,0
									,null
									,null
									,null
									,null
									,null
									,null
									,0)

				end
			");
		}

		public void ProcedureFor_rep_AccountBalanceListByStudent()
		{
			this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_rep_AccountBalanceListByStudent"));
			this.context.Database.ExecuteSqlCommand(@"			
			-- =============================================
			-- Author:		<Eduardo Santana>
			-- Create date: <2018 - 01 - 02>
			-- Description:	<Buscar informacion agrupada filtrada por fecha>
			-- =============================================
			CREATE PROCEDURE [dbo].[sp_rep_AccountBalanceListByStudent]
				-- Add the parameters for the stored procedure here
				@TenantId int = NULL,
				@ToDate datetime = NULL,
				@CourseId int = NULL
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				select 
					es.Sequence, 
					Tenant = at.Name, 
					e.id, 
					e.FirstName, 
					e.LastName, 
					EnrollmentName = e.FirstName + ' ' + e.LastName, 
					e.Phone1, 
					e.Phone2, 
					t.TenantId,
					sum(trst.Amount * (case t.OriginId when 2 then -1 else 1 end)) Balance,
					st.FirstName StudenFirstName, 
					st.LastName StudenLastName, 
					StudenName = st.FirstName + ' ' + st.LastName + ' / ' +  e.FirstName + ' ' + e.LastName,
					trst.EnrollmentStudentId,
					ces.CourseId,
					CourseName = co.Name 
				from 
					Enrollments e 
					inner join EnrollmentSequences es on e.id = es.EnrollmentId
					inner join Transactions t on 
						t.EnrollmentId = e.Id and 
						es.EnrollmentId = t.enrollmentId and 
						t.TenantId = es.TenantId 
					inner join AbpTenants at on at.Id = t.TenantId
					inner join TransactionStudents trst on trst.TransactionId = t.Id 
					inner join EnrollmentStudents enst on enst.Id = trst.EnrollmentStudentId and enst.TenantId = t.TenantId
					inner join Students st on st.Id = enst.StudentId 
					inner join CourseEnrollmentStudents ces on ces.EnrollmentStudentId = trst.EnrollmentStudentId and ces.TenantId = t.TenantId
					inner join Courses co on co.Id = ces.CourseId
				where 
					t.TransactionTypeId in (1, 3) and 
					isnull(t.ReceiptTypeId,0) not in (2, 3) and
					(t.TenantId = @TenantId or @TenantId is null) and 
					t.[Date] < convert(datetime, convert(varchar, @ToDate, 23) + ' 23:59:59:999', 21) and
					(@CourseId IS null or @CourseId = ces.CourseId)
				group by 
					e.id, at.Name,
					e.FirstName, 
					e.LastName, 
					e.Phone1, 
					e.Phone2, 
					es.Sequence, 
					t.TenantId,
					st.FirstName, 
					st.LastName,
					trst.EnrollmentStudentId,
					ces.CourseId,
					co.Name 
			END

			");
		}

		public void UpdateReportsPermissionName()
		{
			this.context.Database.ExecuteSqlCommand(@"			
				update Reports set PermissionName = 'Pages.Reports.Schools' where ReportCode = '01'  ; 
				update Reports set PermissionName = 'Pages.Reports.EnrollmentList' where ReportCode = '02'  ; 
				update Reports set PermissionName = 'Pages.Reports.StudentByCourse' where ReportCode = '03'  ; 
				update Reports set PermissionName = 'Pages.Reports.Receipt' where ReportCode = '04'  ; 
				update Reports set PermissionName = 'Pages.Reports.AccountBalance' where ReportCode = '05'  ; 
				update Reports set PermissionName = 'Pages.Reports.ReceiptsList' where ReportCode = '06'  ; 
				update Reports set PermissionName = 'Pages.Reports.TransactionList' where ReportCode = '07'  ; 
				update Reports set PermissionName = 'Pages.Reports.CxCTutores' where ReportCode = '08'  ; 
				update Reports set PermissionName = 'Pages.Reports.ListadoDepositos' where ReportCode = '09'  ; 
				update Reports set PermissionName = 'Pages.Reports.TiposdeSangre' where ReportCode = '10'  ; 
				update Reports set PermissionName = 'Pages.Reports.ListadoBancos' where ReportCode = '11'  ; 
				update Reports set PermissionName = 'Pages.Reports.ListadoEnfermedades' where ReportCode = '12'  ; 
				update Reports set PermissionName = 'Pages.Reports.ListadoNiveles' where ReportCode = '13'  ; 
				update Reports set PermissionName = 'Pages.Reports.ResumenRecibos' where ReportCode = '17'  ; 
				update Reports set PermissionName = 'Pages.Reports.ListadoConceptos' where ReportCode = '14'  ; 
				update Reports set PermissionName = 'Pages.Reports.CuentasCatalogo' where ReportCode = '15'  ; 
				update Reports set PermissionName = 'Pages.Reports.ResumenTransacciones' where ReportCode = '16'  ; 
				update Reports set PermissionName = 'Pages.Reports.CuentasporCobrarTutores' where ReportCode = '18'  ; 
				update Reports set PermissionName = 'Pages.Reports.ResumendeCuentas' where ReportCode = '19'  ; 
				update Reports set PermissionName = 'Pages.Reports.ReceiptArticles' where ReportCode = '26'  ; 
				update Reports set PermissionName = 'Pages.Reports.DebitosCreditos' where ReportCode = '20'  ; 
				update Reports set PermissionName = 'Pages.Reports.ResumenConceptosRecibos' where ReportCode = '21'  ; 
				update Reports set PermissionName = 'Pages.Reports.ReportedeProyeccionesdePagos' where ReportCode = '27'  ; 
				update Reports set PermissionName = 'Pages.Reports.ReportedeInscripcion' where ReportCode = '50'  ; 
				update Reports set PermissionName = 'Pages.Reports.ReceiptsTotalDepositList' where ReportCode = '51'  ; 
				update Reports set PermissionName = 'Pages.Reports.ReportedeCalificaciones' where ReportCode = '52'  ; 
				update Reports set PermissionName = 'Pages.Reports.ReportedeCalificacionesSecundaria' where ReportCode = '53'  ; 
				update Reports set PermissionName = 'Pages.Reports.ReportedeCalificacionesSecundariaporEstudiante' where ReportCode = '54'  ; 
				update Reports set PermissionName = 'Pages.Reports.ReportedeCalificacionesSecundariaporMateria' where ReportCode = '55'  ; 
				update Reports set PermissionName = 'Pages.Reports.CxCTutoresFechaCorte' where ReportCode = '56' ;
				update Reports set PermissionName = 'Pages.Reports.CxCdeTutor' where ReportCode = '57'  ; 
				update Reports set PermissionName = 'Pages.Reports.ProyeccionesdePagos' where ReportCode = '58'  ; 
				update Reports set PermissionName = 'Pages.Reports.CxCdeTutorporEstudiante' where ReportCode = '59'  ; 
				update Reports set PermissionName = 'Pages.Reports.ReceiptPrinter2' where ReportCode = '60'  ; 
			");
		}

		public void InsertingReportStudentByCourseBySession()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '61')))
					begin

						INSERT INTO [dbo].[Reports]
								   ([ReportsTypesId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportCode]
								   ,[ReportName]
								   ,[View]
								   ,[ReportRoot]
								   ,[ReportPath]
								   ,[ReportDataSourceId]
								   ,[FilterByTenant]
								   ,[IsForTenant])
							 VALUES
								   (1
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,'61'
								   ,'Estudiantes Por Session Con Nacimiento'
								   ,'sp_rep_StudentByCourseBySession'
								   ,'Reports\'
								   ,'RdlcFiles\Generales\StudentByCourseBySession.rdlc'
								   ,2
								   ,1
								   ,1);

						Declare @reportId int = (select Id from Reports where ReportCode = '61');

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,3
								   ,'CourseId'
								   ,'Course'
								   ,'CourseId'
								   ,1
								   ,0
								   ,0
								   ,'api/services/app/course/GetAllCoursesForCombo'
								   ,'id'
								   ,'name'
								   ,null
								   ,null
								   ,null
								   ,1);

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,4
								   ,'SessionId'
								   ,'Session'
								   ,'SessionId'
								   ,1
								   ,0
								   ,0
								   ,'api/services/app/courseSession/getAllCourseSessionsForCombo'
								   ,'id'
								   ,'name'
								   ,null
								   ,null
								   ,null
								   ,1);

							INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  @reportId
								   ,  1 -- <Order, int,>
								   ,  'TenantId' -- <Name, nvarchar(max),>
								   ,  'School' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

					end
						");

		}

		public void InsertingReportStudentByCourseBySessionWB()
		{
			this.context.Database.ExecuteSqlCommand(@" 

					if(not(exists(select ReportCode from Reports where ReportCode = '66')))
					begin

						INSERT INTO [dbo].[Reports]
								   ([ReportsTypesId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportCode]
								   ,[ReportName]
								   ,[View]
								   ,[ReportRoot]
								   ,[ReportPath]
								   ,[ReportDataSourceId]
								   ,[FilterByTenant]
								   ,[IsForTenant])
							 VALUES
								   (1
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,'66'
								   ,'Reporte de Estudiantes Por Session'
								   ,'sp_rep_StudentByCourseBySession'
								   ,'Reports\'
								   ,'RdlcFiles\Generales\StudentByCourseBySessionWB.rdlc'
								   ,2
								   ,1
								   ,1);

						Declare @reportId int = (select Id from Reports where ReportCode = '66');

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,3
								   ,'CourseId'
								   ,'Course'
								   ,'CourseId'
								   ,1
								   ,0
								   ,0
								   ,'api/services/app/course/GetAllCoursesForCombo'
								   ,'id'
								   ,'name'
								   ,null
								   ,null
								   ,null
								   ,1);

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,@reportId
								   ,4
								   ,'SessionId'
								   ,'Session'
								   ,'SessionId'
								   ,1
								   ,0
								   ,0
								   ,'api/services/app/courseSession/getAllCourseSessionsForCombo'
								   ,'id'
								   ,'name'
								   ,null
								   ,null
								   ,null
								   ,1);

							INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   (@reportId
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  @reportId
								   ,  1 -- <Order, int,>
								   ,  'TenantId' -- <Name, nvarchar(max),>
								   ,  'School' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

					end
						");

		}

		public void ProcedureFor_rep_StudentByCourseBySession()
		{
			this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_rep_StudentByCourseBySession"));
			this.context.Database.ExecuteSqlCommand(@"			
			-- =============================================
			-- Author:		<Eduardo Santana>
			-- Create date: <2018 - 09 - 05>
			-- Description:	<Buscar informacion de estudiantes por curso por sessiones>
			-- =============================================
			CREATE PROCEDURE [dbo].[sp_rep_StudentByCourseBySession]
				-- Add the parameters for the stored procedure here
				@TenantId int = NULL,
				@CourseId int = NULL,
				@SessionId int = NULL
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				Select 
					stu.LastName,
					stu.FirstName,
					enrxsec.Sequence,
					enr.FirstName + ' ' + enr.LastName as FullName,
					cou.Name CourseName,
					enrxstu.TenantId,
					stu.Id as StudentId,
					stu.DateOfBirth,
					cou.Id as CourseId,
					enr.Id as EnrollmentId,
					enrxstu.PeriodId ,
					pr.Period,
					tn.Name Tenant,
					cou.LevelId,
					lv.Name AS LevelName,
					isnull(couxenrxstu.Sequence,  ROW_NUMBER() OVER(ORDER BY stu.LastName,stu.FirstName ASC)) as SequenceStudent,
					(CASE WHEN couxenrxstu.Sequence IS NULL THEN '**' ELSE '' END) as IsTemporalSequence,
					couxenrxstu.SessionId,
					corses.Name as SessionName,
					stu.LastName + ' ' + stu.FirstName as StudentFullName
				From Students stu 
					inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
					inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId and enrxsec.TenantId = enrxstu.TenantId 
					inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
					inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
					inner join Courses cou on cou.Id = couxenrxstu.CourseId
					INNER JOIN dbo.Periods pr ON pr.Id = enrxstu.PeriodId 
					LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId 
					INNER JOIN dbo.Levels lv ON lv.Id = cou.LevelId 
					LEFT JOIN dbo.CourseSessions corses on corses.CourseId = cou.Id and corses.Id = couxenrxstu.SessionId
				WHERE stu.IsDeleted = 0 AND stu.IsActive = 1 and enrxsec.IsDeleted = 0 and enrxstu.IsDeleted = 0
				and enrxstu.TenantId = @TenantId 
				and cou.Id  = @CourseId
				and couxenrxstu.SessionId = @SessionId
				and tn.PeriodId = enrxstu.PeriodId
			END

			");
		}

		public void InsertingReportReceiptShops()
		{
			this.context.Database.ExecuteSqlCommand(@" 
				if(not(exists(select ReportCode from Reports where ReportCode = '62')))
				begin
					  INSERT INTO [dbo].[Reports]
										([ReportsTypesId]
										,[IsDeleted]
										,[IsActive]
										,[CreatorUserId]
										,[CreationTime]
										,[LastModifierUserId]
										,[LastModificationTime]
										,[DeleterUserId]
										,[DeletionTime]
										,[ReportCode]
										,[ReportName]
										,[View]
										,[ReportRoot]
										,[ReportPath]
										,[ReportDataSourceId]
										,[FilterByTenant]
										,[IsForTenant])
								  VALUES
										(  3  -- <ReportsTypesId, int,>
										,  0  -- <IsDeleted, bit,>
										,  1  -- <IsActive, bit,>
										,  1  -- <CreatorUserId, bigint,>
										,  GETDATE() -- <CreationTime, datetime,>
										,  null -- <LastModifierUserId, bigint,>
										,  null -- <LastModificationTime, datetime,>
										,  null -- <DeleterUserId, bigint,>
										,  null -- <DeletionTime, datetime,>
										,  '62' -- <ReportCode, nvarchar(80),>
										,  'Recibo de Tienda' -- <ReportName, nvarchar(100),>
										,  'rep_view_ListReciptShops' -- <View, nvarchar(max),>
										,  'Reports\' -- <ReportRoot, nvarchar(100),>
										,  'RdlcFiles\Financiero\ReceiptShopArticles.rdlc' -- <ReportPath, nvarchar(500),>
										,  1 -- <ReportDataSourceId, int,>
										,  1 -- <FilterByTenant, bit,>
										,  2 -- <IsForTenant, int,>
										)

					  INSERT INTO [dbo].[ReportsFilters]
										([ReportsId]
										,[IsDeleted]
										,[IsActive]
										,[CreatorUserId]
										,[CreationTime]
										,[LastModifierUserId]
										,[LastModificationTime]
										,[DeleterUserId]
										,[DeletionTime]
										,[ReportId]
										,[Order]
										,[Name]
										,[DisplayName]
										,[DataField]
										,[DataTypeId]
										,[Range]
										,[OnlyParameter]
										,[UrlService]
										,[FieldService]
										,[DisplayNameService]
										,[DependencyField]
										,[Operator]
										,[DisplayNameRange]
										,[Required])
								  VALUES
										((select Id from Reports where ReportCode = '62') -- <ReportsId, int,>
										,  0  -- <IsDeleted, bit,>
										,  1  -- <IsActive, bit,>
										,  1  -- <CreatorUserId, bigint,>
										,  GETDATE() -- <CreationTime, datetime,>
										,  null -- <LastModifierUserId, bigint,>
										,  null -- <LastModificationTime, datetime,>
										,  null -- <DeleterUserId, bigint,>
										,  null -- <DeletionTime, datetime,>
										,  0 -- <ReportId, int,>
										,  1 -- <Order, int,>
										,  'TenantId' -- <Name, nvarchar(max),>
										,  'School' -- <DisplayName, nvarchar(max),>
										,  'TenantId' -- <DataField, nvarchar(max),>
										,  1 -- <DataTypeId, int,>
										,  0 -- <Range, bit,>
										,  0 -- <OnlyParameter, bit,>
										,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
										,  'id'   -- <FieldService, nvarchar(max),>
										,  'name' -- <DisplayNameService, nvarchar(max),>
										,  null -- <DependencyField, nvarchar(max),>
										,  null -- <Operator, nvarchar(max),>
										,  null -- <DisplayNameRange, nvarchar(max),>
										,  1 -- <Required, bit,>
										)

						INSERT INTO [dbo].[ReportsFilters]
											   ([ReportsId]
											   ,[IsDeleted]
											   ,[IsActive]
											   ,[CreatorUserId]
											   ,[CreationTime]
											   ,[LastModifierUserId]
											   ,[LastModificationTime]
											   ,[DeleterUserId]
											   ,[DeletionTime]
											   ,[ReportId]
											   ,[Order]
											   ,[Name]
											   ,[DisplayName]
											   ,[DataField]
											   ,[DataTypeId]
											   ,[Range]
											   ,[OnlyParameter]
											   ,[UrlService]
											   ,[FieldService]
											   ,[DisplayNameService]
											   ,[DependencyField]
											   ,[Operator]
											   ,[DisplayNameRange]
											   ,[Required])
										 VALUES
											   ((select Id from Reports where ReportCode = '62') -- <ReportsId, int,>
											   ,  0  -- <IsDeleted, bit,>
											   ,  1  -- <IsActive, bit,>
											   ,  1  -- <CreatorUserId, bigint,>
											   ,  GETDATE() -- <CreationTime, datetime,>
											   ,  null -- <LastModifierUserId, bigint,>
											   ,  null -- <LastModificationTime, datetime,>
											   ,  null -- <DeleterUserId, bigint,>
											   ,  null -- <DeletionTime, datetime,>
											   ,  0 -- <ReportId, int,>
											   ,  2 -- <Order, int,>
											   ,  'TransactionShopId' -- <Name, nvarchar(max),>
											   ,  'Numero de Tansaccion' -- <DisplayName, nvarchar(max),>
											   ,  'TransactionShopId' -- <DataField, nvarchar(max),>
											   ,  1 -- <DataTypeId, int,>
											   ,  0 -- <Range, bit,>
											   ,  0 -- <OnlyParameter, bit,>
											   ,  null -- <UrlService, nvarchar(max),>
											   ,  null   -- <FieldService, nvarchar(max),>
											   ,  null -- <DisplayNameService, nvarchar(max),>
											   ,  null -- <DependencyField, nvarchar(max),>
											   ,  null -- <Operator, nvarchar(max),>
											   ,  null -- <DisplayNameRange, nvarchar(max),>
											   ,  1 -- <Required, bit,>
												   )
				end
	  ");
		}

		public void ViewFor_rep_view_ListReciptShops()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_ListReciptShops"));
			this.context.Database.ExecuteSqlCommand(@"
			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 08>
			-- Description:   <Se busca el listado de recibos de tienda>
			-- =============================================
			CREATE VIEW [dbo].[rep_view_ListReciptShops]
			AS 
			select 
				  ts.Id as TransactionShopId,
				  ts.Sequence,
				  ts.Amount,
				  ts.ClientShopId,
				  ts.TenantId,
				  tas.ArticleShopId,
				  tas.Amount as AmountArticle,
				  tas.Total as TotalArticle,
				  tas.Quantity as QuantityArticle,
				  arsh.Description as DescriptionArticle,
				  arsh.Reference as ReferenceArticle,
				  arsh.Price as PriceArticle, 
				  cs.Name as ClientName,
				  cs.Reference as ClientReference,
				  pm.Name as PaymentMethodsName,
				  at.Name as TenantName,
				  ts.CreationTime as Date,
				  ts.CreationTime
			from TransactionShops ts
			inner join TransactionArticleShops tas on tas.TransactionShopId = ts.Id and ts.TenantId = tas.TenantId
			inner join ArticleShops arsh on arsh.Id = tas.ArticleShopId and arsh.TenantId = ts.TenantId
			inner join ClientShops cs on cs.Id = ts.ClientShopId and cs.TenantId = ts.TenantId
			inner join PaymentMethods pm on pm.Id = ts.PaymentMethodId
			inner join AbpTenants at on at.Id = ts.TenantId
	  ");
		}

		public void InsertingReportReceiptShopsList()
		{
			this.context.Database.ExecuteSqlCommand(@" 
				if(not(exists(select ReportCode from Reports where ReportCode = '63')))
				begin
					  INSERT INTO [dbo].[Reports]
										([ReportsTypesId]
										,[IsDeleted]
										,[IsActive]
										,[CreatorUserId]
										,[CreationTime]
										,[LastModifierUserId]
										,[LastModificationTime]
										,[DeleterUserId]
										,[DeletionTime]
										,[ReportCode]
										,[ReportName]
										,[View]
										,[ReportRoot]
										,[ReportPath]
										,[ReportDataSourceId]
										,[FilterByTenant]
										,[IsForTenant])
								  VALUES
										(  3  -- <ReportsTypesId, int,>
										,  0  -- <IsDeleted, bit,>
										,  1  -- <IsActive, bit,>
										,  1  -- <CreatorUserId, bigint,>
										,  GETDATE() -- <CreationTime, datetime,>
										,  null -- <LastModifierUserId, bigint,>
										,  null -- <LastModificationTime, datetime,>
										,  null -- <DeleterUserId, bigint,>
										,  null -- <DeletionTime, datetime,>
										,  '63' -- <ReportCode, nvarchar(80),>
										,  'Listado de Recibos de Tienda' -- <ReportName, nvarchar(100),>
										,  'rep_view_ListReciptShopsList' -- <View, nvarchar(max),>
										,  'Reports\' -- <ReportRoot, nvarchar(100),>
										,  'RdlcFiles\Financiero\ReceiptShopList.rdlc' -- <ReportPath, nvarchar(500),>
										,  1 -- <ReportDataSourceId, int,>
										,  1 -- <FilterByTenant, bit,>
										,  2 -- <IsForTenant, int,>
										)

					  INSERT INTO [dbo].[ReportsFilters]
										([ReportsId]
										,[IsDeleted]
										,[IsActive]
										,[CreatorUserId]
										,[CreationTime]
										,[LastModifierUserId]
										,[LastModificationTime]
										,[DeleterUserId]
										,[DeletionTime]
										,[ReportId]
										,[Order]
										,[Name]
										,[DisplayName]
										,[DataField]
										,[DataTypeId]
										,[Range]
										,[OnlyParameter]
										,[UrlService]
										,[FieldService]
										,[DisplayNameService]
										,[DependencyField]
										,[Operator]
										,[DisplayNameRange]
										,[Required])
								  VALUES
										((select Id from Reports where ReportCode = '63') -- <ReportsId, int,>
										,  0  -- <IsDeleted, bit,>
										,  1  -- <IsActive, bit,>
										,  1  -- <CreatorUserId, bigint,>
										,  GETDATE() -- <CreationTime, datetime,>
										,  null -- <LastModifierUserId, bigint,>
										,  null -- <LastModificationTime, datetime,>
										,  null -- <DeleterUserId, bigint,>
										,  null -- <DeletionTime, datetime,>
										,  0 -- <ReportId, int,>
										,  1 -- <Order, int,>
										,  'TenantId' -- <Name, nvarchar(max),>
										,  'School' -- <DisplayName, nvarchar(max),>
										,  'TenantId' -- <DataField, nvarchar(max),>
										,  1 -- <DataTypeId, int,>
										,  0 -- <Range, bit,>
										,  0 -- <OnlyParameter, bit,>
										,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
										,  'id'   -- <FieldService, nvarchar(max),>
										,  'name' -- <DisplayNameService, nvarchar(max),>
										,  null -- <DependencyField, nvarchar(max),>
										,  null -- <Operator, nvarchar(max),>
										,  null -- <DisplayNameRange, nvarchar(max),>
										,  1 -- <Required, bit,>
										)

									INSERT INTO [dbo].[ReportsFilters]
											   ([ReportsId]
											   ,[IsDeleted]
											   ,[IsActive]
											   ,[CreatorUserId]
											   ,[CreationTime]
											   ,[LastModifierUserId]
											   ,[LastModificationTime]
											   ,[DeleterUserId]
											   ,[DeletionTime]
											   ,[ReportId]
											   ,[Order]
											   ,[Name]
											   ,[DisplayName]
											   ,[DataField]
											   ,[DataTypeId]
											   ,[Range]
											   ,[OnlyParameter]
											   ,[UrlService]
											   ,[FieldService]
											   ,[DisplayNameService]
											   ,[DependencyField]
											   ,[Operator]
											   ,[DisplayNameRange]
											   ,[Required])
										 VALUES
											   ((select Id from Reports where ReportCode = '63') -- <ReportsId, int,>
											   ,  0  -- <IsDeleted, bit,>
											   ,  1  -- <IsActive, bit,>
											   ,  1  -- <CreatorUserId, bigint,>
											   ,  GETDATE() -- <CreationTime, datetime,>
											   ,  null -- <LastModifierUserId, bigint,>
											   ,  null -- <LastModificationTime, datetime,>
											   ,  null -- <DeleterUserId, bigint,>
											   ,  null -- <DeletionTime, datetime,>
											   ,  0 -- <ReportId, int,>
											   ,  2 -- <Order, int,>
											   ,  'dateFrom' -- <Name, nvarchar(max),>
											   ,  'Desde' -- <DisplayName, nvarchar(max),>
											   ,  'Date' -- <DataField, nvarchar(max),>
											   ,  3 -- <DataTypeId, int,>
											   ,  1 -- <Range, bit,>
											   ,  0 -- <OnlyParameter, bit,>
											   ,  null -- <UrlService, nvarchar(max),>
											   ,  null   -- <FieldService, nvarchar(max),>
											   ,  null -- <DisplayNameService, nvarchar(max),>
											   ,  null -- <DependencyField, nvarchar(max),>
											   ,  null -- <Operator, nvarchar(max),>
											   ,  'Hasta' -- <DisplayNameRange, nvarchar(max),>
											   ,  1 -- <Required, bit,>
											   )
				end
			");
		}

		public void ViewFor_rep_view_ListReciptShopsList()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_ListReciptShopsList"));
			this.context.Database.ExecuteSqlCommand(@"
			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 09>
			-- Description:   <Se busca el listado de recibos de tienda listado>
			-- =============================================
			CREATE VIEW [dbo].[rep_view_ListReciptShopsList]
			AS 
			SELECT 
				at.Name AS TenantName,
				ts.CreationTime as Date ,
				NCF = '',
				Number = CAST(ts.Sequence AS VARCHAR),
				cs.Name as Name ,
				ts.Amount as Amount ,
				'Venta Tienda' AS Concept,
				ts.Id AS TransactionId,
				ts.TenantId,
				1 as ConceptId,
				cs.Name as ClientName,
				cs.Reference as ClientReference,
				pm.Name as PaymentMethodsName,
				ts.PaymentMethodId,
				pm.Name PaymentMethod
			from TransactionShops ts
			inner join ClientShops cs on cs.Id = ts.ClientShopId and cs.TenantId = ts.TenantId
			inner join PaymentMethods pm on pm.Id = ts.PaymentMethodId
			inner join AbpTenants at on at.Id = ts.TenantId
			;
			");
		}

		public void ViewFor_rep_vw_StudentByCourse()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_vw_StudentByCourse"));
			this.context.Database.ExecuteSqlCommand(@"
			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 09>
			-- Description:   <Se busca el listado de estsudiantes>
			-- =============================================
			CREATE VIEW [dbo].[rep_vw_StudentByCourse]
			as
			Select 
				'' as StudentByCourse,
				stu.LastName,
				stu.FirstName,
				enrxsec.Sequence,
				isnull(enr.FirstName, '') + ' ' + isnull(enr.LastName, '') as FullName,
				cou.Name CourseName,
				enrxstu.TenantId,
				stu.Id as StudentId,
				cou.Id as CourseId,
				enr.Id as EnrollmentId,
				enrxstu.PeriodId ,
				pr.Period,
				tn.Name Tenant,
				cou.LevelId,
				lv.Name AS LevelName,
				stu.DateOfBirth,
				enr.Phone1
			From Students stu 
			inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
			inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId and enrxsec.TenantId= enrxstu.TenantId
			inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
			inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
			inner join Courses cou on cou.Id = couxenrxstu.CourseId
			INNER JOIN dbo.Periods pr ON pr.Id = enrxstu.PeriodId 
			LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId 
			INNER JOIN dbo.Levels lv ON lv.Id = cou.LevelId 
			WHERE stu.IsDeleted = 0 AND stu.IsActive = 1 and enrxsec.IsDeleted=0 and enrxstu.IsDeleted=0
			and tn.PeriodId = enrxstu.PeriodId and enrxstu.IsActive=1
			;
			");
            
		}

		public void ViewFor_rep_vw_Receipts()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_vw_Receipts"));
			this.context.Database.ExecuteSqlCommand(@"
			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 18>
			-- Description:   <Se busca el listado de recibos>
			-- =============================================
			CREATE VIEW [dbo].[rep_vw_Receipts] 
			 AS
			SELECT 
				tn.Name AS TenantName,
				tn.Phone1,
				t.Date ,
				NCF = '',
				Number = CAST(t.Sequence AS VARCHAR),
				t.Name ,
				t.Amount ,
				isnull(cp.Description,(Select top 1 c2.Description + ' ...' from TransactionConcepts tc2 inner join Concepts c2 on c2.Id = tc2.ConceptId where tc2.TransactionId = t.Id )) AS Concept,
				t.Id AS TransactionId,
				t.TenantId,
				t.ConceptId,
				isnull(ers.Sequence,0) as EnrollmentSequence,
				t.PaymentMethodId,
				pm.Name PaymentMethod,
				t.RncName, 
				t.RncNumber,
                t.GrantEnterpriseId, 
				t.TaxReceiptSequence,
				ExpirationDate = case when t.TaxReceiptExpirationDate is null then  tare.ExpirationDate else t.TaxReceiptExpirationDate end,
				tarety.Name as TaxReceiptName
			FROM dbo.Transactions t
			INNER JOIN dbo.AbpTenants tn ON tn.Id = t.TenantId 
			INNER JOIN dbo.TransactionTypes tt ON t.TransactionTypeId = tt.Id 
			LEFT JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
			LEFT JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
			LEFT JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId
			INNER JOIN dbo.PaymentMethods pm ON pm.Id = t.PaymentMethodId
			LEFT JOIN TaxReceipts tare on tare.Id = t.TaxReceiptId
			LEFT JOIN TaxReceiptTypes tarety on tarety.Id = tare.TaxReceiptTypeId
			WHERE  t.IsDeleted = 0 AND t.IsActive = 1  AND t.TransactionTypeId = 1
			");
		}

		public void ViewFor_rep_view_EnrollmentPaymentStudent()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_EnrollmentPaymentStudent"));
			this.context.Database.ExecuteSqlCommand(@"
			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 23>
			-- Description:   <Se busca el listado de proyecciones de pago de estudiantes>
			-- =============================================
			CREATE VIEW [dbo].[rep_view_EnrollmentPaymentStudent]
			 AS
			Select
					a.CreationTime,
					a.PeriodId,
					e.Description as PeriodDescription,
					e.Period as PeriodPeriod,
					a.TenantId,
					a.StudentId,
					a.Id as EnrollmentStudentId,
					b.LastName as StudentLastName,
					b.FirstName as StudentFirstName,
					b.DateOfBirth,
					f.Name as StudentGender,
					c.Sequence as EnrollmentSequencesId,
					d.Id as Cedula,
					d.FirstName,
					d.LastName,
					g.Name as Gender,
					d.Address,
					d.Phone1,
					d.Phone2,
					d.EmailAddress,
					i.Abbreviation as CourseAbbreviation,
					a.EnrollmentId,
					payr.FirstQuota,
					payr.QuantityQuota,
					payr.MonthlyQuota,
					payr.YearlyQuota,
					DATEDIFF(YEAR,b.DateOfBirth,GETDATE()) as Age,
					j.Code as NivelCode,
					j.Name as NivelName,
					b.MotherPersonalId,
					b.MotherName,
					b.FatherPersonalId,
					b.FatherName
				from EnrollmentStudents a
					inner join Students b on b.Id = a.StudentId
					inner join EnrollmentSequences c on c.EnrollmentId = a.EnrollmentId and c.TenantId = a.TenantId
					inner join Enrollments d on d.Id = a.EnrollmentId
					inner join Periods e on e.Id = a.PeriodId
					inner join Genders f on f.Id = b.GenderId
					inner join Genders g on g.Id = d.GenderId
					inner join CourseEnrollmentStudents h on h.EnrollmentStudentId = a.Id
					inner join Courses i on i.Id = h.CourseId
					inner join AbpTenants k on k.Id = a.TenantId
					inner join Levels j on j.Id = i.LevelId
					inner join (
						SELECT 
							ee.StudentId, ee.PeriodId,
							sum (case t.Sequence when 0 then t.Amount else 0 end) as FirstQuota,
							max (t.Sequence) as QuantityQuota ,
							sum (case t.Sequence when 1 then t.Amount else 0 end) as MonthlyQuota,
							sum (t.Amount) as YearlyQuota
						FROM  dbo.PaymentProjections t 
							inner join EnrollmentStudents ee on ee.id = t.enrollmentStudentId
							INNER JOIN dbo.Enrollments er ON er.Id = ee.EnrollmentId
							INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId  
							inner join AbpTenants ten on ten.Id = t.TenantId
						WHERE t.IsDeleted = 0 AND ers.IsDeleted = 0 AND t.IsActive = 1 
					GROUP BY ee.StudentId, ee.PeriodId) payr 
					ON payr.StudentId = a.StudentId AND payr.PeriodId = a.PeriodId
			");
		}

		public void InsertingReportEnrollmentPaymentStudent()
		{
			this.context.Database.ExecuteSqlCommand(@" 
			if(not(exists(select ReportCode from Reports where ReportCode = '64')))
			begin
				  INSERT INTO [dbo].[Reports]
									([ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
							  VALUES
									(  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  '64' -- <ReportCode, nvarchar(80),>
									,  'Proyeccion Pago de Estudiante' -- <ReportName, nvarchar(100),>
									,  'rep_view_EnrollmentPaymentStudent' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Financiero\EnrolmentPaymentStudent.rdlc' -- <ReportPath, nvarchar(500),>
									,  1 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									)

				  INSERT INTO [dbo].[ReportsFilters]
									([ReportsId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportId]
									,[Order]
									,[Name]
									,[DisplayName]
									,[DataField]
									,[DataTypeId]
									,[Range]
									,[OnlyParameter]
									,[UrlService]
									,[FieldService]
									,[DisplayNameService]
									,[DependencyField]
									,[Operator]
									,[DisplayNameRange]
									,[Required])
							  VALUES
									((select Id from Reports where ReportCode = '64') -- <ReportsId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  0 -- <ReportId, int,>
									,  1 -- <Order, int,>
									,  'StudentId' -- <Name, nvarchar(max),>
									,  'Student' -- <DisplayName, nvarchar(max),>
									,  'StudentId' -- <DataField, nvarchar(max),>
									,  1 -- <DataTypeId, int,>
									,  0 -- <Range, bit,>
									,  0 -- <OnlyParameter, bit,>
									,  'api/services/app/student/getAllStudentsForCombo' -- <UrlService, nvarchar(max),>
									,  'id'   -- <FieldService, nvarchar(max),>
									,  'fullNameInverso' -- <DisplayNameService, nvarchar(max),>
									,  null -- <DependencyField, nvarchar(max),>
									,  null -- <Operator, nvarchar(max),>
									,  null -- <DisplayNameRange, nvarchar(max),>
									,  1 -- <Required, bit,>
									)

								INSERT INTO [dbo].[ReportsFilters]
										   ([ReportsId]
										   ,[IsDeleted]
										   ,[IsActive]
										   ,[CreatorUserId]
										   ,[CreationTime]
										   ,[LastModifierUserId]
										   ,[LastModificationTime]
										   ,[DeleterUserId]
										   ,[DeletionTime]
										   ,[ReportId]
										   ,[Order]
										   ,[Name]
										   ,[DisplayName]
										   ,[DataField]
										   ,[DataTypeId]
										   ,[Range]
										   ,[OnlyParameter]
										   ,[UrlService]
										   ,[FieldService]
										   ,[DisplayNameService]
										   ,[DependencyField]
										   ,[Operator]
										   ,[DisplayNameRange]
										   ,[Required])
									 VALUES
										   ((select Id from Reports where ReportCode = '64') -- <ReportsId, int,>
										   ,  0  -- <IsDeleted, bit,>
										   ,  1  -- <IsActive, bit,>
										   ,  1  -- <CreatorUserId, bigint,>
										   ,  GETDATE() -- <CreationTime, datetime,>
										   ,  null -- <LastModifierUserId, bigint,>
										   ,  null -- <LastModificationTime, datetime,>
										   ,  null -- <DeleterUserId, bigint,>
										   ,  null -- <DeletionTime, datetime,>
										   ,  0 -- <ReportId, int,>
										   ,  2 -- <Order, int,>
										   ,  'PeriodId' -- <Name, nvarchar(max),>
										   ,  'Period' -- <DisplayName, nvarchar(max),>
										   ,  'PeriodId' -- <DataField, nvarchar(max),>
										   ,  1 -- <DataTypeId, int,>
										   ,  0 -- <Range, bit,>
										   ,  0 -- <OnlyParameter, bit,>
										   ,  'api/services/app/period/getAllPeriodsForCombo' -- <UrlService, nvarchar(max),>
										   ,  'id'   -- <FieldService, nvarchar(max),>
										   ,  'description' -- <DisplayNameService, nvarchar(max),>
										   ,  null -- <DependencyField, nvarchar(max),>
										   ,  null -- <Operator, nvarchar(max),>
										   ,  'Hasta' -- <DisplayNameRange, nvarchar(max),>
										   ,  1 -- <Required, bit,>
										   );
			end");
		}

		public void InsertingReportCanceledTrasactions()
		{
			this.context.Database.ExecuteSqlCommand(@" 
			if(not(exists(select ReportCode from Reports where ReportCode = '65')))
			begin
				  INSERT INTO [dbo].[Reports]
									([ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
							  VALUES
									(  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  '65' -- <ReportCode, nvarchar(80),>
									,  'Solicitud Cancelación Recibos' -- <ReportName, nvarchar(100),>
									,  'rep_view_CanceledTrasactions' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Financiero\CanceledTransactions.rdlc' -- <ReportPath, nvarchar(500),>
									,  1 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									)

						INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = '65') -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'TenantId' -- <Name, nvarchar(max),>
								   ,  'Tenant' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  0 -- <Required, bit,>
								   );

					INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = '65') -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'TransactionTypeId' -- <Name, nvarchar(max),>
								   ,  'TransactionType' -- <DisplayName, nvarchar(max),>
								   ,  'TransactionTypeId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/transactionType/getAllTransactionTypesForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );


			end");
		}

		public void ViewFor_rep_view_CanceledTrasactions()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_CanceledTrasactions"));
			this.context.Database.ExecuteSqlCommand(@"
			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 27>
			-- Description:   <Se busca el listado de recibos para cancelar, sin autorizacion>
			-- =============================================
			CREATE VIEW [dbo].[rep_view_CanceledTrasactions]
			AS
			Select 
				a.Sequence as ReceipNumber,
				a.TenantId,
				b.Name as TenantName,
				a.EnrollmentId,
				a.Date as TransactionDate,
				a.TransactionTypeId,
				a.ReceiptTypeId,
				a.Amount
			from Transactions a
			inner join AbpTenants b on b.Id = a.TenantId
			where a.CancelStatusId = 1
			");
		}

		public void FunctionFor_rep_fn_getOneEvaluationHighName()
		{
			this.context.Database.ExecuteSqlCommand(DeleteFunctionString("rep_fn_getOneEvaluationHighName"));
			this.context.Database.ExecuteSqlCommand(@"
				-- =============================================
				-- Author:		<Eduardo Santana>
				-- Create date: <2018-08-13>
				-- Description:	<Buscar el nombre deL mes>
				-- =============================================
				CREATE FUNCTION [dbo].[rep_fn_getOneEvaluationHighName]
				(
					-- Add the parameters for the function here
					@EnrollmentStudentId INT,
					@SubjectHighId INT,
					@TenantId INT,
					@Orden INT
				)
				RETURNS VARCHAR(256)
				AS
				BEGIN
					-- Declare the return variable here
					DECLARE @ResultVar VARCHAR(256) ='';

					-- Add the T-SQL statements to compute the return value here
					SELECT TOP 1 @ResultVar = eoh.Description
					FROM EvaluationOrderHighs eoh
					WHERE eoh.Id = @Orden
					
					-- Return the result of the function
					RETURN @ResultVar;

				END
			
			");
		}

		public void ViewFor_rep_view_EvaluationFinalReport()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_EvaluationFinalReport"));
			this.context.Database.ExecuteSqlCommand(@"
			-- =============================================
			-- Author:		<Eduardo Santana>
			-- Create date: <2018 - 10 - 07>
			-- Description:	<Se busca el listado de evaluaciones finales para un estudiante seleccionado>
			-- =============================================
			ALTER  VIEW [dbo].[rep_view_EvaluationFinalReport]
			AS 
			SELECT
				t.Name as EducationalCenterName, --NombreDelCentro,
				t.AccreditationNumber as AccreditationNumber, --CodigoCentroEducativo,
				t.[Address] as EducationalDistrictAddress, --DireccionCentroEducativo,
				t.EducationalDistrict as EducationalDistrict, --DistritoEducativo,
				t.Regional as DirectionOfRegionalEducation, --DireccionRegionalEducacion,
				'' as Aproved, --PromovidoA,
				'' as PendingsSubjects, --AsignaturasPendientes,
				'' as NotAproved, --Reprobado,
				'' as Observations, --Observaciones,
				p.[Description] as SchoolYear, --AnoEscolar
				'' as AvgAA,

					cast(round((isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0),0)+
						isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0),0)+
						isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0),0)+
						isnull(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0),0))/4,0) as integer)  Average,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) > 0 then 

					cast(round((dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0))/8,0) as integer) else -1 end as PCP50,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) > 0 then
					cast(	round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0),2) as integer)  else  -1 end as CPC,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) > 0 then
					cast(	round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) / 2,0) as integer)  else -1 end as CPC50,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) > 0 then 

					cast(round((dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0))/8,0) + 
						
						round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,5,0) / 2,0) as integer)
						 else -1 end as CC,

						 ---------------------------------------------

					case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) > 0 then 

					cast(round(((dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0))/4)*0.3,0) as integer)  else -1 end as PCP30,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) > 0 then
					cast(	round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0),0) as integer)  else  -1 end as CPEX,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) > 0 then
					cast(	round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) * 0.70,0) as integer)  else -1 end as CPEX70,

				case when dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) > 0 then 

					cast(
					
					round(((dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0)+
						dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0))/4)*0.30,0) + 
						
					round(dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) * 0.70 ,0)  as integer)
						 
						  else -1 end as CEX,

				'' AS FinalA,
				'' AS FinalR,
				'' as CAP1,
				'' as CAP2,
				es.Id as EnrollmentStudentId,
				es.StudentId,
				es.PeriodId,
				s.FirstName,
				s.LastName,
				ces.Sequence, --NumeroOrden,
				sh.Id as SubjectHighId,
				ces.CourseId,
				ces.SessionId,
				es.TenantId,
				c.Name as CourseName,
				c.Abbreviation,
				cs.Name as SessionName,
				sh.Name as SubjectHighsName
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,1,0) as Evaluation1
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,2,0) as Evaluation2
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,3,0) as Evaluation3
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,4,0) as Evaluation4
                ,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,6,0) as Evaluation6
                ,dbo.[rep_fn_getOneEvaluationHighByPositionName](1) as Evaluation1Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](2) as Evaluation2Name	
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](3) as Evaluation3Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](4) as Evaluation4Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](5) as Evaluation5Name
				,dbo.[rep_fn_getOneEvaluationHighByPositionName](6) as Evaluation6Name
				,dbo.[rep_fn_getOneEvaluationHighByPosition](es.Id,sh.Id,es.TenantId,0,1) as Average2
				,s.FirstName + ' ' + s.LastName as StudentFullName
				,(CASE c.Id
					WHEN 11 THEN 'Reports/Images/Finals/R67_IMAGEN11GRADO1.jpg' --	Primero de Secundaria
					WHEN 12 THEN 'Reports/Images/Finals/R67_IMAGEN12GRADO1.jpg' --	Segundo de Secundaria
					WHEN 13 THEN 'Reports/Images/Finals/R67_IMAGEN13GRADO1.jpg' --	Tercero de Secundaria
					WHEN 14 THEN 'Reports/Images/Finals/R67_IMAGEN14GRADO1.jpg' --	Cuarto de Secundaria
					WHEN 15 THEN 'Reports/Images/Finals/R67_IMAGEN15GRADO1.jpg' --	Quinto de Secundaria
					WHEN 18 THEN 'Reports/Images/Finals/R67_IMAGEN18GRADO1.jpg' --	Sexto de Secundaria
					WHEN 19 THEN 'Reports/Images/Finals/R67_IMAGEN19GRADO1.jpg' --	Cuarto de Secundario Técnico
					WHEN 20 THEN 'Reports/Images/Finals/R67_IMAGEN20GRADO1.jpg' --	Quinto de Secundario Técnico
					WHEN 21 THEN 'Reports/Images/Finals/R67_IMAGEN21GRADO1.jpg' --	Sexto de Secundario Técnico
				END) AS Imagen1
				,'Reports/Images/Finals/R67_IMAGEN11GRADO2.jpg' as Imagen2
				,'' as Imagen3
				,'' as Imagen4
				,'' as Imagen5
			FROM 
				EnrollmentStudents es 
			INNER JOIN 
				CourseEnrollmentStudents ces ON es.Id = ces.EnrollmentStudentId and es.TenantId = ces.TenantId
			INNER JOIN 
				Students s ON es.StudentId = s.id
			INNER JOIN 
				Courses c ON ces.CourseId = c.id
			INNER JOIN 
				CourseSessions cs ON ces.SessionId = cs.id
			INNER JOIN 
				SubjectHighs sh ON ces.CourseId = sh.CourseId
			INNER JOIN 
				Periods  p ON p.Id = es.PeriodId 
			INNER JOIN 
				AbpTenants t ON t.Id = es.TenantId

			WHERE es.IsActive = 1 AND es.IsDeleted = 0


			");
		}

		public void FunctionFor_rep_fn_getOneEvaluationHighByPosition()
		{
			this.context.Database.ExecuteSqlCommand(DeleteFunctionString("rep_fn_getOneEvaluationHighByPosition"));
			this.context.Database.ExecuteSqlCommand(@"
				-- =============================================
				-- Author:		<Eduardo Santana>
				-- Create date: <2018-10-07>
				-- Description:	<Buscar la calificacion de un alunno, para una posicion>
				-- =============================================
				CREATE FUNCTION [dbo].[rep_fn_getOneEvaluationHighByPosition]
				(
					-- Add the parameters for the function here
					@EnrollmentStudentId INT,
					@SubjectHighId INT,
					@TenantId INT,
					@PositionId INT,
					@IsTotal BIT
				)
				RETURNS INT
				AS
				BEGIN
					-- Declare the return variable here
					DECLARE @ResultVar INT = 0;
					DECLARE @RowsTotal INT = 0;
					DECLARE @TempVal INT = 0;

					IF (@IsTotal = 1)
					BEGIN
						
						SET @TempVal = ISNULL(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,@SubjectHighId,@TenantId,1,0),0);
						IF(@TempVal > 0)
						BEGIN 
							SET @ResultVar += @TempVal;
							SET @RowsTotal += 1;
						END 
						
						SET @TempVal = ISNULL(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,@SubjectHighId,@TenantId,2,0),0);
						IF(@TempVal > 0)
						BEGIN 
							SET @ResultVar += @TempVal;
							SET @RowsTotal += 1;
						END 

						SET @TempVal = ISNULL(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,@SubjectHighId,@TenantId,3,0),0);
						IF(@TempVal > 0)
						BEGIN 
							SET @ResultVar += @TempVal;
							SET @RowsTotal += 1;
						END 

						SET @TempVal = ISNULL(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,@SubjectHighId,@TenantId,4,0),0);
						IF(@TempVal > 0)
						BEGIN 
							SET @ResultVar += @TempVal;
							SET @RowsTotal += 1;
						END 

						SET @TempVal = ISNULL(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,@SubjectHighId,@TenantId,5,0),0);
						IF(@TempVal > 0)
						BEGIN 
							SET @ResultVar += @TempVal;
							SET @RowsTotal += 1;
						END 

						
						SET @TempVal = ISNULL(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,@SubjectHighId,@TenantId,6,0),0);
						IF(@TempVal > 0)
						BEGIN 
							SET @ResultVar += @TempVal;
							SET @RowsTotal += 1;
						END 

						IF (@ResultVar > 0)
						BEGIN 
							SET @ResultVar = @ResultVar / @RowsTotal;
						END 

					END 
					ELSE 
					BEGIN
						-- Add the T-SQL statements to compute the return value here
						SELECT @ResultVar = AVG(ISNULL([Expression],0)) 
						FROM [dbo].[EvaluationHighs] eh
						inner join EvaluationOrderHighs eoh on eh.EvaluationOrderHighId = eoh.Id
						WHERE eh.[SubjectHighId] = @SubjectHighId
							AND eh.[EnrollmentStudentId] = @EnrollmentStudentId
							AND eh.[TenantId] = @TenantId
							AND  eoh.EvaluationPositionHighId = @PositionId
						;
						
					END
					
					IF(@ResultVar = 0)
					BEGIN
						SET @ResultVar = NULL;
					END

					-- Return the result of the function
					RETURN @ResultVar;

				END
			
			");
		}

		public void FunctionFor_rep_fn_getOneEvaluationHighByPositionName()
		{
			this.context.Database.ExecuteSqlCommand(DeleteFunctionString("rep_fn_getOneEvaluationHighByPositionName"));
			this.context.Database.ExecuteSqlCommand(@"
				-- =============================================
				-- Author:		<Eduardo Santana>
				-- Create date: <2018-10-07>
				-- Description:	<Buscar el nombre deL position>
				-- =============================================
				CREATE FUNCTION [dbo].[rep_fn_getOneEvaluationHighByPositionName]
				(
					-- Add the parameters for the function here
					@PositionId int 
				)
				RETURNS VARCHAR(256)
				AS
				BEGIN
					-- Declare the return variable here
					DECLARE @ResultVar VARCHAR(256) ='';

					-- Add the T-SQL statements to compute the return value here
					SELECT TOP 1 @ResultVar = eph.Description
					FROM EvaluationPositionHighs eph
					WHERE eph.Id = @PositionId
					
					-- Return the result of the function
					RETURN @ResultVar;

				END
			
			");
		}

		public void InsertingReportReporteFinaldeCurso()
		{
			this.context.Database.ExecuteSqlCommand(@" 
				DECLARE @ReportCode varchar(2) = '67';
				if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
				begin
					INSERT INTO [dbo].[Reports]
									([ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
								VALUES
									(  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  @ReportCode -- <ReportCode, nvarchar(80),>
									,  'Reporte Final de Curso' -- <ReportName, nvarchar(100),>
									,  'rep_view_EvaluationFinalReport' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Finals\EnrolmentStudentFinal2.rdlc' -- <ReportPath, nvarchar(500),>
									,  1 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									);

				  INSERT INTO [dbo].[ReportsFilters]
						([ReportsId]
						,[IsDeleted]
						,[IsActive]
						,[CreatorUserId]
						,[CreationTime]
						,[LastModifierUserId]
						,[LastModificationTime]
						,[DeleterUserId]
						,[DeletionTime]
						,[ReportId]
						,[Order]
						,[Name]
						,[DisplayName]
						,[DataField]
						,[DataTypeId]
						,[Range]
						,[OnlyParameter]
						,[UrlService]
						,[FieldService]
						,[DisplayNameService]
						,[DependencyField]
						,[Operator]
						,[DisplayNameRange]
						,[Required])
					VALUES
						((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
						,  0  -- <IsDeleted, bit,>
						,  1  -- <IsActive, bit,>
						,  1  -- <CreatorUserId, bigint,>
						,  GETDATE() -- <CreationTime, datetime,>
						,  null -- <LastModifierUserId, bigint,>
						,  null -- <LastModificationTime, datetime,>
						,  null -- <DeleterUserId, bigint,>
						,  null -- <DeletionTime, datetime,>
						,  0 -- <ReportId, int,>
						,  1 -- <Order, int,>
						,  'EnrollmentStudentId' -- <Name, nvarchar(max),>
						,  'EnrollmentStudent' -- <DisplayName, nvarchar(max),>
						,  'EnrollmentStudentId' -- <DataField, nvarchar(max),>
						,  1 -- <DataTypeId, int,>
						,  0 -- <Range, bit,>
						,  0 -- <OnlyParameter, bit,>
						,  null -- <UrlService, nvarchar(max),>
						,  null   -- <FieldService, nvarchar(max),>
						,  null -- <DisplayNameService, nvarchar(max),>
						,  null -- <DependencyField, nvarchar(max),>
						,  null -- <Operator, nvarchar(max),>
						,  null -- <DisplayNameRange, nvarchar(max),>
						,  1 -- <Required, bit,>
						);

				end
");
		}
        
        public void ReporteDeActaFinalDeCalificaciones()
        {
            this.context.Database.ExecuteSqlCommand(@" 
				DECLARE @ReportCode varchar(2) = '111';
				if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
				begin
					INSERT INTO [dbo].[Reports]
									([ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
								VALUES
									(  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  @ReportCode -- <ReportCode, nvarchar(80),>
									,  'Acta de Calificacion Final' -- <ReportName, nvarchar(100),>
									,  'Rep_View_ActaFinalCalification' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Finals\ActaCalificacionFinalCuerpo.rdlc' -- <ReportPath, nvarchar(500),>
									,  1 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									);

				  INSERT INTO [dbo].[ReportsFilters]
						([ReportsId]
						,[IsDeleted]
						,[IsActive]
						,[CreatorUserId]
						,[CreationTime]
						,[LastModifierUserId]
						,[LastModificationTime]
						,[DeleterUserId]
						,[DeletionTime]
						,[ReportId]
						,[Order]
						,[Name]
						,[DisplayName]
						,[DataField]
						,[DataTypeId]
						,[Range]
						,[OnlyParameter]
						,[UrlService]
						,[FieldService]
						,[DisplayNameService]
						,[DependencyField]
						,[Operator]
						,[DisplayNameRange]
						,[Required])
					VALUES
						((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
						,  0  -- <IsDeleted, bit,>
						,  1  -- <IsActive, bit,>
						,  1  -- <CreatorUserId, bigint,>
						,  GETDATE() -- <CreationTime, datetime,>
						,  null -- <LastModifierUserId, bigint,>
						,  null -- <LastModificationTime, datetime,>
						,  null -- <DeleterUserId, bigint,>
						,  null -- <DeletionTime, datetime,>
						,  0 -- <ReportId, int,>
						,  1 -- <Order, int,>
						,  'CourseId' -- <Name, nvarchar(max),>
						,  'Curso' -- <DisplayName, nvarchar(max),>
						,  'Curso' -- <DataField, nvarchar(max),>
						,  1 -- <DataTypeId, int,>
						,  0 -- <Range, bit,>
						,  0 -- <OnlyParameter, bit,>
						,  null -- <UrlService, nvarchar(max),>
						,  null   -- <FieldService, nvarchar(max),>
						,  null -- <DisplayNameService, nvarchar(max),>
						,  null -- <DependencyField, nvarchar(max),>
						,  null -- <Operator, nvarchar(max),>
						,  null -- <DisplayNameRange, nvarchar(max),>
						,  1 -- <Required, bit,>
						);

				  INSERT INTO [dbo].[ReportsFilters]
						([ReportsId]
						,[IsDeleted]
						,[IsActive]
						,[CreatorUserId]
						,[CreationTime]
						,[LastModifierUserId]
						,[LastModificationTime]
						,[DeleterUserId]
						,[DeletionTime]
						,[ReportId]
						,[Order]
						,[Name]
						,[DisplayName]
						,[DataField]
						,[DataTypeId]
						,[Range]
						,[OnlyParameter]
						,[UrlService]
						,[FieldService]
						,[DisplayNameService]
						,[DependencyField]
						,[Operator]
						,[DisplayNameRange]
						,[Required])
					VALUES
						((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
						,  0  -- <IsDeleted, bit,>
						,  1  -- <IsActive, bit,>
						,  1  -- <CreatorUserId, bigint,>
						,  GETDATE() -- <CreationTime, datetime,>
						,  null -- <LastModifierUserId, bigint,>
						,  null -- <LastModificationTime, datetime,>
						,  null -- <DeleterUserId, bigint,>
						,  null -- <DeletionTime, datetime,>
						,  0 -- <ReportId, int,>
						,  1 -- <Order, int,>
						,  'Session' -- <Name, nvarchar(max),>
						,  'Session' -- <DisplayName, nvarchar(max),>
						,  'SessionId' -- <DataField, nvarchar(max),>
						,  1 -- <DataTypeId, int,>
						,  0 -- <Range, bit,>
						,  0 -- <OnlyParameter, bit,>
						,  null -- <UrlService, nvarchar(max),>
						,  null   -- <FieldService, nvarchar(max),>
						,  null -- <DisplayNameService, nvarchar(max),>
						,  null -- <DependencyField, nvarchar(max),>
						,  null -- <Operator, nvarchar(max),>
						,  null -- <DisplayNameRange, nvarchar(max),>
						,  1 -- <Required, bit,>
						);

				end
");
        }

        public void ViewFor_rep_vw_StudentWithNotProyect()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_vw_StudentWithNotProyect"));
			this.context.Database.ExecuteSqlCommand(@"
			-- =============================================
			-- Author:        <Eduardo Santana>
			-- Create date: <2018 - 09 - 09>
			-- Description:   <Se busca el listado de estsudiantes WithNotProyect>
			-- =============================================
			CREATE VIEW [dbo].[rep_vw_StudentWithNotProyect]
			as
			Select 
			'' as StudentWithNotProyect,
				stu.LastName,
				stu.FirstName,
				enrxsec.Sequence,
				enr.FirstName + ' ' + enr.LastName as FullName,
				cou.Name CourseName,
				enrxstu.TenantId,
				stu.Id as StudentId,
				cou.Id as CourseId,
				enr.Id as EnrollmentId,
				enrxstu.PeriodId ,
				pr.Period,
				tn.Name Tenant,
				cou.LevelId,
				lv.Name AS LevelName
			From Students stu
			inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
			inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId
			inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id
			inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
			inner join Courses cou on cou.Id = couxenrxstu.CourseId
			INNER JOIN dbo.Periods pr ON pr.Id = enrxstu.PeriodId
			LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId
			INNER JOIN dbo.Levels lv ON lv.Id = cou.LevelId
			LEFT JOIN dbo.PaymentProjections pp2
			ON pp2.enrollmentStudentId = enrxstu.Id
			and pp2.TenantId = enrxstu.TenantId
			WHERE stu.IsDeleted = 0 AND
			stu.IsActive = 1 and
			enrxsec.IsDeleted = 0 and
			enrxstu.IsDeleted = 0 and
			tn.PeriodId = enrxstu.PeriodId
			and couxenrxstu.CourseId != 22
			AND pp2.Id IS NULL
			;
			");

		}

		public void InsertingReportStudentWithNotProyect()
		{
			this.context.Database.ExecuteSqlCommand(@" 
				DECLARE @ReportCode varchar(2) = '68';
				if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
				begin
					INSERT INTO [dbo].[Reports]
									([ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
								VALUES
									( 3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  @ReportCode -- <ReportCode, nvarchar(80),>
									,  'Estudiantes Sin Proyeccion de Pago' -- <ReportName, nvarchar(100),>
									,  'rep_vw_StudentWithNotProyect' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Generales\StudentByCourse.rdlc' -- <ReportPath, nvarchar(500),>
									,  1 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									);

				  INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'School' -- <Name, nvarchar(max),>
								   ,  'School' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'levelId' -- <Name, nvarchar(max),>
								   ,  'Level' -- <DisplayName, nvarchar(max),>
								   ,  'levelId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/level/GetAllLevelsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'CourseId' -- <Name, nvarchar(max),>
								   ,  'Course' -- <DisplayName, nvarchar(max),>
								   ,  'CourseId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/course/GetAllCoursesForCombo?levelId={{scope.levelId}}' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  'levelId' -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );
				end
");
		}

		public void InsertingReportEvaluationsByCourseFinals()
		{
			this.context.Database.ExecuteSqlCommand(@" 
			DECLARE @ReportCode varchar(2) = '69';
			if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
				begin
					INSERT INTO [dbo].[Reports]
									([ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
								VALUES
									(  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  @ReportCode -- <ReportCode, nvarchar(80),>
									,  'Reporte Final' -- <ReportName, nvarchar(100),>
									,  'sp_FinalReportByCourse' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Finals\EvaluationsByCourseFinals02.rdlc' -- <ReportPath, nvarchar(500),>
									,  2 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									);

				  INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'TenantId' -- <Name, nvarchar(max),>
								   ,  'Tenant' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  4 -- <Order, int,>
								   ,  'courseId' -- <Name, nvarchar(max),>
								   ,  'Course' -- <DisplayName, nvarchar(max),>
								   ,  'courseId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/course/GetAllCoursesForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

					INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								  ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null
								   ,0
								   ,4
								   ,'SessionId'
								   ,'Session'
								   ,'SessionId'
								   ,1
								   ,0
								   ,0
								   ,'api/services/app/courseSession/getAllCourseSessionsForCombo?courseId={{scope.courseId}}'
								   ,'id'
								   ,'name'
								   ,'courseId'
								   ,null
								   ,null
								   ,1);

							INSERT INTO [dbo].[ReportsFilters]
										   ([ReportsId]
										   ,[IsDeleted]
										   ,[IsActive]
										   ,[CreatorUserId]
										   ,[CreationTime]
										   ,[LastModifierUserId]
										   ,[LastModificationTime]
										   ,[DeleterUserId]
										   ,[DeletionTime]
										   ,[ReportId]
										   ,[Order]
										   ,[Name]
										   ,[DisplayName]
										   ,[DataField]
										   ,[DataTypeId]
										   ,[Range]
										   ,[OnlyParameter]
										   ,[UrlService]
										   ,[FieldService]
										   ,[DisplayNameService]
										   ,[DependencyField]
										   ,[Operator]
										   ,[DisplayNameRange]
										   ,[Required])
									 VALUES
										  ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
										   ,  0  -- <IsDeleted, bit,>
										   ,  1  -- <IsActive, bit,>
										   ,  1  -- <CreatorUserId, bigint,>
										   ,  GETDATE() -- <CreationTime, datetime,>
										   ,  null -- <LastModifierUserId, bigint,>
										   ,  null -- <LastModificationTime, datetime,>
										   ,  null -- <DeleterUserId, bigint,>
										   ,  null -- <DeletionTime, datetime,>
										   ,  0 -- <ReportId, int,>
										   ,  2 -- <Order, int,>
										   ,  'PeriodId' -- <Name, nvarchar(max),>
										   ,  'Period' -- <DisplayName, nvarchar(max),>
										   ,  'PeriodId' -- <DataField, nvarchar(max),>
										   ,  1 -- <DataTypeId, int,>
										   ,  0 -- <Range, bit,>
										   ,  0 -- <OnlyParameter, bit,>
										   ,  'api/services/app/period/getAllPeriodsForCombo' -- <UrlService, nvarchar(max),>
										   ,  'id'   -- <FieldService, nvarchar(max),>
										   ,  'description' -- <DisplayNameService, nvarchar(max),>
										   ,  null -- <DependencyField, nvarchar(max),>
										   ,  null -- <Operator, nvarchar(max),>
										   ,  null -- <DisplayNameRange, nvarchar(max),>
										   ,  1 -- <Required, bit,>
										   );
				end     
");
		}

		public void ProcedureForFinalReportByCourse()
		{
			this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_FinalReportByCourse"));
			this.context.Database.ExecuteSqlCommand(@"			
			-- =============================================
			-- Author:		<Eduardo Santana Severino>
			-- Create date: <2018 - 10 - 16>
			-- Description:	<Buscar informacion ordenada de materias>
			-- =============================================
			CREATE PROCEDURE sp_FinalReportByCourse
			(
				@CourseId INT,
				@SessionId INT,
				@TenantId INT,
				@PeriodId INT
			)
			AS
			BEGIN

				CREATE TABLE #retVal_table          
				( 
					EducationalCenterName Varchar(256),
					EducationalCenterAddress Varchar(256),
					SchoolYear Varchar(256),
					EnrollmentStudentId INT,
					StudentId Varchar(256),
					PeriodId Varchar(256),
					FirstName Varchar(256),
					LastName Varchar(256),
					Sequence Varchar(256),
					CourseId INT,
					SessionId Varchar(256),
					TenantId INT,
					CourseName Varchar(256),
					Abbreviation Varchar(256),
					SessionName Varchar(256),
					StudentFullName Varchar(256),
					RowIndex int
				);

				DECLARE @EducationalCenterName Varchar(256) = '',
						@EducationalCenterAddress Varchar(256) = '',
						@SchoolYear Varchar(256) = '',
						@EnrollmentStudentId INT = NULL,
						@StudentId Varchar(256) = '',
						@FirstName Varchar(256) = '',
						@LastName Varchar(256) = '',
						@Sequence Varchar(256) = '',
						@CourseName Varchar(256) = '',
						@Abbreviation Varchar(256) = '',
						@SessionName Varchar(256) = '',
						@StudentFullName Varchar(256) = '',
						@Index Int = 0,
						@QtyOfSubject Int = 0,
						@IndexOfSubject Int = 0,
						@SubjectHighId Varchar(256) = '',
						@SubjectHighsName Varchar(256) = '',
						@AverageEndYear Varchar(256) = '',
						@Completive Varchar(256) = '',
						@Extra Varchar(256) = '';
  
				DECLARE STUDENTS_CURSOR CURSOR FOR     
				SELECT
					t.Name as EducationalCenterName, --NombreDelCentro,
					t.[Address] as EducationalCenterAddress, --DireccionCentroEducativo,
					p.[Description] as SchoolYear, --AnoEscolar
					es.Id as EnrollmentStudentId,
					es.StudentId,
					s.FirstName,
					s.LastName,
					ces.Sequence, --NumeroOrden,
					c.Name as CourseName,
					c.Abbreviation,
					cs.Name as SessionName,
					s.FirstName + ' ' + s.LastName as StudentFullName
				FROM 
					EnrollmentStudents es 
				INNER JOIN 
					CourseEnrollmentStudents ces ON es.Id = ces.EnrollmentStudentId and es.TenantId = ces.TenantId
				INNER JOIN 
					Students s ON es.StudentId = s.id
				INNER JOIN 
					Courses c ON ces.CourseId = c.id
				INNER JOIN 
					CourseSessions cs ON ces.SessionId = cs.id
				--INNER JOIN 
				--	SubjectHighs sh ON ces.CourseId = sh.CourseId
				INNER JOIN 
					Periods  p ON p.Id = es.PeriodId 
				INNER JOIN 
					AbpTenants t ON t.Id = es.TenantId
				WHERE   
					es.IsActive = 1 AND 
					es.IsDeleted = 0 AND 
					ces.CourseId = @CourseId AND 
					ces.SessionId = @SessionId AND 
					es.TenantId = @TenantId AND 
					es.PeriodId = @PeriodId
				ORDER BY ces.Sequence;    
  
				OPEN STUDENTS_CURSOR    
				FETCH NEXT FROM STUDENTS_CURSOR     
				INTO	@EducationalCenterName,
						@EducationalCenterAddress,
						@SchoolYear,
						@EnrollmentStudentId,
						@StudentId,
						@FirstName,
						@LastName,
						@Sequence,
						@CourseName,
						@Abbreviation,
						@SessionName,
						@StudentFullName;  
  
				WHILE @@FETCH_STATUS = 0    
				BEGIN    

					SET @Index += 1;

					IF(@Index = 1)
					BEGIN
						SELECT @QtyOfSubject = COUNT(1) FROM SubjectHighs sh WHERE sh.CourseId = @CourseId;
						WHILE (@IndexOfSubject < @QtyOfSubject)
						BEGIN  
							SET @IndexOfSubject += 1;
				
							DECLARE @SQL_ADD_COL VARCHAR(256) = '';

							SET @SQL_ADD_COL = 'ALTER TABLE #retVal_table ADD Column' + CAST(@IndexOfSubject AS varchar(256)) + 'SubjectHighId Varchar(256);';
							EXECUTE (@SQL_ADD_COL);

							SET @SQL_ADD_COL = 'ALTER TABLE #retVal_table ADD Column' + CAST(@IndexOfSubject AS varchar(256)) + 'SubjectHighsName Varchar(256);'; 
							EXECUTE (@SQL_ADD_COL);

							SET @SQL_ADD_COL = 'ALTER TABLE #retVal_table ADD Column' + CAST(@IndexOfSubject AS varchar(256)) + 'AverageEndYear Varchar(256);'; 
							EXECUTE (@SQL_ADD_COL);

							SET @SQL_ADD_COL ='ALTER TABLE #retVal_table ADD Column' + CAST(@IndexOfSubject AS varchar(256)) + 'Completive Varchar(256);'; 
							EXECUTE (@SQL_ADD_COL);

							SET @SQL_ADD_COL = 'ALTER TABLE #retVal_table ADD Column' + CAST(@IndexOfSubject AS varchar(256)) + 'Extra Varchar(256);'; 
							EXECUTE (@SQL_ADD_COL);

							SET @SQL_ADD_COL = 'ALTER TABLE #retVal_table ADD Column' + CAST(@IndexOfSubject AS varchar(256)) + 'Header1 Varchar(256);'; 
							EXECUTE (@SQL_ADD_COL);

							SET @SQL_ADD_COL = 'ALTER TABLE #retVal_table ADD Column' + CAST(@IndexOfSubject AS varchar(256)) + 'Header2 Varchar(256);'; 
							EXECUTE (@SQL_ADD_COL);

							SET @SQL_ADD_COL = 'ALTER TABLE #retVal_table ADD Column' + CAST(@IndexOfSubject AS varchar(256)) + 'Header3 Varchar(256);'; 
							EXECUTE (@SQL_ADD_COL);

						END;

					END;
	
					INSERT INTO #retVal_table (
						EducationalCenterName,
						EducationalCenterAddress,
						SchoolYear,
						EnrollmentStudentId,
						StudentId,
						PeriodId,
						FirstName,
						LastName,
						Sequence,
						CourseId,
						SessionId,
						TenantId,
						CourseName,
						Abbreviation,
						SessionName,
						StudentFullName,
						RowIndex)
					VALUES (
						@EducationalCenterName, -- EducationalCenterName,
						@EducationalCenterAddress, -- EducationalCenterAddress,
						@SchoolYear, -- SchoolYear,
						@EnrollmentStudentId, -- EnrollmentStudentId,
						@StudentId, -- StudentId,
						@PeriodId, -- PeriodId,
						@FirstName, -- FirstName,
						@LastName, -- LastName,
						@Sequence, -- Sequence,
						@CourseId, -- CourseId,
						@SessionId, -- SessionId,
						@TenantId, -- TenantId,
						@CourseName, -- CourseName,
						@Abbreviation, -- Abbreviation,
						@SessionName, -- SessionName,
						@StudentFullName, -- StudentFullName,
						@Index);

					SET @IndexOfSubject = 0;
		
					DECLARE Subject_CURSOR CURSOR FOR     
					SELECT
						CAST(sh.Id AS VARCHAR(256)) SubjectHighId,
						sh.Name SubjectHighsName,
						ISNULL(CAST(dbo.[rep_fn_getOneEvaluationHighByPosition](@EnrollmentStudentId,sh.Id,@TenantId,0,1) AS VARCHAR(256)),'') as AverageEndYear,
						'' Completive,
						'' Extra
					FROM SubjectHighs sh 
					WHERE sh.CourseId = @CourseId
	
					OPEN Subject_CURSOR    
					FETCH NEXT FROM Subject_CURSOR     
					INTO	@SubjectHighId,
							@SubjectHighsName,
							@AverageEndYear,
							@Completive,
							@Extra; 
			 
					WHILE @@FETCH_STATUS = 0    
					BEGIN    

						SET @IndexOfSubject += 1;

						DECLARE @SQL VARCHAR(4000) = 'UPDATE #retVal_table 
						SET 
							Column' + CAST(@IndexOfSubject AS varchar(256)) + 'Header1 = ''' + 'Final de Año' + ''',
							Column' + CAST(@IndexOfSubject AS varchar(256)) + 'Header2 = ''' + 'Completivo' + ''',
							Column' + CAST(@IndexOfSubject AS varchar(256)) + 'Header3 = ''' + 'Extraordinario' + ''',
							Column' + CAST(@IndexOfSubject AS varchar(256)) + 'SubjectHighId = ''' + @SubjectHighId + ''',
							Column' + CAST(@IndexOfSubject AS varchar(256)) + 'SubjectHighsName = ''' + @SubjectHighsName + ''',
							Column' + CAST(@IndexOfSubject AS varchar(256)) + 'AverageEndYear = ''' + @AverageEndYear + ''',
							Column' + CAST(@IndexOfSubject AS varchar(256)) + 'Completive = ''' + @Completive + ''',
							Column' + CAST(@IndexOfSubject AS varchar(256)) + 'Extra = ''' + @Extra + '''
						WHERE RowIndex = ' + CAST(@Index AS VARCHAR(256));

						EXECUTE(@SQL);

						FETCH NEXT FROM Subject_CURSOR     
						INTO	@SubjectHighId,
								@SubjectHighsName,
								@AverageEndYear,
								@Completive,
								@Extra;   
   
					END     
					CLOSE Subject_CURSOR;    
					DEALLOCATE Subject_CURSOR;   


				FETCH NEXT FROM STUDENTS_CURSOR     
				INTO	@EducationalCenterName,
						@EducationalCenterAddress,
						@SchoolYear,
						@EnrollmentStudentId,
						@StudentId,
						@FirstName,
						@LastName,
						@Sequence,
						@CourseName,
						@Abbreviation,
						@SessionName,
						@StudentFullName;  
   
				END     
				CLOSE STUDENTS_CURSOR;    
				DEALLOCATE STUDENTS_CURSOR;   

				SELECT * FROM #retVal_table order by cast(sequence as int);

			END
			");
		}

		public void ViewFor_rep_vw_AccountBalance()
		{
			this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_vw_AccountBalance"));
			this.context.Database.ExecuteSqlCommand(@"
			CREATE VIEW rep_vw_AccountBalance
			AS
			SELECT 
			 er.Id AS EnrollmentId,
			 er.FirstName,
			 er.LastName,
			 t.Date ,
			 Number = tt.Abbreviation + '-' + CAST(t.Sequence AS VARCHAR),
			 tt.Abbreviation,
			 t.Sequence,
			 ers.Sequence EnrollmentSequence,
			 cp.Description AS ConceptDescription,
			  t.OriginId,
			  Debit = CASE t.OriginId WHEN 1 THEN t.Amount ELSE 0 END ,
			  Credit = CASE t.OriginId WHEN 2 THEN t.Amount ELSE 0 END ,
			  t.TenantId 
			  FROM Transactions t 
			  INNER JOIN dbo.TransactionTypes tt ON tt.Id = t.TransactionTypeId 
			  INNER JOIN dbo.Enrollments er ON er.Id = t.EnrollmentId
			  INNER JOIN dbo.Concepts cp ON cp.Id = t.ConceptId
			  INNER JOIN dbo.EnrollmentSequences ers ON ers.EnrollmentId = er.Id AND ers.TenantId = t.TenantId  
			  WHERE  t.IsDeleted = 0 AND t.IsActive = 1;
			");
		}

		public void InsertingReportEnrollmentBalanceListByEnrollment()
		{
			this.context.Database.ExecuteSqlCommand(@" 
				DECLARE @ReportCode varchar(2) = '70';
				if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
				begin
					INSERT INTO [dbo].[Reports]
									([ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
								VALUES
									(  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  @ReportCode -- <ReportCode, nvarchar(80),>
									,  'CxC Tutores por Curso' -- <ReportName, nvarchar(100),>
									,  'sp_rep_AccountBalanceListByEnrolment' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Financiero\EnrollmentBalanceListByStudent.rdlc' -- <ReportPath, nvarchar(500),>
									,  2 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									);

				  INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  0 -- <Order, int,>
								   ,  'TenantId' -- <Name, nvarchar(max),>
								   ,  'School' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'levelId' -- <Name, nvarchar(max),>
								   ,  'Level' -- <DisplayName, nvarchar(max),>
								   ,  'levelId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  1 -- <OnlyParameter, bit,>
								   ,  'api/services/app/level/GetAllLevelsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  2 -- <Order, int,>
								   ,  'CourseId' -- <Name, nvarchar(max),>
								   ,  'Course' -- <DisplayName, nvarchar(max),>
								   ,  'CourseId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/course/GetAllCoursesForCombo?levelId={{scope.levelId}}' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  'levelId' -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

								   INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  4 -- <Order, int,>
								   ,  'ToDate' -- <Name, nvarchar(max),>
								   ,  'Fecha Corte' -- <DisplayName, nvarchar(max),>
								   ,  'ToDate' -- <DataField, nvarchar(max),>
								   ,  3 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  null -- <UrlService, nvarchar(max),>
								   ,  null   -- <FieldService, nvarchar(max),>
								   ,  null -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );
				end
			");
		}
        
        public void ProcedureFor_rep_AccountBalanceListByEnrolment()
        {
            this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_rep_AccountBalanceListByEnrolment"));
            this.context.Database.ExecuteSqlCommand(@"			
			-- =============================================
			-- Author:		<Eduardo Santana>   Modified:   <José Manzanillo> <2018-12-11>
			-- Create date: <2018 - 11 - 22>
			-- Description:	<Buscar informacion agrupada filtrada por fecha>
			-- =============================================
			CREATE PROCEDURE [dbo].[sp_rep_AccountBalanceListByEnrolment]
				-- Add the parameters for the stored procedure here
				@TenantId int = NULL,
				@ToDate datetime = NULL,
				@CourseId int = NULL
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				select 
					es.Sequence, 
					Tenant = at.Name, 
					e.id, 
					e.FirstName, 
					e.LastName, 
					EnrollmentName = e.FirstName + ' ' + e.LastName, 
					e.Phone1, 
					e.Phone2, 
					t.TenantId
					,sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) Balance
					,sum( (case t.OriginId when  1 then t.Amount else 0 end)) Debit
					,sum( (case t.OriginId when  2 then t.Amount else 0 end)) Credit
					,e.FirstName StudenFirstName, 
					e.LastName StudenLastName, 
					StudenName = stz.FirstName + ' ' + stz.LastName,
					e.id EnrollmentId,
					c.Id CourseId,
					CourseName = c.Name 
				from 
					Enrollments e 
					inner join EnrollmentSequences es on e.id = es.EnrollmentId
					inner join Transactions t on t.EnrollmentId = e.Id and es.EnrollmentId = t.enrollmentId and t.TenantId = es.TenantId 
					inner join AbpTenants at on at.Id = t.TenantId
					inner join Courses c on c.Id = @CourseId
					inner join EnrollmentStudents estz on estz.EnrollmentId=e.Id
					inner join CourseEnrollmentStudents cestz on cestz.EnrollmentStudentId = estz.Id and cestz.CourseId = c.Id
					inner join Students stz on stz.Id = estz.StudentId 
				where 
					t.TransactionTypeId in (1 , 3) and 
					isnull(t.ReceiptTypeId, 0) not in ( 2 , 3 ) and 
					(t.TenantId = @TenantId or @TenantId is null) and
					t.[Date] < convert(datetime, convert(varchar, @ToDate, 23) + ' 23:59:59:999', 21)
					and es.EnrollmentId in (
						Select 
							enrxstu.EnrollmentId
						From 
						Students stu 
						inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
						inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId
						inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
						inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
						LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId 
						WHERE stu.IsDeleted = 0 AND stu.IsActive = 1 and enrxsec.IsDeleted=0 and enrxstu.IsDeleted=0
						and tn.PeriodId = enrxstu.PeriodId
						and couxenrxstu.CourseId = @CourseId
						and enrxstu.TenantId =  @TenantId
					)
				group by 
					e.id, at.Name,stz.FirstName, stz.LastName, e.Phone1, e.Phone2, es.Sequence, t.TenantId, c.Id, C.Name,e.FirstName,e.LastName
							
			END

			");
        }
        
        public void InsertingReportEnrollmentBalanceListByEnrollmentLess()
        {
            this.context.Database.ExecuteSqlCommand(@" 
				DECLARE @ReportCode varchar(2) = '90';
				if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
				begin
					INSERT INTO [dbo].[Reports]
									([ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
								VALUES
									(  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  @ReportCode -- <ReportCode, nvarchar(80),>
									,  'CxC Tutores por Curso a Favor' -- <ReportName, nvarchar(100),>
									,  'sp_rep_AccountBalanceListByEnrolment_Less' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Financiero\EnrollmentBalanceListByStudent.rdlc' -- <ReportPath, nvarchar(500),>
									,  2 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									);

				  INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  0 -- <Order, int,>
								   ,  'TenantId' -- <Name, nvarchar(max),>
								   ,  'School' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'levelId' -- <Name, nvarchar(max),>
								   ,  'Level' -- <DisplayName, nvarchar(max),>
								   ,  'levelId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  1 -- <OnlyParameter, bit,>
								   ,  'api/services/app/level/GetAllLevelsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  2 -- <Order, int,>
								   ,  'CourseId' -- <Name, nvarchar(max),>
								   ,  'Course' -- <DisplayName, nvarchar(max),>
								   ,  'CourseId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/course/GetAllCoursesForCombo?levelId={{scope.levelId}}' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  'levelId' -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

								   INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  4 -- <Order, int,>
								   ,  'ToDate' -- <Name, nvarchar(max),>
								   ,  'Fecha Corte' -- <DisplayName, nvarchar(max),>
								   ,  'ToDate' -- <DataField, nvarchar(max),>
								   ,  3 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  null -- <UrlService, nvarchar(max),>
								   ,  null   -- <FieldService, nvarchar(max),>
								   ,  null -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );
				end
			");
        }
        
        public void ProcedureFor_rep_AccountBalanceListByEnrolmentLess()
		{
			this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_rep_AccountBalanceListByEnrolment_Less"));
			this.context.Database.ExecuteSqlCommand(@"			
			-- =============================================
			-- Author:	  <José Manzanillo> 
			-- Create date: <2018-12-11>
			-- Description:	<Buscar informacion agrupada filtrada por fecha Saldo a favor>
			-- =============================================
			CREATE PROCEDURE [dbo].[sp_rep_AccountBalanceListByEnrolment_Less]
				-- Add the parameters for the stored procedure here
				@TenantId int = NULL,
				@ToDate datetime = NULL,
				@CourseId int = NULL
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				select 
					es.Sequence, 
					Tenant = at.Name, 
					e.id, 
					e.FirstName, 
					e.LastName, 
					EnrollmentName = e.FirstName + ' ' + e.LastName, 
					e.Phone1, 
					e.Phone2, 
					t.TenantId
					,sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) Balance
					,sum( (case t.OriginId when  1 then t.Amount else 0 end)) Debit
					,sum( (case t.OriginId when  2 then t.Amount else 0 end)) Credit
					,e.FirstName StudenFirstName, 
					e.LastName StudenLastName, 
					StudenName = stz.FirstName + ' ' + stz.LastName,
					e.id EnrollmentId,
					c.Id CourseId,
					CourseName = c.Name 
				from 
					Enrollments e 
					inner join EnrollmentSequences es on e.id = es.EnrollmentId
					inner join Transactions t on t.EnrollmentId = e.Id and es.EnrollmentId = t.enrollmentId and t.TenantId = es.TenantId 
					inner join AbpTenants at on at.Id = t.TenantId
					inner join Courses c on c.Id = @CourseId
					inner join EnrollmentStudents estz on estz.EnrollmentId=e.Id
					inner join CourseEnrollmentStudents cestz on cestz.EnrollmentStudentId = estz.Id and cestz.CourseId = c.Id
					inner join Students stz on stz.Id = estz.StudentId 

				where 
					t.TransactionTypeId in (1 , 3) and 
					isnull(t.ReceiptTypeId, 0) not in ( 2 , 3 ) and 
					(t.TenantId = @TenantId or @TenantId is null) and
					t.[Date] < convert(datetime, convert(varchar, @ToDate, 23) + ' 23:59:59:999', 21)
					and es.EnrollmentId in (
						Select 
							enrxstu.EnrollmentId
						From 
						Students stu 
						inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
						inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId
						inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
						inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
						LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId 
						WHERE stu.IsDeleted = 0 AND stu.IsActive = 1 and enrxsec.IsDeleted=0 and enrxstu.IsDeleted=0
						and tn.PeriodId = enrxstu.PeriodId
						and couxenrxstu.CourseId = @CourseId
						and enrxstu.TenantId =  @TenantId
					)
				group by 
					e.id, at.Name,stz.FirstName, stz.LastName, e.Phone1, e.Phone2, es.Sequence, t.TenantId, c.Id, C.Name,e.FirstName,e.LastName
				having 	sum( (case t.OriginId when  2 then t.Amount else 0 end)) >= sum( (case t.OriginId when  1 then t.Amount else 0 end))	
							
			END

			");
		}
        
        public void InsertingReportEnrollmentBalanceListByEnrollmentMore()
        {
            this.context.Database.ExecuteSqlCommand(@" 
				DECLARE @ReportCode varchar(2) = '91';
				if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
				begin
					INSERT INTO [dbo].[Reports]
									([ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
								VALUES
									(  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  @ReportCode -- <ReportCode, nvarchar(80),>
									,  'CxC Tutores por Curso a Deber' -- <ReportName, nvarchar(100),>
									,  'sp_rep_AccountBalanceListByEnrolment_More' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Financiero\EnrollmentBalanceListByStudent.rdlc' -- <ReportPath, nvarchar(500),>
									,  2 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									);

				  INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  0 -- <Order, int,>
								   ,  'TenantId' -- <Name, nvarchar(max),>
								   ,  'School' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'levelId' -- <Name, nvarchar(max),>
								   ,  'Level' -- <DisplayName, nvarchar(max),>
								   ,  'levelId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  1 -- <OnlyParameter, bit,>
								   ,  'api/services/app/level/GetAllLevelsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  2 -- <Order, int,>
								   ,  'CourseId' -- <Name, nvarchar(max),>
								   ,  'Course' -- <DisplayName, nvarchar(max),>
								   ,  'CourseId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/course/GetAllCoursesForCombo?levelId={{scope.levelId}}' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  'levelId' -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

								   INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  4 -- <Order, int,>
								   ,  'ToDate' -- <Name, nvarchar(max),>
								   ,  'Fecha Corte' -- <DisplayName, nvarchar(max),>
								   ,  'ToDate' -- <DataField, nvarchar(max),>
								   ,  3 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  null -- <UrlService, nvarchar(max),>
								   ,  null   -- <FieldService, nvarchar(max),>
								   ,  null -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );
				end
			");
        }

        public void ProceduraFor_sp_rep_ProjectionRate()
        {
            this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_rep_ProjectionRate"));
            this.context.Database.ExecuteSqlCommand(@"

			-- =============================================
			-- Author:	  <José Manzanillo> 
			-- Create date: <2018-02-24>
			-- Description:	<Ingresos programados por cursos>
			-- =============================================

             Create procedure [dbo].[sp_rep_ProjectionRate] 
				     @School int = NULL,
				     @PaymentDate datetime = NULL,
					 @PaymentDateRange datetime = NULL,
					 @Course int = NULL,
					 @Period int = NULL
                    
					as begin
              Select pp.Amount,
         pp.EnrollmentStudentId,
                 pp.PaymentDate,
                    pp.TenantId,
             enstu.EnrollmentId,
                enstu.StudentId,
                 enstu.PeriodId,
                 concat (enr.FirstName,' ',enr.LastName) as EnrollmentName,
                 ensec.Sequence as TutorEnrollment,
				 concat(stu.FirstName,' ',stu.LastName) as StudentName,
                   cee.CourseId,
                   cee.Sequence as StudentNumber,
                   cee.SessionId,
                   cur.Name as Curso,
				   Mesensualidad =(Select TotalAmount/(Select count(*) from PaymentDates
				where TenantId=@School
				and IsDeleted=0
				and Sequence<>0) from CourseValues
				where CourseId=@Course
				and TenantId=@School
				and PeriodId=@Period)
			  
                 from PaymentProjections pp
                   inner join EnrollmentStudents enstu on enstu.id=pp.EnrollmentStudentId
                   inner join Enrollments enr on enr.id=enstu.EnrollmentId
                   inner join EnrollmentSequences ensec on ensec.EnrollmentId= enr.Id and ensec.TenantId=enstu.TenantId
                   inner join Students  stu on stu.id=enstu.StudentId
                   inner join CourseEnrollmentStudents cee on cee.EnrollmentStudentId = enstu.Id
                   inner join Courses cur on cur.Id=cee.CourseId
                   
                where pp.IsDeleted=0 
                   and pp.IsActive=1
				   and pp.TenantId =@School
				   and cee.CourseId=@Course
				   and enstu.PeriodId=@Period
				   and pp.PaymentDate between @PaymentDate and @PaymentDateRange


				select TotalAmount/(Select max(Sequence) as Total from PaymentDates
				                           where TenantId=@School) as Presupuestado
				from CourseValues
				where CourseId=@Course
					and TenantId = @School
					and PeriodId=@Period
			
				end

          ");

        }
        
        public void ProcedureFor_rep_AccountBalanceListByEnrolmentMore()
        {
            this.context.Database.ExecuteSqlCommand(DeleteProcedureString("sp_rep_AccountBalanceListByEnrolment_More"));
            this.context.Database.ExecuteSqlCommand(@"			
			-- =============================================
			-- Author:	  <José Manzanillo> 
			-- Create date: <2018-12-11>
			-- Description:	<Buscar informacion agrupada filtrada por fecha Saldo a Deber>
			-- =============================================
			CREATE PROCEDURE [dbo].[sp_rep_AccountBalanceListByEnrolment_More]
				-- Add the parameters for the stored procedure here
				@TenantId int = NULL,
				@ToDate datetime = NULL,
				@CourseId int = NULL
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				select 
					es.Sequence, 
					Tenant = at.Name, 
					e.id, 
					e.FirstName, 
					e.LastName, 
					EnrollmentName = e.FirstName + ' ' + e.LastName, 
					e.Phone1, 
					e.Phone2, 
					t.TenantId
					,sum(t.Amount * (case t.OriginId when  2 then -1 else 1 end)) Balance
					,sum( (case t.OriginId when  1 then t.Amount else 0 end)) Debit
					,sum( (case t.OriginId when  2 then t.Amount else 0 end)) Credit
					,e.FirstName StudenFirstName, 
					e.LastName StudenLastName, 
					StudenName = stz.FirstName + ' ' + stz.LastName,
					e.id EnrollmentId,
					c.Id CourseId,
					CourseName = c.Name 
				from 
					Enrollments e 
					inner join EnrollmentSequences es on e.id = es.EnrollmentId
					inner join Transactions t on t.EnrollmentId = e.Id and es.EnrollmentId = t.enrollmentId and t.TenantId = es.TenantId 
					inner join AbpTenants at on at.Id = t.TenantId
					inner join Courses c on c.Id = @CourseId
					inner join EnrollmentStudents estz on estz.EnrollmentId=e.Id
					inner join CourseEnrollmentStudents cestz on cestz.EnrollmentStudentId = estz.Id and cestz.CourseId = c.Id
					inner join Students stz on stz.Id = estz.StudentId 

				where 
					t.TransactionTypeId in (1 , 3) and 
					isnull(t.ReceiptTypeId, 0) not in ( 2 , 3 ) and 
					(t.TenantId = @TenantId or @TenantId is null) and
					t.[Date] < convert(datetime, convert(varchar, @ToDate, 23) + ' 23:59:59:999', 21)
					and es.EnrollmentId in (
						Select 
							enrxstu.EnrollmentId
						From 
						Students stu 
						inner join EnrollmentStudents enrxstu on stu.Id = enrxstu.StudentId
						inner join EnrollmentSequences enrxsec on enrxstu.EnrollmentId = enrxsec.EnrollmentId
						inner join Enrollments enr on enrxstu.EnrollmentId = enr.Id 
						inner join CourseEnrollmentStudents couxenrxstu on enrxstu.Id = couxenrxstu.EnrollmentStudentId
						LEFT JOIN dbo.AbpTenants tn ON tn.Id = enrxstu.TenantId 
						WHERE stu.IsDeleted = 0 AND stu.IsActive = 1 and enrxsec.IsDeleted=0 and enrxstu.IsDeleted=0
						and tn.PeriodId = enrxstu.PeriodId
						and couxenrxstu.CourseId = @CourseId
						and enrxstu.TenantId =  @TenantId
					)
				group by 
					e.id, at.Name,stz.FirstName, stz.LastName, e.Phone1, e.Phone2, es.Sequence, t.TenantId, c.Id, C.Name,e.FirstName,e.LastName
				having 	sum( (case t.OriginId when  2 then t.Amount else 0 end)) < sum( (case t.OriginId when  1 then t.Amount else 0 end))		
							
			END

			");
        }

		public void InsertingReportStudentsTutores()
		{
			this.context.Database.ExecuteSqlCommand(@" 
				DECLARE @ReportCode varchar(2) = '71';
				if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
				begin
				INSERT INTO [dbo].[Reports]
									([ReportsTypesId]
									,[IsDeleted]
									,[IsActive]
									,[CreatorUserId]
									,[CreationTime]
									,[LastModifierUserId]
									,[LastModificationTime]
									,[DeleterUserId]
									,[DeletionTime]
									,[ReportCode]
									,[ReportName]
									,[View]
									,[ReportRoot]
									,[ReportPath]
									,[ReportDataSourceId]
									,[FilterByTenant]
									,[IsForTenant])
								VALUES
									(  3  -- <ReportsTypesId, int,>
									,  0  -- <IsDeleted, bit,>
									,  1  -- <IsActive, bit,>
									,  1  -- <CreatorUserId, bigint,>
									,  GETDATE() -- <CreationTime, datetime,>
									,  null -- <LastModifierUserId, bigint,>
									,  null -- <LastModificationTime, datetime,>
									,  null -- <DeleterUserId, bigint,>
									,  null -- <DeletionTime, datetime,>
									,  @ReportCode -- <ReportCode, nvarchar(80),>
									,  'Estudiantes Matriculas' -- <ReportName, nvarchar(100),>
									,  'rep_vw_StudentByCourse' -- <View, nvarchar(max),>
									,  'Reports\' -- <ReportRoot, nvarchar(100),>
									,  'RdlcFiles\Generales\StudentByCourseTutores.rdlc' -- <ReportPath, nvarchar(500),>
									,  1 -- <ReportDataSourceId, int,>
									,  1 -- <FilterByTenant, bit,>
									,  2 -- <IsForTenant, int,>
									);

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'School' -- <Name, nvarchar(max),>
								   ,  'School' -- <DisplayName, nvarchar(max),>
								   ,  'TenantId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'levelId' -- <Name, nvarchar(max),>
								   ,  'Level' -- <DisplayName, nvarchar(max),>
								   ,  'levelId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/level/GetAllLevelsForCombo' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  null -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );

				INSERT INTO [dbo].[ReportsFilters]
								   ([ReportsId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime]
								   ,[ReportId]
								   ,[Order]
								   ,[Name]
								   ,[DisplayName]
								   ,[DataField]
								   ,[DataTypeId]
								   ,[Range]
								   ,[OnlyParameter]
								   ,[UrlService]
								   ,[FieldService]
								   ,[DisplayNameService]
								   ,[DependencyField]
								   ,[Operator]
								   ,[DisplayNameRange]
								   ,[Required])
							 VALUES
								   ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
								   ,  0  -- <IsDeleted, bit,>
								   ,  1  -- <IsActive, bit,>
								   ,  1  -- <CreatorUserId, bigint,>
								   ,  GETDATE() -- <CreationTime, datetime,>
								   ,  null -- <LastModifierUserId, bigint,>
								   ,  null -- <LastModificationTime, datetime,>
								   ,  null -- <DeleterUserId, bigint,>
								   ,  null -- <DeletionTime, datetime,>
								   ,  0 -- <ReportId, int,>
								   ,  1 -- <Order, int,>
								   ,  'CourseId' -- <Name, nvarchar(max),>
								   ,  'Course' -- <DisplayName, nvarchar(max),>
								   ,  'CourseId' -- <DataField, nvarchar(max),>
								   ,  1 -- <DataTypeId, int,>
								   ,  0 -- <Range, bit,>
								   ,  0 -- <OnlyParameter, bit,>
								   ,  'api/services/app/course/GetAllCoursesForCombo?levelId={{scope.levelId}}' -- <UrlService, nvarchar(max),>
								   ,  'id'   -- <FieldService, nvarchar(max),>
								   ,  'name' -- <DisplayNameService, nvarchar(max),>
								   ,  'levelId' -- <DependencyField, nvarchar(max),>
								   ,  null -- <Operator, nvarchar(max),>
								   ,  null -- <DisplayNameRange, nvarchar(max),>
								   ,  1 -- <Required, bit,>
								   );
				end
			");
		}

        public void ViewFor_rep_view_VerifoneBreaksDetails()
        {
            this.context.Database.ExecuteSqlCommand(DeleteViewString("rep_view_VerifoneBreaksDetails"));
            this.context.Database.ExecuteSqlCommand(@"
            Create View rep_view_VerifoneBreaksDetails
            as
             select t.Name Tenant, v.*,v.Amount*(t.CreditCardPercent*0.01) as Reten,v.Amount-(v.Amount*(t.CreditCardPercent*0.01)) as Neto
			from dbo.VerifoneBreaks v
			inner join AbpTenants t on t.Id = v.TenantId;
			");
        }

        public void InsertingReportVerifoneBreaksDetails()
        {
            this.context.Database.ExecuteSqlCommand(@" 
            DECLARE @ReportCode varchar(2) = '72';
            if(not(exists(select ReportCode from Reports where ReportCode = @ReportCode)))
            begin
            INSERT INTO [dbo].[Reports]
					            ([ReportsTypesId]
					            ,[IsDeleted]
					            ,[IsActive]
					            ,[CreatorUserId]
					            ,[CreationTime]
					            ,[LastModifierUserId]
					            ,[LastModificationTime]
					            ,[DeleterUserId]
					            ,[DeletionTime]
					            ,[ReportCode]
					            ,[ReportName]
					            ,[View]
					            ,[ReportRoot]
					            ,[ReportPath]
					            ,[ReportDataSourceId]
					            ,[FilterByTenant]
					            ,[IsForTenant])
				            VALUES
					            (  3  -- <ReportsTypesId, int,>
					            ,  0  -- <IsDeleted, bit,>
					            ,  1  -- <IsActive, bit,>
					            ,  1  -- <CreatorUserId, bigint,>
					            ,  GETDATE() -- <CreationTime, datetime,>
					            ,  null -- <LastModifierUserId, bigint,>
					            ,  null -- <LastModificationTime, datetime,>
					            ,  null -- <DeleterUserId, bigint,>
					            ,  null -- <DeletionTime, datetime,>
					            ,  @ReportCode -- <ReportCode, nvarchar(80),>
					            ,  'Registro de Verifone' -- <ReportName, nvarchar(100),>
					            ,  'rep_view_VerifoneBreaksDetails' -- <View, nvarchar(max),>
					            ,  'Reports\' -- <ReportRoot, nvarchar(100),>
					            ,  'RdlcFiles\Generales\VerifoneDetails.rdlc' -- <ReportPath, nvarchar(500),>
					            ,  1 -- <ReportDataSourceId, int,>
					            ,  0 -- <FilterByTenant, bit,>
					            ,  2 -- <IsForTenant, int,>
					            );

			            INSERT INTO [dbo].[ReportsFilters]
					            ([ReportsId]
					            ,[IsDeleted]
					            ,[IsActive]
					            ,[CreatorUserId]
					            ,[CreationTime]
					            ,[LastModifierUserId]
					            ,[LastModificationTime]
					            ,[DeleterUserId]
					            ,[DeletionTime]
					            ,[ReportId]
					            ,[Order]
					            ,[Name]
					            ,[DisplayName]
					            ,[DataField]
					            ,[DataTypeId]
					            ,[Range]
					            ,[OnlyParameter]
					            ,[UrlService]
					            ,[FieldService]
					            ,[DisplayNameService]
					            ,[DependencyField]
					            ,[Operator]
					            ,[DisplayNameRange]
					            ,[Required])
				            VALUES
					            ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
					            ,  0  -- <IsDeleted, bit,>
					            ,  1  -- <IsActive, bit,>
					            ,  1  -- <CreatorUserId, bigint,>
					            ,  GETDATE() -- <CreationTime, datetime,>
					            ,  null -- <LastModifierUserId, bigint,>
					            ,  null -- <LastModificationTime, datetime,>
					            ,  null -- <DeleterUserId, bigint,>
					            ,  null -- <DeletionTime, datetime,>
					            ,  0 -- <ReportId, int,>
					            ,  1 -- <Order, int,>
					            ,  'School' -- <Name, nvarchar(max),>
					            ,  'School' -- <DisplayName, nvarchar(max),>
					            ,  'TenantId' -- <DataField, nvarchar(max),>
					            ,  1 -- <DataTypeId, int,>
					            ,  0 -- <Range, bit,>
					            ,  0 -- <OnlyParameter, bit,>
					            ,  'api/services/app/tenant/getAllTenantsForCombo' -- <UrlService, nvarchar(max),>
					            ,  'id'   -- <FieldService, nvarchar(max),>
					            ,  'name' -- <DisplayNameService, nvarchar(max),>
					            ,  null -- <DependencyField, nvarchar(max),>
					            ,  null -- <Operator, nvarchar(max),>
					            ,  null -- <DisplayNameRange, nvarchar(max),>
					            ,  1 -- <Required, bit,>
					            );

			            INSERT INTO [dbo].[ReportsFilters]
			            ([ReportsId]
			            ,[IsDeleted]
			            ,[IsActive]
			            ,[CreatorUserId]
			            ,[CreationTime]
			            ,[LastModifierUserId]
			            ,[LastModificationTime]
			            ,[DeleterUserId]
			            ,[DeletionTime]
			            ,[ReportId]
			            ,[Order]
			            ,[Name]
			            ,[DisplayName]
			            ,[DataField]
			            ,[DataTypeId]
			            ,[Range]
			            ,[OnlyParameter]
			            ,[UrlService]
			            ,[FieldService]
			            ,[DisplayNameService]
			            ,[DependencyField]
			            ,[Operator]
			            ,[DisplayNameRange]
			            ,[Required])
			            VALUES
			            ((select Id from Reports where ReportCode = @ReportCode) -- <ReportsId, int,>
			            ,  0  -- <IsDeleted, bit,>
			            ,  1  -- <IsActive, bit,>
			            ,  1  -- <CreatorUserId, bigint,>
			            ,  GETDATE() -- <CreationTime, datetime,>
			            ,  null -- <LastModifierUserId, bigint,>
			            ,  null -- <LastModificationTime, datetime,>
			            ,  null -- <DeleterUserId, bigint,>
			            ,  null -- <DeletionTime, datetime,>
			            ,  0 -- <ReportId, int,>
			            ,  2 -- <Order, int,>
			            ,  'Date' -- <Name, nvarchar(max),>
			            ,  'Desde Fecha' -- <DisplayName, nvarchar(max),>
			            ,  'Date' -- <DataField, nvarchar(max),>
			            ,  3 -- <DataTypeId, int,>
			            ,  1 -- <Range, bit,>
			            ,  0 -- <OnlyParameter, bit,>
			            ,  null -- <UrlService, nvarchar(max),>
			            ,  null   -- <FieldService, nvarchar(max),>
			            ,  null -- <DisplayNameService, nvarchar(max),>
			            ,  null -- <DependencyField, nvarchar(max),>
			            ,  null -- <Operator, nvarchar(max),>
			            ,  'Hasta' -- <DisplayNameRange, nvarchar(max),>
			            ,  1 -- <Required, bit,>
			            );

            end
			");
        }

    }
}

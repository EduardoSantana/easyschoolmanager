﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Migrations
{
    class RunSqlCommands : DbMigration
    {
        private EntityFramework.EasySchoolManagerDbContext context;

        public RunSqlCommands(EntityFramework.EasySchoolManagerDbContext context)
        {
            this.context = context;
        }


        public override void Up()
        {

        }

        /// <summary>
        /// Method to call all the methods made to create the views
        /// </summary>
        public void RunAllCommands()
        {
            Run_RECEIPT_TYPES();
        }


        /// <summary>
        /// Method to create ALL THE RECEIPT TYPES
        /// </summary>
        public void Run_RECEIPT_TYPES()
        {
            this.context.Database.ExecuteSqlCommand(@"
                
				if(not(exists(select id from ReceiptTypes where Name = 'Tuition')))
				INSERT INTO [dbo].[ReceiptTypes] ([Name] ,[IsDeleted] ,[IsActive], [CreatorUserId] ,[CreationTime]) VALUES ('Tuition',0,1,1,getdate())

				if(not(exists(select id from ReceiptTypes where Name = 'General')))
				INSERT INTO [dbo].[ReceiptTypes] ([Name] ,[IsDeleted] ,[IsActive], [CreatorUserId] ,[CreationTime]) VALUES ('General',0,1,1,getdate())

				if(not(exists(select id from ReceiptTypes where Name = 'Inventary')))
				INSERT INTO [dbo].[ReceiptTypes] ([Name] ,[IsDeleted] ,[IsActive], [CreatorUserId] ,[CreationTime]) VALUES ('Inventary',0,1,1,getdate())

            
            ");
        }



    }
}

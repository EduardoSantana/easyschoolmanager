﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Migrations
{
	public class SeedTables : DbMigration
	{
		private EntityFramework.EasySchoolManagerDbContext context;

		public SeedTables(EntityFramework.EasySchoolManagerDbContext context)
		{
			this.context = context;
		}

		public override void Up()
		{

		}

		public void All()
		{
            CraeteIllnessesTable();
			CraeteAllergiesTable();
			CraeteReligionsTable();
			CraeteUnitsTable();
			CraeteArticleTypesTable();
			CraeteArticleTable();
			CraetePositionsTable();
			CraeteLevelsTable();
		}

        public void InsertOthers()
        {
            this.context.Database.ExecuteSqlCommand(@"

SET IDENTITY_INSERT [dbo].[ReportDataSources] ON 


INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'View', N'01')

INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (2, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'Procedure', N'02')

INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (3, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'Query', N'03')

INSERT [dbo].[ReportDataSources] ([Id], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [Name], [Code]) VALUES (4, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, N'Report', N'04')

SET IDENTITY_INSERT [dbo].[ReportDataSources] OFF

SET IDENTITY_INSERT [dbo].[Reports] ON 


INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant]) VALUES (9, 1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'01', N'Schools', N'Rep_VW_SchoolList', N'Reports\', N'RdlcFiles\Generales\CodeDescription.rdlc', 0)

INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant]) VALUES (10, 1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'02', N'EnrollmentList', N'rep_vw_EnrollmentStudentCount', N'Reports\', N'RdlcFiles\Generales\EnrollmentList.rdlc', 1)

INSERT [dbo].[Reports] ([Id], [ReportsTypesId], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime], [ReportDataSourceId], [ReportCode], [ReportName], [View], [ReportRoot], [ReportPath], [FilterByTenant]) VALUES (11, 1, 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL, 1, N'02', N'StudentByCourse', N'rep_vw_StudentByCourse', N'Reports\', N'RdlcFiles\Generales\StudentByCourse.rdlc', 1)

SET IDENTITY_INSERT [dbo].[Reports] OFF

SET IDENTITY_INSERT [dbo].[DataTypes] ON 


INSERT [dbo].[DataTypes] ([Id], [Code], [Description], [TypeHTML], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (1, N'01', N'Numero', N'number', 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL)

INSERT [dbo].[DataTypes] ([Id], [Code], [Description], [TypeHTML], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (2, N'02', N'Texto', N'text', 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL)

INSERT [dbo].[DataTypes] ([Id], [Code], [Description], [TypeHTML], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (3, N'03', N'Fecha', N'date', 0, 1, 1, CAST(0x0000901A00000000 AS DateTime), NULL, NULL, NULL, NULL)

SET IDENTITY_INSERT [dbo].[DataTypes] OFF



");
        }

		private void CraeteIllnessesTable()
		{
			this.context.Database.ExecuteSqlCommand(@"
				IF NOT EXISTS(select 1 from [Illnesses])
				BEGIN
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Acne' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cold sores' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Albinism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Alcoholism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Avalanche' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Alzheimers' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Amenorrhea' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Blisters' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Anemia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Aneurysm' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Angina pectoris' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Anisakiasis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Anorexy' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Anxiety' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Appendicitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Sleep apnea' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Arrhythmia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Arteriosclerosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Rheumatoid arthritis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Osteoarthritis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Ascites' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Asthma' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Astigmatism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Ataxia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Choking' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Vaginal atrophy' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Autism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Balanitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bartolinitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Goiter' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Botulism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bronchiectasis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Obliterating bronchiolitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bronchitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Brucellosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bruxism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bulimia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Campylobacter' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Head and neck cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Colon cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cervical cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Esophagus cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Stomach cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Liver cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Larynx cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Breast cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Ovarian cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pancreatic cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Penis cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Skin cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Prostate cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Lung cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Kidney cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Testicular cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Thyroid cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bladder cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Oral cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bone cancer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Candidiasis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Carbuncle' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'waterfalls' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cold' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Celiac disease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Chancroid' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Chikungunya' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Sciatica' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cirrhosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cystitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Chlamydia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Anger' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cholesteatoma' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Renal colic' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Ulcerative colitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Irritable bowel' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Freezing' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Nasal congestion' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Conjunctivitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bruises' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Digestion cut' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Crisis of panic' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Strange bodies' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'colour blindness' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Growth hormone deficit' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Macular degeneration' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Dementia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Dementia with Lewy bodies' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Vascular dementia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Dengue' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Depression' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Atopic dermatitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Ocular effusion' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Mild cognitive impairment' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Diabetes' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Gestational diabetes' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Diabetes insipidus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Diphtheria' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Erectile dysfunction' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Dyslexia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Dysmenorrhea' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Dyspepsia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Dystonia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Duchenne muscular dystrophy' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Diverticulosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Ebola' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pulmonary edema' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Elephantiasis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Encephalitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Endocarditis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Endometriosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Addisons disease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Behçets disease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Chagas disease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Crohnsdisease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Huntingtonsdisease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Kawasaki disease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Peyroniesdisease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Lymesdesease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pagetsdisease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Wilsonsdisease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pelvic inflammatory disease' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Sexually transmitted diseases' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Rare diseases' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Epilepsy' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'COPD' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Scleritis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Scleroderma' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Amyotrophic Lateral Sclerosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Multiple sclerosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Scoliosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Barrettsesophagus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Ankylosing spondylitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Schizophrenia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hepatic steatosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Squint' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Constipation' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Premature ejaculation' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pharyngitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Plantar fasciitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Phenylketonuria' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Atrial fibrillation' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Fibromyalgia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cystic fibrosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Yellow fever' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Lassa fever' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Oropouche Fever' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hemorrhagic Fever of Crimea-Congo' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Typhoid fever' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Anal fistula' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Finger fracture' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Nasal fracture' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Galactorrhea' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Galactosemia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Gastritis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Stomach flu' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Giardiasis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Glaucoma' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Heatstroke' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Gonorrhea' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Gout' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Flu' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hemochromatosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hemophilia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Nosebleed' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hemorrhoids' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hepatitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Gunshot wound' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Wounds' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hiatus hernia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Herniated disc' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Inguinal hernia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Herpes' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Herpes zoster' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hyperactivity (ADHD)' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hypercholesterolemia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Farsightedness' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Benign prostatic hyperplasia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Arterial hypertension' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hyperthyroidism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hearing loss' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hypocalcaemia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hypoglycemia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hypogonadism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Arterial hypotension' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hypothermia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hypothyroidism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Hirsutism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Ictus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Impetigo' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Fires' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Urinary incontinence' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Myocardial infarction' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Urinary infections' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Insomnia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Fatal family insomnia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Heart failure' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Mitral insufficiency' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Chronic renal failure' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Ethyl poisoning' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Carbon monoxide poisoning' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Intoxications' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bunions' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bricks' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Laryngitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Legionella' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Leishmaniasis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Leprosy' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Leptospirosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Maxillary lesions' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Leukemia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Lipodystrophy' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Lipotimia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Listeriosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Biliary lithiasis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Low back pain' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Lupus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Dislocations' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Acute mountain sickness' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Malaria' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Melanoma' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Melasma' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Melioidosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Meningitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Menopause' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'MERS' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Mesothelioma' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Myasthenia gravis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Microcephaly' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Migraine' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Uterine myomatosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Myopia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Molluscum contagiosum' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Mononucleosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Animal bites' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Narcolepsy' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pneumonia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pneumothorax' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Trigeminal Neuralgia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Neurofibromatosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Nystagmus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Obesity' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Childhood obesity' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Dry Eye' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Onchocerciasis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Onychomycosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Orchitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Osteomalacia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Osteomyelitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Osteonecrosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Osteoporosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Otitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Heart palpitations' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pancreatitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Mumps' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cerebral palsy' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Parkinsons' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Benign Breast Pathologies' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pericarditis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Peritonitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Plague' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Yaws' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Insect bites' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Athletesfoot' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pyelonephritis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Polycythemia Vera' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Poliomyelitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Uterine polyps' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Prediabetes' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Presbiacusia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Presbyopia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Priapism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Prostatitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Psoriasis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'ITP (primary immune thrombocytopenia)' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Burns' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Rage' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Scrapes and lacerations' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Allergic reactions to medications' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cardiopulmonary resuscitation' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Gastroesophageal reflux' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Mountain rescue' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Insulin resistance' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Retinitis pigmentosa' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Rhinitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Rosacea' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Rubella' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Salmonella' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Measles' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Sarcoidosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Scabies' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Sepsis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Shigellosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Shock' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'AIDS' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Syphilis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Angelman syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Cushingssyndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'DownsSyndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Chronic Fatigue Syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Guillain Barre syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Klinefelter syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Lynch syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Marfan syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Noonan syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Polycystic Ovary Syndrome (PCOS)' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Patau syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Restless Leg Syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Rett syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Reyessyndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Sjogrenssyndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Tourette syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Turner syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Williams syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Fragile X syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Carpal tunnel syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Metabolic syndrome' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Sinusitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Syringomyelia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Overdose' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Thalassemia' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Telangiectasias' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Tendonitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Tapeworm' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Earthquakes' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Tetanus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Tinnitus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Whooping cough' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Toxicodermias' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Toxoplasmosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Trachoma' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Bipolar disorder' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Obsessive compulsive disorder' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Head injury' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Trichomoniasis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Pulmonary embolism' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Venous thrombosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Tuberculosis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Brain tumors' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Ulcer' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Urethritis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Urticaria' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Uveitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Vaginitis' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Chickenpox' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Varicose veins' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Varicocele' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Vertigo' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'West Nile virus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Mayaro virus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Usutu virus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Zika virus' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'Vitiligo' , 0, 1, null, getdate());
					INSERT INTO [dbo].[Illnesses] ([Name] ,[IsDeleted] ,[IsActive] ,[CreatorUserId] ,[CreationTime]) VALUES ( 'HPV' , 0, 1, null, getdate());
				END
			");
		}

		private void CraeteAllergiesTable()
		{
			this.context.Database.ExecuteSqlCommand(@"
				IF NOT EXISTS(select 1 from [Allergies])
				BEGIN
				
					SET IDENTITY_INSERT [dbo].[Allergies] ON
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (1, N'Pollen', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (2, N'Dust Mites', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (3, N'Mold spores', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (4, N'Animal dander', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (5, N'Insect bites', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (6, N'Latex', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (7, N'Nuts', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (8, N'Seafood', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (9, N'Fish', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (10, N'Gluten', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (11, N'Egg', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (12, N'Milk', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (13, N'Wheat', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (14, N'Peanut', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (15, N'Animal hairs', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (16, N'Feathers', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (17, N'Insects', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (18, N'Penicillin', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (19, N'Aspirin', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Allergies] ([Id], [Name], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (20, N'Anesthesia', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					SET IDENTITY_INSERT [dbo].[Allergies] OFF

				END
			");
		}

		private void CraeteReligionsTable()
		{
			this.context.Database.ExecuteSqlCommand(@"
				IF NOT EXISTS(select 1 from [Religions])
				BEGIN
				
					SET IDENTITY_INSERT [dbo].[Religions] ON
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (1, 'Adventist ',' Seventh-day Adventist', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (2, 'Anglicans ',' Anglicans', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (3, 'Baptist ',' Baptista', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (4, 'Buddhism ',' Buddhism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (5, 'Candomblé ',' Candomblé', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (6, 'Celticism ',' Celticism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (7, 'Shamanism ',' Shamanism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (8, 'Chiismo ',' Chiismo', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (9, 'Chondogyo ',' Chondogyo', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (10, 'Confucianism ',' Confucianism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (11, 'Cristadelfianos ',' Cristadelfianos', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (12, 'Quakers ',' Quakers', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (13, 'Davidians ',' Davidians', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (14, 'Dodecateísmo ',' Dodecateísmo', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (15, 'Etenismo ',' Etenismo', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (16, 'Evangelical ',' Evangelical', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (17, 'Pentecostal ',' Pentecostal', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (18, 'Hinduism ',' Hinduism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (19, 'Catholic ',' Catholic', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (20, 'Mormon ',' The Church of Jesus Christ of Latter-day Saints', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (21, 'Islam ',' Islam', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (22, 'Jainism ',' Jainism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (23, 'Judaism ',' Judaism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (24, 'Kemetism ',' Kemetism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (25, 'Kimbanda ',' Kimbanda', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (26, 'Lutheran ',' Lutheran', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (27, 'Mandeism ',' Mandeism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (28, 'Methodism ',' Methodism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (29, 'Samaritanism ',' Samaritanism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (30, 'Shintoism ',' Shintoism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (31, 'Shivaism ',' Shivaism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (32, 'Sikhism ',' Sikhism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (33, 'Sufism ',' Sufism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (34, 'Sunismo ',' Sunismo', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (35, 'Taoism ',' Taoism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (36, 'Jehovahs Witnesses ',' Jehovahs Witnesses', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (37, 'Totemism ',' Totemism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (38, 'Umbanda ',' Umbanda', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (39, 'Vaishnavism ',' Vaishnavism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (40, 'Voodoo ',' Voodoo', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (41, 'Wicca ',' Wicca', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (42, 'Yazidism ',' Yazidism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (43, 'Yoruba ',' Yoruba', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (44, 'Zoroastrianism ',' Zoroastrianism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (45, 'Mita ',' Mita in Aaron ', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (46, 'Coptic ',' Coptic', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (47, 'Episcopal ',' Episcopal', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (48, 'Calvinist ',' Calvinist', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (49, 'Krishnaism ',' Krishnaism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (50, 'Kopimism ',' Kopimism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (51, 'Asatrú ',' Asatrú', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (52, 'Dievturiba ',' Dievturiba', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (53, 'Neodrudismo ',' Neodrudismo', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (54, 'Orphism ',' Orphism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (55, 'Pitagorísmo ',' Pitagorísmo', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (56, 'Romuva ',' Romuva', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (57, 'Noajismo ',' Noajismo', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (58, 'Pastafarismo ',' Pastafarismo', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					INSERT [dbo].[Religions] ([Id], [ShortName], [LongName], [IsDeleted], [IsActive], [CreatorUserId], [CreationTime], [LastModifierUserId], [LastModificationTime], [DeleterUserId], [DeletionTime]) VALUES (59, 'Yainism ',' Yainism', 0, 1, NULL, getdate(), NULL, NULL, NULL, NULL)
					SET IDENTITY_INSERT [dbo].[Religions] OFF

				END
			");
		}

		private void Run_RECEIPT_TYPES()
		{
			this.context.Database.ExecuteSqlCommand(@"
				
				if(not(exists(select id from ReceiptTypes where Name = 'Matricula')))
				INSERT INTO [dbo].[ReceiptTypes] ([Name] ,[IsDeleted] ,[IsActive], [CreatorUserId] ,[CreationTime]) VALUES ('Matricula',0,1,1,getdate())

				if(not(exists(select id from ReceiptTypes where Name = 'General')))
				INSERT INTO [dbo].[ReceiptTypes] ([Name] ,[IsDeleted] ,[IsActive], [CreatorUserId] ,[CreationTime]) VALUES ('General',0,1,1,getdate())

				if(not(exists(select id from ReceiptTypes where Name = 'Inventario')))
				INSERT INTO [dbo].[ReceiptTypes] ([Name] ,[IsDeleted] ,[IsActive], [CreatorUserId] ,[CreationTime]) VALUES ('Inventario',0,1,1,getdate())

			
			");
		}

		private void CraeteUnitsTable()
		{
			this.context.Database.ExecuteSqlCommand(@"
				IF NOT EXISTS(select 1 from [Units])
				BEGIN
					-- Units

					INSERT INTO [dbo].[Units]
							   ([Description]
							   ,[IsDeleted]
							   ,[IsActive]
							   ,[CreatorUserId]
							   ,[CreationTime]
							   ,[LastModifierUserId]
							   ,[LastModificationTime]
							   ,[DeleterUserId]
							   ,[DeletionTime])
						 VALUES
							   ('Unit'
								,0
							   ,1
							   ,1
							   ,GETDATE()
							   ,null
							   ,null
							   ,null
							   ,null);


				END
			");
		}

		private void CraeteArticleTypesTable()
		{
			this.context.Database.ExecuteSqlCommand(@"
				IF NOT EXISTS(select 1 from [ArticlesTypes])
				BEGIN

						INSERT INTO [dbo].[ArticlesTypes]
								   ([Description]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime])
							 VALUES
								   ('Uniforms'
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null);

						INSERT INTO [dbo].[ArticlesTypes]
								   ([Description]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime])
							 VALUES
								   ('Books'
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null);

				END
			");
		}

		private void CraeteArticleTable()
		{
			this.context.Database.ExecuteSqlCommand(@"
				IF NOT EXISTS(select 1 from [Articles])
				BEGIN
				   
						INSERT INTO [dbo].[Articles]
								   ([Description]
								   ,[Reference]
								   ,[Price]
								   ,[ArticleTypeId]
								   ,[UnitId]
								   ,[BrandId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime])
							 VALUES
								   ('Item Description 1 '
								   ,'Item1'
								   ,123
								   ,(select top 1 Id from ArticlesTypes)
								   ,(select top 1 Id from Units)
								   ,null
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null);

						INSERT INTO [dbo].[Articles]
								   ([Description]
								   ,[Reference]
								   ,[Price]
								   ,[ArticleTypeId]
								   ,[UnitId]
								   ,[BrandId]
								   ,[IsDeleted]
								   ,[IsActive]
								   ,[CreatorUserId]
								   ,[CreationTime]
								   ,[LastModifierUserId]
								   ,[LastModificationTime]
								   ,[DeleterUserId]
								   ,[DeletionTime])
							 VALUES
								   ('Item Description 2 '
								   ,'Item2'
								   ,124
								   ,(select top 1 Id from ArticlesTypes)
								   ,(select top 1 Id from Units)
								   ,null
								   ,0
								   ,1
								   ,1
								   ,GETDATE()
								   ,null
								   ,null
								   ,null
								   ,null);
				END
			");
		}

		private void CraetePositionsTable()
		{
			this.context.Database.ExecuteSqlCommand(@"
				IF NOT EXISTS(select 1 from [Positions])
				BEGIN


					INSERT INTO [dbo].[Positions]
							   ([Name]
							   ,[IsDeleted]
							   ,[IsActive]
							   ,[CreatorUserId]
							   ,[CreationTime]
							   ,[LastModifierUserId]
							   ,[LastModificationTime]
							   ,[DeleterUserId]
							   ,[DeletionTime])
						 VALUES
							   ('Financial Assistant'
							   ,0
							   ,1
							   ,1
							   ,GETDATE()
							   ,null
							   ,null
							   ,null
							   ,null);


					INSERT INTO [dbo].[Positions]
							   ([Name]
							   ,[IsDeleted]
							   ,[IsActive]
							   ,[CreatorUserId]
							   ,[CreationTime]
							   ,[LastModifierUserId]
							   ,[LastModificationTime]
							   ,[DeleterUserId]
							   ,[DeletionTime])
						 VALUES
							   ('School Director'
							   ,0
							   ,1
							   ,1
							   ,GETDATE()
							   ,null
							   ,null
							   ,null
							   ,null);
		   

					INSERT INTO [dbo].[Positions]
							   ([Name]
							   ,[IsDeleted]
							   ,[IsActive]
							   ,[CreatorUserId]
							   ,[CreationTime]
							   ,[LastModifierUserId]
							   ,[LastModificationTime]
							   ,[DeleterUserId]
							   ,[DeletionTime])
						 VALUES
							   ('Academic Secretary'
							   ,0
							   ,1
							   ,1
							   ,GETDATE()
							   ,null
							   ,null
							   ,null
							   ,null);


					INSERT INTO [dbo].[Positions]
							   ([Name]
							   ,[IsDeleted]
							   ,[IsActive]
							   ,[CreatorUserId]
							   ,[CreationTime]
							   ,[LastModifierUserId]
							   ,[LastModificationTime]
							   ,[DeleterUserId]
							   ,[DeletionTime])
						 VALUES
							   ('Professor'
							   ,0
							   ,1
							   ,1
							   ,GETDATE()
							   ,null
							   ,null
							   ,null
							   ,null);

		   
					INSERT INTO [dbo].[Positions]
							   ([Name]
							   ,[IsDeleted]
							   ,[IsActive]
							   ,[CreatorUserId]
							   ,[CreationTime]
							   ,[LastModifierUserId]
							   ,[LastModificationTime]
							   ,[DeleterUserId]
							   ,[DeletionTime])
						 VALUES
							   ('Technician'
							   ,0
							   ,1
							   ,1
							   ,GETDATE()
							   ,null
							   ,null
							   ,null
							   ,null);


					INSERT INTO [dbo].[Positions]
							   ([Name]
							   ,[IsDeleted]
							   ,[IsActive]
							   ,[CreatorUserId]
							   ,[CreationTime]
							   ,[LastModifierUserId]
							   ,[LastModificationTime]
							   ,[DeleterUserId]
							   ,[DeletionTime])
						 VALUES
							   ('Coordinator'
							   ,0
							   ,1
							   ,1
							   ,GETDATE()
							   ,null
							   ,null
							   ,null
							   ,null);
				 

				END
			");
		}

		private void CraeteLevelsTable()
		{

			this.context.Database.ExecuteSqlCommand(@"
				IF NOT EXISTS(select 1 from [Levels])
				BEGIN
					
					INSERT INTO [dbo].[Levels]
							   ([Name]
							   ,[Code]
							   ,[IsDeleted]
							   ,[IsActive]
							   ,[CreatorUserId]
							   ,[CreationTime]
							   ,[LastModifierUserId]
							   ,[LastModificationTime]
							   ,[DeleterUserId]
							   ,[DeletionTime])
						 VALUES
							   ('Initial'
							   ,'DPNIVEL01'
							   ,0
							   ,1
							   ,1
							   ,GETDATE()
							   ,null
							   ,null
							   ,null
							   ,null);

					INSERT INTO [dbo].[Levels]
							   ([Name]
							   ,[Code]
							   ,[IsDeleted]
							   ,[IsActive]
							   ,[CreatorUserId]
							   ,[CreationTime]
							   ,[LastModifierUserId]
							   ,[LastModificationTime]
							   ,[DeleterUserId]
							   ,[DeletionTime])
						 VALUES
							   ('Primary'
							   ,'DPNIVEL02'
							   ,0
							   ,1
							   ,1
							   ,GETDATE()
							   ,null
							   ,null
							   ,null
							   ,null);


					INSERT INTO [dbo].[Levels]
							   ([Name]
							   ,[Code]
							   ,[IsDeleted]
							   ,[IsActive]
							   ,[CreatorUserId]
							   ,[CreationTime]
							   ,[LastModifierUserId]
							   ,[LastModificationTime]
							   ,[DeleterUserId]
							   ,[DeletionTime])
						 VALUES
							   ('Secundary'
							   ,'DPNIVEL03'
							   ,0
							   ,1
							   ,1
							   ,GETDATE()
							   ,null
							   ,null
							   ,null
							   ,null);


				END
			");
		}

	}
}

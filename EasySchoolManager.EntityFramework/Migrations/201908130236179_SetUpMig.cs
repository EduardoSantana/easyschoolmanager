namespace EasySchoolManager.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class SetUpMig : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions");
            DropForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles");
            DropForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers");
            CreateTable(
                "dbo.Allergies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Allergies_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_AllergiesName");
            
            CreateTable(
                "dbo.StudentAllergies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AllergyId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_StudentAllergies_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_StudentAllergies_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Allergies", t => t.AllergyId)
                .ForeignKey("dbo.Students", t => t.StudentId)
                .Index(t => t.AllergyId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        DateOfBirth = c.DateTime(nullable: false, storeType: "date"),
                        EmergencyNote = c.String(maxLength: 250),
                        Position = c.Short(nullable: false),
                        EmployeeSon = c.Boolean(),
                        GenderId = c.Int(nullable: false),
                        Derivation = c.String(maxLength: 100),
                        Medication = c.String(maxLength: 200),
                        Nutrition = c.String(maxLength: 200),
                        BloodId = c.Int(nullable: false),
                        EmailAddress = c.String(maxLength: 255),
                        StudentPersonalId = c.String(nullable: false, maxLength: 11),
                        FatherPersonalId = c.String(maxLength: 11),
                        FatherName = c.String(maxLength: 100),
                        MotherPersonalId = c.String(maxLength: 11),
                        MotherName = c.String(maxLength: 100),
                        FatherAttendedHere = c.Boolean(),
                        MotherAttendedHere = c.Boolean(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Students_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Blood", t => t.BloodId)
                .ForeignKey("dbo.Genders", t => t.GenderId)
                .Index(t => t.GenderId)
                .Index(t => t.BloodId)
                .Index(t => t.StudentPersonalId, unique: true, name: "IX_StudentsStudentPersonalId");
            
            CreateTable(
                "dbo.Blood",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 3),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Bloods_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_BloodsName");
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StudentId = c.Int(nullable: false),
                        Comment = c.String(nullable: false, maxLength: 500),
                        CommentTypeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Comments_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Comments_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CommentTypes", t => t.CommentTypeId)
                .ForeignKey("dbo.Students", t => t.StudentId)
                .Index(t => t.StudentId)
                .Index(t => t.CommentTypeId);
            
            CreateTable(
                "dbo.CommentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CommentTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_CommentTypesName");
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentTypeId = c.Int(nullable: false),
                        DocumentData = c.Binary(nullable: false, storeType: "image"),
                        Basa64Type = c.String(nullable: false),
                        StudentId = c.Int(),
                        TransactionId = c.Long(),
                        TransactionTypeId = c.Int(nullable: false),
                        TeacherId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Documents_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Documents_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Students", t => t.StudentId)
                .ForeignKey("dbo.Teachers", t => t.TeacherId)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.StudentId)
                .Index(t => t.TransactionId)
                .Index(t => t.TeacherId);
            
            CreateTable(
                "dbo.Teachers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        DateOfBirth = c.DateTime(nullable: false, storeType: "date"),
                        PersonalId = c.String(nullable: false, maxLength: 11),
                        IsGraduated = c.Boolean(nullable: false),
                        HasMasterDegree = c.Boolean(nullable: false),
                        IsTechnician = c.Boolean(nullable: false),
                        GenderId = c.Int(nullable: false),
                        UserId = c.Long(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Teachers_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Genders", t => t.GenderId)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .Index(t => t.GenderId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Genders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 10),
                        Code = c.String(nullable: false, maxLength: 1),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Genders_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_GendersName");
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 80),
                        LongName = c.String(nullable: false, maxLength: 350),
                        Code = c.String(nullable: false, maxLength: 20),
                        CourseId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        Teachers_Id = c.Int(),
                        ScoreTypes_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Subjects_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .ForeignKey("dbo.Teachers", t => t.Teachers_Id)
                .ForeignKey("dbo.ScoreTypes", t => t.ScoreTypes_Id)
                .Index(t => t.CourseId)
                .Index(t => t.Teachers_Id)
                .Index(t => t.ScoreTypes_Id);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Abbreviation = c.String(nullable: false, maxLength: 5),
                        LevelId = c.Int(nullable: false),
                        Sequence = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Courses_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Levels", t => t.LevelId)
                .Index(t => new { t.Name, t.LevelId }, unique: true, name: "IX_CoursesNameLevels")
                .Index(t => new { t.Abbreviation, t.LevelId }, unique: true, name: "IX_CoursesAbbreviationLevels");
            
            CreateTable(
                "dbo.CourseEnrollmentStudents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CourseId = c.Int(nullable: false),
                        EnrollmentStudentId = c.Int(nullable: false),
                        SessionId = c.Int(),
                        Sequence = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CourseEnrollmentStudents_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CourseEnrollmentStudents_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .ForeignKey("dbo.EnrollmentStudents", t => t.EnrollmentStudentId)
                .ForeignKey("dbo.CourseSessions", t => t.SessionId)
                .Index(t => t.CourseId)
                .Index(t => t.EnrollmentStudentId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.EnrollmentStudents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PeriodId = c.Int(nullable: false),
                        EnrollmentId = c.Long(nullable: false),
                        StudentId = c.Int(nullable: false),
                        RelationshipId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EnrollmentStudents_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EnrollmentStudents_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Enrollments", t => t.EnrollmentId)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .ForeignKey("dbo.Relationships", t => t.RelationshipId)
                .ForeignKey("dbo.Students", t => t.StudentId)
                .Index(t => new { t.TenantId, t.PeriodId, t.EnrollmentId, t.StudentId }, unique: true, name: "UQ_EnrollmentStudentPeriodTenant")
                .Index(t => t.RelationshipId);
            
            CreateTable(
                "dbo.Enrollments",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(maxLength: 100),
                        Address = c.String(nullable: false, maxLength: 100),
                        FullAddress = c.String(nullable: false, maxLength: 300),
                        CityId = c.Int(nullable: false),
                        Phone1 = c.String(maxLength: 20),
                        GenderId = c.Int(nullable: false),
                        Phone2 = c.String(maxLength: 20),
                        OccupationId = c.Int(nullable: false),
                        ReligionId = c.Int(nullable: false),
                        Comment = c.String(maxLength: 250),
                        EmailAddress = c.String(maxLength: 255),
                        OtherDocument = c.String(maxLength: 20),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Enrollments_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .ForeignKey("dbo.Genders", t => t.GenderId)
                .ForeignKey("dbo.Occupations", t => t.OccupationId)
                .ForeignKey("dbo.Religions", t => t.ReligionId)
                .Index(t => t.CityId)
                .Index(t => t.GenderId)
                .Index(t => t.OccupationId)
                .Index(t => t.ReligionId);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProvinceId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Cities_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Provinces", t => t.ProvinceId)
                .Index(t => t.ProvinceId)
                .Index(t => t.Name, unique: true, name: "IX_CitiesName");
            
            CreateTable(
                "dbo.Configurations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Phone1 = c.String(maxLength: 15),
                        Phone2 = c.String(maxLength: 15),
                        Phone3 = c.String(maxLength: 15),
                        Address = c.String(maxLength: 100),
                        CityId = c.Int(),
                        DirectorName = c.String(maxLength: 100),
                        RegisterName = c.String(maxLength: 100),
                        PeriodId = c.Int(),
                        AccreditationNumber = c.String(maxLength: 15),
                        EducationalDistrict = c.String(maxLength: 15),
                        Regional = c.String(maxLength: 60),
                        RegionalDirector = c.String(maxLength: 60),
                        DistritctDirector = c.String(maxLength: 60),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Configurations_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Configurations_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .Index(t => t.CityId)
                .Index(t => t.PeriodId);
            
            CreateTable(
                "dbo.Periods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Period = c.String(nullable: false, maxLength: 5),
                        Description = c.String(nullable: false, maxLength: 50),
                        StartDate = c.DateTime(nullable: false, storeType: "date"),
                        EndDate = c.DateTime(nullable: false, storeType: "date"),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Periods_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Period, unique: true, name: "UQ_Periods1");
            
            CreateTable(
                "dbo.MonthlyPeriods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PeriodId = c.Int(nullable: false),
                        Month = c.Short(nullable: false),
                        Year = c.Short(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MonthlyPeriods_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MonthlyPeriods_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .Index(t => new { t.PeriodId, t.Month, t.Year }, unique: true, name: "IX_MonthlyPeriodsYearMonth");
            
            CreateTable(
                "dbo.Provinces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        RegionId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Provinces_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.Name, unique: true, name: "IX_ProvincesName")
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 20),
                        Code = c.String(nullable: false, maxLength: 3),
                        Rnc = c.String(maxLength: 15),
                        LegalName = c.String(maxLength: 60),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Regions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_RegionsName");
            
            CreateTable(
                "dbo.EnrollmentSequences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EnrollmentId = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        Sequence = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EnrollmentSequences_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EnrollmentSequences_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Enrollments", t => t.EnrollmentId)
                .Index(t => new { t.TenantId, t.EnrollmentId }, unique: true, name: "UQ_EnrollmentSequencesTenantIdEnrollmentId");
            
            CreateTable(
                "dbo.Occupations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Occupations_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_OccupationsName");
            
            CreateTable(
                "dbo.Religions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShortName = c.String(maxLength: 50),
                        LongName = c.String(maxLength: 120),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Religions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ShortName, unique: true, name: "IX_ReligionsShortName");
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TaxReceiptId = c.Int(),
                        TaxReceiptExpirationDate = c.DateTime(),
                        Sequence = c.Int(nullable: false),
                        BankId = c.Int(),
                        BankAccountId = c.Int(),
                        Number = c.String(maxLength: 20),
                        Date = c.DateTime(nullable: false),
                        Name = c.String(maxLength: 100),
                        Comment = c.String(maxLength: 100),
                        Request = c.String(maxLength: 100),
                        CancelAuthorization = c.String(maxLength: 100),
                        EnrollmentId = c.Long(),
                        ConceptId = c.Int(),
                        GrantEnterpriseId = c.Int(),
                        ReceiptTypeId = c.Int(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PaymentMethodId = c.Int(),
                        OriginId = c.Int(nullable: false),
                        TransactionTypeId = c.Int(nullable: false),
                        CreditCardPercent = c.Decimal(nullable: false, precision: 18, scale: 6),
                        TaxReceiptSequence = c.String(maxLength: 11),
                        StatusId = c.Int(nullable: false),
                        RncNumber = c.String(maxLength: 15),
                        RncName = c.String(maxLength: 150),
                        Comments = c.String(),
                        CancelStatusId = c.Int(),
                        CancelRequested = c.DateTime(),
                        CancelAuthorized = c.DateTime(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Transactions_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Transactions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Banks", t => t.BankId)
                .ForeignKey("dbo.BankAccounts", t => t.BankAccountId)
                .ForeignKey("dbo.CancelStatus", t => t.CancelStatusId)
                .ForeignKey("dbo.Concepts", t => t.ConceptId)
                .ForeignKey("dbo.Enrollments", t => t.EnrollmentId)
                .ForeignKey("dbo.GrantEnterprises", t => t.GrantEnterpriseId)
                .ForeignKey("dbo.Origins", t => t.OriginId)
                .ForeignKey("dbo.PaymentMethods", t => t.PaymentMethodId)
                .ForeignKey("dbo.ReceiptTypes", t => t.ReceiptTypeId)
                .ForeignKey("dbo.Statuses", t => t.StatusId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .ForeignKey("dbo.TransactionTypes", t => t.TransactionTypeId)
                .Index(t => new { t.Sequence, t.TenantId, t.TransactionTypeId }, unique: true, name: "UQ_TransactionTenantTransactionType")
                .Index(t => t.BankId)
                .Index(t => t.BankAccountId)
                .Index(t => t.EnrollmentId)
                .Index(t => t.ConceptId)
                .Index(t => t.GrantEnterpriseId)
                .Index(t => t.ReceiptTypeId)
                .Index(t => t.PaymentMethodId)
                .Index(t => t.OriginId)
                .Index(t => t.StatusId)
                .Index(t => t.CancelStatusId);
            
            CreateTable(
                "dbo.BankAccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(nullable: false, maxLength: 25),
                        BanktId = c.Int(nullable: false),
                        RegionId = c.Int(),
                        Description = c.String(maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BankAccounts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Banks", t => t.BanktId)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.Number, unique: true, name: "IX_BankAccountsName")
                .Index(t => t.BanktId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.Banks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Banks_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_BanksName");
            
            CreateTable(
                "dbo.CancelStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 200),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CancelStatus_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Concepts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 200),
                        ShortDescription = c.String(nullable: false, maxLength: 30),
                        CatalogId = c.Int(nullable: false),
                        PeriodId = c.Int(),
                        ReceiptTypeId = c.Int(),
                        AvailableForTransaction = c.Boolean(nullable: false),
                        AvailableForReceipts = c.Boolean(nullable: false),
                        AvailableForDeposits = c.Boolean(nullable: false),
                        AvailableForDiscounts = c.Boolean(nullable: false),
                        T1DimensionId = c.Int(),
                        T2DimensionId = c.Int(),
                        T3DimensionId = c.Int(),
                        T4DimensionId = c.Int(),
                        T5DimensionId = c.Int(),
                        T6DimensionId = c.Int(),
                        T7DimensionId = c.Int(),
                        T8DimensionId = c.Int(),
                        T9DimensionId = c.Int(),
                        T10DimensionId = c.Int(),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        sequenceNext = c.Int(),
                        IsEns = c.Boolean(nullable: false),
                        IsMat = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Concepts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Catalogs", t => t.CatalogId)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .ForeignKey("dbo.ReceiptTypes", t => t.ReceiptTypeId)
                .ForeignKey("dbo.T10Dimensions", t => t.T10DimensionId)
                .ForeignKey("dbo.T1Dimensions", t => t.T1DimensionId)
                .ForeignKey("dbo.T2Dimensions", t => t.T2DimensionId)
                .ForeignKey("dbo.T3Dimensions", t => t.T3DimensionId)
                .ForeignKey("dbo.T4Dimensions", t => t.T4DimensionId)
                .ForeignKey("dbo.T5Dimensions", t => t.T5DimensionId)
                .ForeignKey("dbo.T6Dimensions", t => t.T6DimensionId)
                .ForeignKey("dbo.T7Dimensions", t => t.T7DimensionId)
                .ForeignKey("dbo.T8Dimensions", t => t.T8DimensionId)
                .ForeignKey("dbo.T9Dimensions", t => t.T9DimensionId)
                .Index(t => t.Description, unique: true, name: "IX_ConceptTenantIdName")
                .Index(t => t.CatalogId)
                .Index(t => t.PeriodId)
                .Index(t => t.ReceiptTypeId)
                .Index(t => t.T1DimensionId)
                .Index(t => t.T2DimensionId)
                .Index(t => t.T3DimensionId)
                .Index(t => t.T4DimensionId)
                .Index(t => t.T5DimensionId)
                .Index(t => t.T6DimensionId)
                .Index(t => t.T7DimensionId)
                .Index(t => t.T8DimensionId)
                .Index(t => t.T9DimensionId)
                .Index(t => t.T10DimensionId);
            
            CreateTable(
                "dbo.Catalogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountNumber = c.String(nullable: false, maxLength: 20),
                        Name = c.String(nullable: false, maxLength: 85),
                        Descsription = c.String(nullable: false, maxLength: 300),
                        Code1 = c.String(maxLength: 4),
                        Code10 = c.String(maxLength: 9),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Catalogs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.AccountNumber, unique: true, name: "IX_CatalogAccountNumber");
            
            CreateTable(
                "dbo.ReceiptTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 25),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReceiptTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_ReceiptTypesName");
            
            CreateTable(
                "dbo.T10Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T10Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T1Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T1Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T2Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T2Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T3Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T3Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T4Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T4Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T5Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T5Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T6Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T6Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T7Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T7Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T8Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T8Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T9Dimensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 15),
                        Description = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T9Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GrantEnterprises",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sequence = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        Phone = c.String(maxLength: 15),
                        Address = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GrantEnterprises_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_GrantEnterprises_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, name: "IX_GrantEnterpriseName");
            
            CreateTable(
                "dbo.Origins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 10),
                        Letter = c.String(nullable: false, maxLength: 1),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Origins_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_OriginsName");
            
            CreateTable(
                "dbo.PaymentMethods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 25),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PaymentMethods_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_PaymentMethodsName");
            
            CreateTable(
                "dbo.Statuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 15),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Statuses_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_StatusesName");
            
            CreateTable(
                "dbo.TransactionHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StatusId = c.Int(nullable: false),
                        ChangeTypeId = c.Int(nullable: false),
                        TransactionId = c.Long(nullable: false),
                        TransactionTypeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionHistories_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionHistories_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChangeTypes", t => t.ChangeTypeId)
                .ForeignKey("dbo.Statuses", t => t.StatusId)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.StatusId)
                .Index(t => t.ChangeTypeId)
                .Index(t => t.TransactionId);
            
            CreateTable(
                "dbo.ChangeTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ChangeTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_ChangeTypesName");
            
            CreateTable(
                "dbo.ConceptTenantRegistrations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConceptId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConceptTenantRegistrations_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConceptTenantRegistrations_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Concepts", t => t.ConceptId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.ConceptId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Positions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_PositonName");
            
            CreateTable(
                "dbo.UserPictures",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        Picture = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserPictures_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserTenants",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserTenants_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.UserTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.Districts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Abbreviation = c.String(nullable: false, maxLength: 9),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Districts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_DistricName")
                .Index(t => t.Abbreviation, unique: true, name: "IX_DistricAbbrev");
            
            CreateTable(
                "dbo.PrinterTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 25),
                        ReportId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PrinterTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Reports", t => t.ReportId)
                .Index(t => t.Name, unique: true, name: "IX_PrinterTypesName")
                .Index(t => t.ReportId);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReportCode = c.String(nullable: false, maxLength: 80),
                        ReportName = c.String(nullable: false, maxLength: 100),
                        View = c.String(nullable: false),
                        ReportRoot = c.String(nullable: false, maxLength: 100),
                        ReportPath = c.String(nullable: false, maxLength: 500),
                        ReportsTypesId = c.Int(nullable: false),
                        ReportDataSourceId = c.Int(nullable: false),
                        FilterByTenant = c.Boolean(nullable: false),
                        IsForTenant = c.Int(nullable: false),
                        PermissionName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Reports_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ReportDataSources", t => t.ReportDataSourceId)
                .ForeignKey("dbo.ReportsTypes", t => t.ReportsTypesId)
                .Index(t => t.ReportCode, unique: true, name: "IX_Report_ReportCode")
                .Index(t => t.ReportsTypesId)
                .Index(t => t.ReportDataSourceId);
            
            CreateTable(
                "dbo.ReportDataSources",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 80),
                        Code = c.String(maxLength: 20),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportDataSources_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReportsFilters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReportsId = c.Int(nullable: false),
                        ReportId = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                        Name = c.String(),
                        DisplayName = c.String(),
                        DataField = c.String(),
                        DataTypeId = c.Int(nullable: false),
                        Range = c.Boolean(nullable: false),
                        OnlyParameter = c.Boolean(nullable: false),
                        UrlService = c.String(),
                        FieldService = c.String(),
                        DisplayNameService = c.String(),
                        DependencyField = c.String(),
                        Operator = c.String(),
                        DefaultValue = c.String(),
                        DisplayNameRange = c.String(),
                        Required = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsFilters_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataTypes", t => t.DataTypeId)
                .ForeignKey("dbo.Reports", t => t.ReportsId)
                .Index(t => t.ReportsId)
                .Index(t => t.DataTypeId);
            
            CreateTable(
                "dbo.DataTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Description = c.String(),
                        TypeHTML = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DataType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReportsTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        Descripcion = c.String(maxLength: 120),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TeacherTenants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TeacherId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TeacherTenants_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TeacherTenants_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teachers", t => t.TeacherId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => new { t.TenantId, t.TeacherId }, unique: true, name: "IX_TeacherTenantId_Tenant_Teacher");
            
            CreateTable(
                "dbo.TransactionArticles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionId = c.Long(nullable: false),
                        ArticleId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionArticles_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionArticles_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.TransactionId)
                .Index(t => t.ArticleId);
            
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 200),
                        Reference = c.String(nullable: false, maxLength: 20),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ArticleTypeId = c.Int(),
                        UnitId = c.Int(),
                        BrandId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Articles_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArticlesTypes", t => t.ArticleTypeId)
                .ForeignKey("dbo.Brands", t => t.BrandId)
                .ForeignKey("dbo.Units", t => t.UnitId)
                .Index(t => t.Description, unique: true, name: "IX_ArticleDescription")
                .Index(t => t.Reference, unique: true, name: "IX_ArticleReference")
                .Index(t => t.ArticleTypeId)
                .Index(t => t.UnitId)
                .Index(t => t.BrandId);
            
            CreateTable(
                "dbo.ArticlesTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArticlesTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true, name: "IX_ArticleTypeDescription");
            
            CreateTable(
                "dbo.Brands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 75),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Brands_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_BrandName");
            
            CreateTable(
                "dbo.Inventories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArticleId = c.Int(nullable: false),
                        Existence = c.Long(nullable: false),
                        TenantInventoryId = c.Int(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Inventory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantInventoryId)
                .Index(t => t.ArticleId)
                .Index(t => t.TenantInventoryId);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Units_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true, name: "IX_UnitDescription");
            
            CreateTable(
                "dbo.TransactionConcepts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionId = c.Long(nullable: false),
                        ConceptId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionConcepts_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionConcepts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Concepts", t => t.ConceptId)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.TransactionId)
                .Index(t => t.ConceptId);
            
            CreateTable(
                "dbo.TransactionDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionId = c.Long(nullable: false),
                        CatalogId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionTypeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionDetails_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionDetails_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Catalogs", t => t.CatalogId)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.TransactionId)
                .Index(t => t.CatalogId);
            
            CreateTable(
                "dbo.TransactionStudents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionId = c.Long(nullable: false),
                        TransactionTypeId = c.Int(nullable: false),
                        EnrollmentStudentId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionStudents_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionStudents_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EnrollmentStudents", t => t.EnrollmentStudentId)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.TransactionId)
                .Index(t => t.EnrollmentStudentId);
            
            CreateTable(
                "dbo.TransactionTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 25),
                        Abbreviation = c.String(maxLength: 3),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_TransactionTypesName");
            
            CreateTable(
                "dbo.PaymentProjections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EnrollmentStudentId = c.Int(nullable: false),
                        Sequence = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PaymentDate = c.DateTime(nullable: false, storeType: "date"),
                        Applied = c.Boolean(nullable: false),
                        TransactionId = c.Long(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PaymentProjections_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PaymentProjections_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EnrollmentStudents", t => t.EnrollmentStudentId)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.EnrollmentStudentId)
                .Index(t => t.TransactionId);
            
            CreateTable(
                "dbo.Relationships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Relationships_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CourseSessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CourseId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 1),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CourseSessions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .Index(t => new { t.Name, t.CourseId }, unique: true, name: "IX_CourseSessions");
            
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 15),
                        Code = c.String(maxLength: 9),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Levels_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IndicatorGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        SubjectId = c.Int(nullable: false),
                        ParentIndicatorGroupId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IndicatorGroups_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IndicatorGroups", t => t.ParentIndicatorGroupId)
                .ForeignKey("dbo.Subjects", t => t.SubjectId)
                .Index(t => t.SubjectId)
                .Index(t => t.ParentIndicatorGroupId);
            
            CreateTable(
                "dbo.Indicators",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 1000),
                        IndicatorGroupId = c.Int(nullable: false),
                        Sequence = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Indicators_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IndicatorGroups", t => t.IndicatorGroupId)
                .Index(t => new { t.IndicatorGroupId, t.Sequence }, unique: true, name: "UQ_Indicator_IndicatorGroup_Sequence");
            
            CreateTable(
                "dbo.StudentIllnesses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IllnessId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_StudentIllnesses_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_StudentIllnesses_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Illnesses", t => t.IllnessId)
                .ForeignKey("dbo.Students", t => t.StudentId)
                .Index(t => t.IllnessId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.Illnesses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Illnesses_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_IllnessesName");
            
            CreateTable(
                "dbo.ApplicationDates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationDate = c.DateTime(nullable: false),
                        WorkerId = c.Int(nullable: false),
                        LastModifiedDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ApplicationDates_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ArticleShops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sequence = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 200),
                        Reference = c.String(nullable: false, maxLength: 4),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitId = c.Int(),
                        BrandId = c.Int(),
                        ButtonPositionId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArticleShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ArticleShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brands", t => t.BrandId)
                .ForeignKey("dbo.ButtonPositions", t => t.ButtonPositionId)
                .ForeignKey("dbo.Units", t => t.UnitId)
                .Index(t => t.Description, name: "IX_ArticleDescription")
                .Index(t => t.Reference, name: "IX_ArticleReference")
                .Index(t => t.UnitId)
                .Index(t => t.BrandId)
                .Index(t => new { t.TenantId, t.ButtonPositionId }, unique: true, name: "UQ_ArticleShops_Tenant_ButtonPosition");
            
            CreateTable(
                "dbo.ButtonPositions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(maxLength: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ButtonPositions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Number, unique: true, name: "IX_ButtonPositionNumber");
            
            CreateTable(
                "dbo.InventoryShops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArticleShopId = c.Int(nullable: false),
                        Existence = c.Long(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArticleShops", t => t.ArticleShopId)
                .Index(t => t.ArticleShopId);
            
            CreateTable(
                "dbo.ChangeDateHistoryReceipts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionId = c.Long(nullable: false),
                        Description = c.String(maxLength: 100),
                        DateLast = c.DateTime(nullable: false),
                        DatePrior = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ChangeDateHistoryReceipts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.TransactionId);
            
            CreateTable(
                "dbo.ClientShops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sequence = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        Reference = c.String(nullable: false, maxLength: 20),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClientShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ClientShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, name: "IX_ClientShopName")
                .Index(t => t.Reference, name: "IX_ClientShopReference");
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RNC = c.String(maxLength: 15),
                        Name = c.String(maxLength: 200),
                        CommercialName = c.String(maxLength: 200),
                        CommercialActivity = c.String(maxLength: 200),
                        CompanyDate = c.String(maxLength: 10),
                        Status = c.String(maxLength: 15),
                        PaymentSystem = c.String(maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Companies_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CourrierPersons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 75),
                        FuntionDescription = c.String(maxLength: 150),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CourrierPersons_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_CourrierPersonName");
            
            CreateTable(
                "dbo.CourseSubjectPositions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CourseId = c.Int(nullable: false),
                        SubjectHighId = c.Int(nullable: false),
                        PeriodId = c.Int(nullable: false),
                        Position = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CourseSubjectPositions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .ForeignKey("dbo.SubjectHighs", t => t.SubjectHighId)
                .Index(t => t.CourseId)
                .Index(t => t.SubjectHighId)
                .Index(t => t.PeriodId);
            
            CreateTable(
                "dbo.SubjectHighs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 80),
                        LongName = c.String(nullable: false, maxLength: 350),
                        Code = c.String(nullable: false, maxLength: 20),
                        CourseId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SubjectHighs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.CourseValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PeriodId = c.Int(nullable: false),
                        CourseId = c.Int(nullable: false),
                        InscriptionAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CourseValues_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CourseValues_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .Index(t => new { t.TenantId, t.PeriodId, t.CourseId }, unique: true, name: "UQ_CourseValuesTenantPeriods");
            
            CreateTable(
                "dbo.DiscountNames",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 30),
                        HasGrantEnterprise = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DiscountNames_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_DiscountNamesName");
            
            CreateTable(
                "dbo.PeriodDiscounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DiscountNameId = c.Int(nullable: false),
                        PeriodId = c.Int(nullable: false),
                        ConceptId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PeriodDiscounts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Concepts", t => t.ConceptId)
                .ForeignKey("dbo.DiscountNames", t => t.DiscountNameId)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .Index(t => new { t.DiscountNameId, t.PeriodId, t.ConceptId }, unique: true, name: "IX_PeriodDiscountsPeriodNamePeriod");
            
            CreateTable(
                "dbo.DiscountNameTenants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PeriodDiscountId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DiscountNameTenants_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PeriodDiscounts", t => t.PeriodDiscountId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.PeriodDiscountId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.PeriodDiscountDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PeriodDiscountId = c.Int(nullable: false),
                        DiscountPercent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PeriodDiscountDetails_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PeriodDiscounts", t => t.PeriodDiscountId)
                .Index(t => new { t.PeriodDiscountId, t.DiscountPercent }, unique: true, name: "IX_PeriodDiscountDetailsPeriodPercent");
            
            CreateTable(
                "dbo.Discounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StudentId = c.Int(nullable: false),
                        PeriodId = c.Int(nullable: false),
                        ConceptId = c.Int(nullable: false),
                        PeriodDiscountDetailId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Discounts_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Discounts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Concepts", t => t.ConceptId)
                .ForeignKey("dbo.PeriodDiscountDetails", t => t.PeriodDiscountDetailId)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .ForeignKey("dbo.Students", t => t.StudentId)
                .Index(t => new { t.TenantId, t.StudentId, t.PeriodDiscountDetailId, t.ConceptId, t.PeriodId }, unique: true, name: "IX_DiscountConceptStudentTenant");
            
            CreateTable(
                "dbo.DocumentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_DocumentTypesName");
            
            CreateTable(
                "dbo.ErrorImportings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Details = c.String(),
                        InnserException = c.String(),
                        CurrentItem = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ErrorImportings_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EvaluationHighs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubjectHighId = c.Int(nullable: false),
                        EnrollmentStudentId = c.Int(nullable: false),
                        EvaluationOrderHighId = c.Int(nullable: false),
                        Expression = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EvaluationHighs_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EvaluationHighs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EnrollmentStudents", t => t.EnrollmentStudentId)
                .ForeignKey("dbo.EvaluationOrderHighs", t => t.EvaluationOrderHighId)
                .ForeignKey("dbo.SubjectHighs", t => t.SubjectHighId)
                .Index(t => new { t.TenantId, t.SubjectHighId, t.EnrollmentStudentId, t.EvaluationOrderHighId }, unique: true, name: "UQ_EvaluationHighs_Subje_Enrol_Evalu_Tenant");
            
            CreateTable(
                "dbo.EvaluationOrderHighs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EvaluationPositionHighId = c.Int(nullable: false),
                        Month = c.Int(nullable: false),
                        Description = c.String(maxLength: 15),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EvaluationOrderHighs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EvaluationPositionHighs", t => t.EvaluationPositionHighId)
                .Index(t => t.EvaluationPositionHighId);
            
            CreateTable(
                "dbo.EvaluationPositionHighs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 15),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EvaluationPositionHighs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EvaluationLegends",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 1),
                        Description = c.String(maxLength: 255),
                        Sequence = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EvaluationLegends_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "UQ_EvaluationLegends_Name");
            
            CreateTable(
                "dbo.EvaluationPeriods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 15),
                        Position = c.Int(nullable: false),
                        InitialMonth = c.Int(nullable: false),
                        FinalMonth = c.Int(nullable: false),
                        PeriodId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EvaluationPeriods_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .Index(t => new { t.Name, t.PeriodId }, unique: true, name: "UQ_EvaluationPerios_Name_Period");
            
            CreateTable(
                "dbo.Evaluations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IndicatorId = c.Int(nullable: false),
                        EvaluationLegendId = c.Int(),
                        EnrollmentStudentId = c.Int(nullable: false),
                        EvaluationPeriodId = c.Int(nullable: false),
                        ComplementForDeletion = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Evaluations_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Evaluations_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EnrollmentStudents", t => t.EnrollmentStudentId)
                .ForeignKey("dbo.EvaluationLegends", t => t.EvaluationLegendId)
                .ForeignKey("dbo.EvaluationPeriods", t => t.EvaluationPeriodId)
                .ForeignKey("dbo.Indicators", t => t.IndicatorId)
                .Index(t => new { t.IndicatorId, t.EvaluationLegendId, t.EnrollmentStudentId, t.EvaluationPeriodId, t.ComplementForDeletion }, unique: true, name: "UQ_Evaluations_Indicator_ES_P_EV_Tenant");
            
            CreateTable(
                "dbo.HistoryCancelReceipts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionId = c.Long(nullable: false),
                        Description = c.String(maxLength: 100),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.DateTime(nullable: false),
                        CA = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HistoryCancelReceipts_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_HistoryCancelReceipts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.TransactionId);
            
            CreateTable(
                "dbo.InventoryDateDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryDateId = c.Int(nullable: false),
                        ArticleId = c.Int(nullable: false),
                        excistence = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryDateDetails_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryDateDetails_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId)
                .ForeignKey("dbo.InventoryDates", t => t.InventoryDateId)
                .Index(t => t.InventoryDateId)
                .Index(t => t.ArticleId);
            
            CreateTable(
                "dbo.InventoryDates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(maxLength: 150),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryDates_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryDates_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InventoryShopDateDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryShopDateId = c.Int(nullable: false),
                        ArticleShopId = c.Int(nullable: false),
                        excistence = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryShopDateDetails_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryShopDateDetails_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArticleShops", t => t.ArticleShopId)
                .ForeignKey("dbo.InventoryShopDates", t => t.InventoryShopDateId)
                .Index(t => t.InventoryShopDateId)
                .Index(t => t.ArticleShopId);
            
            CreateTable(
                "dbo.InventoryShopDates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(maxLength: 150),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryShopDates_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryShopDates_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Literals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Letter = c.String(maxLength: 1),
                        FinalPercent = c.Int(nullable: false),
                        InitialPercent = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Literals_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Letter, unique: true, name: "UQ_LiteralLetter");
            
            CreateTable(
                "dbo.MasterTenants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        ShortName = c.String(maxLength: 100),
                        Address = c.String(maxLength: 200),
                        CityId = c.Int(nullable: false),
                        Phone1 = c.String(maxLength: 15),
                        Phone2 = c.String(maxLength: 15),
                        Phone3 = c.String(maxLength: 15),
                        DirectorName = c.String(maxLength: 100),
                        AccountantName = c.String(maxLength: 100),
                        TreasurerName = c.String(maxLength: 100),
                        CurrencyCode = c.String(maxLength: 4),
                        CreditCardPercent = c.Decimal(nullable: false, precision: 18, scale: 6),
                        CatalogForCreditCardId = c.Int(),
                        ConceptForInscriptionTransactionsId = c.Int(),
                        ConceptForMonthlyPaymentTransactionsId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MasterTenants_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Catalogs", t => t.CatalogForCreditCardId)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .ForeignKey("dbo.Concepts", t => t.ConceptForMonthlyPaymentTransactionsId)
                .ForeignKey("dbo.Concepts", t => t.ConceptForInscriptionTransactionsId)
                .Index(t => t.CityId)
                .Index(t => t.CatalogForCreditCardId)
                .Index(t => t.ConceptForInscriptionTransactionsId)
                .Index(t => t.ConceptForMonthlyPaymentTransactionsId);
            
            CreateTable(
                "dbo.MessageReads",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        MessageId = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MessageRead_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Messages", t => t.MessageId)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .Index(t => t.MessageId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DestinationUserId = c.Long(),
                        TenantId = c.Int(),
                        Subject = c.String(nullable: false, maxLength: 200),
                        Body = c.String(nullable: false),
                        AllTenant = c.Boolean(nullable: false),
                        AllUser = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Messages_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.PaymentDates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sequence = c.Int(nullable: false),
                        PaymentDay = c.Int(nullable: false),
                        PaymentMonth = c.Int(nullable: false),
                        PaymentYear = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PaymentDates_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PaymentDates_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Providers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        Reference = c.String(nullable: false, maxLength: 20),
                        Rnc = c.String(maxLength: 15),
                        Phone1 = c.String(maxLength: 15),
                        Phone2 = c.String(maxLength: 15),
                        Address = c.String(maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Providers_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_ProviderName")
                .Index(t => t.Reference, unique: true, name: "IX_ProviderReference");
            
            CreateTable(
                "dbo.ProviderShops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sequence = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        Reference = c.String(nullable: false, maxLength: 20),
                        Rnc = c.String(maxLength: 15),
                        Phone1 = c.String(maxLength: 15),
                        Phone2 = c.String(maxLength: 15),
                        Address = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProviderShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProviderShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, name: "IX_ProviderShopName")
                .Index(t => t.Reference, name: "IX_ProviderShopReference");
            
            CreateTable(
                "dbo.PurchaseArticleShops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PurchaseShopId = c.Int(nullable: false),
                        ArticleShopId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseArticleShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseArticleShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArticleShops", t => t.ArticleShopId)
                .ForeignKey("dbo.PurchaseShops", t => t.PurchaseShopId)
                .Index(t => t.PurchaseShopId)
                .Index(t => t.ArticleShopId);
            
            CreateTable(
                "dbo.PurchaseShops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sequence = c.Int(nullable: false),
                        Document = c.String(maxLength: 25),
                        ProviderShopId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.DateTime(nullable: false),
                        Comnent1 = c.String(maxLength: 250),
                        Comment2 = c.String(maxLength: 250),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProviderShops", t => t.ProviderShopId)
                .Index(t => t.ProviderShopId);
            
            CreateTable(
                "dbo.PurchaseArticles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PurchaseId = c.Int(nullable: false),
                        ArticleId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseArticles_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId)
                .ForeignKey("dbo.Purchases", t => t.PurchaseId)
                .Index(t => t.PurchaseId)
                .Index(t => t.ArticleId);
            
            CreateTable(
                "dbo.Purchases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Document = c.String(maxLength: 25),
                        ProviderId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comnent1 = c.String(maxLength: 250),
                        Comment2 = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Purchases_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Providers", t => t.ProviderId)
                .Index(t => t.ProviderId);
            
            CreateTable(
                "dbo.ReportsQueries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ValueId = c.String(nullable: false, maxLength: 20),
                        ValueDisplay = c.String(nullable: false, maxLength: 40),
                        ReportsFiltersId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsQueries_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ReportsFilters", t => t.ReportsFiltersId)
                .Index(t => t.ReportsFiltersId);
            
            CreateTable(
                "dbo.ScoreTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 35),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ScoreTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_ScoreTypeName");
            
            CreateTable(
                "dbo.Sequences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 5),
                        Name = c.String(maxLength: 30),
                        Sequence = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Sequences_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true, name: "IX_PassportSequencesCode")
                .Index(t => t.Name, unique: true, name: "IX_PassportSequencesName");
            
            CreateTable(
                "dbo.SubjectSessionHighs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubjectHighId = c.Int(nullable: false),
                        TeacherTenantId = c.Int(nullable: false),
                        CourseSessionId = c.Int(nullable: false),
                        PeriodId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SubjectSessionHighs_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SubjectSessionHighs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CourseSessions", t => t.CourseSessionId)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .ForeignKey("dbo.SubjectHighs", t => t.SubjectHighId)
                .ForeignKey("dbo.TeacherTenants", t => t.TeacherTenantId)
                .Index(t => t.SubjectHighId)
                .Index(t => t.TeacherTenantId)
                .Index(t => t.CourseSessionId)
                .Index(t => t.PeriodId);
            
            CreateTable(
                "dbo.SubjectSessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubjectId = c.Int(nullable: false),
                        TeacherTenantId = c.Int(nullable: false),
                        CourseSessionId = c.Int(nullable: false),
                        PeriodId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SubjectSessions_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SubjectSessions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CourseSessions", t => t.CourseSessionId)
                .ForeignKey("dbo.Periods", t => t.PeriodId)
                .ForeignKey("dbo.Subjects", t => t.SubjectId)
                .ForeignKey("dbo.TeacherTenants", t => t.TeacherTenantId)
                .Index(t => t.SubjectId)
                .Index(t => t.TeacherTenantId)
                .Index(t => t.CourseSessionId)
                .Index(t => t.PeriodId);
            
            CreateTable(
                "dbo.TaxReceipts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaxReceiptTypeId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        InitialNumber = c.Long(nullable: false),
                        FinalNumber = c.Long(nullable: false),
                        CurrentSequence = c.Long(nullable: false),
                        RegionId = c.Int(nullable: false),
                        Serie = c.String(nullable: false, maxLength: 1),
                        ExpirationDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TaxReceipts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .ForeignKey("dbo.TaxReceiptTypes", t => t.TaxReceiptTypeId)
                .Index(t => new { t.TaxReceiptTypeId, t.RegionId, t.ExpirationDate }, unique: true, name: "IX_TaxReceiptTypeRegionBase");
            
            CreateTable(
                "dbo.TaxReceiptTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Code = c.String(nullable: false, maxLength: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TaxReceiptTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_TaxReceiptTypesName")
                .Index(t => t.Code, unique: true, name: "IX_TaxReceiptTypesCode");
            
            CreateTable(
                "dbo.TradeArticles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TradeId = c.Int(nullable: false),
                        ArticleId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TradeArticles_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId)
                .ForeignKey("dbo.Trades", t => t.TradeId)
                .Index(t => t.TradeId)
                .Index(t => t.ArticleId);
            
            CreateTable(
                "dbo.Trades",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Comment = c.String(maxLength: 250),
                        CommentSend = c.String(maxLength: 250),
                        CommentReceive = c.String(maxLength: 250),
                        CourrierPersonId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        DateSend = c.DateTime(nullable: false),
                        DateReceive = c.DateTime(),
                        TenantReceiveId = c.Int(),
                        UserReceiveId = c.Long(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Trades_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CourrierPersons", t => t.CourrierPersonId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantReceiveId)
                .ForeignKey("dbo.AbpUsers", t => t.UserReceiveId)
                .Index(t => t.CourrierPersonId)
                .Index(t => t.TenantReceiveId)
                .Index(t => t.UserReceiveId);
            
            CreateTable(
                "dbo.TransactionArticleShops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionShopId = c.Long(nullable: false),
                        ArticleShopId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionArticleShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionArticleShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArticleShops", t => t.ArticleShopId)
                .ForeignKey("dbo.TransactionShops", t => t.TransactionShopId)
                .Index(t => t.TransactionShopId)
                .Index(t => t.ArticleShopId);
            
            CreateTable(
                "dbo.TransactionShops",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Sequence = c.Int(nullable: false),
                        Number = c.String(maxLength: 20),
                        Date = c.DateTime(nullable: false),
                        Name = c.String(maxLength: 100),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClientShopId = c.Int(nullable: false),
                        PaymentMethodId = c.Int(),
                        OriginId = c.Int(nullable: false),
                        TransactionTypeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClientShops", t => t.ClientShopId)
                .ForeignKey("dbo.Origins", t => t.OriginId)
                .ForeignKey("dbo.PaymentMethods", t => t.PaymentMethodId)
                .ForeignKey("dbo.TransactionTypes", t => t.TransactionTypeId)
                .Index(t => t.ClientShopId)
                .Index(t => t.PaymentMethodId)
                .Index(t => t.OriginId)
                .Index(t => t.TransactionTypeId);
            
            CreateTable(
                "dbo.TransactionQueues",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TransactionId = c.Long(nullable: false),
                        Send = c.Boolean(nullable: false),
                        SendDate = c.DateTime(),
                        LastTryDate = c.DateTime(),
                        Retry = c.Long(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionQueues_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionQueues_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.TransactionId);
            
            CreateTable(
                "dbo.VerifoneBreaks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sequence = c.Int(nullable: false),
                        BankId = c.Int(),
                        BankAccountId = c.Int(),
                        Date = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreditCardPercent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comments = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_VerifoneBreaks_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_VerifoneBreaks_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BankAccounts", t => t.BankAccountId)
                .ForeignKey("dbo.Banks", t => t.BankId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.BankId)
                .Index(t => t.BankAccountId)
                .Index(t => t.TenantId);
            
            AddColumn("dbo.AbpRoles", "AffectTenants", c => c.Boolean());
            AddColumn("dbo.AbpRoles", "ParentRolId", c => c.Int());
            AddColumn("dbo.AbpUsers", "ParentUserId", c => c.Long());
            AddColumn("dbo.AbpUsers", "MustChangePassword", c => c.Boolean());
            AddColumn("dbo.AbpUsers", "UserTypeId", c => c.Int());
            AddColumn("dbo.AbpUsers", "PositionId", c => c.Int());
            AddColumn("dbo.AbpTenants", "Phone1", c => c.String(maxLength: 15));
            AddColumn("dbo.AbpTenants", "Phone2", c => c.String(maxLength: 15));
            AddColumn("dbo.AbpTenants", "Phone3", c => c.String(maxLength: 15));
            AddColumn("dbo.AbpTenants", "Address", c => c.String(maxLength: 100));
            AddColumn("dbo.AbpTenants", "CityId", c => c.Int());
            AddColumn("dbo.AbpTenants", "DirectorName", c => c.String(maxLength: 100));
            AddColumn("dbo.AbpTenants", "RegisterName", c => c.String(maxLength: 100));
            AddColumn("dbo.AbpTenants", "PeriodId", c => c.Int());
            AddColumn("dbo.AbpTenants", "PreviousPeriodId", c => c.Int());
            AddColumn("dbo.AbpTenants", "NextPeriodId", c => c.Int());
            AddColumn("dbo.AbpTenants", "DistrictId", c => c.Int());
            AddColumn("dbo.AbpTenants", "AccreditationNumber", c => c.String(maxLength: 15));
            AddColumn("dbo.AbpTenants", "EducationalDistrict", c => c.String(maxLength: 15));
            AddColumn("dbo.AbpTenants", "Regional", c => c.String(maxLength: 60));
            AddColumn("dbo.AbpTenants", "RegionalDirector", c => c.String(maxLength: 60));
            AddColumn("dbo.AbpTenants", "DistritctDirector", c => c.String(maxLength: 60));
            AddColumn("dbo.AbpTenants", "Comment", c => c.String(maxLength: 350));
            AddColumn("dbo.AbpTenants", "PrinterTypeId", c => c.Int());
            AddColumn("dbo.AbpTenants", "Abbreviation", c => c.String(maxLength: 15));
            AddColumn("dbo.AbpTenants", "ApplyAutomaticCharges", c => c.Boolean(nullable: false));
            AddColumn("dbo.AbpTenants", "Email", c => c.String());
            AddColumn("dbo.AbpTenants", "CreditCardPercent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.AbpTenants", "FreeDateReceipt", c => c.DateTime());
            AlterColumn("dbo.AbpTenants", "Name", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.AbpTenants", "TenancyName", c => c.String(nullable: false, maxLength: 150));
            CreateIndex("dbo.AbpTenants", "CityId");
            CreateIndex("dbo.AbpTenants", "PeriodId");
            CreateIndex("dbo.AbpTenants", "PreviousPeriodId");
            CreateIndex("dbo.AbpTenants", "NextPeriodId");
            CreateIndex("dbo.AbpTenants", "DistrictId");
            CreateIndex("dbo.AbpTenants", "PrinterTypeId");
            CreateIndex("dbo.AbpUsers", "UserName", unique: true, name: "IX_UserUserName");
            CreateIndex("dbo.AbpUsers", "UserTypeId");
            CreateIndex("dbo.AbpUsers", "PositionId");
            CreateIndex("dbo.AbpUsers", "TenantId");
            AddForeignKey("dbo.AbpTenants", "CityId", "dbo.Cities", "Id");
            AddForeignKey("dbo.AbpUsers", "PositionId", "dbo.Positions", "Id");
            AddForeignKey("dbo.AbpUsers", "TenantId", "dbo.AbpTenants", "Id");
            AddForeignKey("dbo.AbpUsers", "UserTypeId", "dbo.UserTypes", "Id");
            AddForeignKey("dbo.AbpTenants", "DistrictId", "dbo.Districts", "Id");
            AddForeignKey("dbo.AbpTenants", "NextPeriodId", "dbo.Periods", "Id");
            AddForeignKey("dbo.AbpTenants", "PeriodId", "dbo.Periods", "Id");
            AddForeignKey("dbo.AbpTenants", "PreviousPeriodId", "dbo.Periods", "Id");
            AddForeignKey("dbo.AbpTenants", "PrinterTypeId", "dbo.PrinterTypes", "Id");
            AddForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions", "Id");
            AddForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles", "Id");
            AddForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles");
            DropForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions");
            DropForeignKey("dbo.VerifoneBreaks", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.VerifoneBreaks", "BankId", "dbo.Banks");
            DropForeignKey("dbo.VerifoneBreaks", "BankAccountId", "dbo.BankAccounts");
            DropForeignKey("dbo.TransactionQueues", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.TransactionShops", "TransactionTypeId", "dbo.TransactionTypes");
            DropForeignKey("dbo.TransactionArticleShops", "TransactionShopId", "dbo.TransactionShops");
            DropForeignKey("dbo.TransactionShops", "PaymentMethodId", "dbo.PaymentMethods");
            DropForeignKey("dbo.TransactionShops", "OriginId", "dbo.Origins");
            DropForeignKey("dbo.TransactionShops", "ClientShopId", "dbo.ClientShops");
            DropForeignKey("dbo.TransactionArticleShops", "ArticleShopId", "dbo.ArticleShops");
            DropForeignKey("dbo.TradeArticles", "TradeId", "dbo.Trades");
            DropForeignKey("dbo.Trades", "UserReceiveId", "dbo.AbpUsers");
            DropForeignKey("dbo.Trades", "TenantReceiveId", "dbo.AbpTenants");
            DropForeignKey("dbo.Trades", "CourrierPersonId", "dbo.CourrierPersons");
            DropForeignKey("dbo.TradeArticles", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.TaxReceipts", "TaxReceiptTypeId", "dbo.TaxReceiptTypes");
            DropForeignKey("dbo.TaxReceipts", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.SubjectSessions", "TeacherTenantId", "dbo.TeacherTenants");
            DropForeignKey("dbo.SubjectSessions", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.SubjectSessions", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.SubjectSessions", "CourseSessionId", "dbo.CourseSessions");
            DropForeignKey("dbo.SubjectSessionHighs", "TeacherTenantId", "dbo.TeacherTenants");
            DropForeignKey("dbo.SubjectSessionHighs", "SubjectHighId", "dbo.SubjectHighs");
            DropForeignKey("dbo.SubjectSessionHighs", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.SubjectSessionHighs", "CourseSessionId", "dbo.CourseSessions");
            DropForeignKey("dbo.Subjects", "ScoreTypes_Id", "dbo.ScoreTypes");
            DropForeignKey("dbo.ReportsQueries", "ReportsFiltersId", "dbo.ReportsFilters");
            DropForeignKey("dbo.PurchaseArticles", "PurchaseId", "dbo.Purchases");
            DropForeignKey("dbo.Purchases", "ProviderId", "dbo.Providers");
            DropForeignKey("dbo.PurchaseArticles", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.PurchaseArticleShops", "PurchaseShopId", "dbo.PurchaseShops");
            DropForeignKey("dbo.PurchaseShops", "ProviderShopId", "dbo.ProviderShops");
            DropForeignKey("dbo.PurchaseArticleShops", "ArticleShopId", "dbo.ArticleShops");
            DropForeignKey("dbo.MessageReads", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.Messages", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.MessageReads", "MessageId", "dbo.Messages");
            DropForeignKey("dbo.MasterTenants", "ConceptForInscriptionTransactionsId", "dbo.Concepts");
            DropForeignKey("dbo.MasterTenants", "ConceptForMonthlyPaymentTransactionsId", "dbo.Concepts");
            DropForeignKey("dbo.MasterTenants", "CityId", "dbo.Cities");
            DropForeignKey("dbo.MasterTenants", "CatalogForCreditCardId", "dbo.Catalogs");
            DropForeignKey("dbo.InventoryShopDateDetails", "InventoryShopDateId", "dbo.InventoryShopDates");
            DropForeignKey("dbo.InventoryShopDateDetails", "ArticleShopId", "dbo.ArticleShops");
            DropForeignKey("dbo.InventoryDateDetails", "InventoryDateId", "dbo.InventoryDates");
            DropForeignKey("dbo.InventoryDateDetails", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.HistoryCancelReceipts", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.EvaluationPeriods", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.Evaluations", "IndicatorId", "dbo.Indicators");
            DropForeignKey("dbo.Evaluations", "EvaluationPeriodId", "dbo.EvaluationPeriods");
            DropForeignKey("dbo.Evaluations", "EvaluationLegendId", "dbo.EvaluationLegends");
            DropForeignKey("dbo.Evaluations", "EnrollmentStudentId", "dbo.EnrollmentStudents");
            DropForeignKey("dbo.EvaluationHighs", "SubjectHighId", "dbo.SubjectHighs");
            DropForeignKey("dbo.EvaluationHighs", "EvaluationOrderHighId", "dbo.EvaluationOrderHighs");
            DropForeignKey("dbo.EvaluationOrderHighs", "EvaluationPositionHighId", "dbo.EvaluationPositionHighs");
            DropForeignKey("dbo.EvaluationHighs", "EnrollmentStudentId", "dbo.EnrollmentStudents");
            DropForeignKey("dbo.Discounts", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Discounts", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.Discounts", "PeriodDiscountDetailId", "dbo.PeriodDiscountDetails");
            DropForeignKey("dbo.Discounts", "ConceptId", "dbo.Concepts");
            DropForeignKey("dbo.PeriodDiscounts", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.PeriodDiscountDetails", "PeriodDiscountId", "dbo.PeriodDiscounts");
            DropForeignKey("dbo.DiscountNameTenants", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DiscountNameTenants", "PeriodDiscountId", "dbo.PeriodDiscounts");
            DropForeignKey("dbo.PeriodDiscounts", "DiscountNameId", "dbo.DiscountNames");
            DropForeignKey("dbo.PeriodDiscounts", "ConceptId", "dbo.Concepts");
            DropForeignKey("dbo.CourseValues", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.CourseValues", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.CourseSubjectPositions", "SubjectHighId", "dbo.SubjectHighs");
            DropForeignKey("dbo.SubjectHighs", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.CourseSubjectPositions", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.CourseSubjectPositions", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.ChangeDateHistoryReceipts", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.ArticleShops", "UnitId", "dbo.Units");
            DropForeignKey("dbo.InventoryShops", "ArticleShopId", "dbo.ArticleShops");
            DropForeignKey("dbo.ArticleShops", "ButtonPositionId", "dbo.ButtonPositions");
            DropForeignKey("dbo.ArticleShops", "BrandId", "dbo.Brands");
            DropForeignKey("dbo.StudentIllnesses", "StudentId", "dbo.Students");
            DropForeignKey("dbo.StudentIllnesses", "IllnessId", "dbo.Illnesses");
            DropForeignKey("dbo.StudentAllergies", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Documents", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.Documents", "TeacherId", "dbo.Teachers");
            DropForeignKey("dbo.Teachers", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.Subjects", "Teachers_Id", "dbo.Teachers");
            DropForeignKey("dbo.IndicatorGroups", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Indicators", "IndicatorGroupId", "dbo.IndicatorGroups");
            DropForeignKey("dbo.IndicatorGroups", "ParentIndicatorGroupId", "dbo.IndicatorGroups");
            DropForeignKey("dbo.Subjects", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Courses", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.CourseSessions", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.CourseEnrollmentStudents", "SessionId", "dbo.CourseSessions");
            DropForeignKey("dbo.EnrollmentStudents", "StudentId", "dbo.Students");
            DropForeignKey("dbo.EnrollmentStudents", "RelationshipId", "dbo.Relationships");
            DropForeignKey("dbo.EnrollmentStudents", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.PaymentProjections", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.PaymentProjections", "EnrollmentStudentId", "dbo.EnrollmentStudents");
            DropForeignKey("dbo.Transactions", "TransactionTypeId", "dbo.TransactionTypes");
            DropForeignKey("dbo.TransactionStudents", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.TransactionStudents", "EnrollmentStudentId", "dbo.EnrollmentStudents");
            DropForeignKey("dbo.TransactionDetails", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.TransactionDetails", "CatalogId", "dbo.Catalogs");
            DropForeignKey("dbo.TransactionConcepts", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.TransactionConcepts", "ConceptId", "dbo.Concepts");
            DropForeignKey("dbo.TransactionArticles", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.TransactionArticles", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.Articles", "UnitId", "dbo.Units");
            DropForeignKey("dbo.Inventories", "TenantInventoryId", "dbo.AbpTenants");
            DropForeignKey("dbo.Inventories", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.Articles", "BrandId", "dbo.Brands");
            DropForeignKey("dbo.Articles", "ArticleTypeId", "dbo.ArticlesTypes");
            DropForeignKey("dbo.Transactions", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.TeacherTenants", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.TeacherTenants", "TeacherId", "dbo.Teachers");
            DropForeignKey("dbo.AbpTenants", "PrinterTypeId", "dbo.PrinterTypes");
            DropForeignKey("dbo.PrinterTypes", "ReportId", "dbo.Reports");
            DropForeignKey("dbo.Reports", "ReportsTypesId", "dbo.ReportsTypes");
            DropForeignKey("dbo.ReportsFilters", "ReportsId", "dbo.Reports");
            DropForeignKey("dbo.ReportsFilters", "DataTypeId", "dbo.DataTypes");
            DropForeignKey("dbo.Reports", "ReportDataSourceId", "dbo.ReportDataSources");
            DropForeignKey("dbo.AbpTenants", "PreviousPeriodId", "dbo.Periods");
            DropForeignKey("dbo.AbpTenants", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.AbpTenants", "NextPeriodId", "dbo.Periods");
            DropForeignKey("dbo.AbpTenants", "DistrictId", "dbo.Districts");
            DropForeignKey("dbo.AbpUsers", "UserTypeId", "dbo.UserTypes");
            DropForeignKey("dbo.UserTenants", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.UserTenants", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.UserPictures", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUsers", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.AbpUsers", "PositionId", "dbo.Positions");
            DropForeignKey("dbo.ConceptTenantRegistrations", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.ConceptTenantRegistrations", "ConceptId", "dbo.Concepts");
            DropForeignKey("dbo.AbpTenants", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Transactions", "StatusId", "dbo.Statuses");
            DropForeignKey("dbo.TransactionHistories", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.TransactionHistories", "StatusId", "dbo.Statuses");
            DropForeignKey("dbo.TransactionHistories", "ChangeTypeId", "dbo.ChangeTypes");
            DropForeignKey("dbo.Transactions", "ReceiptTypeId", "dbo.ReceiptTypes");
            DropForeignKey("dbo.Transactions", "PaymentMethodId", "dbo.PaymentMethods");
            DropForeignKey("dbo.Transactions", "OriginId", "dbo.Origins");
            DropForeignKey("dbo.Transactions", "GrantEnterpriseId", "dbo.GrantEnterprises");
            DropForeignKey("dbo.Transactions", "EnrollmentId", "dbo.Enrollments");
            DropForeignKey("dbo.Transactions", "ConceptId", "dbo.Concepts");
            DropForeignKey("dbo.Concepts", "T9DimensionId", "dbo.T9Dimensions");
            DropForeignKey("dbo.Concepts", "T8DimensionId", "dbo.T8Dimensions");
            DropForeignKey("dbo.Concepts", "T7DimensionId", "dbo.T7Dimensions");
            DropForeignKey("dbo.Concepts", "T6DimensionId", "dbo.T6Dimensions");
            DropForeignKey("dbo.Concepts", "T5DimensionId", "dbo.T5Dimensions");
            DropForeignKey("dbo.Concepts", "T4DimensionId", "dbo.T4Dimensions");
            DropForeignKey("dbo.Concepts", "T3DimensionId", "dbo.T3Dimensions");
            DropForeignKey("dbo.Concepts", "T2DimensionId", "dbo.T2Dimensions");
            DropForeignKey("dbo.Concepts", "T1DimensionId", "dbo.T1Dimensions");
            DropForeignKey("dbo.Concepts", "T10DimensionId", "dbo.T10Dimensions");
            DropForeignKey("dbo.Concepts", "ReceiptTypeId", "dbo.ReceiptTypes");
            DropForeignKey("dbo.Concepts", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.Concepts", "CatalogId", "dbo.Catalogs");
            DropForeignKey("dbo.Transactions", "CancelStatusId", "dbo.CancelStatus");
            DropForeignKey("dbo.Transactions", "BankAccountId", "dbo.BankAccounts");
            DropForeignKey("dbo.BankAccounts", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.Transactions", "BankId", "dbo.Banks");
            DropForeignKey("dbo.BankAccounts", "BanktId", "dbo.Banks");
            DropForeignKey("dbo.Enrollments", "ReligionId", "dbo.Religions");
            DropForeignKey("dbo.Enrollments", "OccupationId", "dbo.Occupations");
            DropForeignKey("dbo.Enrollments", "GenderId", "dbo.Genders");
            DropForeignKey("dbo.EnrollmentStudents", "EnrollmentId", "dbo.Enrollments");
            DropForeignKey("dbo.EnrollmentSequences", "EnrollmentId", "dbo.Enrollments");
            DropForeignKey("dbo.Provinces", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.Cities", "ProvinceId", "dbo.Provinces");
            DropForeignKey("dbo.Enrollments", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Configurations", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.MonthlyPeriods", "PeriodId", "dbo.Periods");
            DropForeignKey("dbo.Configurations", "CityId", "dbo.Cities");
            DropForeignKey("dbo.CourseEnrollmentStudents", "EnrollmentStudentId", "dbo.EnrollmentStudents");
            DropForeignKey("dbo.CourseEnrollmentStudents", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Teachers", "GenderId", "dbo.Genders");
            DropForeignKey("dbo.Students", "GenderId", "dbo.Genders");
            DropForeignKey("dbo.Documents", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Comments", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Comments", "CommentTypeId", "dbo.CommentTypes");
            DropForeignKey("dbo.Students", "BloodId", "dbo.Blood");
            DropForeignKey("dbo.StudentAllergies", "AllergyId", "dbo.Allergies");
            DropIndex("dbo.VerifoneBreaks", new[] { "TenantId" });
            DropIndex("dbo.VerifoneBreaks", new[] { "BankAccountId" });
            DropIndex("dbo.VerifoneBreaks", new[] { "BankId" });
            DropIndex("dbo.TransactionQueues", new[] { "TransactionId" });
            DropIndex("dbo.TransactionShops", new[] { "TransactionTypeId" });
            DropIndex("dbo.TransactionShops", new[] { "OriginId" });
            DropIndex("dbo.TransactionShops", new[] { "PaymentMethodId" });
            DropIndex("dbo.TransactionShops", new[] { "ClientShopId" });
            DropIndex("dbo.TransactionArticleShops", new[] { "ArticleShopId" });
            DropIndex("dbo.TransactionArticleShops", new[] { "TransactionShopId" });
            DropIndex("dbo.Trades", new[] { "UserReceiveId" });
            DropIndex("dbo.Trades", new[] { "TenantReceiveId" });
            DropIndex("dbo.Trades", new[] { "CourrierPersonId" });
            DropIndex("dbo.TradeArticles", new[] { "ArticleId" });
            DropIndex("dbo.TradeArticles", new[] { "TradeId" });
            DropIndex("dbo.TaxReceiptTypes", "IX_TaxReceiptTypesCode");
            DropIndex("dbo.TaxReceiptTypes", "IX_TaxReceiptTypesName");
            DropIndex("dbo.TaxReceipts", "IX_TaxReceiptTypeRegionBase");
            DropIndex("dbo.SubjectSessions", new[] { "PeriodId" });
            DropIndex("dbo.SubjectSessions", new[] { "CourseSessionId" });
            DropIndex("dbo.SubjectSessions", new[] { "TeacherTenantId" });
            DropIndex("dbo.SubjectSessions", new[] { "SubjectId" });
            DropIndex("dbo.SubjectSessionHighs", new[] { "PeriodId" });
            DropIndex("dbo.SubjectSessionHighs", new[] { "CourseSessionId" });
            DropIndex("dbo.SubjectSessionHighs", new[] { "TeacherTenantId" });
            DropIndex("dbo.SubjectSessionHighs", new[] { "SubjectHighId" });
            DropIndex("dbo.Sequences", "IX_PassportSequencesName");
            DropIndex("dbo.Sequences", "IX_PassportSequencesCode");
            DropIndex("dbo.ScoreTypes", "IX_ScoreTypeName");
            DropIndex("dbo.ReportsQueries", new[] { "ReportsFiltersId" });
            DropIndex("dbo.Purchases", new[] { "ProviderId" });
            DropIndex("dbo.PurchaseArticles", new[] { "ArticleId" });
            DropIndex("dbo.PurchaseArticles", new[] { "PurchaseId" });
            DropIndex("dbo.PurchaseShops", new[] { "ProviderShopId" });
            DropIndex("dbo.PurchaseArticleShops", new[] { "ArticleShopId" });
            DropIndex("dbo.PurchaseArticleShops", new[] { "PurchaseShopId" });
            DropIndex("dbo.ProviderShops", "IX_ProviderShopReference");
            DropIndex("dbo.ProviderShops", "IX_ProviderShopName");
            DropIndex("dbo.Providers", "IX_ProviderReference");
            DropIndex("dbo.Providers", "IX_ProviderName");
            DropIndex("dbo.Messages", new[] { "TenantId" });
            DropIndex("dbo.MessageReads", new[] { "UserId" });
            DropIndex("dbo.MessageReads", new[] { "MessageId" });
            DropIndex("dbo.MasterTenants", new[] { "ConceptForMonthlyPaymentTransactionsId" });
            DropIndex("dbo.MasterTenants", new[] { "ConceptForInscriptionTransactionsId" });
            DropIndex("dbo.MasterTenants", new[] { "CatalogForCreditCardId" });
            DropIndex("dbo.MasterTenants", new[] { "CityId" });
            DropIndex("dbo.Literals", "UQ_LiteralLetter");
            DropIndex("dbo.InventoryShopDateDetails", new[] { "ArticleShopId" });
            DropIndex("dbo.InventoryShopDateDetails", new[] { "InventoryShopDateId" });
            DropIndex("dbo.InventoryDateDetails", new[] { "ArticleId" });
            DropIndex("dbo.InventoryDateDetails", new[] { "InventoryDateId" });
            DropIndex("dbo.HistoryCancelReceipts", new[] { "TransactionId" });
            DropIndex("dbo.Evaluations", "UQ_Evaluations_Indicator_ES_P_EV_Tenant");
            DropIndex("dbo.EvaluationPeriods", "UQ_EvaluationPerios_Name_Period");
            DropIndex("dbo.EvaluationLegends", "UQ_EvaluationLegends_Name");
            DropIndex("dbo.EvaluationOrderHighs", new[] { "EvaluationPositionHighId" });
            DropIndex("dbo.EvaluationHighs", "UQ_EvaluationHighs_Subje_Enrol_Evalu_Tenant");
            DropIndex("dbo.DocumentTypes", "IX_DocumentTypesName");
            DropIndex("dbo.Discounts", "IX_DiscountConceptStudentTenant");
            DropIndex("dbo.PeriodDiscountDetails", "IX_PeriodDiscountDetailsPeriodPercent");
            DropIndex("dbo.DiscountNameTenants", new[] { "TenantId" });
            DropIndex("dbo.DiscountNameTenants", new[] { "PeriodDiscountId" });
            DropIndex("dbo.PeriodDiscounts", "IX_PeriodDiscountsPeriodNamePeriod");
            DropIndex("dbo.DiscountNames", "IX_DiscountNamesName");
            DropIndex("dbo.CourseValues", "UQ_CourseValuesTenantPeriods");
            DropIndex("dbo.SubjectHighs", new[] { "CourseId" });
            DropIndex("dbo.CourseSubjectPositions", new[] { "PeriodId" });
            DropIndex("dbo.CourseSubjectPositions", new[] { "SubjectHighId" });
            DropIndex("dbo.CourseSubjectPositions", new[] { "CourseId" });
            DropIndex("dbo.CourrierPersons", "IX_CourrierPersonName");
            DropIndex("dbo.ClientShops", "IX_ClientShopReference");
            DropIndex("dbo.ClientShops", "IX_ClientShopName");
            DropIndex("dbo.ChangeDateHistoryReceipts", new[] { "TransactionId" });
            DropIndex("dbo.InventoryShops", new[] { "ArticleShopId" });
            DropIndex("dbo.ButtonPositions", "IX_ButtonPositionNumber");
            DropIndex("dbo.ArticleShops", "UQ_ArticleShops_Tenant_ButtonPosition");
            DropIndex("dbo.ArticleShops", new[] { "BrandId" });
            DropIndex("dbo.ArticleShops", new[] { "UnitId" });
            DropIndex("dbo.ArticleShops", "IX_ArticleReference");
            DropIndex("dbo.ArticleShops", "IX_ArticleDescription");
            DropIndex("dbo.Illnesses", "IX_IllnessesName");
            DropIndex("dbo.StudentIllnesses", new[] { "StudentId" });
            DropIndex("dbo.StudentIllnesses", new[] { "IllnessId" });
            DropIndex("dbo.Indicators", "UQ_Indicator_IndicatorGroup_Sequence");
            DropIndex("dbo.IndicatorGroups", new[] { "ParentIndicatorGroupId" });
            DropIndex("dbo.IndicatorGroups", new[] { "SubjectId" });
            DropIndex("dbo.CourseSessions", "IX_CourseSessions");
            DropIndex("dbo.PaymentProjections", new[] { "TransactionId" });
            DropIndex("dbo.PaymentProjections", new[] { "EnrollmentStudentId" });
            DropIndex("dbo.TransactionTypes", "IX_TransactionTypesName");
            DropIndex("dbo.TransactionStudents", new[] { "EnrollmentStudentId" });
            DropIndex("dbo.TransactionStudents", new[] { "TransactionId" });
            DropIndex("dbo.TransactionDetails", new[] { "CatalogId" });
            DropIndex("dbo.TransactionDetails", new[] { "TransactionId" });
            DropIndex("dbo.TransactionConcepts", new[] { "ConceptId" });
            DropIndex("dbo.TransactionConcepts", new[] { "TransactionId" });
            DropIndex("dbo.Units", "IX_UnitDescription");
            DropIndex("dbo.Inventories", new[] { "TenantInventoryId" });
            DropIndex("dbo.Inventories", new[] { "ArticleId" });
            DropIndex("dbo.Brands", "IX_BrandName");
            DropIndex("dbo.ArticlesTypes", "IX_ArticleTypeDescription");
            DropIndex("dbo.Articles", new[] { "BrandId" });
            DropIndex("dbo.Articles", new[] { "UnitId" });
            DropIndex("dbo.Articles", new[] { "ArticleTypeId" });
            DropIndex("dbo.Articles", "IX_ArticleReference");
            DropIndex("dbo.Articles", "IX_ArticleDescription");
            DropIndex("dbo.TransactionArticles", new[] { "ArticleId" });
            DropIndex("dbo.TransactionArticles", new[] { "TransactionId" });
            DropIndex("dbo.TeacherTenants", "IX_TeacherTenantId_Tenant_Teacher");
            DropIndex("dbo.ReportsFilters", new[] { "DataTypeId" });
            DropIndex("dbo.ReportsFilters", new[] { "ReportsId" });
            DropIndex("dbo.Reports", new[] { "ReportDataSourceId" });
            DropIndex("dbo.Reports", new[] { "ReportsTypesId" });
            DropIndex("dbo.Reports", "IX_Report_ReportCode");
            DropIndex("dbo.PrinterTypes", new[] { "ReportId" });
            DropIndex("dbo.PrinterTypes", "IX_PrinterTypesName");
            DropIndex("dbo.Districts", "IX_DistricAbbrev");
            DropIndex("dbo.Districts", "IX_DistricName");
            DropIndex("dbo.UserTypes", new[] { "Description" });
            DropIndex("dbo.UserTenants", new[] { "TenantId" });
            DropIndex("dbo.UserTenants", new[] { "UserId" });
            DropIndex("dbo.UserPictures", new[] { "UserId" });
            DropIndex("dbo.Positions", "IX_PositonName");
            DropIndex("dbo.AbpUsers", new[] { "TenantId" });
            DropIndex("dbo.AbpUsers", new[] { "PositionId" });
            DropIndex("dbo.AbpUsers", new[] { "UserTypeId" });
            DropIndex("dbo.AbpUsers", "IX_UserUserName");
            DropIndex("dbo.ConceptTenantRegistrations", new[] { "TenantId" });
            DropIndex("dbo.ConceptTenantRegistrations", new[] { "ConceptId" });
            DropIndex("dbo.AbpTenants", new[] { "PrinterTypeId" });
            DropIndex("dbo.AbpTenants", new[] { "DistrictId" });
            DropIndex("dbo.AbpTenants", new[] { "NextPeriodId" });
            DropIndex("dbo.AbpTenants", new[] { "PreviousPeriodId" });
            DropIndex("dbo.AbpTenants", new[] { "PeriodId" });
            DropIndex("dbo.AbpTenants", new[] { "CityId" });
            DropIndex("dbo.ChangeTypes", "IX_ChangeTypesName");
            DropIndex("dbo.TransactionHistories", new[] { "TransactionId" });
            DropIndex("dbo.TransactionHistories", new[] { "ChangeTypeId" });
            DropIndex("dbo.TransactionHistories", new[] { "StatusId" });
            DropIndex("dbo.Statuses", "IX_StatusesName");
            DropIndex("dbo.PaymentMethods", "IX_PaymentMethodsName");
            DropIndex("dbo.Origins", "IX_OriginsName");
            DropIndex("dbo.GrantEnterprises", "IX_GrantEnterpriseName");
            DropIndex("dbo.ReceiptTypes", "IX_ReceiptTypesName");
            DropIndex("dbo.Catalogs", "IX_CatalogAccountNumber");
            DropIndex("dbo.Concepts", new[] { "T10DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T9DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T8DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T7DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T6DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T5DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T4DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T3DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T2DimensionId" });
            DropIndex("dbo.Concepts", new[] { "T1DimensionId" });
            DropIndex("dbo.Concepts", new[] { "ReceiptTypeId" });
            DropIndex("dbo.Concepts", new[] { "PeriodId" });
            DropIndex("dbo.Concepts", new[] { "CatalogId" });
            DropIndex("dbo.Concepts", "IX_ConceptTenantIdName");
            DropIndex("dbo.Banks", "IX_BanksName");
            DropIndex("dbo.BankAccounts", new[] { "RegionId" });
            DropIndex("dbo.BankAccounts", new[] { "BanktId" });
            DropIndex("dbo.BankAccounts", "IX_BankAccountsName");
            DropIndex("dbo.Transactions", new[] { "CancelStatusId" });
            DropIndex("dbo.Transactions", new[] { "StatusId" });
            DropIndex("dbo.Transactions", new[] { "OriginId" });
            DropIndex("dbo.Transactions", new[] { "PaymentMethodId" });
            DropIndex("dbo.Transactions", new[] { "ReceiptTypeId" });
            DropIndex("dbo.Transactions", new[] { "GrantEnterpriseId" });
            DropIndex("dbo.Transactions", new[] { "ConceptId" });
            DropIndex("dbo.Transactions", new[] { "EnrollmentId" });
            DropIndex("dbo.Transactions", new[] { "BankAccountId" });
            DropIndex("dbo.Transactions", new[] { "BankId" });
            DropIndex("dbo.Transactions", "UQ_TransactionTenantTransactionType");
            DropIndex("dbo.Religions", "IX_ReligionsShortName");
            DropIndex("dbo.Occupations", "IX_OccupationsName");
            DropIndex("dbo.EnrollmentSequences", "UQ_EnrollmentSequencesTenantIdEnrollmentId");
            DropIndex("dbo.Regions", "IX_RegionsName");
            DropIndex("dbo.Provinces", new[] { "RegionId" });
            DropIndex("dbo.Provinces", "IX_ProvincesName");
            DropIndex("dbo.MonthlyPeriods", "IX_MonthlyPeriodsYearMonth");
            DropIndex("dbo.Periods", "UQ_Periods1");
            DropIndex("dbo.Configurations", new[] { "PeriodId" });
            DropIndex("dbo.Configurations", new[] { "CityId" });
            DropIndex("dbo.Cities", "IX_CitiesName");
            DropIndex("dbo.Cities", new[] { "ProvinceId" });
            DropIndex("dbo.Enrollments", new[] { "ReligionId" });
            DropIndex("dbo.Enrollments", new[] { "OccupationId" });
            DropIndex("dbo.Enrollments", new[] { "GenderId" });
            DropIndex("dbo.Enrollments", new[] { "CityId" });
            DropIndex("dbo.EnrollmentStudents", new[] { "RelationshipId" });
            DropIndex("dbo.EnrollmentStudents", "UQ_EnrollmentStudentPeriodTenant");
            DropIndex("dbo.CourseEnrollmentStudents", new[] { "SessionId" });
            DropIndex("dbo.CourseEnrollmentStudents", new[] { "EnrollmentStudentId" });
            DropIndex("dbo.CourseEnrollmentStudents", new[] { "CourseId" });
            DropIndex("dbo.Courses", "IX_CoursesAbbreviationLevels");
            DropIndex("dbo.Courses", "IX_CoursesNameLevels");
            DropIndex("dbo.Subjects", new[] { "ScoreTypes_Id" });
            DropIndex("dbo.Subjects", new[] { "Teachers_Id" });
            DropIndex("dbo.Subjects", new[] { "CourseId" });
            DropIndex("dbo.Genders", "IX_GendersName");
            DropIndex("dbo.Teachers", new[] { "UserId" });
            DropIndex("dbo.Teachers", new[] { "GenderId" });
            DropIndex("dbo.Documents", new[] { "TeacherId" });
            DropIndex("dbo.Documents", new[] { "TransactionId" });
            DropIndex("dbo.Documents", new[] { "StudentId" });
            DropIndex("dbo.CommentTypes", "IX_CommentTypesName");
            DropIndex("dbo.Comments", new[] { "CommentTypeId" });
            DropIndex("dbo.Comments", new[] { "StudentId" });
            DropIndex("dbo.Blood", "IX_BloodsName");
            DropIndex("dbo.Students", "IX_StudentsStudentPersonalId");
            DropIndex("dbo.Students", new[] { "BloodId" });
            DropIndex("dbo.Students", new[] { "GenderId" });
            DropIndex("dbo.StudentAllergies", new[] { "StudentId" });
            DropIndex("dbo.StudentAllergies", new[] { "AllergyId" });
            DropIndex("dbo.Allergies", "IX_AllergiesName");
            AlterColumn("dbo.AbpTenants", "TenancyName", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.AbpTenants", "Name", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.AbpTenants", "FreeDateReceipt");
            DropColumn("dbo.AbpTenants", "CreditCardPercent");
            DropColumn("dbo.AbpTenants", "Email");
            DropColumn("dbo.AbpTenants", "ApplyAutomaticCharges");
            DropColumn("dbo.AbpTenants", "Abbreviation");
            DropColumn("dbo.AbpTenants", "PrinterTypeId");
            DropColumn("dbo.AbpTenants", "Comment");
            DropColumn("dbo.AbpTenants", "DistritctDirector");
            DropColumn("dbo.AbpTenants", "RegionalDirector");
            DropColumn("dbo.AbpTenants", "Regional");
            DropColumn("dbo.AbpTenants", "EducationalDistrict");
            DropColumn("dbo.AbpTenants", "AccreditationNumber");
            DropColumn("dbo.AbpTenants", "DistrictId");
            DropColumn("dbo.AbpTenants", "NextPeriodId");
            DropColumn("dbo.AbpTenants", "PreviousPeriodId");
            DropColumn("dbo.AbpTenants", "PeriodId");
            DropColumn("dbo.AbpTenants", "RegisterName");
            DropColumn("dbo.AbpTenants", "DirectorName");
            DropColumn("dbo.AbpTenants", "CityId");
            DropColumn("dbo.AbpTenants", "Address");
            DropColumn("dbo.AbpTenants", "Phone3");
            DropColumn("dbo.AbpTenants", "Phone2");
            DropColumn("dbo.AbpTenants", "Phone1");
            DropColumn("dbo.AbpUsers", "PositionId");
            DropColumn("dbo.AbpUsers", "UserTypeId");
            DropColumn("dbo.AbpUsers", "MustChangePassword");
            DropColumn("dbo.AbpUsers", "ParentUserId");
            DropColumn("dbo.AbpRoles", "ParentRolId");
            DropColumn("dbo.AbpRoles", "AffectTenants");
            DropTable("dbo.VerifoneBreaks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_VerifoneBreaks_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_VerifoneBreaks_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TransactionQueues",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionQueues_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionQueues_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TransactionShops",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TransactionArticleShops",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionArticleShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionArticleShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Trades",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Trades_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TradeArticles",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TradeArticles_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TaxReceiptTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TaxReceiptTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TaxReceipts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TaxReceipts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SubjectSessions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SubjectSessions_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SubjectSessions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SubjectSessionHighs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SubjectSessionHighs_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SubjectSessionHighs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sequences",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Sequences_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ScoreTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ScoreTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ReportsQueries",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsQueries_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Purchases",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Purchases_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PurchaseArticles",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseArticles_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PurchaseShops",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PurchaseArticleShops",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseArticleShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseArticleShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ProviderShops",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProviderShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProviderShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Providers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Providers_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PaymentDates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PaymentDates_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PaymentDates_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Messages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Messages_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MessageReads",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MessageRead_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MasterTenants",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MasterTenants_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Literals",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Literals_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InventoryShopDates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryShopDates_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryShopDates_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InventoryShopDateDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryShopDateDetails_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryShopDateDetails_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InventoryDates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryDates_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryDates_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InventoryDateDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryDateDetails_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryDateDetails_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.HistoryCancelReceipts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HistoryCancelReceipts_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_HistoryCancelReceipts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Evaluations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Evaluations_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Evaluations_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EvaluationPeriods",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EvaluationPeriods_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EvaluationLegends",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EvaluationLegends_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EvaluationPositionHighs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EvaluationPositionHighs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EvaluationOrderHighs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EvaluationOrderHighs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EvaluationHighs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EvaluationHighs_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EvaluationHighs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ErrorImportings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ErrorImportings_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DocumentTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Discounts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Discounts_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Discounts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PeriodDiscountDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PeriodDiscountDetails_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DiscountNameTenants",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DiscountNameTenants_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PeriodDiscounts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PeriodDiscounts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DiscountNames",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DiscountNames_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CourseValues",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CourseValues_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CourseValues_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SubjectHighs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SubjectHighs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CourseSubjectPositions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CourseSubjectPositions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CourrierPersons",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CourrierPersons_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Companies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Companies_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ClientShops",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClientShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ClientShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ChangeDateHistoryReceipts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ChangeDateHistoryReceipts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InventoryShops",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ButtonPositions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ButtonPositions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ArticleShops",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArticleShops_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ArticleShops_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ApplicationDates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ApplicationDates_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Illnesses",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Illnesses_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.StudentIllnesses",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_StudentIllnesses_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_StudentIllnesses_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Indicators",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Indicators_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.IndicatorGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IndicatorGroups_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Levels",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Levels_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CourseSessions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CourseSessions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Relationships",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Relationships_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PaymentProjections",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PaymentProjections_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PaymentProjections_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TransactionTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TransactionStudents",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionStudents_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionStudents_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TransactionDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionDetails_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionDetails_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TransactionConcepts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionConcepts_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionConcepts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Units",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Units_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Inventories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Inventory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Brands",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Brands_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ArticlesTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArticlesTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Articles",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Articles_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TransactionArticles",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionArticles_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionArticles_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TeacherTenants",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TeacherTenants_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TeacherTenants_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ReportsTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DataTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DataType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ReportsFilters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsFilters_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ReportDataSources",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportDataSources_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Reports",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Reports_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PrinterTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PrinterTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Districts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Districts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UserTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UserTenants",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserTenants_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UserPictures",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserPictures_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Positions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Positions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ConceptTenantRegistrations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConceptTenantRegistrations_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConceptTenantRegistrations_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ChangeTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ChangeTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TransactionHistories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionHistories_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionHistories_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Statuses",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Statuses_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PaymentMethods",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PaymentMethods_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Origins",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Origins_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.GrantEnterprises",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GrantEnterprises_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_GrantEnterprises_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T9Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T9Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T8Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T8Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T7Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T7Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T6Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T6Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T5Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T5Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T4Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T4Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T3Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T3Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T2Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T2Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T1Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T1Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.T10Dimensions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_T10Dimensions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ReceiptTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReceiptTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Catalogs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Catalogs_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Concepts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Concepts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CancelStatus",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CancelStatus_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Banks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Banks_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.BankAccounts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BankAccounts_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Transactions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Transactions_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Transactions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Religions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Religions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Occupations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Occupations_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EnrollmentSequences",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EnrollmentSequences_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EnrollmentSequences_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Regions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Regions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Provinces",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Provinces_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MonthlyPeriods",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MonthlyPeriods_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MonthlyPeriods_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Periods",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Periods_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Configurations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Configurations_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Configurations_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cities",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Cities_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Enrollments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Enrollments_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EnrollmentStudents",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EnrollmentStudents_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EnrollmentStudents_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CourseEnrollmentStudents",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CourseEnrollmentStudents_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CourseEnrollmentStudents_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Courses",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Courses_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Subjects",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Subjects_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Genders",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Genders_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Teachers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Teachers_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Documents",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Documents_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Documents_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CommentTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CommentTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Comments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Comments_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Comments_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Blood",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Bloods_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Students",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Students_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.StudentAllergies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_StudentAllergies_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_StudentAllergies_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Allergies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Allergies_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            AddForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions", "Id", cascadeDelete: true);
        }
    }
}

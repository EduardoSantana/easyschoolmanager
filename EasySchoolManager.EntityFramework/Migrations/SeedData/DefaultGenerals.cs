﻿using EasySchoolManager.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Migrations.SeedData
{
    public class DefaultGenerals
    {
        private EasySchoolManagerDbContext context;

        public DefaultGenerals(EasySchoolManagerDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Seed usado para crear los generos
        /// </summary>
        public void CreateGenderSeed()
        {

            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT (Genders, RESEED,1)");

            if (context.Genders.Where(x => x.Code == "M").FirstOrDefault() == null)
            {
                context.Genders.Add(new Models.Genders()
                {
                    Name = "Male",
                    Code = "M",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }

            if (context.Genders.Where(x => x.Code == "F").FirstOrDefault() == null)
            {
                context.Genders.Add(new Models.Genders()
                {
                    Name = "Female",
                    Code = "F",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }

            if (context.ReportsTypes.Where(f=>f.Id == 1).FirstOrDefault() == null)
            {
                context.ReportsTypes.Add(new Models.ReportsTypes
                {
                    Nombre = "Generals",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.ReportsTypes.Where(f => f.Id == 2).FirstOrDefault() == null)
            {
                context.ReportsTypes.Add(new Models.ReportsTypes
                {
                    Nombre = "Academics",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.ReportsTypes.Where(f => f.Id == 3).FirstOrDefault() == null)
            {
                context.ReportsTypes.Add(new Models.ReportsTypes
                {
                    Nombre = "Financial",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.ReportsTypes.Where(f => f.Id == 4).FirstOrDefault() == null)
            {
                context.ReportsTypes.Add(new Models.ReportsTypes
                {
                    Nombre = "Administrative",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }

        }

        public void CreateUserTypeSeed()
        {
            if (context.UserTypes.Count() == 0)
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT (UserTypes, RESEED,1)");

            if (context.UserTypes.FirstOrDefault(x => x.Id == 1) == null)
            {
                context.UserTypes.Add(new Models.UserTypes()
                {
                    Description = "Tenant",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = null,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }

            if (context.UserTypes.FirstOrDefault(x => x.Id == 2) == null)
            {
                context.UserTypes.Add(new Models.UserTypes()
                {
                    Description = "Enrollment",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = null,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Seed usado para crear las formas de pago
        /// </summary>
        public void CreatePaymentMethodSeed()
        {
            if (context.PaymentMethods.Count() == 0)
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT (PaymentMethods, RESEED,1)");

            if (context.PaymentMethods.Where(x => x.Id == 1).FirstOrDefault() == null)
            {
                context.PaymentMethods.Add(new Models.PaymentMethods()
                {
                    Name = "Cash",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.PaymentMethods.Where(x => x.Id == 2).FirstOrDefault() == null)
            {
                context.PaymentMethods.Add(new Models.PaymentMethods()
                {
                    Name = "Card",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.PaymentMethods.Where(x => x.Id == 3).FirstOrDefault() == null)
            {
                context.PaymentMethods.Add(new Models.PaymentMethods()
                {
                    Name = "Transfer",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.PaymentMethods.Where(x => x.Id == 4).FirstOrDefault() == null)
            {
                context.PaymentMethods.Add(new Models.PaymentMethods()
                {
                    Name = "Check",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }


        }

        /// <summary>
        /// Seed usado para crear los tipos de transacciones
        /// </summary>
        public void CreateTransactionTypeSeed()
        {
            if (context.TransactionTypes.Count() == 0)
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT (TransactionTypes, RESEED,1)");

            if (context.TransactionTypes.Where(x => x.Id == 1).FirstOrDefault() == null)
            {
                context.TransactionTypes.Add(new Models.TransactionTypes()
                {
                    Name = "Receipt",
                    IsDeleted = false,
                    IsActive = true,
                    Abbreviation = "REC",
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.TransactionTypes.Where(x => x.Id == 2).FirstOrDefault() == null)
            {
                context.TransactionTypes.Add(new Models.TransactionTypes()
                {
                    Name = "Deposit",
                    IsDeleted = false,
                    IsActive = true,
                    Abbreviation = "DEP",
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.TransactionTypes.Where(x => x.Id == 3).FirstOrDefault() == null)
            {
                context.TransactionTypes.Add(new Models.TransactionTypes()
                {
                    Name = "Transaction",
                    IsDeleted = false,
                    IsActive = true,
                    Abbreviation = "TRA",
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            
        }

        /// <summary>
        /// Seed usado para crear los origenes débito y crédito
        /// </summary>
        public void CreateOriginSeed()
        {
            if (context.Origins.Count() == 0)
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT (Origins, RESEED,1)");

            if (context.Origins.Where(x => x.Id == 1).FirstOrDefault() == null)
            {
                context.Origins.Add(new Models.Origins()
                {
                    Name = "Debit",
                    Letter = "D",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.Origins.Where(x => x.Id == 2).FirstOrDefault() == null)
            {
                context.Origins.Add(new Models.Origins()
                {
                    Name = "Credit",
                    Letter = "C",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            

        }

        /// <summary>
        /// Seed usado para crear los tipos de sangre
        /// </summary>
        public void CreateBloodSeed()
        {
            if (context.Blood.Count() == 0)
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT (Blood, RESEED,1)");

            if (context.Blood.Where(x => x.Id == 1).FirstOrDefault() == null)
            {
                context.Blood.Add(new Models.Bloods()
                {
                    Name = "A+",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.Blood.Where(x => x.Id == 2).FirstOrDefault() == null)
            {
                context.Blood.Add(new Models.Bloods()
                {
                    Name = "A-",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.Blood.Where(x => x.Id == 3).FirstOrDefault() == null)
            {
                context.Blood.Add(new Models.Bloods()
                {
                    Name = "B+",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }

            if (context.Blood.Where(x => x.Id == 4).FirstOrDefault() == null)
            {
                context.Blood.Add(new Models.Bloods()
                {
                    Name = "B-",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }

            if (context.Blood.Where(x => x.Id == 5).FirstOrDefault() == null)
            {
                context.Blood.Add(new Models.Bloods()
                {
                    Name = "AB+",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }

            if (context.Blood.Where(x => x.Id == 6).FirstOrDefault() == null)
            {
                context.Blood.Add(new Models.Bloods()
                {
                    Name = "AB-",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }

            if (context.Blood.Where(x => x.Id == 7).FirstOrDefault() == null)
            {
                context.Blood.Add(new Models.Bloods()
                {
                    Name = "O+",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }

            if (context.Blood.Where(x => x.Id == 8).FirstOrDefault() == null)
            {
                context.Blood.Add(new Models.Bloods()
                {
                    Name = "O-",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }

        }

        /// <summary>
        /// Seed usado para crear los status
        /// </summary>
        public void CreateStatusSeed()
        {
            if (context.Statuses.Count() == 0)
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT (Statuses, RESEED,1)");

            if (context.Statuses.Where(x => x.Id == 1).FirstOrDefault() == null)
            {
                context.Statuses.Add(new Models.Statuses()
                {
                    Name = "Created",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
            if (context.Statuses.Where(x => x.Id == 2).FirstOrDefault() == null)
            {
                context.Statuses.Add(new Models.Statuses()
                {
                    Name = "Canceled",
                    IsDeleted = false,
                    IsActive = true,
                    CreatorUserId = 1,
                    CreationTime = DateTime.Now
                });
                context.SaveChanges();
            }
           

        }

    }
}

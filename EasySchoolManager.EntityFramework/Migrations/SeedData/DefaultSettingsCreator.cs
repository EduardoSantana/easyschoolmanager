﻿using System.Linq;
using Abp.Configuration;
using Abp.Localization;
using Abp.Net.Mail;
using EasySchoolManager.EntityFramework;
using EasySchoolManager.Configuration;

namespace EasySchoolManager.Migrations.SeedData
{
    public class DefaultSettingsCreator
    {
        private readonly EasySchoolManagerDbContext _context;

        public DefaultSettingsCreator(EasySchoolManagerDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            //Emailing
            AddSettingIfNotExists(EmailSettingNames.DefaultFromAddress, "app@EasySchoolManager.com.do");
            AddSettingIfNotExists(EmailSettingNames.DefaultFromDisplayName, "EasySchoolManager ");
            AddSettingIfNotExists(EmailSettingNames.Smtp.Host, "smtp.gmail.com");
            AddSettingIfNotExists(EmailSettingNames.Smtp.Port, "587");
            AddSettingIfNotExists(EmailSettingNames.Smtp.UserName, "AppEasySchoolManager@gmail.com");
            AddSettingIfNotExists(EmailSettingNames.Smtp.Password, "ZHdzRwhM7CerARbvQr");
            AddSettingIfNotExists(EmailSettingNames.Smtp.EnableSsl, "true");
            AddSettingIfNotExists(EmailSettingNames.Smtp.UseDefaultCredentials, "false");
            AddSettingIfNotExists(AppSettingNames.UrlSiteHost, "http://EasySchoolManager.edu.do");
            
            //Languages
            AddSettingIfNotExists(LocalizationSettingNames.DefaultLanguage, "es");
        }

        private void AddSettingIfNotExists(string name, string value, int? tenantId = null)
        {
            if (_context.Settings.Any(s => s.Name == name && s.TenantId == tenantId && s.UserId == null))
            {
                return;
            }

            _context.Settings.Add(new Setting(tenantId, null, name, value));
            _context.SaveChanges();
        }
    }
}
﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace EasySchoolManager.EntityFramework.Repositories
{
    public abstract class EasySchoolManagerRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<EasySchoolManagerDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected EasySchoolManagerRepositoryBase(IDbContextProvider<EasySchoolManagerDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class EasySchoolManagerRepositoryBase<TEntity> : EasySchoolManagerRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected EasySchoolManagerRepositoryBase(IDbContextProvider<EasySchoolManagerDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}

﻿using EasySchoolManager.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySchoolManager.Helpers
{
   public class ConnectionToDB
    {
        public static SqlConnection GetConnection()
        {
            var db = new EasySchoolManagerDbContext();
            SqlConnection Con = new SqlConnection();
            string conString = db.GetDefaultConnectionString();

            return new SqlConnection(conString);
        }



        public static string ExecuteSqlString(string command)
        {
            SqlConnection conexion = GetConnection();
            try
            {
                SqlCommand cmd = new SqlCommand(command, conexion);
                if (conexion.State != System.Data.ConnectionState.Open)
                    conexion.Open();
                cmd.ExecuteNonQuery();
                return "";

            }
            catch (Exception err)
            { return err.Message; }
            finally
            {
                if (conexion.State != System.Data.ConnectionState.Closed)
                    conexion.Close();
            }

        }
    }
}

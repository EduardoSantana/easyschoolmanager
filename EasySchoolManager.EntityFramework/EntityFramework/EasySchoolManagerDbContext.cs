﻿using System.Data.Common;
using Abp.Zero.EntityFramework;
using EasySchoolManager.Authorization.Roles;
using EasySchoolManager.Authorization.Users;
using EasySchoolManager.MultiTenancy;
using System.Data.Entity;
using EasySchoolManager.Models;
using System.Data.Entity.ModelConfiguration.Conventions;
using System;

namespace EasySchoolManager.EntityFramework
{
    public class EasySchoolManagerDbContext : AbpZeroDbContext<Tenant, Role, User>
    {

        public string GetDefaultConnectionString()
        {
            try
            {
                return base.Database.Connection.ConnectionString;
            }
            catch (Exception err)
            {
                return err.Message;
            }
        }

        //TODO: Define an IDbSet for your Entities...
        public virtual DbSet<Allergies> Allergies { get; set; }
        public virtual DbSet<BankAccounts> BankAccounts { get; set; }
        public virtual DbSet<Banks> Banks { get; set; }
        public virtual DbSet<Bloods> Blood { get; set; }
        public virtual DbSet<Catalogs> Catalogs { get; set; }
        public virtual DbSet<ChangeTypes> ChangeTypes { get; set; }
        public virtual DbSet<Cities> Cities { get; set; }
        public virtual DbSet<Comments> Comments { get; set; }
        public virtual DbSet<CommentTypes> CommentTypes { get; set; }
        public virtual DbSet<Concepts> Concepts { get; set; }
        public virtual DbSet<CancelStatus> CancelStatus { get; set; }
        public virtual DbSet<ErrorImportings> ErrorImportings { get; set; }
        public virtual DbSet<Configurations> Configurations { get; set; }
        public virtual DbSet<CourseEnrollmentStudents> CourseEnrollmentStudents { get; set; }
        public virtual DbSet<Courses> Courses { get; set; }
        public virtual DbSet<CourseValues> CourseValues { get; set; }
        public virtual DbSet<Documents> Documents { get; set; }
        public virtual DbSet<DocumentTypes> DocumentTypes { get; set; }
        public virtual DbSet<Enrollments> Enrollments { get; set; }
        public virtual DbSet<EnrollmentStudents> EnrollmentStudents { get; set; }
        public virtual DbSet<Genders> Genders { get; set; }
        public virtual DbSet<Illnesses> Illnesses { get; set; }
        public virtual DbSet<Levels> Levels { get; set; }
        public virtual DbSet<MonthlyPeriods> MonthlyPeriods { get; set; }
        public virtual DbSet<Occupations> Occupations { get; set; }
        public virtual DbSet<Origins> Origins { get; set; }
        public virtual DbSet<PaymentDates> PaymentDates { get; set; }
        public virtual DbSet<PaymentMethods> PaymentMethods { get; set; }
        public virtual DbSet<PaymentProjections> PaymentProjections { get; set; }
        public virtual DbSet<Periods> Periods { get; set; }
        public virtual DbSet<PrinterTypes> PrinterTypes { get; set; }
        public virtual DbSet<Provinces> Provinces { get; set; }
        public virtual DbSet<Relationships> Relationships { get; set; }
        public virtual DbSet<Regions> Regions { get; set; }
        public virtual DbSet<Religions> Religions { get; set; }
        public virtual DbSet<CourseSessions> Sessions { get; set; }
        public virtual DbSet<Statuses> Statuses { get; set; }
        public virtual DbSet<StudentAllergies> StudentAllergies { get; set; }
        public virtual DbSet<StudentIllnesses> StudentIllnesses { get; set; }
        public virtual DbSet<Students> Students { get; set; }
        public virtual DbSet<Teachers> Teachers { get; set; }
        public virtual DbSet<TransactionDetails> TransactionDetails { get; set; }
        public virtual DbSet<TransactionHistories> TransactionHistories { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }
        public virtual DbSet<TransactionQueues> TransactionsQueues { get; set; }
        public virtual DbSet<TransactionStudents> TransactionStudents { get; set; }
        public virtual DbSet<TransactionConcepts> TransactionConcepts { get; set; }
        public virtual DbSet<TransactionTypes> TransactionTypes { get; set; }
        public virtual DbSet<Reports> Reports { get; set; }
        public virtual DbSet<ReportsFilters> ReportsFilters { get; set; }
        public virtual DbSet<ReportsTypes> ReportsTypes { get; set; }
        public virtual DbSet<ReportsQueries> ReportsQueries { get; set; }
        public virtual DbSet<EnrollmentSequences> EnrollmentSequences { get; set; }
        public virtual DbSet<MasterTenants> MasterTenant { get; set; }
        public virtual DbSet<UserTenants> UserTenants { get; set; }
        public virtual DbSet<Districts> Districts { get; set; }
        public virtual DbSet<ReportDataSources> ReportDataSources { get; set; }
        public virtual DbSet<DataType> DataTypes { get; set; }
        public virtual DbSet<Models.Sequences> Sequences { get; set; }
        public virtual DbSet<Articles> Articles { get; set; }
        public virtual DbSet<ArticlesTypes> ArticlesTypes { get; set; }
        public virtual DbSet<Brands> Brands { get; set; }
        public virtual DbSet<Units> Units { get; set; }
        public virtual DbSet<Providers> Providers { get; set; }
        public virtual DbSet<ApplicationDates> ApplicationDates { get; set; }

        public virtual DbSet<TransactionArticles> TransactionArticles { get; set; }
        public virtual DbSet<Purchases> Pursaches { get; set; }
        public virtual DbSet<PurchaseArticles> PurchaseDetails { get; set; }
        public virtual DbSet<CourrierPersons> CourrierPersons { get; set; }
        public virtual DbSet<Trades> Trades { get; set; }
        public virtual DbSet<TradeArticles> TradeDetails { get; set; }
        public virtual DbSet<Inventory> Inventory { get; set; }
        public virtual DbSet<ReceiptTypes> ReceiptTypes { get; set; }
        public virtual DbSet<UserPictures> UserPictures { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<MessageRead> MessageRead { get; set; }
        public virtual DbSet<UserTypes> UserTypes { get; set; }
        public virtual DbSet<Subjects> Subjects { get; set; }
        public virtual DbSet<ScoreTypes> ScoreTypes { get; set; }
        public virtual DbSet<TeacherTenants> TeacherTenants { get; set; }
        public virtual DbSet<InventoryDates> InventoryDates { get; set; }
        public virtual DbSet<InventoryDateDetails> InventoryDateDetails { get; set; }

        public virtual DbSet<ArticleShops> ArticleShops { get; set; }
        public virtual DbSet<ClientShops> ClientShops { get; set; }
        public virtual DbSet<InventoryShopDateDetails> InventoryShopDateDetails { get; set; }
        public virtual DbSet<InventoryShopDates> InventoryShopDates { get; set; }
        public virtual DbSet<InventoryShops> InventoryShops { get; set; }
        public virtual DbSet<ProviderShops> ProviderShops { get; set; }
        public virtual DbSet<PurchaseArticleShops> PurchaseArticleShops { get; set; }
        public virtual DbSet<PurchaseShops> PurchaseShops { get; set; }
        public virtual DbSet<TransactionArticleShops> TransactionArticleShops { get; set; }
        public virtual DbSet<TransactionShops> TransactionShops { get; set; }
        public virtual DbSet<ButtonPositions> ButtonPositions { get; set; }

        public virtual DbSet<Evaluations> Evaluations { get; set; }
        public virtual DbSet<Literals> Literals { get; set; }
        public virtual DbSet<EvaluationPeriods> EvaluationPerios { get; set; }
        public virtual DbSet<EvaluationLegends> EvaluationLegends { get; set; }
        public virtual DbSet<IndicatorGroups> IndicatorGroups { get; set; }
        public virtual DbSet<Indicators> Indicators { get; set; }
        public virtual DbSet<SubjectSessions> SubjectSessions { get; set; }
        public virtual DbSet<Positions> Positions { get; set; }

        public virtual DbSet<GrantEnterprises> GrantEnterprises { get; set; }
        public virtual DbSet<HistoryCancelReceipts> HistoryCancelReceipts { get; set; }
        public virtual DbSet<SubjectHighs> SubjectHighs { get; set; }
        public virtual DbSet<EvaluationHighs> EvaluationHighs { get; set; }
        public virtual DbSet<EvaluationOrderHighs> EvaluationOrderHighs { get; set; }
        public virtual DbSet<EvaluationPositionHighs> EvaluationPositionHighs { get; set; }
        public virtual DbSet<SubjectSessionHighs> SubjectSessionHighs { get; set; }

        public virtual DbSet<TaxReceiptTypes> TaxReceiptTypes { get; set; }
        public virtual DbSet<TaxReceipts> TaxReceipts { get; set; }
        public virtual DbSet<Companies> Companies { get; set; }
        public virtual DbSet<ConceptTenantRegistrations> ConceptTenantRegistrations { get; set; }
        public virtual DbSet<VerifoneBreaks> VerifoneBreaks { get; set; }
        public virtual DbSet<ChangeDateHistoryReceipts> ChangeDateHistoryReceipts { get; set; }


        public virtual DbSet<Discounts> Discounts { get; set; }
        public virtual DbSet<DiscountNames> DiscountNames { get; set; }
        public virtual DbSet<PeriodDiscounts> PeriodDiscounts { get; set; }
        public virtual DbSet<PeriodDiscountDetails> PeriodDiscountDetails { get; set; }
        public virtual DbSet<CourseSubjectPositions> CourseSubjectPositions { get; set; }
        public virtual DbSet<DiscountNameTenants> DiscountNameTenants { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public EasySchoolManagerDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in EasySchoolManagerDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of EasySchoolManagerDbContext since ABP automatically handles it.
         */
        public EasySchoolManagerDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public EasySchoolManagerDbContext(DbConnection existingConnection)
         : base(existingConnection, false)
        {
        }

        public EasySchoolManagerDbContext(DbConnection existingConnection, bool contextOwnsConnection)
         : base(existingConnection, contextOwnsConnection)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();


            modelBuilder.Entity<Models.MasterTenants>().Property(x => x.CreditCardPercent).HasPrecision(18, 6);

            modelBuilder.Entity<Models.Transactions>().Property(x => x.CreditCardPercent).HasPrecision(18, 6);

            modelBuilder.Entity<Concepts>()
                .Property(x => x.Description)
                .HasColumnName("Description")
                .HasUniqueIndexAnnotation("IX_ConceptTenantIdName", 1);

            modelBuilder.Entity<Catalogs>()
                .Property(x => x.AccountNumber)
                .HasColumnName("AccountNumber")
                .HasUniqueIndexAnnotation("IX_CatalogAccountNumber", 1);

            modelBuilder.Entity<EnrollmentSequences>()
                .Property(x => x.TenantId)
                .HasColumnName("TenantId")
                .HasUniqueIndexAnnotation("UQ_EnrollmentSequencesTenantIdEnrollmentId", 0);
            modelBuilder.Entity<EnrollmentSequences>()
                .Property(x => x.EnrollmentId)
                .HasColumnName("EnrollmentId")
                .HasUniqueIndexAnnotation("UQ_EnrollmentSequencesTenantIdEnrollmentId", 1);

            modelBuilder.Entity<Reports>()
                .Property(x => x.ReportCode)
                .HasColumnName("ReportCode")
                .HasUniqueIndexAnnotation("IX_Report_ReportCode", 1);

            modelBuilder.Entity<TeacherTenants>()
                .Property(x => x.TenantId)
                .HasColumnName("TenantId")
                .HasUniqueIndexAnnotation("IX_TeacherTenantId", 1);

            modelBuilder.Entity<TeacherTenants>()
                .Property(x => x.TeacherId)
                .HasColumnName("TeacherId")
                .HasUniqueIndexAnnotation("IX_TeacherTenantId", 2);


            modelBuilder.Entity<CourseValues>()
                .Property(x => x.TenantId)
                .HasColumnName("TenantId")
                .HasUniqueIndexAnnotation("UQ_CourseValuesTenantPeriods", 1);
            modelBuilder.Entity<CourseValues>()
                .Property(x => x.PeriodId)
                .HasColumnName("PeriodId")
                .HasUniqueIndexAnnotation("UQ_CourseValuesTenantPeriods", 2);
            modelBuilder.Entity<CourseValues>()
                .Property(x => x.CourseId)
                .HasColumnName("CourseId")
                .HasUniqueIndexAnnotation("UQ_CourseValuesTenantPeriods", 3);

            modelBuilder.Entity<Transactions>()
                .Property(x => x.Sequence)
                .HasColumnName("Sequence")
                .HasUniqueIndexAnnotation("UQ_TransactionTenantTransactionType", 1);
            modelBuilder.Entity<Transactions>()
                .Property(x => x.TenantId)
                .HasColumnName("TenantId")
                .HasUniqueIndexAnnotation("UQ_TransactionTenantTransactionType", 2);
            modelBuilder.Entity<Transactions>()
                .Property(x => x.TransactionTypeId)
                .HasColumnName("TransactionTypeId")
                .HasUniqueIndexAnnotation("UQ_TransactionTenantTransactionType", 3);

            modelBuilder.Entity<Periods>()
                .Property(x => x.Period)
                .HasColumnName("Period")
                .HasUniqueIndexAnnotation("UQ_Periods1", 1);


            modelBuilder.Entity<EnrollmentStudents>()
                .Property(x => x.TenantId)
                .HasColumnName("TenantId")
                .HasUniqueIndexAnnotation("UQ_EnrollmentStudentPeriodTenant", 1);
            modelBuilder.Entity<EnrollmentStudents>()
                .Property(x => x.PeriodId)
                .HasColumnName("PeriodId")
                .HasUniqueIndexAnnotation("UQ_EnrollmentStudentPeriodTenant", 2);
            modelBuilder.Entity<EnrollmentStudents>()
                .Property(x => x.EnrollmentId)
                .HasColumnName("EnrollmentId")
                .HasUniqueIndexAnnotation("UQ_EnrollmentStudentPeriodTenant", 3);
            modelBuilder.Entity<EnrollmentStudents>()
                .Property(x => x.StudentId)
                .HasColumnName("StudentId")
                .HasUniqueIndexAnnotation("UQ_EnrollmentStudentPeriodTenant", 4);


            modelBuilder.Entity<ArticleShops>()
                .Property(x => x.TenantId)
                .HasColumnName("TenantId")
                .HasUniqueIndexAnnotation("UQ_ArticleShops_Tenant_ButtonPosition", 1);

            modelBuilder.Entity<ArticleShops>()
                .Property(x => x.ButtonPositionId)
                .HasColumnName("ButtonPositionId")
                .HasUniqueIndexAnnotation("UQ_ArticleShops_Tenant_ButtonPosition", 2);


            modelBuilder.Entity<Indicators>()
                .Property(x => x.IndicatorGroupId)
                .HasColumnName("IndicatorGroupId")
                .HasUniqueIndexAnnotation("UQ_Indicator_IndicatorGroup_Sequence", 1);

            modelBuilder.Entity<Indicators>()
                .Property(x => x.Sequence)
                .HasColumnName("Sequence")
                .HasUniqueIndexAnnotation("UQ_Indicator_IndicatorGroup_Sequence", 2);



            modelBuilder.Entity<Literals>()
                .Property(x => x.Letter)
                .HasColumnName("Letter")
                .HasUniqueIndexAnnotation("UQ_LiteralLetter", 1);

            modelBuilder.Entity<EvaluationPeriods>()
                .Property(x => x.Name)
                .HasColumnName("Name")
                .HasUniqueIndexAnnotation("UQ_EvaluationPerios_Name_Period", 1);
            modelBuilder.Entity<EvaluationPeriods>()
                .Property(x => x.PeriodId)
                .HasColumnName("PeriodId")
                .HasUniqueIndexAnnotation("UQ_EvaluationPerios_Name_Period", 2);

            modelBuilder.Entity<EvaluationLegends>()
                .Property(x => x.Name)
                .HasColumnName("Name")
                .HasUniqueIndexAnnotation("UQ_EvaluationLegends_Name", 1);

            modelBuilder.Entity<Evaluations>()
                .Property(x => x.IndicatorId)
                .HasColumnName("IndicatorId")
                .HasUniqueIndexAnnotation("UQ_Evaluations_Indicator_ES_P_EV_Tenant", 1);
            modelBuilder.Entity<Evaluations>()
                .Property(x => x.EvaluationLegendId)
                .HasColumnName("EvaluationLegendId")
                .HasUniqueIndexAnnotation("UQ_Evaluations_Indicator_ES_P_EV_Tenant", 2);
            modelBuilder.Entity<Evaluations>()
                .Property(x => x.EnrollmentStudentId)
                .HasColumnName("EnrollmentStudentId")
                .HasUniqueIndexAnnotation("UQ_Evaluations_Indicator_ES_P_EV_Tenant", 3);
            modelBuilder.Entity<Evaluations>()
                .Property(x => x.EvaluationPeriodId)
                .HasColumnName("EvaluationPeriodId")
                .HasUniqueIndexAnnotation("UQ_Evaluations_Indicator_ES_P_EV_Tenant", 4);
            modelBuilder.Entity<Evaluations>()
                .Property(x => x.ComplementForDeletion)
                .HasColumnName("ComplementForDeletion")
                .HasUniqueIndexAnnotation("UQ_Evaluations_Indicator_ES_P_EV_Tenant", 5);

            modelBuilder.Entity<EvaluationHighs>()
               .Property(x => x.TenantId)
               .HasColumnName("TenantId")
               .HasUniqueIndexAnnotation("UQ_EvaluationHighs_Subje_Enrol_Evalu_Tenant", 1);

            modelBuilder.Entity<EvaluationHighs>()
               .Property(x => x.SubjectHighId)
               .HasColumnName("SubjectHighId")
               .HasUniqueIndexAnnotation("UQ_EvaluationHighs_Subje_Enrol_Evalu_Tenant", 2);

            modelBuilder.Entity<EvaluationHighs>()
              .Property(x => x.EnrollmentStudentId)
              .HasColumnName("EnrollmentStudentId")
              .HasUniqueIndexAnnotation("UQ_EvaluationHighs_Subje_Enrol_Evalu_Tenant", 3);

            modelBuilder.Entity<EvaluationHighs>()
              .Property(x => x.EvaluationOrderHighId)
              .HasColumnName("EvaluationOrderHighId")
              .HasUniqueIndexAnnotation("UQ_EvaluationHighs_Subje_Enrol_Evalu_Tenant", 4);

            modelBuilder.Entity<TeacherTenants>()
             .Property(x => x.TenantId)
             .HasColumnName("TenantId")
             .HasUniqueIndexAnnotation("IX_TeacherTenantId_Tenant_Teacher", 1);

            modelBuilder.Entity<TeacherTenants>()
             .Property(x => x.TeacherId)
             .HasColumnName("TeacherId")
             .HasUniqueIndexAnnotation("IX_TeacherTenantId_Tenant_Teacher", 2);


            modelBuilder.Entity<Discounts>()
             .Property(x => x.TenantId)
             .HasColumnName("TenantId")
             .HasUniqueIndexAnnotation("IX_DiscountConceptStudentTenant", 0);

            modelBuilder.Entity<Discounts>()
             .Property(x => x.StudentId)
             .HasColumnName("StudentId")
             .HasUniqueIndexAnnotation("IX_DiscountConceptStudentTenant", 1);

            modelBuilder.Entity<Discounts>()
             .Property(x => x.PeriodDiscountDetailId)
             .HasColumnName("PeriodDiscountDetailId")
             .HasUniqueIndexAnnotation("IX_DiscountConceptStudentTenant", 2);

            modelBuilder.Entity<Discounts>()
             .Property(x => x.ConceptId)
             .HasColumnName("ConceptId")
             .HasUniqueIndexAnnotation("IX_DiscountConceptStudentTenant", 3);

            modelBuilder.Entity<Discounts>()
             .Property(x => x.PeriodId)
             .HasColumnName("PeriodId")
             .HasUniqueIndexAnnotation("IX_DiscountConceptStudentTenant", 4);
        }

    }
}

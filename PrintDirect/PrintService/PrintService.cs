﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace PrintService
{
    public partial class MyPrintService : ServiceBase
    {
        public MyPrintService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            IPAddress addres = IPAddress.Any;
            int.TryParse(System.Configuration.ConfigurationManager.AppSettings["port"] ?? "8081", out int puerto);
            TcpListener server = new TcpListener(addres, puerto);
            server.Start(10);

            server.BeginAcceptTcpClient(connected, server);

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 60000; // 60 seconds  
            timer.Elapsed += Timer_Elapsed;
            timer.Enabled = true;
            timer.Start();

            //var log = new EventLog();
            //if (!System.Diagnostics.EventLog.SourceExists("MyPrintServiceLog"))
            //{
            //    System.Diagnostics.EventLog.CreateEventSource(
            //        "MyPrintService", "MyPrintServiceLog");
            //}
            //log.Source = "MyPrintService";
            //log.Log = "MyPrintServiceLog";
            //log.WriteEntry("Inicio de la aplicacion" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void connected(IAsyncResult ar)
        {
            TcpListener _server = (TcpListener)ar.AsyncState;
            _server.BeginAcceptTcpClient(connected, _server);

            TcpClient tcpClient = _server.EndAcceptTcpClient(ar);
            tcpClient.ReceiveBufferSize = 99999995;
            string reportString = "";
            Task.Delay(1000).Wait();

            if (tcpClient.Available <= 0)
            {
                tcpClient.Close();
                return;
            }

            try
            {


                while (tcpClient.Available > 0)
                {
                    byte[] data = new byte[tcpClient.Available];
                    var nt = tcpClient.GetStream();
                    nt.Read(data, 0, tcpClient.Available);
                    reportString += System.Text.Encoding.ASCII.GetString(data);

                }

                string code = "||";
                int index = reportString.IndexOf(code);

                reportString = reportString.Substring(index + code.Length);


                byte[] report = Convert.FromBase64String(reportString);

                //Bitmap img = new Bitmap(new MemoryStream(report));
                //PrintDocument pd = new PrintDocument();
                //pd.DefaultPageSettings.Margins.Top = 0;
                //pd.DefaultPageSettings.Margins.Left = 0;
                //pd.DefaultPageSettings.Margins.Bottom = 0;
                //pd.DefaultPageSettings.Margins.Right = 0;
                //pd.OriginAtMargins = true;
                //pd.PrintPage += delegate (object o, PrintPageEventArgs e) {
                //    e.Graphics.DrawImage(img, new Point(0, 0));
                //};
                //pd.Print();


                string adobePath = System.Configuration.ConfigurationManager.AppSettings["adobePath"];

                if (!File.Exists(adobePath))
                    adobePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)}\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe";

                if (!File.Exists(adobePath))
                    adobePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)}\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe";

                Process proc = new Process();
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.StartInfo.Verb = "print";

                string pdfFileName = Path.GetTempFileName() + ".pdf";
                File.WriteAllBytes(pdfFileName, report);
                //Define location of adobe reader/command line
                //switches to launch adobe in "print" mode
                proc.StartInfo.FileName = adobePath;
                proc.StartInfo.Arguments = String.Format(@"/p /h {0}", pdfFileName);
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.CreateNoWindow = true;
                proc.EnableRaisingEvents = true;

                proc.Start();

                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.Exited += delegate (object sender, EventArgs e)
                {
                    proc.Kill();
                };
                proc.Close();


                Task.Delay(10000).ContinueWith((t) =>
                {
                    File.Delete(pdfFileName);
                });

                string _response = "HTTP/1.1 200 OK\nAccess-Control-Allow-Origin:*\nContent-Type:text/plain";

                byte[] response = System.Text.Encoding.ASCII.GetBytes(_response);
                tcpClient.Client.Send(response);

            }
            catch (Exception ex)
            {
                string _response = "HTTP/1.1 500 Error\n\n{\"result\":[],\"targetUrl\":null,\"success\":false,\"error\":\"" + ex.Message + "\",\"unAuthorizedRequest\":false,\"__abp\":true}";
                byte[] response = System.Text.Encoding.ASCII.GetBytes(_response);
                tcpClient.Client.Send(response);
            }
            tcpClient.Close();

        }



        protected override void OnStop()
        {
        }
    }
}

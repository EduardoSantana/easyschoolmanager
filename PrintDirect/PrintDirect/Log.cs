﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


    public class Log
    {
        public static void InicializarLog(String rutaLog, String applicationName)
        {
            RutaLog = rutaLog + applicationName;
            ApplicationName = applicationName;
        }

        public static String RutaLog = string.Empty;
        public static String ApplicationName = string.Empty;

        public static void AppendInfile(String Contenido)
        {
            if (RutaLog == string.Empty || ApplicationName == string.Empty)
            {
                System.Windows.Forms.MessageBox.Show("No se ha inicializado el Log. Inicialicelo con el método InicializarLog");
                return;
            }

            try
            {
                if (!System.IO.Directory.Exists(RutaLog))
                    System.IO.Directory.CreateDirectory(RutaLog);

                String Archivo = RutaLog +  @"\" + ApplicationName + ".txt";
                StreamWriter rs = File.AppendText(Archivo);
                if (!File.Exists(Archivo))
                    rs.WriteLine("Fecha | Hora | contenido ");
                rs.WriteLine("{0}|{1}|{2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), Contenido);
                rs.Close();
            }
            catch (Exception err)
            {
                System.Windows.Forms.MessageBox.Show(string.Format("No se ha podido guardar en el log debido a un error.  [{0}]",err.Message));
            }
        }

    }

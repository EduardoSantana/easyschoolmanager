﻿using System;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

namespace PrintDirect
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        TcpListener server = null;
        private void Form1_Load(object sender, EventArgs e)
        {
            Log.InicializarLog(Application.StartupPath + "\\", Application.ProductName);
            try
            {
                notifyIcon1.Text = "GEDUCOM(Waiting Connection)";
                this.Visible = false;
                Top = -2000;
                Left = -2000;
                ShowInTaskbar = false;
                IPAddress addres = IPAddress.Any;
                int.TryParse(System.Configuration.ConfigurationManager.AppSettings["port"] ?? "8081", out int puerto);
                server = new TcpListener(addres, puerto);
                server.Start(100);

                server.BeginAcceptTcpClient(connected, server);

                notifyIcon1.Text = "GEDUCOM(Conected 1.3)";

                Log.AppendInfile("Proceso Iniciado");

            }
            catch (Exception err)
            {
                Log.AppendInfile(err.Message);
                if (err.InnerException != null)
                    Log.AppendInfile(err.InnerException.Message);
            }
        }

        public void connected(IAsyncResult ar)
        {
            try
            {
                Log.AppendInfile("Method Connected");

                TcpListener _server = (TcpListener)ar.AsyncState;
                _server.BeginAcceptTcpClient(connected, _server);


                Log.AppendInfile("Paso EndAcceptTcpClient");

                TcpClient tcpClient = _server.EndAcceptTcpClient(ar);

                Log.AppendInfile("Paso tcpClient.ReceiveBufferSize = 99999995");
                tcpClient.ReceiveBufferSize = 999999995;

                Log.AppendInfile("Paso EndAcceptTcpClient");
                string reportString = "";


                Log.AppendInfile("Paso System.Threading.Thread.Sleep");

                System.Threading.Thread.Sleep(200);

                var DataAvailabe = tcpClient.Available;
                if (DataAvailabe <= 0)
                {
                    Log.AppendInfile("Closing TCP Client");

                    tcpClient.Close();
                    return;
                }

                try
                {

                    Log.AppendInfile("Paso while (tcpClient.Available > 0)");

                    while (DataAvailabe > 0)
                    {
                        byte[] data = new byte[DataAvailabe];
                        var nt = tcpClient.GetStream();
                        nt.Read(data, 0, DataAvailabe);
                        reportString += System.Text.Encoding.ASCII.GetString(data);
                        DataAvailabe = tcpClient.Available;
                    }

                    Log.AppendInfile("CADENA1 : " + reportString);

                    string code = "||";
                    int index = reportString.IndexOf(code);

                    reportString = reportString.Substring(index + code.Length);


                    Log.AppendInfile("Paso Convert.FromBase64String(reportString)");
                    byte[] report = Convert.FromBase64String(reportString);

                    Log.AppendInfile("CADENA2 : " + reportString);

                    // 1. Open the PDF file!
                    //var file = new org.pdfclown.files.File(new org.pdfclown.bytes.Stream(new MemoryStream(report)));
                    //var document = file.Document;

                    //// 2. Rasterize the first page!
                    //var renderer = new org.pdfclown.tools.Renderer();
                    //renderer.Print(document);
                    //var image = renderer.Render(document.Pages[0], document.Pages[0].Size);

                    // 3. Save the rasterized image!
                    //ImageIO.write(image, "jpg", myOutputPath);
                    //var d = new Spire.Pdf.PdfDocument();
                    //string name = Path.GetTempFileName();
                    //File.WriteAllBytes(name, report);
                    //d.LoadFromFile(name);


                    //Image emf = d.SaveAsImage(0, Spire.Pdf.Graphics.PdfImageType.Metafile);

                    //MemoryStream fs = new MemoryStream(report);
                    //BinaryFormatter bf = new BinaryFormatter();
                    //var pages = (List<Stream>)bf.Deserialize(fs);
                    //fs.Close();

                    // Bitmap emf = new Bitmap(new MemoryStream(report));
                    //PrintDocument pd = new PrintDocument();
                    //pd.DefaultPageSettings.Margins.Top = 0;
                    //pd.DefaultPageSettings.Margins.Left = 0;
                    //pd.DefaultPageSettings.Margins.Bottom = 0;
                    //pd.DefaultPageSettings.Margins.Right = 0;
                    //pd.OriginAtMargins = true;
                    //int printingPage = 0;
                    //pd.PrintPage += delegate (object o, PrintPageEventArgs e)
                    //{
                    //    pages[printingPage].Position = 0;
                    //    var _emf = new Bitmap(pages[printingPage]);
                    //    e.Graphics.ScaleTransform(2.0f, 2.0f);
                    //    e.Graphics.DrawImage(_emf, new Rectangle(new Point(0, 0), _emf.Size), new Rectangle(new Point(0, 0), _emf.Size), GraphicsUnit.Pixel);

                    //    printingPage++;

                    //    e.HasMorePages = (printingPage < pages.Count);
                    //};

                    //pd.Print();

                    Log.AppendInfile("Start Handling Adobe");


                    string adobePath = System.Configuration.ConfigurationManager.AppSettings["adobePath"];

                    if (!File.Exists(adobePath))
                        adobePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)}\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe";

                    if (!File.Exists(adobePath))
                        adobePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)}\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe";

                    Log.AppendInfile("Start Process");

                    Process proc = new Process();
                    proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    proc.StartInfo.Verb = "print";


                    Log.AppendInfile("Getting FileName");
                    string pdfFileName = Path.GetTempFileName() + ".pdf";
                    File.WriteAllBytes(pdfFileName, report);
                    //Define location of adobe reader/command line
                    //switches to launch adobe in "print" mode
                    proc.StartInfo.FileName = adobePath;
                    proc.StartInfo.Arguments = String.Format(@"/p /h {0}", pdfFileName);
                    proc.StartInfo.UseShellExecute = false;
                    proc.StartInfo.CreateNoWindow = true;
                    proc.EnableRaisingEvents = true;

                    proc.Start();
                    proc.Close();

                    Log.AppendInfile("Executing Process");

                    //File.Delete(name);
                    string _response = "HTTP/1.1 200 OK\nAccess-Control-Allow-Origin:*\nContent-Type:text/plain";

                    byte[] response = System.Text.Encoding.ASCII.GetBytes(_response);
                    tcpClient.Client.Send(response);


                    while (true)
                    {
                        System.Threading.Thread.Sleep(10000);
                        break;
                    }
                    //Task.Delay(5000).ContinueWith((t) =>
                    //{

                    Log.AppendInfile("Deleting File");
                    File.Delete(pdfFileName);
                    //});


                }
                catch (Exception ex)
                {

                    string _response = "HTTP/1.1 500 Error\n\n{\"result\":[],\"targetUrl\":null,\"success\":false,\"error\":\"" + ex.Message + "\",\"unAuthorizedRequest\":false,\"__abp\":true}";
                    byte[] response = System.Text.Encoding.ASCII.GetBytes(_response);
                    tcpClient.Client.Send(response);
                    Log.AppendInfile("Error" + ex.Message);
                    if (ex.InnerException != null)
                        Log.AppendInfile("Error" + ex.InnerException.Message);
                }
                tcpClient.Close();
                Log.AppendInfile("Puerto cerrado ");
            }
            catch (Exception err)
            { Log.AppendInfile(err.Message); }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                server.Stop();
            }
            catch { }
            Application.Exit();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                server.Stop();
            }
            catch { }
        }
    }
}

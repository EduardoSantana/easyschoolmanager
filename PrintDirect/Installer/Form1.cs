﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.ServiceProcess;

namespace Installer
{
    public partial class Form1 : Form
    {
        string windir = System.Environment.GetEnvironmentVariable("windir");
        string winDirFrameWork = "";
        public Form1()
        {
            InitializeComponent();

             winDirFrameWork = windir + @"\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe";

            if (File.Exists(windir + @"\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe"))
                winDirFrameWork = windir + @"\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe";

            Refrescar();
        }

        void Refrescar() {
            btnDesinstalar.Enabled = File.Exists(windir + "\\Services\\MyPrintService.exe");
            btnIniciar.Enabled = File.Exists(windir + "\\Services\\MyPrintService.exe");
            btnInstalar.Enabled = !File.Exists(windir + "\\Services\\MyPrintService.exe");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnInstalar_Click(object sender, EventArgs e)
        {
            string root = Application.StartupPath;
           

            string install = windir + "\\Services";

            if (!Directory.Exists(install))
                Directory.CreateDirectory(install);

            //File.Copy(root + "\\MyPrintService.exe.config", install + "\\MyPrintService.exe.config",true);
            //File.Copy(root + "\\MyPrintService.exe", install + "\\MyPrintService.exe",true);

            File.WriteAllBytes(install + "\\MyPrintService.exe.config", global::Installer.Properties.Resources.MyPrintService_exe);
            File.WriteAllBytes(install + "\\MyPrintService.exe", global::Installer.Properties.Resources.MyPrintService);

            System.Diagnostics.Process.Start(winDirFrameWork, "-i " + install + "\\MyPrintService.exe").WaitForExit(30000);

            Exited();
            
        }

        private void Exited()
        {
            ServiceController sc = new ServiceController("MyPrintService");

            try
            {
                if (sc != null && sc.Status == ServiceControllerStatus.Stopped)
                {
                    sc.Start();
                }
                sc.WaitForStatus(ServiceControllerStatus.Running);
                sc.Close();
                MessageBox.Show("Servicio iniciado correctamente.");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo iniciar el servicio automaticamente" );
            }
            Refrescar();
        }

        private void btnDesinstalar_Click(object sender, EventArgs e)
        {
            string root = Application.StartupPath;

          
            string install = windir + "\\Services\\MyPrintService.exe";

            if (!File.Exists(install))
                return;

            System.Diagnostics.Process.Start(winDirFrameWork, "-u " + install ).WaitForExit(30000);

            File.Delete(install);
            Refrescar();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            ServiceController sc = new ServiceController("MyPrintService");

            try
            {
                if (sc != null && sc.Status == ServiceControllerStatus.Stopped)
                {
                    sc.Start();
                }
                sc.WaitForStatus(ServiceControllerStatus.Running);
                sc.Close();
                MessageBox.Show("Servicio iniciado correctamente.");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al iniciar el servicio: " + ex.Message);
            }
        }
    }
}
